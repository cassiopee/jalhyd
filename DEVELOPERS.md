# JaLHyd developers documentation

See also [JaLHyd installation instructions](README.md)

## Software stack

 * typescript
 * jasmine

## Requirements

 * nodejs / npm

## Main concepts

### Calculation modules (Nubs)

JaLHyd offers **calculation modules** also called **Nub**s, that are each responsible for solving a problem, usually by solving an equation.

#### calculator type

Each Nub must have a **calculator type**, that is an item of the `CalculatorType` enum. It is stored in the `calcType` property and must be defined in the properties passed to the Nub `setProperties` method.

#### calculated parameter

Although there are a few exceptions (ex: `SectionParametree`), each Nub generally has a default **calculated parameter**, that is the one of its parameters that will be set as "to be calculated" by default. It is stored in `this._defaultCalculatedParam` and should be defined in the Nub's constructor, followed by a call to `this.resetDefaultCalculatedParam()`.

#### children

A Nub might have one or more **children Nub** (`this._children`), and so on with an unrestricted depth. This is the preferred aggregation mechanism; for instance Nubs needing a calculable **Section** aggregate a Section Nub as their first and only child (ex: `RegimeUniforme`)

#### CalcSerie(), Calc(), Equation(), Solve()

A Nub's calculation is handled through a few different methods.

**`Equation()`** and **`Solve()`** receive a parameter symbol as input, and have to calculate the value of this parameter, given the values of other parameters. Those methods must return a `Result` object, but should not copy it to `this._result` unless it contains only error messages (see chapter "Log" below).

`Equation()` is dedicated to **analytical** calculation (ie. solving an equation) whereas `Solve()` is dedicated to calculation using a **numeric method** such as Newton or Brent (sometimes abusively named "Dichotomy").

**`Calc()`** is a wrapper around `Equation()` and `Solve()` that, given a parameter symbol, triggers the relevant method depending on the parameter's **calculability**. This method produces a `Result` object, and copies it to the current result element of `this._result`.

**`CalcSerie()`** is a wrapper around `Calc()` that triggers it as many times as needed when there is one or more variating parameters. It returns a `Result` object that contains as many `ResultElement` as there are variation steps.  It is also responsible for **chain calculation**.

One should always call `CalcSerie()` to obtain the Nub's result, whatever the input conditions are.

#### dependencies to other Nubs

To be calculated, a Nub might require the results of one or more other Nubs. See "Links" chapter below.

### Parameters

Parameters are objects that represent the input/outputs of a Nub's equation. All the parameters of a given Nub must be grouped in an object derived from `ParamsEquation`, (ex: `CloisonsParams` for Nub `Cloisons`). Only one parameter at a time may be the **calculated parameter** of its Nub; its value is computed based on the fixed values of other parameters.

A parameter is characterized by a **symbol** (ex: "Q"), a **definition domain** (from the `ParamDomainValue` enum, or a free interval), a **unit** (for display purposes only, ex: "m³/s"), an **initial value**, a **family** (see "Links" below), its **calculability**, a **mode** and a **visibility** (for display purposes only).

#### calculability

A parameters **calculability** indicates whether and how it may be calculated, and which kind of values it can have. It is picked among `ParamCalculability` enum :

 * `FIXED` : parameter may have any value, may not vary, may not be calculated
 * `FREE` : parameter may have any value, may vary, may not be calculated
 * `EQUATION` : parameter may have any value, may vary, may be calculated analytically
 * `DICHO` : parameter may have any value, may vary, may be calculated using numerical method

Calculability of a Nub's parameters is defined in the Nub's class by overloading `setParametersCalculability()`.

#### mode

At a given time, a parameter may be in one of the following states, called **modes** and picked among `ParamValueMode` enum. Parameter **calculability** may restrict the available modes.

 * `SINGLE` : parameter has a single fixed numerical value
 * `MINMAX` : parameter is varying and has multiple successive numerical values, defined by a minimum value, a maximum value and a step value
 * `LISTE` : parameter is varying and has multiple successive numerical values, defined in extension by a numbers list
 * `CALCUL` : parameter is to be calculated; its value will be the result of `CalcSerie()`
 * `LINK` : parameter is targetting another parameter or result; its value is the target's value

#### variating parameters

Parameters are said to be **variating** or **variated** when they have multiple successive values. This is the case when the parameter is in `MINMAX` or `LISTE` mode, or when it is in `LINK` mode targetting a variating parameter or result. A result is variating as soon as one of its parameters is variating.

When multiple parameters (for ex. P1 and P2) of the same Nub are variating, `CalcSerie()` iterates simultaneously on all those variating parameters, so that the nth calculation step is performed using the nth value of P1 and the nth value of P2. If P1 and P2 have a different number of values (for ex. 3 and 7), the number of calculation steps will be the that of the "longest" parameter, in this example 7. All the other varying parameters will be extended using their **extension strategy**, picked from `ExtensionStrategy` enum : `REPEAT_LAST` (repeat last value as many times as needed) or `RECYCLE` (repeat the whole series from the beginning).

#### values

A parameter has different contextualized "values" at a time :

 * `.singleValue` : the single fixed value that is set by the user, when in `SINGLE` mode
 * `.valueList` : the list of multiple values that is set by the user, when in `LISTE` mode
 * `.min` : the minimum value that is set by the user, when in `MINMAX` mode
 * `.max` : the maximum value that is set by the user, when in `MINMAX` mode
 * `.step` : the step value that is set by the user, when in `MINMAX` mode
 * `.currentValue` : the value for the current calculation step, among all values of a variating parameter; it is set and returned by iteration methods; initially equal to `.singleValue` for fixed parameters
 * `.v` : the sandbox value used during calculation; initialized by `CalcSerie()` before calculation, with `.singleValue` for parameters in `SINGLE` mode or with `.currentValue` for variating parameters
 * `.V` : a convenience accessor that returns the the calculation result if the parameter is in `CALCUL` mode, or its `.currentValue` otherwise

 Additionally `getInferredValuesList()`method allows to retrieve multiple values as a numbers list, whether in `LISTE` or `MINMAX` mode.

#### families

Parameters are grouped into **families**, indicating that they may be linked to other parameters or results of the same family. Available families are listed in `ParamFamily` enum. A special family `ParamFamily.ANY` indicates that the parameter may be linked to any other parameter or result, whether it has a family or not.

### Properties

A **property** is an attribute of a Nub that is not numerical and/or accepts only values from a limited set. It is thus distinct from a **parameter**. Usually they are bound to enums, through `Session.enumFromProperty`. Example: "trigoOperation" in `Trigo` that only accepts items from `TrigoOperation` enum.

Properties are stored in `this._props_` and accessed by client code through `getPropValue`/`setPropValue` methods. Not all Nubs have properties. Those who do should set a default value for each property in their constructor, through a call to `this.setPropValue()`.

Properties are **observable** objects: subscribing to a Nub's properties through `addObserver()` method guarantees to be informed of a property value change through the `update()` method of the `Observer` interface.

### Results

A `Result` is an object that is produced by `CalcSerie()`, `Calc()`, `Equation()` and `Solve()` methods, and represents the result of a calculation.

It contains one or more `ResultElement` objects and a global log, essentially useful for Nubs having children or when parameters are varying.

When calculation fails "globally" before any calculation step is attempted (for ex. when input parameters are in a state that does not allow calculation), the result may have zero result element, and only a global log containing one or more messages describing the issue.

Otherwise, when no parameter is varying there is exactly one result element, and when one or more parameter is varying there are as many result elements as variation steps.

#### result elements

A `ResultElement` is an object that contains a main calculated value `vCalc`, optional additional calculated values stored as key-value pairs in `values` (often referred to as "extra results"), and a calculation log concerning this calculation step only.

The result element state (returned by `ok` getter) is true if it has a value and no error-level message.

#### families

Results families allow results to be linked to parameters of the same **family**. They must be defined in the Nub's class by overloading `setResultsFamilies()`.

### Log

A **log** is a collection of `Message` objects used to inform about events during the calculation process.

Every result element has a log, and the `Result` object has an additional `globalLog`.

#### messages

A **message** is an object containing a `MessageCode` that describes the issue, along with optional key-value pairs stored in `extraVars`. The message severity `INFO`, `WARNING` or `ERROR` is inferred from the message code string prefix.

### Links

Links can be defined between two parameters, or between a parameter and a result. The **source** parameter, when read, will return the current value of the **target** parameter or result. Links can be chained.

Only parameters and results of the same **family** may be linked to one another.

#### chain calculation

When a parameter is targetting another Nub's result and this result is not calculated yet, `CalcSerie()` takes care of calculating it on-the-fly so that it is transparent for the user.

In such a case, when an input parameter of the targetted nub is altered and the calculated result is reset, all parameters in other Nubs pointing to this result have their value reset too.

### Session

The `Session` object is a singleton designed to hold a collection of Nubs related to each other in a work session context. One can register Nubs in the session or create them directly in the session.

Having a top view on the work context, the session is able to produce the list of all values linkable to a given parameter and the list of all Nubs depending on a given one, among other things.

#### serialisation

A session can be serialized to a JSON file using `serialise()`, and loaded from a JSON file using `unserialise()`. The serialisation process uses the `objectRepresentation()` method of `Nub` class.

#### settings

`SessionSettings` is a static class that holds some session-related settings. Those settings are saved and loaded to/from the serialized JSON files.

## How to add a calculation module

In this example we'll see how to add a very simple dummy **Addition** module, that solves the equation **Y = A + B**.

### Write module classes

Create two new files named `addition.ts` and `addition_params.ts` in `src` (or a subdirectory).

#### parameters class

In `addition_params.ts`, create and export a new class named `AdditionParams`, that holds the required parameters A, B and Y:
```typescript
import { ParamDefinition, ParamFamily } from "../param/param-definition";
import { ParamDomainValue } from "../param/param-domain";
import { ParamsEquation } from "../param/params-equation";

/** Y = A + B */
export class AdditionParams extends ParamsEquation {

    private _A: ParamDefinition;
    private _B: ParamDefinition;
    private _Y: ParamDefinition;

    constructor(rA: number, rB: number, rY: number) {
        super();
        this._A = new ParamDefinition(this, "A", ParamDomainValue.ANY, "", rA);
        this._B = new ParamDefinition(this, "B", ParamDomainValue.ANY, "", rB);
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, "", rY, ParamFamily.ANY);
        this.addParamDefinition(this._A);
        this.addParamDefinition(this._B);
        this.addParamDefinition(this._Y);
    }

    get A() {
        return this._A;
    }
    get B() {
        return this._B;
    }
    get Y() {
        return this._Y;
    }
}

```

Here we defined A, B and Y to be unitless numbers (4th parameter of `new ParamDefinition()` is an empty string), allowed to take any value (`ParamDomainValue.ANY`). A and B are not linkable (they have no family) but Y is linkable to anything (`ParamFamily.ANY`).

Note the **mandatory** call to `this.addParamDefinition()` and getter method for every parameter.

#### Nub class

In `addition.ts`, create and export a new class named `Addition` that extends Nub class:
```typescript
import { CalculatorType } from "../compute-node";
import { Nub } from "../nub";
import { ParamCalculability } from "../param/param-definition";
import { Message, MessageCode } from "../util/message";
import { Result } from "../util/result";
import { AdditionParams } from "./addition_params";

export class Addition extends Nub {

    constructor(prms: AdditionParams, dbg: boolean = false) {
      super(prms, dbg);
      this._defaultCalculatedParam = prms.Y;
      this.resetDefaultCalculatedParam();
    }

    public get prms(): AdditionParams {
      return this._prms as AdditionParams;
    }

    public Equation(sVarCalc: string): Result {
      let v: number;
      switch (sVarCalc) {
        case "A":
          v = this.prms.Y.v - this.prms.B.v;
          break;
        case "B":
          v = this.prms.Y.v - this.prms.A.v;
          break;
        case "Y":
          if (this.prms.A.v === 0 && this.prms.B.v === 0) {
             return new Result(new Message(MessageCode.ERROR_EASTER_EGG));
          }
          v = this.prms.A.v + this.prms.B.v;
          break;
        default:
          throw new Error("Addition.Equation() : invalid variable name " + sVarCalc);
      }
      return new Result(v, this);
    }

    protected setParametersCalculability() {
      this.prms.A.calculability = ParamCalculability.EQUATION;
      this.prms.B.calculability = ParamCalculability.EQUATION;
      this.prms.Y.calculability = ParamCalculability.EQUATION;
    }
}
```

Here we set the 3 parameters to be calculated analytically, and write `Equation()` accordingly. To show a glimpse of how log works, we added an easter egg that triggers an error when trying to calculate 0 + 0 (it is assumed that `ERROR_EASTER_EGG` item was added to `MessageCode` enum in `message.ts`).

Note the getter on `prms()`, for the sake of correct typing.

### Register module

Add an entry to `CalculatorType` enum in `compute-node.ts`:
```typescript
export enum CalculatorType {
  ConduiteDistributrice,
  …
  ConcentrationBlocs,
  Addition
}
```

Add a `case` entry to `Session.createNub()` in `session.ts`, with default parameter values:
```typescript
case CalculatorType.Addition:
  nub = new Addition(
    new AdditionParams(
        2,    // A
        0.5,  // B
        2.5   // Y
    ), dbg
  );
  break;
```

In `addition.ts`, add the calculator type to the constructor of class `Addition`:
```typescript
constructor(prms: AdditionParams, dbg: boolean = false) {
  super(prms, dbg);
  this._calcType = CalculatorType.Addition;
  …
}
```

### Unit tests
Create a new file named `addition.spec.ts` in `spec` (or a subdirectory), with the following contents.

```typescript
import { Addition } from "../src/addition";
import { AdditionParams } from "../src/addition_params";
import { MessageCode } from "../util/message";

describe("Class Addition: ", () => {

  let nub: Addition;

  beforeEach(() => {
    const prms = new AdditionParams(1, 2, 5,);
    nub = new Addition(prms);
  }

  it("A + B should equal 3", () => {
    nub.calculatedParam = nub.prms.Y;
    expect(nub.CalcSerie().vCalc).toBe(3);
  });

  it("Y - B should equal 3", () => {
    nub.calculatedParam = nub.prms.A;
    expect(nub.CalcSerie().vCalc).toBe(3);
  });

  it("Y - A should equal 4", () => {
    nub.calculatedParam = nub.prms.B;
    expect(nub.CalcSerie().vCalc).toBe(4);
  });

  it ("0 + 0 should give an error log", () => {
    nub.calculatedParam = nub.prms.Y;
    nub.prms.A.singleValue = 0;
    nub.prms.B.singleValue = 0;
    const res = nub.CalcSerie();
    expect(res.log.messages.length).toBe(1);
    expect(res.log.messages[0].code).toBe(MessageCode.ERROR_EASTER_EGG);
  });

});
```

#### fuzzy tests

Fuzzy tests are automatically conducted on all Nub types, unless it is explicitly stated by adding a CalculatorType to `nubsNotTested` array in `spec/fuzzing.spec.ts`.

For proper fuzzy testing on Nubs containing properties driven by an Enum, see "Further considerations for adding more complex modules" / "properties" below.

### Expose classes

Add exports in `index.ts` for the new classes:
```typescript
export * from "./addition";
export * from "./addition_params";
```

Also add exports in `internal_modules.ts` for the new classes, as index.ts should not be used in JalHyd to avoid the "TypeError: Class extends value undefined is not a constructor or null" error (internal module pattern).

## Further considerations for adding more complex modules

### properties

If the module uses **properties**, one should add a default value in the Nub's constructor for each property (see "Properties" above). The Nub should then register itself as **observer** of properties values changes. Example from `Trigo`:

```typescript
constructor(prms: TrigoParams, dbg: boolean = false) {
  …
  this._props.addObserver(this);
  this.setPropValue("trigoOperation", TrigoOperation.COS);
}
```

Note that value should be set after registering the observer, so that the changes observation mechanism is properly triggered.

The changes observation mechanism should be added by implementing `Observer` interface, and creating an `update()` method as in the following example:

```typescript
public update(sender: any, data: any) {
	if (data.action === "propertyChange" && data.name === "trigoOperation") {
	    // do something
	}
}
```

Update property-enum associations list in `src/props.ts` (example for operation property in Trigo module)

```typescript
public static enumFromProperty = {
  …
  trigoOperation: TrigoOperation
};
```

Finally, for all the property values to be properly tested by fuzzy unit tests, one has to add a `case` in `CreateTestNub()` method, as well as a new method, in `spec/fuzzing.spec.ts`:

```typescript
function CreateTestNub(iCalType: number): Nub {
  …
  if (iCalType === CalculatorType.Trigo) {
    setRandomTrigoOperation(n as Trigo);
  }
  …
}

function setRandomTrigoOperation(sn: Trigo) {
    const op = Math.floor(Math.random() * 6); // enum has 6 values
    sn.properties.setPropValue("trigoOperation", op);
}
```

### Nubs with children

If a module has children, the `intlType` attribute should be set in the Nub's constructor. This attribute is used for translation in GUIs, notably ngHyd. Example from `MacroRugoCompound`:

```typescript
constructor(prms: MacrorugoCompoundParams, dbg: boolean = false) {
  …
  this._intlType = "MacroRugo";
```

When implementing parent's `Equation()` method, children should be calculated using `Calc()` and not `Equation()` or `Solve()`, so that their logs and results are properly attached to them.

```typescript
public Equation(sVarCalc: string): Result {
  …
  for (const child of this.children) {
    child.Calc(sVarCalc);
    …
}
```

Also, to take advantage of typing a getter should be added to the parent's class, to return children with the correct type. Example with `Verificateur`:

```typescript
public get children(): Espece[] {
    return this._children as Espece[];
}
```

## How to set up a new hydraulic structure equation

@TODO

### structure type

### structure class

### admissible laws

### structures factory

### session

## How to write CLI scripts based on JaLHyd

Using `npm run build-node` allows building JaLHyd for execution with Node.js (ie. with "commonjs" modules).

Examples of Javascript and Typescript programs can be found in `/boilerplate`.

## Release policy

Use [semantic versioning](https://semver.org/).

**When releasing a Cassiopee (NgHyd) version along with a JaLHyd version, it's discouraged to execute release steps manually, see "Release Policy / Release Script" in [NgHyd's README.md](https://forgemia.inra.fr/cassiopee/nghyd/blob/master/README.md)**

Before releasing a new stable version, one should complete the following files
 - `CHANGELOG.md`
 - `package.json` (update "version", or use `npm version`)

Every stable version should be tagged with both
 - a tag stating compatibility with NgHyd, of the form `nghyd_X.Y.Z`
 - the `stable` tag

The `stable` tag should be set **after** the NgHyd compatibility tag, so that `git describe` returns `stable` (latest tag). There should be **at least 1s** between the two tag commands, so that date sorting is not confused.

Here are the steps to follow for an example **4.5.0** version
```sh
npm version 4.5.0
# commit changes

git tag -fa nghyd_4.5.0
sleep 1
git tag -fa stable
git push --tags --force
```
