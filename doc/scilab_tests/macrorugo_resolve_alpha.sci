function [res]= resolve_alpha(alpha,CdCh,R,U0,hstar,hp,C,D,Cd,ustar)

if bDbg_RA then
    printf('resolve_alpha(alpha=%f,CdCh=%f,R=%f,U0=%f,hstar=%f,hp=%f,C=%f,D=%f,Cd=%f,ustar=%f)\n',alpha,CdCh,R,U0,hstar,hp,C,D,Cd,ustar)
end
g=9.81;
kappa=0.41;

L=D*(1/C^0.5-1);
beta2=hp.*CdCh./alpha./R;

beta2=(beta2).^0.5;
a1=beta2*(hstar-1)/(cosh(beta2));

c=1;
UhU0=(a1*sinh(beta2)+c)^0.5;
Uh=UhU0*U0;

//choix du modele de turbulence

//switch choixturb
//    
//   case 1       

    L1=min(L,0.15*hp);
    //L1=L;//0.15*hp;  
   
//     case 2
//         
//         L1=L.*hp./(1/0.41*L+hp);
//         
//     case 3
//         L1=0.15*min(hp,D./C./Cd);//;
//     case 4
//         if  L/hp<1
//             L1=(1-L/hp).*L;
//             if L1<0.05*hp && L/hp>0.8;L1=0.05*hp;end
//         else
//             L1=0.41*((1-hp./L))*hp;
//             if L1<0.05*hp && L/hp<1.2;L1=0.05*hp;end
//         end
//end


res=abs(alpha*Uh-L1*ustar);
if bDbg_RA then
    printf('resolve_alpha res=%f\n',res)
end

endfunction
