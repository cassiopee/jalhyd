function macrorugo_resultComp(z_amont, S, long, Q, L, pf, C, Cd0, h, D)
    g = 9.81

    ZF2=z_amont-S*long;
    print_r("ZF2");

    Vdeb=Q/L/pf;
    print_r("Vdeb");


    Vg=Q/L/pf/(1-C^0.5);
    print_r("Vg");
    Fr=Vg./(g.*pf).^0.5;
    print_r("Fr");

    PV=1000*g*Vdeb*S;
    print_r("PV");

    if pf/h<1
        flowcond = 'emergent'
    elseif pf/h<1.1 & pf/h>=1
        flowcond = 'quasi emergent'
    else
        flowcond = 'immerge'
    end
    print_r("flowcond");

    if pf/h>1.1
        q_technique=0.955*(pf/h)^2.282*S^0.466*C^(-0.23)*(9.81*h)^0.5.*h*L;
    else
        if Cd0==2
            q_technique= 0.648*(pf/D)^1.084*S^0.56*C^(-0.456)*(9.81*D)^0.5.*D*L;
            V_technique=3.35*(pf/D)^0.27*S^0.53*(9.81*D)^0.5;
        else
            q_technique=0.815*(pf/D)^1.45*S^0.557*C^(-0.456)*(9.81*D)^0.5.*D*L;
            V_technique=4.54*(pf/D)^0.32*S^0.56*(9.81*D)^0.5;
        end
        if Cd0 > 1.99
            rv=1.5;
        else
            rv=1.1;
        end
        Vmax=Vg.*sqrt(calcfFr(Fr, rv));
        print_r("Vmax");
        print_r("V_technique");
    end
    print_r("q_technique");

    Strickler = Q /(pf*L*pf^(2/3)*S^(1/2));
    print_r("Strickler");

endfunction
