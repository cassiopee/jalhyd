function [res]=find_Q_nat(Q,ks,D,h,Cd0,S,L,pf,C,sigma,bDbg)

    //fonction pour ax=ay
    maxfun=1000;
    maxiter=1000;
    if bDbg_RA then
        maxiter=1
    end
    tolfun=1e-8;
    tolx=1e-8;
    opt = optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'TolFun',tolfun,'TolX',tolx)
    g=9.81;
    kappa=0.41;
    U0=Q./L./pf;

    Fr=U0./(9.81*pf).^0.5;
    Frg=Fr/(1-C^0.5);

    fFr = calcfFr(Frg)

    //Cd=Cd0.*(1+0.4./(pf./D).^2);
    Cd=Cd0.*min(1E9,(1+1./(pf./D).^2));
    //Cd=Cd0.*(0.8-2*C).*(1+0.4./(pf./D).^2).*fFr;
    R=(1-sigma*C);//%.*(1-C.^0.5).^2;
    if(bDbg) then
        printf('*************************************\n')
        print_r('Q')
        print_r("U0")
        print_r("fFr")
        print_r("R")
    end

    if pf/h>1; //fFr=1;

        choixturb=1;
        htilde=h./D;
        hstar=pf./h;
        Rh=h.*(hstar-1);
        ustar=(g.*S.*Rh).^0.5;
        if bDbg then print_r('ustar'); end;

        Cd1=Cd;
        if bDbg then print_r('Cd1'); end;
        CdCh=Cd1.*C.*htilde;
        if bDbg then print_r('CdCh'); end;

        Cf=2./(5.1.*log10(h./ks)+6).^2;
        U0b=(2*g.*S.*R./(Cd1.*C.*h./D+Cf.*R).*h).^0.5;
        if bDbg then print_r('U0b'); end;

        // U0=(2*g.*S.*D.*R./(Cd1.*C)).^0.5;

        //[P]=fminbnd(@(alphai) resolve_alpha(alphai,CdCh,R,U0b,hstar,h,C,D,Cd1,ustar,choixturb),1e-5*h,h,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx));
        //alpha=P(1);
        [alpha fval] = fminsearch(list(resolve_alpha, CdCh,R,U0b,hstar,h,C,D,Cd1,ustar), h/10, opt)
        if bDbg then print_r('alpha'); end;
        beta2=h.*CdCh./alpha./R;
        beta=(beta2).^0.5;
        a1=beta*(hstar-1)/(cosh(beta));
        c=1;

        UhU0=(a1*sinh(beta)+c)^0.5;
        Uh=UhU0*U0b;
        if bDbg then print_r('Uh'); end;

        dhp=1-1/kappa*alpha./h.*Uh./ustar;
        z0hp=(1-dhp).*exp(-1*(kappa*Uh./ustar));

        qsup=ustar./kappa.*h.*((hstar-dhp).*(log((hstar-dhp)./z0hp) - 1)-((1-dhp).*(log((1-dhp)./z0hp) - 1)));
        if bDbg then print_r('qsup'); end;
        //calcul intégrale dans la canopée----
        dzinf=0.01;
        Zinf=(0:dzinf:1);
        Uinf = U0b.*(beta.*Rh./h.*sinh(beta*Zinf)./cosh(beta)+1).^0.5;
        if bDbg then print_r('Uinf(2)'); end;
        if bDbg then print_r('Uinf($)'); end;
        Ub=zeros(Uinf);
        Ub(1:$-1)=Uinf(2:$);
        qinf=sum((Uinf(1:$-1)+Ub(1:$-1))/2*dzinf.*h);
        if bDbg then print_r('qinf'); end;
        qtot=qinf+qsup;

        PI=0;
        delta=1;

        Umoy=qtot./pf;
        res=abs(U0-Umoy);

    else

        Cd2=Cd.*fFr;
        if bDbg then print_r('Cd2'); end;

        hstar=pf/D;
        Re=U0.*pf/1e-6;
        if bDbg then print_r('Re'); end;

        if ks==0
            Cf=0.3164/4.*Re.^(-0.25);
        else
            Cf=2/(5.1*log10(pf/ks)+6)^2;
        end
        if bDbg then print_r('Cf'); end;

        alpha=1-(1.*C).^0.5-1/2*sigma.*C;
        if bDbg then print_r('alpha'); end;
        N= (alpha .*Cf)./(pf./D.*Cd2.*C);
        if bDbg then print_r('N'); end;

        U0a=(2*g.*S.*D.*(R)./(Cd2.*C.*(1+N))).^0.5;
        if bDbg then print_r("U0a");end;
        U0complet = find_U0_complet(U0a, pf,C,D,sigma,Cd,Cf,S, bDbg)
        if bDbg then print_r("U0complet");end;

        [u fval] = fminsearch(list(find_U0_complet, pf,C,D,sigma,Cd,Cf,S), U0a, opt)

        res=abs(U0-u);

    end

endfunction
