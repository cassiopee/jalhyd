function print_r(s)
    e = evstr(s)
    if typeof(e) == "string" then
        pat = "s"
    else
        pat = "f"
    end
    if size(e,1) > 1 | size(e,2) > 1 then
        printf("%s: [",s);
        if size(e,2) > 1 then
            e = e'
        end
        printf("%f, ",e)
        printf("]\n")
    else
        printf("%s: %"+pat+",\n",s,e);
    end
endfunction
