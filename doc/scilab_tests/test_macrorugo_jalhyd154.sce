// Test JalHyd #144

clear
sCurrentPath = get_absolute_file_path("test_macrorugo_jalhyd154.sce");
getd(sCurrentPath);

bDbg = %f;
bDbg_RA = %f;
Q_na = []
for q0 = 0.2:0.05:0.3
    Q_na = [Q_na find_Q_nat(q0,0.1,0.4,0.4,1,0.05,1,0.3,0.13,%pi/4,%t)]
end
printf("%f, ", Q_na')

Q = []
// macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)
for h = 0.34:0.01:0.41
    Q = [Q macrorugo_searchQ(0.1, 0.4, 0.4, 1, 0.05, 1, h, 0.13, 12.5, 6, bDbg)]
end
printf("%f, ", Q')
