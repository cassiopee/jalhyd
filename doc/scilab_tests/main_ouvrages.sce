// Test des équations d'ouvrages pour JaLHyd
clear
sCurrentPath = get_absolute_file_path("main_ouvrages.sce");
getd(sCurrentPath);

function TestQOuvrageSuite(T)
    //TestQOuvrageLoop(sTest, T, L, Cd, H1, h2, W)
    TestQOuvrageLoop("Calcul Q avec W croissant",T, 2, 0.6, 1.2, 1, 0:0.1:1.3);
    TestQOuvrageLoop("Calcul Q en charge avec h1 croissant",T, 2, 0.6, [1.05,1.3,1.5], 1, 0.8);
    TestQOuvrageLoop("Calcul Q a surface libre avec h1 croissant",T, 2, 0.6, [1.1,1.5], 1, 2);
endfunction

describe("CEM88D");
TestQOuvrageSuite(1);

describe("CEM88V");
TestQOuvrageSuite(2);

describe("Classique - Seuil dénoyé");
TestQOuvrageSuite(3);

describe("Classique - Orifice dénoyé");
TestQOuvrageSuite(4);

describe("Classique - Orifice noyé");
TestQOuvrageSuite(5);

describe("Cunge 80");
TestQOuvrageSuite(6);
