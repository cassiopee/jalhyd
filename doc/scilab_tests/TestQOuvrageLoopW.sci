function TestQOuvrageLoop(sTest, T, L, Cd, H1, h2, W)
    printf("\n")
    printf("*** %s ***\n", sTest)
    
    // Tests des différents régimes
    sCodeC = ["debit nul","surface libre denoye","surface libre noye","charge denoye","charge noye partiel","charge noye total"];
    Q = []
    C = []
    CS = []
    for w = W
        for h1 = H1
            [q, c] = Qouvrage(T,h1,h2,w,0,L,Cd)
            Q = [Q q]
            C = [C c]
            CS = [CS sCodeC(c+1)]
        end
    end
    
    printf("W=[")
    printf("%f,",W')
    printf("];\n")
    
    printf("h1=[")
    printf("%f,",H1')
    printf("];\n")

    printf("Q=[")
    printf("%f,",Q')
    printf("];\n")
    
    printf("C=[")
    printf("%i,",C')
    printf("];\n")
    
    printf("Code=[")
    printf("%s,",CS')
    printf("];\n")
    
endfunction
