function [res]=find_U0_complet(U01,h,C,D,sigma,Cd,cfmean,S, bDbgU0)
    if ~exists("bDbgU0","local"); bDbgU0 = %f; end;
g=9.81;
Frexp=U01./(1-(1.*C).^0.5)./(9.81*h)^0.5;

fFr = calcfFr(Frexp)

alpha=1-(1.*C).^0.5-1/2*sigma.*C;

//Cd=Cd0.*(0.8-2*C).*(1+0.4./(h./D).^2).*fFr;
Cd2=Cd.*fFr;

N= (alpha.*cfmean)./(h./D.*Cd2.*C);

if bDbgU0 then
    print_r("Frexp")
    print_r("fFr")
    print_r("Cd")
    print_r("N")
end

res=abs(U01-(2*g.*S.*D.*(1-(sigma*C))./(Cd2.*C.*(1+N))).^0.5);
if bDbgU0 then
    print_r("res")
end

endfunction
