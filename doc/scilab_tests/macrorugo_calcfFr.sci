function [f] = calcfFr(Frg, rQ)
    if ~exists("rQ","local") then
        rQ = 0.4 * Cd0 + 0.7
    end
    
    if abs(Frg) < 1
        f=(min(rQ/(1-(Frg.^2)/4),abs(Frg).^(-2/3))).^2;
    else 
        f = 1
    end
endfunction
