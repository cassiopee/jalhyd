function [Q] = macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)
    printf("*** INPUT *** \n")
    print_r("ks")
    print_r("D")
    printf("k = %f\n", k)
    print_r("Cd0")
    print_r("S")
    print_r("B")
    print_r("h")
    print_r("C")

    maxfun=1000;
    maxiter=1000;
    tolfun=1e-8;
    tolx=1e-8;
    if bDbg then
        maxiter=1
    end
    opt = optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'TolFun',tolfun,'TolX',tolx)
    if Cd0>=2; sigma=1; else sigma=%pi/4; end
    g=9.81
    N=0;
    q0=(2*g.*S.*D.*(1-(sigma*C))/(Cd0.*C.*(1+N))).^0.5*h*B;
    fVal = find_Q_nat(q0,ks,D,k,Cd0,S,B,h,C,sigma,bDbg);
    printf("RESULTS:\n")
    printf("find_Q_nat(%f)=%f\n",q0,fVal);
    [Q fVal, exitflag, outputs] = fminsearch(list(find_Q_nat, ks,D,k,Cd0,S,B,h,C,sigma,bDbg), q0, opt);
    printf("Q=%f  fVal=%f\n",Q, fVal);
    macrorugo_resultComp(z_amont, S, long, Q, B, h, C, Cd0, k, D)
    printf("\n");
endfunction
