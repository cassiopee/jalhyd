// Test des passes à macro rugosité pour JaLHyd
clear
sCurrentPath = get_absolute_file_path("main_macrorugo.sce");
getd(sCurrentPath);

bDbg = %f;
bDbg_RA = %f;

// Tests parameters
ks = 0.01 // Rugosité de fond (m)
S = 0.05 // Pente
C = 0.13; // Concentration
D = 0.5; // Diamètre
B = 1 // Largeur
z_amont = 12.5; // Cote amont (m)
long = 6; // Longueur rampe (m)

// *****************************************************************************
printf("\n*** Emergent conditions Cd=1.***\n")
// *****************************************************************************
h = 0.4
k = 0.6
Cd0 = 1. // Forme ronde
macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)

// *****************************************************************************
printf("\n*** Emergent conditions Cd=2***\n")
// *****************************************************************************
h = 0.4
k = 0.6
Cd0 = 2. // Forme plane
macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)

// *****************************************************************************
printf("\n*** Submerged conditions  Cd=1***\n")
// *****************************************************************************
k = 0.6
h = 0.8
Cd0 = 1. // Forme ronde
macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)

// *****************************************************************************
printf("\n*** JalHyd #85 ***\n")
// *****************************************************************************
Cd0 = 1.
k = 0.8
C = 0.2; // Concentration
Q = []
for h = 0.7:0.1:1.2
    Q = [Q, macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)]
end
print_r("Q")

macrorugo_searchQ(ks, D, 0.5, 2.592, S, 2, 0.85, C, z_amont, long, bDbg)

// *****************************************************************************
printf("\n*** JalHyd #144 ***\n")
// rZF1, rL, rB,  rIf, rQ,   rY,  rRF,  rCB, rPBD, rPBH, rCd0
//  100, 25,  5, 0.05,  1, 0.05, 0.15, 0.13,  0.5,  0.4,  1.1
// *****************************************************************************
// macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)
//macrorugo_searchQ(0.15, 0.5, 0.4, 1.1, 0.05, 5, 0.05, 0.13, z_amont, long, bDbg)

// *****************************************************************************
printf("\n*** JalHyd #154 ***\n")
// *****************************************************************************
//Q = []
//for h = 0.34:0.01:0.41
//    Q = [Q, macrorugo_searchQ(0.1, 0.4, 0.4, 1, S, 1, h, 0.13, z_amont, long, bDbg)]
//end
//print_r("Q")
