// Test JalHyd #144

clear
sCurrentPath = get_absolute_file_path("test_macrorugo_jalhyd144.sce");
getd(sCurrentPath);

bDbg = %f;
bDbg_RA = %f;

// macrorugo_searchQ(ks, D, k, Cd0, S, B, h, C, z_amont, long, bDbg)
for h = 0.05
    macrorugo_searchQ(0.15, 0.5, 0.4, 1, 0.05, 5, h, 0.13, 100, 25, bDbg)
end
