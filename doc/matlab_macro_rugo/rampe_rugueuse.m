function varargout = rampe_rugueuse(varargin)
% RAMPE_RUGUEUSE MATLAB code for rampe_rugueuse.fig
%      RAMPE_RUGUEUSE, by itself, creates a new RAMPE_RUGUEUSE or raises the existing
%      singleton*.
%
%      H = RAMPE_RUGUEUSE returns the handle to a new RAMPE_RUGUEUSE or the handle to
%      the existing singleton*.
%
%      RAMPE_RUGUEUSE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RAMPE_RUGUEUSE.M with the given input arguments.
%
%      RAMPE_RUGUEUSE('Property','Value',...) creates a new RAMPE_RUGUEUSE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before rampe_rugueuse_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to rampe_rugueuse_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help rampe_rugueuse

% Last Modified by GUIDE v2.5 06-Jan-2017 09:58:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @rampe_rugueuse_OpeningFcn, ...
    'gui_OutputFcn',  @rampe_rugueuse_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before rampe_rugueuse is made visible.
function rampe_rugueuse_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to rampe_rugueuse (see VARARGIN)

% Choose default command line output for rampe_rugueuse
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%cd('E:\lcassan\Mes documents\macro_rugosite\cassiopee\gui_matlab')
%cd('D:\macrorugosite\cassiopee\gui_matlab')

if exist('data_precalcul_nat.mat')==2

load data_precalcul_nat






set(handles.edit1,'string',z_amont);
set(handles.edit17,'string',long);
set(handles.edit2,'string',ks);
set(handles.edit3,'string',D);
set(handles.edit4,'string',h);
set(handles.edit5,'string',Cd);
set(handles.edit6,'string',S);
set(handles.edit7,'string',L);
set(handles.edit8,'string',pf);
set(handles.edit9,'string',C);
set(handles.edit10,'string',Q);
set(handles.edit20,'string',paramin);
set(handles.edit21,'string',parapas);
set(handles.edit22,'string',paramax);

end



% UIWAIT makes rampe_rugueuse wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = rampe_rugueuse_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double



% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double



% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double



% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end








% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


maxfun=1000;
maxiter=1000;
tolfun=1e-8;
tolx=1e-8;


z_amont=get(handles.edit1,'string');
z_amont=str2num(z_amont);

long=get(handles.edit17,'string');
long=str2num(long);

%setappdata(gcf,'z_amont',z_amont);
ks=get(handles.edit2,'string');
ks=str2num(ks);
%setappdata(gcf,'ks',ks);
D=get(handles.edit3,'string');
D=str2num(D);
%setappdata(gcf,'D',D);
h=get(handles.edit4,'string');
h=str2num(h);
%setappdata(gcf,'h',h);
Cd=get(handles.edit5,'string');
Cd=str2num(Cd);
%setappdata(gcf,'Cd',Cd);
S=get(handles.edit6,'string');
S=str2num(S);
%setappdata(gcf,'S',S);
L=get(handles.edit7,'string');
L=str2num(L);
%setappdata(gcf,'L',L);
pf=get(handles.edit8,'string');
pf=str2num(pf);
%setappdata(gcf,'pf',pf);
C=get(handles.edit9,'string');
C=str2num(C);
%setappdata(gcf,'C',C);
Q=get(handles.edit10,'string');
Q=str2num(Q);
%setappdata(gcf,'Q',Q);


paramin=str2num(get(handles.edit20,'string'));
parapas=str2num(get(handles.edit21,'string'));
paramax=str2num(get(handles.edit22,'string'));
para=(paramin:parapas:paramax);

save data_precalcul_nat z_amont long ks D h Cd S L pf C Q paramin parapas paramax

% %
%  z_amont=getappdata(gcf,'z_amont');
%  ks=getappdata(gcf,'ks');
%  D=getappdata(gcf,'D');
%  h=getappdata(gcf,'h');
%  Cd=getappdata(gcf,'Cd');
%  S=getappdata(gcf,'S');
%  L=getappdata(gcf,'L');
%  pf=getappdata(gcf,'pf');
%  C=getappdata(gcf,'C');
%  Q=getappdata(gcf,'Q');

f=1;



if Cd==2 ;sigma=1;else sigma=pi/4; end
alpha=1-(f.*C).^0.5-1/2*sigma.*C;
g=9.81;

if Q~=0  & pf~=0 & L~=0 & S~=0 & C~=0
    warning_4a1_1
end


if Q==0
    N=0;
    q0=(2*g.*S.*D.*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5*pf*L;

    [Q res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,D,h,Cd,S,L,pf,C,Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx))
    set(handles.edit10,'string',num2str(Q))

elseif pf==0
    [pf res]=fminsearch(@(pfi) find_Q_nat(z_amont,ks,D,h,Cd,S,L,pfi,C,Q,sigma),h*1.1,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx))
    set(handles.edit8,'string',num2str(pf))

elseif L==0
    [L res]=fminsearch(@(Li) find_Q_nat(z_amont,ks,D,h,Cd,S,Li,pf,C,Q,sigma),10,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx))
    set(handles.edit7,'string',num2str(L))

elseif S==0
    S0=0.1;
    [S res]=fminsearch(@(Si) find_Q_nat(z_amont,ks,D,h,Cd,Si,L,pf,C,Q,sigma),S0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx))
    set(handles.edit6,'string',num2str(S))

elseif C==0
    C0=0.1;
    [C res]=fminsearch(@(Ci) find_Q_nat(z_amont,ks,D,h,Cd,S,L,pf,Ci,Q,sigma),C0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx))
    set(handles.edit9,'string',num2str(C))

end




cote_bas=z_amont-S*long;
set(handles.edit11,'string',num2str(cote_bas))

Vdeb=Q/L/pf;
set(handles.edit12,'string',num2str(Vdeb))
Vg=Q/L/pf/(1-C^0.5);
Fr=Vg./(g.*pf).^0.5;
coeff_contraction=0.2*Cd+0.9;
Vmax=Vg.*(min(coeff_contraction./(1-(Fr.^2)/4),Fr.^(-2/3)));

set(handles.edit16,'string',num2str(Vmax))

P=1000*g*Vdeb*S;
set(handles.edit13,'string',num2str(P))


set(handles.edit14,'string',num2str(Fr))

if pf/h<1
    set(handles.edit15,'string','emergent')
elseif pf/h<1.1 & pf/h>=1
    set(handles.edit15,'string','quasi emergent')
elseif pf/h>1.2
    set(handles.edit15,'string','immerge')
end

%
%
% if pf/h>1.1
%
%     q_technique=0.955*(pf/h)^2.282*S^0.466*C^(-0.23)*(9.81*h)^0.5.*h*L;
%        set(handles.edit19,'string','NC')
%        set(handles.edit16,'string','NC')
% else
%     if Cd==2
%
%         q_technique= 0.648*(pf/D)^1.084*S^0.56*C^(-0.456)*(9.81*D)^0.5.*D*L;
%         V_technique=3.35*(pf/D)^0.27*S^0.53*(9.81*D)^0.5;
%     else
%         q_technique=0.815*(pf/D)^1.45*S^0.557*C^(-0.456)*(9.81*D)^0.5.*D*L;
%         V_technique=4.54*(pf/D)^0.32*S^0.56*(9.81*D)^0.5;
%
%     end
%
%  set(handles.edit19,'string',num2str(V_technique)) ;
% end


Strickler = Q /(pf*L*pf^(2/3)*S^(1/2));


set(handles.edit19,'string',num2str(Strickler)) ;



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edi5_Callback(hObject, eventdata, handles)
% hObject    handle to edi5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edi5 as text
%        str2double(get(hObject,'String')) returns contents of edi5 as a double


% --- Executes during object creation, after setting all properties.
function edi5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edi5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



maxfun=1000;
maxiter=1000;
tolfun=1e-12;
tolx=1e-12;


z_amont=get(handles.edit1,'string');
z_amont=str2num(z_amont);

long=get(handles.edit17,'string');
long=str2num(long);

%setappdata(gcf,'z_amont',z_amont);
ks=get(handles.edit2,'string');
ks=str2num(ks);
%setappdata(gcf,'ks',ks);
D=get(handles.edit3,'string');
D=str2num(D);
%setappdata(gcf,'D',D);
h=get(handles.edit4,'string');
h=str2num(h);
%setappdata(gcf,'h',h);
Cd=get(handles.edit5,'string');
Cd=str2num(Cd);
%setappdata(gcf,'Cd',Cd);
S=get(handles.edit6,'string');
S=str2num(S);
%setappdata(gcf,'S',S);
L=get(handles.edit7,'string');
L=str2num(L);
%setappdata(gcf,'L',L);
pf=get(handles.edit8,'string');
pf=str2num(pf);
%setappdata(gcf,'pf',pf);
C=get(handles.edit9,'string');
C=str2num(C);
%setappdata(gcf,'C',C);
Q=get(handles.edit10,'string');
Q=str2num(Q);
%setappdata(gcf,'Q',Q);


paramin=str2num(get(handles.edit20,'string'));
parapas=str2num(get(handles.edit21,'string'));
paramax=str2num(get(handles.edit22,'string'));
para=(paramin:parapas:paramax);

save data_precalcul_nat z_amont long ks D h Cd S L pf C Q paramin parapas paramax


%
% z_amont=getappdata(gcf,'z_amont');
% ks=getappdata(gcf,'ks');
% D=getappdata(gcf,'D');
% h=getappdata(gcf,'h');
% Cd=getappdata(gcf,'Cd');
% S=getappdata(gcf,'S');
% L=getappdata(gcf,'L');
% pf=getappdata(gcf,'pf');
% C=getappdata(gcf,'C');
% Q=getappdata(gcf,'Q');

f=1;



if Cd==2 ;sigma=1;else sigma=pi/4; end
alpha=1-(f.*C).^0.5-1/2*sigma.*C;
g=9.81;

if Q~=0  & pf~=0 & L~=0 & S~=0 & C~=0
    warning_4a1_1
end

for tt=1:length(para)

    if D==0

        N=0;
        q0=(2*g.*S.*para(tt).*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5*pf*L;
        [Qr(tt) res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,para(tt),h,Cd,S,L,pf,C,Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx)) ;

        Vdeb(tt)=Qr(tt)/L/pf;
        Vg(tt)=Qr(tt)/L/pf/(1-C^0.5);
        Fr(tt)=Vg(tt)./(g.*pf).^0.5;
            Strickler(tt) = Qr(tt) /(pf*L*(pf)^(2/3)*S^(1/2));

    elseif Q==0

        N=0;


        [pf2(tt) res]=fminsearch(@(pfi) find_Q_nat(z_amont,ks,D,h,Cd,S,L,pfi,C,para(tt),sigma),pf,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx));

        Vdeb(tt)=para(tt)/L/pf2(tt);
        Vg(tt)=para(tt)/L/pf2(tt)/(1-C^0.5);
        Fr(tt)=Vg(tt)./(g.*pf2(tt)).^0.5;
         Strickler(tt) = para(tt) /(pf2(tt)*L*pf2(tt)^(2/3)*S^(1/2));

    elseif pf==0

        N=0;
        q0=(2*g.*S.*D.*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5*para(tt)*L;

        [Qr(tt) res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,D,h,Cd,S,L,para(tt),C,Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx));

        Vdeb(tt)=Qr(tt)/L/para(tt);
        Vg(tt)=Qr(tt)/L/para(tt)/(1-C^0.5);
        Fr(tt)=Vg(tt)./(g.*para(tt)).^0.5;
           Strickler(tt) = Qr(tt) /(para(tt)*L*para(tt)^(2/3)*S^(1/2));

    elseif L==0

        N=0;
        q0=(2*g.*S.*D.*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5*pf*para(tt);
        [Qr(tt) res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,D,h,Cd,S,para(tt),pf,C,Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx));

        Vdeb(tt)=Qr(tt)/para(tt)/pf;
        Vg(tt)=Qr(tt)/para(tt)/pf/(1-C^0.5);
        Fr(tt)=Vg(tt)./(g.*pf).^0.5;
           Strickler(tt) = Qr(tt) /(pf*para(tt)*pf^(2/3)*S^(1/2));

    elseif S==0
        S0=0.1;

        N=0;
        q0=(2*g.*para(tt).*D.*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5*pf*L;
        [Qr(tt) res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,D,h,Cd,para(tt),L,pf,C,Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx));

        Vdeb(tt)=Qr(tt)/L/pf;
        Vg(tt)=Qr(tt)/L/pf/(1-C^0.5);
        Fr(tt)=Vg(tt)./(g.*pf).^0.5;
        Strickler(tt) = Qr(tt) /(pf*L*pf^(2/3)*para(tt)^(1/2));


    elseif C==0

        N=0;
        q0=(2*g.*S.*D.*(1-(sigma*para(tt)))./(Cd.*para(tt).*(1+N))).^0.5*pf*L;
        [Qr(tt) res]=fminsearch(@(Qi) find_Q_nat(z_amont,ks,D,h,Cd,S,L,pf,para(tt),Qi,sigma),q0,optimset('MaxIter',maxiter,'MaxFunEvals',maxfun,'Tolfun',tolfun,'TolX',tolx)) ;

        Vdeb(tt)=Qr(tt)/L/pf;
        Vg(tt)=Qr(tt)/L/pf/(1-para(tt)^0.5);
        Fr(tt)=Vg(tt)./(g.*pf).^0.5;
           Strickler(tt) = Qr(tt) /(pf*L*(pf)^(2/3)*S^(1/2));

    end


    if Cd==2
        Vmax(tt)=Vg(tt).*(min(1.6./(1-(Fr(tt).^2)/4),Fr(tt).^(-2/3)));%
    else
        Vmax(tt)=Vg(tt).*(min(1.2./(1-(Fr(tt).^2)/4),Fr(tt).^(-2/3)));
    end




end



figure(1)
subplot(3,1,1)
hold on
plot(para,Vg,'k','LineWidth',2)
plot(para,Vmax,'k:','LineWidth',2)
plot(para,Vdeb,'k-.','LineWidth',2)

if D==0
    xlabel('Diametre (m)','Interpreter','Latex','FontSize',24)
elseif Q==0
    xlabel('D�bit (m2/s)','Interpreter','Latex','FontSize',24)
elseif pf==0
    xlabel('Profondeur','Interpreter','Latex','FontSize',24)
elseif L==0
    xlabel('Largeur','Interpreter','Latex','FontSize',24)
elseif S==0
    xlabel('Pente','Interpreter','Latex','FontSize',24)

elseif C==0
    xlabel('Concentration','Interpreter','Latex','FontSize',24)
end

ylabel('Vitesse (m/s)','Interpreter','Latex','FontSize',18)
hleng=legend('Vg','V_{max}','V_{debitant}');
set(hleng, 'FontName','Times New Roman','FontSize',14)
set(gca, 'FontSize', 14, 'fontName','Times');

%
figure(1)
subplot(3,1,2)
hold on
if Q==0
    plot(para,pf2,'k','LineWidth',2)
else
    plot(para,Qr,'k','LineWidth',2)
end
if D==0
    xlabel('Diametre (m)','Interpreter','Latex','FontSize',24)
elseif Q==0
    xlabel('D�bit (m3/s)','Interpreter','Latex','FontSize',24)
elseif pf==0
    xlabel('profondeur','Interpreter','Latex','FontSize',24)
elseif L==0
    xlabel('Largeur','Interpreter','Latex','FontSize',24)
elseif S==0
    xlabel('Pente','Interpreter','Latex','FontSize',24)

elseif C==0
    xlabel('Concentration','Interpreter','Latex','FontSize',24)
end


if Q==0
    ylabel('Profondeur (m)','Interpreter','Latex','FontSize',24)
else
    ylabel('D�bit (m3/s)','Interpreter','Latex','FontSize',24)
end

%hleng=legend('D�bit (m^2/s)');
%set(hleng, 'FontName','Times New Roman','FontSize',20)
set(gca, 'FontSize', 14, 'fontName','Times');

figure(1)
subplot(3,1,3)
hold on
if Q==0
    plot(para,Strickler,'k','LineWidth',2)
else
    plot(para,Strickler,'k','LineWidth',2)
end
if D==0
    xlabel('Diametre (m)','Interpreter','Latex','FontSize',24)
elseif Q==0
    xlabel('D�bit (m3/s)','Interpreter','Latex','FontSize',24)
elseif pf==0
    xlabel('profondeur','Interpreter','Latex','FontSize',24)
elseif L==0
    xlabel('Largeur','Interpreter','Latex','FontSize',24)
elseif S==0
    xlabel('Pente','Interpreter','Latex','FontSize',24)

elseif C==0
    xlabel('Concentration','Interpreter','Latex','FontSize',24)
end


    ylabel('Strcikler','Interpreter','Latex','FontSize',24)


%hleng=legend('D�bit (m^2/s)');
%set(hleng, 'FontName','Times New Roman','FontSize',20)
set(gca, 'FontSize', 14, 'fontName','Times');


function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
