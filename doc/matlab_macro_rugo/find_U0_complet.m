function [res]=find_U0_complet(U01,h,C,D,sigma,Cd0,cfmean,S)
g=9.81;
Frexp=U01./(1-(1.*C).^0.5)./(9.81*h)^0.5;
%Frexp=U01./(9.81*h)^0.5;

    fFr=(min(1./(1-(Frexp.^2)/4),Frexp.^(-2/3))).^2;



if Frexp>=1.5
fFr=(Frexp.^(-2/3)).^2;
end

alpha=1-(1.*C).^0.5-1/2*sigma.*C;

%Cd=Cd0.*(0.8-2*C).*(1+0.4./(h./D).^2).*fFr;
Cd=Cd0.*(1+1./(h./D).^2).*fFr;
 N= (alpha.*cfmean)./(h./D.*Cd.*C);

hstar=h/D;

res=abs(U01-(2*g.*S.*D.*(1-(sigma*C))./(Cd.*C.*(1+N))).^0.5);
%res=abs(U01-(1-C^0.5)*(2*g.*S.*D*(1-sigma.*C)./(Cd.*C.*(1+N))).^0.5);

