/**
 * JaLHyd must be compiled with "es2016" target and "commonjs" modules
 * @see `npm run build-node` and `src/tsconfig.app.node.json`
 */
import {
    CalculatorType,
    formattedValue,
    LoiDebit,
    Props,
    Session,
    Cloisons,
    PabChute,
    PabNombre,
    PabPuissance,
    PabDimension,
    RectangularStructure
} from "jalhyd";

// ---- example of modules setup and calculation : fish ladder ----

const pabChute = Session.getInstance().createSessionNub(
    new Props({ calcType: CalculatorType.PabChute })
) as PabChute;
pabChute.prms.Z1.singleValue = 29.99;
pabChute.prms.Z2.singleValue = 26.81;
pabChute.calculatedParam = pabChute.prms.DH;

const pabNombre = Session.getInstance().createSessionNub(
    new Props({ calcType: CalculatorType.PabNombre })
) as PabNombre;
pabNombre.prms.N.singleValue = 14;
pabNombre.calculatedParam = pabNombre.prms.DH;
pabNombre.prms.DHT.defineReference(pabChute, "DH");

const pabPuissance = Session.getInstance().createSessionNub(
    new Props({ calcType: CalculatorType.PabPuissance })
) as PabPuissance;
pabPuissance.prms.Q.singleValue = 1.8;
pabPuissance.prms.PV.singleValue = 140;
pabPuissance.calculatedParam = pabPuissance.prms.V;
pabPuissance.prms.DH.defineReference(pabNombre, "DH");

const pabDimension = Session.getInstance().createSessionNub(
    new Props({ calcType: CalculatorType.PabDimensions })
) as PabDimension;
pabDimension.prms.L.singleValue = 5;
pabDimension.prms.W.singleValue = 3.6;
pabDimension.calculatedParam = pabDimension.prms.Y;
pabDimension.prms.V.defineReference(pabPuissance, "V");

const resDim = pabDimension.CalcSerie();
console.log("Water level in PAB:Dimension :", formattedValue(resDim.vCalc, 3));

const cloisons = Session.getInstance().createSessionNub(
    new Props({ calcType: CalculatorType.Cloisons })
) as Cloisons;
const struct = Session.getInstance().createNub(
    new Props({ calcType: CalculatorType.Structure, loiDebit: LoiDebit.WeirSubmergedLarinier })
) as RectangularStructure;
struct.prms.L.singleValue = 0.5;
struct.prms.CdWSL.singleValue = 0.83;
cloisons.addChild(struct);
cloisons.calculatedParam = cloisons.structures[0].prms.h1;
cloisons.prms.Z1.singleValue = 30.14;
cloisons.prms.LB.singleValue = 4.5;
cloisons.prms.PB.singleValue = 2.5;
cloisons.prms.Q.defineReference(pabPuissance, "Q");
cloisons.prms.BB.defineReference(pabDimension, "W");
cloisons.prms.DH.defineReference(pabNombre, "DH");

const resC = cloisons.CalcSerie();
console.log("Head in Cloisons :", formattedValue(resC.vCalc, 3));
