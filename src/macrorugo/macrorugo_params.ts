import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class MacrorugoParams extends ParamsEquation {

    /** Cote de fond amont (m) */
    private _ZF1: ParamDefinition;

    /** Longueur (m) */
    private _L: ParamDefinition;

    /** Largeur (m) */
    private _B: ParamDefinition;

    /** Pente (m/m) */
    private _If: ParamDefinition;

    /** Débit (m3/s) */
    private _Q: ParamDefinition;

    /** Tirant d'eau (m) */
    private _Y: ParamDefinition;

    /** Rugosité de fond (m) */
    private _Ks: ParamDefinition;

    /** Concentration de blocs (-) */
    private _C: ParamDefinition;

    /** Paramètre de bloc : Diamètre (m) */
    private _PBD: ParamDefinition;

    /** Paramètre de bloc : Hauteur (m) */
    private _PBH: ParamDefinition;

    /** Paramètre de bloc : Forme (1 pour rond, 2 pour carré) */
    private _Cd0: ParamDefinition;

    /**
     *
     * @param rZF1 Cote de fond amont (m)
     * @param rL Longueur (m)
     * @param rB Largeur (m)
     * @param rIf Pente (m/m)
     * @param rQ Débit (m3/s)
     * @param rY Tirant d'eau (m)
     * @param rRF Rugosité de fond (m)
     * @param rCB Concentration de blocs (m)
     * @param rPBD Paramètre de bloc : Diamètre (m)
     * @param rPBH Paramètre de bloc : Hauteur (m)
     * @param rCd0 Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     */
    constructor(rZF1: number, rL: number, rB: number, rIf: number, rQ: number,
        rY: number, rRF: number, rCB: number, rPBD: number, rPBH: number, rCd0: number, nullParams: boolean = false) {
        super();

        this._ZF1 = new ParamDefinition(this, "ZF1", ParamDomainValue.ANY, "m", rZF1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZF1);

        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.LENGTHS, undefined, nullParams);
        this.addParamDefinition(this._L);

        this._B = new ParamDefinition(this, "B", ParamDomainValue.POS, "m", rB, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this._B);

        this._If = new ParamDefinition(this, "If",
            new ParamDomain(ParamDomainValue.INTERVAL, 0, 0.5), "m/m", rIf, ParamFamily.SLOPES, undefined, nullParams);
        this.addParamDefinition(this._If);

        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.addParamDefinition(this._Q);

        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.POS_NULL, "m", rY, ParamFamily.HEIGHTS, undefined, nullParams);
        this.addParamDefinition(this._Y);

        this._Ks = new ParamDefinition(this, "Ks", new ParamDomain(ParamDomainValue.INTERVAL, 0, 1), "m", rRF, ParamFamily.STRICKLERS, undefined, nullParams);
        this.addParamDefinition(this._Ks);

        this._C = new ParamDefinition(this, "C", new ParamDomain(ParamDomainValue.INTERVAL, 0, 1), "", rCB, ParamFamily.BLOCKCONCENTRATION, undefined, nullParams);
        this.addParamDefinition(this._C);

        this._PBD = new ParamDefinition(this, "PBD", new ParamDomain(ParamDomainValue.INTERVAL, 0, 2), "m", rPBD, ParamFamily.DIAMETERS, undefined, nullParams);
        this.addParamDefinition(this._PBD);

        this._PBH = new ParamDefinition(this, "PBH", ParamDomainValue.POS, "m", rPBH, ParamFamily.HEIGHTS, undefined, nullParams);
        this.addParamDefinition(this._PBH);

        this._Cd0 = new ParamDefinition(
            this,
            "Cd0",
            new ParamDomain(ParamDomainValue.INTERVAL, 0.5, 3),
            undefined,
            rCd0,
            undefined, undefined, false
        );
        this.addParamDefinition(this._Cd0);

    }

    /**
     * Cote de fond amont (m)
     * @return {ParamDefinition}
     */
    public get ZF1(): ParamDefinition {
        return this._ZF1;
    }

    /**
     * Longueur (m)
     * @return {ParamDefinition}
     */
    public get L(): ParamDefinition {
        return this._L;
    }

    /**
     * Largeur (m)
     * @return {ParamDefinition}
     */
    public get B(): ParamDefinition {
        return this._B;
    }

    /**
     * Pente (m/m)
     * @return {ParamDefinition}
     */
    public get If(): ParamDefinition {
        return this._If;
    }

    /**
     * Débit (m3/s)
     * @return {ParamDefinition}
     */
    public get Q(): ParamDefinition {
        return this._Q;
    }

    /**
     * Tirant d'eau (m)
     * @return {ParamDefinition}
     */
    public get Y(): ParamDefinition {
        return this._Y;
    }

    /**
     * Rugosité de fond (m)
     * @return {ParamDefinition}
     */
    public get Ks(): ParamDefinition {
        return this._Ks;
    }

    /**
     * Concentration de blocs (-)
     * @return {ParamDefinition}
     */
    public get C(): ParamDefinition {
        return this._C;
    }

    /**
     * Paramètre de bloc : Diamètre (m)
     * @return {ParamDefinition}
     */
    public get PBD(): ParamDefinition {
        return this._PBD;
    }

    /**
     * Paramètre de bloc : Hauteur (m)
     * @return {ParamDefinition}
     */
    public get PBH(): ParamDefinition {
        return this._PBH;
    }

    /**
     * Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     * @return {ParamDefinition}
     */
    public get Cd0(): ParamDefinition {
        return this._Cd0;
    }

}
