import { ParamValueMode, round } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Props } from "../internal_modules";
import { Session } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { MacroRugo } from "../internal_modules";
import { MacrorugoCompoundParams } from "../internal_modules";
import { MRCInclination } from "../internal_modules";

export class MacrorugoCompound extends MacroRugo implements Observer {

    /** enfants castés au bon type */
    get children(): MacroRugo[] {
        return this._children as MacroRugo[];
    }

    /** parameters cast to the right type */
    get prms(): MacrorugoCompoundParams {
        return this._prms as MacrorugoCompoundParams;
    }

    constructor(prms: MacrorugoCompoundParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.MacroRugoCompound);
        this._props.addObserver(this);
        this.inclinedApron = MRCInclination.NOT_INCLINED;
    }

    public get inclinedApron(): MRCInclination {
        return this.getPropValue("inclinedApron");
    }

    public set inclinedApron(i: MRCInclination) {
        this.setPropValue("inclinedApron", i);
    }

    public CalcSerie(rInit?: number): Result {
        if (this.getPropValue("inclinedApron") === MRCInclination.INCLINED) {
            // important to regenerate it here, at every iteration of CalcSerie()
            this.generateInclinedFishway();
        }
        // calculate and store depths once for all
        for (const child of this.children) {
            // do we have a series of values ?
            if (this.prms.Z1.hasMultipleValues || child.prms.ZF1.hasMultipleValues) {
                let valsZ1;
                let valsChildZF1;
                if (this.prms.Z1.hasMultipleValues) {
                    valsZ1 = this.prms.Z1.getInferredValuesList();
                } else {
                    valsZ1 = new Array(child.prms.ZF1.getInferredValuesList().length)
                        .fill(this.prms.Z1.singleValue);
                }
                if (child.prms.ZF1.hasMultipleValues) {
                    valsChildZF1 = child.prms.ZF1.getInferredValuesList();
                } else {
                    valsChildZF1 = new Array(this.prms.Z1.getInferredValuesList().length)
                        .fill(child.prms.ZF1.singleValue);
                }
                // adjust lengths (should never happen when only one of the paramters is varying)
                if (valsZ1.length > valsChildZF1.length) {
                    valsChildZF1 = child.prms.ZF1.getInferredValuesList(valsZ1.length);
                } else if (valsZ1.length < valsChildZF1.length) {
                    valsZ1 = this.prms.Z1.getInferredValuesList(valsChildZF1.length);
                }
                // define calculated values list on child
                const valsY: number[] = [];
                for (let i = 0; i < valsZ1.length; i++) {
                    valsY.push(Math.max(valsZ1[i] - valsChildZF1[i], 0));
                }
                child.prms.Y.setValues(valsY);
            } else {
                // or just a single value ?
                child.prms.Y.setValue(Math.max(this.prms.Z1.singleValue - child.prms.ZF1.singleValue, 0));
            }
        }

        return super.CalcSerie(rInit);
    }

    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string, rInit?: number): Result {
        if (sVarCalc !== "Q") {
            throw new Error("MacrorugoCompound.Calc() : invalid parameter " + sVarCalc);
        }
        this.copyPrmsToChildren();
        this.currentResultElement = this.Equation(sVarCalc);

        // lateral inclination for inclined aprons
        if (this.getPropValue("inclinedApron") === MRCInclination.INCLINED) {
            // extraResult : inclination
            this.result.resultElement.values.LIncl = (this.prms.ZRB.v - this.prms.ZRT.v) / this.prms.BR.v;
            // La largeur de la rampe inclinée est-elle adéquate par rapport à la largeur de motif ax ?
            const ax: number = this.prms.PBD.v / Math.sqrt(this.prms.C.v);
            const tol = 0.01; // tolérance avant avertissement (1 cm)
            if (this.prms.BR.v - ax < -tol) { // BR < ax, with a little tolerance
                const m = new Message(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                m.extraVar.pattern = ax;
                this._result.resultElement.log.add(m);
            } else if (this.prms.BR.v % (ax / 2) > tol && this.prms.BR.v % (ax / 2) < ax / 2 - tol) {
                const m = new Message(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                m.extraVar.halfPattern = ax / 2;
                m.extraVar.lower = Math.floor(this.prms.BR.v / ax * 2) * (ax / 2);
                m.extraVar.higher = Math.ceil(this.prms.BR.v / ax * 2) * (ax / 2);
                this._result.resultElement.log.add(m);
            }
        }
        // Check block concentration bounds
        if (this.prms.C.v < 0.08 || this.prms.C.v > 0.2) {
            this._result.resultElement.log.add(
                new Message(MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS)
            );
        }
        return this._result;
    }

    public Equation(sVarCalc: string): Result {
        let B: number = 0;
        const res = new Result(0);
        for (const child of this.children) {
            child.Calc(sVarCalc);
            if (!child.result.ok) {
                const m = new Message(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                m.extraVar.number = String(child.findPositionInParent() + 1);
                return new Result(m);
            }
            res.vCalc += child.result.vCalc;
            child.result.values.xCenter = B + child.prms.B.v / 2;
            B += child.prms.B.v;
            // la cote de fond aval n'a pas de sens dans la MR-Complexe car on
            // ne précise pas la longueur
            delete child.result.values.ZF2;
        }
        return res;
    }

    public addChild(child: MacroRugo, after?: number) {
        super.addChild(child, after);
        for (const p of child.parameterIterator) {
            p.visible = false;
        }
        child.prms.ZF1.visible = true;
        child.prms.B.visible = true;
        child.prms.B.calculability = ParamCalculability.FREE;
    }

    public addDefaultChild(after?: number) {
        this.addChild(
            Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo,
            after
        );
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (data.action === "propertyChange" && data.name === "inclinedApron") {
            if (data.value === MRCInclination.INCLINED) { // switch to inclined apron mode
                this.prms.ZRB.visible = true;
                this.prms.ZRT.visible = true;
                this.prms.BR.visible = true;
                this.prms.PBD.calculability = ParamCalculability.FIXED;
                // @see jalhyd#372 https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/372
                this.prms.PBD.valueMode = ParamValueMode.SINGLE;
                this.prms.C.calculability = ParamCalculability.FIXED;
                this.prms.C.valueMode = ParamValueMode.SINGLE;
            } else { // switch to multiple aprons mode
                this.prms.ZRB.visible = false;
                this.prms.ZRT.visible = false;
                this.prms.BR.visible = false;
                this.prms.PBD.calculability = ParamCalculability.FREE;
                this.prms.C.calculability = ParamCalculability.FREE;
            }
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.ZF1.calculability = ParamCalculability.FREE;
        this.prms.L.calculability = ParamCalculability.FREE;
        this.prms.Ks.calculability = ParamCalculability.FREE;
        this.prms.B.calculability = ParamCalculability.FREE;
        this.prms.If.calculability = ParamCalculability.FREE;
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.Y.calculability = ParamCalculability.FREE;
        this.prms.C.calculability = ParamCalculability.FREE;
        this.prms.PBD.calculability = ParamCalculability.FREE;
        this.prms.PBH.calculability = ParamCalculability.FREE;
        this.prms.Cd0.calculability = ParamCalculability.FREE;

        this.prms.ZRB.calculability = ParamCalculability.FIXED;
        this.prms.ZRT.calculability = ParamCalculability.FIXED;
        this.prms.BR.calculability = ParamCalculability.FIXED;
    }

    protected exposeResults() {
        // overload MacroRugo.exposeResults() with empty method, just
        // to avoid exposing inherited results that are not calculated here
        this._resultsFamilies = {
            LIncl: undefined
        };
    }

    private copyPrmsToChildren() {
        for (const child of this.children) {
            // Copy common parameters with MacrorugoCompound
            for (const v of ["Ks", "If", "C", "PBD", "PBH", "Cd0"]) {
                child.getParameter(v).v = this.getParameter(v).v;
            }
            // Calculate Length and depth from other parameters
            child.prms.L.v = this.prms.DH.v / this.prms.If.v;
        }
    }

    /**
     * Discrétisation d'un radier incliné en radiers horizontaux
     * Le pas de discrétisationest fixée à D / C^0.5.
     * Les cellules discrétisées sont centrées sur la passe, on crée
     * les cellules manquantes aux extrémités si le reste > ax / 2
     * sinon les cellules aux extrémités sont étendues jusqu'aux bords.
     * La cote de radier de chaque cellule est calculée au milieu de celle-ci.
     */
    private generateInclinedFishway() {
        // Calcul de la position des cellules
        // Taille d'une cellule de calcul
        const ax: number = this.prms.PBD.singleValue / Math.sqrt(this.prms.C.singleValue);
        // Nombre entier de cellules et reste
        const nCells: number = Math.floor(this.prms.BR.singleValue / ax);
        const xRest: number = this.prms.BR.singleValue % ax;
        const xCenters: number[] = [];
        let lastBorder: number = 0;
        // Position du centre des cellules
        for (let i = 0; i < nCells; i++) {
            if (i === nCells - 1 && xRest <= ax / 2) {
                // Agrandissement de la première cellule (taille normale + xRest) si
                // le reste était trop faible pour en rajouter une
                xCenters.push(lastBorder + (ax + xRest) / 2);
                lastBorder += ax + xRest;
            } else {
                xCenters.push(lastBorder + ax / 2);
                lastBorder += ax;
            }
        }
        // Ajout éventuel d'une cellule à droite (en haut), de taille xRest
        if (xRest > ax / 2) {
            xCenters.push(lastBorder + xRest / 2);
        }
        // Génération des radiers
        // Suppression des radiers existants
        this.deleteAllChildren();
        // Ajout des radiers et calcul de leur cote amont et de leur largeur
        lastBorder = 0;
        for (const xCenter of xCenters) {
            this.addDefaultChild();
            this.children[this.children.length - 1].prms.ZF1.singleValue = round(
                this.prms.ZRT.singleValue + xCenter / this.prms.BR.singleValue
                * (this.prms.ZRB.singleValue - this.prms.ZRT.singleValue)
                , 3);
            this.children[this.children.length - 1].prms.B.singleValue = round((xCenter - lastBorder) * 2, 3);
            lastBorder += this.children[this.children.length - 1].prms.B.singleValue;
        }
    }
}
