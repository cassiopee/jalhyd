import { floatDivAndMod, isEqual } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { ConcentrationBlocsParams } from "../internal_modules";

export class ConcentrationBlocs extends Nub {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        ax: "m",
        R: "m",
        AXB: "m",
        AXH: "m"
    };

    constructor(prms: ConcentrationBlocsParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.ConcentrationBlocs);
        this._defaultCalculatedParam = prms.C;
        this.resetDefaultCalculatedParam();
    }

    get prms(): ConcentrationBlocsParams {
        return this._prms as ConcentrationBlocsParams;
    }

    /**
     * L / N = ax = D / sqrt(C)
     */
    public Equation(sVarCalc: string): Result {
        let rounded = false;
        if (sVarCalc !== "N" && !Number.isInteger(this.prms.N.v)) {
            this.prms.N.v = Math.round(this.prms.N.v);
            rounded = true;
        }

        let v: number;
        let ax: number; // pattern size
        let R: number; // rest
        let NB: number;
        let axB: number;
        let cB: number;
        let NH: number;
        let axH: number;
        let cH: number;

        switch (sVarCalc) {
            case "C":
                ax = this.prms.L.v / this.prms.N.v;
                v = Math.pow(this.prms.D.v / ax, 2);
                break;

            case "N":
                ax = this.prms.D.v / Math.sqrt(this.prms.C.v);
                const divAndMod = floatDivAndMod(this.prms.L.v, ax);
                v = divAndMod.q;
                R = divAndMod.r;
                // parfois la largeur résiduelle peut être à peine en dessous de la largeur de motif (modulo foireux)
                if (isEqual(R, ax, 1e-2)) {
                    R -= ax;
                    v++;
                }
                // harmonisation ?
                if (!isEqual(R, 0, 1e-2)) {
                    // vers le bas
                    if (v > 0) {
                        NB = v;
                        axB = this.prms.L.v / NB;
                        cB = Math.pow(this.prms.D.v / axB, 2);
                    }
                    // vers le haut
                    NH = v + 1;
                    axH = this.prms.L.v / NH;
                    cH = Math.pow(this.prms.D.v / axH, 2);
                }
                break;

            case "L":
                ax = this.prms.D.v / Math.sqrt(this.prms.C.v);
                v = ax * this.prms.N.v;
                break;

            case "D":
                ax = this.prms.L.v / this.prms.N.v;
                v = ax * Math.sqrt(this.prms.C.v);
                break;

            default:
                throw new Error("ConcentrationBlocs.Equation() : invalid variable name " + sVarCalc);
        }

        const r = new Result(v, this);
        // extra results
        r.resultElement.values.ax = ax;
        if (R !== undefined) {
            r.resultElement.values.R = R;
        }
        if (NB !== undefined) {
            r.resultElement.values.NB = NB;
        }
        if (axB !== undefined) {
            r.resultElement.values.AXB = axB;
        }
        if (cB !== undefined) {
            r.resultElement.values.CB = cB;
        }
        if (NH !== undefined) {
            r.resultElement.values.NH = NH;
        }
        if (axH !== undefined) {
            r.resultElement.values.AXH = axH;
        }
        if (cH !== undefined) {
            r.resultElement.values.CH = cH;
        }

        if (rounded) {
            const m = new Message(MessageCode.WARNING_VALUE_ROUNDED_TO_INTEGER);
            m.extraVar.symbol = "N";
            m.extraVar.rounded = this.prms.N.v;
            r.resultElement.log.add(m);
        }

        return r;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.C.calculability = ParamCalculability.EQUATION;
        this.prms.N.calculability = ParamCalculability.EQUATION;
        this.prms.L.calculability = ParamCalculability.EQUATION;
        this.prms.D.calculability = ParamCalculability.EQUATION;
    }

    public static override resultsUnits() {
        return ConcentrationBlocs._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            ax: undefined,
            R: undefined,
            NB: undefined,
            AXB: undefined,
            CB: undefined,
            NH: undefined,
            AXH: undefined,
            CH: undefined
        };
    }
}
