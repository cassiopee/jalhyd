import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { MacrorugoParams } from "../internal_modules";

export class MacrorugoCompoundParams extends MacrorugoParams {

    /** Upstream water elevation (m) */
    private _Z1: ParamDefinition;

    /** Water fall (m) */
    private _DH: ParamDefinition;

    /** Inclined apron: Left bank apron elevation (m) */
    private _ZRB: ParamDefinition;

    /** Inclined apron: Right bank apron elevation (m) */
    private _ZRT: ParamDefinition;

    /** Inclined apron: Apron total width (m) */
    private _BR: ParamDefinition;

    /**
     *
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZRB Cote de radier amont haut (m) (Radier incliné seulement)
     * @param rZRT Cote de radier amont bas (m) (Radier incliné seulement)
     * @param rB Largeur (m) (Radier incliné seulement)
     * @param rDH Chute (m)
     * @param rIf Pente (m/m)
     * @param rY Tirant d'eau (m)
     * @param rRF Rugosité de fond (m)
     * @param rCB Concentration de blocs (m)
     * @param rPBD Paramètre de bloc : Diamètre (m)
     * @param rPBH Paramètre de bloc : Hauteur (m)
     * @param rCd0 Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     */
    constructor(
        rZ1: number,
        rZRB: number,
        rZRT: number,
        rB: number,
        rDH: number,
        rIf: number,
        rRF: number,
        rCB: number,
        rPBD: number,
        rPBH: number,
        rCd0: number,
        nullParams: boolean = false
    ) {
        super((rZRB + rZRT) / 2, 1, rDH / rIf, rIf, 1, 1, rRF, rCB, rPBD, rPBH, rCd0, nullParams);

        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._Z1);

        this._ZRB = new ParamDefinition(this, "ZRB", ParamDomainValue.ANY, "m", rZRB, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZRB);

        this._ZRT = new ParamDefinition(this, "ZRT", ParamDomainValue.ANY, "m", rZRT, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZRT);

        this._BR = new ParamDefinition(this, "BR",
            new ParamDomain(ParamDomainValue.INTERVAL, 0, 100), "m", rB, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this._BR);

        this._DH = new ParamDefinition(this, "DH",
            new ParamDomain(ParamDomainValue.INTERVAL, 0, 100), "m", rDH, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._DH);

        // Width, water depth, and Bottom elevation are defined in Macrorugo children
        this.B.visible = false;
        this.ZF1.visible = false;
        this.Y.visible = false;
        this.Q.visible = false;

    }

    public get Z1(): ParamDefinition {
        return this._Z1;
    }

    public get ZRB(): ParamDefinition {
        return this._ZRB;
    }

    public get ZRT(): ParamDefinition {
        return this._ZRT;
    }

    public get BR(): ParamDefinition {
        return this._BR;
    }

    public get DH(): ParamDefinition {
        return this._DH;
    }
}
