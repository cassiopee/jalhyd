import { CalculatorType, Message, MessageCode } from "../internal_modules";
import { CourbeRemous } from "../internal_modules";
import { cSnRectang } from "../internal_modules";
import { MacroRugo } from "../internal_modules";
import { MethodeResolution } from "../internal_modules";
import { Result } from "../internal_modules";
import { Session } from "../internal_modules";
import { MacrorugoRemousParams } from "../internal_modules";

export class MacrorugoRemous extends CourbeRemous {

    private _nubMacroRugo: MacroRugo;
    private stricklers: { [Y: string]: number; } = {};

    constructor(nubMacroRugo: MacroRugo,
        crp: MacrorugoRemousParams,
        method: MethodeResolution = MethodeResolution.Trapezes,
        dbg: boolean = false
    ) {
        super(undefined, crp, method, dbg);
        this.setCalculatorType(CalculatorType.MacrorugoRemous);
        const s = new cSnRectang(crp.sectionParams);
        this.setSection(s);

        this._nubMacroRugo = nubMacroRugo;
        if (this._nubMacroRugo !== undefined) {
            this.setPropValue("nubMacroRugo", this._nubMacroRugo.uid);
        }
    }

    public get nubMacroRugo(): MacroRugo {
        return this._nubMacroRugo;
    }

    get prms(): MacrorugoRemousParams {
        return this._prms as MacrorugoRemousParams;
    }

    /**
     * Clone the MacroRugo nub for the sake of calculation
     */
    public setNubMacroRugoFromProps(): void {
        const nubUID: string = this._props.getPropValue("nubMacroRugo");
        if (nubUID !== undefined && nubUID !== "") {
            const nub = Session.getInstance().findNubByUid(nubUID);
            if (nub !== undefined) {
                if (nub.calcType !== CalculatorType.MacroRugo) {
                    throw new Error("nubMacroRugo must be of MacroRugo calculator type");
                }
                //TODO: check that all parameters are in fixed mode
                this._nubMacroRugo = nub.clone() as MacroRugo;
                // Set result as input parameter
                if (this._nubMacroRugo.result === undefined) {
                    this._nubMacroRugo.CalcSerie();
                }
                if (this._nubMacroRugo.result.ok) {
                    if (this._nubMacroRugo.result.resultElements.length > 1) {
                        throw new Error("nubMacroRugo should have only one result");
                    }
                    this._nubMacroRugo.calculatedParam.singleValue = this._nubMacroRugo.result.vCalc;
                    this._nubMacroRugo.calculatedParam = this._nubMacroRugo.prms.If;
                }
            }
            // silent fail if (nub === undefined)
        }
        if (this._nubMacroRugo === undefined) {
            throw new Error("nubMacroRugo property is undefined");
        }
    }

    /**
     * Synchronises section and MacroRugo parameters
     */
    public syncSectionMacroRugo() {
        this.setNubMacroRugoFromProps();
        if (this._nubMacroRugo.result.ok) {
            this.section.prms.LargeurBerge.singleValue = this._nubMacroRugo.prms.B.singleValue;
            this.section.prms.Q.singleValue = this._nubMacroRugo.prms.Q.singleValue;
            this.section.prms.If.singleValue = this._nubMacroRugo.prms.If.singleValue;
            const Z2 = this.prms.Z2.singleValue;
            const ZF2 = this._nubMacroRugo.result.getValue("ZF2");
            this.section.prms.YB.singleValue = Math.max((Z2 - ZF2) * 1.5, 1); // pour éviter de fournir Infinity (max avec 1 à cause des fuzz tests)
            this.section.prms.Ks.singleValue = this._nubMacroRugo.result.resultElement.values.Strickler;
            this.prms.ZF1.singleValue = this._nubMacroRugo.prms.ZF1.singleValue;
            this.prms.ZF2.singleValue = ZF2;
            this.prms.Long.singleValue = this._nubMacroRugo.prms.L.singleValue;

            // set Dx to half of block distance
            const ax: number = this._nubMacroRugo.prms.PBD.v / Math.sqrt(this._nubMacroRugo.prms.C.v);
            this.prms.Dx.singleValue = ax / 2;
        }
    }

    /**
     * Computes the backwater curve
     * @param rInit Not used
     * @returns Nub result
     */
    public CalcSerie(rInit?: number): Result {
        this.syncSectionMacroRugo();
        if (!this._nubMacroRugo.result.ok) {
            const res = new Result();
            res.addLog(this._nubMacroRugo.result.globalLog);
            res.addLog(this._nubMacroRugo.result.log);
            return res;
        }
        this.preCalc();
        const rYC = this.Sn.CalcSection("Yc");
        if (!rYC.ok) {
            const res = new Result(undefined, this);
            res.addLog(rYC.log);
            return res;
        }
        let bDownFall = this.prms.Z2.singleValue - this.prms.ZF2.singleValue <= rYC.vCalc;
        const z2 = this.prms.Z2.singleValue;
        if (bDownFall) {
            this.prms.Z2.singleValue = this.prms.ZF2.singleValue + rYC.vCalc + 1E-3;
        }

        const res = super.CalcSerie(rInit);

        res.resultElements.forEach((re) => {
            const Y = re.values["Y"];
            re.values["Ks"] = this.stricklers[re.values["Y"].toFixed(3)];
        })

        // add warning about downstream fall in case of downstream water elevation < critical elevation
        if (bDownFall) {
            this.prms.Z2.singleValue = z2;
            const zCrit = this.prms.ZF2.singleValue + rYC.vCalc;
            const downFall = zCrit - this.prms.Z2.singleValue;
            res.globalLog.insert(new Message(
                MessageCode.WARNING_MACRORUGOREMOUS_CHUTE_AVAL,
                {
                    fall: downFall,
                    zCrit: zCrit
                }
            ));
        }

        return res;
    }

    /**
     * Updates the Strickler coefficient from MacroRugo and
     * computes the next point of the backwater curve
     * @param Y current water depth (m)
     */
    protected Calc_Y(Y: number): Result {
        this._nubMacroRugo.prms.Y.v = Y;
        const res = this._nubMacroRugo.Calc("If");
        if (!res.ok) {
            return res;
        }
        this.section.prms.Ks.singleValue = this._nubMacroRugo.result.resultElement.values.Strickler;
        this.stricklers[Y.toFixed(3)] = this.section.prms.Ks.singleValue;
        return super.Calc_Y(Y);
    }
}
