/** Inclination state for MacroRugoCompound */
export enum MRCInclination {
    NOT_INCLINED,
    INCLINED
}
