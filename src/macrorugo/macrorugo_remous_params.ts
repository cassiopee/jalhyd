import { CourbeRemousParams, ParamDefinition, ParamDomain, ParamFamily, ParamsSectionRectang } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";

/**
 * paramètres pour les courbes de remous
 */
export class MacrorugoRemousParams extends CourbeRemousParams {

    private _sectParams: ParamsSectionRectang;

    constructor(rZ2: number, nullParams: boolean = false) {
        super(undefined, rZ2, undefined, undefined, undefined, undefined, nullParams);

        this._sectParams = new ParamsSectionRectang(
            undefined, undefined, undefined, undefined, undefined, Infinity
        );

        this.Long.visible = false;
        this.Z1.visible = false;
        this.ZF1.visible = false;
        this.ZF2.visible = false;
        this._sectParams.Ks.visible = false;
        this._sectParams.YB.visible = false;
        this._sectParams.LargeurBerge.visible = false;
    }

    public get sectionParams(): ParamsSectionRectang {
        return this._sectParams;
    }
}
