import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class ConcentrationBlocsParams extends ParamsEquation {

    /** Concentration de blocs */
    private _C: ParamDefinition;

    /** Nombre de motifs */
    private _N: ParamDefinition;

    /** Largeur de la passe */
    private _L: ParamDefinition;

    /** Diamètre des plots */
    private _D: ParamDefinition;

    constructor(rC: number, rN: number, rL: number, rD: number, nullParams: boolean = false) {
        super();
        this._C = new ParamDefinition(this, "C", new ParamDomain(ParamDomainValue.INTERVAL, 0, 1), undefined, rC, ParamFamily.BLOCKCONCENTRATION, undefined, nullParams);
        this._N = new ParamDefinition(this, "N", ParamDomainValue.POS, undefined, rN, undefined, undefined, nullParams);
        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.WIDTHS, undefined, nullParams);
        this._D = new ParamDefinition(this, "D", new ParamDomain(ParamDomainValue.INTERVAL, 0, 2), "m", rD, ParamFamily.DIAMETERS, undefined, nullParams);

        this.addParamDefinition(this._C);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._D);
    }

    /** Concentration de blocs */
    get C() {
        return this._C;
    }

    /** Nombre de motifs */
    get N() {
        return this._N;
    }

    /** Largeur de la passe */
    get L() {
        return this._L;
    }

    /** Diamètre des plots */
    get D() {
        return this._D;
    }
}
