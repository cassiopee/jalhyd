import { Nub } from "./internal_modules";

/**
 * An intermediate class for all fish passes (Pab, Par, MacroRugo[Compound]…),
 * just for the sake of typing
 */
export abstract class FishPass extends Nub { }
