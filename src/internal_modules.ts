// Implementation of the "internal module pattern" to solve the "TypeError: Class extends value undefined is not a constructor or null" error
// cf. https://medium.com/visual-development/how-to-fix-nasty-circular-dependency-issues-once-and-for-all-in-javascript-typescript-a04c987cf0de
// All imports in jalhyd are from this file only.
// Exported modules outside jalhyd by index.ts may be different (generally a subset of this present file).

export * from "./base";
export * from "./param/param-definition";
export * from "./jalhyd_object";
export * from "./compute-node";
export * from "./dichotomie";
export * from "./nub";
export * from "./child_nub";
export * from "./session_settings";
export * from "./config";
export * from "./param/param-domain";
export * from "./param/param-value-mode";
export * from "./param/param-values";
export * from "./param/param-value-iterator";
export * from "./param/param_definition_iterator";
export * from "./param/params-equation";
export * from "./param/params_equation_array_iterator";
export * from "./param/mirror-iterator";
export * from "./linked-value";
export * from "./util/interval";
export * from "./util/message";
export * from "./util/result";
export * from "./util/resultelement";
export * from "./util/observer";
export * from "./util/log";
export * from "./util/map_iterator";
export * from "./util/search_interval";
export * from "./util/mermaid";
export * from "./util/array_reverse_iterator";
export * from "./variated-details";
export * from "./lc-material";

// Calculettes
export * from "./devalaison/grille";
export * from "./devalaison/grille_params";
export * from "./devalaison/jet";
export * from "./devalaison/jet_params";
export * from "./fish_pass";
export * from "./macrorugo/macrorugo";
export * from "./macrorugo/macrorugo_params";
export * from "./macrorugo/macrorugo_compound";
export * from "./macrorugo/macrorugo_compound_params";
export * from "./macrorugo/concentration_blocs";
export * from "./macrorugo/concentration_blocs_params";
export * from "./macrorugo/mrc-inclination";
export * from "./math/spp";
export * from "./math/spp_params";
export * from "./math/trigo";
export * from "./math/trigo_params";
export * from "./math/yaxb";
export * from "./math/yaxb_params";
export * from "./math/yaxn";
export * from "./math/yaxn_params";
export * from "./open-channel/section/section_nub";
export * from "./open-channel/methode-resolution";
export * from "./open-channel/remous";
export * from "./open-channel/remous_params";
export * from "./open-channel/section/section_type";
export * from "./open-channel/section/newton";
export * from "./open-channel/section/section_type_params";
export * from "./open-channel/section/section_parametree";
export * from "./open-channel/section/section_parametree_params";
export * from "./open-channel/section/section_circulaire";
export * from "./open-channel/section/section_circulaire_params";
export * from "./open-channel/section/section_puissance";
export * from "./open-channel/section/section_puissance_params";
export * from "./open-channel/section/section_rectang";
export * from "./open-channel/section/section_rectang_params";
export * from "./open-channel/section/section_trapez";
export * from "./open-channel/section/section_trapez_params";
export * from "./open-channel/section/hauteur_conjuguee";
export * from "./open-channel/section/hauteur_correspondante";
export * from "./open-channel/section/hauteur_critique";
export * from "./open-channel/section/hauteur_normale";
export * from "./open-channel/bief";
export * from "./open-channel/bief_params";
export * from "./open-channel/pente";
export * from "./open-channel/pente_params";
export * from "./open-channel/regime_uniforme";
export * from "./par/par";
export * from "./par/par_params";
export * from "./par/par_simulation";
export * from "./par/par_simulation_params";
export * from "./par/par_type";
export * from "./par/par_type_pf";
export * from "./par/par_type_fatou";
export * from "./par/par_type_sc";
export * from "./par/par_type_chevron";
export * from "./par/par_type_plane";
export * from "./par/par_type_superactive";
export * from "./structure/structure";
export * from "./structure/structure_params";
export * from "./structure/parallel_structure";
export * from "./structure/parallel_structure_params";
export * from "./structure/dever";
export * from "./structure/dever_params";
export * from "./structure/factory_structure";
export * from "./structure/structure_props";
export * from "./structure/rectangular_structure";
export * from "./structure/rectangular_structure_params";
export * from "./structure/structure_gate_cem88d";
export * from "./structure/structure_gate_cem88v";
export * from "./structure/structure_gate_cunge80";
export * from "./structure/structure_kivi";
export * from "./structure/structure_kivi_params";
export * from "./structure/structure_orifice_free";
export * from "./structure/structure_orifice_free_params";
export * from "./structure/structure_orifice_submerged";
export * from "./structure/structure_orifice_submerged_params";
export * from "./structure/structure_rectangular_orifice_free";
export * from "./structure/structure_rectangular_orifice_submerged";
export * from "./structure/structure_triangular_trunc_weir";
export * from "./structure/structure_triangular_trunc_weir_params";
export * from "./structure/structure_triangular_weir_free";
export * from "./structure/structure_triangular_weir";
export * from "./structure/structure_triangular_weir_params";
export * from "./structure/structure_triangular_weir_broad";
export * from "./structure/structure_weir_submerged";
export * from "./structure/structure_weir_submerged_larinier";
export * from "./structure/structure_weir_free";
export * from "./structure/structure_weir_villemonte";
export * from "./structure/structure_vanlev_params";
export * from "./structure/structure_vanlev_larinier";
export * from "./structure/structure_vanlev_villemonte";
export * from "./structure/structure_weir_cem88d";
export * from "./structure/structure_weir_cem88v";
export * from "./structure/structure_weir_cunge80";
export * from "./structure/villemonte";
export * from "./pab/pab";
export * from "./pab/pab_params";
export * from "./pab/cloison_aval";
export * from "./pab/cloison_aval_params";
export * from "./pab/cloisons";
export * from "./pab/cloisons_params";
export * from "./pab/pab_chute";
export * from "./pab/pab_chute_params";
export * from "./pab/pab_dimension";
export * from "./pab/pab_dimensions_params";
export * from "./pab/pab_nombre";
export * from "./pab/pab_nombre_params";
export * from "./pab/pab_puissance";
export * from "./pab/pab_puissance_params";
export * from "./pipe_flow/cond_distri";
export * from "./pipe_flow/cond_distri_params";
export * from "./pipe_flow/pressureloss";
export * from "./pipe_flow/pressureloss_params";
export * from "./pipe_flow/pressureloss_law";
export * from "./pipe_flow/pressureloss_law_params";
export * from "./pipe_flow/pl_lechaptcalmon";
export * from "./pipe_flow/pl_lechaptcalmon_params";
export * from "./pipe_flow/pl_strickler";
export * from "./pipe_flow/pl_strickler_params";
export * from "./prebarrage/pb_bassin";
export * from "./prebarrage/pb_bassin_params";
export * from "./prebarrage/pb_cloison";
export * from "./prebarrage/pre_barrage";
export * from "./prebarrage/pre_barrage_params";
export * from "./solveur/solveur";
export * from "./solveur/solveur_params";
export * from "./verification/diving-jet-support";
export * from "./verification/espece";
export * from "./verification/espece_params";
export * from "./verification/fish_species";
export * from "./verification/verificateur";
export * from "./verification/verificateur_params";
export * from "./props";
export * from "./session";
