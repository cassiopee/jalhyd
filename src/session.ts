import { CalculatorType, IProperties, PL_Strickler, PL_StricklerParams, PressureLossParams, SectionType } from "./internal_modules";
import { config } from "./internal_modules";
import { LinkedValue } from "./internal_modules";
import { Nub } from "./internal_modules";
import { ParamDefinition } from "./internal_modules";
import { Props, Prop_NullParameters } from "./internal_modules";
import { SessionSettings } from "./internal_modules";

// Calculettes
import { Grille } from "./internal_modules";
import { GrilleParams } from "./internal_modules";
import { Jet } from "./internal_modules";
import { JetParams } from "./internal_modules";
import { ConcentrationBlocs } from "./internal_modules";
import { ConcentrationBlocsParams } from "./internal_modules";
import { MacroRugo } from "./internal_modules";
import { MacrorugoCompound } from "./internal_modules";
import { MacrorugoCompoundParams } from "./internal_modules";
import { MacrorugoParams } from "./internal_modules";
import { SPP } from "./internal_modules";
import { SPPParams } from "./internal_modules";
import { Trigo } from "./internal_modules";
import { TrigoParams } from "./internal_modules";
import { YAXB } from "./internal_modules";
import { YAXBParams } from "./internal_modules";
import { YAXN } from "./internal_modules";
import { YAXNParams } from "./internal_modules";
import { Bief } from "./internal_modules";
import { BiefParams } from "./internal_modules";
import { MethodeResolution } from "./internal_modules";
import { Pente } from "./internal_modules";
import { PenteParams } from "./internal_modules";
import { RegimeUniforme } from "./internal_modules";
import { CourbeRemous } from "./internal_modules";
import { CourbeRemousParams } from "./internal_modules";
import { cSnCirc } from "./internal_modules";
import { ParamsSectionCirc } from "./internal_modules";
import { SectionParametree } from "./internal_modules";
import { cSnPuiss } from "./internal_modules";
import { ParamsSectionPuiss } from "./internal_modules";
import { cSnRectang } from "./internal_modules";
import { ParamsSectionRectang } from "./internal_modules";
import { cSnTrapez } from "./internal_modules";
import { ParamsSectionTrapez } from "./internal_modules";
import { acSection } from "./internal_modules";
import { CloisonAval } from "./internal_modules";
import { CloisonsAvalParams } from "./internal_modules";
import { Cloisons } from "./internal_modules";
import { CloisonsParams } from "./internal_modules";
import { Pab } from "./internal_modules";
import { PabChute } from "./internal_modules";
import { PabChuteParams } from "./internal_modules";
import { PabDimension } from "./internal_modules";
import { PabDimensionParams } from "./internal_modules";
import { PabNombre } from "./internal_modules";
import { PabNombreParams } from "./internal_modules";
import { PabParams } from "./internal_modules";
import { PabPuissance } from "./internal_modules";
import { PabPuissanceParams } from "./internal_modules";
import { ConduiteDistrib } from "./internal_modules";
import { ConduiteDistribParams } from "./internal_modules";
import { PL_LechaptCalmon } from "./internal_modules";
import { PL_LechaptCalmonParams } from "./internal_modules";
import { Solveur } from "./internal_modules";
import { SolveurParams } from "./internal_modules";
import { Dever } from "./internal_modules";
import { DeverParams } from "./internal_modules";
import { CreateStructure } from "./internal_modules";
import { ParallelStructure } from "./internal_modules";
import { ParallelStructureParams } from "./internal_modules";
import { LoiDebit } from "./internal_modules";
import { Par } from "./internal_modules";
import { ParParams } from "./internal_modules";
import { ParSimulation } from "./internal_modules";
import { ParSimulationParams } from "./internal_modules";
import { Espece } from "./internal_modules";
import { EspeceParams } from "./internal_modules";
import { Verificateur } from "./internal_modules";
import { PreBarrage } from "./internal_modules";
import { PreBarrageParams } from "./internal_modules";
import { PbCloison } from "./internal_modules";
import { PbBassin } from "./internal_modules";
import { PbBassinParams } from "./internal_modules";
import { ParamValueMode } from "./internal_modules";
import { PressureLoss } from "./internal_modules";
import { PressureLossLaw, PressureLossType } from "./internal_modules";

export class Session {

    public static getInstance(): Session {
        if (Session._instance === undefined) {
            Session._instance = new Session();
        }
        return Session._instance;
    }

    /** instance pour le pattern singleton */
    private static _instance: Session;

    /** free documentation text, in Markdown format, to save into session file (optional) */
    public documentation = "";

    /** Nubs de la session */
    private _nubs: Nub[] = [];

    /**
     * crée un Nub et l'ajoute à la session
     * @param props propriétés du Nub (computeType, nodeType...)
     */
    public createSessionNub(p: Props, dbg: boolean = false): Nub {
        const res = this.createNub(p, undefined, dbg);
        this._nubs.push(res);
        return res;
    }

    /**
     * Adds an existing Nub to the session
     */
    public registerNub(n: Nub) {
        if (this.uidAlreadyUsed(n.uid)) {
            n.setUid(Nub.nextUID);
        }
        this._nubs.push(n);
    }

    /**
     * Adds many existing Nubs to the session
     */
    public registerNubs(nubs: Nub[]) {
        for (const n of nubs) {
            this.registerNub(n);
        }
    }

    /**
     * Removes all Nubs from the Session
     */
    public clear() {
        this._nubs = [];
    }

    /**
     * Returns number of Nubs in the session
     */
    public getNumberOfNubs() {
        return this._nubs.length;
    }

    /** Accessor for Nubs list */
    public getAllNubs() {
        return this._nubs;
    }

    /**
     * Removes a Nub from the session; does not consider Structure nubs inside Calculator nubs
     * @param sn the Nub to remove from the session
     */
    public deleteNub(sn: Nub) {
        let i = 0;
        for (const n of this._nubs) {
            if (n.uid === sn.uid) {
                this._nubs.splice(i, 1);
                this.fixDanglingLinks();
                return;
            }
            i++;
        }
        throw new Error(`Session.deleteNub() : le Nub (uid ${sn.uid}) à supprimer n'a pas été trouvé`);
    }

    /**
     * set parameters linked to non exsting modules to fixed mode
     */
    private fixDanglingLinks() {
        for (const n of this._nubs) {
            for (const p of n.parameterIterator) {
                if (p.valueMode === ParamValueMode.LINK) {
                    const targetId = p.referencedValue.nub.uid;
                    if (this.findNubByUid(targetId) === undefined) {
                        p.valueMode = ParamValueMode.SINGLE;
                    }
                }
            }
        }
    }

    /**
     * Returns a JSON representation of (a part of) the current session
     * @param options an object having Nub uids as keys, with extra data object as values;
     *      if empty or undefined, all Nubs are serialised
     * @param settings app preferences to store in the session file (decimals, precision…)
     */
    public serialise(options?: { [key: string]: {} }, settings?: { [key: string]: {} }): string {
        const sess: any[] = [];
        // session-wide settings
        let sessionSettings = {
            precision: SessionSettings.precision,
            maxIterations: SessionSettings.maxIterations
        };
        if (settings) {
            sessionSettings = { ...sessionSettings, ...settings };
        }
        // nubs in session
        let ids: string[];
        let idsWithChildren: string[];
        if (options) {
            ids = Object.keys(options);
            idsWithChildren = [...ids];
            // add ids of children
            for (const n of this._nubs) {
                if (ids.includes(n.uid)) {
                    for (const c of n.getChildren()) {
                        idsWithChildren.push(c.uid);
                    }
                }
            }
        }
        for (const n of this._nubs) {
            if (ids === undefined || ids.length === 0) {
                sess.push(n.objectRepresentation(undefined, ids));
            } else if (ids.includes(n.uid)) {
                sess.push(n.objectRepresentation(options[n.uid], idsWithChildren));
            }
        }
        return JSON.stringify({
            header: {
                source: "jalhyd",
                format_version: config.serialisation.fileFormatVersion,
                created: (new Date()).toISOString()
            },
            settings: sessionSettings,
            documentation: this.documentation,
            session: sess
        });
    }

    /**
     * Loads (a part of) a session from a JSON representation
     * @param serialised JSON data
     * @param uids unserialise only the Nubs havin the given UIDs
     */
    public unserialise(serialised: string, uids?: string[]): { nubs: any[], hasErrors: boolean, settings: any } {
        // return value
        const ret: { nubs: any[], hasErrors: boolean, settings: any } = {
            nubs: [],
            hasErrors: false,
            settings: {}
        };
        // unserialise to object
        const data = JSON.parse(serialised);
        // settings
        if (data.settings) {
            ret.settings = data.settings;
            if (data.settings.precision !== undefined) {
                SessionSettings.precision = data.settings.precision;
            }
            if (data.settings.maxIterations !== undefined) {
                SessionSettings.maxIterations = data.settings.maxIterations;
            }
        }
        // nubs
        if (data.session && Array.isArray(data.session)) {
            data.session.forEach((e: any) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nubPointer = this.createNubFromObjectRepresentation(e);
                    ret.nubs.push(nubPointer);
                    // forward errors
                    if (nubPointer.hasErrors) {
                        ret.hasErrors = true;
                    }
                }
            });
        }
        // concatenate doc
        if (data.documentation !== undefined && data.documentation !== "") {
            this.documentation += `\n\n` + data.documentation;
        }

        // second pass for links
        const flRes = this.fixLinks(serialised, uids);
        // forward errors
        if (flRes.hasErrors) {
            ret.hasErrors = true;
        }
        // second pass for Solveurs
        const fsRes = this.fixSolveurs(serialised, uids);
        // forward errors
        if (fsRes.hasErrors) {
            ret.hasErrors = true;
        }

        return ret;
    }

    /**
     * Creates a Nub from a JSON representation and adds it to the current session; returns
     * a pointer to the Nub and its JSON metadata
     * @param serialised JSON representation of a single Nub
     * @param register if false, new Nub will just be returned and won't be registered into the session
     */
    public unserialiseSingleNub(serialised: string, register: boolean = true): { nub: Nub, meta: any } {
        return this.createNubFromObjectRepresentation(JSON.parse(serialised), register);
    }

    /**
     * Returns the Nub identified by uid if any
     */
    public findNubByUid(uid: string): Nub {
        let foundNub: Nub;
        outerLoop:
        for (const n of this._nubs) {
            if (n.uid === uid) {
                foundNub = n;
            }
            for (const s of n.getChildren()) {
                if (s.uid === uid) {
                    foundNub = s;
                    break outerLoop;
                }
            }
        }
        return foundNub;
    }

    private static parameterIndex(obj: any, symbol: string): number {
        let i;
        const prms = obj["parameters"];
        for (i in prms) {
            if (prms[i]["symbol"] === symbol) {
                return +i;
            }
        }
        return -1;
    }

    /**
     * Calcule le type de calculette compatible et modifie les propriétés en conséquence.
     * Permet de charger des fichiers session avec une version antérieure.
     * Par ex : Lechapt-Calmon -> PressureLoss
     */
    private compatibleCalculator(obj: any): any {
        switch (obj["props"]["calcType"]) {
            case CalculatorType.LechaptCalmon:
                // create parent PressureLoss nub
                const plProps = new Props();
                plProps.setPropValue("calcType", CalculatorType.PressureLoss);
                const pl = this.createNub(plProps);

                // JSON representation
                let res: any = pl.objectRepresentation();

                // set Lechapt-Calmon as child
                res["children"] = [obj];

                // move PressureLoss parameters from Lechapt-Calmon
                const movedParams = ["Q", "D", "J", "Lg", "Kloc"];
                for (const p of movedParams) {
                    // if child has parameter
                    const cp = Session.parameterIndex(obj, p);
                    if (cp !== -1) {
                        const pp = Session.parameterIndex(res, p);
                        if (pp === -1) {
                            res["parameters"].push(obj["parameters"][cp]);
                        } else {
                            res["parameters"][pp] = obj["parameters"][cp];
                        }
                        // delete obj["parameters"][pp];
                        obj["parameters"].splice(cp, 1);
                    }
                }
                return res;

            default:
                return obj;
        }
    }

    /**
     * Crée un Nub à partir d'une description (Props)
     * @param params propriétés à partir desquelles on détermine la classe du Nub à créer
     *      - calcType: type de Nub
     *      - nodeType: pour un Nub contenant une section
     *      - loiDebit: pour un Nub de type Structure (calcType doit être CalculatorType.Structure)
     *    Si d'autres propriétés sont fournies, elle écraseront les éventuelles propriétés par défaut
     *    définies dans le constructeur du Nub créé
     * @param dbg activer débogage
     */
    public createNub(params: IProperties, parentNub?: Nub, dbg: boolean = false): Nub {
        // true if provided values to parameter creation must be ignored
        const nullParams: boolean = params.getPropValue(Prop_NullParameters) === undefined ? false : params.getPropValue(Prop_NullParameters);

        let nub: Nub;
        let prms: any;

        const calcType: CalculatorType = params.getPropValue("calcType");
        switch (calcType) {
            case CalculatorType.ConduiteDistributrice:
                prms = new ConduiteDistribParams(
                    3, // débit Q
                    1.2, // diamètre D
                    0.6, // perte de charge J
                    100, // Longueur de la conduite Lg
                    1e-6, // Viscosité dynamique Nu
                    nullParams
                );
                nub = new ConduiteDistrib(prms, dbg);
                break;

            case CalculatorType.LechaptCalmon:
                prms = new PL_LechaptCalmonParams(
                    1.863, // paramètre L du matériau
                    2, // paramètre M du matériau
                    5.33, // paramètre N du matériau
                    nullParams
                );
                nub = new PL_LechaptCalmon(prms, dbg);
                break;

            case CalculatorType.SectionParametree:
                nub = new SectionParametree(undefined, dbg);
                break;

            case CalculatorType.RegimeUniforme:
                nub = new RegimeUniforme(undefined, dbg);
                break;

            case CalculatorType.CourbeRemous:
                prms = new CourbeRemousParams(
                    100.25, // Z1 = cote de l'eau amont
                    100.4,  // Z2 = cote de l'eau aval
                    100.1,  // ZF1 = cote de fond amont
                    100,    // ZF2 = cote de fond aval
                    100,    // Long = Longueur du bief
                    5,      // Dx = Pas d'espace
                    nullParams
                );
                nub = new CourbeRemous(undefined, prms, MethodeResolution.EulerExplicite, dbg);
                break;

            case CalculatorType.PabDimensions:
                prms = new PabDimensionParams(
                    2,      // Longueur L
                    1,      // Largeur W
                    0.5,    // Tirant d'eau Y
                    2,       // Volume V
                    nullParams
                );
                nub = new PabDimension(prms, dbg);
                break;

            case CalculatorType.PabPuissance:
                prms = new PabPuissanceParams(
                    0.3,      // Chute entre bassins DH (m)
                    0.1,      // Débit Q (m3/s)
                    0.5,    // Volume V (m3)
                    588.6,   // Puissance dissipée PV (W/m3)
                    nullParams
                );
                nub = new PabPuissance(prms, dbg);
                break;

            case CalculatorType.Structure:
                const loiDebit: LoiDebit = params.getPropValue("loiDebit");
                nub = CreateStructure(loiDebit, (parentNub as ParallelStructure), dbg, nullParams);
                break;

            case CalculatorType.ParallelStructure:
                prms = new ParallelStructureParams(
                    0.5, // Q
                    102, // Z1
                    101.5, // Z2
                    nullParams
                );
                nub = new ParallelStructure(prms, dbg);
                break;

            case CalculatorType.Dever:
                const deverPrms = new DeverParams(
                    0.5, // Q
                    102, // Z1
                    10,  // BR : largeur du cours d'eau
                    99,   // ZR : cote du lit du cours d'eau
                    nullParams
                );
                nub = new Dever(deverPrms, dbg);
                break;

            case CalculatorType.Cloisons:
                nub = new Cloisons(
                    new CloisonsParams(
                        1.5,      // Débit total (m3/s)
                        102,    // Cote de l'eau amont (m)
                        10,     // Longueur des bassins (m)
                        1,      // Largeur des bassins (m)
                        1,      // Profondeur moyenne (m)
                        0.5,     // Hauteur de chute (m)
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.MacroRugo:
                nub = new MacroRugo(
                    new MacrorugoParams(
                        12.5,   // ZF1
                        6,      // L
                        1,      // B
                        0.05,   // If
                        1.57,   // Q
                        0.6,    // h
                        0.01,   // Ks
                        0.13,   // C
                        0.4,    // D
                        0.4,    // k
                        1,     // Cd0
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.PabChute:
                nub = new PabChute(
                    new PabChuteParams(
                        2,      // Z1
                        0.5,    // Z2
                        1.5,     // DH
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.PabNombre:
                nub = new PabNombre(
                    new PabNombreParams(
                        6,     // DHT
                        10,    // N
                        0.6,    // DH
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Section:
                const nodeType: SectionType = params.getPropValue("nodeType");
                nub = this.createSection(nodeType, dbg, nullParams);
                break;

            case CalculatorType.Pab:
                nub = new Pab(
                    new PabParams(
                        1.5,    // Q
                        102,    // Z1
                        99,      // Z2
                        nullParams
                    ), undefined, dbg
                );
                break;

            case CalculatorType.CloisonAval: {
                prms = new CloisonsAvalParams(
                    0.5, // Q
                    102, // Z1
                    101.5, // Z2
                    0, // ZRAM
                    nullParams
                );
                nub = new CloisonAval(prms, dbg);
                break;
            }

            case CalculatorType.MacroRugoCompound:
                nub = new MacrorugoCompound(
                    new MacrorugoCompoundParams(
                        13.1,   // Z1
                        12.5,   // ZRL
                        12.5,   // ZRR
                        4,      // B
                        3,      // DH
                        0.05,   // If
                        0.01,   // Ks
                        0.13,   // C
                        0.4,    // D
                        0.4,    // k
                        1,     // Cd0
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Jet:
                nub = new Jet(
                    new JetParams(
                        5,      // V0
                        0.03,   // S
                        30,     // ZJ
                        29.2,   // ZW
                        28.5,   // ZF
                        3,       // D
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Grille:
                nub = new Grille(
                    new GrilleParams(
                        10,         // QMax
                        100,        // CRad
                        101.5,      // CEau
                        101.5,      // CSomGrille
                        2,          // B
                        72,         // Beta
                        90,         // Alpha
                        20,         // b
                        20,         // p
                        20,         // e
                        2,          // a
                        1.5,        // c
                        0.5,        // O
                        0.5,        // Ob
                        0.1,        // OEntH
                        4,           // cIncl
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Pente:
                nub = new Pente(
                    new PenteParams(
                        101,        // Z1
                        99.5,       // Z2
                        10,         // L
                        0.15,        // I
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Bief:
                nub = new Bief(
                    undefined,
                    new BiefParams(
                        100.25, // Z1 = cote de l'eau amont
                        100.4,  // Z2 = cote de l'eau aval
                        100.1,  // ZF1 = cote de fond amont
                        100,    // ZF2 = cote de fond aval
                        100,    // Long = Longueur du bief
                        5,      // Dx = Pas d'espace
                        nullParams
                    ),
                    dbg
                );
                break;

            case CalculatorType.Solveur:
                nub = new Solveur(
                    new SolveurParams(undefined, undefined, nullParams)
                );
                break;

            case CalculatorType.YAXB:
                nub = new YAXB(
                    new YAXBParams(
                        10,     // Y
                        2,      // A
                        3,      // X
                        4,       // B
                        nullParams
                    ),
                    dbg
                );
                break;

            case CalculatorType.Trigo:
                nub = new Trigo(
                    new TrigoParams(
                        0.985,  // Y
                        10,      // X
                        nullParams
                    ),
                    dbg
                );
                break;

            case CalculatorType.SPP:
                nub = new SPP(
                    new SPPParams(
                        1,  // Y
                        nullParams
                    ),
                    dbg
                );
                break;

            case CalculatorType.YAXN:
                nub = new YAXN(
                    new YAXNParams(
                        1,  // A
                        1,  // X
                        1,   // B
                        undefined,
                        nullParams
                    ),
                    dbg
                );
                break;

            case CalculatorType.ConcentrationBlocs:
                nub = new ConcentrationBlocs(
                    new ConcentrationBlocsParams(
                        0.128, // Concentration de blocs
                        5,     // Nombre de motifs
                        4.9,   // Largeur de la passe
                        0.35,   // Diamètre des plots
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Par:
                nub = new Par(
                    new ParParams(
                        0.25,   // Q
                        10,     // Z1
                        9,      // Z2
                        0.64,   // ha
                        0.2,    // S
                        0.4,    // P
                        0.6,    // L
                        0.1,    // a
                        1,      // N
                        1,       // M
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.ParSimulation:
                nub = new ParSimulation(
                    new ParSimulationParams(
                        0.25,       // Q
                        10,         // Z1
                        9,          // Z2
                        0.2,        // S
                        0.4,        // P
                        undefined,  // Nb
                        9.242,       // ZR1
                        undefined,  // ZD1
                        8.222,       // ZR2
                        undefined,  // ZD2
                        0.6,        // L
                        0.1,        // a
                        1,          // N
                        1,           // M
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.PreBarrage:
                nub = new PreBarrage(
                    new PreBarrageParams(
                        1,      // Q
                        101,    // Z1
                        100,     // Z2
                        nullParams
                    ), dbg
                );
                break;

            case CalculatorType.Espece:
                nub = new Espece(
                    // default params are those for SPECIES_1 (Salmons and trouts)
                    new EspeceParams(
                        0.35,   // DHMaxS
                        0.35,   // DHMaxP
                        0.3,    // BMin
                        1,      // PMinS
                        1,      // PMinP
                        2.5,    // LMinS
                        2.5,    // LMinP
                        0.3,    // HMin
                        0.4,    // YMin
                        2.5,    // VeMax
                        0.2,    // YMinSB
                        0.3,    // YMinPB
                        150,    // PVMaxPrec
                        200,     // PVMaxLim
                        nullParams
                    )
                );
                break;

            case CalculatorType.Verificateur:
                nub = new Verificateur();
                break;

            case CalculatorType.PbBassin:
                nub = new PbBassin(new PbBassinParams(
                    10,  // S
                    100,  // ZF
                    nullParams
                ), dbg);
                break;

            case CalculatorType.PbCloison:
                nub = new PbCloison(undefined, undefined, undefined, nullParams);
                break;

            case CalculatorType.PressureLoss:
                const plParams = new PressureLossParams(
                    3, // débit
                    1.2, // diamètre
                    0.6, /// perte de charge
                    100, // longueur du toyo
                    0, // Kloc Perte de charge singulière
                    nullParams
                );
                const lossType: PressureLossType = params.getPropValue("pressureLossType") ?? PressureLossType.LechaptCalmon;
                nub = new PressureLoss(plParams, undefined, dbg);
                nub.setPropValue("pressureLossType", lossType);
                break;

            case CalculatorType.PressureLossLaw:
                const lt = params.getPropValue("pressureLossType") ?? PressureLossType.LechaptCalmon;
                nub = this.createPressureLossLaw(lt, dbg, nullParams);
                nub.setPropValue("pressureLossType", lt);
                break;

            default:
                throw new Error(
                    `Session.createNub() : type de module '${CalculatorType[calcType]}' non pris en charge`
                );
        }

        // propagate properties
        try {
            nub.setProperties(params);
        } catch (e) {
            // loading Solveur properties when unserialising a session might fail because target
            // Nub / param do not exist yet; silent fail in this case, and Solveur.fixTargets()
            // might fix it later
            if (!(nub instanceof Solveur)) {
                throw e;
            }
        }

        return nub;
    }

    /**
     * Returns true if given uid is already used by a Nub in this session,
     * or a Structure nub inside one of them
     */
    public uidAlreadyUsed(uid: string, nubs: Nub[] = this._nubs): boolean {
        let alreadyUsed = false;
        for (const n of nubs) {
            if (n.uid === uid) {
                alreadyUsed = true;
                break;
            }
            if (!alreadyUsed) {
                alreadyUsed = this.uidAlreadyUsed(uid, n.getChildren());
            }
        }
        return alreadyUsed;
    }

    /**
     * Returns all Nubs depending on the given one (parameter or result),
     * without following links (1st level only)
     * @param uid UID of the Nub that underwent a change
     * @param symbol symbol of the parameter whose value change triggered this method; if specified,
     *      Nubs targetting this symbol will be considered dependent
     * @param includeValuesLinks if true, even Nubs targetting non-calculated non-modified parameters
     *      will be considered dependent @see jalhyd#98
     * @param includeOtherDependencies if true, will be considered dependent
     *      - Solveur Nubs having given Nub either as X or as Ytarget's parent Nub
     *      - Verificateur Nubs having given Nub either as selected Custom Species or Pass to check
     */
    public getDependingNubs(
        uid: string,
        symbol?: string,
        includeValuesLinks: boolean = false,
        includeOtherDependencies: boolean = false
    ): Nub[] {
        const dependingNubs: Nub[] = [];
        for (const n of this._nubs) {
            if (
                n.uid !== uid
                && n.resultDependsOnNub(uid, [], symbol, includeValuesLinks, includeOtherDependencies)
            ) {
                dependingNubs.push(n);
            }
        }
        return dependingNubs;
    }

    /**
     * Returns all Nubs depending on the result of at least one other Nub.
     * Used by Solveur to find available "target" nubs to calculate.
     */
    public getDownstreamNubs(): Nub[] {
        const downstreamNubs: Nub[] = [];
        for (const n of this._nubs) {
            if (n.getRequiredNubs().length > 0) {
                downstreamNubs.push(n);
            }
        }
        return downstreamNubs;
    }

    /**
     * Returns all Nubs that do not depend on the result of any other Nub
     * (includes single Nubs).
     */
    public getUpstreamNubs(): Nub[] {
        const upstreamNubs: Nub[] = [];
        for (const n of this._nubs) {
            if (n.getRequiredNubs().length === 0) {
                upstreamNubs.push(n);
            }
        }
        return upstreamNubs;
    }

    /**
     * Returns all upstream Nubs that have at least one declared extra result.
     * Used by Solveur to find available "target" nubs to calculate.
     */
    public getUpstreamNubsHavingExtraResults() {
        const unher: Nub[] = [];
        for (const n of this.getUpstreamNubs()) {
            if (n.resultsFamilies && Object.keys(n.resultsFamilies).length > 0) {
                unher.push(n);
            }
        }
        return unher;
    }

    /**
     * Returns a list of nub/symbol couples, that can be linked to the given
     * parameter, among all current nubs
     * @param p
     */
    public getLinkableValues(p: ParamDefinition): LinkedValue[] {
        let res: LinkedValue[] = [];
        for (const n of this._nubs) {
            const linkableValues = n.getLinkableValues(p);
            res = res.concat(linkableValues);
        }
        /* console.log("LINKABLE VALUES", res.map((lv) => {
            return `${lv.nub.uid}(${lv.nub.constructor.name})/${lv.symbol}`;
        })); */
        return res;
    }

    /**
     * @returns true if parameter is a link target in any session nub
     */
    public isParameterLinkTarget(p: ParamDefinition): boolean {
        for (const n of this._nubs) {
            if (n.dependsOnParameter(p)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Crée un Nub de type Section
     * @param nt SectionType
     * @param dbg activer débogage
     */
    public createSection(nt: SectionType, dbg: boolean = false, nullParams: boolean = false): acSection {
        switch (nt) {
            case SectionType.SectionTrapeze: {
                const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                    0.56, // fruit
                    0.8, // tirant d'eau
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    1, // YB= hauteur de berge
                    nullParams
                );

                return new cSnTrapez(prms, dbg);
            }

            case SectionType.SectionRectangle: {
                const prms = new ParamsSectionRectang(0.8, // tirant d'eau
                    2.5, // largeur de fond
                    40, //  Ks=Strickler
                    1.2, // Q=Débit
                    0.001, // If=pente du fond
                    1, // YB=hauteur de berge
                    nullParams
                );
                return new cSnRectang(prms, dbg);
            }

            case SectionType.SectionCercle: {
                const prms = new ParamsSectionCirc(2, // diamètre
                    0.8, // tirant d'eau
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    1, // YB= hauteur de berge
                    nullParams
                );
                return new cSnCirc(prms, dbg);
            }

            case SectionType.SectionPuissance: {
                const prms = new ParamsSectionPuiss(0.5, // coefficient
                    0.8, // tirant d'eau
                    4, // largeur de berge
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    1, // YB= hauteur de berge
                    nullParams
                );
                return new cSnPuiss(prms, dbg);
            }

            default:
                throw new Error(`type de section ${SectionType[nt]} non pris en charge`);
        }
    }

    /**
     * Crée un Nub de type loi de perte de charge
     */
    public createPressureLossLaw(plt: PressureLossType, dbg: boolean = false, nullParams: boolean = false): PressureLossLaw {
        switch (plt) {
            case PressureLossType.LechaptCalmon:
                const prms = new PL_LechaptCalmonParams(
                    1.863, // paramètre L du matériau
                    2, // paramètre M du matériau
                    5.33, // paramètre N du matériau
                    nullParams
                );
                return new PL_LechaptCalmon(prms, dbg);

            case PressureLossType.Strickler:
                const sp = new PL_StricklerParams(1, nullParams);
                return new PL_Strickler(sp);

            default:
                throw new Error(`type de perte de charge ${PressureLossType[plt]} non pris en charge`);
        }
    }

    /**
     * Creates a Nub from an object representation and adds it to the current session; returns
     * a pointer to the Nub and its JSON metadata
     * @param obj object representation of a single Nub
     * @param register if false, new Nub will just be returned and won't be registered into the session
     */
    private createNubFromObjectRepresentation(obj: any, register: boolean = true)
        : { nub: Nub, meta: any, hasErrors: boolean } {

        // return value;
        const nubPointer: { nub: Nub, meta: any, hasErrors: boolean } = {
            nub: undefined,
            meta: undefined,
            hasErrors: false
        };
        // get upward compatible calculator
        obj = this.compatibleCalculator(obj);
        // decode properties
        const props = Props.invertEnumKeysAndValuesInProperties(obj.props, true);
        // create the Nub
        let newNub;
        if (register) {
            newNub = this.createSessionNub(new Props(props));
        } else {
            newNub = this.createNub(new Props(props));
        }
        // try to keep the original ID
        if (!this.uidAlreadyUsed(obj.uid)) {
            newNub.setUid(obj.uid);
        }
        const res = newNub.loadObjectRepresentation(obj);
        nubPointer.nub = newNub;
        // forward errors
        if (res.hasErrors) {
            nubPointer.hasErrors = true;
        }
        // add metadata (used by GUI, for ex.)
        if (obj.meta) {
            nubPointer.meta = obj.meta;
        }
        return nubPointer;
    }

    /**
     * Asks all loaded Nubs to relink any parameter that has a wrong target
     */
    private fixLinks(serialised: string, uids?: string[]): { hasErrors: boolean } {
        // return value
        const res = {
            hasErrors: false
        };
        const data = JSON.parse(serialised);
        if (data.session && Array.isArray(data.session)) {
            // find each corresponding Nub in the session
            data.session.forEach((e: any) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nub = this.findNubByUid(e.uid);
                    // find linked parameters
                    const ret = nub.fixLinks(e);
                    // forwardErrors
                    if (ret.hasErrors) {
                        res.hasErrors = true;
                    }
                }
            });
        }
        return res;
    }

    /**
     * Asks every loaded Solveur to reconnect to its nubToCalculate / searchedParameter
     */
    private fixSolveurs(serialised: string, uids?: string[]): { hasErrors: boolean } {
        // return value
        const res = {
            hasErrors: false
        };
        const data = JSON.parse(serialised);
        if (data.session && Array.isArray(data.session)) {
            // find each corresponding Nub in the session
            data.session.forEach((e: any) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nub = this.findNubByUid(e.uid);
                    if (nub instanceof Solveur) {
                        // find targetted nubToCalculate / searchedParam
                        const ret = nub.fixTargets(e);
                        // forwardErrors
                        if (ret.hasErrors) {
                            res.hasErrors = true;
                        }
                    }
                }
            });
        }
        return res;
    }
}
