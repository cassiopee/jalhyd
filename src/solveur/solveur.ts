import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability, ParamDefinition } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";
import { Session } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { ResultElement } from "../internal_modules";
import { SolveurParams } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

export class Solveur extends Nub implements Observer {

    /**
     * Finds all parameters whose value can be searched for by dichotomy, for
     * a given value of calculated param of given Nub
     * @param nub Nub calculated by Solveur
     * @param addSelf if true, will also return parameters of the current Nub
     */
    public static getDependingNubsSearchableParams(nub: Nub, addSelf: boolean = false): ParamDefinition[] {
        const searchableParams: ParamDefinition[] = [];
        if (nub !== undefined) {
            const upstreamNubs = nub.getRequiredNubsDeep();
            if (addSelf) {
                upstreamNubs.push(nub);
            }
            for (const un of upstreamNubs) {
                for (const p of un.parameterIterator) {
                    // only visible, non-varying, non-calculated parameters can be looked for
                    if (p.visible && p.valueMode === ParamValueMode.SINGLE) {
                        searchableParams.push(p);
                    }
                }
            }
        }
        return searchableParams;
    }

    constructor(prms: SolveurParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Solveur);
        this._props.addObserver(this);
        this.prms.addObserver(this);
        // UID of Session Nub to calculate, the result of the which must be this.prms.Ytarget.singleValue
        this.nubToCalculate = undefined;
        // symbol of extra result targetted on the Nub to calculate; if specified, then this.prms.Ytarget.singleValue
        // will be the expected value not for nubToCalculate.vCalc, but for nubToCalculate.values[this.targettedResult]
        this.targettedResult = undefined;
        // UID of Session Nub / symbol of source parameter to vary (the "searched" parameter)
        this.searchedParameter = undefined;
        // calculated param should always be pseudo-parameter "X" (the one whose value we're looking for)
        this._defaultCalculatedParam = prms.X;
        this.resetDefaultCalculatedParam();
    }

    public get prms(): SolveurParams {
        return this._prms as SolveurParams;
    }

    /** finds the Nub to calculate by its UID */
    public get nubToCalculate(): Nub {
        let nub: Nub;
        const nubUID: string = this._props.getPropValue("nubToCalculate");
        if (nubUID !== undefined && nubUID !== "") {
            nub = Session.getInstance().findNubByUid(nubUID);
            if (nub === undefined) {
                // silent fail
                // throw new Error(`Solveur.get nubToCalculate(): cannot find Nub ${nubUID} in session`);
            }
        }
        return nub;
    }

    /** defines the Nub to calculate by setting property "nubToCalculate" to the UID of the given Nub */
    public set nubToCalculate(n: Nub) {
        let uid = ""; // empty value
        if (n !== undefined) {
            uid = n.uid;
        }
        this.setPropValue("nubToCalculate", uid);
    }

    public get targettedResult(): string {
        return this._props.getPropValue("targettedResult")
    }

    public set targettedResult(symbol: string) {
        this._props.setPropValue("targettedResult", symbol);
    }

    /** finds the source parameter whose value we're looking for, by its Nub UID / symbol */
    public get searchedParameter(): ParamDefinition {
        let p: ParamDefinition;
        const nubUIDAndSymbol: string = this._props.getPropValue("searchedParameter");
        if (nubUIDAndSymbol !== undefined && nubUIDAndSymbol !== "") {
            const slashPos = nubUIDAndSymbol.indexOf("/");
            const nubUID = nubUIDAndSymbol.substring(0, slashPos);
            const paramSymbol = nubUIDAndSymbol.substring(slashPos + 1);
            // console.log("(i) searched param Nub UID / symbol :", nubUID, paramSymbol);
            if (nubUID) {
                const nub: Nub = Session.getInstance().findNubByUid(nubUID);
                if (nub !== undefined) {
                    if (paramSymbol) {
                        p = nub.getParameter(paramSymbol);
                        if (p === undefined) {
                            throw new Error(
                                `Solveur.get searchedParameter(): cannot find Parameter ${paramSymbol} in Nub`
                            );
                        }
                    }
                } else {
                    // silent fail
                    // throw new Error(`Solveur.get searchedParameter(): cannot find Nub ${nubUID} in session`);
                }
            }
        }
        return p;
    }

    /**
     * defines the searched parameter by setting property "searchedParameter" to the UID of the
     * parameter's Nub / the parameter's symbol
     */
    public set searchedParameter(p: ParamDefinition) {
        let sp = ""; // empty value
        if (p !== undefined) {
            sp = p.nubUid + "/" + p.symbol;
        }
        this.setPropValue("searchedParameter", sp);
    }

    /**
     * Looks for potential errors before calling Nub.Calc()
     */
    public Calc(sVarCalc?: string, rInit?: number): Result {
        const r: Result = new Result(new ResultElement());
        if (this.nubToCalculate.resultHasMultipleValues()) {
            r.resultElement.addMessage(new Message(MessageCode.ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED));
            this.currentResultElement = r;
            return this.result;
        }
        return super.Calc(sVarCalc, rInit);
    }

    public CalcSerie(rInit?: number): Result {
        // affect initial value of X for Dichotomie search
        this.prms.X.singleValue = this.prms.Xinit.currentValue;
        const res = super.CalcSerie(rInit);

        res.updateSourceNub(this);
        res.removeExtraResults();
        // clodo trick: realculate nubToCalculate() dissociates results @see jalhyd#157
        this.nubToCalculate.CalcSerie();

        return res;
    }

    /**
     * Calculates the target "downstream" Nub, that will trigger calculation of
     * all linked "upstream" Nubs, including the one that contains the variating
     * parameter X
     * @param sVarCalc should always be pseudo-parameter "Y"
     */
    public Equation(sVarCalc: string): Result {
        if (sVarCalc !== "Y") {
            throw new Error(`Solveur: calculated param should always be Y (found ${sVarCalc})`);
        }
        // set the Y value we have to obtain
        this.prms.Y.v = this.prms.Ytarget.v;
        // set the current value of X, determined by Dichotomie, on the upstream Nub
        this.searchedParameter.singleValue = this.prms.X.v;
        // calculate Nubs chain
        const res = this.nubToCalculate.CalcSerie();
        // if targetting an extraresult, replace vCalc with targetted extraresult
        if (this.targettedResult) { // might be undefined, or "" (when unserializing)
            res.vCalc = res.values[this.targettedResult];
        }
        return res;
    }

    public getFirstAnalyticalParameter() {
        // always use pseudo-parameter Y for Dichotomie iterations;
        // always update target value from input parameter
        this.prms.Y.v = this.prms.Ytarget.v;
        return this.prms.Y;
    }

    // interface Observer
    public update(sender: any, data: any): void {
        if (data.action === "propertyChange") {
            if (data.name === "nubToCalculate" || data.name === "searchedParameter" || data.name === "targettedResult") {
                const n = this.nubToCalculate;
                const t = this.targettedResult;
                const p = this.searchedParameter;
                if (n !== undefined && p !== undefined) {
                    if (n !== p.parentNub && !n.dependsOnNubResult(p.parentNub)) {
                        throw new Error(
                            "Solveur.update(): Nub to calculate is not linked to result of searchedParameter parent Nub"
                        );
                    }
                }
                if (data.name === "nubToCalculate") {
                    if (n !== undefined) {
                        if (n.resultHasMultipleValues()) {
                            throw new Error("Solveur.update(): Nub to calculate must not have multiple values");
                        }
                        // reset targetted result
                        this.targettedResult = undefined;
                    }
                }
                if (data.name === "searchedParameter") {
                    if (p !== undefined) {
                        if (p.valueMode !== ParamValueMode.SINGLE) {
                            throw new Error("Solveur.update(): searched parameter X must be in SINGLE mode");
                        }
                        // update pseudo-parameter X
                        this.prms.setX(p);
                    }
                }
                if (data.name === "targettedResult") {
                    if (t !== undefined && t !== "") {
                        if (
                            n.resultsFamilies
                            && !Object.keys(n.resultsFamilies).includes(t)
                        ) {
                            throw new Error("Solveur.update(): targetted result T is not a declared extra result of Nub to calculate");
                        }
                    } else {
                        // empty value − is nubToCalc single ? If so, invalidate searchedParam
                        if (Solveur.getDependingNubsSearchableParams(n).length === 0) {
                            this.searchedParameter = undefined;
                        }
                    }
                }
            }
        }
        if (data.action === "XUpdated") {
            this._defaultCalculatedParam = this.prms.X;
            this.resetDefaultCalculatedParam();
        }
    }

    /**
     * Once session is loaded, run a second pass on targetted
     * objects if they couldn't be set before
     */
    public fixTargets(obj: any): { hasErrors: boolean } {
        // return value
        const ret = {
            hasErrors: false
        };
        try {
            // do not use setters, to allow setting directly the string UIDs
            this.setPropValue("nubToCalculate", obj.props.nubToCalculate);
            this.setPropValue("targettedResult", obj.props.targettedResult);
            this.setPropValue("searchedParameter", obj.props.searchedParameter);
        } catch (e) {
            ret.hasErrors = true;
        }
        return ret;
    }

    protected setParametersCalculability(): void {
        if (this.prms.X !== undefined) {
            this.prms.X.calculability = ParamCalculability.DICHO;
        }
        this.prms.Xinit.calculability = ParamCalculability.FIXED;
        this.prms.Ytarget.calculability = ParamCalculability.FREE;
        this.prms.Y.calculability = ParamCalculability.FREE;
    }

    protected Solve(sVarCalc: string, rInit: number): Result {
        const res = super.Solve(sVarCalc, rInit);
        // if Y has log about a failure in calc chain, copy it to
        // a resultElement so that Nub.Calc() does not complain
        if (res.resultElements.length === 0) {
            const re = new ResultElement();
            re.log.addLog(res.globalLog);
            res.addResultElement(re);
        }
        return res;
    }
}
