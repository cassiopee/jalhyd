import { ParamDefinition } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";
import { IObservable, Observable, Observer } from "../internal_modules";

/**
 * Y = f(X)
 * On cherche la valeur de X pour laquelle Y vaut Ytarget
 */
export class SolveurParams extends ParamsEquation implements IObservable {

    /** X : paramètre à faire varier par Dichotomie, dont la valeur sera reportée dans paramètre du Nub amont */
    private _X: ParamDefinition;

    /** Y : pseudo-paramètre à calculer, pour la dichotomie (déclenchera en fait le calcul du Nub aval) */
    private _Y: ParamDefinition;

    /** Xinit : valeur initiale de X */
    private _Xinit: ParamDefinition;

    /** Ytarget : valeur recherchée pour Y */
    private _Ytarget: ParamDefinition;

    /** implémentation de IObservable par délégation */
    private _observable: Observable;

    /**
     * Nub to calculate, targetted result and searched parameter must be set through
     * Solveur properties, or .nubToCalculate(), .targettedResult() and .searchedParameter() setters
     * @param rYtarget value we want to obtain for Y
     * @param rXinit initial value f searched parameter; will be overwritten as soon
     *               as Solveur's property "searchedParam" is updated
     */
    constructor(rYtarget?: number, rXinit?: number, nullParams: boolean = false) {
        super();
        this._observable = new Observable();
        this._Xinit = new ParamDefinition(this, "Xinit", ParamDomainValue.ANY, undefined, rXinit, undefined, undefined, nullParams);
        this._Ytarget = new ParamDefinition(this, "Ytarget", ParamDomainValue.ANY, undefined, rYtarget, undefined, undefined, nullParams);
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, undefined, undefined, undefined, false, nullParams);

        this.addParamDefinition(this._Xinit);
        this.addParamDefinition(this._Ytarget);
        this.addParamDefinition(this._Y);
    }

    public get X(): ParamDefinition {
        return this._X;
    }

    public setX(X: ParamDefinition) {
        // copy of real X, to be used by Dichotomie() that will look for its value
        this._X = new ParamDefinition(this, "X", X.domain, X.unit, X.singleValue, undefined, false);
        // Update Xinit
        if (this._Xinit !== undefined) {
            this._Xinit.singleValue = X.singleValue;
            this._Xinit.setDomain(X.domain);
            this._Xinit.setUnit(X.unit);
        }
        // replace pointer for parameters iterator
        this.addParamDefinition(this._X, true);
        // notify Solveur so that it updates calculatedParam pointer
        this.notifyObservers({
            action: "XUpdated"
        }, this);
    }

    public get Y(): ParamDefinition {
        return this._Y;
    }

    public get Xinit(): ParamDefinition {
        return this._Xinit;
    }

    public get Ytarget(): ParamDefinition {
        return this._Ytarget;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }

}
