/** materials for Lechapt-Calmon */
export enum LCMaterial {
    UnlinedCastIronCoarseConcrete,
    CastSteelOrUncoatedCoarseConcrete,
    CastSteelOrCementCoating,
    CastIronOrSteelCoatingBitumen,
    RolledSteelSmoothConcrete,
    CastIronOrSteelCoatingCentrifuged,
    PVCPolyethylene,
    HydraulicallySmoothPipe005D02,
    HydraulicallySmoothPipe025D1
}
