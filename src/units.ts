import { CalculatorType } from "./compute-node";
import { Cloisons, ConcentrationBlocs, Dever, Grille, Jet, MacroRugo, PabNombre, Par, ParSimulation, RegimeUniforme, MacrorugoCompound } from "./index";
import { PressureLoss, RugoFondMultiple } from "./internal_modules";

/**
 * correspondance type de calculette - classe de Nub
 */
const nubClasses = new Map<CalculatorType, any>([
    [CalculatorType.Grille, Grille],
    [CalculatorType.ConcentrationBlocs, ConcentrationBlocs],
    [CalculatorType.Jet, Jet],
    [CalculatorType.MacroRugo, MacroRugo],
    [CalculatorType.RegimeUniforme, RegimeUniforme],
    [CalculatorType.Cloisons, Cloisons],
    [CalculatorType.PabNombre, PabNombre],
    [CalculatorType.Par, Par],
    [CalculatorType.ParSimulation, ParSimulation],
    [CalculatorType.Dever, Dever],
    [CalculatorType.PressureLoss, PressureLoss],
    [CalculatorType.RugoFondMultiple, RugoFondMultiple],
    [CalculatorType.MacroRugoCompound, MacrorugoCompound],
]);

/**
 * détermine l'unité d'un paramètre de résultat
 * @param nubType Nub contenant le paramètre de résultat
 * @param symbol symbole du paramètre
 * @returns unité du paramètre
 */
export function getNubResultUnit(nubType: CalculatorType, symbol: string) {
    const cl = nubClasses.get(nubType);
    if (cl !== undefined && "resultsUnits" in cl) {
        const f = cl.resultsUnits;
        const units = f();
        return units[symbol];
    }
    return undefined;
}
