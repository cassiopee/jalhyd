import { ParamDefinition } from "./internal_modules";
import { ParamValues } from "./internal_modules";
import { INumberIterator } from "./internal_modules";

export interface VariatedDetails {
    param: ParamDefinition;
    values: ParamValues;
    // copy of iterator, for linked params: calling initValuesIterator() overwrites this._iterator, so
    // that calling .next() or .hasNext() on linked variating params returns a pointer to the same
    // iterator @see bug #222
    iterator?: INumberIterator
};
