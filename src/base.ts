/**
 * Gestion des messages de debogage dans la console
 * @note Implémenter cette interface pour toutes les classes à debugguer
 * Dans le constructeur, utiliser super(true) pour debugger la classe, super(false) sinon.
 */
export interface IDebug {

    readonly DBG: boolean;

    debug(s: any): void;
}

export class Debug {
    /**
     * @param _DBG Flag de débuggage
     */
    constructor(private _DBG: boolean) { }

    /**
     * Affiche un message dans la console si le flag this.DBG est à true
     * @param s Message à afficher dans la console
     */
    public debug(...args: any[]) {
        // tslint:disable-next-line:no-console
        if (this._DBG) {
            // tslint:disable-next-line:no-console
            console.log(...args);
        }
    }

    get DBG(): boolean {
        return this._DBG;
    }

    set DBG(d: boolean) {
        this._DBG = d;
    }
}

/**
 * Méthode simulant l'opérateur booléen xor
 * @see http://www.howtocreate.co.uk/xor.html
 */
export function XOR(a: boolean, b: boolean): boolean {
    return (a || b) && !(a && b);
}

/**
 * Méthode simulant l'opérateur booléen identité (vrai si a=b)
 */
export function BoolIdentity(a: boolean, b: boolean): boolean {
    return (a && b) || (!a && !b);
}

/**
 * arrondi d'un nombre avec une précision donnée
 * @param val nombre à arrondir
 * @param prec nombre de chiffres
 */
export function round(val: number, prec: number) {
    const m = Math.pow(10, prec || 0);
    return Math.round(val * m) / m;
}

export function isNumeric(s: any): boolean {
    if (s !== undefined && s !== null) {
        if (typeof s === "string") {
            const trimmed = s.trim();
            return trimmed !== "" && !isNaN(Number(trimmed));
        } else if (typeof s === "number") {
            return !isNaN(s);
        }
    }
    return false;
}

/**
 * Retourne true si a et b sont égaux à e près (en gros)
 */
export function isEqual(a: number, b: number, e: number = 1E-7): boolean {
    if (a === 0 && b === 0) {
        return true;
    }
    if (b === 0) { // ne pas diviser par 0
        [a, b] = [b, a];
    }
    if (a === 0) { // éviter d'avoir un quotient toujours nul
        return (Math.abs(b) < e);
    }
    // cas général
    return (Math.abs((a / b) - 1) < e);
}

/** Retourne true si a < b à e près */
export function isLowerThan(a: number, b: number, e: number = 1E-7): boolean {
    return (b - a) > e; // enfin (a + e) < b quoi
}

/** Retourne true si a > b à e près */
export function isGreaterThan(a: number, b: number, e: number = 1E-7): boolean {
    return (a - b) > e;
}

/**
 * Division entière de a par b, plus modulo, avec une tolérance e
 * pour gérer le cas des nombres réels
 */
export function floatDivAndMod(a: number, b: number, e: number = 1E-7): { q: number, r: number } {
    let q = Math.floor(a / b);
    let r = a % b;
    // si le modulo est bon à un pouïème, pas de reste
    if (isEqual(r, b, e)) {
        r = 0;
        // si la division entière n'est pas tombée juste, +1 au quotient
        if (q !== (a / b)) {
            q++;
        }
    }
    // si le modulo est nul à un pouïème, il est nul
    if (isEqual(r, 0, e)) {
        r = 0;
    }
    return { q, r };
}

/**
 * Formats (rounds) the given number (or the value of the given parameter) with the
 * number of decimals specified in app preferences; if given number is too low and
 * more decimals would be needed, keep as much significative digits as the number of
 * decimals specified in app preferences, except potential trailing zeroes;
 * returns "-" to point out an error if value is not a number or is undefined
 *
 * ex. with 3 decimals:
 * 1             => 1.000
 * 65431         => 65431.000
 * 0.002         => 0.002
 * 0.00004521684 => 0.0000452
 * 0.000001      => 0.000001
 * 5.00004521684 => 5.000
 * 5.000001      => 5.000
 */
export function formattedValue(value: number, nDigits: number) {
    // bulletproof
    nDigits = Math.max(nDigits, 1);
    nDigits = Math.min(nDigits, 100);
    if (typeof value !== "number" || value === undefined) {
        return "-";
    }
    const minRenderableNumber = Number("1E-" + nDigits);
    // if required precision is too low, avoid rendering only zeroes
    if (value < minRenderableNumber) {
        return String(Number(value.toPrecision(nDigits))); // double casting avoids trailing zeroes
    } else {
        return value.toFixed(nDigits);
    }
}

export function capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}
