import { CalculatorType, ComputeNode } from "../internal_modules";
import { Session } from "../internal_modules";
import { LinkedValue } from "../internal_modules";
import { Nub } from "../internal_modules";
import { acSection } from "../internal_modules";
import { Interval } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { IObservable, Observable, Observer } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { INamedIterableValues, INumberIterator } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";
import { ParamValues } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Calculabilité du paramètre
 */
export enum ParamCalculability {
    /** parameter may have any value, may not vary, may not be calculated */
    FIXED,
    /** parameter may have any value, may vary, may not be calculated */
    FREE,
    /** parameter may have any value, may vary, may be calculated analytically */
    EQUATION,
    /** parameter may have any value, may vary, may be calculated using numerical method */
    DICHO
}

/**
 * Parameter family: defines linkability with other parameters/results
 */
export enum ParamFamily {
    ANY, // peut être lié à n'importe quel paramètre
    LENGTHS, // longueur
    WIDTHS, // largeur
    SLOPES, // pente
    HEIGHTS, // profondeur, tirant d'eau
    BASINFALLS, // chute entre bassins
    TOTALFALLS, // chute totale
    ELEVATIONS, // cote
    VOLUMES,
    FLOWS, // débit
    DIAMETERS,
    SPEEDS, // vitesses, seulement des résultats
    STRICKLERS,
    BLOCKCONCENTRATION // concentrations de blocs
}

/**
 * Strategy to apply when multiple parameters are variating, and
 * values series have to be extended for sizes to match
 */
export enum ExtensionStrategy {
    REPEAT_LAST,    // repeat last value as many times as needed
    RECYCLE         // repeat the whole series from the beginning
    // autres propositions :
    // PAD_LEFT     // pad with zeroes at the beginning ?
    // PAD_RIGHT    // pad with zeroes at the end ?
    // INTERPOLATE  // insert regular steps between first and last value
}

/**
 * Paramètre avec symbole, famille, domaine de définition, calculabilité,
 * pointant éventuellement vers un autre paramètre / résultat
 */
export class ParamDefinition implements INamedIterableValues, IObservable {

    /** le paramètre doit-il être exposé (par ex: affiché par l'interface graphique) ? */
    public visible: boolean = true;

    /** sandbox value used during calculation */
    public v: number;

    /** value used to initialize "DICHO" calculation, never supposed to be undefined */
    private _initValue: number;

    /** extension strategy, when multiple parameters vary */
    private _extensionStrategy: ExtensionStrategy;

    /** symbole */
    private _symbol: string;

    /** unité */
    private _unit: string;

    /** related parameters groups; indirectly gives access to the Nub using this parameter */
    private _parent: ParamsEquation;

    /** domaine de définition */
    private _domain: ParamDomain;

    /** calculabilité */
    private _calc: ParamCalculability;

    /** valeur(s) prise(s) */
    private _paramValues: ParamValues;

    /** parameters family, for linking */
    private _family: ParamFamily;

    /** pointer to another Parameter / Result, in case of LINK mode */
    private _referencedValue: LinkedValue;

    /** implémentation par délégation de IObservable */
    private _observable: Observable;

    /**
     * @param nullParams true if provided value must be ignored (@see nghyd/enableEmptyFieldsOnFormInit)
     */
    constructor(parent: ParamsEquation, symb: string, d: ParamDomain | ParamDomainValue, unit?: string,
        val?: number, family?: ParamFamily, visible: boolean = true, nullParams = false
    ) {
        this._parent = parent;
        this._symbol = symb;
        this._unit = unit;
        this._observable = new Observable();
        this._paramValues = new ParamValues();

        // set single value and copy it to sandbox value and initValue
        if (!nullParams) {
            this._paramValues.singleValue = val;
            this.v = val;
        }
        this.initValue = val;

        // if (!nullParams && this._paramValues.singleValue === undefined) {
        //     throw new Error("a value must be provided for " + symb + " parameter");
        // }

        this._calc = ParamCalculability.FREE;
        this._family = family;
        this.visible = visible;
        this.valueMode = ParamValueMode.SINGLE;
        this.extensionStrategy = ExtensionStrategy.REPEAT_LAST;

        this.setDomain(d);

        this.checkValueAgainstDomain(val);
    }

    /**
     * set parent a-posteriori; used by nghyd when populating forms
     */
    // public set parent(parent: ParamsEquation) {
    //     this._parent = parent;
    // }

    /**
     * identifiant unique de la forme
     *   (uid du JalhydObject (en général un Nub) + "_" + symbole du paramètre)
     * ex: 123_Q
     */
    public get uid(): string {
        return this.nubUid + "_" + this._symbol;
    }

    public get unit(): string {
        return this._unit;
    }

    public setUnit(u: string) {
        this._unit = u;
    }

    public get initValue(): number {
        return this._initValue;
    }

    /**
     * Make sure that this value is never undefined, null or NaN, so that
     * there is always an initial value for "DICHO" calculations, even
     * when param value was set to undefined before
     */
    public set initValue(v: number) {
        if (v !== undefined && !isNaN(v) && v !== null) {
            this._initValue = v;
        } else {
            if (this._initValue === undefined || isNaN(this._initValue) || this._initValue === null) {
                this._initValue = Math.random();
            }
        }
    }

    /**
     * @see getParentComputeNode()
     */
    public get parentComputeNode(): ComputeNode {
        return this.getParentComputeNode();
    }

    /**
     * pointer to the ComputeNode (usually Nub) that uses the ParamsEquation that
     * uses this ParamDefinition; for Section params, if returnEnclosingNubForSections
     * is true, this will return the Nub enclosing the Section, otherwise the section itself
     */
    public getParentComputeNode(returnEnclosingNubForSections = true): ComputeNode {
        let parentCN: ComputeNode;
        if (this._parent) {
            // ComputeNode utilisant le ParamsEquation
            parentCN = this._parent.parent;
            // Section: go up to enclosing Nub if any (might not have any yet when unserializing sessions)
            if (parentCN instanceof acSection && returnEnclosingNubForSections && parentCN.parent) {
                parentCN = parentCN.parent;
            }
        } else {
            // fail silently (the story of my life)
            // throw new Error("ParamDefinition.parentComputeNode : parameter has no parent !");
        }
        return parentCN;
    }

    /**
     * Returns the parent compute node as Nub if it is a Nub; returns
     * undefined otherwise
     */
    public get parentNub(): Nub {
        if (this.parentComputeNode && this.parentComputeNode instanceof Nub) {
            return this.parentComputeNode as Nub;
        } else {
            return undefined;
        }
    }

    /**
     * Identifiant unique du Nub parent
     */
    public get nubUid(): string {
        return this.parentComputeNode.uid;
    }

    /**
     * Type de module du Nub parent
     */
    public get nubCalcType(): CalculatorType {
        let parentCalcType: CalculatorType;
        if (this.parentComputeNode instanceof Nub) {
            parentCalcType = this.parentComputeNode.calcType;
        } else {
            throw new Error("ParamDefinition.nubCalcType : parameter has no parent !");
        }
        return parentCalcType;
    }

    public get symbol(): string {
        return this._symbol;
    }

    public setDomain(d: any) {
        if (d instanceof ParamDomain) {
            this._domain = d;
        } else {
            this._domain = new ParamDomain(d as ParamDomainValue);
        }
    }

    public get domain(): ParamDomain {
        return this._domain;
    }

    public get interval(): Interval {
        return this._domain.interval;
    }

    public get extensionStrategy(): ExtensionStrategy {
        return this._extensionStrategy;
    }

    public set extensionStrategy(strategy: ExtensionStrategy) {
        this._extensionStrategy = strategy;
        // synchronise with underlying local ParamValues (for iterator), except for links
        if ([ParamValueMode.SINGLE, ParamValueMode.MINMAX, ParamValueMode.LISTE].includes(this.valueMode)) {
            if (this._paramValues) {
                this._paramValues.extensionStrategy = strategy;
            }
        }
    }

    public get valueMode(): ParamValueMode {
        return this._paramValues.valueMode;
    }

    /**
     * Easy setter that propagates the change to other parameters
     * (default behaviour); use setValueMode(..., false) to prevent
     * possible infinite loops
     */
    public set valueMode(newMode: ParamValueMode) {
        this.setValueMode(newMode);
    }

    /**
     * Sets the value mode and asks the Nub to ensure there is only one parameter
     * in CALC mode
     *
     * If propagateToCalculatedParam is true, any param that goes from CALC mode
     * to any other mode will trigger a reset of the default calculated param at
     * Nub level
     *
     * Propagates modes other than CALC and LINK to the underlying
     * ParamValues (local instance)
     */
    public setValueMode(newMode: ParamValueMode, propagateToCalculatedParam: boolean = true) {
        const oldMode = this.valueMode;

        // ignore idempotent calls
        if (oldMode === newMode) {
            return;
        }

        this.propagateValueMode(oldMode, newMode, propagateToCalculatedParam);

        // set new mode
        this._paramValues.valueMode = newMode;
    }

    private propagateValueMode(oldMode: ParamValueMode, newMode: ParamValueMode, propagateToCalculatedParam: boolean = true) {
        if (oldMode === ParamValueMode.CALCUL) {
            if (propagateToCalculatedParam) {
                const parentNode = this.parentComputeNode;
                // Set default calculated parameter, only if previous CALC param was
                // manually set to something else than CALC
                if (parentNode instanceof Nub) {
                    parentNode.resetDefaultCalculatedParam(this);
                }
            }
        } else if (newMode === ParamValueMode.CALCUL) {
            const parentNode = this.parentComputeNode;
            // set old CALC param to SINGLE mode
            if (parentNode instanceof Nub) {
                parentNode.unsetCalculatedParam(this);
            }
        }
    }

    /**
     * Returns true if this parameter is the calculatedParam of
     * its parent Nub
     */
    public get isCalculated(): boolean {
        if (this.parentNub) {
            return (this.parentNub.calculatedParam === this); // should be the same object
        }
        return false;
    }

    /**
     * Sets this parameter as the one to be calculated
     */
    public setCalculated() {
        if (this.parentNub) {
            this.parentNub.calculatedParam = this;
        }
    }

    /**
     * Current values set associated to this parameter; in LINK mode, points
     * to a remote set of values.
     */
    public get paramValues(): ParamValues {
        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            return this._referencedValue.getParamValues();
        } else {
            return this._paramValues;
        }
    }

    public get referencedValue() {
        return this._referencedValue;
    }

    public get family() {
        return this._family;
    }

    get calculability(): ParamCalculability {
        if (this._calc === undefined) {
            const e = new Message(MessageCode.ERROR_PARAMDEF_CALC_UNDEFINED);
            e.extraVar.symbol = this.symbol;
            throw e;
        }

        return this._calc;
    }

    set calculability(c: ParamCalculability) {
        this._calc = c;
    }

    /**
     * Returns true if current value (not singleValue !) is defined
     */
    public get hasCurrentValue(): boolean {
        return (this.v !== undefined);
    }

    /**
     * Returns true if held value (not currentValue !) is defined,
     * depending on the value mode
     */
    public get isDefined(): boolean {
        let defined = false;
        switch (this.valueMode) {
            case ParamValueMode.SINGLE:
                defined = (this.singleValue !== undefined);
                break;
            case ParamValueMode.MINMAX:
                defined = (
                    this.paramValues.min !== undefined
                    && this.paramValues.max !== undefined
                    && this.paramValues.step !== undefined
                );
                break;
            case ParamValueMode.LISTE:
                defined = (this.valueList && this.valueList.length > 0 && this.valueList[0] !== undefined);
                break;
            case ParamValueMode.CALCUL:
                if (this.parentNub && this.parentNub.result && this.parentNub.result.resultElements.length > 0) {
                    const res = this.parentNub.result;
                    defined = (res.vCalc !== undefined);
                }
                break;
            case ParamValueMode.LINK:
                defined = this.referencedValue.isDefined();
        }
        return defined;
    }

    // -- values getters / setters; in LINK mode, reads / writes from / to the target values

    public get singleValue(): number {
        this.checkValueMode([ParamValueMode.SINGLE, ParamValueMode.CALCUL, ParamValueMode.LINK]);
        return this.paramValues.singleValue;
    }

    public set singleValue(v: number) {
        this.checkValueMode([ParamValueMode.SINGLE, ParamValueMode.CALCUL, ParamValueMode.LINK]);
        this.checkValueAgainstDomain(v);
        this.paramValues.singleValue = v;
        this.initValue = v;
        this.notifyValueModified(this);
    }

    public get min() {
        this.checkValueMode([ParamValueMode.MINMAX, ParamValueMode.LINK]);
        return this.paramValues.min;
    }

    public set min(v: number) {
        this.checkValueMode([ParamValueMode.MINMAX]);
        this.paramValues.min = v;
    }

    public get max() {
        this.checkValueMode([ParamValueMode.MINMAX, ParamValueMode.LINK]);
        return this.paramValues.max;
    }

    public set max(v: number) {
        this.checkValueMode([ParamValueMode.MINMAX]);
        this.paramValues.max = v;
    }

    public get step() {
        this.checkValueMode([ParamValueMode.MINMAX, ParamValueMode.LINK]);
        return this.paramValues.step;
    }

    public set step(v: number) {
        this.checkValueMode([ParamValueMode.MINMAX]);
        this.paramValues.step = v;
    }

    /**
     * Generates a reference step value, given the current (local) values for min / max
     */
    public get stepRefValue(): Interval {
        this.checkValueMode([ParamValueMode.MINMAX]);
        return new Interval(1e-9, this._paramValues.max - this._paramValues.min);
    }

    public get valueList() {
        this.checkValueMode([ParamValueMode.LISTE, ParamValueMode.LINK]);
        return this.paramValues.valueList;
    }

    public set valueList(l: number[]) {
        this.checkValueMode([ParamValueMode.LISTE]);
        this.paramValues.valueList = l;
    }

    public count() {
        return this.paramValues.valuesIterator.count();
    }

    /**
     * Copies values of p into the current parameter, whatever the valueMode is; if
     * p has linked values, the target values will be copied and the links won't be kept
     * @param p a reference ParamDefinition or ParamValues to copy values from
     */
    public copyValuesFrom(p: ParamDefinition | ParamValues) {
        this.valueMode = p.valueMode;
        switch (p.valueMode) {
            case ParamValueMode.MINMAX:
                this.min = p.min;
                this.max = p.max;
                this.step = p.step;
                break;
            case ParamValueMode.LISTE:
                this.valueList = p.valueList;
                break;
            case ParamValueMode.LINK:
                if (p instanceof ParamDefinition) {
                    this.copyValuesFrom(p.referencedValue.getParamValues());
                }
                break;
            case ParamValueMode.CALCUL:
            case ParamValueMode.SINGLE:
            default:
                this.singleValue = p.singleValue;
        }
    }

    // for INumberiterator interface
    public get currentValue(): number {
        // magically follows links
        return this.paramValues.currentValue;
    }

    /**
     * Get single value
     */
    public getValue(): number {
        return this.paramValues.singleValue;
    }

    /**
     * Returns values as a number list, for LISTE and MINMAX modes;
     * in MINMAX mode, infers the list from min/max/step values
     * @param extendTo if given, will extend the values list to this number of values
     */
    public getInferredValuesList(extendTo?: number) {
        this.checkValueMode([ParamValueMode.LISTE, ParamValueMode.MINMAX, ParamValueMode.LINK]);
        return this.paramValues.getInferredValuesList(false, extendTo, true);
    }

    /**
     * Magic method to define current values into Paramvalues set; defines the mode
     * accordingly by detecting values nature
     */
    public setValues(o: number | any, max?: number, step?: number) {
        const oldMode = this.valueMode;
        this.paramValues.setValues(o, max, step);
        this.propagateValueMode(oldMode, this.valueMode, true);
    }

    /**
     * Sets the current value of the parameter's own values set, then
     * notifies all observers
     */
    public setValue(val: number, sender?: any) {
        this.valueMode = ParamValueMode.SINGLE;
        this.checkValueAgainstDomain(val);
        this.paramValues.singleValue = val;
    }

    /**
     * Same as setValue() but does not force SINGLE value mode; used for
     * updating initial value of a DICHO calculated parameter
     */
    public setInitValue(val: number, sender?: any) {
        this.checkValueAgainstDomain(val);
        this.initValue = val;
    }

    /**
     * Validates the given numeric value against the definition domain; throws
     * an error if value is outside the domain
     */
    public checkValueAgainstDomain(v: number) {
        const sDomain = ParamDomainValue[this._domain.domain];

        switch (this._domain.domain) {
            case ParamDomainValue.ANY:
                break;

            case ParamDomainValue.POS:
                if (v <= 0) {
                    const f = new Message(MessageCode.ERROR_PARAMDEF_VALUE_POS);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;

            case ParamDomainValue.POS_NULL:
                if (v < 0) {
                    const f = new Message(MessageCode.ERROR_PARAMDEF_VALUE_POSNULL);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;

            case ParamDomainValue.NOT_NULL:
                if (v === 0) {
                    const f = new Message(MessageCode.ERROR_PARAMDEF_VALUE_NULL);
                    f.extraVar.symbol = this.symbol;
                    throw f;
                }
                break;

            case ParamDomainValue.INTERVAL:
                const min = this._domain.minValue;
                const max = this._domain.maxValue;
                if (v < min || v > max) {
                    const f = new Message(MessageCode.ERROR_PARAMDEF_VALUE_INTERVAL);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    f.extraVar.minValue = min;
                    f.extraVar.maxValue = max;
                    throw f;
                }
                break;

            case ParamDomainValue.INTEGER:
                if (!Number.isInteger(v)) {
                    const f = new Message(MessageCode.ERROR_PARAMDEF_VALUE_INTEGER);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;

            default:
                const e = new Message(MessageCode.ERROR_PARAMDOMAIN_INVALID);
                e.extraVar.symbol = this.symbol;
                e.extraVar.domain = sDomain;
                throw e;
        }
    }

    public checkMin(min: number): boolean {
        return this.isMinMaxDomainValid(min) && (min < this.max);
    }

    public checkMax(max: number): boolean {
        return this.isMinMaxDomainValid(max) && (this.min < max);
    }

    public checkMinMaxStep(step: number): boolean {
        return this.isMinMaxValid && this.stepRefValue.intervalHasValue(step);
    }

    /**
     * Return true if single value is valid regarding the domain constraints
     */
    get isValueValid(): boolean {
        try {
            const v = this.paramValues.singleValue;
            this.checkValueAgainstDomain(v);
            return true;
        } catch (e) {
            return false;
        }
    }

    get isMinMaxValid(): boolean {
        return this.checkMinMax(this.min, this.max);
    }

    /**
     * Return true if current value is valid regarding the range constraints : min / max / step
     */
    public get isRangeValid(): boolean {
        switch (this.valueMode) {
            case ParamValueMode.LISTE:
                return this.isListValid;

            case ParamValueMode.MINMAX:
                return this.checkMinMaxStep(this.step);
        }

        throw new Error(`ParamDefinition.isRangeValid() : valeur ${ParamValueMode[this.valueMode]}`
            + `de ParamValueMode non prise en compte`);
    }

    /**
     * Root method to determine if a field value is valid, regarding the model constraints.
     *
     * Does not take care of input validation (ie: valid number, not empty...) because an
     * invalid input should never be set as a value; input validation is up to the GUI.
     *
     * In LINK mode :
     *  - if target is a parameter, checks validity of the target value against the local model constraints
     *  - if target is a result :
     *    - is result is already computed, checks validity of the target result against the local model constraints
     *    - if it is not, checks the "computability" of the target Nub (ie. validity of the Nub) to allow
     *      triggering chain computation
     */
    public get isValid(): boolean {
        switch (this.valueMode) {
            case ParamValueMode.SINGLE:
                return this.isValueValid;

            case ParamValueMode.MINMAX:
            case ParamValueMode.LISTE:
                return this.isRangeValid;

            case ParamValueMode.CALCUL:
                return true;

            case ParamValueMode.LINK:
                if (!this.isReferenceDefined()) {
                    return false;
                }
                // valuesIterator covers both target param and target result cases
                const iterator = this.valuesIterator;
                if (iterator) {
                    try {
                        for (const v of iterator) {
                            this.checkValueAgainstDomain(v);
                        }
                        return true;
                    } catch (e) {
                        return false;
                    }
                } else { // undefined iterator means target results are not computed yet
                    // check target Nub computability
                    return this.referencedValue.nub.isComputable();
                }
        }

        throw new Error(
            `ParamDefinition.isValid() : valeur de ParamValueMode '${ParamValueMode[this.valueMode]}' inconnue`
        );
    }

    /**
     * Sets the current parameter to LINK mode, pointing to the given
     * symbol (might be a Parameter (computed or not) or an ExtraResult)
     * of the given Nub
     *
     * Prefer @see defineReferenceFromLinkedValue() whenever possible
     *
     * @param nub
     * @param symbol
     */
    public defineReference(nub: Nub, symbol: string) {
        // prevent loops - beware that all Nubs must be registered in the
        // current session or this test will always fail
        if (!this.isAcceptableReference(nub, symbol)) {
            throw new Error(`defineReference() : link target ${nub.uid}.${symbol} is not an available linkable value`);
        }
        // clear current reference
        this.undefineReference();
        // find what the symbol points to
        if (nub) {
            // 1. extra result ?
            // - start here to avoid extra results being presented as
            // parameters by the iterator internally used by nub.getParameter()
            // - extra results with no family are linkable only if linking parameter family is ANY
            if (
                Object.keys(nub.resultsFamilies).includes(symbol)
                && (
                    nub.resultsFamilies[symbol] !== undefined
                    || this._family === ParamFamily.ANY
                )
            ) {
                this._referencedValue = new LinkedValue(nub, undefined, symbol);
            } else {
                // 2. is it a parameter (possibly in CALC mode) ?
                const p = nub.getParameter(symbol);
                if (p) {
                    this._referencedValue = new LinkedValue(nub, p, symbol);
                }
            }
        }
        if (this._referencedValue) {
            // set value mode
            this.valueMode = ParamValueMode.LINK;
        } else {
            throw new Error(`defineReference - could not find target for ${nub.uid}.${symbol}`);
        }
    }

    /**
     * Asks the Session for available linkabke values for the current parameter,
     * and returns true if given { nub / symbol } pair is part of them
     */
    public isAcceptableReference(nub: Nub, symbol: string) {
        const linkableValues = Session.getInstance().getLinkableValues(this);
        for (const lv of linkableValues) {
            if (lv.isTargetOf(nub, symbol)) {
                return true;
            }
        }
        return false;
    }

    public defineReferenceFromLinkedValue(target: LinkedValue) {
        this._referencedValue = target;
        this.valueMode = ParamValueMode.LINK;
    }

    public undefineReference() {
        this._referencedValue = undefined;
    }

    public isReferenceDefined(): boolean {
        return (this._referencedValue !== undefined);
    }

    /**
     * Returns an object representation of the Parameter's current state
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *        useful to determine if linked parameters must be kept as links or have their value
     *        copied (if target is not in UIDs list); if undefined, wil consider all Nubs as
     *        available (ie. will not break links)
     */
    public objectRepresentation(nubUidsInSession?: string[]): { symbol: string, mode: string } {
        // parameter representation
        const paramRep: any = {
            symbol: this.symbol,
            mode: ParamValueMode[this.valueMode]
        };
        // adjust parameter representation depending on value mode
        switch (this.valueMode) {
            case ParamValueMode.SINGLE:
                paramRep.value = this.singleValue;
                break;

            case ParamValueMode.CALCUL:
                // save initial value if param is calculated using dichotomy
                if (this.calculability === ParamCalculability.DICHO) {
                    paramRep.value = this.singleValue;
                }
                break;

            case ParamValueMode.MINMAX:
                paramRep.min = this.min;
                paramRep.max = this.max;
                paramRep.step = this.step;
                paramRep.extensionStrategy = this.extensionStrategy;
                break;

            case ParamValueMode.LISTE:
                paramRep.values = this.valueList;
                paramRep.extensionStrategy = this.extensionStrategy;
                break;

            case ParamValueMode.LINK:
                if (nubUidsInSession === undefined || nubUidsInSession.includes(this._referencedValue.nub.uid)) {
                    // target Nub is available in session, link won't get broken
                    paramRep.targetNub = this._referencedValue.nub.uid;
                    paramRep.targetParam = this._referencedValue.symbol;
                } else {
                    // target Nub will be lost, copy value(s) to keep consistency
                    const targetPV = this._referencedValue.getParamValues(true);
                    paramRep.mode = ParamValueMode[targetPV.valueMode];
                    switch (targetPV.valueMode) {
                        case ParamValueMode.SINGLE:
                            paramRep.value = targetPV.singleValue;
                            break;
                        case ParamValueMode.MINMAX:
                            paramRep.min = targetPV.min;
                            paramRep.max = targetPV.max;
                            paramRep.step = targetPV.step;
                            paramRep.extensionStrategy = targetPV.extensionStrategy;
                            break;
                        case ParamValueMode.LISTE:
                            paramRep.values = targetPV.valueList;
                            paramRep.extensionStrategy = targetPV.extensionStrategy;
                            break;
                        // should never be in LINK or CALC mode (see LinkedValue methods)
                    }
                }
                break;
        }
        return paramRep;
    }

    /**
     * Fills the current Parameter, provided an object representation
     * @param obj object representation of a Parameter state
     * @returns object {
     *      calculated: boolean true if loaded parameter is in CALC mode
     *      hasErrors: boolean true if errors were encountered during loading
     * }
     */
    public loadObjectRepresentation(obj: any, setCalcMode: boolean = true)
        : { calculated: boolean, hasErrors: boolean } {

        const ret = {
            calculated: false,
            hasErrors: false
        };

        // set mode
        const mode: ParamValueMode = (ParamValueMode as any)[obj.mode]; // get enum index for string value

        // when loading parent Nub, setting mode to CALC would prevent ensuring
        // consistency when setting calculatedParam afterwards
        if (mode !== ParamValueMode.CALCUL || setCalcMode) {
            try {
                this.setValueMode(mode, setCalcMode);
            } catch (err) {
                // silent fail : impossible to determine if this is an error, because
                // at this time, it is possible that no candidate for calculatedParam can
                // be found, since Nub children are not loaded yet (see nghyd#263)

                /* ret.hasErrors = true;
                console.error("loadObjectRepresentation: set valueMode error"); */
            }
        }

        // set value(s)
        switch (mode) {
            case ParamValueMode.SINGLE:
                this.singleValue = obj.value;
                break;

            case ParamValueMode.MINMAX:
                this.min = obj.min;
                this.max = obj.max;
                this.step = obj.step;
                if (obj.extensionStrategy !== undefined) {
                    this.extensionStrategy = obj.extensionStrategy;
                }
                break;

            case ParamValueMode.LISTE:
                this.valueList = obj.values;
                if (obj.extensionStrategy !== undefined) {
                    this.extensionStrategy = obj.extensionStrategy;
                }
                break;

            case ParamValueMode.CALCUL:
                // although calculated param is set at Nub level (see below),
                // it is detected as "the only parameter in CALC mode"
                if (obj.value !== undefined) { // dichotomy params have an initial value, others don't
                    this.singleValue = obj.value;
                }
                ret.calculated = true;
                break;

            case ParamValueMode.LINK:
                // formulaire dont le Nub est la cible du lien
                const destNub = Session.getInstance().findNubByUid(obj.targetNub);
                if (destNub) {
                    try {
                        this.defineReference(destNub, obj.targetParam);
                    } catch (err) {
                        // silent fail : impossible to determine if this is an error, because
                        // fixLinks() might solve it later
                    }
                } // si la cible du lien n'existe pas, Session.fixLinks() est censé s'en occuper
                break;

            default:
                throw new Error(`session file : invalid value mode '${obj.valueMode}' in param object ${obj.symbol}`);
        }

        return ret;
    }

    /**
     * Returns true if both the Nub UID and the symbol are identical
     */
    public equals(p: ParamDefinition): boolean {
        return (
            this.nubUid === p.nubUid
            && this.symbol === p.symbol
        );
    }

    /**
     * Returns true if this parameter's value can safely be linked to the
     * given parameter "src" (ie. without leading to circular dependencies)
     */
    public isLinkableTo(src: ParamDefinition): boolean {
        // did we loop back to the candidate parameter ?
        if (this.equals(src)) {
            // prevent infinite recursion
            return false;
        }

        if (this.valueMode === ParamValueMode.CALCUL) {
            // if my Nub doesn't depend on your result,
            // and my Nub doesn't already have a parameter linked to you,
            // you may depend on its result (me) !
            if (this._parent && this._parent.parent) {
                const myNub = (this.parentComputeNode as Nub);
                const ok = (
                    !myNub.dependsOnNubResult(src.parentNub)
                    && !myNub.dependsOnParameter(src)
                );
                return ok;
            }
        }

        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            // does the link point to an extra result ?
            if (this.referencedValue.isExtraResult()) {
                // if the target Nub doesn't depend on your result, you may depend on its extra result (me) !
                return (!this.referencedValue.nub.dependsOnNubResult(src.parentNub));

            } else { // the link points to a parameter, computed or not
                // recursively follow links, unless it loops back to the original parameter
                return (this.referencedValue.element as ParamDefinition).isLinkableTo(src);
            }
        }

        // SINGLE / MINMAX / LISTE parameters can always be linked without danger
        return true;
    }

    /**
     * Returns true if this parameter is ultimately (after followink links chain) linked
     * to a result or extra result of any of the given nubs.
     * If followLinksChain is false, will limit theexploration to the first target level only
     */
    public isLinkedToResultOfNubs(nubs: Nub[], followLinksChain: boolean = true): boolean {
        let linked = false;
        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            const targetNubUid = this.referencedValue.nub.uid;
            // is target a result ?
            if (this.referencedValue.isResult() || this.referencedValue.isExtraResult()) {
                for (const y of nubs) {
                    if (y.uid === targetNubUid) {
                        // dependence found !
                        linked = true;
                    } else if (this.referencedValue.nub.dependsOnNubResult(y)) {
                        // indirect result dependence found !
                        linked = true;
                    }
                }
            } else { // target is a parameter
                // recursion ?
                if (followLinksChain) {
                    linked = (this.referencedValue.element as ParamDefinition).isLinkedToResultOfNubs(nubs);
                }
            }
        }
        return linked;
    }

    /**
     * Returns true if the current parameter is directly linked to the
     * Nub having UID "uid", its parent or any of its children
     * @param uid
     * @param symbol symbol of the target parameter whose value change triggered this method;
     *      if current Parameter targets this symbol, Nub will be considered dependent
     * @param includeValuesLinks if true, even if this Parameter targets a non-calculated non-modified
     *      parameter, Nub will be considered dependent @see jalhyd#98
     */
    public dependsOnNubFamily(uid: string, symbol?: string, includeValuesLinks: boolean = false): boolean {
        let linked = false;
        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            const ref = this._referencedValue;

            // direct, parent or children reference ?
            if (
                (ref.nub.uid === uid)
                || (ref.nub.parent && ref.nub.parent.uid === uid)
                || (ref.nub.getChildren().map((c) => c.uid).includes(uid))
            ) {
                linked = (
                    (symbol !== undefined && symbol === ref.symbol)
                    || ref.isCalculated()
                    || includeValuesLinks
                );
            } // else no recursion, checking level 1 only
        }
        return linked;
    }

    // interface IterableValues

    /**
     * Transparent proxy to own values iterator or targetted values iterator (in LINK mode)
     */
    public get valuesIterator(): INumberIterator {
        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            try {
                return this._referencedValue.getParamValues().valuesIterator;
            } catch (e) {
                // values are not computed yet
                return undefined;
            }
        } else {
            return this.paramValues.valuesIterator;
        }
    }

    public getExtendedValuesIterator(size: number): INumberIterator {
        return this.paramValues.getValuesIterator(false, size, true);
    }

    /**
     * Returns true if there are more than 1 value associated to this parameter;
     * might be its own values (MINMAX / LISTE mode), or targetted values (LINK mode)
     */
    public get hasMultipleValues(): boolean {
        if (this.valueMode === ParamValueMode.LINK && this.isReferenceDefined()) {
            return this._referencedValue.hasMultipleValues();
        } else {
            if (this.valueMode === ParamValueMode.CALCUL) {
                return this.parentNub.resultHasMultipleValues();
            } else {
                return this.paramValues.hasMultipleValues;
            }
        }
    }

    public initValuesIterator(reverse: boolean = false): INumberIterator {
        return this.paramValues.getValuesIterator(reverse, undefined, true);
    }

    public get hasNext(): boolean {
        return this.paramValues.hasNext;
    }

    public next(): IteratorResult<number> {
        return this.paramValues.next();
    }

    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    public nextValue(): IteratorResult<number> {
        return this.next();
    }

    public [Symbol.iterator](): IterableIterator<number> {
        return this.paramValues;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }

    /**
     * variable calculable par l'équation ?
     */
    public isAnalytical(): boolean {
        return this.calculability === ParamCalculability.EQUATION;
    }

    /**
     * Get the highest nub in the child nub hierarchy
     */
    get originNub(): Nub {
        let originNub = this.parentNub;
        while (true) {
            if (originNub.parent === undefined) {
                return originNub;
            }
            originNub = originNub.parent;
        }
    }

    /**
     * Renvoie la valeur actuelle du paramètre (valeur courante ou résultat si calculé)
     */
    get V(): number {
        // paramètre en calcul ou résultat complémentaire
        if (this.parentNub.result !== undefined) {
            if (this.parentNub.result.resultElement.ok) {
                if (this.parentNub.result.resultElement.values[this.symbol] !== undefined) {
                    return this.parentNub.result.resultElement.values[this.symbol];
                }
            }
        }
        // sinon, valeur courante
        return this.currentValue;
    }

    public clone(): ParamDefinition {
        const res = new ParamDefinition(this._parent, this._symbol,
            this.domain.clone(), this._unit, undefined, this._family, this.visible);

        res._paramValues = this._paramValues.clone();
        res.v = this.v;
        res._initValue = this._initValue;
        res._calc = this._calc;
        res._extensionStrategy = this._extensionStrategy;
        res._referencedValue = this._referencedValue;

        return res;
    }

    /**
     * notification envoyée après la modification de la valeur du paramètre
     */
    private notifyValueModified(sender: any) {
        this.notifyObservers(
            { action: "paramdefinitionAfterValue" }, sender
        );
    }

    /**
     * vérifie si un min/max est valide par rapport au domaine de définition
     */
    private isMinMaxDomainValid(v: number): boolean {
        if (v === undefined) {
            return false;
        }
        if (this.valueMode === ParamValueMode.MINMAX) {
            try {
                this.checkValueAgainstDomain(v);
            } catch (e) {
                return false;
            }
        }
        return true;
    }

    private checkMinMax(min: number, max: number): boolean {
        return this.isMinMaxDomainValid(min) && this.isMinMaxDomainValid(max) && (min < max);
    }

    private get isListValid(): boolean {
        if (this.paramValues.valueList === undefined) {
            return false;
        }

        for (const v of this.paramValues.valueList) {
            try {
                this.checkValueAgainstDomain(v);
            } catch (e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Throws an error if this.paramValues._valueMode is not the expected value mode.
     * In LINK mode, the tested valueMode is the mode of the ultimately targetted
     * set of values (may never be LINK)
     */
    private checkValueMode(expected: ParamValueMode[]) {
        if (!expected.includes(this.valueMode)) {
            throw new Error(
                `ParamDefinition : mode de valeurs ${ParamValueMode[this.valueMode]} incorrect pour le paramètre ${this.symbol}`
            );
        }
    }

}
