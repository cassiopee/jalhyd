import { INumberIterator } from "../internal_modules";
import { ParamValues } from "../internal_modules";

/**
 * Itérateur miroir sur les (ou la) valeurs prises par un ParamValues.
 * Différent d'un ParamValuesIterator avec reverse = true.
 *
 * ex: min = 0, max = 10, step = 3
 * ParamValuesIterator:         [ 0, 3, 6, 9, 10 ]
 * ParamValuesIterator reverse: [ 10, 7, 4, 1, 0 ]
 * MirrorIterator:              [ 10, 9, 6, 3, 0 ]
 */
export class MirrorIterator implements INumberIterator {

    /** paramètre à itérer */
    private _param: ParamValues;

    /** valeur courante */
    private _current: number;

    /** index sur la liste de valeurs locales */
    private _index: number;

    /** liste des valeurs généres par l'itérateur du ParamValues, dans l'ordre inverse */
    private _valuesList: number[];

    constructor(prm: ParamValues) {
        prm.check();
        this._param = prm;
        this.reset();
    }

    public get currentValue(): number {
        return this._current;
    }

    /**
     * Returns the number of elements in the iterator without altering it,
     * by counting a copy of the iterator
     */
    public count() {
        return this._param.valuesIterator.count();
    }

    /**
     * Reinits the value iterator
     */
    public reset() {
        this._index = 0;
        this._current = undefined;
        // get original values list
        this._valuesList = [];
        const it = this._param.valuesIterator;
        while (it.hasNext) {
            this._valuesList.push(it.next().value);
        }
        // mirror it
        this._valuesList.reverse();
    }

    public next(): IteratorResult<number> {
        if (this.hasNext) {
            this._current = this._valuesList[this._index];
            this._index++;
            return {
                done: false,
                value: this._current
            };
        } else { // end of iterator
            return {
                done: true,
                value: undefined
            };
        }
    }

    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    public nextValue(): IteratorResult<number> {
        return this.next();
    }

    // interface IterableIterator

    public [Symbol.iterator](): IterableIterator<number> {
        return this;
    }

    /**
     * Returns true if iterator has at least one more value
     */
    public get hasNext(): boolean {
        return this._index < this._valuesList.length;
    }
}
