import { isEqual } from "../internal_modules";
import { INamedObject, IObjectWithFamily } from "../internal_modules";
import { ExtensionStrategy } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";
import { ParamValues } from "../internal_modules";

/**
 * itérateur sur des nombres
 */
export interface INumberIterator extends IterableIterator<number> {
    /**
     * @return true si il reste des valeurs à parcourir
     */
    readonly hasNext: boolean;

    /**
     * valeur courante cad résultat du précédent appel à next().
     * contrairement à next(), on peut l'appeler plusieurs fois sans modifier l'état de l'itérateur
     */
    readonly currentValue: number;

    /**
     * prochaine valeur
     */
    next(): IteratorResult<number>;

    nextValue(): IteratorResult<number>;

    count(): number;
}

/**
 * interface implémentée par les objets pouvant posséder/renvoyer un itérateur sur une série de valeurs numériques
 */
// tslint:disable-next-line:interface-name
export interface IterableValues extends INumberIterator {
    /**
     * crée un nouvel itérateur sur les valeurs
     */
    readonly valuesIterator: INumberIterator;

    /**
     * true si la série de valeurs a plus d'une valeur
     */
    readonly hasMultipleValues: boolean;

    /**
     * initialise un itérateur interne sur les valeurs implicitement utilisé par hasNext() et next()
     */
    initValuesIterator(reverse: boolean): INumberIterator;
}

/**
 * objets pouvant avoir un nom et un itérateur sur une série de valeurs numériques
 */
export interface INamedIterableValues extends INamedObject, IObjectWithFamily, IterableValues {
}

/**
 * itérateur sur les (ou la) valeurs prises par un ParamValues
 */
export class ParamValueIterator implements INumberIterator {

    /** paramètre à itérer */
    private _param: ParamValues;

    /**
     * true si les valeurs sont fournies de max à min (ParamValueMode.MINMAX);
     * utilisé uniquement par Remous
     */
    private _reverse: boolean;

    /** length to extend the values series to, depending on _param ExtensionStrategy */
    private _extendTo: number;

    /**
     * if true, in MINMAX mode ensures that max value is returned,
     * even if (max - min) is not a multiple of step
     */
    private _addLastStep: boolean;

    /** valeur courante */
    private _current: number;

    /**
     * index sur les valeurs définies (et non sur les valeurs étendues): en mode LISTE,
     * nombre de valeurs actuellement produites; en mode MINMAX, valeur courante (!)
     */
    private _index: number;

    /**
     * index sur les valeurs étendues (lorsque plusieurs paramètres varient): en mode MINMAX,
     * nombre de valeurs actuellement produites, à comparer à this._extendTo; en mode LISTE,
     * index de la valeur courante à répéter (!)
     */
    private _xindex: number;

    /**
     * itérateur local pour étendre les valeurs en mode MINMAX
     */
    private _locExIt: INumberIterator;

    /**
     * indicates that the MINMAX iterator has already run once until
     * the last value (whether it is aligned or not) was reached
     */
    private _minMaxLastValueReached: boolean;

    constructor(prm: ParamValues, reverse: boolean = false, extendTo: number, addLastStep: boolean = false) {
        prm.check();
        this._param = prm;
        this.reset(reverse, extendTo, addLastStep);
    }

    public get currentValue(): number {
        return this._current;
    }

    /**
     * Returns the number of elements in the iterator without altering it,
     * by counting a copy of the iterator
     */
    public count() {
        const itCopy = new ParamValueIterator(this._param, this._reverse, this._extendTo, this._addLastStep);
        let length = 0;
        for (const i of itCopy) {
            length++;
        }
        return length;
    }

    /**
     * Reinits the value iterator
     * @param reverse if true, will iterate starting at the end
     * @param extendTo if defined, will extend values list until this boundary
     * @param addLastStep if true, if (max - min) is not a multiple of step, one more
     *                    iteration will be done return last value (max)
     */
    public reset(reverse: boolean, extendTo?: number, addLastStep: boolean = false) {
        this._reverse = reverse;
        this._extendTo = extendTo;
        this._addLastStep = addLastStep;
        this._xindex = 0;
        this._minMaxLastValueReached = false;

        switch (this._param.valueMode) {
            case ParamValueMode.SINGLE:
                this._index = 0;
                break;

            case ParamValueMode.MINMAX:
                if (reverse) {
                    this._index = this._param.max;
                } else {
                    this._index = this._param.min;
                }
                break;

            case ParamValueMode.LISTE:
                this._index = 0;
                break;

            default:
                // tslint:disable-next-line:max-line-length
                throw new Error(`ParamValueIterator : mode ${ParamValueMode[this._param.valueMode]} incorrect`);
        }
    }

    /**
     * Same as next(), but sets _currentValue on attached ParamValues; to be
     * used with dedicated iterators, for params having multiple links on them
     * @see jalhyd#222
     */
    public nextValue(): IteratorResult<number> {
        const res = this.next();
        if (!res.done) {
            this._param.setCurrentValueFromIterator(res.value);
        }
        return res;
    }

    public next(): IteratorResult<number> {
        if (this.hasNext) {
            switch (this._param.valueMode) {

                case ParamValueMode.SINGLE:
                    if (this.hasNext) {
                        this._current = this._param.singleValue;
                        this._index++;
                        this._xindex++;
                        return {
                            done: false,
                            value: this._current
                        };
                    } else {
                        return {
                            done: true,
                            value: undefined
                        };
                    }

                case ParamValueMode.LISTE:
                    if (this.hasNextWithoutExtension) { // default case
                        this._current = this._param.valueList[this._index++]; // what about _reverse ?
                        this._xindex++; // count values for possible extension
                        return {
                            done: false,
                            value: this._current
                        };
                    } else { // no more real values
                        if (this._extendTo && this.hasNext) {
                            // extend
                            this._index++;
                            switch (this._param.extensionStrategy) {
                                case ExtensionStrategy.REPEAT_LAST:
                                    // repeat last real value (do not change this._current)
                                    return {
                                        done: false,
                                        value: this._current
                                    };

                                case ExtensionStrategy.RECYCLE:
                                    // loop over real values
                                    if (
                                        this._xindex === undefined
                                        || this._xindex === this._param.valueList.length
                                    ) {
                                        this._xindex = 0;
                                    }
                                    this._current = this._param.valueList[this._xindex++];
                                    return {
                                        done: false,
                                        value: this._current
                                    };

                                default:
                                    throw new Error(`ParamValueIterator.next() : no extension strategy (LISTE)`);
                            }
                        } else {
                            return {
                                done: true,
                                value: undefined
                            };
                        }
                    }

                case ParamValueMode.MINMAX:
                    // protection against infinite loops
                    if (this._param.step === 0) {
                        this._index = this._param.max;
                        return {
                            done: true,
                            value: this._current
                        };
                    }
                    // s'il reste des valeurs normales (sans extension, mais en comptant le lastStep)
                    if (this.hasNextWithoutExtension) {
                        // si on a atteint la dernière valeur sans compter le dernier pas
                        if (this._addLastStep && this.minMaxLastAlignedValueReached()) {
                            // on ajoute la dernière valeur
                            this._current = this._reverse ? this._param.min : this._param.max;
                        } else {
                            // on ajoute / retranche un pas
                            this._current = this._index;
                            if (this._reverse) {
                                this._index -= this._param.step;
                            } else {
                                this._index += this._param.step;
                            }
                        }
                        this._xindex++; // count values for possible extension
                        return {
                            done: false,
                            value: this._current
                        };
                    } else { // no more real values
                        if (this._extendTo && this.hasNext) {
                            this._xindex++; // count values for possible extension
                            switch (this._param.extensionStrategy) {
                                case ExtensionStrategy.REPEAT_LAST:
                                    // repeat last real value (do not change this._current)
                                    return {
                                        done: false,
                                        value: this._current
                                    };

                                case ExtensionStrategy.RECYCLE:
                                    // loop over real values using a local iterator that does not extend
                                    if (this._locExIt === undefined || !this._locExIt.hasNext) {
                                        // create / rewind iterator
                                        this._locExIt = this._param.getValuesIterator(
                                            this._reverse, undefined, this._addLastStep
                                        );
                                    }
                                    this._current = this._locExIt.next().value;
                                    return {
                                        done: false,
                                        value: this._current
                                    };

                                default:
                                    throw new Error(`ParamValueIterator.next() : no extension strategy (MINMAX)`);

                            }
                        } else { // end of iterator
                            return {
                                done: true,
                                value: undefined
                            };
                        }
                    }

                default:
                    throw new Error(`ParamValueIterator.next() : internal error`);
            }
        } else { // end of iterator
            return {
                done: true,
                value: undefined
            };
        }
    }

    // interface IterableIterator

    public [Symbol.iterator](): IterableIterator<number> {
        return this;
    }

    public get hasNext(): boolean {
        return this.hasNextValue();
    }

    protected get hasNextWithoutExtension() {
        return this.hasNextValue(true);
    }

    /**
     * Returns true if iterator has at least one more value
     * @param ignoreExtension if true, will consider only real values (those that were
     *      set up), including the possible extraStep, and ignore extended values
     */
    protected hasNextValue(ignoreExtension: boolean = false): boolean {
        switch (this._param.valueMode) {

            case ParamValueMode.SINGLE:
                return this._index === 0;

            case ParamValueMode.LISTE:
                if (this._extendTo && !ignoreExtension) {
                    return this._index < this._extendTo;
                } else {
                    return this._index < this._param.valueList.length;
                }

            case ParamValueMode.MINMAX:
                // si extension, compter le nombre de valeurs
                if (this._extendTo && !ignoreExtension) {
                    return this._xindex < this._extendTo;
                } else {
                    // sinon, s'il y a un dernier pas à faire
                    return !this.minMaxLastValueReached();
                }

            default:
                throw new Error(`ParamValueIterator.hasNext() : internal error`);
        }
    }

    /**
     * Returns true if last value that is a multiple of step was reached;
     * it might still miss the lastStep if required
     */
    protected minMaxLastAlignedValueReached() {
        return this._reverse ?
            this._index < this._param.min - this._param.step * 1E-7 :
            this._index > this._param.max + this._param.step * 1E-7;
    }

    /**
     * Returns true if last value was reached in MINMAX mode
     */
    protected minMaxLastValueReached() {
        if (!this._minMaxLastValueReached) {
            // update flag
            if (this._addLastStep) {
                this._minMaxLastValueReached = this._reverse ?
                    isEqual(this._current, this._param.min, this._param.step * 1E-7) :
                    isEqual(this._current, this._param.max, this._param.step * 1E-7);
            } else {
                this._minMaxLastValueReached = this.minMaxLastAlignedValueReached();
            }
        }
        // return stored flag in any case
        return this._minMaxLastValueReached;
    }
}
