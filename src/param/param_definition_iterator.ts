import { MapIterator } from "../internal_modules";
import { ParamDefinition } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export interface IParamDefinitionIterator extends IterableIterator<ParamDefinition> {
}

/**
 * itérateur sur les paramètres d'une seule instance de ParamsEquation
 */
export class ParamDefinitionIterator implements IParamDefinitionIterator {
    private _mapIterator: MapIterator<ParamDefinition>;

    constructor(_params: ParamsEquation) {
        this._mapIterator = new MapIterator(_params.map);
    }

    public next(): IteratorResult<ParamDefinition> {
        return this._mapIterator.next();
    }

    public [Symbol.iterator](): IterableIterator<ParamDefinition> {
        return this;
    }
}
