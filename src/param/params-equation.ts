import { ComputeNode } from "../internal_modules";
import { ParamDefinition } from "../internal_modules";
import { IParamDefinitionIterator, ParamDefinitionIterator } from "../internal_modules";

/**
 * liste des paramètres d'une équation
 */
export abstract class ParamsEquation implements Iterable<ParamDefinition> {

    /**
     * Pointer to calculator using these parameters; usually defined in Nub constructor
     * when affecting ParamsEquation. Usually a Nub but might be an acSection
     */
    public parent: ComputeNode;

    protected _paramMap: { [key: string]: ParamDefinition } = {};

    public constructor(parent?: ComputeNode) {
        this.parent = parent;
    }

    get nubUid() {
        if (!this.parent) {
            throw new Error("ParamsEquation.nubUid : equation has no parent Nub !");
        }
        return this.parent.uid;
    }

    public hasParameter(name: string): boolean {
        for (const p of this) {
            if (p.symbol === name) {
                return true;
            }
        }
        return false;
    }

    public get map(): { [key: string]: ParamDefinition } {
        return this._paramMap;
    }

    /**
     * Retrieves a parameter from its symbol
     */
    public get(name: string): ParamDefinition {
        for (const s in this._paramMap) {
            if (s === name) {
                return this._paramMap[s];
            }
        }
        return undefined;
    }

    public checkParametersCalculability() {
        const res = [];

        for (const p of this) {
            if (p.calculability === undefined) {
                res.push(p.symbol);
            }
        }

        if (res.length > 0) {
            throw new Error("Calculability of parameter(s) " + res.toString() + " has not been defined");
        }
    }

    public [Symbol.iterator](): Iterator<ParamDefinition> {
        return this.iterator;
    }

    public get iterator(): IParamDefinitionIterator {
        return new ParamDefinitionIterator(this);
    }

    protected addParamDefinition(p: ParamDefinition, force: boolean = false) {
        if (force || !this.hasParameter(p.symbol)) {
            this._paramMap[p.symbol] = p;
        }
    }

    protected addParamDefinitions(ps: ParamsEquation) {
        for (const p of ps) {
            // Force update of the map index
            this.addParamDefinition(p, true);
        }
    }
}
