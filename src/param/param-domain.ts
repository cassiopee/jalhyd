import { Interval } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

/**
 * domaine de définition du paramètre
 */
export enum ParamDomainValue {
    /** >0, =0, <0 (-inf -> +inf) */
    ANY,

    /** >=0 */
    POS_NULL,

    /** > 0 */
    POS,

    /** <>0 */
    NOT_NULL,

    /** intervalle */
    INTERVAL,

    /** nombre entier relatif */
    INTEGER
}

export class ParamDomain {

    public static getDefaultBounds(d: ParamDomainValue): { min: number, max: number } {
        switch (d) {
            case ParamDomainValue.INTERVAL:
                const e: Message = new Message(MessageCode.ERROR_PARAMDOMAIN_INVALID);
                throw e;

            case ParamDomainValue.ANY:
            case ParamDomainValue.NOT_NULL:
            case ParamDomainValue.INTEGER:
                return { min: -Infinity, max: Infinity };

            case ParamDomainValue.POS:
                return { min: 1e-9, max: Infinity };

            case ParamDomainValue.POS_NULL:
                return { min: 0, max: Infinity };

            // default:
            //     throw "valeur de ParamDomainValue" + ParamDomainValue[d] + " non prise en charge";
        }
    }

    private _domain: ParamDomainValue;

    private _minValue: number;

    private _maxValue: number;

    constructor(d: ParamDomainValue, min?: number, max?: number) {
        this.checkValue(d, min, max);
        this._domain = d;

        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                this._minValue = min;
                this._maxValue = max;
                break;

            default:
                const b = ParamDomain.getDefaultBounds(this._domain);
                this._minValue = b.min;
                this._maxValue = b.max;
                break;
        }
    }

    get domain() {
        return this._domain;
    }

    get minValue() {
        return this._minValue;
    }

    get maxValue() {
        return this._maxValue;
    }

    public get interval(): Interval {
        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                return new Interval(this._minValue, this._maxValue);

            default:
                const b = ParamDomain.getDefaultBounds(this._domain);
                return new Interval(b.min, b.max);
        }
    }

    public clone(): ParamDomain {
        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                return new ParamDomain(this._domain, this._minValue, this._maxValue);

            default:
                return new ParamDomain(this._domain);
        }
    }

    /**
     * Return a value bounded by the definition domain
     * @param v the value to be bounded
     */
    public getBoundedValue(v: number): number {
        return Math.min(Math.max(v, this.minValue), this.maxValue);
    }

    private checkValue(val: number, min: number, max: number) {
        switch (val) {
            case ParamDomainValue.INTERVAL:
                if (min === undefined || max === undefined || min > max) {
                    const e: Message = new Message(MessageCode.ERROR_PARAMDOMAIN_INTERVAL_BOUNDS);
                    e.extraVar.minValue = min;
                    e.extraVar.maxValue = max;
                    throw e;
                }
                break;

            default:
                // en dehors du cas INTERVAL, on ne doit pas fournir de valeur
                if (min !== undefined || max !== undefined) {
                    const e: Message = new Message(MessageCode.ERROR_PARAMDOMAIN_INTERVAL_BOUNDS);
                    e.extraVar.minValue = min;
                    e.extraVar.maxValue = max;
                    throw e;
                }
                break;
        }
    }
}
