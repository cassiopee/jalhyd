
/**
 * mode de génération des valeurs d'un paramètre
 */
export enum ParamValueMode {
    /**
     * valeur unique
     */
    SINGLE,

    /**
     * min, max, pas
     */
    MINMAX,

    /**
     * liste de valeurs discrètes
     */
    LISTE,

    /**
     * la valeur du paramètre est non définie et à calculer
     */
    CALCUL, // calculated param is now set at Nub level, but this mode is kept to ease differenciation with other modes

    /**
     * la valeur du paramètre est liée à celle d'un paramètre, d'un résultat, ...
     */
    LINK
}
