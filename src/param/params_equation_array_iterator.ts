import { MapIterator } from "../internal_modules";
import { ParamDefinition } from "../internal_modules";
import { IParamDefinitionIterator } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * itérateur sur les paramètres d'un tableau de de ParamsEquation
 */
export class ParamsEquationArrayIterator implements IParamDefinitionIterator {

    private get done(): IteratorResult<ParamDefinition> {
        return {
            done: true,
            value: undefined
        };
    }
    private _paramsEqs: ParamsEquation[];

    private _index: number = 0;

    private _currentMapIterator: MapIterator<ParamDefinition>;

    constructor(p: ParamsEquation[]) {
        this._paramsEqs = p;
    }

    public next(): IteratorResult<ParamDefinition> {
        if (this._currentMapIterator === undefined) {
            this.nextIterator();
        }

        let res = this.done;
        if (this._currentMapIterator) {
            res = this._currentMapIterator.next();
            if (res.done) {
                this.nextIterator();
                if (this._currentMapIterator) {
                    res = this._currentMapIterator.next();
                }
            }
        }

        return res;
    }

    public [Symbol.iterator](): IterableIterator<ParamDefinition> {
        return this;
    }

    private nextIterator() {
        if (this._index < this._paramsEqs.length) {
            this._currentMapIterator = new MapIterator(this._paramsEqs[this._index++].map);
        } else {
            this._currentMapIterator = undefined;
        }
    }
}
