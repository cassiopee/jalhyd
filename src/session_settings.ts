import { IObservable, Observable, Observer } from "./internal_modules";
import { Props } from "./internal_modules";

export class SessionSettings implements IObservable {
    private static _instance: SessionSettings;

    /** session-wide calculation precision, used by all Nubs */
    public static readonly defaultComputePrecision = 1e-7;

    /** session-wide maximum number of iterations, used by all Nubs in Newton() and Dichotomie() */
    public static readonly defaultMaxIterations = 100;

    // properties
    private _props: Props;

    // property names
    private static readonly ComputePrecisionProp = "computePrecision";

    private static readonly MaxIterationsProp = "maxIterations";

    /**
     * IObservable delegation.
     * Do not use Props IObservable interface to make notifications specific to SessionSettings
     * and to not disturb observers.
     */
    private _observable: Observable;

    private constructor() {
        this._props = new Props();
        this._observable = new Observable();
        this._props.setPropValue(SessionSettings.ComputePrecisionProp, SessionSettings.defaultComputePrecision);
        this._props.setPropValue(SessionSettings.MaxIterationsProp, SessionSettings.defaultMaxIterations);
    }

    public static get instance(): SessionSettings {
        if (SessionSettings._instance === undefined) {
            SessionSettings._instance = new SessionSettings();
        }
        return SessionSettings._instance;
    }

    public static get precision(): number {
        return SessionSettings.instance._props.getPropValue(SessionSettings.ComputePrecisionProp);
    }

    public static set precision(p: number) {
        SessionSettings.instance._props.setPropValue(SessionSettings.ComputePrecisionProp, p, this._instance);
        SessionSettings.instance.notifyObservers(SessionSettings.ComputePrecisionProp);
    }

    public static get maxIterations(): number {
        return SessionSettings.instance._props.getPropValue(SessionSettings.MaxIterationsProp);
    }

    public static set maxIterations(n: number) {
        SessionSettings.instance._props.setPropValue(SessionSettings.MaxIterationsProp, n, this._instance);
        SessionSettings.instance.notifyObservers(SessionSettings.MaxIterationsProp);
    }

    // IObserver interface

    addObserver(o: Observer): void {
        this._observable.addObserver(o);
    }

    removeObserver(o: Observer): void {
        this._observable.removeObserver(o);
    }

    notifyObservers(data: any, sender?: any): void {
        // use SessionSettings._instance and not this to be able to use instanceof operator
        this._observable.notifyObservers(data, SessionSettings._instance);
    }
}
