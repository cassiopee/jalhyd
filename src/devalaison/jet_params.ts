import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class JetParams extends ParamsEquation {

    /** Jet start elevation (m) */
    private _ZJ: ParamDefinition;

    /** Water elevation (m) */
    private _ZW: ParamDefinition;

    /** Bottom elevation (m) */
    private _ZF: ParamDefinition;

    /** Initial speed */
    private _V0: ParamDefinition;

    /** Initial Slope */
    private _S: ParamDefinition;

    /** Impact distance */
    private _D: ParamDefinition;

    constructor(V0: number, S: number, ZJ: number, ZW: number, ZF: number, D: number, nullParams: boolean = false) {
        super();
        this._V0 = new ParamDefinition(this, "V0", ParamDomainValue.POS, "m", V0, ParamFamily.SPEEDS, undefined, nullParams);
        this.addParamDefinition(this._V0);
        this._S = new ParamDefinition(
            this, "S",
            new ParamDomain(ParamDomainValue.INTERVAL, -0.99, 0.99),
            "m/m", S, ParamFamily.SLOPES, undefined, nullParams
        );
        this.addParamDefinition(this._S);
        this._ZJ = new ParamDefinition(this, "ZJ", ParamDomainValue.ANY, "m", ZJ, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZJ);
        this._ZW = new ParamDefinition(this, "ZW", ParamDomainValue.ANY, "m", ZW, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZW);
        this._ZF = new ParamDefinition(this, "ZF", ParamDomainValue.ANY, "m", ZF, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZF);
        this._D = new ParamDefinition(this, "D", ParamDomainValue.POS_NULL, "m", D, ParamFamily.LENGTHS, undefined, nullParams);
        this.addParamDefinition(this._D);
    }

    public get V0(): ParamDefinition {
        return this._V0;
    }

    public get S(): ParamDefinition {
        return this._S;
    }

    public get ZJ(): ParamDefinition {
        return this._ZJ;
    }

    public get ZW(): ParamDefinition {
        return this._ZW;
    }

    public get ZF(): ParamDefinition {
        return this._ZF;
    }

    public get D(): ParamDefinition {
        return this._D;
    }
}
