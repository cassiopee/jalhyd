import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class GrilleParams extends ParamsEquation {

    /** Débit maximal turbiné QMax */
    private _QMax: ParamDefinition;

    /** Cote du radier CRad */
    private _CRad: ParamDefinition;

    /** Cote du niveau d'eau CEau */
    private _CEau: ParamDefinition;

    /** Cote du sommet immergé du plan de grille CSomGrille */
    private _CSomGrille: ParamDefinition;

    /** Largeur de la section B */
    private _B: ParamDefinition;

    /** Inclinaison Beta */
    private _Beta: ParamDefinition;

    /** Orientation Alpha */
    private _Alpha: ParamDefinition;

    /** Épaisseur des barreaux B */
    private _b: ParamDefinition;

    /** Profondeur des barreaux P */
    private _p: ParamDefinition;

    /** Espacement libre entre les barreaux E */
    private _e: ParamDefinition;

    /** Coefficient de forme des barreaux a */
    private _a: ParamDefinition;

    /** Coefficient de forme des barreaux c */
    private _c: ParamDefinition;

    /** Obstruction globale du plan de grille O */
    private _O: ParamDefinition;

    /** Obstruction due aux barreaux et éléments de support longitudinaux (type Incliné) */
    private _Ob: ParamDefinition;

    /** Obstruction effective due aux entretoises et éléments de support transversaux (type Incliné) OEntH */
    private _OEntH: ParamDefinition;

    /** Coefficient de forme moyen des entretoises et éléments transversaux (type Incliné) CIncl */
    private _cIncl: ParamDefinition;

    constructor(
        rQMax: number, rCRad: number, rCEau: number, rCSomGrille: number, rB: number, rBeta: number,
        rAlpha: number, rb: number, rp: number, re: number, ra: number, rc: number, rO: number,
        rOb: number, rOEntH: number, rcIncl: number, nullParams: boolean = false
    ) {
        super();
        this._QMax = new ParamDefinition(
            this, "QMax", ParamDomainValue.POS_NULL, "m³/s", rQMax, ParamFamily.FLOWS, undefined, nullParams
        );
        this._CRad = new ParamDefinition(this, "CRad", ParamDomainValue.ANY, "m", rCRad, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._CEau = new ParamDefinition(this, "CEau", ParamDomainValue.ANY, "m", rCEau, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._CSomGrille = new ParamDefinition(
            this, "CSomGrille", ParamDomainValue.ANY, "m", rCSomGrille, ParamFamily.ELEVATIONS, undefined, nullParams
        );
        this._B = new ParamDefinition(this, "B", ParamDomainValue.POS, "m", rB, ParamFamily.WIDTHS, undefined, nullParams);
        this._Beta = new ParamDefinition(
            this, "Beta", new ParamDomain(ParamDomainValue.INTERVAL, 15, 90), "°", rBeta, undefined, undefined, nullParams
        );
        this._Alpha = new ParamDefinition(
            this, "Alpha", new ParamDomain(ParamDomainValue.INTERVAL, 30, 90), "°", rAlpha, undefined, undefined, nullParams
        );
        this._b = new ParamDefinition(this, "b", ParamDomainValue.POS, "mm", rb, undefined, undefined, nullParams);
        this._p = new ParamDefinition(this, "p", ParamDomainValue.POS, "mm", rp, undefined, undefined, nullParams);
        this._e = new ParamDefinition(this, "e", ParamDomainValue.POS, "mm", re, undefined, undefined, nullParams);
        this._a = new ParamDefinition(this, "a", ParamDomainValue.POS, "", ra, undefined, false, nullParams);
        this._c = new ParamDefinition(this, "c", ParamDomainValue.POS, "", rc, undefined, false, nullParams);
        this._O = new ParamDefinition(
            this, "O", new ParamDomain(ParamDomainValue.INTERVAL, 0, 1), "", rO, undefined, undefined, nullParams
        );
        this._Ob = new ParamDefinition(
            this, "Ob", new ParamDomain(ParamDomainValue.INTERVAL, 0.28, 0.53), "", rOb, undefined, false, nullParams
        );
        this._OEntH = new ParamDefinition(
            this, "OEntH", new ParamDomain(ParamDomainValue.INTERVAL, 0, 0.28), "", rOEntH, undefined, false, nullParams
        );
        this._cIncl = new ParamDefinition(this, "cIncl", ParamDomainValue.POS, "", rcIncl, undefined, undefined, nullParams);

        this.addParamDefinition(this._QMax);
        this.addParamDefinition(this._CRad);
        this.addParamDefinition(this._CEau);
        this.addParamDefinition(this._CSomGrille);
        this.addParamDefinition(this._B);
        this.addParamDefinition(this._Beta);
        this.addParamDefinition(this._Alpha);
        this.addParamDefinition(this._b);
        this.addParamDefinition(this._p);
        this.addParamDefinition(this._e);
        this.addParamDefinition(this._a);
        this.addParamDefinition(this._c);
        this.addParamDefinition(this._O);
        this.addParamDefinition(this._Ob);
        this.addParamDefinition(this._OEntH);
        this.addParamDefinition(this._cIncl);
    }

    get QMax() {
        return this._QMax;
    }

    get CRad() {
        return this._CRad;
    }

    get CEau() {
        return this._CEau;
    }

    get CSomGrille() {
        return this._CSomGrille;
    }

    get B() {
        return this._B;
    }

    get Beta() {
        return this._Beta;
    }

    get Alpha() {
        return this._Alpha;
    }

    get b() {
        return this._b;
    }

    get p() {
        return this._p;
    }

    get e() {
        return this._e;
    }

    get a() {
        return this._a;
    }

    get c() {
        return this._c;
    }

    get O() {
        return this._O;
    }

    get Ob() {
        return this._Ob;
    }

    get OEntH() {
        return this._OEntH;
    }

    get cIncl() {
        return this._cIncl;
    }
}
