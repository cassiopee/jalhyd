import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Interval } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { GrilleParams } from "../internal_modules";

export enum GrilleProfile {
    Rectangular,
    Hydrodynamic,
    Custom
}

export enum GrilleType {
    Conventional,
    Oriented,
    Inclined
}

export class Grille extends Nub implements Observer {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        H: "m",
        HG: "m",
        S: "m²",
        SPDG: "m²",
        VA: "m/s",
        VAPDG: "m/s",
        LG: "m",
        D: "m",
        DG: "m",
        SG: "m²",
        VN: "m/s",
        DH00: "cm",
        DH05: "cm",
        DH10: "cm",
        DH15: "cm",
        DH20: "cm",
        DH25: "cm",
        DH30: "cm",
        DH35: "cm",
        DH40: "cm",
        DH45: "cm",
        DH50: "cm",
        DH55: "cm",
        DH60: "cm"
    }

    constructor(prms: GrilleParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Grille);
        this._props.addObserver(this);
        this.type = GrilleType.Inclined;
        this.profile = GrilleProfile.Rectangular;
    }

    /**
     * paramètres castés au bon type
     */
    public get prms(): GrilleParams {
        return this._prms as GrilleParams;
    }

    public get gridType(): GrilleType {
        return this.getPropValue("gridType");
    }

    public get gridProfile(): GrilleProfile {
        return this.getPropValue("gridProfile");
    }

    public set type(type: GrilleType) {
        this.setPropValue("gridType", type);
    }

    public set profile(profile: GrilleProfile) {
        this.setPropValue("gridProfile", profile);
    }

    /** Coefficient de forme des barreaux a */
    public get a() {
        let v: number;
        const p = this.gridProfile;
        const t = this.gridType;
        if (p === GrilleProfile.Custom) {
            v = this.prms.a.v;
        } else {
            if (t === GrilleType.Inclined) {
                if (p === GrilleProfile.Rectangular) {
                    v = 3.85;
                } else if (p === GrilleProfile.Hydrodynamic) {
                    v = 2.10;
                }
            } else {
                if (p === GrilleProfile.Rectangular) {
                    v = 2.89;
                } else if (p === GrilleProfile.Hydrodynamic) {
                    v = 1.70;
                }
            }
        }
        return v;
    }

    /** Coefficient de forme des barreaux c */
    public get c() {
        let v: number;
        const p = this.gridProfile;
        const t = this.gridType;
        if (t === GrilleType.Oriented) {
            if (p === GrilleProfile.Rectangular) {
                v = 1.69;
            } else if (p === GrilleProfile.Hydrodynamic) {
                v = 2.78;
            } else if (p === GrilleProfile.Custom) {
                v = this.prms.c.v;
            }
        } else {
            throw new Error("Grille.c() : only oriented grids have a c coefficient");
        }
        return v;
    }

    public Calc(): Result {
        this.currentResultElement = this.Equation();
        return this.result;
    }

    public Equation(): Result {
        const r = this._result;
        const t = this.gridType;

        // Préconisations d'inclinaison
        if (this.gridPlanParamsOK) {
            if (t === GrilleType.Oriented) {
                if (this.prms.Alpha.v > 45) {
                    r.resultElement.addMessage(new Message(MessageCode.WARNING_GRILLE_ALPHA_GREATER_THAN_45));
                }
            }
            if (t === GrilleType.Inclined) {
                if (this.prms.Beta.v > 26) {
                    r.resultElement.addMessage(new Message(MessageCode.WARNING_GRILLE_BETA_GREATER_THAN_26));
                }
            }
        }

        // 1st calculation step

        // Hauteur d'eau
        const H = this.prms.CEau.v - this.prms.CRad.v;
        r.resultElement.values.H = H;
        // Hauteur de grille
        const HG = this.prms.CSomGrille.v - this.prms.CRad.v;
        r.resultElement.values.HG = HG;
        // Section d'approche de la prise d'eau
        const S = H * this.prms.B.v;
        r.resultElement.values.S = S;
        // Section d'approche du plan de grille
        const SPDG = HG * this.prms.B.v;
        r.resultElement.values.SPDG = SPDG;
        // Vitesse d'approche moyenne pour le débit maximum turbiné
        const VA = this.prms.QMax.v / S;
        r.resultElement.values.VA = VA;
        // Vitesse d'approche moyenne pour le débit maximum turbiné, en soustr. la partie sup. évent. obturée
        const VAPDG = this.prms.QMax.v / SPDG;
        r.resultElement.values.VAPDG = VAPDG;

        // 2nd calculation step
        if (this.gridPlanParamsOK) {
            let VN: number;

            if (t === GrilleType.Conventional || t === GrilleType.Inclined) {
                // Longueur de grille immergée
                const LG = HG / Math.sin(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.LG = LG;
                // Distance longitudinale entre le point émergent du plan de grille et le pied de grille
                const D = H / Math.tan(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.D = D;
                // Distance longitudinale entre le sommet immergé et le pied de grille
                const DG = HG / Math.tan(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.DG = DG;
                // Surface de grille immergée
                const SG = LG * this.prms.B.v;
                r.resultElement.values.SG = SG;
                // Vitesse normale moyenne pour le débit maximum turbiné
                VN = this.prms.QMax.v / SG;
                r.resultElement.values.VN = VN;
            }

            if (t === GrilleType.Oriented) {
                // Largeur du plan de grille
                const BG = this.prms.B.v / Math.sin(this.prms.Alpha.v * Math.PI / 180);
                // ... BG is not exposed as a result !
                // Surface de grille immergée
                const SG = BG * HG;
                r.resultElement.values.SG = SG;
                // Vitesse normale moyenne pour le débit maximum turbiné
                VN = this.prms.QMax.v / SG;
                r.resultElement.values.VN = VN;
            }

            // Préconisation pour éviter le placage des poissons sur le plan de grille
            if (VN > 0.5) {
                r.resultElement.addMessage(new Message(MessageCode.WARNING_GRILLE_VN_GREATER_THAN_05));
            }
        }

        // 3rd calculation step
        if (this.gridPlanParamsOK && this.gridParamsOK) {
            // Rapport de forme des barreaux
            const RFB = this.prms.b.v / this.prms.p.v;
            r.resultElement.values.RFB = RFB;
            // Rapport espacement/épaisseur des barreaux
            const REEB = this.prms.e.v / this.prms.b.v;
            r.resultElement.values.REEB = REEB;
            // Obstruction due aux barreaux seuls
            const OB = this.prms.b.v / (this.prms.b.v + this.prms.e.v);
            r.resultElement.values.OB = OB;

            // L'obstruction totale est-elle au moins égale à l'obstruction due aux barreaux ?
            if (this.prms.O.v < r.resultElement.values.OB) {
                r.resultElement.addMessage(new Message(MessageCode.WARNING_GRILLE_O_LOWER_THAN_OB));
            }

            // perte de charge
            this.calcDH(r, VAPDG);
        }

        return r;
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "gridType":
                    this.updateBarsCoefficientsDisplay();
                    if (data.value === GrilleType.Conventional) {
                        // adjust hidden params
                        this.prms.Beta.visible = true;
                        this.prms.Alpha.visible = false;
                        this.prms.O.visible = true;
                        this.prms.Ob.visible = false;
                        this.prms.OEntH.visible = false;
                        this.prms.cIncl.visible = false;
                        // set recommended values
                        this.prms.Alpha.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new Interval(45, 90));
                        this.prms.Alpha.domain.interval.setInterval(new Interval(0, 90));
                        this.prms.O.domain.interval.setInterval(new Interval(0.2, 0.6));
                    }
                    if (data.value === GrilleType.Oriented) {
                        // adjust hidden params
                        this.prms.Beta.visible = false;
                        this.prms.Alpha.visible = true;
                        this.prms.O.visible = true;
                        this.prms.Ob.visible = false;
                        this.prms.OEntH.visible = false;
                        this.prms.cIncl.visible = false;
                        // set recommended values
                        this.prms.Beta.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new Interval(0, 90));
                        this.prms.Alpha.domain.interval.setInterval(new Interval(30, 90));
                        this.prms.O.domain.interval.setInterval(new Interval(0.35, 0.6));
                    }
                    if (data.value === GrilleType.Inclined) {
                        // adjust hidden params
                        this.prms.Beta.visible = true;
                        this.prms.Alpha.visible = false;
                        this.prms.O.visible = false;
                        this.prms.Ob.visible = true;
                        this.prms.OEntH.visible = true;
                        this.prms.cIncl.visible = true;
                        // set recommended values
                        this.prms.Alpha.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new Interval(15, 90));
                        this.prms.Alpha.domain.interval.setInterval(new Interval(0, 90));
                    }
                    break;

                case "gridProfile":
                    this.updateBarsCoefficientsDisplay();
                    break;
            }
        }
    }

    protected updateBarsCoefficientsDisplay() {
        this.prms.a.visible = (this.gridProfile === GrilleProfile.Custom);
        this.prms.c.visible = (this.gridProfile === GrilleProfile.Custom && this.gridType === GrilleType.Oriented);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.QMax.calculability = ParamCalculability.FIXED;
        this.prms.CRad.calculability = ParamCalculability.FIXED;
        this.prms.CEau.calculability = ParamCalculability.FIXED;
        this.prms.CSomGrille.calculability = ParamCalculability.FIXED;
        this.prms.B.calculability = ParamCalculability.FIXED;
        this.prms.Beta.calculability = ParamCalculability.FIXED;
        this.prms.Alpha.calculability = ParamCalculability.FIXED;
        this.prms.b.calculability = ParamCalculability.FIXED;
        this.prms.p.calculability = ParamCalculability.FIXED;
        this.prms.e.calculability = ParamCalculability.FIXED;
        this.prms.a.calculability = ParamCalculability.FIXED;
        this.prms.c.calculability = ParamCalculability.FIXED;
        this.prms.O.calculability = ParamCalculability.FIXED;
        this.prms.Ob.calculability = ParamCalculability.FIXED;
        this.prms.OEntH.calculability = ParamCalculability.FIXED;
        this.prms.cIncl.calculability = ParamCalculability.FIXED;
    }

    public static override resultsUnits() {
        return Grille._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            H: undefined,
            HG: undefined,
            S: undefined,
            SPDG: undefined,
            VA: undefined,
            VAPDG: undefined,
            LG: undefined,
            D: undefined,
            DG: undefined,
            VN: undefined,
            SG: undefined,
            RFB: undefined,
            REEB: undefined,
            OB: undefined,
            a: undefined,
            c: undefined
        };
    }

    // no calculated param
    protected findCalculatedParameter(): any {
        return undefined;
    }

    // no calculated param
    protected doCalc(computedSymbol?: any, rInit?: number) {
        return this.Calc();
    }

    /**
     * Returns true if every "grid plan" parameter (2nd fieldset in NgHyd)
     * has a defined value; allows to perform 2nd calculation step
     */
    private get gridPlanParamsOK(): boolean {
        if (this.gridType === GrilleType.Oriented) {
            return this.prms.Alpha.singleValue !== undefined;
        } else {
            return this.prms.Beta.singleValue !== undefined;
        }
    }

    /**
     * Returns true if every "grid" parameter (3rd fieldset in NgHyd)
     * has a defined value; allows to perform 3rd calculation step
     */
    private get gridParamsOK(): boolean {
        let optionalFieldsOk: boolean = true;
        if (this.gridProfile === GrilleProfile.Custom) {
            optionalFieldsOk = optionalFieldsOk && (this.prms.a.singleValue !== undefined);
            if (this.gridType === GrilleType.Oriented) {
                optionalFieldsOk = optionalFieldsOk && (this.prms.c.singleValue !== undefined);
            }
        }
        if (this.gridType === GrilleType.Inclined) {
            optionalFieldsOk = (
                optionalFieldsOk
                && (this.prms.Ob.singleValue !== undefined)
                && (this.prms.OEntH.singleValue !== undefined)
                && (this.prms.cIncl.singleValue !== undefined)
            );
        } else { // type "Conventional" and "Oriented"
            optionalFieldsOk = (
                optionalFieldsOk
                && (this.prms.O.singleValue !== undefined)
            );
        }
        return (
            optionalFieldsOk
            // mandatory fields
            && this.prms.b.singleValue !== undefined
            && this.prms.p.singleValue !== undefined
            && this.prms.e.singleValue !== undefined
        );
    }

    /**
     * Perte de charge pour un taux de colmatage allant de 0 à 60% par pas de 5%.
     * @param r Result to complete with calculated DH values
     */
    private calcDH(r: Result, VAPDG: number) {
        const t = this.gridType;
        const g = 9.81;

        // add automatic bars profile coeficients to results ?
        if (this.gridProfile !== GrilleProfile.Custom) {
            r.resultElement.values.a = this.a;
            if (this.gridType === GrilleType.Oriented) {
                r.resultElement.values.c = this.c;
            }
        }

        for (const pct of [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]) {
            let KO: number;
            let XI: number;
            const VCSDG = (VAPDG * VAPDG) / (2 * g);

            switch (t) {
                case GrilleType.Conventional:
                    if (pct === 0) {
                        KO = Math.pow(this.prms.O.v / (1 - this.prms.O.v), 1.6);
                    } else {
                        // Obstruction totale
                        const OC = this.prms.O.v + ((1 - this.prms.O.v) * (pct / 100));
                        KO = Math.pow(OC / (1 - OC), 1.6);
                    }
                    const KB = Math.pow(1 - Math.cos(this.prms.Beta.v * Math.PI / 180), 0.39);
                    XI = this.a * KO * KB;
                    break;

                case GrilleType.Oriented:
                    let KA: number;
                    if (pct === 0) {
                        KO = Math.pow(this.prms.O.v / (1 - this.prms.O.v), 1.6);
                        KA = 1 + (
                            this.c * Math.pow((90 - this.prms.Alpha.v) / 90, 2.35)
                            * Math.pow((1 - this.prms.O.v) / this.prms.O.v, 3)
                        );
                    } else {
                        // Obstruction totale
                        const OC = this.prms.O.v + ((1 - this.prms.O.v) * (pct / 100));
                        KO = Math.pow(OC / (1 - OC), 1.6);
                        KA = 1 + (
                            this.c * Math.pow((90 - this.prms.Alpha.v) / 90, 2.35)
                            * Math.pow((1 - OC) / OC, 3)
                        );
                    }
                    XI = this.a * KO * KA;
                    break;

                case GrilleType.Inclined:
                    let KOB: number;
                    const SINBC = Math.pow(Math.sin(this.prms.Beta.v * Math.PI / 180), 2);
                    const KOENTH = Math.pow(this.prms.OEntH.v / (1 - this.prms.OEntH.v), 0.77);
                    if (pct === 0) {
                        KOB = Math.pow(this.prms.Ob.v / (1 - this.prms.Ob.v), 1.65);
                    } else {
                        // Obstruction totale
                        const OC = this.prms.Ob.v + ((1 - this.prms.Ob.v) * (pct / 100));
                        KOB = Math.pow(OC / (1 - OC), 1.65);
                    }
                    XI = this.a * KOB * SINBC + this.prms.cIncl.v * KOENTH;
                    break;
            }

            const DH = 100 * XI * VCSDG;

            // store as multiple result values
            let valueKey = String(pct);
            if (valueKey.length === 1) { // clodo padStart() for ES < 2017
                valueKey = "0" + valueKey;
            }
            valueKey = "DH" + valueKey;
            r.resultElement.values[valueKey] = DH;
        }
    }

}
