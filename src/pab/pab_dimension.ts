import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { PabDimensionParams } from "../internal_modules";

export class PabDimension extends Nub {
    constructor(prms: PabDimensionParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PabDimensions);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PabDimensionParams {
        return this._prms as PabDimensionParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "V":
                v = this.prms.L.v * this.prms.W.v * this.prms.Y.v;
                break;

            case "L":
                v = this.prms.V.v / this.prms.W.v / this.prms.Y.v;
                break;

            case "W":
                v = this.prms.V.v / this.prms.L.v / this.prms.Y.v;
                break;

            case "Y":
                v = this.prms.V.v / this.prms.L.v / this.prms.W.v;
                break;

            default:
                throw new Error("PabDimension.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.L.calculability = ParamCalculability.EQUATION;
        this.prms.W.calculability = ParamCalculability.EQUATION;
        this.prms.Y.calculability = ParamCalculability.EQUATION;
        this.prms.V.calculability = ParamCalculability.EQUATION;
    }

}
