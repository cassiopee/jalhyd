import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { PabPuissanceParams } from "../internal_modules";

export class PabPuissance extends Nub {
    constructor(prms: PabPuissanceParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PabPuissance);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PabPuissanceParams {
        return this._prms as PabPuissanceParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        const ro: number = 1000;     // masse volumique de l'eau en kg/m3
        const g: number = 9.81;     // accélération de la gravité terrestre en m/s2.

        switch (sVarCalc) {
            case "PV":
                v = ro * g * this.prms.Q.v * this.prms.DH.v / this.prms.V.v;
                break;

            case "Q":
                v = this.prms.PV.v * this.prms.V.v / (ro * g * this.prms.DH.v);
                break;

            case "V":
                v = ro * g * this.prms.Q.v * this.prms.DH.v / this.prms.PV.v;
                break;

            case "DH":
                v = this.prms.PV.v * this.prms.V.v / (ro * g * this.prms.Q.v);
                break;

            default:
                throw new Error("PabPuissance.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.DH.calculability = ParamCalculability.EQUATION;
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.V.calculability = ParamCalculability.EQUATION;
        this.prms.PV.calculability = ParamCalculability.EQUATION;
    }

}
