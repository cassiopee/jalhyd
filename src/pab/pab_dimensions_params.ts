import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PabDimensionParams extends ParamsEquation {

    /** Longueur L */
    private _L: ParamDefinition;

    /** Largeur W */
    private _W: ParamDefinition;

    /** Tirant d'eau Y */
    private _Y: ParamDefinition;

    /** Volume V */
    private _V: ParamDefinition;

    constructor(rL: number, rW: number, rY: number, rV?: number, nullParams: boolean = false) {
        super();
        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.LENGTHS, undefined, nullParams);
        this._W = new ParamDefinition(this, "W", ParamDomainValue.POS, "m", rW, ParamFamily.WIDTHS, undefined, nullParams);
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.POS, "m", rY, ParamFamily.HEIGHTS, undefined, nullParams);
        this._V = new ParamDefinition(this, "V", ParamDomainValue.POS, "m³", rV, ParamFamily.VOLUMES, undefined, nullParams);

        this.addParamDefinition(this._L);
        this.addParamDefinition(this._W);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._V);
    }

    get L() {
        return this._L;
    }

    get W() {
        return this._W;
    }

    get Y() {
        return this._Y;
    }

    get V() {
        return this._V;
    }
}
