import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PabPuissanceParams extends ParamsEquation {

    /** Chute entre bassins DH */
    private _DH: ParamDefinition;

    /** Débit Q */
    private _Q: ParamDefinition;

    /** Volume V */
    private _V: ParamDefinition;

    /** Puissance dissipée PV */
    private _PV: ParamDefinition;

    constructor(rDH: number, rQ: number, rV: number, rPV?: number, nullParams: boolean = false) {
        super();
        this._DH = new ParamDefinition(this, "DH", ParamDomainValue.POS, "m", rDH, ParamFamily.BASINFALLS, undefined, nullParams);
        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS_NULL, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this._V = new ParamDefinition(this, "V", ParamDomainValue.POS, "m³", rV, ParamFamily.VOLUMES, undefined, nullParams);
        this._PV = new ParamDefinition(this, "PV", ParamDomainValue.POS, "W/m³", rPV, undefined, undefined, nullParams);

        this.addParamDefinition(this._DH);
        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._V);
        this.addParamDefinition(this._PV);
    }

    get DH() {
        return this._DH;
    }

    get Q() {
        return this._Q;
    }

    get V() {
        return this._V;
    }

    get PV() {
        return this._PV;
    }
}
