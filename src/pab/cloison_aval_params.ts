import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParallelStructureParams } from "../internal_modules";

export class CloisonsAvalParams extends ParallelStructureParams {

    /** Cote radier amont paroi (m) */
    public ZRAM: ParamDefinition;

    constructor(rQ: number, rZ1: number, rZ2: number, rZRAM: number, nullParams: boolean = false) {
        super(rQ, rZ1, rZ2);
        this.ZRAM = new ParamDefinition(this, "ZRAM", ParamDomainValue.ANY, "m", rZRAM, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZRAM);
    }
}
