import { CalculatorType } from "../internal_modules";
import { Nub, Pab, Structure, StructureParams } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { ParallelStructure } from "../internal_modules";
import { StructureKiviParams } from "../internal_modules";
import { loiAdmissiblesCloisons, LoiDebit } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { CloisonsParams } from "../internal_modules";

export class Cloisons extends ParallelStructure {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        PV: "W/m³",
        ZRAM: "m",
        ZRMB: "m"
    };

    constructor(prms: CloisonsParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Cloisons);
        this._intlType = "Cloison";
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): CloisonsParams {
        return this._prms as CloisonsParams;
    }

    public getLoisAdmissibles(): { [key: string]: LoiDebit[]; } {
        return loiAdmissiblesCloisons;
    }

    /**
     * Is the Nub used in a PAB?
     */
    get isInPAB(): boolean {
        return (this.parent !== undefined && this.parent instanceof Pab);
    }

    public Equation(sVarCalc: string): Result {
        this.updatePrms();
        return super.Equation(sVarCalc);
    }

    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer :
     *                 "Q", "Z1", "DH" ou "n.X" avec "n" l'index de l'ouvrage et "X" son paramètre
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string | any, rInit?: number): Result {
        let sVC: string = sVarCalc;
        if (sVarCalc === "DH") {
            sVC = "Z2";
            rInit = this.prms.Z1.v - this.prms.DH.v;
        }
        this.updatePrms();
        const r: Result = super.Calc(sVC, rInit);

        if (!r.ok) { // Error during calculation
            return r;
        }

        // Transformation Z2 => DH
        if (sVarCalc === "DH") {
            r.vCalc = this.prms.Z1.v - r.vCalc;
        }

        // Ajout du calcul de la puissance dissipée
        const prms = this.getParamValuesAfterCalc(sVarCalc, r);
        const ro: number = 1000;     // masse volumique de l'eau en kg/m3
        const g: number = 9.81;     // accélération de la gravité terrestre en m/s2.
        r.resultElement.values.PV = ro * g * this.prms.Q.v * (prms.Z1 - prms.Z2) / (prms.LB * prms.BB * prms.PB);

        // Ajout de la cote de radier de bassin
        r.resultElement.values.ZRMB = this.prms.ZRMB.v;
        r.resultElement.values.ZRAM = this.prms.ZRAM.v;

        // Ajout de ZDV pour les seuils
        for (const s of this.structures) {
            if (s.prms.h1.visible) {
                s.result.resultElement.addExtraResult("ZDV", this.prms.Z1.v - s.prms.h1.v);
            }
            // calcul de la pelle
            if (s.getPropValue("loiDebit") !== LoiDebit.OrificeSubmerged) {
                const pelle = s.prms.ZDV.v - this.prms.ZRAM.v;
                s.result.resultElement.values.P = pelle;
                if (pelle < -1E-7) { // si c'est enfoncé d'un dixième de micron ça va
                    const m = new Message(MessageCode.WARNING_NEGATIVE_SILL);
                    s.result.resultElement.log.add(m);
                    // merge in parent for GUI display
                    this.result.resultElement.log.add(m);
                }
            }
        }

        // Ajout de la charge aval et de l'ennoiement pour tous les ouvrages sauf les orifices
        for (const st of this.structures) {
            if (!st.isOrifice) {
                const h1 = st.prms.h1.v;
                const h2 = st.prms.h2.v;
                st.result.resultElement.addExtraResult("H2", h2);
                st.result.resultElement.addExtraResult("SUBMERGENCE", Structure.computeSubmergencePercentage(h1, h2));
            }
        }

        return r;
    }

    public adjustChildParameters(child: Nub) {
        const prms = child.prms as StructureParams;
        if (!this.isInPAB && prms.ZDV.visible) {
            // Dans le contexte hors PAB
            // Pour les seuils (i.e. Structures avec cote de radier de seuil)
            // on remplace ZDV par h1 la charge sur le seuil
            prms.h1.visible = true;
            prms.ZDV.visible = false;
        }
        if (child.prms instanceof StructureKiviParams) {
            // hide ZRAM for KIVI, in Cloisons and PAB context only
            child.prms.ZRAM.visible = false;
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.LB.calculability = ParamCalculability.FREE;
        this.prms.BB.calculability = ParamCalculability.FREE;
        this.prms.PB.calculability = ParamCalculability.FREE;
        this.prms.DH.calculability = ParamCalculability.DICHO;
        this.prms.Z1.calculability = ParamCalculability.FREE;
        this.prms.ZRMB.calculability = ParamCalculability.FREE;
        this.prms.ZRAM.calculability = ParamCalculability.FREE;
        this.prms.QA.calculability = ParamCalculability.FREE;
    }

    public static override resultsUnits() {
        return Cloisons._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            PV: undefined,
            ZRAM: undefined,
            ZRMB: undefined,
            P: undefined
        };
    }

    private updatePrms() {
        if (this.prms.DH.visible !== false) {
            // if NONE => PAB context. it doesn't need update of elevations
            if (this.calculatedParam !== this.prms.DH) {
                // Z2 is the variable to find if DH is the calculated param
                this.prms.Z2.v = this.prms.Z1.v - this.prms.DH.v;
            }
            this.prms.ZRMB.v = this.prms.Z1.v - this.prms.PB.v - this.prms.DH.v;
            this.prms.ZRAM.v = this.prms.ZRMB.v + this.prms.DH.v / 2;
            for (const structure of this.structures) {
                const prms = structure.prms;
                if (prms.h1.visible) {
                    // MAJ de ZDV des seuils à partir de la charge
                    prms.ZDV.v = this.prms.Z1.v - prms.h1.v;
                }
                if (structure.prms instanceof StructureKiviParams) {
                    structure.prms.ZRAM.v = this.prms.Z1.v - this.prms.PB.v;
                }
            }
        }
    }
}
