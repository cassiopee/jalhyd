import { CalculatorType, Structure } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { ParallelStructure } from "../internal_modules";
import { loiAdmissiblesCloisonAval, LoiDebit } from "../internal_modules";
import { StructureVanLevLarinier } from "../internal_modules";
import { StructureVanLevVillemonte } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { CloisonsAvalParams } from "../internal_modules";

export class CloisonAval extends ParallelStructure {

    constructor(prms: CloisonsAvalParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.CloisonAval);
        this._intlType = "Cloison";
    }

    /**
     * Return index structure of vanne levante
     */
    public get indexVanneLevante(): number {
        for (let i = 0; i < this.structures.length; i++) {
            if (loiAdmissiblesCloisonAval.VanneLevante.includes(
                this.structures[i].getPropValue("loiDebit"))
            ) {
                return i;
            }
        }
        return null;
    }

    public get prms(): CloisonsAvalParams {
        return this._prms as CloisonsAvalParams;
    }

    /**
     * Returns admissible LoiDebit grouped by StructureType
     * Only one vanne levante is allowed on a CloisonAval
     */
    public getLoisAdmissibles(): { [key: string]: LoiDebit[]; } {
        return loiAdmissiblesCloisonAval;
    }

    /**
     * Calcul de la cote amont de la cloison
     * @param sVarCalc Nom du paramètre à calculer : seul Z1 est admis
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string | any, rInit?: number): Result {
        if (sVarCalc !== "Z1") {
            throw new Error("CloisonAval sVarCalc should be Z1");
        }
        if (!this.checkVanneLevante()) {
            this.currentResultElement.addMessage(new Message(MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
            return this.result;
        }
        let m: Message;
        if (this.hasVanneLevante()) {
            const s = this.structures[this.indexVanneLevante] as StructureVanLevVillemonte | StructureVanLevLarinier;
            this.prms.Z1.v = this.prms.Z2.v + s.getParameter("DH").v;
            this.currentResultElement = this.CalcStructPrm(this.indexVanneLevante, "ZDV");
            if (this.result.ok) {
                s.prms.ZDV.v = this.result.vCalc;
            }
            // Check if calculated ZDV is between minZDV and maxZDV
            if (this.result.vCalc < s.prms.minZDV.v) {
                m = new Message(MessageCode.WARNING_VANLEV_ZDV_INF_MIN);
                s.prms.ZDV.v = s.prms.minZDV.v;
            } else if (this.result.vCalc > s.prms.maxZDV.v) {
                m = new Message(MessageCode.WARNING_VANLEV_ZDV_SUP_MAX);
                s.prms.ZDV.v = s.prms.maxZDV.v;
            }
        }
        if (!this.hasVanneLevante() || this.result.ok) {
            // Calculation of Z1 with the new ZDV in case of existing vanne levante
            this.currentResultElement = super.Calc("Z1", rInit);
            if (this.result.ok) {
                this.getParameter(sVarCalc).v = this.result.vCalc;
                // Recalcul du débit total pour récupérer les résultats des ouvrages dans les résultats complémentaires
                const resQtot: Result = this.CalcQ();
                for (const extraResKey in resQtot.extraResults) {
                    if (resQtot.resultElement.values.hasOwnProperty(extraResKey)) {
                        this.result.resultElement.addExtraResult(
                            extraResKey, resQtot.resultElement.values[extraResKey]
                        );
                    }
                }
                if (this.hasVanneLevante()) {
                    this.result.resultElement.values.ZDV = this.structures[this.indexVanneLevante].prms.ZDV.v;
                    if (m !== undefined) {
                        this.result.resultElement.addMessage(m);
                    }
                }
            }
        }

        // Ajout de la charge amont/aval et de l'ennoiement pour tous les ouvrages sauf les orifices
        let prms: { [key: string]: number };
        for (const st of this.structures) {
            if (!st.isOrifice) {
                if (prms === undefined) {
                    prms = this.getParamValuesAfterCalc(sVarCalc, this.result);
                }
                const h1 = st.prms.h1.v;
                const h2 = st.prms.h2.v;
                st.result.resultElement.addExtraResult("H1", h1);
                st.result.resultElement.addExtraResult("H2", h2);
                st.result.resultElement.addExtraResult("SUBMERGENCE", Structure.computeSubmergencePercentage(h1, h2));
            }
        }

        return this.result;
    }

    /**
     * return false if there is more than one vanne levante in the cloison
     */
    public checkVanneLevante() {
        let n: number = 0;
        for (const st of this.structures) {
            if (loiAdmissiblesCloisonAval.VanneLevante.includes(st.getPropValue("loiDebit"))) {
                n += 1;
            }
        }
        return n < 2;
    }

    /**
     * Is there at least one Vanne Levante in the Cloison ?
     */
    public hasVanneLevante(): boolean {
        return !(this.indexVanneLevante === null);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.Z1.calculability = ParamCalculability.DICHO;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            ZDV: undefined
        };
    }
}
