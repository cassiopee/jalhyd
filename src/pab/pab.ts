import { CalculatorType } from "../internal_modules";
import { Session, JalhydObject } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability, ParamDefinition } from "../internal_modules";
import { IParamDefinitionIterator } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";
import { ParamsEquationArrayIterator } from "../internal_modules";
import { Props } from "../internal_modules";
import { ParallelStructure } from "../internal_modules";
import { StructureTriangularTruncWeirFree } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { ResultElement } from "../internal_modules";
import { CloisonAval } from "../internal_modules";
import { CloisonsAvalParams } from "../internal_modules";
import { Cloisons } from "../internal_modules";
import { PabParams } from "../internal_modules";
import { FishPass } from "../internal_modules";

export class Pab extends FishPass {

    /**
     * paramètres castés au bon type
     */
    get prms(): PabParams {
        return this._prms as PabParams;
    }

    /**
     * enfants castés au bon type
     */
    get children(): Cloisons[] {
        return this._children as Cloisons[];
    }

    /**
    * @returns iterator on all child nubs (may include extra nubs, see Pab nub)
    * @see Nub.directChildNubIterator()
    */
    public directChildNubIterator() {
        // clone children array
        const arr = Object.assign([], this.children);
        // append wall at downstream
        arr.push(this._downWall);
        // iterator
        return arr[Symbol.iterator]();
    }

    /**
     * Last wall at downstream
     */
    private _downWall: CloisonAval;

    constructor(prms: PabParams, downWall: CloisonAval, dbg: boolean = false) {
        super(prms, dbg);
        this.downWall = downWall;
        this.setCalculatorType(CalculatorType.Pab);
    }

    public get downWall() {
        return this._downWall;
    }

    public set downWall(dw: CloisonAval) {
        this._downWall = dw;
        if (dw) { // might be undefined
            dw.setParent(this); // important
            // postprocessing
            this.adjustDownwallParameters(this.downWall);
        }
    }

    /**
     * Add Cloisons to the PAB from a cloison model
     * @param cloisonModel Cloison model parametrised as first upstream basin
     * @param n Number of walls (or falls) of the PAB (Number of basin = n - 1)
     */
    public addCloisonsFromModel(cloisonModel: Cloisons, n: number) {
        // Fix some parameters of the upstream cloison (= Wall + basin)
        const DH: number = cloisonModel.prms.DH.currentValue;
        const ZRMB: number = this.prms.Z1.currentValue - cloisonModel.prms.PB.currentValue - DH;
        const ZRAM: number = ZRMB + DH / 2;

        // Generate an image of the object for multiplication
        // Use 2nd param as a trick to break links and copy values
        const serialisedCloisonModel = cloisonModel.serialise(undefined, [cloisonModel.uid]);
        for (let i = 0; i < n; i++) {
            const cl: Cloisons =
                Session.getInstance().unserialiseSingleNub(serialisedCloisonModel, false).nub as Cloisons;
            cl.setUid(JalhydObject.nextUID); // prevent all Cloisons having the same UID
            const p = cl.prms;
            // Copy calculated param to the new Cloison #130
            cl.calculatedParam.singleValue = cloisonModel.calculatedParam.V;
            p.ZRMB.singleValue = ZRMB - i * DH;
            p.ZRAM.singleValue = ZRAM - i * DH;
            p.QA.singleValue = 0; // set QA (attraction flow) to 0 (jalhyd #317)
            // Set Structure ZDVs
            for (const st of cl.structures) {
                if (st.isZDVcalculable) {
                    st.prms.ZDV.singleValue = (this.prms.Z1.currentValue - st.prms.h1.currentValue) - i * DH;
                    if (st.getParameter("ZT") !== undefined) {
                        const stTT = st as StructureTriangularTruncWeirFree;
                        stTT.prms.ZT.singleValue = stTT.prms.ZT.currentValue - i * DH;
                    }
                }
            }
            if (i !== n - 1) {
                // Add wall + basin = cloison
                this.addChild(cl);
            } else {
                // The last wall is a CloisonAval (= Wall only, without basin)
                const zram = this.children[this.children.length - 1].prms.ZRMB.singleValue
                    - this.children[this.children.length - 1].prms.DH.singleValue / 2;
                const dw = new CloisonAval(new CloisonsAvalParams(0, 0, 0, zram));
                dw.structures = cl.structures;
                this.downWall = dw;
            }
        }
    }

    public CalcSerie(rInit?: number): Result {
        if (!this.downWall.checkVanneLevante()) {
            this._result = new Result(undefined, this);
            this._result.globalLog.insert(new Message(MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
            return this._result;
        }
        return super.CalcSerie(rInit);
    }

    /**
     * @param sVarCalc Calcul
     */
    public Calc(sVarCalc: string, rInit?: number): Result {
        const r: Result = new Result(new ResultElement());
        if (!this.downWall.checkVanneLevante()) {
            r.resultElement.addMessage(new Message(MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
        }
        if (sVarCalc === "Q") {
            if (this.prms.Z1.v < this.prms.Z2.v) {
                r.resultElement.addMessage(new Message(MessageCode.ERROR_PAB_Z1_LOWER_THAN_Z2));
            }
            this.children[0].prms.Z1.v = this.prms.Z1.v;
            this.children[0].prms.Z2.v = -Infinity;
            if (this.children[0].CalcQ().vCalc < 1E-7) {
                r.resultElement.addMessage(new Message(MessageCode.ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL));
            }
        }
        if (r.hasErrorMessages()) {
            this.currentResultElement = r;
            return this.result;
        }
        return super.Calc(sVarCalc, rInit);
    }

    /**
     * add upstream/downstream head and submergence extra results to a parallel structure result
     * (except for orifices)
     */
    private addWallResults(cl: ParallelStructure) {
        for (const st of cl.structures) {
            if (!st.isOrifice) {
                const re: ResultElement = st.result.resultElement;

                const h1 = st.prms.h1.v;
                re.addExtraResult("H1", h1);

                const h2 = st.prms.h2.v;
                re.addExtraResult("H2", h2);

                const subm = st.result.values.SUBMERGENCE;
                re.addExtraResult("SUBMERGENCE", subm);
            }
        }
    }

    /**
     * Calcul analytique
     * @warning Should be called by this.Calc only for parameter initialisations
     * @param sVarCalc Variable à calculer (Z1 uniquement)
     */
    public Equation(sVarCalc: string): Result {
        const r: Result = new Result(0, this);

        // Up to down course : discharge distribution and bottom elevation
        if (this.children.length > 0) {
            this.children[0].prms.Q.v = this.prms.Q.v;
        }
        let l: number = 0; // Length of the fishway and wall abscissae
        for (let i = 0; i < this.children.length; i++) {
            let wall: CloisonAval | Cloisons;
            if (i !== this.children.length - 1) {
                wall = this.children[i + 1];
            } else {
                wall = this.downWall;
            }
            l += this.children[i].prms.LB.v;
            // Set discharge for the next wall from the current basin
            wall.prms.Q.v = this.children[i].prms.Q.v + this.children[i].prms.QA.v;
        }

        // Down to up course : water surface calculation
        let Z: number = this.prms.Z2.v;
        Z = this.calcCloisonZ1(this.downWall, Z);
        if (!this.downWall.result.ok) {
            return new Result(new Message(MessageCode.ERROR_PAB_CALC_Z1_CLOISON_DW), this);
        }
        this.downWall.result.resultElement.values.Q = this.downWall.prms.Q.v;
        this.downWall.result.resultElement.values.x = l;
        this.debug("Downstream wall");
        this.dbgWall(this.downWall);

        for (let i = this.children.length - 1; i >= 0; i--) {

            // Initialisations for current cloison
            const cl: Cloisons = this.children[i];

            // Calculation of upstream water elevation
            cl.prms.PB.v = Z - cl.prms.ZRMB.v;
            Z = this.calcCloisonZ1(cl, Z);
            if (!cl.result.ok) {
                const m = new Message(MessageCode.ERROR_PAB_CALC_Z1_CLOISON);
                m.extraVar.n = String(cl.findPositionInParent() + 1);
                return new Result(m, this);
            }
            // Add extraresults: mean depth in pool and discharge
            cl.result.resultElement.values.YMOY = cl.prms.PB.v;
            cl.result.resultElement.values.Q = cl.prms.Q.v;
            cl.result.resultElement.values.QA = cl.prms.QA.v;
            l -= cl.prms.LB.v;
            cl.result.resultElement.values.x = l;

            // Add h1, h2 and submergence to results
            this.addWallResults(cl);

            this.debug("Bassin n°" + i);
            this.dbgWall(cl);
        }

        // Add h1, h2 and submergence to results for downstream wall
        this.addWallResults(this._downWall);

        r.vCalc = Z;
        return r;
    }

    public get calculableParameters(): ParamDefinition[] {
        const calcPrms: ParamDefinition[] =
            super.calculableParameters.filter((p) => p.nubCalcType === CalculatorType.Pab);
        return calcPrms;
    }

    /**
     * Returns an iterator over :
     *  - own parameters (this._prms)
     *  - children parameters (this._children[*]._prms)
     *  Special treatment for PAB's downwall
     */
    public get parameterIterator(): IParamDefinitionIterator {
        const prms: ParamsEquation[] = [];
        prms.push(this._prms);
        if (this._children) {
            Nub.concatPrms(prms, this.childrenPrms);
        }
        if (this.downWall) {
            Nub.concatPrms(prms, this.downWall.childrenPrms);
        }
        return new ParamsEquationArrayIterator(prms);
    }

    /**
     * Returns an object representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     */
    public objectRepresentation(extra?: object) {
        // regular serialization
        const ret: any = super.objectRepresentation(extra, []);
        // downwall
        ret.downWall = this.downWall.objectRepresentation(undefined, []);
        return ret;
    }

    /**
     * Fills the current Nub with parameter values, provided an object representation
     * @param obj object representation of a Nub content (parameters)
     * @returns the calculated parameter found, if any - used by child Nub to notify
     *          its parent of the calculated parameter to set
     */
    public loadObjectRepresentation(obj: any): { p: ParamDefinition, hasErrors: boolean, changedUids: { [key: string]: string } } {
        // return value
        const ret: { p: ParamDefinition, hasErrors: boolean, changedUids: { [key: string]: string } } = super.loadObjectRepresentation(obj);
        // load downwall if any
        if (obj.downWall) {
            // decode properties
            const props = Props.invertEnumKeysAndValuesInProperties(obj.downWall.props, true);
            // create the Nub
            const dw = Session.getInstance().createNub(new Props(props), this) as CloisonAval;
            // try to keep the original ID
            if (!Session.getInstance().uidAlreadyUsed(obj.downWall.uid)) {
                dw.setUid(obj.downWall.uid);
            }
            const childRet = dw.loadObjectRepresentation(obj.downWall);
            // add downWall to parent
            this.downWall = dw;
            // forward errors
            if (childRet.hasErrors) {
                ret.hasErrors = true;
            }
        }
        return ret;
    }

    /**
     * Adds a new empty resultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all children and downWall
     */
    public initNewResultElement() {
        super.initNewResultElement();
        this.downWall.initNewResultElement();
    }

    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all children and downWall
     */
    public reinitResult() {
        super.reinitResult();
        this.downWall.reinitResult();
    }

    /**
     * Only Q and Z1 are calculable (see #108)
     */
    public findFirstCalculableParameter(otherThan?: ParamDefinition) {
        for (const p of [this.prms.Q, this.prms.Z1]) {
            if (p !== otherThan
                && p.valueMode !== ParamValueMode.CALCUL // event LINK might be reset here
            ) {
                return p;
            }
        }
        throw new Error(`PAB : no calculable parameter found other than ${otherThan ? otherThan.symbol : "undefined"}`);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.Z1.calculability = ParamCalculability.EQUATION;
        this.prms.Z2.calculability = ParamCalculability.FREE;
        this.prms.Q.calculability = ParamCalculability.DICHO;
    }

    /**
     * Remove Calculability of DH for not updating Z2 during calculation; adjust
     * visibility for PAB display and serialisation
     * @param child Cloison newly added to the PAB
     */
    protected adjustChildParameters(child: Cloisons) {
        child.prms.QA.visible = true;
        child.prms.ZRAM.visible = true;
        child.prms.ZRMB.visible = true;
        child.prms.Q.visible = false;
        child.prms.Z1.visible = false;
        child.prms.PB.visible = false;
        child.prms.DH.visible = false;
        for (const st of child.structures) {
            if (st.prms.h1.visible) {
                // Set parameter visibility for ZDV and h1 in PAB context
                st.prms.ZDV.visible = true;
                st.prms.h1.visible = false;
            }
        }
    }

    /**
     * Remove visibility of downwall hydraulic parameters, for serialisation
     * @param dw
     */
    protected adjustDownwallParameters(dw: CloisonAval) {
        dw.prms.Q.visible = false;
        dw.prms.Z1.visible = false;
        dw.prms.Z2.visible = false;
        for (const st of dw.structures) {
            if (st.prms.h1.visible) {
                // Set parameter visibility for ZDV and h1 in PAB context
                st.prms.ZDV.visible = true;
                st.prms.h1.visible = false;
            }
        }
    }

    private calcCloisonZ1(cl: CloisonAval | Cloisons, Z: number): number {
        // Initialisations for current cloison
        cl.prms.Z2.v = Z;

        // Calculation of upstream water elevation
        cl.Calc("Z1", Z + 0.1);

        // Fall on this wall
        cl.result.resultElement.values.DH = cl.result.vCalc - cl.prms.Z2.v;

        // Return Update elevation for next pool
        return cl.result.vCalc;
    }

    private dbgWall(cl: ParallelStructure) {
        if (!this.DBG) { return; }
        let s: string = "";
        for (const p of cl.prms) {
            s += p.symbol + " = " + p.v + "; ";
        }
        this.debug(s);

        for (const c of cl.getChildren()) {
            this.debug("Ouvrage");
            s = "";
            for (const p of c.prms) {
                if (p.visible) {
                    s += "*";
                }
                s += p.symbol + " = " + p.v + "; ";
            }
            this.debug(s);
        }
    }
}
