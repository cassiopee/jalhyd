import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PabChuteParams extends ParamsEquation {

    /** Cote amont Z1 */
    private _Z1: ParamDefinition;

    /** Cote aval Z2 */
    private _Z2: ParamDefinition;

    /** Chute DH */
    private _DH: ParamDefinition;

    constructor(rZ1: number, rZ2: number, rDH: number, nullParams: boolean = false) {
        super();
        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._DH = new ParamDefinition(this, "DH", ParamDomainValue.POS_NULL, "m", rDH, ParamFamily.TOTALFALLS, undefined, nullParams);

        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._DH);
    }

    get Z1() {
        return this._Z1;
    }

    get Z2() {
        return this._Z2;
    }

    get DH() {
        return this._DH;
    }
}
