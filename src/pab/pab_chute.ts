import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { PabChuteParams } from "../internal_modules";

export class PabChute extends Nub {
    constructor(prms: PabChuteParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PabChute);
        this._defaultCalculatedParam = prms.DH;
        this.resetDefaultCalculatedParam();
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PabChuteParams {
        return this._prms as PabChuteParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        if (!["Z1", "Z2"].includes(sVarCalc) && this.prms.Z1.v <= this.prms.Z2.v) {
            const m = new Message(MessageCode.ERROR_ELEVATION_ZI_LOWER_THAN_Z2);
            m.extraVar.Z1 = this.prms.Z1.v;
            m.extraVar.Z2 = this.prms.Z2.v;
            return new Result(m);
        }

        switch (sVarCalc) {
            case "Z1":
                v = this.prms.Z2.v + this.prms.DH.v;
                break;

            case "Z2":
                v = this.prms.Z1.v - this.prms.DH.v;
                break;

            case "DH":
                v = this.prms.Z1.v - this.prms.Z2.v;
                break;

            default:
                throw new Error("PabChute.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.Z1.calculability = ParamCalculability.EQUATION;
        this.prms.Z2.calculability = ParamCalculability.EQUATION;
        this.prms.DH.calculability = ParamCalculability.EQUATION;
    }

}
