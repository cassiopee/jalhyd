import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PabNombreParams extends ParamsEquation {

    /** Chute totale DHT */
    private _DHT: ParamDefinition;

    /** Nombre de chutes N */
    private _N: ParamDefinition;

    /** Chute entre bassins DH */
    private _DH: ParamDefinition;

    constructor(rDHT: number, rN: number, rDH: number, nullParams: boolean = false) {
        super();
        this._DHT = new ParamDefinition(this, "DHT", ParamDomainValue.POS, "m", rDHT, ParamFamily.TOTALFALLS, undefined, nullParams);
        this._N = new ParamDefinition(this, "N", ParamDomainValue.POS, undefined, rN, undefined, undefined, nullParams);
        this._DH = new ParamDefinition(this, "DH", ParamDomainValue.POS, "m", rDH, ParamFamily.BASINFALLS, undefined, nullParams);

        this.addParamDefinition(this._DHT);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._DH);
    }

    get DHT() {
        return this._DHT;
    }

    get N() {
        return this._N;
    }

    get DH() {
        return this._DH;
    }
}
