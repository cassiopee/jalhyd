import { floatDivAndMod, isEqual } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { PabNombreParams } from "../internal_modules";

export class PabNombre extends Nub {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        DHR: "m",
        DHB: "m",
        DHH: "m"
    }

    constructor(prms: PabNombreParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PabNombre);
        this._defaultCalculatedParam = prms.N;
        this.resetDefaultCalculatedParam();
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PabNombreParams {
        return this._prms as PabNombreParams;
    }

    public Equation(sVarCalc: string): Result {
        let rounded = false;
        if (sVarCalc !== "N" && !Number.isInteger(this.prms.N.v)) {
            this.prms.N.v = Math.round(this.prms.N.v);
            rounded = true;
        }

        let v: number;
        let DHR;
        let NB;
        let DHB;
        let NH;
        let DHH;

        switch (sVarCalc) {
            case "DHT":
                v = this.prms.N.v * this.prms.DH.v;
                break;

            case "N":
                const divAndMod = floatDivAndMod(this.prms.DHT.v, this.prms.DH.v);
                v = divAndMod.q;
                DHR = divAndMod.r;
                // harmonisation ?
                if (!isEqual(DHR, 0)) {
                    // vers le bas
                    if (v > 0) {
                        NB = v;
                        DHB = this.prms.DHT.v / NB;
                    }
                    // vers le haut
                    NH = v + 1;
                    DHH = this.prms.DHT.v / NH;
                }
                break;

            case "DH":
                v = this.prms.DHT.v / this.prms.N.v;
                break;

            default:
                throw new Error("PabNombre.Equation() : invalid variable name " + sVarCalc);
        }

        const r = new Result(v, this);
        if (DHR !== undefined) {
            r.resultElement.values.DHR = DHR;
        }
        if (NB !== undefined) {
            r.resultElement.values.NB = NB;
        }
        if (DHB !== undefined) {
            r.resultElement.values.DHB = DHB;
        }
        if (NH !== undefined) {
            r.resultElement.values.NH = NH;
        }
        if (DHH !== undefined) {
            r.resultElement.values.DHH = DHH;
        }

        if (rounded) {
            const m = new Message(MessageCode.WARNING_VALUE_ROUNDED_TO_INTEGER);
            m.extraVar.symbol = "N";
            m.extraVar.rounded = this.prms.N.v;
            r.resultElement.log.add(m);
        }

        return r;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.DHT.calculability = ParamCalculability.EQUATION;
        this.prms.N.calculability = ParamCalculability.EQUATION;
        this.prms.DH.calculability = ParamCalculability.EQUATION;
    }

    public static override resultsUnits() {
        return PabNombre._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            DHR: undefined,
            NB: undefined,
            NH: undefined,
            DHB: undefined,
            DHH: undefined
        };
    }
}
