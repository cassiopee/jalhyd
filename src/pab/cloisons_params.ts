import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParallelStructureParams } from "../internal_modules";

/**
 * Common parameters of hydraulic structure equations
 */
export class CloisonsParams extends ParallelStructureParams {
    /** Largeur des bassins (m) */
    public BB: ParamDefinition;

    /** Longueur des bassins (m) */
    public LB: ParamDefinition;

    /** Profondeur moyenne (m) */
    public PB: ParamDefinition;

    /** Hauteur de chute (m) */
    public DH: ParamDefinition;

    /** Cote radier mi-bassin (m) */
    public ZRMB: ParamDefinition;

    /** Cote radier amont paroi (m) */
    public ZRAM: ParamDefinition;

    /** Débit d'attrait (m3/s) */
    public QA: ParamDefinition;

    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rLB Longueur des bassins (m)
     * @param rBB Largeur des bassins (m)
     * @param rPB Profondeur moyenne (m)
     * @param rDH Hauteur de chute (m)
     */
    constructor(rQ: number, rZ1: number, rLB: number, rBB: number, rPB: number, rDH: number, nullParams: boolean = false) {
        super(rQ, rZ1, rZ1 - rDH);
        this.LB = new ParamDefinition(this, "LB", ParamDomainValue.POS, "m", rLB, ParamFamily.LENGTHS, undefined, nullParams);
        this.addParamDefinition(this.LB);
        this.BB = new ParamDefinition(this, "BB", ParamDomainValue.POS, "m", rBB, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this.BB);
        this.PB = new ParamDefinition(this, "PB", ParamDomainValue.POS, "m", rPB, ParamFamily.HEIGHTS, undefined, nullParams);
        this.addParamDefinition(this.PB);
        this.DH = new ParamDefinition(this, "DH", ParamDomainValue.POS, "m", rDH, ParamFamily.BASINFALLS, undefined, nullParams);
        this.addParamDefinition(this.DH);
        this.ZRMB = new ParamDefinition(this, "ZRMB", ParamDomainValue.ANY, "m", 0, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZRMB);
        this.ZRAM = new ParamDefinition(this, "ZRAM", ParamDomainValue.ANY, "m", 0, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZRAM);
        this.QA = new ParamDefinition(this, "QA", ParamDomainValue.ANY, "m³/s", 0, undefined, undefined, nullParams);
        this.addParamDefinition(this.QA);
        this.Z2.visible = false;
        this.ZRAM.visible = false;
        this.ZRMB.visible = false;
        this.QA.visible = false;
    }
}
