import { CalculatorType, ComputeNode, IProperties } from "./internal_modules";
import { Dichotomie } from "./internal_modules";
import {
    acSection, MacrorugoCompound, Pab, ParamDefinition, ParamsEquation,
    Session, Solveur, Structure, Verificateur
} from "./internal_modules";
import { LinkedValue } from "./internal_modules";
import { ParamCalculability, ParamFamily } from "./internal_modules";
import { ParamValueMode } from "./internal_modules";
import { IParamDefinitionIterator } from "./internal_modules";
import { ParamsEquationArrayIterator } from "./internal_modules";
import { Props } from "./internal_modules";
import { SessionSettings } from "./internal_modules";
import { Message, MessageCode } from "./internal_modules";
import { Observable, Observer } from "./internal_modules";
import { Result } from "./internal_modules";
import { ResultElement } from "./internal_modules";
import { VariatedDetails } from "./internal_modules";

class NubIterator implements IterableIterator<Nub> {
    private _nubs: Nub[];
    private _index: number;

    constructor(n: Nub) {
        this._nubs = [];
        this.fill(n);
        this._index = 0;
    }

    private fill(n: Nub) {
        this._nubs.push(n);
        for (const c of n.getChildren()) {
            this.fill(c);
        }
    }

    // interface IterableIterator

    public next(): IteratorResult<Nub> {
        const i = this._index;
        if (this._index < this._nubs.length) {
            this._index = i + 1;
            return {
                done: false,
                value: this._nubs[i]
            };
        } else {
            return {
                done: true,
                value: undefined
            };
        }
    }

    [Symbol.iterator](): IterableIterator<Nub> {
        return this;
    }
}

/**
 * Classe abstraite de Noeud de calcul dans une session :
 * classe de base pour tous les calculs
 */
export abstract class Nub extends ComputeNode implements IProperties {

    /**
     * Find longest series, BUT: if any varying parameter is a calculation result,
     * its values can't be extended (could be, but not at the moment), so
     * the global size in this case will be the min of sizes of linked variated results
     * @param variated list of variated parameter/valuesList pairs to examinate
     */
    public static findVariatedSize(variated: VariatedDetails[])
        : { size: number, longest: number, minLinkedResultParam: VariatedDetails } {
        if (variated.length > 0) {
            let minLinkedResultParam;
            let minLinkedResultsSize = Infinity;
            let longest = 0;
            for (let i = 0; i < variated.length; i++) {
                if (
                    variated[i].param.valueMode === ParamValueMode.LINK
                    && variated[i].param.referencedValue.isCalculated()
                ) {
                    if (variated[i].values.valuesIterator.count() < minLinkedResultsSize) {
                        minLinkedResultParam = variated[i];
                        minLinkedResultsSize = minLinkedResultParam.values.valuesIterator.count();
                    }
                }
                if (variated[i].values.valuesIterator.count() > variated[longest].values.valuesIterator.count()) {
                    longest = i;
                }
            }
            return {
                size: variated[longest].values.valuesIterator.count(),
                longest,
                minLinkedResultParam
            }
        } else {
            return {
                size: 0,
                longest: undefined,
                minLinkedResultParam: undefined
            }
        }
    }

    protected static concatPrms(p1: ParamsEquation[], p2: ParamsEquation[]): ParamsEquation[] {
        const p3: ParamsEquation[] = p1;
        for (const p of p2) {
            p3.push(p);
        }
        return p3;
    }

    private static progressPercentageAccordedToChainCalculation = 50;

    /** paramétrage de la dichotomie */
    public dichoStartIntervalMaxSteps: number = 100;

    /** pointer to parent Nub */
    private _parent: Nub;

    public get parent(): Nub {
        return this._parent;
    }

    public setParent(p: Nub) {
        this._parent = p;
    }

    /**
     * Used by GUI for translation of the nub type in ngHyd (snackbar, ...).
     * Formely childrenType, this was indicating type of children nubs.
     * Now the child nub itself declares its type.
     * Related to CalculatorType and INFO_CHILD_TYPE_* messages.
     */
    protected _intlType: string;

    public get intlType(): string {
        return this._intlType;
    }

    public get result(): Result {
        return this._result;
    }

    /**
     * Local setter to set result element of Equation() / Solve() / …  as current
     * ResultElement, instead of overwriting the whole Result object
     * (used by CalcSerie with varying parameters)
     */
    protected set currentResultElement(r: Result) {
        if (!this._result) {
            this.initNewResultElement();
        }
        this._result.resultElement = r.resultElement;
    }

    public set properties(props: Props) {
        this.setProperties(props);
    }

    /**
     * return ParamsEquation of all children recursively
     */
    public get childrenPrms(): ParamsEquation[] {
        const prms: ParamsEquation[] = [];
        if (this._children.length) { // if called within constructor, default class member value is not set yet
            for (const child of this._children) {
                prms.push(child.prms);
                if (child.getChildren()) {
                    if (child.getChildren().length) {
                        Nub.concatPrms(prms, child.childrenPrms);
                    }
                }
            }
        }
        return prms;
    }

    /**
     * Returns an array with the calculable parameters
     */
    public get calculableParameters(): ParamDefinition[] {
        const calcPrms: ParamDefinition[] = [];
        for (const p of this.parameterIterator) {
            if ([ParamCalculability.DICHO, ParamCalculability.EQUATION].includes(p.calculability)) {
                calcPrms.push(p);
            }
        }
        return calcPrms;
    }

    /**
     * Returns an iterator over :
     *  - own parameters (this._prms)
     *  - children parameters (this._children[*]._prms)
     */
    public get parameterIterator(): IParamDefinitionIterator {
        const prms: ParamsEquation[] = [];
        prms.push(this._prms);
        if (this._children) {
            Nub.concatPrms(prms, this.childrenPrms);
        }
        return new ParamsEquationArrayIterator(prms);
    }

    protected get progress() {
        return this._progress;
    }

    /**
     * Updates the progress percentage and notifies observers,
     * at most once per 300ms
     */
    protected set progress(v: number) {
        this._progress = v;
        const currentTime = new Date().getTime();
        if (
            (currentTime - this.previousNotificationTimestamp) > 30
            || v === 100
        ) {
            // console.log(">> notifying !");
            this.notifyProgressUpdated();
            this.previousNotificationTimestamp = currentTime;
        }
    }

    public get calcType(): CalculatorType {
        return this.getPropValue("calcType");
    }

    /**
     * set Nub calculator type.
     * give children the opportunity to react to assignment
     * @see Structure
     */
    protected setCalculatorType(ct: CalculatorType) {
        this.setPropValue("calcType", ct);
    }

    public get calculatedParam(): ParamDefinition {
        return this._calculatedParam;
    }

    /**
     * Sets p as the parameter to be computed; sets it to CALC mode
     */
    public set calculatedParam(p: ParamDefinition) {
        this._calculatedParam = p;
        this._calculatedParam.valueMode = ParamValueMode.CALCUL;
    }

    /**
     * Returns a parameter descriptor compatible with Calc() methods,
     * ie. a symbol string for a Nub's main parameter, or an object
     * of the form { uid: , symbol: } for a Nub's sub-Nub parameter
     * (ex: Structure parameter in ParallelStructure)
     */
    public get calculatedParamDescriptor(): string | { uid: string, symbol: string } {
        if (this.uid === this._calculatedParam.nubUid) {
            return this._calculatedParam.symbol;
        } else {
            return {
                uid: this._calculatedParam.nubUid,
                symbol: this._calculatedParam.symbol
            };
        }
    }

    /** parameter that is to be computed by default - to be overloaded by child classes */
    protected _defaultCalculatedParam: ParamDefinition;

    /** parameter that is to be computed */
    protected _calculatedParam: ParamDefinition;

    /**
     * List of children Nubs; browsed by parameters iterator.
     *  - for ParallelStructures: contains 0 or more Structures
     *  - for SectionNub : contains exactly 1 acSection
     */
    protected _children: Nub[];

    /** properties describing the Nub type */
    protected _props: Props = new Props();

    /** résultat de Calc()/CalcSerie() */
    protected _result: Result;

    /** implémentation par délégation de IObservable */
    private _observable: Observable;

    /** a rough indication of calculation progress, between 0 and 100 */
    private _progress: number = 0;

    /** allows notifying of progress every X milliseconds only */
    private previousNotificationTimestamp = 0;

    private _firstAnalyticalPrmSymbol: string;

    public constructor(prms: ParamsEquation, dbg: boolean = false) {
        super(prms, dbg);
        this.deleteAllChildren();
        this._observable = new Observable();
        this._defaultCalculatedParam = this.getFirstAnalyticalParameter();
        this.resetDefaultCalculatedParam();
    }

    // move code out of setter to ease inheritance
    public setProperties(props: IProperties, resetProps: boolean = false) {
        // copy observers
        const observers = this._props.getObservers();
        // empty props
        if (resetProps) {
            this._props.reset();
        }
        // restore observers
        for (const obs of observers) {
            this._props.addObserver(obs);
        }
        // set new props values
        let error: Error;
        for (const p of props.keys) {
            // try properties one by one so that if an error is thrown,
            // remaining properties are still copied
            let oldValue: any;
            try {
                // keep old value to restore it in case of an error, or else it breaks fixTargets()
                oldValue = this.getPropValue(p); // should always be undefined but who knows
                this.setPropValue(p, props.getPropValue(p));
            } catch (e) {
                error = e;
                // no one will notice ^^
                this.setPropValue(p, oldValue);
            }
        }
        // throw caught error if any
        if (error) {
            throw error;
        }
    }

    /**
     * Finds the previous calculated parameter and sets its mode to SINGLE
     */
    public unsetCalculatedParam(except: ParamDefinition) {
        for (const p of this.parameterIterator) {
            if (
                p.valueMode === ParamValueMode.CALCUL
                && p !== except
            ) {
                p.setValueMode(ParamValueMode.SINGLE, false);
            }
        }
    }

    /**
     * @param excluded parameter to exclude from search
     * @returns true if one of the parameters (other than excluded if provided) is in calculated mode
     */
    private hasCalculedParam(excluded?: ParamDefinition): boolean {
        for (const p of this._prms) {
            if (p.valueMode == ParamValueMode.CALCUL && (p !== excluded || excluded === undefined)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tries to reset the calculated parameter, successively, to :
     *  - the default one if it is in SINGLE mode
     *  - the first SINGLE calculable parameter other than requirer
     *  - the first MINMAX/LISTE calculable parameter other than requirer
     *
     * If no default calculated parameter is defined, does nothing.
     * 
     * @param force checking of non pre-existing calculated parameter
     */
    public resetDefaultCalculatedParam(requirer?: ParamDefinition, force: boolean = true) {
        if (this._defaultCalculatedParam) {
            // if forced, first check there is not already another parameter
            // in calculated mode (eg. in a sibling nub) other than requirer (if provided)
            let doReset = true;
            if (!force) {
                // children (except for Pab: in this case, child parameters
                // can be calculated and must not prevent reset)
                if (!(this instanceof Pab)) {
                    for (const kid of this.getChildren()) {
                        doReset = doReset && !kid.hasCalculedParam(requirer);
                    }
                }

                doReset = doReset && !this.hasCalculedParam(requirer);
            }

            if (doReset) {
                // if default calculated param is not eligible to CALC mode
                if (
                    requirer === this._defaultCalculatedParam
                    || this._defaultCalculatedParam.valueMode === ParamValueMode.LINK
                ) {
                    // first SINGLE calculable parameter if any
                    const newCalculatedParam = this.findFirstCalculableParameter(requirer);
                    if (newCalculatedParam) {
                        this.calculatedParam = newCalculatedParam;
                    } else {
                        // @TODO throws when a new linkable nub is added, and all parameters are already linked !
                        throw Error("resetDefaultCalculatedParam : could not find any SINGLE/MINMAX/LISTE parameter");
                    }
                } else {
                    // default one
                    this.calculatedParam = this._defaultCalculatedParam;
                }
            }
        } else {
            // do nothing (ex: Section Paramétrée)
        }
    }

    /**
     * Returns the first visible calculable parameter other than otherThan,
     * that is set to SINGLE, MINMAX or LISTE mode (there might be none)
     */
    public findFirstCalculableParameter(otherThan?: ParamDefinition) {
        for (const p of this.parameterIterator) {
            if (
                [ParamCalculability.EQUATION, ParamCalculability.DICHO].includes(p.calculability)
                && p.visible
                && p !== otherThan
                && [ParamValueMode.SINGLE, ParamValueMode.MINMAX, ParamValueMode.LISTE].includes(p.valueMode)
            ) {
                return p;
            }
        }
        return undefined;
    }

    /**
     * Formule utilisée pour le calcul analytique (solution directe ou méthode de résolution spécifique)
     */
    public abstract Equation(sVarCalc: string): Result;

    /**
     * Calculate and put in cache the symbol of first parameter calculable analytically
     */
    get firstAnalyticalPrmSymbol(): string {
        if (this._firstAnalyticalPrmSymbol === undefined) {
            this._firstAnalyticalPrmSymbol = this.getFirstAnalyticalParameter().symbol;
        }
        return this._firstAnalyticalPrmSymbol;
    }

    /**
     * Run Equation with first analytical parameter to compute
     * Returns the result in number form
     */
    public EquationFirstAnalyticalParameter(): number {
        const res = this.Equation(this.firstAnalyticalPrmSymbol);
        if (!res.ok) {
            this._result = res;
            throw new Error(this.constructor.name + ".EquationFirstAnalyticalParameter(): fail");
        }
        return res.vCalc;
    }

    /**
     * Calcul d'une équation quelle que soit l'inconnue à calculer; déclenche le calcul en
     * chaîne des modules en amont si nécessaire
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    public Calc(sVarCalc?: string, rInit?: number): Result {
        let computedVar;
        // overload calculated param at execution time ?
        if (sVarCalc) {
            computedVar = this.getParameter(sVarCalc);
        } else {
            computedVar = this.calculatedParam;
        }

        if (rInit === undefined) {
            rInit = computedVar.initValue;
        }
        if (computedVar.isAnalytical()) {
            this.currentResultElement = this.Equation(sVarCalc);
            this._result.symbol = sVarCalc;
            return this._result;
        }

        const resSolve: Result = this.Solve(sVarCalc, rInit);
        if (!resSolve.ok) {
            this.currentResultElement = resSolve;
            this._result.symbol = sVarCalc;
            return this._result;
        }
        const sAnalyticalPrm: string = this.getFirstAnalyticalParameter().symbol;
        computedVar.v = resSolve.vCalc;
        const res: Result = this.Equation(sAnalyticalPrm);
        res.vCalc = resSolve.vCalc;

        this.currentResultElement = res;
        this._result.symbol = sVarCalc;

        this.notifyResultUpdated();

        return this._result;
    }

    /**
     * Calculates required Nubs so that all input data is available;
     * uses 50% of the progress
     * @returns true if everything went OK, false otherwise
     */
    public triggerChainCalculation(): { ok: boolean, message: Message } {
        const requiredNubs1stLevel = this.getRequiredNubs();
        if (requiredNubs1stLevel.length > 0) {
            const progressStep = Nub.progressPercentageAccordedToChainCalculation / requiredNubs1stLevel.length;
            for (const rn of requiredNubs1stLevel) {
                const r = rn.CalcSerie(undefined, false);
                if (r.hasGlobalError() || r.hasOnlyErrors) {
                    // something has failed in chain
                    return {
                        ok: false,
                        message: new Message(MessageCode.ERROR_IN_CALC_CHAIN)
                    };
                } else if (
                    this.resultHasMultipleValues()
                    && (
                        r.hasErrorMessages() // some steps failed
                        // or upstream Nub has already triggered a warning message; pass it on
                        || r.globalLog.contains(MessageCode.WARNING_ERROR_IN_CALC_CHAIN_STEPS)
                    )
                ) {
                    // if a parameter varies, errors might have occurred for
                    // certain steps (but not all steps)
                    return {
                        ok: true,
                        message: new Message(MessageCode.WARNING_ERROR_IN_CALC_CHAIN_STEPS)
                    };
                }
                this.progress += progressStep;
            }
            // round progress to accorded percentage
            this.progress = Nub.progressPercentageAccordedToChainCalculation;
        }
        return {
            ok: true,
            message: undefined
        };
    }

    /**
     * Returns a list of parameters that are fixed, either because their valueMode
     * is SINGLE, or because their valueMode is LINK and the reference is
     * defined and fixed. Does not take calculated parameters into account.
     */
    public findFixedParams(): ParamDefinition[] {
        const fixed: ParamDefinition[] = [];
        for (const p of this.parameterIterator) {
            if (
                p.visible
                && (
                    p.valueMode === ParamValueMode.SINGLE
                    || (
                        p.valueMode === ParamValueMode.LINK
                        && p.isReferenceDefined()
                        && !p.referencedValue.hasMultipleValues()
                    )
                )
            ) {
                fixed.push(p);
            }
        }
        return fixed;
    }

    /**
     * Returns a list of parameters that are variating, either because their valueMode
     * is LISTE or MINMAX, or because their valueMode is LINK and the reference is
     * defined and variated. Does not take calculated parameters into account.
     */
    public findVariatedParams(): VariatedDetails[] {
        const variated: VariatedDetails[] = [];
        for (const p of this.parameterIterator) {
            if (
                p.valueMode === ParamValueMode.LISTE
                || p.valueMode === ParamValueMode.MINMAX
                || (
                    p.valueMode === ParamValueMode.LINK
                    && p.isReferenceDefined()
                    && p.referencedValue.hasMultipleValues()
                )
            ) {
                variated.push({
                    param: p,
                    // extract variated values from variated Parameters
                    // (in LINK mode, proxies to target data)
                    values: p.paramValues
                });
            }
        }
        return variated;
    }

    /**
    * compute log stats (# of error, warning, info) on all result elements for all nub (source and source's children)
    */
    public resultElementsLogStats(stats?: any): any {
        stats = this._result.resultElementsLogStats(0, stats);

        let sn = 1;
        const it = this.directChildNubIterator();
        let icn = it.next();
        while (!icn.done) {
            stats = icn.value.result.resultElementsLogStats(sn++, stats);
            icn = it.next();
        }
        return stats;
    }

    /**
     * abstract of iterations logs
     */
    private generateAbstractLogMessages() {
        const stats = this.resultElementsLogStats();

        // String()ify numbers below, to avoid decimals formatting on screen (ex: "3.000 errors encoutered...")
        if (stats.error > 0) {
            if (stats.error > 1) {
                this._result.globalLog.add(
                    new Message(MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, { nb: String(stats.error) })
                );
            } else {
                this._result.globalLog.add(
                    new Message(MessageCode.WARNING_ERRORS_ABSTRACT, { nb: String(stats.error) }) // should always be "1"
                );
            }
        }
        if (stats.warning > 0) {
            this._result.globalLog.add(
                new Message(MessageCode.WARNING_WARNINGS_ABSTRACT, { nb: String(stats.warning) })
            );
        }
    }

    /**
     * Effectue une série de calculs sur un paramètre; déclenche le calcul en chaîne
     * des modules en amont si nécessaire
     * @param rInit solution approximative du paramètre
     * @param resetDepending true pour resetResult() sur les Nubs dépendants également
     */
    public CalcSerie(rInit?: number, resetDepending: boolean = true): Result {
        // prepare calculation
        let extraLogMessage: Message; // potential chain calculation warning to add to result at the end
        this.progress = 0;
        this.resetResult([], resetDepending);
        const ccRes = this.triggerChainCalculation();
        if (ccRes.ok) {
            // might still have a warning log
            if (ccRes.message !== undefined) {
                extraLogMessage = ccRes.message;
            }
        } else {
            // something went wrong in the chain
            this._result = new Result(undefined, this);
            this._result.globalLog.add(ccRes.message);
            return this._result;
        }
        this.copySingleValuesToSandboxValues();

        // variated parameters caracteristics
        const variated: VariatedDetails[] = this.findVariatedParams();

        // find calculated parameter
        const computedSymbol = this.findCalculatedParameter();

        if (rInit === undefined && this.calculatedParam) {
            rInit = this.calculatedParam.v;
        }

        // reinit Result and children Results
        this.reinitResult();

        if (variated.length === 0) { // no parameter is varying
            // prepare a new slot to store results
            this.initNewResultElement();
            this.doCalc(computedSymbol, rInit); // résultat dans this.currentResult (resultElement le plus récent)
            this.progress = 100;

        } else { // at least one parameter is varying
            const fvsRes = Nub.findVariatedSize(variated);
            let size = fvsRes.size;
            const longest = fvsRes.longest;
            // if at least one linked variated result was found
            if (fvsRes.minLinkedResultParam !== undefined) {
                // if the size limited by linked variated results is shorter
                // than the size of the longest variating element, limit it
                if (fvsRes.minLinkedResultParam.values.valuesIterator.count() < size) {
                    size = fvsRes.minLinkedResultParam.values.valuesIterator.count();
                    // and add a warning
                    const m = new Message(MessageCode.WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT);
                    m.extraVar.symbol = fvsRes.minLinkedResultParam.param.symbol;
                    m.extraVar.size = size;
                    this._result.globalLog.add(m);
                }
            }

            // grant the remaining percentage of the progressbar to local calculation
            // (should be 50% if any chain calculation occurred, 100% otherwise)
            let progressStep;
            const remainingProgress = 100 - this.progress;
            progressStep = remainingProgress / size;

            // (re)init all iterators
            for (const v of variated) {
                // copy iterators, for linked params @see bug #222
                if (v === variated[longest]) {
                    v.iterator = v.values.initValuesIterator(false, undefined, true);
                } else {
                    v.iterator = v.values.initValuesIterator(false, size, true);
                }
            }

            // iterate over longest series (in fact any series would do)
            let l = 0; // extra counter if size is limited by linked variated results
            while (variated[longest].values.hasNext && l < size) {
                // get next value for all variating parameters
                for (const v of variated) {
                    const currentIteratorValue = v.iterator.nextValue();
                    v.param.v = currentIteratorValue.value;
                }
                // prepare a new slot to store results
                this.initNewResultElement();
                // calculate
                this.doCalc(computedSymbol, rInit); // résultat dans this.currentResult (resultElement le plus récent)
                if (this._result.resultElement.ok) {
                    rInit = this._result.resultElement.vCalc;
                }
                // update progress
                this.progress += progressStep;
                l++;
            }
            // round progress to 100%
            this.progress = 100;

            this.generateAbstractLogMessages();
        }

        if (computedSymbol !== undefined) {
            const realSymbol = (typeof computedSymbol === "string") ? computedSymbol : computedSymbol.symbol;
            this._result.symbol = realSymbol;
        }

        this.notifyResultUpdated();

        if (extraLogMessage !== undefined) {
            this._result.globalLog.add(extraLogMessage);
        }

        return this._result;
    }

    public addChild(child: Nub, after?: number) {
        if (after !== undefined) {
            this._children.splice(after + 1, 0, child);
        } else {
            this._children.push(child);
        }
        // add reference to parent collection (this)
        child.setParent(this);
        // postprocessing
        this.adjustChildParameters(child);
    }

    public getChildren(): Nub[] {
        return this._children;
    }

    /**
     * @returns iterator on direct child nubs (may include extra nubs, see Pab nub)
     */
    public directChildNubIterator() {
        return this._children[Symbol.iterator]();
    }

    /**
     * @returns iterator on all child nubs (recursively)
     */
    public get allChildNubIterator() {
        return new NubIterator(this);
    }

    /**
     * Returns true if all parameters are valid; used to check validity of
     * parameters linked to Nub results
     */
    public isComputable() {
        let valid = true;
        for (const p of this.prms) {
            valid = valid && p.isValid;
        }
        return valid;
    }

    /**
     * @returns true if nub is an orifice structure
     */
    public get isOrifice(): boolean {
        if (this._prms.hasParameter("h1") && this._prms.hasParameter("ZDV")) {
            return !this.prms.get("h1").visible && !this.prms.get("ZDV").visible;

        }
        return false;
    }

    /**
     * Liste des valeurs (paramètre, résultat, résultat complémentaire) liables à un paramètre
     * @param src paramètre auquel lier d'autres valeurs
     */
    public getLinkableValues(src: ParamDefinition): LinkedValue[] {
        let res: LinkedValue[] = [];
        const symbol = src.symbol;

        // If parameter comes from the same Nub, its parent or any of its children,
        // no linking is possible at all.
        // Different Structures in the same parent can get linked to each other.
        if (
            !this.isParentOrChildOf(src.nubUid)
            && ( // check grand-parent for PreBarrage special case @TODO find a generic way
                // to perform both tests (synthesise .nubUid and .originNub.uid)
                !(this.calcType === CalculatorType.PreBarrage)
                || !this.isParentOrChildOf(src.originNub.uid)
            )
        ) {
            // 1. own parameters
            for (const p of this._prms) {
                // if symbol and Nub type are identical
                if (
                    (p.symbol === symbol && this.calcType === src.nubCalcType && p.visible)
                    // or if this is a Section, and src direct parent is a Section too, and symbol is identical
                    || (
                        p.symbol === symbol
                        && this instanceof acSection && src.getParentComputeNode(false) instanceof acSection
                        && p.visible // different types of sections hide certain parameters
                    )
                    // or if family is identical
                    || (p.family !== undefined && p.family === src.family && p.visible)
                    // or if one of the families is ANY and target parameter is visible
                    || (
                        (src.family === ParamFamily.ANY || p.family === ParamFamily.ANY)
                        && p.visible
                    )
                ) {
                    // if variability doesn't cause any problem (a non-variable
                    // parameter cannot be linked to a variating one)
                    if (src.calculability !== ParamCalculability.FIXED || !p.hasMultipleValues) {
                        // if it is safe to link p's value to src
                        if (p.isLinkableTo(src)) {
                            // if p is a CALC param of a Structure other than "Q"
                            // (structures always have Q as CALC param and cannot have another)
                            // or a CALC param of a Section, that is not sibling of the target
                            // (to prevent circular dependencies among ParallelStructures),
                            // expose its parent
                            if (
                                (
                                    (this instanceof Structure && p.symbol !== "Q" && !this.isSiblingOf(src.nubUid))
                                    || this instanceof acSection
                                )
                                && (p.valueMode === ParamValueMode.CALCUL)
                            ) {
                                if (this.parent._prms.hasParameter(symbol)) {
                                    // trick to expose p as a result of the parent Nub
                                    res.push(new LinkedValue(this.parent, p, p.symbol));
                                }
                                else {
                                    // in case parent does not hold parameter, inform about real owning nub (this)
                                    res.push(new LinkedValue(this.parent, p, p.symbol, undefined, this));
                                }
                            } else {
                                // do not suggest parameters that are already linked to another one
                                if (p.valueMode !== ParamValueMode.LINK) {
                                    res.push(new LinkedValue(this, p, p.symbol));
                                }
                            }
                        }
                    }
                }
            }

            // 2. extra results
            if (this._resultsFamilies) {
                // if I don't depend on your result, you may depend on mine !
                if (!this.dependsOnNubResult(src.parentNub)) {
                    const erk = Object.keys(this._resultsFamilies);
                    // browse extra results
                    for (const erSymbol of erk) {
                        const erFamily = this._resultsFamilies[erSymbol];
                        // if family is identical or ANY, and variability doesn't cause any problem
                        if (
                            (
                                ( // both families cannot be undefined
                                    erFamily === src.family
                                    && erFamily !== undefined
                                )
                                || erFamily === ParamFamily.ANY
                                || src.family === ParamFamily.ANY
                            )
                            && (
                                src.calculability !== ParamCalculability.FIXED
                                || !this.resultHasMultipleValues()
                            )
                        ) {
                            res.push(new LinkedValue(this, undefined, erSymbol));
                        }
                    }
                }
            }
        }

        // 3. children Nubs, except for PAB and MRC and PreBarrage
        if (
            !(this instanceof MacrorugoCompound)
            && ( // meta-except, if source param in a PAB's Cloison (should only apply to QA)
                !(this instanceof Pab)
                || (
                    src.originNub instanceof Pab
                    && src.symbol === "QA"
                )
            )
            && this.calcType !== CalculatorType.PreBarrage
        ) {
            for (const cn of this.getChildren()) {
                res = res.concat(cn.getLinkableValues(src));
            }
        }

        return res;
    }

    /**
     * Returns true if the given Nub UID is either of :
     *  - the current Nub UID
     *  - the current Nub's parent Nub UID
     *  - the UID of any of the current Nub's children
     */
    public isParentOrChildOf(uid: string): boolean {
        if (this.uid === uid) {
            return true;
        }
        if (this._parent && this._parent.uid === uid) {
            return true;
        }
        for (const c of this.getChildren()) {
            if (c.uid === uid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if the given Nub UID :
     *  - is the current Nub UID
     *  - is the UID of any of the current Nub's siblings (children of the same parent)
     */
    public isSiblingOf(uid: string): boolean {
        if (this.uid === uid) {
            return true;
        }
        if (this._parent) {
            for (const c of this._parent.getChildren()) {
                if (c.uid === uid) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Returns all Nubs whose results are required by the given one,
     * without following links (stops when it finds a Nub that has to
     * be calculated). Used to trigger chain calculation.
     */
    public getRequiredNubs(visited: string[] = []): Nub[] {
        let requiredNubs: Nub[] = [];
        // prevent loops
        if (!visited.includes(this.uid)) {
            visited.push(this.uid);

            // inspect all target Nubs
            for (const p of this.parameterIterator) {
                if (p.valueMode === ParamValueMode.LINK && p.isReferenceDefined()) {
                    const nub = p.referencedValue.nub;

                    // a Nub is required if I depend on its result
                    if (this.dependsOnNubResult(nub, false)) {
                        requiredNubs.push(nub);
                    }
                }
            }
        }
        // 1. deduplicate
        requiredNubs = requiredNubs.filter(
            (element, index, self) => self.findIndex((e) => e.uid === element.uid) === index
        );
        // 2. only keep requiredNubs that are not dependencies of others
        const realRequiredNubs: Nub[] = [];
        for (const rn of requiredNubs) {
            let keep = true;
            // test all combinations
            for (const rn2 of requiredNubs) {
                if (rn.uid !== rn2.uid) {
                    keep = (
                        keep
                        && !rn2.dependsOnNubResult(rn, true)
                    );
                }
            }
            if (keep) {
                realRequiredNubs.push(rn);
            }
        }

        return realRequiredNubs;
    }

    /**
     * Returns all Nubs whose results are required by the given one,
     * following links. Used by Solveur.
     */
    public getRequiredNubsDeep(visited: string[] = []): Nub[] {
        let requiredNubs: Nub[] = this.getRequiredNubs(visited);
        for (const rn of requiredNubs) {
            requiredNubs = requiredNubs.concat(rn.getRequiredNubsDeep(visited));
        }
        requiredNubs = requiredNubs.filter(
            (item, index) => requiredNubs.indexOf(item) === index // deduplicate
        );
        return requiredNubs;
    }

    /**
     * Returns all Nubs whose parameters or results are targetted
     * by the given one.
     * (used for dependencies checking at session saving time)
     */
    public getTargettedNubs(visited: string[] = []) {
        const targettedNubs: Nub[] = [];
        // prevent loops
        if (!visited.includes(this.uid)) {
            visited.push(this.uid);

            // inspect all target Nubs
            for (const p of this.parameterIterator) {
                if (p.valueMode === ParamValueMode.LINK && p.isReferenceDefined()) {
                    targettedNubs.push(p.referencedValue.nub);
                }
            }
        }
        return targettedNubs;
    }

    /**
     * Returns true if
     *  - this Nub
     *  - any of this Nub's children
     *  - this Nub's parent
     * depends on
     *  - the result of the given Nub
     *  - the result of any of the given Nub's children
     *  [ note: but NOT the result of the given Nub's parent, or the given Nub might
     *    be recalculated independently, which is not wanted ]
     *
     * (ie. "my family depends on the result of your family")
     *
     * If followLinksChain is false, will limit the exploration to the first target level only
     */
    public dependsOnNubResult(nub: Nub, followLinksChain: boolean = true): boolean {
        let thisFamily: Nub[] = [this];
        if (this._parent) {
            thisFamily = thisFamily.concat(this._parent);
        }
        thisFamily = thisFamily.concat(this.getChildren());

        let yourFamily: Nub[] = [];
        const you = nub;
        if (you) {
            yourFamily = [you].concat(you.getChildren());
        }

        let depends = false;

        outerloop:
        for (const t of thisFamily) {
            // look for a parameter linked to the result of any Nub of yourFamily
            for (const p of t.prms) {
                if (p.valueMode === ParamValueMode.LINK) {
                    if (p.isLinkedToResultOfNubs(yourFamily, followLinksChain)) {
                        // dependence found !
                        depends = true;
                        break outerloop;
                    }
                }
            }
        }

        return depends;
    }

    /**
     * Returns true if this nub (parent and children included)
     * directly requires (1st level only)
     * the given parameter
     */
    public dependsOnParameter(src: ParamDefinition) {
        let thisFamily: Nub[] = [this];
        if (this._parent) {
            thisFamily = thisFamily.concat(this._parent);
        }
        thisFamily = thisFamily.concat(this.getChildren());

        let depends = false;

        outerloop:
        for (const t of thisFamily) {
            // look for a parameter of thisFamily that is directly linked to src
            for (const p of t.prms) {
                if (
                    p.valueMode === ParamValueMode.LINK
                    && p.isReferenceDefined()
                    && p.referencedValue.isParameter()
                ) {
                    if (
                        p.referencedValue.nub.uid === src.nubUid
                        && p.referencedValue.symbol === src.symbol
                    ) {
                        // dependence found !
                        depends = true;
                        break outerloop;
                    }
                }
            }
        }

        return depends;
    }

    /**
     * Returns true if the computation of the current Nub (parent and children
     * included) directly requires (1st level only), anything (parameter or result)
     * from the given Nub UID "uid", its parent or any of its children
     * @param uid
     * @param symbol symbol of the target parameter whose value change triggered this method;
     *      if current Nub targets this symbol, it will be considered dependent
     * @param includeValuesLinks if true, even if this Nub targets only non-calculated non-modified
     *      parameters, it will be considered dependent @see jalhyd#98
     * @param includeOtherDependencies if true, then:
     *      - if this Nub is a Solveur, also returns true if ${uid} is either the X or the Ytarget's parent
     *        Nub of this Solveur
     *      - if this Nub is a Verificateur, also returns true if ${uid} is either a selected Custom
     *        Species or the Pass to check of this Verificateur
     */
    public resultDependsOnNub(
        uid: string,
        visited: string[] = [],
        symbol?: string,
        includeValuesLinks: boolean = false,
        includeOtherDependencies: boolean = false
    ): boolean {
        if (uid !== this.uid && !visited.includes(this.uid)) {
            visited.push(this.uid);

            // does any of our parameters depend on the target Nub ?
            for (const p of this._prms) {
                if (p.valueMode === ParamValueMode.LINK) {
                    if (p.dependsOnNubFamily(uid, symbol, includeValuesLinks)) {
                        return true;
                    }
                }
            }
            // does any of our parent's parameters depend on the target Nub ?
            if (this._parent) {
                if (this._parent.resultDependsOnNub(uid, visited, symbol, includeValuesLinks, includeOtherDependencies)) {
                    return true;
                }
            }
            // does any of our children' parameters depend on the target Nub ?
            for (const c of this.getChildren()) {
                if (c.resultDependsOnNub(uid, visited, symbol, includeValuesLinks, includeOtherDependencies)) {
                    return true;
                }
            }
            // other dependencies
            if (includeOtherDependencies) {
                // is this a Solveur
                if (this instanceof Solveur) {
                    return (
                        (this.searchedParameter?.nubUid === uid)
                        || (this.nubToCalculate?.uid === uid)
                    );
                } else if (this instanceof Verificateur) {
                    return (
                        (this.nubToVerify !== undefined && this.nubToVerify.uid === uid)
                        || (this.speciesList.includes(uid))
                    );
                }
            }
        }
        return false;
    }

    /**
     * Returns true if the computation of the current Nub has multiple values
     * (whether it is already computed or not), by detecting if any parameter,
     * linked or not, is variated
     */
    public resultHasMultipleValues(): boolean {
        for (const p of this.parameterIterator) {
            if (p.valueMode === ParamValueMode.MINMAX || p.valueMode === ParamValueMode.LISTE) {
                return true;
            } else if (p.valueMode === ParamValueMode.LINK && p.isReferenceDefined()) {
                // indirect recursivity
                if (p.referencedValue.hasMultipleValues()) {
                    return true
                }
            }
        }
        return false;
    }

    /**
     * Sets the current result to undefined, as well as the results
     * of all depending Nubs; also invalidates all fake ParamValues
     * held by LinkedValues pointing to this result
     * @param resetDepending true pour resetResult() sur les Nubs dépendants également
     */
    public resetResult(visited: string[] = [], resetDepending: boolean = true) {
        this._result = undefined;
        visited.push(this.uid);
        const dependingNubs = Session.getInstance().getDependingNubs(this.uid); // @TODO include Solveur / Verificateur links ?
        for (const dn of dependingNubs) {
            if (resetDepending && !visited.includes(dn.uid)) {
                dn.resetResult(visited);
            }
            dn.resetLinkedParamValues(this);
        }
    }

    /**
     * For all parameters pointing to the result of the given Nub,
     * invalidates fake ParamValues held by pointed LinkedValue
     */
    private resetLinkedParamValues(nub: Nub) {
        for (const p of this.parameterIterator) {
            if (p.valueMode === ParamValueMode.LINK && p.isReferenceDefined()) {
                if (
                    (p.referencedValue.isResult() || p.referencedValue.isExtraResult())
                    && p.referencedValue.nub.uid === nub.uid
                ) {
                    p.referencedValue.invalidateParamValues();
                }
            }
        }
    }

    /**
     * Duplicates the current Nub, but does not register it in the session
     */
    public clone(): Nub {
        const serialised = this.serialise();
        const clone = Session.getInstance().unserialiseSingleNub(serialised, false);
        return clone.nub;
    }

    /**
     * Returns a JSON representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *                         useful to determine if linked parameters must be kept as links
     *                         or have their value copied (if target is not in UIDs list)
     */
    public serialise(extra?: object, nubUidsInSession?: string[]) {
        return JSON.stringify(this.objectRepresentation(extra, nubUidsInSession));
    }

    /**
     * Returns an object representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *                         useful to determine if linked parameters must be kept as links
     *                         or have their value copied (if target is not in UIDs list)
     */
    public objectRepresentation(extra?: object, nubUidsInSession?: string[]): object {
        let ret: any = {
            uid: this.uid,
            props: this.invertedPropertiesEnumAndValues(),
        };

        if (extra) {
            ret = { ...ret, ...{ meta: extra } }; // merge properties
        }
        ret = { ...ret, ...{ children: [], parameters: [] } }; // extend here to make "parameters" the down-most key

        // iterate over local parameters
        const localParametersIterator = new ParamsEquationArrayIterator([this._prms]);
        for (const p of localParametersIterator) {
            if (p.visible) {
                ret.parameters.push(p.objectRepresentation(nubUidsInSession));
            }
        }

        // iterate over children Nubs
        for (const child of this._children) {
            ret.children.push(child.objectRepresentation(undefined, nubUidsInSession));
        }

        return ret;
    }

    /**
     * Fills the current Nub with parameter values, provided an object representation
     * @param obj object representation of a Nub content (parameters)
     * @returns the calculated parameter found, if any - used by child Nub to notify
     *          its parent of the calculated parameter to set
     */
    public loadObjectRepresentation(obj: any): { p: ParamDefinition, hasErrors: boolean, changedUids: { [key: string]: string } } {
        // return value
        const ret: { p: ParamDefinition, hasErrors: boolean, changedUids: { [key: string]: string } } = {
            p: undefined,
            hasErrors: false,
            changedUids: {}
        };
        // set parameter modes and values
        if (obj.parameters && Array.isArray(obj.parameters)) {
            // 1st pass: find calculated param
            // (if calculated param is not the default one, and default one is processed
            // before new one, prevents changing the former's mode from setting the 1st
            // param of the Nub to calculated, resetting the mode that had been loaded)
            for (const p of obj.parameters) {
                if ((ParamValueMode as any)[p.mode] === ParamValueMode.CALCUL) {
                    this.loadParam(p, ret);
                }
            }
            // define calculated param at Nub level
            if (ret.p) {
                this.calculatedParam = ret.p;
            }
            // set parameter modes and values - 2nd pass: set non-calculated params
            for (const p of obj.parameters) {
                if ((ParamValueMode as any)[p.mode] !== ParamValueMode.CALCUL) {
                    this.loadParam(p, ret);
                }
            }
        }

        // iterate over children if any
        if (obj.children && Array.isArray(obj.children)) {
            for (const s of obj.children) {
                // decode properties
                const props = Props.invertEnumKeysAndValuesInProperties(s.props, true);
                // create the Nub
                const subNub = Session.getInstance().createNub(new Props(props), this);
                // try to keep the original ID
                if (!Session.getInstance().uidAlreadyUsed(s.uid)) {
                    subNub.setUid(s.uid);
                } else {
                    ret.changedUids[s.uid] = subNub.uid;
                }
                const childRet = subNub.loadObjectRepresentation(s);
                // add Structure to parent
                this.addChild(subNub);
                // set calculated parameter for child ?
                if (childRet.p) {
                    this.calculatedParam = childRet.p;
                }
                // forward errors
                if (childRet.hasErrors) {
                    ret.hasErrors = true;
                }
            }
        }

        return ret;
    }

    /**
     * Load parameter state from JSON and amend "ret" status variable;
     * to be used by loadObjectRepresentation() only
     */
    protected loadParam(p: any, ret: { p: ParamDefinition, hasErrors: boolean }) {
        const param = this.getParameter(p.symbol);
        if (param) {
            // load parameter
            const pRet = param.loadObjectRepresentation(p, false);
            if (pRet.calculated) {
                ret.p = param;
            }
            // forward errors
            if (pRet.hasErrors) {
                ret.hasErrors = true;
            }
        } else {
            // tslint:disable-next-line:no-console
            console.error(`session file : cannot find parameter ${p.symbol} in target Nub`);
            ret.hasErrors = true;
        }
    }

    /**
     * Once session is loaded, run a second pass on all linked parameters to
     * reset their target if needed
     */
    public fixLinks(obj: any): { hasErrors: boolean } {
        // return value
        const ret = {
            hasErrors: false
        };
        if (obj.parameters && Array.isArray(obj.parameters)) {
            for (const p of obj.parameters) {
                const mode: ParamValueMode = (ParamValueMode as any)[p.mode]; // get enum index for string value
                if (mode === ParamValueMode.LINK) {
                    const param = this.getParameter(p.symbol);
                    // formulaire dont le Nub est la cible du lien
                    const destNub = Session.getInstance().findNubByUid(p.targetNub);
                    if (destNub) {
                        try {
                            param.defineReference(destNub, p.targetParam);
                        } catch (err) {
                            // tslint:disable-next-line:no-console
                            console.error(`fixLinks: defineReference error`
                                + ` (${this.uid}.${param.symbol} => ${destNub.uid}.${p.targetParam})`);
                            // forward error
                            ret.hasErrors = true;
                            // set parameter to single mode/undefined value
                            param.setValue(undefined);
                        }
                    } else {
                        // tslint:disable-next-line:no-console
                        console.error("fixLinks : cannot find target Nub");
                        // forward error
                        ret.hasErrors = true;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Replaces a child Nub
     * @param index index of child in _children array
     * @param child new child
     * @param resetCalcParam if true, resets default calculated parameter after replacing child
     * @param keepParametersState if true, mode and value of parameters will be kept between old and new section
     */
    public replaceChild(index: number, child: Nub, resetCalcParam: boolean = false,
        keepParametersState: boolean = true
    ) {
        if (index > -1 && index < this._children.length) {
            const hasOldChild = (this._children[index] !== undefined);
            const parametersState: any = {};

            // store old parameters state
            if (keepParametersState && hasOldChild) {
                for (const p of this._children[index].parameterIterator) {
                    // if p is also present and visible in new Nub
                    const cp = child.getParameter(p.symbol);
                    if (cp?.visible && p.visible) {
                        parametersState[p.symbol] = p.objectRepresentation(undefined);
                    }
                }
            }

            // replace child
            this._children[index] = child;

            // reapply parameters state
            if (keepParametersState && hasOldChild) {
                for (const p of this._children[index].parameterIterator) {
                    if (p.symbol in parametersState) {
                        p.loadObjectRepresentation(parametersState[p.symbol]);
                    }
                }
            }
        } else {
            throw new Error(`${this.constructor.name}.replaceChild invalid index ${index}`
                + ` (children length: ${this._children.length})`);
        }
        // postprocessing
        this.adjustChildParameters(child);
        // ensure one parameter is calculated
        if (resetCalcParam) {
            this.resetDefaultCalculatedParam();
        }
        // add reference to parent collection (this)
        child.setParent(this);
    }

    /**
     * Finds oldChild in the list, and replaces it at the same index with newChild;
     * if the current calculated parameter belonged to the old child, resets a default one
     * @param oldChild child to get the index for
     * @param newChild child to set at this index
     */
    public replaceChildInplace(oldChild: Nub, newChild: Nub) {
        const calcParamLost = (
            typeof this.calculatedParamDescriptor !== "string"
            && this.calculatedParamDescriptor.uid === oldChild.uid
        );
        const index = this.getIndexForChild(oldChild);
        if (index === -1) {
            throw new Error("old child not found");
        }
        this.replaceChild(index, newChild, calcParamLost);
    }

    /**
     * Returns the current index of the given child if any,
     * or else returns -1
     * @param child child or child UID to look for
     */
    public getIndexForChild(child: Nub | string): number {
        let index: number = -1;
        const uid = (child instanceof Nub) ? child.uid : child;
        for (let i = 0; i < this._children.length; i++) {
            if (this._children[i].uid === uid) {
                index = i;
            }
        }
        return index;
    }

    /**
     * @return true if given Nub id a child of current Nub
     */
    public hasChild(child: Nub): boolean {
        return this.hasChildUid(child.uid);
    }

    /**
     * @return true if given Nub uid id a child of current Nub
     */
    public hasChildUid(childUid: string): boolean {
        for (const s of this._children) {
            if (s.uid === childUid) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return child having the given UID
     */
    public getChild(uid: string): Nub {
        for (const s of this._children) {
            if (s.uid === uid) {
                return s;
            }
        }
        return undefined;
    }

    /** Moves a child up (1 step) */
    public moveChildUp(child: Nub) {
        let i = 0;
        for (const s of this._children) {
            if (s.uid === child.uid && i > 0) {
                const t = this._children[i - 1];
                this._children[i - 1] = this._children[i];
                this._children[i] = t;
                return;
            }
            i++;
        }
    }

    /** Moves a child down (1 step) */
    public moveChildDown(child: Nub) {
        let i = 0;
        for (const s of this._children) {
            if (s.uid === child.uid && i < this._children.length - 1) {
                const t = this._children[i];
                this._children[i] = this._children[i + 1];
                this._children[i + 1] = t;
                return;
            }
            i++;
        }
    }

    /**
     * Removes a child
     * @param index
     */
    public deleteChild(index: number) {
        if (index > -1) {
            this._children.splice(index, 1);
            this.resetDefaultCalculatedParam(undefined, false);
        } else {
            throw new Error(`${this.constructor.name}.deleteChild invalid index=${index}`);
        }
    }

    /** Removes all children */
    public deleteAllChildren() {
        this._children = [];
    }

    /**
     * Returns the position or the current structure (starting at 0)
     * in the parent Nub
     */
    public findPositionInParent(): number {
        if (this.parent) {
            return this.parent.getIndexForChild(this);
        }
        throw new Error(`Nub of class ${this.constructor.name} has no parent`);
    }

    /**
     * Adds a new empty ResultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all children
     */
    public initNewResultElement() {
        if (!this._result) {
            this._result = new Result(undefined, this);
        }
        // push empty result element for further affectation va set currentResult()
        this._result.addResultElement(new ResultElement());
        // do the same for children
        for (const c of this._children) {
            c.initNewResultElement();
        }
    }

    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all children
     */
    public reinitResult() {
        this._result = new Result(undefined, this);
        for (const c of this._children) {
            c.reinitResult();
        }
    }

    /**
     * @returns true if results for this nub and all its children do not contain errors
     */
    public get allResultsOk(): boolean {
        for (const n of this.allChildNubIterator) {
            if (!n.result.ok) {
                return false;
            }
        }
        return true;
    }

    /**
     * @returns true if all messages in nub hierarchy have the same code
     */
    public hasOnlyMessage(code: MessageCode): boolean {
        for (const n of this.allChildNubIterator) {
            if (!n.result.hasOnlyMessage(code)) {
                return false;
            }
        }
        return true;
    }

    public get uniqueMessageCodes(): MessageCode[] {
        let res: MessageCode[] = [];
        for (const n of this.allChildNubIterator) {
            if (n.uid === "MDZ3aH") {
                debugger
            }
            res = res.concat(n.result.uniqueMessageCodes);
        }
        return res;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }

    /**
     * Retrieves the ParamFamily associated to the given symbol, whether it is a
     * parameter or a result
     */
    public getFamily(symbol: string): ParamFamily {
        // is it a parameter ?
        let p;
        try { // SectionNub.getParameter() (for ex.) throws exceptions
            p = this.getParameter(symbol);
        } catch (e) {
            // silent fail
        }
        if (p) {
            return p.family;
        }
        // is it a result ?
        if (symbol in this.resultsFamilies) {
            return this.resultsFamilies[symbol]; // might be undefined
        }
        // give up
        return undefined;
    }

    /**
     * Retrieves the unit (string) associated to the given symbol, whether it is a
     * parameter or a result
     */
    public getUnit(symbol: string): string {
        // is it a parameter ?
        let p;
        try { // SectionNub.getParameter() (for ex.) throws exceptions
            p = this.getParameter(symbol);
        } catch (e) {
            // silent fail
        }
        if (p) {
            return p.unit;
        }
        // is it a result ?
        if (symbol in Nub.resultsUnits()) {
            return Nub.resultsUnits()[symbol]; // might be undefined
        }
        // give up
        return undefined;
    }

    /**
     * Find the longest variating parameter, which will also be the Nub's result length
     */
    public variatingLength(): number {
        let size = 0;
        for (const p of this.parameterIterator) {
            try {
                const iter = p.valuesIterator;
                size = Math.max(size, iter.count());
            } catch (e) {
                // silent fail (when p is in SINGLE mode and its value is undefined)
            }
        }
        return size;
    }

    /**
     * For all SINGLE, LINK and CALC parameters, copies singleValue to
     * sandbox value (.v) before calculation
     */
    public copySingleValuesToSandboxValues() {
        for (const p of this.parameterIterator) {
            switch (p.valueMode) {
                case ParamValueMode.SINGLE:
                case ParamValueMode.CALCUL:
                    p.v = p.singleValue;
                    break;
                case ParamValueMode.LINK:
                    if (p.referencedValue) {
                        p.v = p.referencedValue.getValue();
                    } else {
                        throw Error("Nub.copySingleValuesToSandboxValues() : linked value not defined");
                    }
                    break;
                default:
                    // variable mode, let iterator do the job
                    p.v = undefined;
            }
        }
    }

    protected findCalculatedParameter(): any {
        let computedSymbol: any;
        if (this.calculatedParam === undefined) {
            throw new Error(`CalcSerie() : aucun paramètre à calculer`);
        }
        computedSymbol = this.calculatedParamDescriptor;
        return computedSymbol;
    }

    protected doCalc(computedSymbol?: any, rInit?: number): Result {
        return this.Calc(computedSymbol, rInit);
    }

    /**
     * Returns values of parameters and the result of the calculation for the calculated parameter
     * @param sVarCalc Symbol of the calculated param
     * @param result Result object of the calculation
     */
    protected getParamValuesAfterCalc(sVarCalc: string, result: Result): { [key: string]: number } {
        const cPrms: { [key: string]: number } = {};
        for (const p of this.parameterIterator) {
            if (p.symbol === sVarCalc) {
                cPrms[p.symbol] = result.vCalc;
            } else {
                cPrms[p.symbol] = p.v;
            }
        }
        return cPrms;
    }

    /**
     * To call Nub.calc(…) from indirect child
     */
    protected nubCalc(sVarCalc: string, rInit?: number): Result {
        return Nub.prototype.Calc.call(this, sVarCalc, rInit);
    }

    /**
     * Used by GUI to update results display
     */
    protected notifyResultUpdated() {
        this.notifyObservers({
            action: "resultUpdated"
        }, this);
    }

    /**
     * Used by GUI to update progress bar
     */
    protected notifyProgressUpdated() {
        this.notifyObservers({
            action: "progressUpdated",
            value: this._progress
        }, this);
    }

    /**
     * Optional postprocessing after adding / replacing a child
     */
    // tslint:disable-next-line:no-empty
    protected adjustChildParameters(child: Nub) { }

    /**
     * Résoud l'équation par une méthode numérique
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    protected Solve(sVarCalc: string, rInit: number): Result {
        const dicho: Dichotomie = new Dichotomie(this, sVarCalc, this.DBG);
        dicho.startIntervalMaxSteps = this.dichoStartIntervalMaxSteps;
        const target = this.getFirstAnalyticalParameter();
        return dicho.Dichotomie(target.v, SessionSettings.precision, rInit);
    }

    // /**
    //   * print all messages of this nub and its children
    //   * @param m header string
    //   * @param ident identation
    //   */
    // public printMessages(m?: string, ident: number = 0) {
    //     const idnt = " ".repeat(ident);
    //     console.log("");
    //     console.log(idnt + "--- " + this.constructor.name + " " + this.uid + " --- " + m);
    //     const errs = this.getMessages(ident);
    //     if (errs.length > 0) {
    //         errs.forEach(m => {
    //             console.log(m);
    //         });
    //     }
    // }

    // /**
    //  * get all messages of this nub and its children
    //  */
    // private getMessages(ident: number = 0): string[] {
    //     let hasMessages = false;
    //     let res: string[] = []
    //     const idnt = " ".repeat(ident);
    //     if (this.result.log.messages.length > 0) {
    //         hasMessages = true;
    //         for (const m of this.result.log.messages) {
    //             res.push(idnt + m.toString());
    //         }
    //     }
    //     for (const c of this._children) {
    //         const subMsgs = c.getMessages(ident + 2);
    //         if (subMsgs.length > 0) {
    //             hasMessages = true;
    //             res = res.concat(subMsgs);
    //         }
    //     }
    //     if (hasMessages) {
    //         return res;
    //     }
    //     return [];
    // }

    public invertedPropertiesEnumAndValues(forceNumbers: boolean = false) {
        return this._props.invertEnumKeysAndValues(forceNumbers);
    }

    public addPropertiesObserver(o: Observer) {
        this._props.addObserver(o);
    }

    protected childHasProperty(key: string, nth?: number): boolean {
        if (this._children.length > 0) {
            const n = nth ?? 0;
            if (n < this._children.length) {
                return this._children[n].hasProperty(key);
            }
        }
        return false;
    }

    // interface IProperties

    public hasProperty(key: string): boolean {
        return true;
    }

    public get keys(): string[] {
        return this._props.keys;
    }

    public getPropValue(key: string): any {
        return this._props.getPropValue(key);
    }

    public setPropValue(key: string, val: any, sender?: any): boolean {
        return this._props.setPropValue(key, val, sender);
    }
}
