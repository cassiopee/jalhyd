import { ParamsEquation } from "../internal_modules";
import { ParamDefinition } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";

export class EspeceParams extends ParamsEquation {

    /** Résultat de la vérification (seul paramètre calculable) */
    private _OK: ParamDefinition;

    /** Chute maximale (m) (PAB, jet de Surface) */
    private _DHMaxS: ParamDefinition;

    /** Chute maximale (m) (PAB, jet Plongeant) */
    private _DHMaxP: ParamDefinition;

    /** Largeur minimale de fente ou échancrure latérale (m) (PAB jet de surface) */
    private _BMin: ParamDefinition;

    /** Profondeur minimale de bassin (m) (PAB, jet de Surface) */
    private _PMinS: ParamDefinition;

    /** Profondeur minimale de bassin (m) (PAB, jet Plongeant) */
    private _PMinP: ParamDefinition;

    /** Longueur minimale de bassins (m) (PAB, jet de Surface) */
    private _LMinS: ParamDefinition;

    /** Longueur minimale de bassins (m) (PAB, jet Plongeant) */
    private _LMinP: ParamDefinition;

    /** Charge minimale sur l'échancrure (m) (PAB jet plongeant) */
    private _HMin: ParamDefinition;

    /** Tirant d'eau minimum (m) (MacroRugo enrochements régulièrement répartis) */
    private _YMin: ParamDefinition;

    /** Vitesse d'écoulement maximale (m) (MacroRugo enrochements régulièrement répartis) */
    private _VeMax: ParamDefinition;

    /** Tirant d'eau minimum sur les ralentisseurs suractifs de fond (m) */
    private _YMinSB: ParamDefinition;

    /** Tirant d'eau minimum sur les ralentisseurs plans (m) */
    private _YMinPB: ParamDefinition;

    /** Puissance volumique dissipée maximum préconisée (W/m³) */
    private _PVMaxPrec: ParamDefinition;

    /** Puissance volumique dissipée maximum limite (W/m³) */
    private _PVMaxLim: ParamDefinition;

    constructor(
        rDHMaxS: number, rDHMaxP?: number, rBMin?: number, rPMinS?: number, rPMinP?: number, rLMinS?: number,
        rLMinP?: number, rHMin?: number, rYMin?: number, rVeMax?: number, rYMinSB?: number, rYMinPB?: number,
        rPVMaxPrec?: number, rPVMaxLim?: number, nullParams: boolean = false
    ) {
        super();
        this._OK = new ParamDefinition(this, "OK", ParamDomainValue.POS, "", undefined, undefined, undefined, nullParams);
        this._DHMaxS = new ParamDefinition(this, "DHMaxS", ParamDomainValue.POS, "m", rDHMaxS, undefined, undefined, nullParams);
        this._DHMaxP = new ParamDefinition(this, "DHMaxP", ParamDomainValue.POS, "m", rDHMaxP, undefined, undefined, nullParams);
        this._BMin = new ParamDefinition(this, "BMin", ParamDomainValue.POS, "m", rBMin, undefined, undefined, nullParams);
        this._PMinS = new ParamDefinition(this, "PMinS", ParamDomainValue.POS, "m", rPMinS, undefined, undefined, nullParams);
        this._PMinP = new ParamDefinition(this, "PMinP", ParamDomainValue.POS, "m", rPMinP, undefined, undefined, nullParams);
        this._LMinS = new ParamDefinition(this, "LMinS", ParamDomainValue.POS, "m", rLMinS, undefined, undefined, nullParams);
        this._LMinP = new ParamDefinition(this, "LMinP", ParamDomainValue.POS, "m", rLMinP, undefined, undefined, nullParams);
        this._HMin = new ParamDefinition(this, "HMin", ParamDomainValue.POS, "m", rHMin, undefined, undefined, nullParams);
        this._YMin = new ParamDefinition(this, "YMin", ParamDomainValue.POS, "m", rYMin, undefined, undefined, nullParams);
        this._VeMax = new ParamDefinition(this, "VeMax", ParamDomainValue.POS, "m", rVeMax, undefined, undefined, nullParams);
        this._YMinSB = new ParamDefinition(this, "YMinSB", ParamDomainValue.POS, "m", rYMinSB, undefined, undefined, nullParams);
        this._YMinPB = new ParamDefinition(this, "YMinPB", ParamDomainValue.POS, "m", rYMinPB, undefined, undefined, nullParams);
        this._PVMaxPrec = new ParamDefinition(this, "PVMaxPrec", ParamDomainValue.POS, "W/m³", rPVMaxPrec, undefined, false, nullParams);
        this._PVMaxLim = new ParamDefinition(this, "PVMaxLim", ParamDomainValue.POS, "W/m³", rPVMaxLim, undefined, false, nullParams);

        this.addParamDefinition(this._OK);
        this.addParamDefinition(this._DHMaxS);
        this.addParamDefinition(this._DHMaxP);
        this.addParamDefinition(this._BMin);
        this.addParamDefinition(this._PMinS);
        this.addParamDefinition(this._PMinP);
        this.addParamDefinition(this._LMinS);
        this.addParamDefinition(this._LMinP);
        this.addParamDefinition(this._HMin);
        this.addParamDefinition(this._YMin);
        this.addParamDefinition(this._VeMax);
        this.addParamDefinition(this._YMinSB);
        this.addParamDefinition(this._YMinPB);
        this.addParamDefinition(this._PVMaxPrec);
        this.addParamDefinition(this._PVMaxLim);
    }

    public get OK(): ParamDefinition {
        return this._OK;
    }

    public get DHMaxS(): ParamDefinition {
        return this._DHMaxS;
    }

    public get DHMaxP(): ParamDefinition {
        return this._DHMaxP;
    }

    public get BMin(): ParamDefinition {
        return this._BMin;
    }

    public get PMinS(): ParamDefinition {
        return this._PMinS;
    }

    public get PMinP(): ParamDefinition {
        return this._PMinP;
    }

    public get LMinS(): ParamDefinition {
        return this._LMinS;
    }

    public get LMinP(): ParamDefinition {
        return this._LMinP;
    }

    public get HMin(): ParamDefinition {
        return this._HMin;
    }

    public get YMin(): ParamDefinition {
        return this._YMin;
    }

    public get VeMax(): ParamDefinition {
        return this._VeMax;
    }

    public get YMinSB(): ParamDefinition {
        return this._YMinSB;
    }

    public get YMinPB(): ParamDefinition {
        return this._YMinPB;
    }

    public get PVMaxPrec(): ParamDefinition {
        return this._PVMaxPrec;
    }

    public get PVMaxLim(): ParamDefinition {
        return this._PVMaxLim;
    }
}
