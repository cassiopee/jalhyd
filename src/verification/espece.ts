import { Nub } from "../internal_modules";
import { EspeceParams } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { Observer } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { FishSpecies } from "../internal_modules";
import { Result } from "../internal_modules";
import { FishPass } from "../internal_modules";
import { StructureJetType, StructureFlowMode } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { ParSimulation } from "../internal_modules";
import { ParType } from "../internal_modules";
import { MacroRugo, MacroRugoFlowType } from "../internal_modules";
import { ResultElement } from "../internal_modules";
import { MacrorugoCompound } from "../internal_modules";
import { Pab } from "../internal_modules";
import { LoiDebit, loiAdmissiblesCloisonAval } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { ParallelStructure } from "../internal_modules";
import { StructureOrificeSubmergedParams } from "../internal_modules";
import { Cloisons } from "../internal_modules";
import { isLowerThan, isGreaterThan } from "../internal_modules";
import { DivingJetSupport } from "../internal_modules";

/**
 * Settings for a given fish species (or custom settings), to verify crossing capacities on fish passes.
 * Meant to be used with Verificateur.
 */
export class Espece extends Nub implements Observer {

    /** Multidimension table of parameters values presets, by pass type and species */
    protected presets: any;

    /**
     * The Pab, ParSimulation, or MacroRugo[Compound] to check (Par are not supposed to be checked).
     * Passed by the Verificateur
     */
    protected _passToCheck: FishPass;

    /**
     * For variating passes, index of the result to examine.
     * Passed by the Verificateur
     */
    public indexToCheck: number;

    constructor(prms: EspeceParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Espece);
        this._props.addObserver(this);
        // Diving jets in PAB are not supported by default
        this.divingJetSupported = DivingJetSupport.NOT_SUPPORTED;
        // defaults to CUSTOM mode, for GUI instanciation
        this.species = FishSpecies.SPECIES_CUSTOM;
        this.indexToCheck = 0;
        this.calculatedParam = this.prms.OK;
        this.resetDefaultCalculatedParam();

        // presets
        // Source: Informations sur la Continuité Ecologique (ICE), Onema 2014
        this.presets = {};
        // PAB
        this.presets[CalculatorType.Pab] = {};
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_1] = {
            DHMaxS: 0.35,
            BMin: 0.3,
            PMinS: 1,
            LMinS: 2.5,
            DHMaxP: 0.75,
            PMinP: 1,
            HMin: 0.3,
            LMinP: 2,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: DivingJetSupport.SUPPORTED
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_2] = {
            DHMaxS: 0.35,
            BMin: 0.2,
            PMinS: 1,
            LMinS: 1.75,
            DHMaxP: 0.6,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: DivingJetSupport.SUPPORTED
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_3a] =
            this.presets[CalculatorType.Pab][FishSpecies.SPECIES_3b] = {
                DHMaxS: 0.3,
                BMin: 0.4,
                PMinS: 1,
                HMin: 0.4,
                LMinS: 3.5,
                PVMaxPrec: 150,
                PVMaxLim: 200
            };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_3c] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 1,
            HMin: 0.15,
            LMinS: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_4a] = {
            DHMaxS: 0.35,
            BMin: 0.2,
            PMinS: 1,
            LMinS: 1.75,
            DHMaxP: 0.4,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: DivingJetSupport.SUPPORTED
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_4b] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 0.75,
            LMinS: 1,
            DHMaxP: 0.3,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: DivingJetSupport.SUPPORTED
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_5] = {
            DHMaxS: 0.3,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_6] = {
            DHMaxS: 0.3,
            BMin: 0.2,
            PMinS: 0.75,
            LMinS: 1.75,
            DHMaxP: 0.3,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: DivingJetSupport.SUPPORTED
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_7a] = {
            DHMaxS: 0.3,
            BMin: 0.25,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_7b] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 0.75,
            HMin: 0.15,
            LMinS: 1.25,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_8a] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_8b] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_8c] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_8d] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_9a] = {
            DHMaxS: 0.25,
            BMin: 0.25,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_9b] = {
            DHMaxS: 0.2,
            BMin: 0.15,
            PMinS: 0.5,
            HMin: 0.2,
            LMinS: 1.25,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[CalculatorType.Pab][FishSpecies.SPECIES_10] = {
            DHMaxS: 0.2,
            BMin: 0.15,
            PMinS: 0.5,
            HMin: 0.2,
            LMinS: 1.25,
            PVMaxPrec: 100,
            PVMaxLim: 150
        };
        // PAR Simulation
        this.presets[CalculatorType.ParSimulation] = {};
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_1] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_2] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_3a] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_3b] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_3c] = {
            YMinSB: 0.1,
            YMinPB: 0.1
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_4a] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_4b] = {
            YMinSB: 0.1,
            YMinPB: 0.2
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_5] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_6] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_7a] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[CalculatorType.ParSimulation][FishSpecies.SPECIES_7b] = {
            YMinSB: 0.1,
            YMinPB: 0.1
        };
        // MacroRugo
        this.presets[CalculatorType.MacroRugo] = {};
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_1] = {
            YMin: 0.4,
            VeMax: 2.5
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_2] = {
            YMin: 0.3,
            VeMax: 2.5
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_3a] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_3b] = {
                YMin: 0.4,
                VeMax: 2
            };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_3c] = {
            YMin: 0.15,
            VeMax: 2
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_4a] = {
            YMin: 0.3,
            VeMax: 2
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_4b] = {
            YMin: 0.2,
            VeMax: 2
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_5] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_6] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_7a] = {
                YMin: 0.3,
                VeMax: 2
            };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_7b] = {
            YMin: 0.15,
            VeMax: 2
        };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_8a] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_8b] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_8c] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_8d] = {
                YMin: 0.3,
                VeMax: 1.5
            };
        this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_9a] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_9b] =
            this.presets[CalculatorType.MacroRugo][FishSpecies.SPECIES_10] = {
                YMin: 0.2,
                VeMax: 1.5
            };
    }

    public get prms(): EspeceParams {
        return this._prms as EspeceParams;
    }

    public get species(): FishSpecies {
        return this._props.getPropValue("species") as FishSpecies;
    }

    /** Changing the fish species will load adequate predefined values */
    public set species(s: FishSpecies) {
        this.setPropValue("species", s);
    }

    public set passToCheck(p: FishPass) {
        this._passToCheck = p;
        this.loadPredefinedParametersValues();
    }

    public get divingJetSupported(): DivingJetSupport {
        return this.getPropValue("divingJetSupported");
    }

    public set divingJetSupported(i: DivingJetSupport) {
        this.setPropValue("divingJetSupported", i);
    }

    /**
     * Check that all given criteria are defined before controlling their values
     * @param symbols list of symbols of criteria that have to be defined
     * @return undefined if all criteria are present, a Result with appropriate error
     *      log messages otherwise
     */
    protected checkCriteriaPresence(symbols: string[]): Result {
        const r = new Result();
        r.addResultElement(new ResultElement());
        let error = false;
        for (const s of symbols) {
            if (this.getParameter(s).singleValue === undefined) {
                error = true;
                const m = new Message(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                m.extraVar.var_criterion = s;
                r.log.add(m);
            }
        }
        if (error) {
            return r;
        } else {
            return undefined; // everything OK
        }
    }

    /** Returns 1 if the fish can go through the pass, 0 if it cannot */
    public Equation(): Result {
        // default result is 1 (OK)
        const res = new Result(1, this);

        const sp = this.species;
        // detect pass characteristics and species
        let pt = this._passToCheck.calcType;
        // MacroRugoCompound is just a repetition of the MacroRugo check
        if (pt === CalculatorType.MacroRugoCompound) {
            pt = CalculatorType.MacroRugo;
        }

        // get result to examine
        const r = this._passToCheck.result.resultElements[this.indexToCheck];
        if (r === undefined) {
            throw new Error(`Espece.Equation(): pass to check has no resultElements[${this.indexToCheck}]`);
        }

        // is there a valid preset for the pass type / species couple ?
        if (sp !== FishSpecies.SPECIES_CUSTOM && this.presets[pt][sp] === undefined) {
            res.vCalc = 0;
            res.log.add(new Message(MessageCode.ERROR_VERIF_NO_PRESET));
            return res;

        } else {
            // normal processing
            switch (this._passToCheck.calcType) { // not pt, to detect MacroRugoCompound
                case CalculatorType.Pab:
                    const passB = this._passToCheck as Pab;

                    // Check criteria presence (depends on jet types)
                    let hasAtLeastOneSurfaceJet = false;
                    let hasAtLeastOneDivingJet = false;
                    for (const wall of passB.children) {
                        hasAtLeastOneSurfaceJet = hasAtLeastOneSurfaceJet || this.hasJet(wall, StructureJetType.SURFACE);
                        hasAtLeastOneDivingJet = hasAtLeastOneDivingJet || this.hasJet(wall, StructureJetType.PLONGEANT);
                    }
                    hasAtLeastOneDivingJet = hasAtLeastOneDivingJet || this.hasJet(passB.downWall, StructureJetType.PLONGEANT);

                    let cpRetPAB: Result;
                    let criteriaToCheck: string[] = [ /* "PVMaxPrec", "PVMaxLim", */ "HMin", "BMin"];
                    if (hasAtLeastOneSurfaceJet) {
                        criteriaToCheck = criteriaToCheck.concat(["DHMaxS", "PMinS", "LMinS"]);
                    }
                    if (hasAtLeastOneDivingJet && this.divingJetSupported === DivingJetSupport.SUPPORTED) {
                        criteriaToCheck = criteriaToCheck.concat(["DHMaxP", "PMinP", "LMinP"]);
                    }
                    if (criteriaToCheck.length > 0) {
                        cpRetPAB = this.checkCriteriaPresence(criteriaToCheck);
                        if (cpRetPAB !== undefined) {
                            return cpRetPAB;
                        }
                    }

                    // A -- walls
                    let cloisonNumber = 1;
                    for (const wall of passB.children) {
                        const iRes = wall.result.resultElements[this.indexToCheck];
                        const hasSJet = this.hasJet(wall, StructureJetType.SURFACE);
                        const hasPJet = this.hasJet(wall, StructureJetType.PLONGEANT);

                        // Wall crossability array, one element for each device, by default every device is crossable;
                        // marked only when device is receiving a Warning, to avoid errors redundancy
                        const wca: boolean[] = [];
                        wca.length = wall.structures.length;
                        wca.fill(true);

                        // 0. diving jet support
                        this.checkDivingJetSupport(wall, res, wca);

                        // 1. falls
                        this.checkFalls(wall, iRes, res, wca, hasSJet, hasPJet);

                        // 2. weirs and slots witdh
                        this.checkWeirsAndSlotsWidth(wall, res, wca);

                        // 3. basins depth
                        this.checkBasinDepth(wall, iRes, res, wca, hasSJet, hasPJet);
                        // diving jets only: depth < 2x fall
                        if (hasPJet) {
                            if (isLowerThan(iRes.values.YMOY, (2 * iRes.values.DH), 1e-3)) {
                                let m: Message;
                                // diving jet fails, but surface jet exists
                                if (hasSJet) {
                                    m = new Message(MessageCode.WARNING_VERIF_PAB_YMOY_2_DH)
                                    const failingDevices = this.getDevicesHavingJet(wall, StructureJetType.PLONGEANT);
                                    for (const idx of failingDevices) {
                                        wca[idx] = false;
                                    }
                                } else {
                                    m = new Message(MessageCode.ERROR_VERIF_PAB_YMOY_2_DH)
                                    res.vCalc = 0;
                                };
                                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                                m.extraVar.PB = iRes.values.YMOY;
                                m.extraVar.DH = iRes.values.DH;
                                res.log.add(m);
                            }
                        }

                        // 4. head on weirs
                        this.checkHeadOnWeirs(wall, res, wca);

                        // 5. basins length
                        this.checkBasinLength(wall, res, wca, hasSJet, hasPJet);

                        // 6. dissipated power
                        /* this.checkDissipatedPowerPAB(wall, res); */

                        // 7. orifices
                        this.checkOrifices(wall, res, wca);

                        // check crossability
                        let ok = false;
                        for (const devC of wca) {
                            ok = ok || devC;
                        }
                        if (!ok) {
                            const m = new Message(MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
                            m.extraVar.N = String(cloisonNumber); // keep the "+1" for readability
                            res.log.add(m);
                        }

                        cloisonNumber++;
                    }

                    // B -- downwall
                    const dw = passB.downWall;
                    const iResDw = dw.result.resultElements[this.indexToCheck];
                    const hasSJetDw = this.hasJet(dw, StructureJetType.SURFACE);
                    const hasPJetDw = this.hasJet(dw, StructureJetType.PLONGEANT);
                    // Downwall crossability array, one element for each device, by default every device is crossable;
                    // marked only when device is receiving a Warning, to avoid errors redundancy
                    const dwca: boolean[] = [];
                    dwca.length = dw.structures.length;
                    dwca.fill(true);
                    // 0. diving jet support
                    this.checkDivingJetSupport(dw, res, dwca);
                    // 1. falls
                    this.checkFalls(dw, iResDw, res, dwca, hasSJetDw, hasPJetDw);
                    // 2. weirs and slots witdh
                    this.checkWeirsAndSlotsWidth(dw, res, dwca);
                    // 3. head on weirs
                    this.checkHeadOnWeirs(dw, res, dwca);
                    // 4. orifices
                    this.checkOrifices(dw, res, dwca);
                    // check crossability
                    let okDw = false;
                    for (const devC of dwca) {
                        okDw = okDw || devC;
                    }
                    if (!okDw) {
                        const m = new Message(MessageCode.ERROR_VERIF_PAB_DW_NOT_CROSSABLE);
                        res.log.add(m);
                    }

                    break;

                case CalculatorType.ParSimulation:
                    const passR = this._passToCheck as ParSimulation;

                    // Check criteria presence, depending on baffle type
                    let parCriteria: string[] = [];
                    if ([ParType.PLANE, ParType.FATOU].includes(passR.parType)) {
                        parCriteria = ["YMinPB"];
                    } else if ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(passR.parType)) {
                        parCriteria = ["YMinSB"];
                    }
                    if (parCriteria.length > 0) {
                        const cpRetPAR = this.checkCriteriaPresence(parCriteria);
                        if (cpRetPAR !== undefined) {
                            return cpRetPAR;
                        }
                    }

                    // 1. species groups 3a, 3b, 7b are discouraged
                    if ([FishSpecies.SPECIES_3a, FishSpecies.SPECIES_3b, FishSpecies.SPECIES_7b].includes(this.species)) {
                        res.log.add(new Message(MessageCode.WARNING_VERIF_PAR_SPECIES_GROUP));
                    }

                    // 2. water level
                    let minY = 0; // if this.prms.YMin(P|S)B is undefined, "skip test"
                    if ([ParType.PLANE, ParType.FATOU].includes(passR.parType)) {
                        minY = this.prms.YMinPB.singleValue;
                    } else if ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(passR.parType)) {
                        minY = this.prms.YMinSB.singleValue;
                    }
                    if (isLowerThan(r.values.h, minY, 1e-3)) {
                        res.vCalc = 0;
                        const m = new Message(MessageCode.ERROR_VERIF_PAR_YMIN);
                        m.extraVar.h = r.values.h;
                        m.extraVar.minY = minY;
                        res.log.add(m);
                    }

                    break;

                case CalculatorType.MacroRugoCompound:
                    const passMRC = this._passToCheck as MacrorugoCompound;

                    // Check criteria presence
                    const cpRetMRC = this.checkCriteriaPresence(["YMin", "VeMax"]);
                    if (cpRetMRC !== undefined) {
                        return cpRetMRC;
                    }

                    let atLeastOneOK = false;
                    let apronNumber = 1;
                    const consecutiveWidths: number[] = [];
                    let previousApronFailed = true;
                    // loop on aprons with MacroRugo check; at least one is required
                    for (const mr of passMRC.children) {
                        const mrRes = this.checkMacroRugo(mr, mr.result.resultElements[this.indexToCheck], res, apronNumber);
                        if (mrRes === 1) {
                            atLeastOneOK = true;
                            if (previousApronFailed) {
                                consecutiveWidths.push(0);
                                previousApronFailed = false;
                            }
                            consecutiveWidths[consecutiveWidths.length - 1] += mr.prms.B.v;
                        } else {
                            previousApronFailed = true;
                        }
                        apronNumber++;
                    }
                    if (!atLeastOneOK) {
                        res.vCalc = 0;
                        res.log.add(new Message(MessageCode.ERROR_VERIF_MRC_AT_LEAST_ONE_APRON));
                    } else {
                        // Check max. crossable width
                        const maxCrossableWidth = Math.max(...consecutiveWidths);
                        const patternWidth = passMRC.prms.PBD.v / Math.sqrt(passMRC.prms.C.v);
                        if (isLowerThan(maxCrossableWidth, patternWidth, 1e-3)) {
                            res.vCalc = 0;
                            const m = new Message(MessageCode.ERROR_VERIF_MRC_CROSSABLE_WIDTH);
                            m.extraVar.width = maxCrossableWidth;
                            m.extraVar.patternWidth = patternWidth;
                            res.log.add(m);
                        } else {
                            const m = new Message(MessageCode.INFO_VERIF_MRC_CROSSABLE_WIDTH);
                            m.extraVar.width = maxCrossableWidth;
                            res.log.add(m);
                        }
                    }
                    break;

                case CalculatorType.MacroRugo:
                    // Check criteria presence
                    const cpRetMR = this.checkCriteriaPresence(["YMin", "VeMax"]);
                    if (cpRetMR !== undefined) {
                        return cpRetMR;
                    }
                    res.vCalc = this.checkMacroRugo(this._passToCheck as MacroRugo, r, res);
                    break;

                default:
                    // should never happen
                    throw new Error(`Espece.Equation(): undefined pass type ${CalculatorType[this._passToCheck.calcType]}`);
            }
        }

        return res;
    }

    public update(sender: any, data: any): void {
        if (data.action === "propertyChange") {
            if (data.name === "species") {
                this.loadPredefinedParametersValues();
                // no parameter visibility update: GUI will never show anything but SPECIES_CUSTOM mode,
                // where all parameters are visible
            }
        }
    }

    /**
     * Checks the diving jet support on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    protected checkDivingJetSupport(wall: ParallelStructure, res: Result, ca: boolean[]) {
        // if diving jets are not supported by the current species
        if (this.divingJetSupported === DivingJetSupport.NOT_SUPPORTED) {
            const devices = this.getDevicesHavingJet(wall, StructureJetType.PLONGEANT);
            // mark every device having a diving jet as not crossable
            for (const idx of devices) {
                ca[idx] = false;
            }
            // if every device is failing, add an error for the whole wall
            if (devices.length === wall.structures.length) {
                let m: Message;
                m = new Message(MessageCode.ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.log.add(m);
            } else {
                // add a warning for every device having a diving jet
                for (const idx of devices) {
                    let m: Message;
                    m = new Message(MessageCode.WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
                    m.parent = wall.structures[idx].result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    res.log.add(m);
                }
            }
        }
    }

    /**
     * Checks the falls on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param iRes the current result element to check on the wall result
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    protected checkFalls(wall: ParallelStructure, iRes: ResultElement, res: Result, ca: boolean[], hasSJet: boolean, hasPJet: boolean) {
        const fallSJetFails = (hasSJet && this.prms.DHMaxS.singleValue !== undefined && isGreaterThan(iRes.values.DH, this.prms.DHMaxS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === DivingJetSupport.SUPPORTED && hasPJet && this.prms.DHMaxP.singleValue !== undefined && isGreaterThan(iRes.values.DH, this.prms.DHMaxP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m: Message;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new Message(MessageCode.ERROR_VERIF_PAB_DHMAX);
                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.maxDHS = this.prms.DHMaxS.singleValue;
                m.extraVar.maxDHP = this.prms.DHMaxP.singleValue;

            } else { // only one type of jet fails, the other type may be present or not

                const failingJetType = fallSJetFails ? StructureJetType.SURFACE : StructureJetType.PLONGEANT;
                // one jet fails, but other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new Message(MessageCode.WARNING_VERIF_PAB_DHMAX_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                } else { // one jet fails and there is no other jet
                    m = new Message(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.maxDH = fallSJetFails ? this.prms.DHMaxS.singleValue : this.prms.DHMaxP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.DH = iRes.values.DH;
            res.log.add(m);
        } // else everything ok
    }

    /**
     * Checks the basin depth on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param iRes the current result element to check on the wall result
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    protected checkBasinDepth(wall: ParallelStructure, iRes: ResultElement, res: Result, ca: boolean[], hasSJet: boolean, hasPJet: boolean) {
        const fallSJetFails = (hasSJet && this.prms.PMinS.singleValue !== undefined && isLowerThan(iRes.values.YMOY, this.prms.PMinS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === DivingJetSupport.SUPPORTED && hasPJet && this.prms.PMinP.singleValue !== undefined && isLowerThan(iRes.values.YMOY, this.prms.PMinP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m: Message;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new Message(MessageCode.ERROR_VERIF_PAB_YMOY);
                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.minPBS = this.prms.PMinS.singleValue;
                m.extraVar.minPBP = this.prms.PMinP.singleValue;
            } else { // only one type of jet fails, the other type may be present or not
                const failingJetType = fallSJetFails ? StructureJetType.SURFACE : StructureJetType.PLONGEANT;
                // other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new Message(MessageCode.WARNING_VERIF_PAB_YMOY_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                } else { // there is no other jet
                    m = new Message(MessageCode.ERROR_VERIF_PAB_YMOY_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.minPB = fallSJetFails ? this.prms.PMinS.singleValue : this.prms.PMinP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.PB = iRes.values.YMOY;
            res.log.add(m);
        } // else everything ok
    }

    /**
     * Checks the basin length on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    protected checkBasinLength(wall: Cloisons, res: Result, ca: boolean[], hasSJet: boolean, hasPJet: boolean) {
        const fallSJetFails = (hasSJet && this.prms.LMinS.singleValue !== undefined && isLowerThan(wall.prms.LB.singleValue, this.prms.LMinS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === DivingJetSupport.SUPPORTED && hasPJet && this.prms.LMinP.singleValue !== undefined && isLowerThan(wall.prms.LB.singleValue, this.prms.LMinP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m: Message;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new Message(MessageCode.ERROR_VERIF_PAB_LMIN);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.minLBS = this.prms.LMinS.singleValue;
                m.extraVar.minLBP = this.prms.LMinP.singleValue;
                ca.fill(false); // no device is crossable
            } else { // only one type of jet fails, the other type may be present or not
                const failingJetType = fallSJetFails ? StructureJetType.SURFACE : StructureJetType.PLONGEANT;
                // other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new Message(MessageCode.WARNING_VERIF_PAB_LMIN_JET);
                    m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                } else { // there is no other jet
                    m = new Message(MessageCode.ERROR_VERIF_PAB_LMIN_JET);
                    m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.minLB = fallSJetFails ? this.prms.LMinS.singleValue : this.prms.LMinP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.LB = wall.prms.LB.singleValue;
            res.log.add(m);
        } // else everything ok
    }

    /**
     * Checks the weirs and slots widths on every structure of the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    protected checkWeirsAndSlotsWidth(wall: ParallelStructure, res: Result, ca: boolean[]) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            const hasOtherDevicesThanWeirs = (
                this.hasJet(wall, [StructureJetType.PLONGEANT])
                || (this.getDevicesHavingLaw(wall, [LoiDebit.WeirVillemonte, LoiDebit.WeirSubmergedLarinier]).length < wall.structures.length)
            );
            const hasOtherDevicesThanOrifice = (
                this.hasJet(wall, [StructureJetType.PLONGEANT])
                || (this.getDevicesHavingLaw(wall, LoiDebit.OrificeSubmerged).length < wall.structures.length)
            );
            const iRes = device.result.resultElements[this.indexToCheck];
            const jt = iRes.values.ENUM_StructureJetType;
            if (jt !== StructureJetType.PLONGEANT) {
                // Surface jets: for "Villemonte 1947" ("échancrure") or "Fente noyée (Larinier 1992)" ("fente") only
                if ([LoiDebit.WeirVillemonte, LoiDebit.WeirSubmergedLarinier].includes(device.loiDebit)) {
                    const structParams = device.prms as RectangularStructureParams;
                    if (this.prms.BMin.singleValue !== undefined && isLowerThan(structParams.L.v, this.prms.BMin.singleValue, 1e-3)) {
                        let m: Message;
                        if (hasOtherDevicesThanWeirs) {
                            ca[structureNumber - 1] = false; // device is not crossable
                            m = new Message(MessageCode.WARNING_VERIF_PAB_BMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        } else {
                            res.vCalc = 0;
                            m = new Message(MessageCode.ERROR_VERIF_PAB_BMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        m.extraVar.L = structParams.L.v;
                        m.extraVar.minB = this.prms.BMin.singleValue;
                        res.log.add(m);
                    }
                } else if (device.loiDebit === LoiDebit.OrificeSubmerged) {
                    const structParams = device.prms as StructureOrificeSubmergedParams;
                    // Orifices (no jet)
                    if (this.prms.BMin.singleValue !== undefined && isLowerThan(structParams.S.v, Math.pow(this.prms.BMin.singleValue, 2), 1e-3)) {
                        let m: Message;
                        if (hasOtherDevicesThanOrifice) {
                            ca[structureNumber - 1] = false; // device is not crossable
                            m = new Message(MessageCode.WARNING_VERIF_PAB_SMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        } else {
                            res.vCalc = 0;
                            m = new Message(MessageCode.ERROR_VERIF_PAB_SMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        m.extraVar.S = structParams.S.v;
                        m.extraVar.minS = Math.pow(this.prms.BMin.singleValue, 2);
                        res.log.add(m);
                    }
                }
            }
            structureNumber++;
        }
    }

    /**
     * Checks the head on weirs on every structure of the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    protected checkHeadOnWeirs(wall: ParallelStructure, res: Result, ca: boolean[]) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            let zdv = device.prms.ZDV.singleValue;
            // if device is a regulated weir, get calculated ZDV at current iteration from parent downWall
            if (loiAdmissiblesCloisonAval.VanneLevante.includes(device.getPropValue("loiDebit"))) {
                zdv = device.parent.result.resultElements[this.indexToCheck].values.ZDV;
            }
            // do not use device.prms.h1.v, that contains only the latest calculated value if pass is variyng
            const h1 = wall.result.resultElements[this.indexToCheck].vCalc - zdv;
            /* console.log(
                `wall ${wall.findPositionInParent() + 1} / device ${structureNumber} : h1=${h1}, ZDV=${zdv}, class=${device.constructor.name}`
            ); */
            if (this.prms.HMin.singleValue !== undefined && isLowerThan(h1, this.prms.HMin.singleValue, 1e-3)) {
                let m: Message;
                ca[structureNumber - 1] = false; // device is not crossable
                m = new Message(MessageCode.WARNING_VERIF_PAB_HMIN);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                m.extraVar.h1 = Math.max(0, h1);
                m.extraVar.minH = this.prms.HMin.singleValue;
                res.log.add(m);
            }
            structureNumber++;
        }
    }

    /**
     * Checks the dissipated power on the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     */
    protected checkDissipatedPowerPAB(wall: ParallelStructure, res: Result) {
        const pv = wall.result.resultElements[this.indexToCheck].values.PV;
        if (this.prms.PVMaxLim.singleValue !== undefined && isGreaterThan(pv, this.prms.PVMaxLim.singleValue, 1e-3)) {
            res.vCalc = 0;
            const m = new Message(MessageCode.ERROR_VERIF_PAB_PVMAX);
            m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
            m.extraVar.PV = pv;
            m.extraVar.maxPV = this.prms.PVMaxLim.singleValue;
            res.log.add(m);
        } else if (this.prms.PVMaxPrec.singleValue !== undefined && isGreaterThan(pv, this.prms.PVMaxPrec.singleValue, 1e-3)) {
            res.vCalc = 0;
            const m = new Message(MessageCode.WARNING_VERIF_PAB_PVMAX);
            m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
            m.extraVar.PV = pv;
            m.extraVar.maxPV = this.prms.PVMaxPrec.singleValue;
            res.log.add(m);
        }
    }

    /**
     * Marks orifices as not supposed to be crossable, with a warning (jalhyd#249)
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    protected checkOrifices(wall: ParallelStructure, res: Result, ca: boolean[]) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            // a structure is considered an orifice if its flow mode is "Orifice"
            if (device.result.resultElements[this.indexToCheck].values.ENUM_StructureFlowMode === StructureFlowMode.ORIFICE) {
                let m: Message;
                ca[structureNumber - 1] = false; // device is not crossable
                m = new Message(MessageCode.WARNING_VERIF_PAB_ORIFICE);
                m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.log.add(m);
            }
            structureNumber++;
        }
    }

    /**
     * Returns true if given wall has at least one device with a jet type among given jetTypes
     */
    protected hasJet(wall: ParallelStructure, jetTypes: StructureJetType | StructureJetType[]): boolean {
        if (!Array.isArray(jetTypes)) {
            jetTypes = [jetTypes];
        }
        for (const device of wall.structures) {
            const deviceJetType = device.result.resultElements[this.indexToCheck].values.ENUM_StructureJetType;
            if (jetTypes.includes(deviceJetType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns indexes of the devices having given jet type, on the given wall
     * @param wall wall to check devices on
     * @param t jet type
     */
    protected getDevicesHavingJet(wall: ParallelStructure, t: StructureJetType): number[] {
        const idxs: number[] = [];
        for (let i = 0; i < wall.structures.length; i++) {
            const deviceJetType = wall.structures[i].result.resultElements[this.indexToCheck].values.ENUM_StructureJetType;
            if (deviceJetType === t) {
                idxs.push(i);
            }
        }
        return idxs;
    }

    /**
     * Returns indexes of the devices having given discharge law, on the given wall
     * @param wall wall to check devices on
     * @param l discharge law
     */
    protected getDevicesHavingLaw(wall: ParallelStructure, l: LoiDebit | LoiDebit[]): number[] {
        const idxs: number[] = [];
        if (!Array.isArray(l)) {
            l = [l];
        }
        for (let i = 0; i < wall.structures.length; i++) {
            if (l.includes(wall.structures[i].loiDebit)) {
                idxs.push(i);
            }
        }
        return idxs;
    }

    /**
     * Checks a MacroRugo pass (factorized to use when checking MacroRugoCompound)
     * @param pass the MacroRugo pass to check
     * @param passResult the current result of the pass to check
     * @param verifResult the result of the check, to add logs to
     * @param apronNumber if defined, means that we're checking an apron of a MacroRugoCompound
     */
    protected checkMacroRugo(pass: MacroRugo, passResult: ResultElement, verifResult: Result, apronNumber?: number): number {
        let val = 1;

        // 1. water level
        let yValue: number;
        if (pass.prms.Y.isCalculated) {
            yValue = passResult.vCalc;
        } else {
            if (pass.prms.Y.hasMultipleValues) {
                yValue = pass.prms.Y.getInferredValuesList()[this.indexToCheck]; // @TODO extend if necessary ?
            } else {
                // fixed Y
                yValue = pass.prms.Y.V;
            }
        }
        if (this.prms.YMin.singleValue !== undefined && isLowerThan(yValue, this.prms.YMin.singleValue, 1e-3)) {
            val = 0;
            let m: Message;
            if (apronNumber !== undefined) {
                m = new Message(MessageCode.WARNING_VERIF_MRC_YMIN_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
            } else {
                m = new Message(MessageCode.ERROR_VERIF_MR_YMIN);
            }
            m.extraVar.Y = yValue;
            m.extraVar.minY = this.prms.YMin.singleValue;
            verifResult.log.add(m);
        }

        // 2. velocity or submerged
        if (passResult.values.ENUM_MacroRugoFlowType === MacroRugoFlowType.SUBMERGED) {
            val = 0;
            if (apronNumber !== undefined) {
                const m: Message = new Message(MessageCode.WARNING_VERIF_MRC_SUBMERGED_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
                verifResult.log.add(m);
            } else {
                verifResult.log.add(new Message(MessageCode.ERROR_VERIF_MR_SUBMERGED));
            }
        } else if (passResult.values.ENUM_MacroRugoFlowType === MacroRugoFlowType.SUBMERGED || this.prms.VeMax.singleValue !== undefined && isGreaterThan(passResult.values.Vmax, this.prms.VeMax.singleValue, 1e-3)) {
            val = 0;
            let m: Message;
            if (apronNumber !== undefined) {
                m = new Message(MessageCode.WARNING_VERIF_MRC_VMAX_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
            } else {
                m = new Message(MessageCode.ERROR_VERIF_MR_VMAX);
            }
            m.extraVar.V = passResult.values.Vmax;
            m.extraVar.maxV = this.prms.VeMax.singleValue;
            verifResult.log.add(m);
        }

        return val;
    }

    /**
     * Load ICE tables values into parameters, depending on species
     * and pass to check
     */
    protected loadPredefinedParametersValues() {
        if (this.species !== FishSpecies.SPECIES_CUSTOM && this._passToCheck !== undefined) {
            const sp = this.species;
            let pt = this._passToCheck.calcType;
            // MacroRugoCompound is just a repetition of the MacroRugo check
            if (pt === CalculatorType.MacroRugoCompound) {
                pt = CalculatorType.MacroRugo;
            }
            // type de jet pour les PAB
            if (
                this.presets[pt] !== undefined
                && this.presets[pt][sp] !== undefined
                && this.presets[pt][sp].DivingJetSupported !== undefined
            ) {
                this.divingJetSupported = this.presets[pt][sp].DivingJetSupported;
            } else {
                this.divingJetSupported = DivingJetSupport.NOT_SUPPORTED;
            }
            this.debug(`load predefined values for species=${FishSpecies[sp].substring(8)} and pass class=${CalculatorType[pt]}`);
            for (const p of this.prms) {
                if (p.symbol !== "OK") {
                    const preset = this.presets[pt];
                    if (preset[sp] !== undefined) {
                        // if (preset[sp][p.symbol] !== undefined) console.log(`-> setting ${p.symbol} to ${preset[sp][p.symbol]}`);
                        p.singleValue = preset[sp][p.symbol];
                    }
                }
            }
        }
    }

    /**
     * Load ICE tables values into parameters, for a given species
     * (used in GUI for loading predefined species)
     */
    public loadPredefinedSpecies(sp: FishSpecies) {
        let found: boolean;
        // for each param
        for (const p of this.prms) {
            if (p.symbol !== "OK") {
                // reset current value
                p.singleValue = undefined;
                // whatever the pass type (criteria are supposed to apply to 1 pass type only)
                found = false;
                for (const pt of Object.keys(this.presets)) {
                    const preset = this.presets[pt];
                    if (!found && preset !== undefined && preset[sp] !== undefined && preset[sp][p.symbol] !== undefined) {
                        found = true;
                        // if (preset[sp][p.symbol] !== undefined) console.log(`-> setting ${p.symbol} to ${preset[sp][p.symbol]}`);
                        // apply preset
                        p.singleValue = preset[sp][p.symbol];
                    }
                }
                // type de jet pour les PAB
                if (
                    this.presets[CalculatorType.Pab] !== undefined
                    && this.presets[CalculatorType.Pab][sp] !== undefined
                    && this.presets[CalculatorType.Pab][sp].DivingJetSupported !== undefined
                ) {
                    this.divingJetSupported = this.presets[CalculatorType.Pab][sp].DivingJetSupported;
                } else {
                    this.divingJetSupported = DivingJetSupport.NOT_SUPPORTED;
                }
            }
        }
    }

    /** Parameters other than OK are just settings here, nothing can vary or be calculated */
    protected setParametersCalculability(): void {
        for (const p of this._prms) {
            p.calculability = ParamCalculability.FIXED;
        }
        this.prms.OK.calculability = ParamCalculability.EQUATION;
    }

}
