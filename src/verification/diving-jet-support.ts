/** Diving jet support for Espece */
export enum DivingJetSupport {
    NOT_SUPPORTED,
    SUPPORTED
}
