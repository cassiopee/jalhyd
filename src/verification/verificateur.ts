import { Nub } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { Session } from "../internal_modules";
import { Result } from "../internal_modules";
import { Espece } from "../internal_modules";
import { FishSpecies } from "../internal_modules";
import { EspeceParams } from "../internal_modules";
import { FishPass } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { VerificateurParams } from "../internal_modules";
import { ParSimulation } from "../internal_modules";
import { isGreaterThan } from "../internal_modules";
import { ParType } from "../internal_modules";

export class Verificateur extends Nub {

    /** Instances of Espece re-created before each calculation */
    protected _species: Espece[];

    /** Index of the reseult element to verify, when the pass to verify is variating */
    protected _variatingIndex = 0;

    constructor(dbg: boolean = false) {
        super(new VerificateurParams(), dbg);
        this.setCalculatorType(CalculatorType.Verificateur);
        // UID of Session Nub to verify
        this.nubToVerify = undefined;
        // List of fish species to check the pass against
        this.speciesList = [];
        this._species = [];
        // no calculated param
    }

    public get species(): Espece[] {
        return this._species;
    }

    public get prms(): VerificateurParams {
        return this._prms as VerificateurParams;
    }

    /** Contains either string keys from FishSpecies enum, or UIDs of Espece Session Nubs */
    public get speciesList(): string[] {
        return this._props.getPropValue("speciesList") as string[];
    }

    public set speciesList(l: string[]) {
        this.setPropValue("speciesList", l);
        // (re)create Espece instances
        this.initSpecies();
    }

    /** finds the Nub to verify by its UID */
    public get nubToVerify(): FishPass {
        let nub: Nub;
        const nubUID: string = this._props.getPropValue("nubToVerify");
        if (nubUID !== undefined && nubUID !== "") {
            nub = Session.getInstance().findNubByUid(nubUID);
            // silent fail if (nub === undefined)
        }
        return nub;
    }

    /** defines the Nub to verify by setting property "nubToVerify" to the UID of the given Nub */
    public set nubToVerify(n: FishPass) {
        let uid = ""; // empty value
        if (n !== undefined) {
            uid = n.uid;
        }
        this.setPropValue("nubToVerify", uid);
    }

    /**
     * Adds a new empty ResultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all Espece Nubs
     */
    public initNewResultElement() {
        super.initNewResultElement();
        // do the same for Espece
        for (const c of this.species) {
            c.initNewResultElement();
        }
    }

    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all Espece Nubs
     */
    public reinitResult() {
        super.reinitResult();
        // do the same for Espece
        for (const c of this.species) {
            c.reinitResult();
        }
    }

    public CalcSerie(rInit?: number): Result {
        // calc pass systematically
        this.nubToVerify.CalcSerie();
        // check presence of valid result
        if (!this.nubToVerify.result || !this.nubToVerify.result.ok) {
            this._result = new Result(undefined, this);
            this._result.addMessage(new Message(MessageCode.ERROR_VERIF_ERRORS_IN_PASS));
            return this._result;
        }

        // (re)create Espece instances
        this.initSpecies();

        this.progress = 0;
        this.resetResult();
        // reinit Result and children Results
        this.reinitResult();

        const passIsVarying = this.nubToVerify.resultHasMultipleValues();

        if (!passIsVarying) { // pass to verify is not varying
            // prepare a new slot to store results
            this._variatingIndex = 0;
            this.initNewResultElement();

            this.doCalc(undefined, rInit);
            this.progress = 100;

            // global error messages
            if (this._result.hasErrorMessages()) {
                this._result.addMessage(new Message(MessageCode.ERROR_VERIF_KO));
            } else if (this._result.hasWarningMessages()) {
                this._result.addMessage(new Message(MessageCode.WARNING_VERIF_OK_BUT));
            } else { // if no error log (everything went ok), add a success message
                this._result.addMessage(new Message(MessageCode.INFO_VERIF_OK));
            }

        } else { // pass to verify is varying
            const size = this.nubToVerify.result.resultElements.length;
            let progressStep;
            const remainingProgress = 100 - this.progress;
            progressStep = remainingProgress / size;

            let stageErrorsCount = 0;
            // iterate over pass to verify
            for (let l = 0; l < size; l++) {
                // prepare a new slot to store results
                this._variatingIndex = l;
                this.initNewResultElement();

                // don't calculate if this iteration has errors
                if (!this.nubToVerify.result.resultElements[this._variatingIndex].ok) {
                    const m = new Message(MessageCode.ERROR_VERIF_VARYING_ERRORS_IN_PASS);
                    m.extraVar.i = String(l + 1);
                    this._result.resultElement.addMessage(m);
                    stageErrorsCount++;
                } else {
                    // calculate
                    this.doCalc(undefined); // résultat dans this.currentResult (resultElement le plus récent)
                    if (!this._result.resultElement.ok) {
                        stageErrorsCount++;
                    }
                }

                // update progress
                this.progress += progressStep;
            }
            // round progress to 100%
            this.progress = 100;

            // global error messages
            if (stageErrorsCount === size) {
                this._result.addMessage(new Message(MessageCode.ERROR_VERIF_VARYING_KO));
            } else if (stageErrorsCount > 0) {
                this._result.addMessage(new Message(MessageCode.WARNING_VERIF_VARYING_OK_BUT));
            } else {
                this._result.addMessage(new Message(MessageCode.INFO_VERIF_VARYING_OK));
            }
        }

        this.notifyResultUpdated();
        return this._result;
    }

    public Calc(): Result {
        this.currentResultElement = this.Equation();
        return this.result;
    }

    /**
     * Returns 1 if everything is OK, 0 as soon as a species check fails
     */
    public Equation(): Result {
        let v: number = 1;
        const r = new Result(v, this);

        // species-independent check: PAR
        if (this.nubToVerify instanceof ParSimulation) {
            // 1. slope
            const maxS = [ParType.PLANE, ParType.FATOU].includes(this.nubToVerify.parType) ? 0.2 : 0.16;
            if (isGreaterThan(this.nubToVerify.prms.S.v, maxS)) {
                const m = new Message(MessageCode.ERROR_VERIF_PAR_SLOPE);
                m.extraVar.S = this.nubToVerify.prms.S.v;
                m.extraVar.maxS = maxS;
                r.resultElement.addMessage(m);
                r.vCalc = 0;
                return r;
            }
            // 2. downstream fall
            if (isGreaterThan(this.nubToVerify.result.resultElements[this._variatingIndex].values.DH, 0, 1e-3)) {
                r.log.add(new Message(MessageCode.ERROR_VERIF_PAR_DH));
                r.vCalc = 0;
                return r;
            }
        }


        let spIndex = 0;
        for (const speciesNub of this._species) {
            // give the pass pointer
            speciesNub.passToCheck = this.nubToVerify;
            // give the index to check if pass is variating
            speciesNub.indexToCheck = this._variatingIndex;

            const subRes = speciesNub.Calc();

            let m: Message;
            const isCustomSpecies = !(this.speciesList[spIndex] in FishSpecies);
            // if at least one error log in Espece nub, add one at Verificateur level, so that .ok is false
            // else add a success info message
            if (isCustomSpecies) {
                if (subRes.resultElements[this._variatingIndex].hasErrorMessages()) {
                    m = new Message(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
                } else if (subRes.resultElements[this._variatingIndex].hasWarningMessages()) {
                    m = new Message(MessageCode.WARNING_VERIF_SPECIES_NUB_OK_BUT);
                } else {
                    m = new Message(MessageCode.INFO_VERIF_SPECIES_NUB_OK);
                }
                m.extraVar.uid = this.speciesList[spIndex];
            } else {
                if (subRes.resultElements[this._variatingIndex].hasErrorMessages()) {
                    m = new Message(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
                } else if (subRes.resultElements[this._variatingIndex].hasWarningMessages()) {
                    m = new Message(MessageCode.WARNING_VERIF_SPECIES_GROUP_OK_BUT);
                } else {
                    m = new Message(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
                }
                m.extraVar.speciesGroup = this.speciesList[spIndex];
            }
            r.resultElement.addMessage(m);

            // @TODO remove vCalc as no parameter is calculated ?
            if (subRes.vCalc === 0) {
                v = 0;
            }

            spIndex++;
        }
        r.vCalc = v;

        return r;
    }

    /**
     * Creates an instance of Espece for each element of this.speciesList, and places
     * it in this.species so that their results are kept and their logs are accessible
     * after calculation has ended
     */
    protected initSpecies() {
        this._species = [];
        for (const s of this.speciesList) {
            this._species.push(this.getEspeceFromString(s));
        }
    }

    /**
     * Retrieves an Espece from a given string, which should be either
     * the UID of an Espece Session Nub, or the label of an element
     * of FishSpecies enum
     * @param s
     */
    protected getEspeceFromString(s: string): Espece {
        let e: Espece;
        // is it a predefined species ?
        if (Object.keys(FishSpecies).includes(s)) {
            if (s === "SPECIES_CUSTOM") {
                throw new Error("Verificateur.getEspeceFromString(): cannot automatically instantiate SPECIES_CUSTOM");
            }
            // instanciate out of Session
            e = new Espece(new EspeceParams(1, 1));
            e.species = (FishSpecies as any)[s];
        } else {
            // is it a session Nub ?
            e = Session.getInstance().findNubByUid(s) as Espece;
            if (e === undefined) {
                throw new Error(`Verificateur.getEspeceFromString(): could not find Nub ${s} in the session`);
            }
        }
        return e;
    }

    // tslint:disable-next-line:no-empty
    protected setParametersCalculability(): void { }

    // no calculated param
    protected findCalculatedParameter(): any {
        return undefined;
    }

}
