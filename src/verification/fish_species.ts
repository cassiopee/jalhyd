/**
 * Predefined crossing capabilities criteria for different fish species
 * Source: Informations sur la Continuité Ecologique (ICE), Onema 2014
 * @see Espece
 */
export enum FishSpecies {
    SPECIES_CUSTOM,
    SPECIES_1,      // Saumon atlantique (Salmo salar), Truite de mer ou de rivière [50-100] (Salmo trutta)
    SPECIES_2,      // Mulets (Chelon labrosus, Liza ramada)
    SPECIES_3a,     // Grande alose (Alosa alosa)
    SPECIES_3b,     // Alose feinte (Alosa fallax fallax)
    SPECIES_3c,     // Lamproie marine (Petromyzon marinus)
    SPECIES_4a,     // Truite de rivière ou truite de mer [25-55] (Salmo trutta)
    SPECIES_4b,     // Truite de rivière [15-30] (Salmo trutta)
    SPECIES_5,      // Aspe (Aspius aspius), Brochet (Esox lucius)
    SPECIES_6,      // Ombre commun (Thymallus thymallus)
    SPECIES_7a,     // Barbeau fluviatile (Barbus barbus), Chevaine (Squalius cephalus), Hotu (Chondrostoma nasus)
    SPECIES_7b,     // Lamproie fluviatile (Lampetra fluviatilis)
    SPECIES_8a,     // Carpe commune (Cyprinus carpio)
    SPECIES_8b,     // Brème commune (Abramis brama), Sandre (Sander lucioperca)
    SPECIES_8c,     // Brème bordelière (Blicca bjoerkna), Ide melanote (Leuciscus idus), Lotte de rivière (Lota lota), Perche (Perca fluviatilis), Tanche (Tinca tinca)
    SPECIES_8d,     // Vandoises (Leuciscus sp hors Idus)
    SPECIES_9a,     // Ablette commune (Alburnus alburnus), Ablette sprirlin (Alburnoides bipunctatus), Barbeau méridional (Barbus meridionalis), Blageon (Telestes souffia), Carassin commun (Carassius carassius), Carassin argenté (Carassius gibelio), Gardon (Rutilus rutilus), Rotengle (Scardinius erythrophthalmus), Toxostome (Parachondrostoma toxostoma)
    SPECIES_9b,     // Apron (Zingel asper), Chabots (Cottus sp), Goujons (Gobio sp), Grémille (Gymnocephalus cernuus), Lamproie de Planer (Lampetra planeri), Loche franche ( Barbatula barbatula), Loche de rivière (Cobitis taenia)
    SPECIES_10,     // Able de Heckel (Leucaspius delineatus), Bouvière (Rhodeus amarus), Epinoche (Gasterosteus gymnurus), Epinochette (Pungitius laevis), Vairons (Phoxinus sp)
    // SPECIES_11a,    // Anguille européenne [jaune]
    // SPECIES_11b     // Anguille européenne [civelle]
}
