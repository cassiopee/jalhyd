import { Nub } from "./internal_modules";
import { ParamDefinition } from "./internal_modules";

/**
 * A Nub that is meant to exist within a parent only
 */
export abstract class ChildNub extends Nub {

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    public unsetCalculatedParam(except: ParamDefinition) {
        if (this.parent) {
            this.parent.unsetCalculatedParam(except);
        }
    }

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    public get calculatedParam(): ParamDefinition {
        if (this.parent) {
            return this.parent.calculatedParam;
        }
        // For testing purpose without ParallelStructure
        return this._calculatedParam;
    }

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    public set calculatedParam(p: ParamDefinition) {
        if (this.parent) {
            this.parent.calculatedParam = p;
        } else {
            // For testing purpose without ParallelStructure
            this._calculatedParam = p;
        }
    }

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    public findFirstCalculableParameter(otherThan?: ParamDefinition) {
        if (this.parent) {
            return this.parent.findFirstCalculableParameter(otherThan);
        }
        return undefined;
    }

    /**
     * Forwards to parent, that has visibility over
     * all the parameters, including the Structure ones
     */
    public resetDefaultCalculatedParam(requirer?: ParamDefinition) {
        if (this.parent) {
            this.parent.resetDefaultCalculatedParam(requirer);
        }
    }

}
