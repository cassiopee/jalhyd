import { ParamFamily } from "./internal_modules";

import * as base64 from "base-64";

export interface IJalhydObject {
    readonly uid: string;
}

export interface INamedObject extends IJalhydObject {
    readonly symbol: string;
}

export interface IObjectWithFamily extends IJalhydObject {
    readonly family: ParamFamily;
}

export abstract class JalhydObject implements IJalhydObject {

    public get uid(): string {
        return this._uid;
    }

    public static get nextUID(): string {
        return base64.encode(Math.random().toString(36).substring(2)).substring(0, 6);
    }

    /** id numérique unique */
    private _uid: string;

    constructor() {
        this._uid = JalhydObject.nextUID;
    }

    /**
     * @WARNING utiliser uniquement pour conserver l'ID lorsqu'on charge des sessions
     */
    public setUid(uid: string) {
        this._uid = uid;
    }
}
