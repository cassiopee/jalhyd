import { ParamDefinition, ParamFamily } from "../param/param-definition";
import { ParamDomain, ParamDomainValue } from "../param/param-domain";
import { ParamsEquation } from "../param/params-equation";
import { RugoFondParams } from "./rugofond_params";

export class RugoFondMultipleParams extends RugoFondParams {
    /** Cote de fond bas amont (m) 
    */
    private _ZRB: ParamDefinition;
    /** Cote de fond haut amont (m)
    */
    private _ZRT: ParamDefinition;
     /** Nombre de tranche d'écoulement
    */
     private _NbT: ParamDefinition;

    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m) 
     * @param rL Largeur totale de la rampe  (m)
     * @param rIf Pente longitudinale de la rampe (m/m)
     * @param rCd Coefficient de débit
     * @param rd65 d65 des enrochements du fond (m)
     * @param ra Coefficient correcteur du Strickler(m1/3/s)
     * @param rNb Nombre de tranche d'écoulement
     * @param rZF1 Cote de fond amont (m)
     * @param rZRB Cote de fond bas amont (m)
     * @param rZRT Cote de fond haut amont (m)
     * @param rNbT Nombre de tranche d'écoulement
     * 
     */
    constructor( rQ: number, rZ1: number, rL: number, rIf: number,
        rCd: number, rd65: number, ra: number,rZF1: number, rZRB: number, rZRT: number, rNbT: number, nullParams = false) {
        super(rQ, rZ1, rL, rIf, rCd,rd65, ra, rZF1, nullParams);
        // type de rampe
        this._ZRB = new ParamDefinition(this, "ZRB", ParamDomainValue.ANY, "m", rZRB, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZRB);
        this._ZRT = new ParamDefinition(this, "ZRT", ParamDomainValue.ANY, "m", rZRT, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZRT);
        this._NbT = new ParamDefinition(this, "NbT", ParamDomainValue.POS, undefined, rNbT, undefined, undefined, nullParams);
        this.addParamDefinition(this._NbT);
    }
    
    public get ZRB() {
        return this._ZRB;
    }
    public get ZRT() {
        return this._ZRT;
    }
    public get NbT() {
        return this._NbT;
    }
}