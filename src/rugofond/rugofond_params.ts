import { ParamDefinition, ParamFamily } from "../param/param-definition";
import { ParamDomain, ParamDomainValue } from "../param/param-domain";
import { ParamsEquation } from "../param/params-equation";

export class RugoFondParams extends ParamsEquation {
    /** Débit total (m3/s) */
    public _Q: ParamDefinition;
    /** Cote de l'eau amont (m)(m) */
    private _Z1: ParamDefinition;
    /** Largeur totale de la rampe (m) */
    private _L: ParamDefinition;
    /** Pente longitudinale de la rampe (m/m) */
    private _If: ParamDefinition;
    /** Coefficient de débit*/
    private _Cd: ParamDefinition;
    /** d65 des enrochements du fond (m)*/
    private _d65: ParamDefinition;
     /** Coefficient correcteur du Strickler (m1/3/s) */
     private _a: ParamDefinition;
    /** Cote de fond amont (m) 
     * Radier horizontal uniquement
    */
    private _ZF1: ParamDefinition;


    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m) 
     * @param rL Largeur totale de la rampe  (m)
     * @param rIf Pente longitudinale de la rampe (m/m)
     * @param rCd Coefficient de débit
     * @param rd65 d65 des enrochements du fond (m)
     * @param ra Coefficient correcteur du Strickler(m1/3/s)
     * @param rZF1 Cote de fond amont (m)
     */
    constructor(rQ: number, rZ1: number, rL: number, rIf: number,
        rCd: number, rd65: number, ra: number, rZF1: number, nullParams = false) {
        super();
        // paramètres hydrauliques
        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS_NULL, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.addParamDefinition(this._Q);
        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._Z1);
        // géométrie de la crête
        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.LENGTHS, undefined, nullParams);
        this.addParamDefinition(this._L);
        this._Cd = new ParamDefinition(this, "Cd", new ParamDomain(ParamDomainValue.INTERVAL, 1E-3, 10), undefined, rCd, undefined, undefined, nullParams);
        this.addParamDefinition(this._Cd);
        // géométrie de la rampe
        this._If = new ParamDefinition(this, "If",
            new ParamDomain(ParamDomainValue.INTERVAL, 1E-8, 0.5), "m/m", rIf, ParamFamily.SLOPES, undefined, nullParams);
        this.addParamDefinition(this._If);
        this._d65 = new ParamDefinition(this, "d65", new ParamDomain(ParamDomainValue.INTERVAL, 1E-8, 3), "m", rd65, undefined, undefined, nullParams);
        this.addParamDefinition(this._d65);
        this._a = new ParamDefinition(this, "a", new ParamDomain(ParamDomainValue.INTERVAL, 13, 23), undefined, ra, undefined, undefined, undefined);
        this.addParamDefinition(this._a);
        // type de rampe
        this._ZF1 = new ParamDefinition(this, "ZF1", ParamDomainValue.ANY, "m", rZF1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this._ZF1);
    }

    public get Q() {
        return this._Q;
    }
    public get Z1() {
        return this._Z1;
    }
    public get L() {
        return this._L;
    }
    public get If() {
        return this._If;
    }
    public get Cd() {
        return this._Cd;
    }
    public get d65() {
        return this._d65;
    }
    public get a() {
        return this._a;
    }
    public get ZF1() {
        return this._ZF1;
    }
}