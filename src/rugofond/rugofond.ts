import { CalculatorType } from "../compute-node";
import { Message, MessageCode } from "../internal_modules";
import { Nub } from "../nub";
import { ParamCalculability, ParamFamily } from "../param/param-definition";
import { Result } from "../util/result";
import { RugoFondParams } from "./rugofond_params";

export class RugoFond extends Nub {

  constructor(prms: RugoFondParams, dbg: boolean = false) {
    super(prms, dbg);
    this._defaultCalculatedParam = this.prms.Q;
    this.resetDefaultCalculatedParam();
    this._intlType = "Rugofond";
  }

  public get prms(): RugoFondParams {
    return this._prms as RugoFondParams;
  }

  public Equation(sVarCalc: string): Result {
    let v: number;
    // Accélération de la pesanteur ( m/s2)
    let g = 9.81;
    switch (sVarCalc) {
      case "Q":
        this.calculatedParam = this.prms.Q;
        v = this.prms.Cd.v * Math.sqrt(2 * g) * Math.pow((this.prms.Z1.v - this.prms.ZF1.v), 3 / 2) * this.prms.L.v;
        break;

      case "Z1":
        this.calculatedParam = this.prms.Z1;
        v = Math.pow((this.prms.Q.v / (this.prms.Cd.v * this.prms.L.v * Math.sqrt(2 * g))), 2 / 3) + this.prms.ZF1.v;
        break;

      case "ZF1":
        this.calculatedParam = this.prms.ZF1;
        v = this.prms.Z1.v - Math.pow((this.prms.Q.v / (this.prms.Cd.v * this.prms.L.v * Math.sqrt(2 * g))), 2 / 3);
        break;

      case "L":
        this.calculatedParam = this.prms.L;
        v = this.prms.Q.v / (this.prms.Cd.v * Math.sqrt(2 * g) * Math.pow((this.prms.Z1.v - this.prms.ZF1.V), 3 / 2));
        break;
      case "Cd":
        this.calculatedParam = this.prms.Cd;
        v = this.prms.Q.v / ((Math.sqrt(2 * g)) * Math.pow((this.prms.Z1.v - this.prms.ZF1.v), 3 / 2) * this.prms.L.v)
        break;

      default:
        throw new Error("Addition.Equation() : invalid variable name " + sVarCalc);
    }
    if(isNaN(v)) {
      v = 0;
    }
    this.result.vCalc = v;
    return this.result;
  }

  public Calc(sVarCalc?: string, rInit?: number): Result {
    this.currentResultElement = this.Equation(sVarCalc);
    if (this.prms.Z1.v < this.prms.ZF1.v) {
      this.result.resultElement.vCalc = 0;
    }
    // Ajout des résultats complémentaires
    if (this.result.vCalc !== undefined && (this.prms.Z1.V > this.prms.ZF1.V)) {
      // Strickler équivalent
      let Ks = (this.prms.a.v / (Math.pow(this.prms.d65.v, 1 / 6)));
      // Hauteur normale
      let Yn = Math.pow(this.prms.Q.V / (this.prms.L.v * Ks * Math.sqrt(this.prms.If.v)), 3 / 5);
      if (isNaN(this.prms.Q.V)) {
        Yn = 0;
      }
      this.result.resultElement.values.Yn = Yn;

      if(this.parent) {
        if (Yn > 2/3 * (this.prms.Z1.v -this.prms.ZF1.v) && Yn <= (this.prms.Z1.v -this.prms.ZF1.v)) {
          const m = new Message(MessageCode.WARNING_RUGOFOND_WATERLINE_TOO_HIGHT);
          m.extraVar.number = String(this.findPositionInParent() + 1);
          m.extraVar.YnValue = String(Yn.toFixed(3));
          m.extraVar.criticalElevation = String((2/3 * (this.prms.Z1.v -this.prms.ZF1.v)).toFixed(3));
          this.parent.result.resultElement.log.add(m);
        }
  
        if (Yn > (this.prms.Z1.v -this.prms.ZF1.v)) {
          const m = new Message(MessageCode.ERROR_RUGOFOND_WATERLINE_TOO_HIGHT);
          m.extraVar.number = String(this.findPositionInParent() + 1);
          m.extraVar.YnValue = String(Yn.toFixed(3));
          m.extraVar.elevation = String(((this.prms.Z1.v -this.prms.ZF1.v)).toFixed(3));
          this.parent.result.resultElement.log.add(m);
        }
      }
      // Vitesse débitante
      let resVdeb = this.prms.Q.V / this.prms.L.v / Yn;
      if (isNaN(resVdeb)) {
        resVdeb = 0;
      }
      this.result.resultElement.values.Vdeb = resVdeb;
    }
    return this.result;
  }

  protected exposeResults() {
    this._resultsFamilies = {
      Yn: ParamFamily.ELEVATIONS,
      Vdeb: ParamFamily.SPEEDS,
    };
  }

  protected setParametersCalculability() {
    this.prms.Q.calculability = ParamCalculability.EQUATION;
    this.prms.Z1.calculability = ParamCalculability.EQUATION;
    this.prms.ZF1.calculability = ParamCalculability.EQUATION;
    this.prms.L.calculability = ParamCalculability.EQUATION;
    this.prms.Cd.calculability = ParamCalculability.EQUATION;
    this.prms.If.calculability = ParamCalculability.FIXED;
    this.prms.d65.calculability = ParamCalculability.FIXED;
    this.prms.a.calculability = ParamCalculability.FIXED;
  }
}
