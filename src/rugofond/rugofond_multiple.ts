import { Nub, ParamFamily, RugoFond, RugoFondParams, round } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { ParamCalculability, ParamValueMode } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { RugoFondMultipleParams } from "./rugofond_multiple_params";

/** Type de radier */
export enum RugofondType {
    /** Horizontale */
    HORIZONTAL,
    /** Incliné*/
    INCLINED,
}

export class RugoFondMultiple extends Nub implements Observer {

    /**
     * { symbol => string } map that defines units for extra results
     */
  private static _resultsUnits = {
    Q: "m³/s",
    Ks: "SI",
    Yn: "m",
    Vdeb:"m/s",
    LIncl: "m/m"
  };

    /** enfants castés au bon type */
    get children(): RugoFond[] {
        return this._children as RugoFond[];
    }

    /** parameters cast to the right type */
    get prms(): RugoFondMultipleParams {
        return this._prms as RugoFondMultipleParams;
    }

    constructor(prms: RugoFondMultipleParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.RugoFondMultiple);
        this._props.addObserver(this);
        this.setPropValue("rugofondType", RugofondType.HORIZONTAL);
        this._intlType = "RugoFondMultiple";
    }

    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string, rInit?: number): Result {
        this.copyPrmsToChildren();
        this.currentResultElement = super.Calc(sVarCalc, rInit);
         // Avertissement sur le domaine de validité de la pente pour le calcul de Yn
         if (this.prms.If.v < 0.02 || this.prms.If.v > 0.15) {
            const m = new Message(MessageCode.WARNING_RUGOFOND_SLOPE_OUT_OF_RANGE);
            m.extraVar.slope = String((this.prms.If.v * 100).toFixed(2));
            this.result.resultElement.log.add(m);
        }
        // Calcul des résultats complémentaires des enfants
        for (const child of this.children) {
            child.getParameter(sVarCalc).v = this.result.vCalc;
            child.Calc("Q");
        }
        // Strickler équivalent
        let Ks = (this.prms.a.v / (Math.pow(this.prms.d65.v, 1 / 6)));
        this.result.resultElement.values.Ks = Ks;

        // Calcul de l'inclinaison du radier
        if (this.getPropValue("rugofondType") === RugofondType.INCLINED) {
            this.result.resultElement.values.LIncl = (this.prms.ZRT.v - this.prms.ZRB.v) / this.prms.L.v;
        }
        else {
            this.result.resultElement.values.LIncl = 0;
        }
        return this.result;
    }

    public CalcSerie(rInit?: number): Result {
        if (this.getPropValue("rugofondType") === RugofondType.INCLINED) {
            this.generateInclinedRugoFond();
            for (const child of this.children) {
                // do we have a series of values 
                if (child.prms.Z1.hasMultipleValues || child.prms.ZF1.hasMultipleValues) {
                    let valsZ1;
                    let valsChildZF1;
                    if (this.prms.Z1.hasMultipleValues) {
                        valsZ1 = this.prms.Z1.getInferredValuesList();
                    } else {
                        valsZ1 = new Array(child.prms.ZF1.getInferredValuesList().length)
                            .fill(this.prms.Z1.singleValue);
                    }
                    if (child.prms.ZF1.hasMultipleValues) {
                        valsChildZF1 = child.prms.ZF1.getInferredValuesList();
                    } else {
                        valsChildZF1 = new Array(this.prms.Z1.getInferredValuesList().length)
                            .fill(child.prms.ZF1.singleValue);
                    }
                    // adjust lengths (should never happen when only one of the paramters is varying)
                    if (valsZ1.length > valsChildZF1.length) {
                        valsChildZF1 = child.prms.ZF1.getInferredValuesList(valsZ1.length);
                    } else if (valsZ1.length < valsChildZF1.length) {
                        valsZ1 = this.prms.Z1.getInferredValuesList(valsChildZF1.length);
                    }
                    child.prms.ZF1.setValues(valsChildZF1);
                } else {
                    // or just a single value ?
                    child.prms.ZF1.setValue(Math.max(child.prms.ZF1.singleValue, 0));
                }
            }
        }
        else {
            this.generateHorizontalRugoFond(this.prms.ZF1.V, this.prms.L.v);
        }
        // calculate and store depths once for all
        return super.CalcSerie(rInit);
    }

    public Equation(sVarCalc: string): Result {
        this.updateChildParams();
        const res = new Result(0, this);
        if (this.getPropValue("rugofondType") === RugofondType.INCLINED) {
            sVarCalc = "Q";
        }
        for (const child of this.children) {
            child.Equation(sVarCalc);
            if (child.prms.Z1.v < child.prms.ZF1.v) {
                const m = new Message(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_LOWER_THAN_ZF1);
                m.extraVar.number = String(child.findPositionInParent() + 1);
                res.resultElement.log.add(m);
            }
            if (child.prms.Z1.v === child.prms.ZF1.v) {
                const m = new Message(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1);
                m.extraVar.number = String(child.findPositionInParent() + 1);
                res.resultElement.log.add(m);
            }
            res.vCalc += child.result.vCalc;
        }
        return res;
    }

    public addChild(child: RugoFond, after?: number) {
        super.addChild(child, after);
    }

    public addDefaultChild(after?: number) {
        const child = new RugoFond(new RugoFondParams(this.prms.Q.V, this.prms.Z1.V, this.prms.L.V, this.prms.If.V, this.prms.Cd.V, this.prms.d65.V, this.prms.a.V, this.prms.ZF1.V));
        this.addChild(child, after);
    }

    public generateInclinedRugoFond() {
        // Calcul de la pente
        let inclined = (this.prms.ZRT.singleValue - this.prms.ZRB.singleValue) / this.prms.L.singleValue;
        this.deleteAllChildren();
        // Ajout des radiers et calcul de leur cote amont et affectation de leur largeur
        for (let i = 0; i < this.prms.NbT.V; i++) {
            this.addDefaultChild();
            this.children[this.children.length - 1].prms.ZF1.singleValue = this.prms.ZRT.singleValue - (this.prms.L.singleValue / this.prms.NbT.singleValue) * (i + 0.5) * inclined;
        }
    }

    public generateHorizontalRugoFond(ZF1: number, L: number) {
        this.deleteAllChildren();
        const child = new RugoFond(new RugoFondParams(this.prms.Q.v, this.prms.Z1.V, L, this.prms.If.V, this.prms.Cd.V, this.prms.d65.V, this.prms.a.V, ZF1));
        this.addChild(child)

    }

    public updateChildParams() {
        if (this.prms.NbT.visible === false) {
            this.prms.NbT.v = 1;
        }
        for (const child of this.children) {
            child.prms.Z1.v = this.prms.Z1.v;
            child.prms.Cd.v = this.prms.Cd.v;
            child.prms.L.v = this.prms.L.v / this.prms.NbT.v;
        }
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (data.action === "propertyChange" && data.name === "rugofondType") {
            if (data.value === RugofondType.INCLINED) { // switch to inclined apron mode
                this.prms.ZF1.visible = false;
                this.prms.ZF1.valueMode = ParamValueMode.SINGLE;
                this.prms.ZRB.visible = true;
                this.prms.ZRT.visible = true;
                this.prms.NbT.visible = true;
                this.prms.L.calculability = ParamCalculability.FIXED;
                // @see jalhyd#372 https://forgemia.inra.fr/cassiopee/jalhyd/-/issues/372
                this.prms.L.valueMode = ParamValueMode.SINGLE;
                this.prms.Cd.calculability = ParamCalculability.DICHO;
                this.prms.Z1.calculability = ParamCalculability.DICHO;

            } else { // switch to horizontal aprons mode
                this.prms.ZF1.visible = true;
                this.prms.ZRB.visible = false;
                this.prms.ZRT.visible = false;
                this.prms.NbT.visible = false;
                this.prms.Q.calculability = ParamCalculability.EQUATION;
                this.prms.Z1.calculability = ParamCalculability.EQUATION;
                this.prms.ZF1.calculability = ParamCalculability.EQUATION;
                this.prms.L.calculability = ParamCalculability.EQUATION;
                this.prms.Cd.calculability = ParamCalculability.EQUATION;
            }
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.ZF1.calculability = ParamCalculability.EQUATION;
        this.prms.Z1.calculability = ParamCalculability.EQUATION;
        this.prms.L.calculability = ParamCalculability.EQUATION;
        this.prms.If.calculability = ParamCalculability.FREE;
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.ZRB.calculability = ParamCalculability.FIXED;
        this.prms.ZRT.calculability = ParamCalculability.FIXED;
        this.prms.NbT.calculability = ParamCalculability.FIXED;
    }

    public static override resultsUnits() {
        return RugoFondMultiple._resultsUnits;
      }

    protected exposeResults() {
        this._resultsFamilies = {
            LIncl: ParamFamily.SLOPES
        };
    }

    private copyPrmsToChildren() {
        for (const child of this.children) {
            // Copy common parameters with rugoFond
            for (const v of ["Q", "Z1", "L", "Cd", "If", "d65", "a"]) {
                child.getParameter(v).v = this.getParameter(v).v;
            }
        }
    }
}
