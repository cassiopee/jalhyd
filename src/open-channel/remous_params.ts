import { ParamDefinition, ParamDomain, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * paramètres pour les courbes de remous
 */
export class CourbeRemousParams extends ParamsEquation {

    /** Tirant imposé à l'amont */
    private _Yamont: ParamDefinition;

    /** Tirant imposé à l'aval */
    private _Yaval: ParamDefinition;

    /** Longueur du bief */
    private _Long: ParamDefinition;

    /** Pas de discrétisation de l'espace (positif en partant de l'aval, négatif en partant de l'amont) */
    private _Dx: ParamDefinition;

    /** Cote de fond amont */
    private _ZF1: ParamDefinition;

    /** Cote de fond aval */
    private _ZF2: ParamDefinition;

    /** Cote de l'eau à l'amont */
    private _Z1: ParamDefinition;

    /** Cote de l'eau à l'aval */
    private _Z2: ParamDefinition;

    constructor(rZ1: number, rZ2: number, rZF1: number, rZF2: number, rLong: number, rDx: number, nullParams: boolean = false) {
        super();
        this._Yamont = new ParamDefinition(this, "Yamont", ParamDomainValue.POS, "m", undefined, undefined, false, nullParams);
        this._Yaval = new ParamDefinition(this, "Yaval", ParamDomainValue.POS, "m", undefined, undefined, false, nullParams);
        this._Long = new ParamDefinition(this, "Long", ParamDomainValue.POS, "m", rLong, ParamFamily.LENGTHS, undefined, nullParams);
        this._Dx = new ParamDefinition(this, "Dx", ParamDomainValue.POS, "m", rDx, undefined, undefined, nullParams);
        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._ZF1 = new ParamDefinition(this, "ZF1", ParamDomainValue.ANY, "m", rZF1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._ZF2 = new ParamDefinition(this, "ZF2", ParamDomainValue.ANY, "m", rZF2, ParamFamily.ELEVATIONS, undefined, nullParams);

        this.addParamDefinition(this._Yamont);
        this.addParamDefinition(this._Yaval);
        this.addParamDefinition(this._Long);
        this.addParamDefinition(this._Dx);
        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._ZF1);
        this.addParamDefinition(this._ZF2);
    }

    get Yamont() {
        return this._Yamont;
    }

    get Yaval() {
        return this._Yaval;
    }

    get Long() {
        return this._Long;
    }

    get Dx(): ParamDefinition {
        return this._Dx;
    }

    get Z1(): ParamDefinition {
        return this._Z1;
    }

    get Z2(): ParamDefinition {
        return this._Z2;
    }

    get ZF1(): ParamDefinition {
        return this._ZF1;
    }

    get ZF2(): ParamDefinition {
        return this._ZF2;
    }

    /**
     * Reconstruit Yamont et Yaval à partir de Z1/ZF1 et Z2/ZF2,
     * durant le calcul
     */
    public setYamontYavalFromElevations() {
        this._Yamont.singleValue = Math.max(1e-12, (this._Z1.v - this._ZF1.v));
        this._Yamont.v = this._Yamont.singleValue;
        this._Yaval.singleValue = Math.max(1e-12, (this._Z2.v - this._ZF2.v));
        this._Yaval.v = this._Yaval.singleValue;
    }
}
