import { CalculatorType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { ParamValues } from "../internal_modules";
import { Session } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { BiefParams, BiefRegime } from "../internal_modules";
import { MethodeResolution } from "../internal_modules";
import { CourbeRemous, ITrYResult } from "../internal_modules";
import { CourbeRemousParams } from "../internal_modules";
import { SectionNub } from "../internal_modules";
import { acSection } from "../internal_modules";

export class Bief extends SectionNub implements Observer {

    private courbeRemous: CourbeRemous;

    constructor(s: acSection, bp: BiefParams, dbg: boolean = false) {
        super(bp, dbg);
        this.setCalculatorType(CalculatorType.Bief);
        this.setSection(s);
        this._props.addObserver(this);
        this.regime = BiefRegime.Fluvial;
        this._defaultCalculatedParam = bp.Z1;
    }

    public get prms(): BiefParams {
        return this._prms as BiefParams;
    }

    public setSection(s: acSection) {
        super.setSection(s);
        this.setParametersCalculability(); // to override acSection's tuning
        if (this.section !== undefined) {
            // If from Section is not linkable here
            this.section.prms.If.visible = false;
            this.section.prms.Y.visible = false;
        }
    }

    public set regime(regime: BiefRegime) {
        this.setPropValue("regime", regime);
    }

    public Calc(sVarCalc?: string, rInit?: number): Result {
        // reinit local CourbeRemous with same section and same parameters
        const rp = new CourbeRemousParams(
            this.prms.Z1.v,
            this.prms.Z2.v,
            this.prms.ZF1.v,
            this.prms.ZF2.v,
            this.prms.Long.v,
            this.prms.Dx.v
        );
        const serialisedSection = this.section.serialise();
        const sectionCopy = Session.getInstance().unserialiseSingleNub(serialisedSection, false).nub;
        this.courbeRemous = new CourbeRemous(sectionCopy as acSection, rp, MethodeResolution.Trapezes);
        // set values for hidden parameters
        this.prms.setYamontYavalFromElevations();
        this.courbeRemous.prms.setYamontYavalFromElevations();
        this.courbeRemous.setIfFromElevations();
        // init .v for every parameter
        this.courbeRemous.section.copySingleValuesToSandboxValues();
        this.courbeRemous.copySingleValuesToSandboxValues();
        // if a section parameter is variating, copySingleValuesToSandboxValues() just set it
        // to undefined, because in general case the iterator is called after; here, re-set
        // the section copy parameters to the current section values
        for (const p of this.section.parameterIterator) {
            if (p.visible) { // do not affect hidden generated parameters (If)
                this.courbeRemous.section.getParameter(p.symbol).v = p.v;
            }
        }

        if (
            this.calculatedParam === this.prms.Z1
            && this.getPropValue("regime") === BiefRegime.Torrentiel
        ) {
            throw new Error("Bief.Equation() : cannot calculate Z1 in Torrential regime");
        }
        if (
            this.calculatedParam === this.prms.Z2
            && this.getPropValue("regime") === BiefRegime.Fluvial
        ) {
            throw new Error("Bief.Equation() : cannot calculate Z2 in Fluvial regime");
        }

        // regular calculation
        return super.Calc(sVarCalc, rInit);
    }

    public Equation(sVarCalc: string): Result {
        let v: number;
        const xValues = new ParamValues();
        const l = this.prms.Long.v;
        const dx = this.prms.Dx.v;
        xValues.setValues(0, l, dx);

        // in Dichotomie mode, update sandbox value of calculated param
        if (!this.calculatedParam.isAnalytical()) {
            // we're calculating Ks or Q, in section
            this.courbeRemous.section.getParameter(this.calculatedParam.symbol).v = this.calculatedParam.v;
        }

        switch (sVarCalc) {
            case "Z1":
                this.courbeRemous.section.CalcSection("Yc");
                const ligneFlu: ITrYResult = this.courbeRemous.calculFluvial(xValues);
                if (
                    ligneFlu !== undefined
                    && ligneFlu.trY !== undefined
                    && ligneFlu.trY[0] !== undefined
                ) {
                    v = ligneFlu.trY[0] + this.prms.ZF1.v;
                } else {
                    const res = new Result(new Message(MessageCode.ERROR_BIEF_Z1_CALC_FAILED), this);
                    res.log.insertLog(ligneFlu.log);
                    return res;
                }
                break;

            case "Z2":
                this.courbeRemous.section.CalcSection("Yc");
                const ligneTor: ITrYResult = this.courbeRemous.calculTorrentiel(xValues);
                if (
                    ligneTor !== undefined
                    && ligneTor.trY !== undefined
                    && ligneTor.trY[l] !== undefined
                ) {
                    v = ligneTor.trY[l] + this.prms.ZF2.v;
                } else {
                    const res = new Result(new Message(MessageCode.ERROR_BIEF_Z2_CALC_FAILED), this);
                    res.log.insertLog(ligneTor.log);
                    return res;
                }
                break;

            default:
                throw new Error("Bief.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    // interface Observer
    public update(sender: any, data: any) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "regime":
                    if (data.value === BiefRegime.Fluvial) {
                        this.prms.Z1.calculability = ParamCalculability.EQUATION;
                        if (this.calculatedParam === undefined || this.calculatedParam === this.prms.Z2) {
                            this.prms.Z1.setCalculated();
                        }
                        this.prms.Z2.calculability = ParamCalculability.FREE;
                    }
                    if (data.value === BiefRegime.Torrentiel) {
                        this.prms.Z2.calculability = ParamCalculability.EQUATION;
                        if (this.calculatedParam === undefined || this.calculatedParam === this.prms.Z1) {
                            this.prms.Z2.setCalculated();
                        }
                        this.prms.Z1.calculability = ParamCalculability.FREE;
                    }
                    break;
            }
        }
    }

    protected setParametersCalculability() {
        this.prms.Long.calculability = ParamCalculability.FREE;
        this.prms.Dx.calculability = ParamCalculability.FIXED;
        this.prms.Yamont.calculability = ParamCalculability.FIXED;
        this.prms.Yaval.calculability = ParamCalculability.FIXED;
        // do not set Z1 and Z2 here to avoid both being calculable at the same time
        this.prms.ZF1.calculability = ParamCalculability.FREE;
        this.prms.ZF2.calculability = ParamCalculability.FREE;
        // section params
        if (this.section) {
            this.section.prms.Ks.calculability = ParamCalculability.DICHO;
            this.section.prms.Q.calculability = ParamCalculability.DICHO;
            this.section.prms.If.calculability = ParamCalculability.FREE;
            this.section.prms.YB.calculability = ParamCalculability.FREE;
            this.section.prms.Y.calculability = ParamCalculability.FREE;
            this.section.prms.LargeurBerge.calculability = ParamCalculability.FREE;
            // parameters depending on section type
            const D = this.section.getParameter("D");
            if (D) {
                D.calculability = ParamCalculability.FREE;
            }
            const k = this.section.getParameter("k");
            if (k) {
                k.calculability = ParamCalculability.FREE;
            }
            const fruit = this.section.getParameter("Fruit");
            if (fruit) {
                fruit.calculability = ParamCalculability.FREE;
            }
            const largeurFond = this.section.getParameter("LargeurFond");
            if (largeurFond) {
                largeurFond.calculability = ParamCalculability.FREE;
            }
        }
    }
}
