import { CalculatorType } from "../internal_modules";
import { ParamCalculability, ParamFamily } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { SectionNub } from "../internal_modules";
import { SectionParams } from "../internal_modules";
import { acSection } from "../internal_modules";
import { cSnCirc } from "../internal_modules";
import { isGreaterThan } from "../internal_modules";

export class RegimeUniforme extends SectionNub {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        V: "m/s"
    };

    constructor(s: acSection, dbg: boolean = false) {
        super(new SectionParams(), dbg);
        this.setCalculatorType(CalculatorType.RegimeUniforme);
        this.setSection(s);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): SectionParams {
        return this._prms as SectionParams;
    }

    public Equation(sVarCalc: string): Result {
        this.debug("RU: Equation(" + sVarCalc + ")");
        let r: Result;

        if (typeof sVarCalc !== "string") {
            // dirty hack because calculated param descriptor for section params is an object { uid: , symbol: }
            sVarCalc = (sVarCalc as any).symbol;
        }

        switch (sVarCalc) {
            case "Y":
                r = this.section.CalcSection("Yn");
                break;

            case "Q":
                r = this.Calc_Qn();
                break;

            default:
                throw new Error("RegimeUniforme.Equation() : invalid variable name " + sVarCalc);
        }

        return r;
    }

    /**
     * Calcul d'une équation quelle que soit l'inconnue à calculer; déclenche le calcul en
     * chaîne des modules en amont si nécessaire
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    public Calc(sVarCalc?: string, rInit?: number): Result {
        const r = super.Calc(sVarCalc, rInit);

        // Vitesse moyenne
        const V = this.section.CalcSection("V", this.section.prms.Y.v);
        r.resultElement.values.V = V.vCalc;

        // Est-ce que ça déborde ?
        if (this.section.prms.Y.v > this.section.prms.YB.v) {
            r.resultElement.log.add(new Message(MessageCode.WARNING_SECTION_OVERFLOW));
        }

        // Est-ce qu'on est en charge sur une section circulaire ? @see #214
        if (
            this.section instanceof cSnCirc
            && this.section.prms.YB.v >= this.section.prms.D.v
            && isGreaterThan(this.section.prms.Y.v, this.section.prms.D.v, 1e-3)
        ) {
            this.currentResultElement = new Result(new Message(MessageCode.ERROR_RU_CIRC_LEVEL_TOO_HIGH));
            return this.result;
        }

        return r;
    }

    public setSection(s: acSection) {
        super.setSection(s);
        // had no analytical parameters before ?
        this._defaultCalculatedParam = this.getFirstAnalyticalParameter();
        this.resetDefaultCalculatedParam();
    }

    // tslint:disable-next-line:no-empty
    protected setParametersCalculability() { }

    protected adjustChildParameters(): void {
        this.section.prms.Q.calculability = ParamCalculability.EQUATION;
        this.section.prms.Y.calculability = ParamCalculability.EQUATION;
        this.section.prms.YB.calculability = ParamCalculability.FREE;
    }

    public static override resultsUnits() {
        return RegimeUniforme._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            V: ParamFamily.SPEEDS
        };
    }

    /**
     * Calcul du débit en régime uniforme.
     * @return Débit en régime uniforme
     */
    private Calc_Qn(): Result {
        if (this.section.prms.If.v <= 0) {
            return new Result(0, this);
        }

        const rR: Result = this.section.CalcSection("R", this.section.prms.Y.v);
        if (!rR) {
            return rR;
        }

        const rS: Result = this.section.CalcSection("S", this.section.prms.Y.v);
        if (!rS) {
            return rS;
        }

        this.section.prms.Q.v = this.section.prms.Ks.v * Math.pow(rR.vCalc, 2 / 3) * rS.vCalc * Math.sqrt(this.section.prms.If.v);
        return new Result(this.section.prms.Q.v, this);
    }

}
