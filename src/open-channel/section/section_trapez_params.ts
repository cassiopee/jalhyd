import { ParamDefinition, ParamFamily } from "../../internal_modules";
import { ParamDomainValue } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

export class ParamsSectionTrapez extends ParamsSection {
    private _LargeurFond: ParamDefinition; // Largeur au fond
    private _Fruit: ParamDefinition; // Fruit des berges

    constructor(rLargeurFond: number, rFruit: number, rY: number, rKs: number,
        rQ: number, rIf: number, rYB: number, nullParams: boolean = false) {

        // set LargeurBerge to default value so that it is not undefined when changing section type
        super(rY, 2.5, rKs, rQ, rIf, rYB, nullParams);
        this._LargeurFond = new ParamDefinition(this, "LargeurFond", ParamDomainValue.POS_NULL,
            "m", rLargeurFond, ParamFamily.WIDTHS, undefined, nullParams);
        this._Fruit = new ParamDefinition(this, "Fruit", ParamDomainValue.POS_NULL, "m/m", rFruit, undefined, undefined, nullParams);

        this.addParamDefinition(this._LargeurFond);
        this.addParamDefinition(this._Fruit);
        // hide params
        this.LargeurBerge.visible = false; // or else something might get linked to it although it is undefined
    }

    /**
     * Largeur au fond
     */
    get LargeurFond(): ParamDefinition {
        return this._LargeurFond;
    }

    /**
     * Fruit des berges
     */
    get Fruit(): ParamDefinition {
        return this._Fruit;
    }
}
