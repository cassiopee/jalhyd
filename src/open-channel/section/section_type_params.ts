import { ParamDefinition, ParamFamily } from "../../internal_modules";
import { ParamDomainValue } from "../../internal_modules";
import { ParamsEquation } from "../../internal_modules";
import { SessionSettings } from "../../internal_modules";

export abstract class ParamsSection extends ParamsEquation {

    public static readonly G: number = 9.81; /// Constante de gravité

    private _Ks: ParamDefinition; // Strickler
    private _Q: ParamDefinition; // Débit
    private _If: ParamDefinition;  // Pente du fond
    private _YB: ParamDefinition;  // Hauteur de berge
    private _iPrec: number;  // Précision en nombre de décimales
    private _Y: ParamDefinition;          // Tirant d'eau
    private _LargeurBerge: ParamDefinition; // largeur au débordement

    constructor(rY: number,
        rLargeurBerge: number,
        rKs: number,
        rQ: number,
        rIf: number,
        rYB: number,
        nullParams: boolean = false
    ) {
        super();
        this._Ks = new ParamDefinition(this, "Ks", ParamDomainValue.POS, "SI", rKs, ParamFamily.STRICKLERS, undefined, nullParams);
        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS_NULL, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this._If = new ParamDefinition(this, "If", ParamDomainValue.ANY, "m/m", rIf, ParamFamily.SLOPES, undefined, nullParams);
        this._YB = new ParamDefinition(this, "YB", ParamDomainValue.POS, "m", rYB, ParamFamily.HEIGHTS, undefined, nullParams);
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.POS_NULL, "m", rY, ParamFamily.HEIGHTS, undefined, nullParams);
        this._LargeurBerge = new ParamDefinition(
            this, "LargeurBerge", ParamDomainValue.POS_NULL, "m", rLargeurBerge, ParamFamily.WIDTHS, undefined, nullParams);

        this._iPrec = Math.round(-Math.log(SessionSettings.precision) / Math.log(10));

        this.addParamDefinition(this._Ks);
        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._If);
        this.addParamDefinition(this._YB);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._LargeurBerge);

        if (rY === undefined) { // avoid undefined on Swap()
            this._Y.v = 1E6;
        }
    }

    /**
     * Strickler
     */
    get Ks(): ParamDefinition {
        return this._Ks;
    }

    /**
     * Débit
     */
    get Q(): ParamDefinition {
        return this._Q;
    }

    /**
     * Pente du fond
     */
    get If(): ParamDefinition {
        return this._If;
    }

    /**
     * Précision en nombre de décimales
     */
    get iPrec(): number {
        return this._iPrec;
    }

    /**
     * Hauteur de berge
     */
    get YB(): ParamDefinition {
        return this._YB;
    }

    /**
     * Tirant d'eau
     */
    get Y(): ParamDefinition {
        return this._Y;
    }

    /**
     * Largeur au débordement
     */
    get LargeurBerge(): ParamDefinition {
        return this._LargeurBerge;
    }
}
