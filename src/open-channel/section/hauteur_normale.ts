import { Result } from "../../internal_modules";
import { acNewton } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Calcul de la hauteur normale
 */
// tslint:disable-next-line:class-name
export class cHautNormale extends acNewton {
    /**
     * Section sur laquuelle porte le calcul
     */
    // tslint:disable-next-line:variable-name
    private Sn: acSection;

    /**
     * Débit connu
     */
    private Q: number;

    /**
     * Coefficient de Strickler
     */
    // tslint:disable-next-line:variable-name
    private Ks: number;

    /**
     * Pente du fond
     */
    // tslint:disable-next-line:variable-name
    private If: number;

    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn: acSection, maxIter: number, dbg: boolean = false) {
        super(Sn.prms, maxIter, dbg);
        this.Sn = Sn;
        this.Q = Sn.prms.Q.v;
        this.Ks = Sn.prms.Ks.v;
        this.If = Sn.prms.If.v;
    }

    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcFn(rX: number): Result {
        // Calcul de la fonction
        /* return (this.Q - this.Ks * Math.pow(this.Sn.CalcSection("R", rX), 2 / 3)
            * this.Sn.CalcSection("S", rX) * Math.sqrt(this.If)); */
        const rR: Result = this.Sn.CalcSection("R", rX);
        if (!rR.ok) {
            return rR;
        }

        const rS: Result = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }

        const v = (this.Q - this.Ks * Math.pow(rR.vCalc, 2 / 3) * rS.vCalc * Math.sqrt(this.If));
        return new Result(v);
    }

    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcDer(rX: number): Result {
        const rDR: Result = this.Sn.CalcSection("dR");
        if (!rDR.ok) {
            return rDR;
        }

        const rR: Result = this.Sn.CalcSection("R");
        if (!rR.ok) {
            return rR;
        }

        const rS: Result = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }

        // L'initialisation a été faite lors de l'appel à CalcFn

        // let Der = 2 / 3 * this.Sn.CalcSection("dR") * Math.pow(this.Sn.CalcSection("R"), -1 / 3)
        //                 * this.Sn.CalcSection("S");
        // tslint:disable-next-line:variable-name
        let Der = 2 / 3 * rDR.vCalc * Math.pow(rR.vCalc, -1 / 3) * rS.vCalc;

        const rR2: Result = this.Sn.CalcSection("R");
        if (!rR2.ok) {
            return rR2;
        }

        const rB: Result = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }

        // Der = Der + Math.pow(this.Sn.CalcSection("R"), 2 / 3) * this.Sn.CalcSection("B");
        Der = Der + Math.pow(rR2.vCalc, 2 / 3) * rB.vCalc;

        Der = Der * -this.Ks * Math.sqrt(this.If);
        return new Result(Der);
    }
}
