import { SectionType } from "../../internal_modules";
import { ParamCalculability } from "../../internal_modules";
import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { ParamsSectionCirc } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Calculs de la section circulaire
 */
// tslint:disable-next-line:class-name
export class cSnCirc extends acSection {

    get prms(): ParamsSectionCirc {
        return this._prms as ParamsSectionCirc;
    }
    protected nbDessinPoints = 50;

    constructor(prms: ParamsSectionCirc, dbg: boolean = false) {
        super(prms, dbg);
        this.nodeType = SectionType.SectionCercle;
        // commenté car si D est la variable à déterminer, il peut valoir n'importe
        // quoi... if (prms.YB.v > D) { prms.YB.v = D; } // On place la berge au sommet du cercle

        // if (this.prms.D.isDefined() && this.prms.YB.isDefined())
        //         this.CalcGeo('B');
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.D.calculability = ParamCalculability.DICHO;
    }

    protected Calc_Alpha(): Result {
        this.debug("Calc_Alpha : bSnFermee " + this.bSnFermee);

        if (this.prms.Y.v <= 0) {
            return new Result(0);
        }

        if (this.isDebordement()) {
            if (this.bSnFermee) {
                return new Result(Math.PI);
            }
            return this.Calc_AlphaY(this.prms.YB.v, this.prms.D.v);
        }

        return this.Calc_AlphaY(this.prms.Y.v, this.prms.D.v);
    }

    /**
     * Calcul de dérivée de l'angle Alpha de la surface libre par rapport au fond.
     * @return dAlpha
     */
    protected Calc_dAlpha(): Result {
        if (this.prms.Y.v <= 0 || this.isDebordement()) {
            return new Result(0);
        }

        const v = 2. / this.prms.D.v / Math.sqrt(1. - Math.pow(1. - 2. * this.prms.Y.v / this.prms.D.v, 2));
        return new Result(v);
    }

    /**
     * Calcul de la largeur au miroir.
     * @return B
     */
    protected Calc_B(): Result {
        if (this.isDebordement()) {
            return this.Calc_LargeurBerge();
        }

        this.debug("circ.Calc_B() : PAS débordement");
        if (this.prms.D.hasCurrentValue && this.prms.Y.hasCurrentValue) {
            const rAlpha: Result = this.calcFromY("Alpha");
            if (!rAlpha.ok) {
                return rAlpha;
            }

            const res = this.prms.D.v * Math.sin(rAlpha.vCalc);
            this.debug("circ.Calc_B() : res=" + res);
            return new Result(res);
        }

        const e: Message = new Message(MessageCode.ERROR_PARAMDEF_VALUE_UNDEFINED);
        e.extraVar.diameterValue = this.prms.D.hasCurrentValue ? String(this.prms.D.v) : "undefined";
        e.extraVar.yValue = this.prms.Y.hasCurrentValue ? String(this.prms.Y.v) : "undefined";
        throw e;
    }

    /**
     * Calcul du périmètre mouillé.
     * @return B
     */
    protected Calc_P(): Result {
        let v;
        if (!this.bSnFermee && this.isDebordement()) {
            // On n'ajoute pas le périmètre dans le cas d'une fente de Preissmann
            // return this.CalcGeo("P") + super.Calc_P_Debordement(this.valeurYDebordement());

            const rGeoP: Result = this.CalcGeo("P");
            if (!rGeoP.ok) {
                return rGeoP;
            }

            const rPDeb: Result = super.Calc_P_Debordement(this.valeurYDebordement());
            if (!rPDeb.ok) {
                return rPDeb;
            }

            v = rGeoP.vCalc + rPDeb.vCalc;
            return new Result(v);
        }

        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        v = this.prms.D.v * rAlpha.vCalc;
        return new Result(v);
    }

    /**
     * Calcul de la surface mouillée.
     * @return S
     */
    protected Calc_S(): Result {
        let v;
        if (!this.bSnFermee && this.isDebordement()) {
            // return this.CalcGeo("S") + super.Calc_S_Debordement(this.valeurYDebordement());
            const rGeoS: Result = this.CalcGeo("S");
            if (!rGeoS.ok) {
                return rGeoS;
            }

            const rSDeb: Result = super.Calc_S_Debordement(this.valeurYDebordement());
            if (!rSDeb.ok) {
                return rSDeb;
            }

            v = rGeoS.vCalc + rSDeb.vCalc;
            return new Result(v);
        }

        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        // return Math.pow(this.prms.D.v, 2) / 4 * (alpha - Math.sin(alpha) * Math.cos(alpha));
        const alpha = rAlpha.vCalc;
        v = Math.pow(this.prms.D.v, 2) / 4 * (alpha - Math.sin(alpha) * Math.cos(alpha));
        return new Result(v);
    }

    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    protected Calc_dP(): Result {
        if (!this.bSnFermee && this.isDebordement()) {
            return super.Calc_dP_Debordement();
        }

        const rDAlpha: Result = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }

        const v = this.prms.D.v * rDAlpha.vCalc;
        return new Result(v);
    }

    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    protected Calc_dB(): Result {
        if (this.isDebordement()) {
            return super.Calc_dB_Debordement();
        }

        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const rDAlpha: Result = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }

        const v = this.prms.D.v * rDAlpha.vCalc * Math.cos(rAlpha.vCalc);
        return new Result(v);
    }

    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_SYg(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const alpha = rAlpha.vCalc;

        // tslint:disable-next-line:variable-name
        let SYg = Math.sin(alpha) - Math.pow(Math.sin(alpha), 3) / 3 - alpha * Math.cos(alpha);
        SYg = Math.pow(this.prms.D.v, 3) / 8 * SYg;
        return new Result(SYg);
    }

    /**
     * Calcul de la dérivée de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_dSYg(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const rDAlpha: Result = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }

        const alpha = rAlpha.vCalc;
        const dAlpha = rDAlpha.vCalc;

        const cos = Math.cos(alpha);
        const sin = Math.sin(alpha);
        // tslint:disable-next-line:variable-name
        let SYg = dAlpha * cos;
        SYg += - dAlpha * cos * Math.pow(sin, 2);
        SYg += - dAlpha * cos + alpha * dAlpha * sin;
        SYg = 3 * Math.pow(this.prms.D.v, 3) / 8 * SYg;
        return new Result(SYg);
    }

    /**
     * valeur du débordement
     * Le tirant d'eau est soustrait soit à la côte de berge, soit au diamètre
     * au cas où le canal soit "enterré" (côte de berge > diamètre)
     */
    private valeurYDebordement(): number {
        return this.prms.Y.v - Math.min(this.prms.YB.v, this.prms.D.v);
        // >= 0 par définition, et toujours vrai car utilisé après test isDebordement()
    }

    /**
     * teste le débordement.
     * @returns true en cas de débordement
     */
    private isDebordement(): boolean {
        this.debug("circ.isDebordement() : Y " + this.prms.Y.toString());
        this.debug("circ.isDebordement() : D " + this.prms.D.toString());
        this.debug("circ.isDebordement() : YB " + this.prms.YB.toString());
        this.debug("circ.isDebordement() : res=" + (this.prms.Y.v > Math.min(this.prms.D.v, this.prms.YB.v)));

        return this.prms.Y.v > Math.min(this.prms.D.v, this.prms.YB.v);
    }

    /**
     * Calcul de l'angle Alpha de la surface libre par rapport au fond.
     * @return Alpha
     */
    private Calc_AlphaY(Y: number, D: number): Result {
        const alpha = Math.acos(1. - Y / (D / 2.));
        return new Result(Math.min(alpha, Math.PI));
    }

    private Calc_LargeurBerge(): Result {

        const d = this.prms.D.v;
        const yb = Math.min(this.prms.YB.v, d);

        const rAY = this.Calc_AlphaY(yb, d);
        if (!rAY.ok) {
            return rAY;
        }

        // let res = d * Math.sin(this.Calc_AlphaY(yb, d));
        const res = d * Math.sin(rAY.vCalc);

        return new Result(res);
    }
}
