import { SectionType } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { ParamsSectionRectang } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Calculs de la section rectangulaire
 */
// tslint:disable-next-line:class-name
export class cSnRectang extends acSection {
    constructor(prms: ParamsSectionRectang, dbg: boolean = false) {
        super(prms, dbg);
        this.nodeType = SectionType.SectionRectangle;
    }

    get prms(): ParamsSectionRectang {
        return this._prms as ParamsSectionRectang;
    }

    /**
     * Calcul du périmètre mouillé
     * @return Périmètre mouillé (m)
     */
    protected Calc_P(): Result {
        // return this.prms.LargeurBerge.v + super.Calc_P_Debordement(this.prms.Y.v);
        const rPDeb: Result = super.Calc_P_Debordement(this.prms.Y.v);
        if (!rPDeb.ok) {
            return rPDeb;
        }

        const v = this.prms.LargeurBerge.v + rPDeb.vCalc;
        return new Result(v);
    }

    protected Calc_dP(): Result {
        return super.Calc_dP_Debordement();
    }

    protected Calc_B(): Result {
        return super.Calc_B_Debordement();
    }

    protected Calc_dB(): Result {
        return super.Calc_dB_Debordement();
    }

    protected Calc_S(): Result {
        return super.Calc_S_Debordement(this.prms.Y.v);
    }

    /**
     * Calcul du tirant d'eau conjugué avec la formule analytique pour la section rectangulaire
     * @return tirant d'eau conjugué
     */
    protected CalcYco(): Result {
        const rFR: Result = this.calcFromY("Fr");
        if (!rFR.ok) {
            return rFR;
        }

        const v = this.prms.Y.v * (Math.sqrt(1 + 8 * Math.pow(rFR.vCalc, 2)) - 1) / 2;
        return new Result(v);
    }

    /**
     * Critical depth calculation.
     * Analytical formula

     * @return Result object with critical depth
     */
    protected Calc_Yc(): Result {
        const v = Math.pow(
            Math.pow(this.prms.Q.v / this.prms.LargeurBerge.v, 2) / ParamsSectionRectang.G,
            1 / 3
        );
        return super.Calc_Yc(v);
    }
}
