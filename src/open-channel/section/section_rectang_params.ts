import { ParamsSection } from "../../internal_modules";

export class ParamsSectionRectang extends ParamsSection {
    constructor(rY: number, rLargeurFond: number, rKs: number, rQ: number, rIf: number, rYB: number, nullParams: boolean = false) {

        super(rY,
            rLargeurFond, // LargeurBerge=LargeurFond
            rKs, rQ, rIf, rYB, nullParams);
    }
}
