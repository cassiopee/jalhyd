import { ParamDefinition, ParamFamily } from "../../internal_modules";
import { ParamDomainValue } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

export class ParamsSectionCirc extends ParamsSection {
	private _D: ParamDefinition;          // Diamètre du cercle

	constructor(rD: number, rY: number, rKs: number, rQ: number, rIf: number, rYB: number, nullParams: boolean = false) {
		// set LargeurBerge to default value so that it is not undefined when changing section type
		super(rY, 2.5, rKs, rQ, rIf, rYB, nullParams);
		this._D = new ParamDefinition(this, "D", ParamDomainValue.POS, "m", rD, ParamFamily.DIAMETERS, undefined, nullParams);
		this.addParamDefinition(this._D);
		// hide params
		this.LargeurBerge.visible = false; // or else something might get linked to it although it is undefined
	}

	/**
	 * Diamètre du cercle
	 */
	get D(): ParamDefinition {
		return this._D;
	}
}
