import { CalculatorType, SectionType } from "../../internal_modules";
import { Nub } from "../../internal_modules";
import { ParamCalculability, ParamDefinition } from "../../internal_modules";
import { Props } from "../../internal_modules";
import { SessionSettings } from "../../internal_modules";
import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { cHautConjuguee } from "../../internal_modules";
import { cHautCorrespondante } from "../../internal_modules";
import { cHautCritique } from "../../internal_modules";
import { cHautNormale } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

/**
 * Gestion commune pour les différents types de section.
 * Comprend les formules pour la section rectangulaire pour gérer les débordements
 */
// tslint:disable-next-line:class-name
export abstract class acSection extends Nub {
    [key: string]: any; // pour pouvoir faire this['methode]();

    private static _availableSectionTypes: { id: string, value: SectionType }[] = [
        {
            id: "SectionCercle",
            value: SectionType.SectionCercle
        },
        {
            id: "SectionRectangle",
            value: SectionType.SectionRectangle
        },
        {
            id: "SectionTrapeze",
            value: SectionType.SectionTrapeze
        },
        {
            id: "SectionPuissance",
            value: SectionType.SectionPuissance
        }
    ];

    get prms(): ParamsSection {
        return this._prms as ParamsSection;
    }

    public get HautCritique() {
        return this._hautCritique;
    }

    /**
     * true si la section est fermée (on considère alors qu'il existe une fente de Preissmann)
     */
    protected bSnFermee: boolean = false;
    protected arCalcGeo: { [key: string]: number } = {}; /// Données ne dépendant pas de la cote de l'eau

    private _hautCritique: Result;  // Tirant d'eau critique

    /**
     * Tableau contenant les données dépendantes du tirant d'eau this->rY.
     *
     * Les clés du tableau peuvent être :
     * - S : la surface hydraulique
     * - P : le périmètre hydraulique
     * - R : le rayon hydraulique
     * - B : la largeur au miroir
     * - J : la perte de charge
     * - Fr : le nombre de Froude
     * - dP : la dérivée de P par rapport Y
     * - dR : la dérivée de R par rapport Y
     * - dB : la dérivée de B par rapport Y
     */
    private arCalc: { [key: string]: number } = {};
    // tslint:disable-next-line:variable-name
    private Y_old: number; /// Mémorisation du tirant d'eau pour calcul intermédiaire
    // tslint:disable-next-line:variable-name
    private Calc_old = {}; /// Mémorisation des données hydrauliques pour calcul intermédiaire

    /**
     * flag de débuggage pour Newton
     */
    private _newtonDbg: boolean;

    private _indentCalc: number;

    /**
     * Construction de la classe.
     * Calcul des hauteurs normale et critique
     */
    constructor(prms: ParamsSection, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Section);
        this._newtonDbg = dbg;
        this._intlType = "Section";
    }

    public static get availableSectionTypes() {
        return acSection._availableSectionTypes;
    }

    public get nodeType() {
        return this._props.getPropValue("nodeType");
    }

    protected set nodeType(nt: SectionType) {
        this._props.setPropValue("nodeType", nt);
    }

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Section ones
     */
    public get calculatedParam(): ParamDefinition {
        if (this.parent) {
            return this.parent.calculatedParam;
        }
        return undefined;
    }

    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Section ones
     */
    public set calculatedParam(p: ParamDefinition) {
        if (this.parent) {
            this.parent.calculatedParam = p;
        }
    }

    public clone(): acSection {
        return Object.create(this);
    }

    /**
     * Efface toutes les données calculées pour forcer le recalcul
     * @param bGeo Réinitialise les données de géométrie aussi
     */
    public Reset(bGeo = true) {
        this.debug("reset(" + bGeo + ")");
        this.arCalc = {};
        if (bGeo) {
            this.arCalcGeo = {};
            this.bSnFermee = false;
        }
    }

    public debug(m: string) {
        super.debug(this.calcIndent() + m);
    }

    /**
     * The parent is the Nub that holds the parameter to calculate;
     * ask it to perform calculation and copy its result to local
     * result, to facilitate values reading by targetting modules
     * (used by triggerChainCalculation)
     */
    public CalcSerie(rInit?: number): Result {
        this.currentResultElement = this.parent.CalcSerie(rInit);
        return this.result;
    }

    /**
     * Calcul des données à la section
     * effectue un reset du cache des résultats
     * @param sDonnee Clé de la donnée à calculer (voir this->arCalc)
     * @param rY valeur de Y à utiliser
     * @return la donnée calculée
     */
    public CalcSection(sDonnee: string, rY?: number): Result {
        this.debug(`Calc(${sDonnee},${rY})`);
        this._indentCalc = -1;
        this.Reset(true);
        return this.calcFromY(sDonnee, rY);
    }

    /**
     * Proxy to parent SectionNub, to compute non-hydraulic variables
     * @param sVarCalc
     * @param rInit
     */
    public Calc(sVarCalc?: string, rInit?: number): Result {
        return this.parent.Calc(sVarCalc, rInit);
    }

    public Equation(sVarCalc: string): Result {
        throw new Error("acSection.Equation() : cannot be called");
    }

    protected setParametersCalculability() {
        this.prms.Ks.calculability = ParamCalculability.DICHO;
        this.prms.Q.calculability = ParamCalculability.DICHO;
        this.prms.If.calculability = ParamCalculability.DICHO;
        this.prms.YB.calculability = ParamCalculability.DICHO;
        this.prms.Y.calculability = ParamCalculability.DICHO;
        this.prms.LargeurBerge.calculability = ParamCalculability.DICHO;
    }

    /**
     * calcul des données à la section dépendant de Y
     */
    protected calcFromY(sDonnee: string, rY?: number): Result {
        this._indentCalc++;
        this.debug("in calcFromY(" + sDonnee + ", rY=" + rY + ") old " + sDonnee + "=" + this.arCalc[sDonnee]);
        this.debug("this.Y=" + this.prms.Y.toString());

        if (rY !== undefined && (!this.prms.Y.isDefined || (this.prms.Y.isDefined && rY !== this.prms.Y.v))) {
            this.prms.Y.v = rY;
            // On efface toutes les données dépendantes de Y pour forcer le calcul
            this.Reset(false);
        }
        let res;
        if (this.arCalc[sDonnee] === undefined) {
            // La donnée a besoin d'être calculée
            switch (sDonnee) {
                case "I-J": // Variation linéaire de l'énergie spécifique (I-J) en m/m
                    const rJ: Result = this.calcFromY("J");
                    if (rJ.ok) {
                        this.arCalc[sDonnee] = this.prms.If.v - rJ.vCalc;
                        res = new Result(this.arCalc[sDonnee]);
                    } else {
                        res = rJ;
                    }
                    break;

                default:
                    const methode = "Calc_" + sDonnee;

                    /* !!!!!
                       si on réunit les 2 lignes suivantes
                       ( this.arCalc[sDonnee] = this[methode](); ),
                       il arrive que this.arCalc[sDonnee] soit affecté à
                       undefined alors que this[methode]() renvoie
                       une valeur bien définie
                       !!!!!
                     */
                    res = this[methode]();
                    if (res.ok) {
                        this.arCalc[sDonnee] = res.vCalc;
                    }
                    break;
            }
            this.debug("calcFromY(" + sDonnee + ") resultat -> " + this.arCalc[sDonnee]);
        } else {
            this.debug("calcFromY(" + sDonnee + ") cache= " + this.arCalc[sDonnee]);
            res = new Result(this.arCalc[sDonnee]);
        }
        this._indentCalc--;

        this.debug("this.Y=" + this.prms.Y.toString());

        // return this.arCalc[sDonnee];
        return res;
    }

    /**
     * Calcul des données uniquement dépendantes de la géométrie de la section
     * @param sDonnee Clé de la donnée à calculer (voir this->arCalcGeo)
     * @return la donnée calculée
     */
    protected CalcGeo(sDonnee: string): Result {
        this._indentCalc++;
        this.debug("in CalcGeo(" + sDonnee + ") old " + sDonnee + "=" + this.arCalcGeo[sDonnee]);
        this.debug("this.Y=" + this.prms.Y.toString());

        // Si la largeur aux berges n'a pas encore été calculée, on commence par ça
        if (sDonnee !== "B" && this.arCalcGeo.B === undefined) {
            this.CalcGeo("B");
        }

        let res: Result;
        if (this.arCalcGeo[sDonnee] === undefined) {
            // La donnée a besoin d'être calculée
            this.Swap(true); // On mémorise les données hydrauliques en cours
            this.Reset(false);
            this.prms.Y.v = this.prms.YB.v;
            switch (sDonnee) {
                case "B": // Largeur aux berges
                    // this.arCalcGeo[sDonnee] = this.Calc_B();
                    res = this.Calc_B();
                    if (res.ok) {
                        this.arCalcGeo[sDonnee] = res.vCalc;
                        if (this.arCalcGeo[sDonnee] < this.prms.YB.v / 100) {
                            // Section fermée
                            this.bSnFermee = true;
                            // On propose une fente de Preissmann
                            // égale à 1/100 de la hauteur des berges
                            this.arCalcGeo[sDonnee] = this.prms.YB.v / 100;
                        }
                        this.prms.LargeurBerge.v = this.arCalcGeo[sDonnee];
                    }
                    break;

                default:
                    const methode = "Calc_" + sDonnee;
                    // this.arCalcGeo[sDonnee] = this[methode]();
                    res = this[methode]();
                    if (res.ok) {
                        this.arCalcGeo[sDonnee] = res.vCalc;
                    }
                    break;
            }
            this.Swap(false); // On restitue les données hydrauliques en cours
            this.debug("CalcGeo(" + sDonnee + ") resultat -> " + this.arCalcGeo[sDonnee]);
        } else {
            this.debug("CalcGeo(" + sDonnee + ") cache= " + this.arCalcGeo[sDonnee]);
            res = new Result(this.arCalcGeo[sDonnee]);
        }

        // this.debug('this.Y=' + this.prms.Y.toString());

        this._indentCalc--;

        // return this.arCalcGeo[sDonnee];
        return res;
    }

    /**
     * Calcul de la surface hydraulique.
     * @return La surface hydraulique
     */
    protected abstract Calc_S(): Result;

    protected Calc_dS(): Result {
        return this.calcFromY("B"); // largeur au miroir
    }

    /**
     * Calcul de la surface hydraulique en cas de débordement
     * @param Y hauteur d'eau au delà de la berge
     */
    protected Calc_S_Debordement(Y: number): Result {
        this.debug("section->CalcS(rY=" + Y + ") LargeurBerge=" + this.CalcGeo("B"));
        // return Y * this.CalcGeo("B");
        const rB: Result = this.CalcGeo("B");
        if (!rB.ok) {
            return rB;
        }

        return new Result(Y * rB.vCalc);
    }

    /**
     * Calcul du périmètre hydraulique.
     * @return Le périmètre hydraulique
     */
    protected abstract Calc_P(): Result;

    /**
     * Calcul du périmètre hydraulique en cas de débordement
     * @param Y hauteur d'eau au dela de la berge
     */
    protected Calc_P_Debordement(Y: number): Result {
        return new Result(2 * Y);
    }

    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    protected abstract Calc_dP(): Result;

    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau en cas de débordement
     * @return la dérivée du périmètre hydraulique par rapport au tirant d'eau en cas de débordement
     */
    protected Calc_dP_Debordement(): Result {
        return new Result(2);
    }

    /**
     * Calcul de la largeur au miroir.
     * @return La largeur au miroir
     */
    protected abstract Calc_B(): Result;

    /**
     * Calcul de la largeur au miroir en cas de débordement
     * @return La largeur au miroir en cas de débordement
     */
    protected Calc_B_Debordement(): Result {
        // return this.prms.LargeurBerge.v;
        return new Result(this.prms.LargeurBerge.v);
    }

    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    protected abstract Calc_dB(): Result;

    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau en cas de débordement
     * @return la dérivée de la largeur au miroir par rapport au tirant d'eau en cas de débordement
     */
    protected Calc_dB_Debordement(): Result {
        return new Result(0);
    }

    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_SYg(): Result {
        // return Math.pow(this.prms.Y.v, 2) * this.CalcGeo("B") / 2;

        const rGeoB: Result = this.CalcGeo("B");
        if (!rGeoB.ok) {
            return rGeoB;
        }

        // return Math.pow(this.prms.Y.v, 2) * this.CalcGeo("B") / 2;
        const v = Math.pow(this.prms.Y.v, 2) * rGeoB.vCalc / 2;
        return new Result(v);
    }

    /**
     * Calcul de la dérivée distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_dSYg(): Result {
        const rGeoB: Result = this.CalcGeo("B");
        if (!rGeoB.ok) {
            return rGeoB;
        }

        // return this.prms.Y.v * this.CalcGeo("B");
        const v = this.prms.Y.v * rGeoB.vCalc;
        return new Result(v);
    }

    /**
     * Calcul de l'angle Alpha entre la surface libre et le fond pour les sections circulaires.
     * @return Angle Alpha pour une section circulaire, 0 sinon.
     */
    protected Calc_Alpha(): Result {
        return new Result(0);
    }

    /**
     * Calcul de la dérivée de l'angle Alpha entre la surface libre et le fond pour les sections circulaires.
     * @return Dérivée de l'angle Alpha pour une section circulaire, 0 sinon.
     */
    protected Calc_dAlpha(): Result {
        return new Result(0);
    }

    /**
     * Mémorise les données hydrauliques en cours ou les restitue
     * @param bMem true pour mémorisation, false pour restitution
     */
    private Swap(bMem: boolean) {
        if (bMem) {
            this.debug("save Y=" + this.prms.Y.toString());
            this.Y_old = this.prms.Y.v;
            this.Calc_old = this.arCalc;
        } else {
            this.debug("restore Y=" + this.Y_old);
            this.prms.Y.v = this.Y_old;
            this.arCalc = this.Calc_old;
            this.Calc_old = {};
        }
    }

    private calcIndent(): string {
        let res = "";
        for (let i = 0; i < this._indentCalc; i++) {
            res += "  ";
        }
        return res;
    }

    /**
     * Calcul de la dérivée surface hydraulique en cas de débordement
     * @return La dérivée de la surface hydraulique en cas de débordement
     */
    private Calc_dS_Debordement(): Result {
        return this.CalcGeo("B");
    }

    /**
     * Calcul du rayon hydraulique.
     * @return Le rayon hydraulique
     */
    private Calc_R(): Result {
        const rP: Result = this.calcFromY("P");
        if (!rP.ok) {
            return rP;
        }

        if (rP.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        }

        const rS = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }

        return new Result(rS.vCalc / rP.vCalc);
    }

    /**
     * Calcul de dérivée du rayon hydraulique par rapport au tirant d'eau.
     * @return dR
     */
    private Calc_dR(): Result {
        const rP: Result = this.calcFromY("P");

        if (!rP.ok) {
            return rP;
        }

        // if (P !== 0)
        if (rP.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        }

        const rB: Result = this.calcFromY("B");
        if (!rB.ok) {
            return rB;
        }

        const rS: Result = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }

        const rDP: Result = this.calcFromY("dP");
        if (!rDP.ok) {
            return rDP;
        }

        const v = ((rB.vCalc * rP.vCalc - rS.vCalc * rDP.vCalc) / Math.pow(rP.vCalc, 2));
        return new Result(v);

        // return 0;
    }

    /**
     * Calcul de la perte de charge par la formule de Manning-Strickler.
     * @return La perte de charge
     */
    private Calc_J(): Result {
        const rR: Result = this.calcFromY("R");
        if (!rR.ok) {
            return rR;
        }

        if (rR.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_RAYON_NUL));
        }

        const rV: Result = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }

        const v = Math.pow(rV.vCalc / this.prms.Ks.v, 2) / Math.pow(rR.vCalc, 4 / 3);
        return new Result(v);
    }

    /**
     * Calcul du nombre de Froude.
     * @return Le nombre de Froude
     */
    private Calc_Fr(): Result {
        const rS: Result = this.calcFromY("S");

        if (!rS.ok) {
            return rS;
        }

        if (rS.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }

        const rB: Result = this.calcFromY("B");
        if (!rB.ok) {
            return rB;
        }

        const v = this.prms.Q.v / rS.vCalc * Math.sqrt(rB.vCalc / rS.vCalc / ParamsSection.G);
        return new Result(v);
    }

    /**
     * Calcul de dy/dx
     */
    private Calc_dYdX(Y: number): Result {
        // L'appel à calcFromY("J') avec Y en paramètre réinitialise toutes les données
        // dépendantes de la ligne d'eau

        const rJ: Result = this.calcFromY("J", Y);
        if (!rJ.ok) {
            return rJ;
        }

        const rFR: Result = this.calcFromY("Fr", Y);
        if (!rFR.ok) {
            return rFR;
        }

        const v = - (this.prms.If.v - rJ.vCalc / (1 - Math.pow(rFR.vCalc, 2)));
        return new Result(v);
    }

    /**
     * Calcul de la vitesse moyenne.
     * @return Vitesse moyenne
     */
    private Calc_V(): Result {
        const rS: Result = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }

        if (rS.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }

        const v = this.prms.Q.v / rS.vCalc;
        return new Result(v);
    }

    /**
     * Calcul de la charge spécifique.
     * @return Charge spécifique
     */
    private Calc_Hs(): Result {
        const rV: Result = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }

        const v = this.prms.Y.v + Math.pow(rV.vCalc, 2) / (2 * ParamsSection.G);
        return new Result(v);
    }

    /**
     * Calcul de la charge spécifique critique.
     * @return Charge spécifique critique
     */
    private Calc_Hsc(): Result {
        let res: Result;

        this.Swap(true); // On mémorise les données hydrauliques en cours

        // On calcule la charge avec la hauteur critique
        const rYC: Result = this.CalcGeo("Yc");
        if (!rYC.ok) {
            res = rYC;
        } else {
            res = this.calcFromY("Hs", rYC.vCalc);
        }

        // On restitue les données initiales
        this.Swap(false);

        // return Hsc;
        return res;
    }

    /**
     * Calcul du tirant d'eau critique.
     * @return tirant d'eau critique
     */
    protected Calc_Yc(init: number = this.prms.YB.v): Result {
        const maxIter = SessionSettings.maxIterations;
        const hautCritique = new cHautCritique(this, maxIter, this.newtonDbg);
        this._hautCritique = hautCritique.Newton(init);

        if (!this._hautCritique.ok) {
            const m = new Message(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCRITIQUE);
            this._hautCritique = new Result(m);
        }
        return this._hautCritique;
    }

    /**
     * Calcul du tirant d'eau normal.
     * @return tirant d'eau normal
     */
    private Calc_Yn(): Result {
        if (this.prms.If.v <= 0) {
            const m = new Message(MessageCode.ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF);
            // this.oLog.add(m);
            // return undefined;
            return new Result(m);
        }

        const rYC: Result = this.CalcGeo("Yc");
        if (!rYC.ok) {
            return rYC;
        }

        const maxIter = SessionSettings.maxIterations;
        const oHautNormale = new cHautNormale(this, maxIter, this._newtonDbg);
        // let res = oHautNormale.Newton(this.CalcGeo("Yc"));
        let rYN = oHautNormale.Newton(rYC.vCalc);

        // if (res === undefined || !oHautNormale.hasConverged()) {
        if (!rYN.ok) {
            const m = new Message(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE);
            // this.oLog.add(m);
            rYN = new Result(m);
        }

        return rYN;
    }

    /**
     * Calcul du tirant d'eau correspondant.
     * @return tirant d'eau correpondant
     */
    private Calc_Ycor(): Result {
        const rGeoYC: Result = this.CalcGeo("Yc");
        if (!rGeoYC.ok) {
            return rGeoYC;
        }

        let nInit;
        if (this.prms.Y.v < rGeoYC.vCalc) {
            nInit = rGeoYC.vCalc * 2;
        } else {
            nInit = rGeoYC.vCalc / 2;
        }

        const maxIter = SessionSettings.maxIterations;
        const oHautCorrespondante = new cHautCorrespondante(this, maxIter, this._newtonDbg);

        let rYF = oHautCorrespondante.Newton(nInit);

        if (!rYF.ok) {
            const m = new Message(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
            rYF = new Result(m);
        }

        return rYF;
    }

    /**
     * Calcul du tirant d'eau conjugué.
     * @return tirant d'eau conjugué
     */
    private Calc_Ycon(): Result {
        let res: Result;

        this.Swap(true);

        const maxIter = SessionSettings.maxIterations;
        const oHautConj = new cHautConjuguee(this, maxIter, this._newtonDbg);

        const rFR: Result = this.calcFromY("Fr");
        if (!rFR.ok) {
            res = rFR;
        } else {
            let Y0: Result;
            // Choisir une valeur initiale du bon côté de la courbe
            Y0 = this.calcFromY("Ycor");
            if (!Y0.ok) {
                res = Y0;
            } else {
                // let Ycon = oHautConj.Newton(Y0);
                // tslint:disable-next-line:variable-name
                const Ycon: Result = oHautConj.Newton(Y0.vCalc);

                // if (Ycon === undefined || !oHautConj.hasConverged()) {
                if (!Ycon.ok) {
                    const m = new Message(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCONJUG);
                    // this.oLog.add(m);
                    res = new Result(m);
                } else {
                    res = Ycon;
                }
            }
        }

        this.Swap(false);
        return res;
    }

    /**
     * Calcul de la contrainte de cisaillement.
     * @return contrainte de cisaillement
     */
    private Calc_Tau0(): Result {
        const rR: Result = this.calcFromY("R");
        if (!rR.ok) {
            return rR;
        }

        const rJ: Result = this.calcFromY("J");
        if (!rJ.ok) {
            return rJ;
        }

        return new Result(1000 * ParamsSection.G * rR.vCalc * rJ.vCalc);
    }

    /**
     * Calcul de l'impulsion hydraulique.
     * @return Impulsion hydraulique
     */
    private Calc_Imp(): Result {
        const rV: Result = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }

        const rSYG: Result = this.calcFromY("SYg");
        if (!rSYG.ok) {
            return rSYG;
        }

        const v = 1000 * (this.prms.Q.v * rV.vCalc + ParamsSection.G * rSYG.vCalc);
        return new Result(v);
    }

    // interface IProperties

    public hasProperty(key: string): boolean {
        return key === "nodeType";
    }
}
