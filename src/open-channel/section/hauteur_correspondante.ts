import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { acNewton } from "../../internal_modules";
import { acSection } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

/**
 * Calcul de la hauteur correspondante (charge égale)
 */
// tslint:disable-next-line:class-name
export class cHautCorrespondante extends acNewton {
    /**
     * Tirant d'eau connu
     */
    private Y: number;

    /**
     * 1/S^2 associé au tirant d'eau connu
     */
    private _rS2: Result;

    /**
     * Section contenant les données de la section avec la hauteur à calculer
     */
    // tslint:disable-next-line:variable-name
    private Sn: acSection;

    /**
     * Constante de gravité
     */
    private rQ2G: number;

    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn: acSection, maxIter: number, dbg: boolean = false) {
        super(Sn.prms, maxIter, dbg);
        this.Y = Sn.prms.Y.v;
        //                this.rS2 = Math.pow(Sn.CalcSection("S"), -2);
        this.Sn = Sn;
        // tslint:disable-next-line:no-unused-expression
        this.rS2;  // pour initialiser la valeur @WTF (utilise le getter)
        this.rQ2G = Math.pow(Sn.prms.Q.v, 2) / (2 * ParamsSection.G);
    }

    private get rS2(): Result {
        if (this._rS2 === undefined) {
            const rS = this.Sn.CalcSection("S");
            if (rS.ok) {
                const v = Math.pow(rS.vCalc, -2);
                this._rS2 = new Result(v);
            } else {
                this._rS2 = rS;
            }
        }
        return this._rS2;
    }

    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcFn(rX: number): Result {
        if (!this.rS2.ok) {
            return this.rS2;
        }

        const rS: Result = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }

        // Calcul de la fonction
        // return this.Y - rX + (this.rS2 - Math.pow(this.Sn.CalcSection("S", rX), -2)) * this.rQ2G;
        const v = this.Y - rX + (this.rS2.vCalc - Math.pow(rS.vCalc, -2)) * this.rQ2G;
        return new Result(v);
    }

    /**
     * Calcul analytique de la dérivée de la fonction dont on protected function cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcDer(rX: number): Result {
        // let S = this.Sn.CalcSection("S");
        const rS: Result = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }
        const S = rS.vCalc;

        // L'initialisation a été faite lors de l'appel à CalcFn
        // if (S !== 0)
        if (S === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }

        const rB: Result = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }

        // return -1 + 2 * this.rQ2G * this.Sn.CalcSection("B") / Math.pow(S, 3);
        const v = -1 + 2 * this.rQ2G * rB.vCalc / Math.pow(S, 3);
        return new Result(v);

        // return Infinity;
    }
}
