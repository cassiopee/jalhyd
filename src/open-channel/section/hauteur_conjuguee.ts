import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { acNewton } from "../../internal_modules";
import { acSection } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

/**
 * Calcul de la hauteur conjuguée (Impulsion égale)
 */
// tslint:disable-next-line:class-name
export class cHautConjuguee extends acNewton {
    /** Section contenant les données de la section avec la hauteur à calculer */
    // tslint:disable-next-line:variable-name
    private Sn: acSection;

    /** Carré du débit */
    private rQ2: number;

    /** Surface hydraulique associée au tirant d'eau connu */
    private rS: Result;

    /** SYg associée au tirant d'eau connu */
    private rSYg: Result;

    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    constructor(oSn: acSection, maxIter: number, dbg: boolean = false) {
        super(oSn.prms, maxIter, dbg);
        this.rQ2 = Math.pow(oSn.prms.Q.v, 2);
        this.Sn = oSn;
        this.rS = oSn.CalcSection("S");
        this.rSYg = oSn.CalcSection("SYg");
    }

    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcFn(rX: number) {
        if (!this.rS.ok) {
            return this.rS;
        }

        if (!this.rSYg.ok) {
            return this.rSYg;
        }

        const rS2: Result = this.Sn.CalcSection("S", rX);
        if (!rS2.ok) {
            return rS2;
        }

        // Réinitialisation des paramètres hydrauliques de this.Sn avec l'appel this.Sn.CalcSection("S',rX)
        // if (this.rS > 0 && this.Sn.CalcSection("S", rX) > 0) {
        if (this.rS.vCalc > 0 && rS2.vCalc > 0) {
            // let Fn = this.rQ2 * (1 / this.rS - 1 / this.Sn.CalcSection("S"));
            const rS3: Result = this.Sn.CalcSection("S");
            if (!rS3.ok) {
                return rS3;
            }

            // tslint:disable-next-line:variable-name
            const Fn = this.rQ2 * (1 / this.rS.vCalc - 1 / rS3.vCalc);

            const rSYg = this.Sn.CalcSection("SYg");
            if (!rSYg.ok) {
                return rSYg;
            }

            // return Fn + ParamsSection.G * (this.rSYg - this.Sn.CalcSection("SYg"));
            const v = Fn + ParamsSection.G * (this.rSYg.vCalc - rSYg.vCalc);
            return new Result(v);
        }

        return new Result(new Message(MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        // return -Infinity;
    }

    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcDer(rX: number): Result {
        if (!this.rS.ok) {
            return this.rS;
        }

        //                let S = this.Sn.CalcSection("S");
        const rS2: Result = this.Sn.CalcSection("S");
        if (!rS2.ok) {
            return rS2;
        }
        const S = rS2.vCalc;

        // L'initialisation a été faite lors de l'appel à CalcFn
        // if (this.rS > 0 && S > 0) {
        if (this.rS.vCalc > 0 && S > 0) {
            const rDS: Result = this.Sn.CalcSection("dS");
            if (!rDS.ok) {
                return rDS;
            }

            // let Der = this.rQ2 * this.Sn.CalcSection("dS") * Math.pow(S, -2);
            // tslint:disable-next-line:variable-name
            const Der = this.rQ2 * rDS.vCalc * Math.pow(S, -2);

            const rDYG: Result = this.Sn.CalcSection("dSYg", rX);
            if (!rDYG.ok) {
                return rDYG;
            }

            // return Der - ParamsSection.G * this.Sn.CalcSection("dSYg", rX);
            const v = Der - ParamsSection.G * rDYG.vCalc;
            return new Result(v);
        }

        return new Result(new Message(MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        // return -Infinity;
    }
}
