import { SectionType } from "../../internal_modules";
import { ParamCalculability } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { ParamsSectionTrapez } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Calculs de la section trapézoïdale
 */
// tslint:disable-next-line:class-name
export class cSnTrapez extends acSection {

    get prms(): ParamsSectionTrapez {
        return this._prms as ParamsSectionTrapez;
    }
    constructor(prms: ParamsSectionTrapez, dbg: boolean = false) {
        super(prms, dbg);
        this.nodeType = SectionType.SectionTrapeze;
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.LargeurFond.calculability = ParamCalculability.DICHO;
        this.prms.Fruit.calculability = ParamCalculability.DICHO;
    }

    protected Calc_B(): Result {
        let v;
        if (this.isDebordement()) {
            v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.YB.v;
            return new Result(v);
        }

        v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.Y.v;
        return new Result(v);
    }

    /**
     * Calcul du périmètre mouillé
     * @return Périmètre mouillé (m)
     */
    protected Calc_P(): Result {
        let v;
        if (this.isDebordement()) {
            // return this.CalcGeo("P") + super.Calc_P_Debordement(this.prms.Y.v - this.prms.YB.v);

            const rGeoP: Result = this.CalcGeo("P");
            if (!rGeoP.ok) {
                return rGeoP;
            }

            const rPDeb: Result = super.Calc_P_Debordement(this.prms.Y.v - this.prms.YB.v);
            if (!rPDeb.ok) {
                return rPDeb;
            }

            v = rGeoP.vCalc + rPDeb.vCalc;
            return new Result(v);
        }

        v = this.prms.LargeurFond.v + 2 * Math.sqrt(1 + Math.pow(this.prms.Fruit.v, 2)) * this.prms.Y.v;
        return new Result(v);
    }

    /**
     * Calcul de la surface mouillée
     * @return Surface mouillée (m2)
     */
    protected Calc_S(): Result {
        let v;
        if (this.isDebordement()) {
            // return this.CalcGeo("S") + super.Calc_S_Debordement(this.prms.Y.v - this.prms.YB.v);
            const rGeoS: Result = this.CalcGeo("S");
            if (!rGeoS.ok) {
                return rGeoS;
            }

            const rSDeb: Result = super.Calc_S_Debordement(this.prms.Y.v - this.prms.YB.v);
            if (!rSDeb.ok) {
                return rSDeb;
            }

            v = rGeoS.vCalc + rSDeb.vCalc;
            return new Result(v);
        }

        v = this.prms.Y.v * (this.prms.LargeurFond.v + this.prms.Fruit.v * this.prms.Y.v);
        return new Result(v);
    }

    /**
     * Calcul de dérivée de la surface hydraulique par rapport au tirant d'eau.
     * @return dS
     */
    protected Calc_dS(): Result {
        if (this.isDebordement()) {
            return super.Calc_dS();
        }

        const v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.Y.v;
        return new Result(v);
    }

    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    protected Calc_dP(): Result {
        if (this.isDebordement()) {
            return super.Calc_dP_Debordement();
        }

        const v = 2 * Math.sqrt(1 + this.prms.Fruit.v * this.prms.Fruit.v);
        return new Result(v);
    }

    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    protected Calc_dB(): Result {
        if (this.isDebordement()) {
            return super.Calc_dB_Debordement();
        }

        const v = 2 * this.prms.LargeurFond.v * this.prms.Fruit.v;
        return new Result(v);
    }

    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_SYg(): Result {
        const v = (this.prms.LargeurFond.v / 2 + this.prms.Fruit.v * this.prms.Y.v / 3)
            * Math.pow(this.prms.Y.v, 2);
        return new Result(v);
    }

    /**
     * Calcul de la dérivée de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_dSYg(): Result {
        // tslint:disable-next-line:variable-name
        let SYg = this.prms.Fruit.v / 3 * Math.pow(this.prms.Y.v, 2);
        SYg += (this.prms.LargeurFond.v / 2 + this.prms.Fruit.v * this.prms.Y.v / 3) * 2 * this.prms.Y.v;
        return new Result(SYg);
    }

    private isDebordement(): boolean {
        return this.prms.Y.v > this.prms.YB.v;
    }

    /**
     * Critical depth calculation.
     * Calculation is performed by Newton algorithm initialised by approximation provided by
     * Vatankhah, A.R., 2013. Explicit solutions for critical and normal depths in trapezoidal
     * and parabolic open channels.
     * Ain Shams Engineering Journal 4, 17–23. https://doi.org/10.1016/j.asej.2012.05.002

     * @return Result object with critical depth
     */
    protected Calc_Yc(): Result {
        const z = this.prms.Fruit.v;
        const Q = this.prms.Q.v;
        const g = ParamsSectionTrapez.G;
        const B = this.prms.LargeurFond.v;
        const Qstar = 4 * Math.pow(Math.pow(z, 3) * Math.pow(Q, 2) / g / Math.pow(B, 5), 1 / 3);
        const Yc_star = Math.pow(1 + 1.161 * Qstar * (1 + 2 / 3 * Math.pow(Qstar, 1.041), 0.374), 0.144);
        const nu = - 0.5 + 0.5 *
            Math.pow((5 * Math.pow(Yc_star, 6) + 1) / (6 * Math.pow(Yc_star, 5) - Qstar), 3);
        const Yc = nu * B / z;
        return super.Calc_Yc(Yc);
    }
}
