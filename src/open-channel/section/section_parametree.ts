import { CalculatorType } from "../../internal_modules";
import { ParamCalculability, ParamDefinition, ParamFamily } from "../../internal_modules";
import { ParamDomain, ParamDomainValue } from "../../internal_modules";
import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { SectionNub } from "../../internal_modules";
import { SectionParams } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Nub sur les sections paramétrées
 */
export class SectionParametree extends SectionNub {

    constructor(s: acSection, dbg: boolean = false) {
        super(new SectionParams(), dbg);
        this.setCalculatorType(CalculatorType.SectionParametree);
        this.setSection(s);
        this.initSectionVars();
        // no calculated parameter
        this._defaultCalculatedParam = undefined;
    }

    public setSection(s: acSection) {
        super.setSection(s);
        this.setParametersCalculability(); // to override acSection's tuning
    }

    public getFirstAnalyticalParameter(): ParamDefinition {
        const res = super.getFirstAnalyticalParameter();
        if (res) {
            return res;
        }

        // tslint:disable-next-line:forin
        for (const k in this._sectionVars) {
            const p = this._sectionVars[k];
            if (p.isAnalytical) {
                return p;
            }
        }
        return undefined;
    }

    public Equation(sVarCalc: string): Result {
        let r: Result;
        switch (sVarCalc) {
            case "Hs":
            case "Hsc":
            case "B":
            case "P":
            case "S":
            case "R":
            case "V":
            case "Fr":
            case "Yc":
            case "Yn":
            case "Ycor":
            case "Ycon":
            case "J":
            case "Imp":
            case "Tau0":
            case "I-J":
                r = this.section.CalcSection(sVarCalc, this.getParameter("Y").v);
                break;

            default:
                throw new Error(`SectionParam.Equation() : calcul sur ${sVarCalc} non implémenté`);
        }

        return r;
    }

    /**
     * Aucune variable à calculer plus que les autres, on stocke toutes les
     * valeurs des variables à calcul dans les résultats complémentaires
     */
    public Calc(): Result {

        // tirant d'eau original (doit être fourni à acSection.Calc() sous peine d'être modifié
        // par les appels successifs car c'est en même temps un paramètre et une variable temporaire)
        const Y = this.getParameter("Y").v;

        if (!this.result) {
            this.initNewResultElement();
        }

        // charge spécifique
        this.addExtraResultFromVar("Hs", Y);

        // charge critique
        this.addExtraResultFromVar("Hsc", Y);

        // largeur au miroir
        this.addExtraResultFromVar("B", Y);

        // périmètre hydraulique
        this.addExtraResultFromVar("P", Y);

        // surface hydraulique
        this.addExtraResultFromVar("S", Y);

        // rayon hydraulique
        this.addExtraResultFromVar("R", Y);

        // vitesse moyenne
        this.addExtraResultFromVar("V", Y);

        // nombre de Froude
        this.addExtraResultFromVar("Fr", Y);

        // tirant d'eau critique
        this.addExtraResultFromVar("Yc", Y);

        // tirant d'eau normal
        this.addExtraResultFromVar("Yn", Y);

        // tirant d'eau correspondant
        this.addExtraResultFromVar("Ycor", Y);

        // tirant d'eau conjugué
        this.addExtraResultFromVar("Ycon", Y);

        // perte de charge
        this.addExtraResultFromVar("J", Y);

        // Variation linéaire de l'énergie spécifique
        this.addExtraResultFromVar("I-J", Y);

        // impulsion hydraulique
        this.addExtraResultFromVar("Imp", Y);

        // contrainte de cisaillement / force tractrice
        this.addExtraResultFromVar("Tau0", Y);

        // Est-ce que ça déborde ?
        if (this.section.prms.Y.v > this.section.prms.YB.v) {
            this.result.resultElement.log.add(new Message(MessageCode.WARNING_SECTION_OVERFLOW));
        }

        return this.result;
    }

    // there is no calculated param
    protected findCalculatedParameter(): any {
        return undefined;
    }

    // calculated param is always "Y"
    protected doCalc(computedSymbol?: any, rInit?: number) {
        return this.Calc();
    }

    // tslint:disable-next-line:no-empty
    protected setParametersCalculability(): void {
        if (this.section) {
            this.section.prms.Ks.calculability = ParamCalculability.FREE;
            this.section.prms.Q.calculability = ParamCalculability.FREE;
            this.section.prms.If.calculability = ParamCalculability.FREE;
            this.section.prms.YB.calculability = ParamCalculability.FREE;
            this.section.prms.Y.calculability = ParamCalculability.FREE;
            this.section.prms.LargeurBerge.calculability = ParamCalculability.FREE;
            // parameters depending on section type
            const D = this.section.getParameter("D");
            if (D) {
                D.calculability = ParamCalculability.FREE;
            }
            const k = this.section.getParameter("k");
            if (k) {
                k.calculability = ParamCalculability.FREE;
            }
            const fruit = this.section.getParameter("Fruit");
            if (fruit) {
                fruit.calculability = ParamCalculability.FREE;
            }
            const largeurFond = this.section.getParameter("LargeurFond");
            if (largeurFond) {
                largeurFond.calculability = ParamCalculability.FREE;
            }
        }
    }

    // tslint:disable-next-line:no-empty
    protected adjustChildParameters(): void { }

    protected exposeResults() {
        this._resultsFamilies = {
            B: ParamFamily.WIDTHS,
            Yc: ParamFamily.HEIGHTS,
            Yn: ParamFamily.HEIGHTS,
            Ycor: ParamFamily.HEIGHTS,
            Ycon: ParamFamily.HEIGHTS
        };
    }

    private initSectionVars() {
        this.initSectionVar("Hs");
        this.initSectionVar("Hsc");
        this.initSectionVar("B");
        this.initSectionVar("P");
        this.initSectionVar("S");
        this.initSectionVar("R");
        this.initSectionVar("V");
        this.initSectionVar("Fr");
        this.initSectionVar("Yc");
        this.initSectionVar("Yn");
        this.initSectionVar("Ycor");
        this.initSectionVar("Ycon");
        this.initSectionVar("J");
        this.initSectionVar("Imp");
        this.initSectionVar("Tau0");
        this.initSectionVar("I-J");
    }

    private initSectionVar(symbol: string) {
        this._sectionVars[symbol] = this.createSectionVar(symbol);
    }

    private createSectionVar(symbol: string): ParamDefinition {
        let dom;
        let unit;
        switch (symbol) {
            case "Hs":
            case "Hsc":
            case "B":
            case "P":
            case "R":
            case "Yc":
            case "Yn":
            case "Ycor":
            case "Ycon":
            case "J":
            case "Imp":
                dom = new ParamDomain(ParamDomainValue.POS_NULL);
                unit = "m";
                break;
            case "Tau0": // force tractrice
                dom = new ParamDomain(ParamDomainValue.POS_NULL);
                unit = "Pa";
                break;
            case "V": // vitesse moyenne
                dom = new ParamDomain(ParamDomainValue.POS_NULL);
                unit = "m/s";
                break;
            case "S": // surface
                dom = new ParamDomain(ParamDomainValue.POS_NULL);
                unit = "m²";
                break;
            case "Fr": // Froude
                dom = new ParamDomain(ParamDomainValue.POS_NULL);
                break;

            case "I-J": // var. lin. énergie spécifique
                unit = "m/m";
                dom = new ParamDomain(ParamDomainValue.ANY);
                break;

            default:
                throw new Error(`SectionParametree.createSectionVar() : symbole ${symbol} non pris en charge`);
        }

        const pe = new SectionParams(this);
        const res = new ParamDefinition(pe, symbol, dom, unit);
        res.calculability = ParamCalculability.EQUATION;
        return res;
    }

    /**
     * Calculates varCalc from Y, and sets the result as an
     * extraResult of the given ResultElement
     */
    private addExtraResultFromVar(varCalc: string, Y: number) {
        const r: Result = this.section.CalcSection(varCalc, Y);
        if (r.ok) {
            this.result.resultElement.addExtraResult(varCalc, r.vCalc);
        } else {
            this.result.resultElement.log.addLog(r.log);
        }
    }
}
