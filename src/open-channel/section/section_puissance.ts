import { SectionType } from "../../internal_modules";
import { ParamCalculability } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { ParamsSectionPuiss } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * Calculs de la section parabolique ou "puissance"
 */
// tslint:disable-next-line:class-name
export class cSnPuiss extends acSection {
    protected nbDessinPoints = 50;

    constructor(prms: ParamsSectionPuiss, dbg: boolean = false) {
        super(prms, dbg);
        this.nodeType = SectionType.SectionPuissance;
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.k.calculability = ParamCalculability.DICHO;
    }

    get prms(): ParamsSectionPuiss {
        return this._prms as ParamsSectionPuiss;
    }

    /**
     * Calcul de Lambda (mais on garde la routine Alpha commune avec la section circulaire)
     * @return Lambda
     */
    protected Calc_Alpha(): Result {
        const v = this.prms.LargeurBerge.v / Math.pow(this.prms.YB.v, this.prms.k.v);
        return new Result(v);
    }

    /**
     * Calcul de la largeur au miroir.
     * @return B
     */
    protected Calc_B(): Result {
        if (this.prms.Y.v >= this.prms.YB.v) {
            return new Result(this.prms.LargeurBerge.v);
        }

        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, this.prms.k.v);
        return new Result(v);
    }

    /**
     * Calcul du périmètre mouillé.
     * @return B
     */
    protected Calc_P(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const n = 100; /// Le nombre de partie pour le calcul de l'intégrale
        // tslint:disable-next-line:variable-name
        const Lambda2 = Math.pow(rAlpha.vCalc, 2);
        let P = 0; /// Le périmètre à calculer
        // tslint:disable-next-line:variable-name
        let Previous = 0;
        for (let i = 1; i <= n; i++) {
            // tslint:disable-next-line:variable-name
            const Current = Math.pow(this.prms.Y.v * i / n, this.prms.k.v) / 2;
            P += Math.sqrt(Math.pow(n, -2) + Lambda2 * Math.pow(Current - Previous, 2));
            Previous = Current;
        }
        P *= 2;
        return new Result(P);
    }

    /**
     * Calcul de la surface mouillée.
     * @return S
     */
    protected Calc_S(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const k = this.prms.k.v;
        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, k + 1) / (k + 1);
        return new Result(v);
    }

    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    protected Calc_dP(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const k = this.prms.k.v;
        const v = 2 * Math.sqrt(1 + Math.pow(k * rAlpha.vCalc / 2, 2) * Math.pow(this.prms.Y.v, 2 * (k - 1)));
        return new Result(v);
    }

    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    protected Calc_dB(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const k = this.prms.k.v;
        const v = rAlpha.vCalc * k * Math.pow(this.prms.Y.v, k - 1);
        return new Result(v);
    }

    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_SYg(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const k = this.prms.k.v;
        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, k + 2) / ((k + 1) * (k + 2));
        return new Result(v);
    }

    /**
     * Calcul de la dérivée distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    protected Calc_dSYg(): Result {
        const rAlpha: Result = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }

        const rDAlpha: Result = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }

        const k = this.prms.k.v;
        const Y = this.prms.Y.v;
        // tslint:disable-next-line:variable-name
        const SYg = rDAlpha.vCalc * Math.pow(Y, k + 2) + rAlpha.vCalc * Math.pow(Y, k + 1) * (k + 2);
        const v = SYg / ((k + 1) * (k + 2));
        return new Result(v);
    }
}
