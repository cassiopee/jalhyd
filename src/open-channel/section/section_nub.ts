import { Session } from "../../internal_modules";
import { Nub } from "../../internal_modules";
import { ParamDefinition } from "../../internal_modules";
import { ParamsEquation } from "../../internal_modules";
import { Props } from "../../internal_modules";
import { acSection } from "../../internal_modules";

/**
 * A Nub that contains an acSection (ex: RegimeUniforme, SectionParametree, Remous)
 */
export abstract class SectionNub extends Nub {

    /** moved from SectionParametree */
    protected _sectionVars: { [key: string]: ParamDefinition };

    /** Section is always the first child */
    public get section(): acSection {
        if (this._children && this._children.length > 0) {
            return this._children[0] as acSection;
        } else {
            return undefined;
        }
    }

    public constructor(prms: ParamsEquation, dbg: boolean = false) {
        super(prms, dbg);
        this._sectionVars = {};
    }

    public getParameter(name: string): ParamDefinition {
        if (typeof name !== "string") {
            // dirty hack because calculated param descriptor for section params is an object { uid: , symbol: }
            name = (name as any).symbol;
        }
        let res = super.getParameter(name);
        if (res === undefined) {
            // not found - maybe a section var ?
            res = this._sectionVars[name];

            if (!res) {
                throw new Error(
                    `in ${this.constructor.name}, SectionNub.getParameter() : nom de paramètre ${name} incorrect`
                );
            }
        }
        return res;
    }

    /**
     * Once session is loaded, run a second pass on all linked parameters to
     * reset their target if needed
     */
    public fixLinks(obj: any): { hasErrors: boolean } {
        // return value
        const ret = {
            hasErrors: false
        };
        // iterate over parameters
        super.fixLinks(obj);

        // iterate over children if any
        if (obj.children && Array.isArray(obj.children)) {
            for (const s of obj.children) {
                // get the Nub
                const subNub = Session.getInstance().findNubByUid(s.uid);
                const res = subNub.fixLinks(s);
                // forward errors
                if (res.hasErrors) {
                    ret.hasErrors = true;
                }
            }
        }
        return ret;
    }

    /**
     * Proxy to setSection() to ensure extra code is executed when
     * unserialising session (a SectionNub child can only be the
     * one and only Section)
     */
    public addChild(child: Nub, after?: number) {
        this.setSection(child as acSection);
    }

    /**
     * Sets / replaces the current section
     */
    public setSection(s: acSection) {
        if (s) {
            // prevent index out of bounds at first time
            if (this._children.length === 0) {
                this._children[0] = undefined;
            }
            this.replaceChild(0, s);
            this._props.removeProp("nodeType"); // because this property belongs to child section and is set as long as section is not defined
            /**
             * Y linkability managed in child classes through 'visible' flag
             * @see Bief,CourbeRemous
             */
        }
    }

    // interface IProperties

    public getPropValue(key: string): any {
        if (this.childHasProperty(key)) {
            return this.section.getPropValue(key);
        }
        return this._props.getPropValue(key);
    }

    public setPropValue(key: string, val: any, sender?: any): boolean {
        if (this.childHasProperty(key)) {
            return this.section.setPropValue(key, val, sender);
        }
        return this._props.setPropValue(key, val, sender);
    }
}
