import { ParamDefinition } from "../../internal_modules";
import { ParamDomain, ParamDomainValue } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

/**
 * Paramètres de la section parabolique ou "puissance"
 */
export class ParamsSectionPuiss extends ParamsSection {
    private _k: ParamDefinition; // Coefficient de forme compris entre 0 et 1

    constructor(rk: number, rY: number, rLargeurBerge: number, rKs: number, rQ: number,
        rIf: number, rYB: number, nullParams: boolean = false) {

        super(rY, rLargeurBerge, rKs, rQ, rIf, rYB, nullParams);
        this._k = new ParamDefinition(
            this, "k", new ParamDomain(ParamDomainValue.INTERVAL, 0, 1), undefined, rk, undefined, undefined, nullParams
        );

        this.addParamDefinition(this._k);
    }

    /**
     * Coefficient de forme compris entre 0 et 1
     */
    get k(): ParamDefinition {
        return this._k;
    }
}
