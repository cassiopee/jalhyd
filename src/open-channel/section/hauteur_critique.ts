import { Message, MessageCode } from "../../internal_modules";
import { Result } from "../../internal_modules";
import { acNewton } from "../../internal_modules";
import { acSection } from "../../internal_modules";
import { ParamsSection } from "../../internal_modules";

/**
 * Calcul de la hauteur critique
 */
// tslint:disable-next-line:class-name
export class cHautCritique extends acNewton {
    /**
     * Section sur laquuelle porte le calcul
     */
    // tslint:disable-next-line:variable-name
    private Sn: acSection;

    /**
     * Constructeur de la classe
     * @param Sn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn: acSection, maxIter: number, dbg: boolean = false) {
        super(Sn.prms, maxIter, dbg);
        this.Sn = Sn.clone();
    }

    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcFn(rX: number): Result {
        const rS: Result = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }

        // Calcul de la fonction
        // if (this.Sn.CalcSection("S", rX) !== 0)
        if (rS.vCalc === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }

        /* return (Math.pow(this.Sn.prms.Q.v, 2)
                * this.Sn.CalcSection("B", rX) / Math.pow(this.Sn.CalcSection("S", rX), 3)
                / ParamsSection.G - 1); */
        const rB: Result = this.Sn.CalcSection("B", rX);
        if (!rB.ok) {
            return rB;
        }

        const rS2: Result = this.Sn.CalcSection("S", rX);
        if (!rS2.ok) {
            return rS2;
        }

        const v = (Math.pow(this.Sn.prms.Q.v, 2) * rB.vCalc / Math.pow(rS2.vCalc, 3) / ParamsSection.G - 1);
        return new Result(v);

        // return Infinity;
    }

    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    public CalcDer(rX: number): Result {
        // let S = this.Sn.CalcSection("S");
        const rS: Result = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }
        const S = rS.vCalc;
        // if (S !== 0) {
        if (S === 0) {
            return new Result(new Message(MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }

        // let B = this.Sn.CalcSection("B");
        const rB: Result = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }
        const B = rB.vCalc;

        const rDB: Result = this.Sn.CalcSection("dB");
        if (!rDB.ok) {
            return rDB;
        }

        // L'initialisation à partir de rX a été faite lors de l'appel à CalcFn
        // let Der = (this.Sn.CalcSection("dB") * S - 3 * B * B);
        // tslint:disable-next-line:variable-name
        const Der = (rDB.vCalc * S - 3 * B * B);
        const v = Math.pow(this.Sn.prms.Q.v, 2) / ParamsSection.G * Der / Math.pow(S, 4);
        return new Result(v);
        // }

        // return Infinity;
    }
}
