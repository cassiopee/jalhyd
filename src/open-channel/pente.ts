import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { PenteParams } from "../internal_modules";

export class Pente extends Nub {

    constructor(prms: PenteParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Pente);
        this._defaultCalculatedParam = prms.I;
        this.resetDefaultCalculatedParam();
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PenteParams {
        return this._prms as PenteParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Z1":
                v = this.prms.Z2.v + (this.prms.I.v * this.prms.L.v);
                break;

            case "Z2":
                v = this.prms.Z1.v - (this.prms.I.v * this.prms.L.v);
                break;

            case "L":
                v = (this.prms.Z1.v - this.prms.Z2.v) / this.prms.I.v;
                break;

            case "I":
                v = (this.prms.Z1.v - this.prms.Z2.v) / this.prms.L.v;
                break;

            default:
                throw new Error("Pente.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.Z1.calculability = ParamCalculability.EQUATION;
        this.prms.Z2.calculability = ParamCalculability.EQUATION;
        this.prms.L.calculability = ParamCalculability.EQUATION;
        this.prms.I.calculability = ParamCalculability.EQUATION;
    }

}
