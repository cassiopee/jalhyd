import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PenteParams extends ParamsEquation {

    /** Cote amont Z1 */
    private _Z1: ParamDefinition;

    /** Cote aval Z2 */
    private _Z2: ParamDefinition;

    /** Longueur L */
    private _L: ParamDefinition;

    /** Pente I */
    private _I: ParamDefinition;

    constructor(rZ1: number, rZ2: number, rL: number, rI: number, nullParams: boolean = false) {
        super();
        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.LENGTHS, undefined, nullParams);
        this._I = new ParamDefinition(this, "I", ParamDomainValue.ANY, "m/m", rI, ParamFamily.SLOPES, undefined, nullParams);

        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._I);
    }

    get Z1() {
        return this._Z1;
    }

    get Z2() {
        return this._Z2;
    }

    get L() {
        return this._L;
    }

    get I() {
        return this._I;
    }
}
