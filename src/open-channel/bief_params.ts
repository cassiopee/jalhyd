import { CourbeRemousParams } from "../internal_modules";

export enum BiefRegime {
    Fluvial,
    Torrentiel
}

/**
 * paramètres pour les cotes de bief; pour l'instant rien de spécifique
 * (le régime est une propriété et non un paramètre)
 */
export class BiefParams extends CourbeRemousParams {

}
