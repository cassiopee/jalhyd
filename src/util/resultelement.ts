import { MessageCode, cLog } from "../internal_modules";
import { Message, MessageSeverity } from "../internal_modules";
import { Result } from "../internal_modules";

/**
 * Calculation result for one iteration.
 * May include value of the Nub's calculated parameter (if any), and/or other
 * values (previously known as extraResults)
 */
export class ResultElement {

    /** local messages, related to this calculation iteration */
    public log: cLog;

    /** pointer to parent result */
    private _parent: Result;

    /**
     * result values by symbol; generally contains at least the symbol of the calculated
     * parameter and its associated calculated value (aka. vCalc)
     */
    private _values: { [key: string]: number };

    /** latest key assigned to vCalc (this key might change) */
    private _vCalcSymbol: string;

    /**
     * @param v value of calculated parameter, or key-values map, or (usually error) Message
     */
    constructor(v?: number | Message | { [key: string]: number }) {
        this.log = new cLog();
        this._values = {};
        this._vCalcSymbol = ""; // default retrocompatible pseudo-symbol for cases like  `new ResultElement(42);`
        // detect first argument
        if (typeof v === "number") {
            this.vCalc = v;
        } else if (v instanceof Message) {
            this.addMessage(v);
        } else if (v !== undefined) {
            // assuming key-value map
            this._values = v;
            // assuming 1st value is vCalc
            if (Object.keys(v).length > 0) {
                this._vCalcSymbol = Object.keys(v)[0];
            }
        }
    }

    public get parent(): Result {
        return this._parent;
    }

    public set parent(r: Result) {
        this._parent = r;
        this.log.parent = this.parent;
    }

    /**
     * @returns value of the calculated parameter (main result); might be undefined,
     *          for ex. with SectionParametree
     */
    get vCalc(): number {
        return this._values[this._vCalcSymbol];
    }

    /**
     * sets value of the calculated parameter (main result)
     */
    set vCalc(r: number) {
        this.setVCalc(r);
    }

    public get vCalcSymbol(): string {
        return this._vCalcSymbol;
    }

    public setVCalc(r: number, keepSymbol: boolean = false) {
        if (!keepSymbol && this.parent && this.parent.symbol) {
            this._vCalcSymbol = this.parent.symbol;
        }
        this._values[this._vCalcSymbol] = r;
    }

    /**
     * Hack to update the key of vCalc, when Result.symbol is updated
     * after Equation() returned a ResultElement
     */
    public updateVCalcSymbol(s: string) {
        if (!this.hasValues) {
            throw new Error("updateVCalcSymbol() : values map is empty");
        }
        const tmp = this.vCalc;
        delete this._values[this._vCalcSymbol];
        this._vCalcSymbol = s;
        this.setVCalc(tmp, true);
    }

    /**
     * Sets vCalcSymbol back to the parent Nub's calculated parameter symbol
     */
    public resetVCalcSymbol() {
        this.updateVCalcSymbol(this.parent.sourceNub.calculatedParam.symbol);
    }

    /**
     * Adds given values map to the local values map; in case of key conflict,
     * precedence goes to the given value
     */
    public mergeValues(values: { [key: string]: number }) {
        this._values = { ...this._values, ...values };
    }

    /**
     * Returns an array of all the keys in the local values map,
     * where vCalcSymbol is always in first position (used by
     * GUI to iterate on displayable results)
     */
    public get keys(): string[] {
        const keys = Object.keys(this._values);
        //  make sure vCalc symbol is always first
        if (this._vCalcSymbol) { // sometimes empty (ex: SectionParametree)
            const index = keys.indexOf(this._vCalcSymbol);
            if (index > -1) {
                keys.splice(index, 1);
            }
            keys.unshift(this._vCalcSymbol);
        }
        return keys;
    }

    /** Returns the whole key-value map */
    public get values() {
        return this._values;
    }

    /**
     * @returns the result value associated to the given symbol, or undefined if the given
     *          symbol was not found in the local values map
     */
    public getValue(symbol: string) {
        return this._values[symbol];
    }

    public get firstValue(): number {
        return this._values[Object.keys(this._values)[0]];
    }

    public count(): number {
        return Object.keys(this._values).length;
    }

    /**
     * @returns true if local values map is not empty, and local log has no error-level
     *          message (log still might have non-error level messages)
     */
    public get ok(): boolean {
        return this.hasValues() && !this.hasErrorMessages();
    }

    /**
     * @returns true if local values map has at least one key-value pair
     */
    public hasValues(): boolean {
        return Object.keys(this._values).length > 0;
    }

    /**
     * Adds a key-value pair to the values map
     */
    public addExtraResult(name: string, value: any) {
        this._values[name] = value;
    }

    /**
     * Returns a copy of the local key-values map, without vCalc (only "extra" results)
     */
    public get extraResults() {
        const er = JSON.parse(JSON.stringify(this._values)); // clone
        // remove vCalc
        delete er[this._vCalcSymbol];
        return er;
    }

    /**
     * Removes all values that are not vCalc
     */
    public removeExtraResults() {
        for (const k in this._values) {
            if (k !== this._vCalcSymbol) {
                delete this._values[k];
            }
        }
    }

    /**
     * Removes all values
     */
    public removeValues() {
        this._values = {};
    }

    public toString(): string {
        if (this.vCalc !== undefined) {
            return String(this.vCalc);
        }
        return JSON.stringify(this);
    }

    // -------------- log related methods

    /** adds a message to the global log */
    public addMessage(m: Message) {
        this.log.add(m);
    }

    /** @returns true if current log has at least one message */
    public hasLog() {
        return this.log.messages.length > 0;
    }

    /**
     * @returns true if at least one of the log messages has an error level,
     *          i.e. if calculation has failed
     */
    public hasErrorMessages(): boolean {
        for (const m of this.log.messages) {
            if (m.getSeverity() === MessageSeverity.ERROR) {
                return true;
            }
        }
        return false;
    }

    /** @returns true if at least one of the log messages has a warning level */
    public hasWarningMessages(): boolean {
        for (const m of this.log.messages) {
            if (m.getSeverity() === MessageSeverity.WARNING) {
                return true;
            }
        }
        return false;
    }

    public hasMessage(code: MessageCode): boolean {
        for (const m of this.log.messages) {
            if (m.code === code) {
                return true;
            }
        }
        return false;
    }

    /**
     * compute error, warning, info count on all log messages
     */
    public logStats(stats?: any): any {
        return cLog.messagesStats(this.log.messages, stats);
    }
}
