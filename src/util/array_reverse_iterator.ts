/**
 * itérateur sur un tableau dans le sens inverse (depuis la fin)
 *
 * utilisation :
 *   let arr = [1,2,3];
 *   const it = new ArrayReverseIterator<Result>(arr);
 *   for (let r of it)
 *     console.log( r );   // 3 2 1
 */
export class ArrayReverseIterator<T> implements IterableIterator<T> {
    private _index: number;

    constructor(protected _arr: any[]) {
        this._index = this._arr === undefined ? 0 : this._arr.length - 1;
    }

    public next(): IteratorResult<T> {
        if (this._arr !== undefined && this._index >= 0) {
            return {
                done: false,
                value: this._arr[this._index--]
            };
        } else {
            return {
                done: true,
                value: null
            };
        }
    }

    public [Symbol.iterator](): IterableIterator<T> {
        return this;
    }
}
