/**
 * itérateur sur un map string<->any
 *
 * utilisation :
 * class MaClasseAIterer implements Iterable<Machin> {
 *     private _machinMap: { [key: string]: Machin } = {};
 *     [Symbol.iterator](): Iterator<Machin> {
 *        return new MapIterator(this._machinMap);
 *     }
 * }
 * const mc = new MaClasseAIterer();
 * for ( const m of mc ) {
 *   ...
 * }
 *
 * avec IterableIterator au lieu de Iterator, on peut faire :
 * const m = {"a":0, "b":1}
 * const it = new MapIterator<number>();
 * for (let e of it) {
 *   ...
 * }
 */
export class MapIterator<T> implements IterableIterator<T> {
    private _map: { [key: string]: T };

    private _keys: string[];

    private _lastIndex: number = -1; // index of last element returned; -1 if no such

    private _index: number = 0;

    private _currentKey: string;

    constructor(m: { [key: string]: T }) {
        this._map = m;
        if (this._map !== undefined) {
            this._keys = Object.keys(this._map);
        } else {
            this._keys = [];
        }
    }

    public next(): IteratorResult<T> {
        const i = this._index;
        if (this._index < this._keys.length) {
            this._currentKey = this._keys[this._index++];
            this._lastIndex = i;
            return {
                done: false,
                value: this._map[this._currentKey]
            };
        } else {
            this._currentKey = undefined;
            this._lastIndex = undefined;
            return {
                done: true,
                value: undefined
            };
        }
    }

    public get index() {
        return this._lastIndex;
    }

    public get key(): string {
        return this._currentKey;
    }

    // interface IterableIterator

    public [Symbol.iterator](): IterableIterator<T> {
        return this;
    }
}
