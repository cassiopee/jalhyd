// tslint:disable-next-line:interface-name
export interface Observer {
    update(sender: any, data: any): void;
}

export interface IObservable {
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o: Observer): void;

    /**
     * supprime un observateur de la liste
     */
    removeObserver(o: Observer): void;

    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data: any, sender?: any): void;
}

export class Observable implements IObservable {
    private _observers: Observer[];

    constructor() {
        this._observers = [];
    }

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        if (this._observers.indexOf(o) === -1) {
            this._observers.push(o);
        }
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observers = this._observers.filter((a) => a !== o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        if (sender === undefined) {
            sender = this;
        }
        for (const o of this._observers) {
            o.update(sender, data);
        }
    }

    public getObservers() {
        return this._observers;
    }
}

/**
 * @return true si l'objet passé implémente IObservable
 * @param o objet à tester
 */
export function isObservable(o: any): boolean {
    if (o === undefined) {
        return false;
    }

    const ob = o as IObservable;
    return typeof ob.addObserver === "function";
}

/**
 * cast d'un objet implémentant IObservable
 * @param o objet à convertir
 */
export function asObservable(o: any): IObservable {
    if (isObservable(o)) {
        return o as IObservable;
    }

    return undefined;
}
