import { Message, MessageCode, MessageSeverity } from "../internal_modules";
import { Result } from "../internal_modules";

// tslint:disable-next-line:class-name
export class cLog {

    /** pointer to parent result */
    public parent: Result;

    private _messages: Message[] = [];

    public constructor(parent?: Result) {
        this.parent = parent;
    }

    /**
     * efface tous les messages
     */
    public clear() {
        this._messages = [];
    }

    /**
     * insère un message en fin de liste
     */
    public add(m: Message) {
        // add pointer to the current log as message's parent, unless the message
        // already has a parent, and unless the current log has no pointer to a result
        if (m.parent === undefined) {
            m.parent = this;
        }
        this._messages.push(m);
    }

    /**
     * insère un log entier en fin de liste
     */
    public addLog(l: cLog) {
        for (const m of l.messages) {
            this.add(m);
        }
    }

    /**
     * insère un message en début de liste
     */
    public insert(m: Message) {
        // add pointer to the current log as message's parent, unless the message
        // already has a parent, and unless the current log has no pointer to a result
        if (m.parent === undefined && this.parent !== undefined) {
            m.parent = this;
        }
        this._messages.unshift(m);
    }

    /**
     * insère un log entier en début de liste
     */
    public insertLog(l: cLog) {
        for (let i = l.messages.length - 1; i >= 0; i--) {
            this.insert(l.messages[i]);
        }
    }

    public get messages() {
        return this._messages;
    }

    /**
     * @return a clone of "this" (clone error messages only)
     */
    public cloneErrors(): cLog {
        const res: cLog = new cLog();
        for (const m of this._messages) {
            if (m.getSeverity() === MessageSeverity.ERROR) {
                res.add(m);
            }
        }
        return res;
    }

    /**
     * @return a clone of "this" (do not clone messages, keep references to original messages)
     */
    public clone(): cLog {
        const res: cLog = new cLog();
        for (const m of this._messages) {
            res.add(m);
        }
        return res;
    }

    public toString(): string {
        return this._messages.join("\n");
    }

    /**
     * @param mc message code you're looking for
     * @returns true if log contains at least one occurrence of given message code
     */
    public contains(mc: MessageCode): boolean {
        for (const m of this.messages) {
            if (m.code === mc) {
                return true;
            }
        }
        return false;
    }

    public getMessage(mc: MessageCode): Message {
        for (const m of this.messages) {
            if (m.code === mc) {
                return m;
            }
        }
        return undefined;
    }

    /**
     * @returns true if all messages have the same code
     */
    public hasOnlyMessage(code: MessageCode): boolean {
        for (const m of this.messages) {
            if (m.code !== code) {
                return false;
            }
        }
        return true;
    }

    /**
     * @returns true if given message exactly matches one of the messages
     */
    private hasMessage(m: Message): boolean {
        for (const msg of this._messages) {
            if (m.equals(msg, true)) {
                return true;
            }
        }
        return false;
    }

    public get uniqueMessageCodes(): MessageCode[] {
        const res: MessageCode[] = [];
        for (const m of this.messages) {
            if (res.indexOf(m.code) === -1) {
                res.push(m.code);
            }
        }
        return res;
    }

    /**
     * compute error, warning, info count in a message list
     */
    public static messagesStats(messages: Message[], stats?: any): any {
        if (stats === undefined) {
            stats = { info: 0, warning: 0, error: 0 };
        }
        for (const m of messages) {
            const s = m.getSeverity();
            switch (s) {
                case MessageSeverity.INFO:
                    stats.info++;
                    break;
                case MessageSeverity.WARNING:
                    stats.warning++;
                    break;
                case MessageSeverity.ERROR:
                    stats.error++;
                    break;
            }
        }
        return stats;
    }
}
