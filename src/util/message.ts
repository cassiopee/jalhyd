import { cLog } from "../internal_modules";
import { Nub } from "../internal_modules";

export enum MessageCode {

    /** generic error stating that something triggered a fatal error leading to an undefined vCalc */
    ERROR_SOMETHING_FAILED_IN_CHILD,

    /** abstract showing number of error messages encountered in an iterative calculation, if number is 1 (otherwise see _PLUR below) */
    WARNING_ERRORS_ABSTRACT,

    /** abstract showing number of error messages encountered in an iterative calculation */
    WARNING_ERRORS_ABSTRACT_PLUR,

    /** short message for info count "xx info(s)" */
    WARNING_INFO_COUNT_SHORT,

    /** short message for warning count "xx warning(s)" */
    WARNING_WARNING_COUNT_SHORT,

    /** short message for error count "xx error(s)" */
    WARNING_ERROR_COUNT_SHORT,

    /** calculation of Z1 in Fluvial regime has failed (upstream abscissa not present in results) */
    ERROR_BIEF_Z1_CALC_FAILED,

    /** calculation of Z1 in Torrential regime has failed (downstream abscissa not present in results) */
    ERROR_BIEF_Z2_CALC_FAILED,

    /**
     * La dichotomie n'a pas trouvé de solution dans sa recherche d'intervalle:
     * La valeur cible est trop élevée
     */
    ERROR_DICHO_TARGET_TOO_HIGH,

    /**
     * La dichotomie n'a pas trouvé de solution dans sa recherche d'intervalle:
     * La valeur cible est trop basse
     */
    ERROR_DICHO_TARGET_TOO_LOW,

    /**
     * la dichotomie n'a pas pu trouver automatiquement d'intervalle de départ
     * car la valeur cible de la fonction n'existe pas pour des valeurs de la
     * variable dans son domaine de définition, cad il n'existe pas de solution
     */
    ERROR_DICHO_INIT_DOMAIN,

    /**
     * la dichotomie n'a pas pu converger
     */
    ERROR_DICHO_CONVERGE,

    /**
     * le pas pour la recherche de l'intervalle de départ est =0
     */
    ERROR_DICHO_NULL_STEP,

    /**
     * l'augmentation du pas pour la recherche de l'intervalle de départ est incorrecte (=0)
     */
    ERROR_DICHO_INVALID_STEP_GROWTH,

    /**
     * impossible de déterminer le sens de variation de la fonction
     */
    ERROR_DICHO_FUNCTION_VARIATION,

    /**
     * impossible de résoudre l'équation en raison d'une division par zéro
     */
    ERROR_DIVISION_BY_ZERO,

    /**
     * la cote amont Z1 est plus basse que la cote aval Z2
     */
    ERROR_ELEVATION_ZI_LOWER_THAN_Z2,

    /**
     * Something failed when calculating upstream Nubs
     */
    ERROR_IN_CALC_CHAIN,

    /** Jet submergé, pente trop faible: pas de solution pour calculer l'abscisse de l'impact */
    ERROR_JET_SUBMERGED_NO_SOLUTION,

    /** At least one of the %variables% variables must be defined */
    ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED,

    /** PAR - simulation : le nombre de ralentisseurs fourni ne correspond pas aux dimensions de la passe %stdNb% */
    ERROR_PAR_NB_INCONSISTENT,

    /**
     * Passe à ralentisseurs: la valeur donnée de P est plus de 10% plus petite / plus de 5%
     * plus grande que la valeur standard %stdP%
     */
    ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT,

    /** La valeur %val% de Q sort de l'intervalle de validité [ %min%, %max% ] donné par les abaques de Q* */
    ERROR_PAR_QSTAR_OUT_OF_RANGE,

    /** La valeur %val% de ha sort de l'intervalle de validité [ %min%, %max% ] donné par les abaques */
    ERROR_PAR_HA_OUT_OF_RANGE,

    /** Les valeurs de %var_ZR% et %var_ZD% ne correspondent pas : soit %var_ZR% devrait valoir %expectedZR%, soit %var_ZD% devrait valoir %expectedZD% */
    ERROR_PAR_ZR_ZD_MISMATCH,

    /**
     * Something failed in certain steps (but not all), when calculating upstream Nubs with varying parameter
     */
    WARNING_ERROR_IN_CALC_CHAIN_STEPS,

    /**
     * les bornes de l'intervalle d'un ParamDomain sont incorrectes
     */
    ERROR_PARAMDOMAIN_INTERVAL_BOUNDS,

    /**
     * la valeur du ParamDomain est incorrecte
     */
    ERROR_PARAMDOMAIN_INVALID,

    /**
     * la calculabilité d'un ParamDefinition est non définie
     */
    ERROR_PARAMDEF_CALC_UNDEFINED,

    /**
     * la valeur d'un ParamDefinition est non définie
     */
    ERROR_PARAMDEF_VALUE_UNDEFINED,

    /**
     * la valeur de la cible d'un ParamDefinition est non définie
     */
    ERROR_PARAMDEF_LINKED_VALUE_UNDEFINED,

    /**
     * la valeur d'un ParamDefinition ne peut pas être changée
     */
    ERROR_PARAMDEF_VALUE_FIXED,

    /**
     * la valeur d'un ParamDefinition doit être entière
     */
    ERROR_PARAMDEF_VALUE_INTEGER,

    /**
     * la valeur d'un ParamDefinition doit être > 0
     */
    ERROR_PARAMDEF_VALUE_POS,

    /**
     * la valeur d'un ParamDefinition doit être >= 0
     */
    ERROR_PARAMDEF_VALUE_POSNULL,

    /**
     * la valeur d'un ParamDefinition doit être = 0
     */
    ERROR_PARAMDEF_VALUE_NULL,

    /**
     * la valeur d'un ParamDefinition est en dehors de son intervalle autorisé
     */
    ERROR_PARAMDEF_VALUE_INTERVAL,

    /**
     * la valeur passée à une méthode de la classe Interval est undefined
     */
    ERROR_INTERVAL_UNDEF,

    /**
     * la valeur passée à une méthode de la classe Interval est hors de l'intervalle défini
     */
    ERROR_INTERVAL_OUTSIDE,

    /**
     * Un seul ouvrage régulé est autorisé sur la cloison aval
     */
    ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE,

    /**
     * PAB : La cote amont est plus basse que la cote aval
     */
    ERROR_PAB_Z1_LOWER_THAN_Z2,

    /**
     * PAB : La cote amont est plus basse que la cloison amont de la passe
     */
    ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL,

    /** PAB : Erreur de calcul de la cote amont de la cloison %n% */
    ERROR_PAB_CALC_Z1_CLOISON,

    /** PAB : Erreur de calcul de la cote amont de la cloison aval */
    ERROR_PAB_CALC_Z1_CLOISON_DW,

    /** Aucune ligne d'eau ne peut être calculée (aucun tirant d'eau à l'amont ni nà l'aval) */
    ERROR_REMOUS_NO_WATER_LINE,

    /** Solveur : interdiction de faire varier des paramètres dans le Nub calculé */
    ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED,

    /** Vérificateur : la passe à vérifier contient des erreurs */
    ERROR_VERIF_ERRORS_IN_PASS,

    /** Vérificateur : le critère testé %var_criterion% est indéfini */
    ERROR_VERIF_MISSING_CRITERION,

    /** Vérificateur : la passe variée à vérifier contient des erreurs à l'itération %i% */
    ERROR_VERIF_VARYING_ERRORS_IN_PASS,

    /** Vérificateur : les nubs Espece ont produit au moins un message de niveau erreur sur l'ensemble des résultats */
    ERROR_VERIF_KO,

    /** Vérificateur, passe variée : les nubs Espece ont produit au moins un message de niveau erreur pour chacune des itérations */
    ERROR_VERIF_VARYING_KO,

    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% a produit au moins un message de niveau erreur dans le résultat en cours */
    ERROR_VERIF_SPECIES_GROUP_KO,

    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% n'a pas produit de message de niveau erreur, mais en a produit de niveau avertissement, dans le résultat en cours */
    WARNING_VERIF_SPECIES_GROUP_OK_BUT,

    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% n'a produit aucun message de niveau erreur */
    INFO_VERIF_SPECIES_GROUP_OK,

    /** Vérificateur : le nub Espece personnalisé %uid% a produit au moins un message de niveau erreur dans le résultat en cours */
    ERROR_VERIF_SPECIES_NUB_KO,

    /** Vérificateur : le nub Espece personnalisé %uid% n'a pas produit de message de niveau erreur, mais en a produit de niveau avertissement, dans le résultat en cours */
    WARNING_VERIF_SPECIES_NUB_OK_BUT,

    /** Vérificateur : le nub Espece personnalisé %uid% n'a produit aucun message de niveau erreur */
    INFO_VERIF_SPECIES_NUB_OK,

    /** Vérificateur, passe à macrorugosités : vitesse max. %V% trop élevée (maximum: %maxV%) */
    ERROR_VERIF_MR_VMAX,

    /** Vérificateur, passe à macrorugosités : Blocs submergés */
    ERROR_VERIF_MR_SUBMERGED,

    /** Vérificateur, passe à macrorugosités : puissance dissipée %PV% trop élevée (maximum: %maxPV%) */
    ERROR_VERIF_MR_PVMAX,

    /** Vérificateur, passe à macrorugosités : tirant d'eau %Y% insuffisant (minimum: %minY%) */
    ERROR_VERIF_MR_YMIN,

    /** Vérificateur, passe à macrorugosités complexe : au moins un radier doit être franchissable */
    ERROR_VERIF_MRC_AT_LEAST_ONE_APRON,

    /** Vérificateur, passe à macrorugosités complexe : la largeur franchissable %width% est inférieure à la largeur d'un motif de blocs %patternWidth% */
    ERROR_VERIF_MRC_CROSSABLE_WIDTH,

    /** Vérificateur, passe à macrorugosités complexe : vitesse max. %V% trop élevée (maximum: %maxV%) dans le radier %N% */
    WARNING_VERIF_MRC_VMAX_APRON_N,

    /** Vérificateur, passe à macrorugosités complexe : Blocs submergés dans le radier %N% */
    WARNING_VERIF_MRC_SUBMERGED_APRON_N,

    /** Vérificateur, passe à macrorugosités complexe : tirant d'eau %Y% insuffisant (minimum: %minY%) dans le radier %N% */
    WARNING_VERIF_MRC_YMIN_APRON_N,

    /** Vérificateur, passe à macrorugosités complexe : la largeur franchissable est %width% */
    INFO_VERIF_MRC_CROSSABLE_WIDTH,

    /** Vérificateur : aucun jeu de contraintes pour le couple espèce / type de passe */
    ERROR_VERIF_NO_PRESET,

    /** Vérificateur, passe à bassins : jet plongeant non supporté */
    ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED,

    /** Vérificateur, passe à bassins : jet plongeant non supporté */
    WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED,

    /** Vérificateur, passe à bassins : chute %DH% trop importante pour les deux types de jets (maximum: %maxDHS% et %maxDHP%) */
    ERROR_VERIF_PAB_DHMAX,

    /** Vérificateur, passe à bassins : chute %DH% trop importante pour le type de jet %jetType% (maximum: %maxDH%) */
    ERROR_VERIF_PAB_DHMAX_JET,

    /** Vérificateur, passe à bassins : chute %DH% trop importante pour le type de jet %jetType% (maximum: %maxDH%), mais la cloison est franchissable grâce à l'autre type de jet */
    WARNING_VERIF_PAB_DHMAX_JET,

    /** Vérificateur, passe à bassins : largeur de l'échancrure ou de la fente %L% insuffisante (minimum: %minB%) */
    ERROR_VERIF_PAB_BMIN,

    /** Vérificateur, passe à bassins : largeur de l'échancrure ou de la fente %L% insuffisante (minimum: %minB%) */
    WARNING_VERIF_PAB_BMIN,

    /** Vérificateur, passe à bassins : surface de l'orifice %S% insuffisante (minimum: %minS%) */
    ERROR_VERIF_PAB_SMIN,

    /** Vérificateur, passe à bassins : surface de l'orifice %S% insuffisante (minimum: %minS%) */
    WARNING_VERIF_PAB_SMIN,

    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour les deux types de jet (minimum: %minLBS% et %minLBP%) */
    ERROR_VERIF_PAB_LMIN,

    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour le type de jet %jetType% (minimum: %minLB%) */
    ERROR_VERIF_PAB_LMIN_JET,

    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour le type de jet %jetType% (minimum: %minLB%) */
    WARNING_VERIF_PAB_LMIN_JET,

    /** Vérificateur, passe à bassins : charge sur l'échancrure %h1% insuffisante (minimum: %minH%) */
    WARNING_VERIF_PAB_HMIN,

    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour les deux types de jet (minimum: %minPBS% et %minPBP%) */
    ERROR_VERIF_PAB_YMOY,

    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour le type de jet %jetType% (minimum: %minPB%) */
    ERROR_VERIF_PAB_YMOY_JET,

    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour le type de jet %jetType% (minimum: %minPB%) */
    WARNING_VERIF_PAB_YMOY_JET,

    /** Vérificateur, passe à bassins, jet plongeant : profondeur de bassin %PB% inférieure à 2x la chute %DH% */
    ERROR_VERIF_PAB_YMOY_2_DH,

    /** Vérificateur, passe à bassins, jet plongeant : profondeur de bassin %PB% inférieure à 2x la chute %DH% */
    WARNING_VERIF_PAB_YMOY_2_DH,

    /** Vérificateur, passe à bassins : les poissons ne sont pas censés passer par les ouvrages de type "orifice" (cloison %NC%, ouvrage %NS%) */
    WARNING_VERIF_PAB_ORIFICE,

    /** Vérificateur, passe à bassins : la puissance dissipée %PV% est trop élevée (maximum : %maxPV%) sur la cloison %N% */
    ERROR_VERIF_PAB_PVMAX,

    /** Vérificateur, passe à bassins : aucun ouvrage de la cloison %N% n'est franchissable */
    ERROR_VERIF_PAB_WALL_NOT_CROSSABLE,

    /** Vérificateur, passe à bassins : aucun ouvrage de la cloison aval n'est franchissable */
    ERROR_VERIF_PAB_DW_NOT_CROSSABLE,

    /** Vérificateur, passe à ralentisseurs : présence d'une chute en bas de passe */
    ERROR_VERIF_PAR_DH,

    /** Vérificateur, passe à ralentisseurs : pente %S% trop forte (maximum : %maxS%) */
    ERROR_VERIF_PAR_SLOPE,

    /** Vérificateur, passe à ralentisseurs : tirant d'eau %h% insuffisant (minimum: %minY%) */
    ERROR_VERIF_PAR_YMIN,

    /** Vérificateur, passe à bassins : la puissance dissipée %PV% est très élevée (recommandée : %maxPV%) sur la cloison %N% */
    WARNING_VERIF_PAB_PVMAX,

    /** Vérificateur, passe à ralentisseurs : les groupes d'espèces 3a, 3b et 7b sont déconseillés */
    WARNING_VERIF_PAR_SPECIES_GROUP,

    /**
     * Dever : La cote du lit de la rivière ne peut pas être supérieure à la cote de l'eau
     */
    WARNING_DEVER_ZR_SUP_Z1,

    /**
     * courbes de remous : Arrêt du calcul : hauteur critique atteinte à l'abscisse x
     */
    WARNING_REMOUS_ARRET_CRITIQUE,

    /** Baqssin d'un PréBarrage : description (numéro d'ordre %order%) */
    INFO_PB_BASSIN_DESCRIPTION,

    /** Cloison d'un PréBarrage : description (bassin amont %ub%, bassin aval %db%) */
    INFO_PB_CLOISON_DESCRIPTION,

    /**
     * courbe de remous : Condition limite aval >= Hauteur critique : calcul de la partie fluviale à partir de l'aval
     */
    INFO_REMOUS_CALCUL_FLUVIAL,

    /**
     * courbe de remous : Condition limite amont <= Hauteur critique :
     * calcul de la partie torrentielle à partir de l'amont
     */
    INFO_REMOUS_CALCUL_TORRENTIEL,

    /**
     * courbe de remous : ressaut hydraulique détecté à l'amont/aval de l'abscisse x
     */
    INFO_REMOUS_RESSAUT_DEHORS,

    /**
     * courbe de remous : Largeur au niveau des berges
     */
    INFO_REMOUS_LARGEUR_BERGE,

    /**
     * courbe de remous : Tirant d'eau critique
     */
    INFO_REMOUS_H_CRITIQUE,

    /**
     * courbe de remous : Tirant d'eau normal
     */
    INFO_REMOUS_H_NORMALE,

    /**
     * courbe de remous : Ressaut hydraulique détecté entre les abscisses Xmin et Xmax m
     */
    INFO_REMOUS_RESSAUT_HYDRO,

    /** Verificateur : tout s'est bien passé (aucun autre message d'avertissement ni d'erreur) */
    INFO_VERIF_OK,

    /** Verificateur, passe variée : tout s'est bien passé à toutes les itérations (aucun message d'erreur sur aucune des itérations) */
    INFO_VERIF_VARYING_OK,

    /** Verificateur : tout s'est bien passé mais il y a des messages d'avertissement */
    WARNING_VERIF_OK_BUT,

    /** Verificateur, passe variée : au moins une itération "passe" et au moins une "ne passe pas" */
    WARNING_VERIF_VARYING_OK_BUT,

    /**
     * courbe de remous : La pente de la ligne d'eau est trop forte à l'abscisse x m
     */
    ERROR_REMOUS_PENTE_FORTE,

    /**
     * courbe de remous : Le pas de discrétisation doit être inférieur ou égal à la longueur du bief
     */
    ERROR_REMOUS_PAS_SUPERIEUR_BIEF,

    /**
     * courbe de remous : Condition limite aval < Hauteur critique : pas de calcul possible depuis l'aval
     */
    WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL, // pas changer assiette pour fromage

    /**
     * courbe de remous : Condition limite amont > Hauteur critique : pas de calcul possible depuis l'amont
     */
    WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT,

    /**
     * courbe de remous : pas de calcul possible, ni depuis l'amont ni depuis l'aval
     */
    ERROR_REMOUS_PAS_CALCUL,

    /** RegimeUniforme : impossible de calculer avec uen conduite en charge (section circulaire) */
    ERROR_RU_CIRC_LEVEL_TOO_HIGH,

    /**
     * section : Non convergence du calcul de la hauteur critique (Méthode de Newton)
     */
    ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCRITIQUE,

    /**
     * section : Non convergence du calcul de la hauteur normale (Méthode de Newton)
     */
    ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE,

    /** équivalent de l'erreur ci-dessus, pour la courbe de remous */
    WARNING_YN_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE,

    /**
     * section : Non convergence du calcul de la hauteur conjuguée (Méthode de Newton)
     */
    ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCONJUG,

    /**
     * section : Non convergence du calcul de la hauteur correspondante (Méthode de Newton)
     */
    ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR,

    /**
     * section : La pente est négative ou nulle, la hauteur normale est infinie
     */
    ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF,

    /** équivalent de l'erreur ci-dessus, pour la courbe de remous */
    WARNING_YN_SECTION_PENTE_NEG_NULLE_HNORMALE_INF,

    /**
     * section : calcul impossible à cause d'un périmètre nul
     */
    ERROR_SECTION_PERIMETRE_NUL,

    /**
     * section : calcul impossible à cause d'un rayon nul
     */
    ERROR_SECTION_RAYON_NUL,

    /**
     * section : calcul impossible à cause d'une surface nulle
     */
    ERROR_SECTION_SURFACE_NULLE,

    /**
     * newton : pas de convergence
     */
    ERROR_NEWTON_NON_CONVERGENCE,

    /**
     * newton : dérivée nulle
     */
    ERROR_NEWTON_DERIVEE_NULLE,

    /**
     * Le paramètre "Cote de radier" ne peut pas être calculé avec cette loi de débit
     */
    ERROR_STRUCTURE_ZDV_PAS_CALCULABLE,

    /**
     * Le débit passant par les autres ouvrages est trop important
     */
    ERROR_STRUCTURE_Q_TROP_ELEVE,

    /**
     * Les cotes amont aval sont égales et le débit n'est pas nul
     */
    ERROR_STRUCTURE_Z_EGAUX_Q_NON_NUL,

    /**
     * Structure : l'ennoiement %submergencePerc% est inférieur à %min%
     */
    ERROR_STRUCTURE_SUBMERGENCE_LOWER_THAN,

    /**
     * Il faut au moins un ouvrage dans une structure
     */
    ERROR_STRUCTURE_AU_MOINS_UNE,

    /** On essaye d'appliquer une puissance non entière à un nombre négatif */
    ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER,

    /** abstract showing number of warning messages encountered in an iterative calculation */
    WARNING_WARNINGS_ABSTRACT,

    /** Déversoirs: la cote de déversoir est en dessous de la cote de fond du lit */
    WARNING_DEVER_ZDV_INF_ZR,

    /** La cote de fond aval est plus élevée que la code de l'eau aval */
    WARNING_DOWNSTREAM_BOTTOM_HIGHER_THAN_WATER,

    /** La cote de fond amont est plus élevée que la code de l'eau amont */
    WARNING_UPSTREAM_BOTTOM_HIGHER_THAN_WATER,

    /**
     * La cote de l'eau aval est plus élevée que la cote du seuil (ennoiement possible)
     */
    WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION,

    /**
     * Lechapt-Calmon : la vitesse est en dehors de l'intervalle [0.4, 2]
     */
    WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2,

    /**
     * La cote de l'eau aval est plus élevée que la cote du centre de l'orifice (ennoiement possible)
     */
    WARNING_ORIFICE_FREE_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION,

    /**
     * Grille orientée, préconisation pour le guidage des poissons : α ≤ 45°
     */
    WARNING_GRILLE_ALPHA_GREATER_THAN_45,

    /**
     * Grille inclinée, préconisation pour le guidage des poissons : β ≤ 26°
     */
    WARNING_GRILLE_BETA_GREATER_THAN_26,

    /** L'obstruction totale (saisie) est inférieure à l'obstruction due aux barreaux seulement (calculée) */
    WARNING_GRILLE_O_LOWER_THAN_OB,

    /**
     * Préconisation pour éviter le placage des poissons sur le plan de grille (barrière physique),
     * ou leur passage prématuré au travers (barrière comportementale) : VN ≤ 0.5 m/s
     */
    WARNING_GRILLE_VN_GREATER_THAN_05,

    /** La cote de départ du jet est plus basse que la code de l'eau */
    WARNING_JET_START_SUBMERGED,

    /** La cote de départ du jet est plus basse que la code de fond */
    WARNING_JET_START_ELEVATION_UNDERGROUND,

    /** La cote de l'eau est plus basse ou égale à la cote de fond */
    WARNING_JET_WATER_ELEVATION_UNDERGROUND,

    /** La longueur du résultat varié est limitée par un résultat varié lié trop court */
    WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT,

    /** Macrorugo : la rampe ne contient pas au moins un motif de plot */
    WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH,

    /** Macrorugo : la largeur de la rampe devrait être un multiple de la demie-largeur d'un motif de plot */
    WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH,

    /** MacroRugo : concentration des blocs hors 8-20% */
    WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS,

    /** MacrorugoRemous : la PAM cible comporte des paramètres variés */
    ERROR_MACRORUGOREMOUS_VARIATED_TARGET_PAM,

    /**
     * courbe de remous sur PAM : chute à l'aval (cote de l'eau < hauteur critique)
     */
    WARNING_MACRORUGOREMOUS_CHUTE_AVAL,

    /** section : le tirant d'eau dépasse la hauteur de berge */
    WARNING_SECTION_OVERFLOW,

    /** section : le tirant d'eau dépasse la hauteur de berge entre les abscisses %xa% et %xb% */
    WARNING_SECTION_OVERFLOW_ABSC,

    /**
     * StructureKivi : La pelle du seuil doit mesurer au moins 0,1 m. Le coefficient béta est forcé à 0.
     */
    WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE,

    /**
     * StructureKivi : h/p ne doit pas être supérieur à 2,5. h/p est forcé à 2,5.
     */
    WARNING_STRUCTUREKIVI_HP_TROP_ELEVE,

    /** Cloisons: la pelle de l'ouvrage est en dessous du radier */
    WARNING_NEGATIVE_SILL,

    /**
     * La formule de l'échancrure n'est pas conseillée pour un ennoiement supérieur à 0.7
     */
    WARNING_NOTCH_SUBMERGENCE_GREATER_THAN_09,

    /**
     * La formule de la fente n'est pas conseillée pour un ennoiement inférieur à 0.7 et supérieur à 0.9
     */
    WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09,

    /** PAR : la largeur est plus grande que la valeur d'alerte %max% */
    WARNING_PAR_L,

    /** PAR : la largeur est en dehors des valeurs admissibles %min% et %max% (comme ERROR_PAR_L), mais pas d'erreur fatale */
    WARNING_ERROR_PAR_L,

    /** PAR : la pente est en dehors des valeurs standard (%min%, facultatif) et %max% */
    WARNING_PAR_S,

    /** PAR : la largeur est en dehors des valeurs admissibles %min% et %max% */
    ERROR_PAR_L,

    /** PAR : la pente est en dehors des valeurs admissibles %min% et %max% */
    ERROR_PAR_S,

    /** PAR superactive / chevrons : la hauteur est en dehors des valeurs admissibles %min% et %max% */
    ERROR_PAR_A,

    /** PAR superactive / chevrons : la hauteur dépasse la valeur d'alerte %max% */
    WARNING_PAR_A,

    /** PAR superactive / chevrons : le nombre de motifs a été arrondi à 0.5 près, à la valeur %val% */
    WARNING_PAR_N_ROUNDED_TO_05,

    /** PAR chevrons : le nombre de bandes dépasse deux fois le nombre de motifs %max% */
    ERROR_PAR_M_GREATER_THAN_2_N,

    /** PAR chevrons : le nombre de bandes a été arrondi à 1 près, à la valeur %val% */
    WARNING_PAR_M_ROUNDED_TO_1,

    /** PAR :  La cote de l'eau aval doit être supérieure ou égale à la hauteur d'eau dans la passe au niveau du dernier ralentisseur aval %ZDB% */
    WARNING_PAR_NOT_SUBMERGED,

    /**
     * La formule du seuil noyé n'est pas conseillé pour un ennoiement inférieur à 0.8
     */
    WARNING_WEIR_SUBMERGENCE_LOWER_THAN_08,

    /**
     * Vanne levante : ZDV > ZDV max
     */
    WARNING_VANLEV_ZDV_SUP_MAX,

    /**
     * Vanne levante : ZDV < ZDV min
     */
    WARNING_VANLEV_ZDV_INF_MIN,

    /**
     * La valeur du paramètre a été arrondie à l'entier
     */
    WARNING_VALUE_ROUNDED_TO_INTEGER,

    /** %name% n°%position% : */
    INFO_PARENT_PREFIX,

    /** %name%%position% */
    INFO_PARENT_PREFIX_SHORT,

    /** downwall : */
    INFO_PARENT_PREFIX_DOWNWALL,

    /**
     * Pré-barrage : non convergence du calcul
     */
    WARNING_PREBARRAGE_NON_CONVERGENCE,

    /** Pré-barrage : cote de l'eau aval supérieure à la cote de l'eau amont */
    ERROR_PREBARRAGE_Z2_SUP_Z1,

    /** Pré-barrage : cote de fond du bassin %n% supérieure à la cote de l'eau amont */
    WARNING_PREBARRAGE_BASSIN_ZF_SUP_Z1,

     /**
     * la cote amont Z1 est plus basse que la cote de fond amont de la rampe
     */
     WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_LOWER_THAN_ZF1,

    /**
     * rugofond : débit nul car la cote de l'eau amont est égale à la cpte de fond amont de la rampe
     */
    WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1,

    /**
     * rugofond : hauteur critique
     */
    WARNING_RUGOFOND_WATERLINE_TOO_HIGHT,

     /**
     * rugofond : hauteur critique
     */
     ERROR_RUGOFOND_WATERLINE_TOO_HIGHT,

      /**
     * rugofond : pente
     */
      WARNING_RUGOFOND_SLOPE_OUT_OF_RANGE

}

/**
 * niveau de criticité du message
 */
export enum MessageSeverity {
    ERROR, WARNING, INFO
}

/**
 * Résultat de calcul comprenant la valeur du résultat et des calculs annexes (flag, calculs intermédiaires...)
 */
export class Message {

    /** Variables intermédiaires, flags d'erreur */
    public extraVar: { [key: string]: any };

    /**
     * code du message
     */
    private _code: MessageCode;

    /** pointer to parent log */
    public parent: cLog;

    constructor(c: MessageCode, extraVar: { [key: string]: any } = {}) {
        this._code = c;
        this.extraVar = extraVar;
    }

    get code() { return this._code; }

    /**
     * retourne le niveau de criticité (erreur, warning, info) du message
     */
    public getSeverity(): MessageSeverity {
        const m: string = MessageCode[this._code];
        const prefix: string = m.split("_")[0];
        switch (prefix) {
            case "ERROR":
                return MessageSeverity.ERROR;
            case "WARNING":
                return MessageSeverity.WARNING;
            case "INFO":
                return MessageSeverity.INFO;
        }
        throw new Error("Message.getSeverity() : valeur de code '" + this._code + "' invalide");
    }

    public get sourceNub(): Nub {
        return this.parent?.parent?.sourceNub;
    }

    public toString(): string {
        const sourceNub = this.sourceNub;
        return MessageCode[this._code] + " " + JSON.stringify(this.extraVar) + (sourceNub === undefined ? "" : " parentnub " + sourceNub.constructor.name);
    }

    public equals(m: Message, includeSourceNub: boolean): boolean {
        if (this._code !== m._code)
            return false;

        const keys1 = Object.keys(this.extraVar);
        const keys2 = Object.keys(m.extraVar);
        if (keys1.length !== keys2.length)
            return false;

        if (keys1.length === 0) {
            return true;
        }

        for (const k of keys1) {
            if (!keys2.includes(k)) {
                return false;
            }
            if (this.extraVar[k] !== m.extraVar[k]) {
                return false;
            }
        }

        // to be sure all keys are shared, do the same thing the other way around
        for (const k of keys2) {
            if (!keys1.includes(k)) {
                return false;
            }
            if (this.extraVar[k] !== m.extraVar[k]) {
                return false;
            }
        }

        if (includeSourceNub) {
            if (this.sourceNub !== undefined && (this.sourceNub.uid !== m.sourceNub?.uid)) {
                return false;
            }
        }

        return true;
    }
}
