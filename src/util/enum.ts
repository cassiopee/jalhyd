/**
 * classe d'itérateurs pour les enums
 *
 * utilisation :
 *
 *  for (const v of EnumEx.getValues(MonTypeEnum)) {
 *     console.log(v);
 *  }
 *
 *  for (const n of EnumEx.getNames(MonTypeEnum)) {
 *     console.log(n);
 *  }
 *
 *  for (const c of EnumEx.getNamesAndValues(MonTypeEnum)) {
 *     console.log(c.name);
 *     console.log(c.value);
 *  }
 */
// tslint:disable-next-line:max-line-length

import { Session } from "../internal_modules";

// see https://stackoverflow.com/questions/21293063/how-to-programmatically-enumerate-an-enum-type-in-typescript-0-9-5#21294925
export class EnumEx {
    /**
     * retourne les noms et les valeurs d'un enum
     */
    public static getNamesAndValues<T extends number>(e: any) {
        return EnumEx.getNames(e).map((n) => ({ name: n, value: e[n] as T }));
    }

    /**
     * retourne les noms d'un enum
     */
    public static getNames(e: any) {
        return EnumEx.getObjValues(e).filter((v) => typeof v === "string") as string[];
    }

    /**
     * retourne les valeurs d'un enum
     */
    public static getValues<T extends number>(e: any) {
        return EnumEx.getObjValues(e).filter((v) => typeof v === "number") as T[];
    }

    private static getObjValues(e: any): (number | string)[] {
        return Object.keys(e).map((k) => e[k]);
    }
}
