import { Message, MessageCode } from "../internal_modules";
import { Debug } from "../internal_modules";

/**
 * Couple de valeurs ordonnées
 */
export class Interval extends Debug {

    constructor(public val1: number, public val2: number, dbg: boolean = false) {
        super(dbg);
    }

    public setValues(v1: number, v2: number) {
        this.val1 = v1;
        this.val2 = v2;
    }

    /** "constructeur" par copie */
    public setInterval(i: Interval) {
        this.setValues(i.val1, i.val2);
    }

    get min() {
        return Math.min(this.val1, this.val2);
    }

    get max() {
        return Math.max(this.val1, this.val2);
    }

    get length(): number {
        return this.max - this.min;
    }

    public intervalHasValue(v: number) {
        return this.min <= v && v <= this.max;
    }

    public intersect(i: Interval): Interval {
        const min = Math.max(this.min, i.min);
        const max = Math.min(this.max, i.max);

        let intersection = null;
        if (min <= max) {
            intersection = new Interval(min, max);
        } // else no intersection

        return intersection;
    }

    public checkValue(v: number) {
        if (v === undefined) {
            const e = new Message(MessageCode.ERROR_INTERVAL_UNDEF);
            throw e;
        }
        if (!this.intervalHasValue(v)) {
            const e = new Message(MessageCode.ERROR_INTERVAL_OUTSIDE);
            e.extraVar.value = v;
            e.extraVar.interval = this.toString();
            throw e;
        }
    }

    public toString(): string {
        return "[" + this.min + "," + this.max + "]";
    }

    public getVal(i: number): number {
        if (i < 1 || i > 2) { throw new Error("Interval getVal n'accepte que 1 ou 2  en argument"); }
        if (i === 1) {
            return this.val1;
        } else {
            return this.val2;
        }
    }

    public setVal(i: number, val: number) {
        if (i < 1 || i > 2) { throw new Error("Interval getVal n'accepte que 1 ou 2  en argument"); }
        if (i === 1) {
            this.val1 = val;
        } else {
            this.val2 = val;
        }
    }

    get minIndex(): number {
        if (this.val1 === this.min) {
            return 1;
        } else {
            return 2;
        }
    }

    get maxIndex(): number {
        if (this.val1 === this.max) {
            return 1;
        } else {
            return 2;
        }
    }
}
