import { Dichotomie } from "../internal_modules";
import { Interval } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

export class SearchInterval extends Interval {

    private _initialValue: number;

    private _initialStep: number;

    private _step: number;

    private _dicho: Dichotomie;

    private _targets: Interval;

    /** Last interval target difference */
    private _signDeltaTargets: number;

    /** Last value used for calculation without error */
    private _lastGoodValue: number;

    constructor(d: Dichotomie, rInit: number, s: number, dbg: boolean = false) {
        super(rInit, rInit + s, dbg);
        if (s === 0) {
            const e = new Message(MessageCode.ERROR_DICHO_NULL_STEP);
            throw e;
        }
        this._initialValue = rInit;
        this._initialStep = s;
        this._step = this._initialStep;
        this._dicho = d;
    }

    public debug(s: string) {
        if (this.DBG) {
            super.debug("SearchInterval: " + s);
        }
    }

    public reInit(rInit?: number) {
        if (rInit !== undefined) {
            this.val1 = rInit;
        } else {
            this.val1 = this._initialValue;
        }
        this._step = this._initialStep;
        this.val2 = this.val1 + this._step;
    }

    public growStep(k: number) {
        if (k === 0) {
            const e = new Message(MessageCode.ERROR_DICHO_INVALID_STEP_GROWTH);
            throw e;
        }
        this._step *= k;
    }

    get step() {
        return this._step;
    }

    get targets() {
        if (this._targets === undefined) {
            this._targets = new Interval(undefined, undefined);
        }
        return this._targets;
    }

    public next() {
        if (this._step > 0) {
            this.val1 = this.val2;
            this.val2 += this._step;
            this.targets.setValues(this.targets.val2, undefined);
        } else {
            this.val2 = this.val1;
            this.val1 += this._step;
            this.targets.setValues(undefined, this.targets.val1);
        }
        this.updateTargets(false); // false => this.reInit() if calculation fails
    }

    public checkDirection() {
        if (this._signDeltaTargets) { // !== undefined and !== 0
            if (Math.sign(this.targets.val1 - this.targets.val2) !== this._signDeltaTargets) {
                this.debug(this.toString() + " = [" + this.targets.val1 + "," + this.targets.val2 + "] sign changed!");
                // We have gone too far, let's start again the search from here
                if (this._step > 0) {
                    this.val2 = this.val1;
                } else {
                    this.val1 = this.val2;
                }
                this._step = -1 * Math.sign(this._step) * this._initialStep;
                this.next();
            }
        }
        this._signDeltaTargets = Math.sign(this.targets.val1 - this.targets.val2)
        this.debug(this.toString() + " = [" + this.targets.val1 + "," + this.targets.val2 + "]" + (this._signDeltaTargets > 0) ? ">0" : "<0");
        this.debug("_signDeltaTargets=" + this._signDeltaTargets);
    }

    public hasTargetValue(targ: number) {
        this.updateTargets();
        return this.targets.intervalHasValue(targ);
    }

    public toString(): string {
        return super.toString() + " step=" + this._step;
    }

    public updateTargets(bFatal: boolean = true) {
        try {
            let t1 = this.targets.val1;
            if (t1 === undefined || isNaN(t1)) {
                t1 = this._dicho.CalculX(this.val1);
                this._lastGoodValue = this.val1;
                this.debug(`updateTargets 1: f(${this.val1})=${t1}`)
            }

            let t2 = this.targets.val2;
            if (t2 === undefined || isNaN(t2)) {
                t2 = this._dicho.CalculX(this.val2);
                this._lastGoodValue = this.val2;
                this.debug(`updateTargets 2: f(${this.val2})=${t2}`)
            }
            this.targets.setValues(t1, t2);
        } catch (e) {
            if (bFatal) {
                throw e;
            } else {
                this.reInit(this._lastGoodValue);
            }
        }
    }

}
