import { JalhydObject, MessageCode } from "../internal_modules";
import { Nub } from "../internal_modules";
import { cLog } from "../internal_modules";
import { Message, MessageSeverity } from "../internal_modules";
import { ResultElement } from "../internal_modules";

/**
 * Result of a calculation.
 * May contain several ResultElement, if one or more parameters were varying.
 */
export class Result extends JalhydObject {

    /** calculated parameter (main result) symbol */
    private _symbol: string;

    /** Nub that produced this result */
    private _sourceNub: Nub;

    /** global messages, related to the whole module */
    private _globalLog: cLog;

    /** one result element for each iteration (when parameters are varying) */
    private _resultElements: ResultElement[];

    /**
     * @param v value of calculated parameter, or result element, or (usually error) Message
     * @param sourceNub Nub that generated this result
     * @param extraValues map of result values by symbol, to add to the first result element
     */
    constructor(v?: number | Message | ResultElement, sourceNub?: Nub, extraValues?: { [key: string]: number }) {
        super();
        this._globalLog = new cLog(this);
        this._resultElements = [];
        this._sourceNub = sourceNub;
        // try to store main result symbol
        if (this._sourceNub) {
            if (this._sourceNub.calculatedParam) {
                this.symbol = this._sourceNub.calculatedParam.symbol;
            }
        }
        // detect first argument
        if (typeof (v) === "number") {
            this.addResultElement(new ResultElement());
            this.resultElement.vCalc = v;
        } else if (v instanceof Message) {
            this.addResultElement(new ResultElement(v));
        } else if (v instanceof ResultElement) {
            this.resultElement = v;
        }
        if (extraValues !== undefined) { // for setter
            this.resultElement.mergeValues(extraValues);
        }
    }

    public get symbol(): string {
        return this._symbol;
    }

    public set symbol(s: string) {
        this._symbol = s;
        // hack to update "vCalc"
        if (this.resultElement) {
            this.resultElement.updateVCalcSymbol(s);
        }
    }

    public get sourceNub(): Nub {
        return this._sourceNub;
    }

    /** Use with caution, for Solveur for ex. */
    public updateSourceNub(sn: Nub) {
        this._sourceNub = sn;
    }

    public get globalLog() {
        return this._globalLog;
    }

    public get resultElements(): ResultElement[] {
        return this._resultElements;
    }

    /**
     * @return the most recent result element
     */
    public get resultElement(): ResultElement {
        /* if (this._resultElements.length === 0) {
            throw new Error("Result.resultElement() : there are no result elements");
        } */
        return this._resultElements[this._resultElements.length - 1];
    }

    /**
     * sets the most recent result element
     */
    public set resultElement(r: ResultElement) {
        if (this._resultElements.length === 0) {
            this._resultElements.push(r);
        } else {
            this._resultElements[this._resultElements.length - 1] = r;
        }
        r.parent = this;
    }

    public addResultElement(r: ResultElement) {
        this._resultElements.push(r);
        r.parent = this;
    }

    /**
     * @return true if there is at least 1 result element
     */
    public hasResultElements(): boolean {
        return this._resultElements.length > 0;
    }

    /**
     * @return true if there are at least 2 result elements
     */
    public hasMultipleValues(): boolean {
        return this.resultElements.length > 1;
    }

    /**
    * iterator on nth result element in this result and child results
    * @param index nth result element index
    */
    public nthResultElementIterator(index: number): Iterator<ResultElement> {
        const res = [];
        let re = this._resultElements[index];
        if (re !== undefined) {
            res.push(re);
        }
        const it: Iterator<Nub> = this._sourceNub.directChildNubIterator();
        let inub = it.next()
        while (!inub.done) {
            let re = inub.value.result._resultElements[index];
            if (re !== undefined) {
                res.push(re);
            }
            inub = it.next();
        }
        return res[Symbol.iterator]();
    }

    /**
     * Returns the list of calculated values for the parameter in CALC mode;
     * if no parameter is variated, the list will contain only 1 value
     */
    public getCalculatedValues(): number[] {
        return this.resultElements.map((re) => {
            return re.vCalc;
        });
    }

    /**
     * @returns proxy to .resultElement.ok (false if there is no result element)
     */
    public get ok(): boolean {
        return this._resultElements.length > 0 && this.resultElement.ok;
    }

    // -------------- log related methods

    /** adds a message to the global log */
    public addMessage(m: Message) {
        this._globalLog.add(m);
    }

    /** merges the given log with the global log */
    public addLog(l: cLog) {
        this._globalLog.addLog(l);
    }

    /**
     * @return true if global log has at least one message, whatever its level
     */
    public hasGlobalLog(): boolean {
        return this.globalLog.messages.length > 0;
    }

    /**
     * @return true if union of global log and result elements logs has at least one
     *         message, whatever its level
     */
    public hasLog(): boolean {
        if (this.hasGlobalLog()) {
            return true;
        } else {
            // result elements logs
            for (const r of this._resultElements) {
                if (r.hasLog()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @returns true if globalLog has at least one message with level ERROR
     */
    public hasGlobalError(): boolean {
        for (const m of this._globalLog.messages) {
            if (m.getSeverity() === MessageSeverity.ERROR) {
                return true;
            }
        }
        return false;
    }

    /**
     * @returns true if globalLog has at least one message with level WARNING
     * @WARNING this is NOT the famous hasGlobalWarming() Al-Gore-ithm !
     */
    public hasGlobalWarning(): boolean {
        for (const m of this._globalLog.messages) {
            if (m.getSeverity() === MessageSeverity.WARNING) {
                return true;
            }
        }
        return false;
    }

    /**
     * @returns true if at least one of the log messages (general log or result elements logs)
     *          has an error-level message, i.e. true if at least one calculation has failed
     */
    public hasErrorMessages(): boolean {
        // global log
        if (this.hasGlobalError()) {
            return true;
        }
        // result elements logs
        for (const r of this._resultElements) {
            if (r.hasErrorMessages()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @returns true if at least one of the log messages (general log or result elements logs)
     *          has a warning-level message
     */
    public hasWarningMessages(): boolean {
        // global log
        if (this.hasGlobalWarning()) {
            return true;
        }
        // result elements logs
        for (const r of this._resultElements) {
            if (r.hasWarningMessages()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @returns true if every result element log contains an error (i.e. no calculation has succeeded);
     *          does not inspect global log
     */
    public get hasOnlyErrors(): boolean {
        for (const r of this._resultElements) {
            if (!r.hasErrorMessages()) {
                return false;
            }
        }
        return true;
    }

    /**
     * determine if a message is present in result
     * @param code message code to find
     * @param recurs if true, search into nub children
     */
    public hasMessage(code: MessageCode, recurs: boolean = false): boolean {
        if (this._globalLog.contains(code)) {
            return true;
        }

        for (const r of this._resultElements) {
            if (r.log.contains(code)) {
                return true;
            }
        }

        if (recurs && this._sourceNub !== undefined) {
            for (const n of this._sourceNub.directChildNubIterator()) {
                if (n.result.hasMessage(code, true)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * determine if all messages have the same code
     * @param code message code to find
     */
    public hasOnlyMessage(code: MessageCode): boolean {
        if (!this._globalLog.hasOnlyMessage(code)) {
            return false;
        }

        if (!this.resultElement.log.hasOnlyMessage(code)) {
            return false;
        }

        return true;
    }

    public get uniqueMessageCodes(): MessageCode[] {
        let res: MessageCode[] = [];
        res = res.concat(this._globalLog.uniqueMessageCodes);
        res = res.concat(this.resultElement.log.uniqueMessageCodes);
        return res;
    }

    /**
     * compute log stats (# of error, warning, info) on all result element
     */
    public resultElementsLogStats(n: number, res?: any): any {
        let i = 0;
        for (const re of this._resultElements) {
            res = re.logStats(res)
        }
        return res;
    }

    // -------------- shortcuts to most recent result element

    /**
     * @return value of the calculated parameter, for the most recent result element
     */
    get vCalc(): number {
        return this.resultElement.vCalc;
    }

    /**
     * sets the value of the calculated parameter, for the most recent result element
     */
    set vCalc(r: number) {
        this.resultElement.vCalc = r;
    }

    /**
     * @returns log from the most recent result element
     * @TODO ambiguous / counter-intuitive : should be renamed
     */
    public get log() {
        return this.resultElement.log;
    }

    // set of all results, from most recent result element
    public get values() {
        return this.resultElement.values;
    }

    // set of all results except vCalc, from most recent result element
    public get extraResults() {
        return this.resultElement.extraResults;
    }

    // value of a result given its symbol, from most recent result element
    public getValue(name: string): number {
        return this.resultElement.getValue(name);
    }

    /**
     * Removes all values that are not vCalc, from all result elements
     */
    public removeExtraResults() {
        for (const re of this.resultElements) {
            re.removeExtraResults();
        }
    }
}
