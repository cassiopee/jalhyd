/**
 * utility functions around Mermaid package
 */
export class MermaidUtil {
    /**
     * check if two ids are equal in a Mermaid way
     * @param id1 first id to compare. Can be equal to the original,graph provided id or Mermaid transformed (ie. flowchart-id-xx)
     * @param id2 second  to compare (same remarks)
     */
    public static isMermaidEqualIds(id1: string, id2: string): boolean {
        if (id1 === id2) {
            return true;
        }
        const mid = "flowchart-" + id1 + "-"; // Mermaid generated element id
        if (id2.startsWith(mid)) {
            return true;
        }

        const mid2 = "flowchart-" + id2 + "-"; // so the same in reverse order
        if (mid2.startsWith(id1)) {
            return true;
        }

        return false;
    }

    /**
     * convert Mermaid formatted id ("flowchat-id-xx") to id
     */
    public static removeMermaidIdFormat(id: string): string {
        if (id.startsWith("flowchart-")) {
            const i1 = id.indexOf("-");
            const i2 = id.lastIndexOf("-");
            id = id.substring(i1 + 1, i2);
        }
        return id;
    }
}
