import { INumberIterator } from "../internal_modules";
import { ArrayReverseIterator } from "../internal_modules";

/**
 * itérateur sur les valeurs prises par un tableau (parcourues depuis la fin)
 */
export class NumberArrayReverseIterator extends ArrayReverseIterator<number> implements INumberIterator {
    private _count: number;

    /**
     * valeur courante
     */
    private _current: number;

    constructor(arr: number[]) {
        super(arr);
        this._count = 0;
    }

    public count() {
        return this._arr.length;
    }

    public get hasNext(): boolean {
        return this._count < super._arr.length;
    }

    public next(): IteratorResult<number> {
        this._count++;
        const res = super.next();
        if (!res.done) {
            this._current = res.value;
        }
        return res;
    }

    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    public nextValue(): IteratorResult<number> {
        return this.next();
    }

    public get currentValue(): number {
        return this._current;
    }

    // interface IterableIterator

    public [Symbol.iterator](): IterableIterator<number> {
        return this;
    }
}
