import { INumberIterator } from "../internal_modules";

/**
 * itérateur sur les valeurs prises par un tableau
 */
export class NumberArrayIterator implements INumberIterator {
    private _it: IterableIterator<number>;

    private _index: number;

    /**
     * valeur courante
     */
    private _current: number;

    constructor(private _arr: number[]) {
        this._it = this._arr[Symbol.iterator](); // crée un itérateur à partir d'un tableau
        this._index = 0;
    }

    public count() {
        return this._arr.length;
    }

    public get hasNext(): boolean {
        return this._index < this._arr.length;
    }

    public next(): IteratorResult<number> {
        this._index++;
        const res = this._it.next();
        if (!res.done) {
            this._current = res.value;
        }
        return res;
    }

    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    public nextValue(): IteratorResult<number> {
        return this.next();
    }

    public get currentValue(): number {
        return this._current;
    }

    // interface IterableIterator

    public [Symbol.iterator](): IterableIterator<number> {
        return this;
    }
}
