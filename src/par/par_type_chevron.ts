import { Result } from "../internal_modules";
import { ParTypeSC } from "../internal_modules";
import { ParParams } from "../internal_modules";
import { ParType } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

export class ParTypeChevron extends ParTypeSC {

    public constructor(prms: ParParams) {
        super(prms);
        this.type = ParType.CHEVRON;
    }

    // tslint:disable-next-line:variable-name
    public CalcZRFromZD(ZDv: number): number {
        return ZDv + (4 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }

    // tslint:disable-next-line:variable-name
    public CalcZDFromZR(ZRv: number): number {
        return ZRv - (4 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }

    public CalcP(): number {
        return 4 * this.prms.a.v;
    }

    public get minQstar(): number {
        return 0.7;
    }

    public get maxQstar(): number {
        return 8.1;
    }

    public addExtraResults(res: Result): void {
        super.addExtraResults(res);
        res.resultElement.values.B = this.CalcB();
    }

    public checkInput(): { fatal: boolean, messages: Message[] } {
        const status = super.checkInput();
        // M, round to 1
        if (this.prms.M.v % 1 > 0.001 && this.prms.M.v % 1 < 0.999) {
            const m = new Message(MessageCode.WARNING_PAR_M_ROUNDED_TO_1);
            this.prms.M.v = Math.round(this.prms.M.v);
            m.extraVar.val = this.prms.M.v;
            status.messages.push(m);
        }
        // M, max. 2*N
        if (this.prms.M.v > this.prms.N.v * 2) {
            status.fatal = true;
            const m = new Message(MessageCode.ERROR_PAR_M_GREATER_THAN_2_N);
            m.extraVar.max = this.prms.N.v * 2;
            status.messages.push(m);
        }

        return status;
    }

    protected get coef() {
        return {
            "h": {
                "c0": [0, -4.97686, 1.30546],
                "c1": [0, 0.176261, 0.661656],
                "c2": [0, -0.0733832, -0.00839864]
            },
            "ha": {
                "c0": [0, 5.02138, 0.709434],
                "c1": [0, -2.47998, 1.25363],
                "c2": [0, 0.188324, -0.0427461]
            }
        };
    }

    protected CalcB(): number {
        return 6 * this.prms.a.v * this.prms.N.v + 0.5 * this.prms.a.v * this.prms.M.v;
    }
}
