import { ParTypeAbstract } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { isLowerThan, isGreaterThan } from "../internal_modules";

/**
 * Intermediate class for common stuff between SUPERACTIVE and CHEVRON pass types
 */
export abstract class ParTypeSC extends ParTypeAbstract {

    /**
     * Math.sqrt(2 * 9.81)
     */
    protected get RG(): number {
        return 4.42945;
    }

    public CalcH() {
        const qStar = this.CalcQStar();
        return this.prms.a.v * (this.c2h * Math.pow(qStar, 2) + this.c1h * qStar + this.c0h);
    }

    public CalcHa(qStar?: number) {
        if (qStar === undefined) {
            qStar = this.CalcQStar();
        }
        return this.prms.a.v * (this.c2ha * Math.pow(qStar, 2) + this.c1ha * qStar + this.c0ha);
    }

    public CalcQStar(): number {
        return (this.prms.Q.V / this.CalcB()) / (this.RG * Math.pow(this.prms.a.v, 1.5));
    }

    public CalcQFromQStar(qStar: number): number {
        return qStar * this.RG * Math.pow(this.prms.a.v, 1.5) * this.CalcB();
    }

    public CalcQFromHa(): number {
        const x = this.ha / this.prms.a.v;
        const y = this.doCalcQYFromHa(x);
        return this.CalcQFromQStar(y);
    }

    public CalcAw(): number {
        return this.CalcH() * this.CalcB();
    }

    /** calculates the number of baffles Nb based on the baffles spacing P */
    public CalcNb(): number {
        return this.CalcLs() / this.P;
    }

    public checkInput(): { fatal: boolean, messages: Message[] } {
        let fatal = false;
        const messages: Message[] = [];
        // S
        if (isLowerThan(this.prms.S.v, 0.1) || isGreaterThan(this.prms.S.v, 0.18)) {
            fatal = true;
            const m = new Message(MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.1;
            m.extraVar.max = 0.18;
            messages.push(m);
        } else if (isGreaterThan(this.prms.S.v, 0.16)) {
            const m = new Message(MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.1;
            m.extraVar.max = 0.16;
            messages.push(m);
        }
        // N
        if (this.prms.N.v % 0.5 > 0.001 && this.prms.N.v % 0.5 < 0.499) {
            const m = new Message(MessageCode.WARNING_PAR_N_ROUNDED_TO_05);
            this.prms.N.v = Math.round(this.prms.N.v * 2) / 2;
            m.extraVar.val = this.prms.N.v;
            messages.push(m);
        }
        // a
        if (isLowerThan(this.prms.a.v, 0.05) || isGreaterThan(this.prms.a.v, 0.25)) {
            fatal = true;
            const m = new Message(MessageCode.ERROR_PAR_A);
            m.extraVar.min = 0.05;
            m.extraVar.max = 0.25;
            messages.push(m);
        } else if (isGreaterThan(this.prms.a.v, 0.2)) {
            const m = new Message(MessageCode.WARNING_PAR_A);
            m.extraVar.max = 0.2;
            messages.push(m);
        }

        return { fatal, messages };
    }

    protected abstract CalcB(): number;
}
