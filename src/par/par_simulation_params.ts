import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParParams } from "../internal_modules";

export class ParSimulationParams extends ParParams {

    /** Nombre de ralentisseurs [facultatif] */
    private _Nb: ParamDefinition;

    /** Cote de déversement à l'amont de la passe (m) */
    private _ZD1: ParamDefinition;

    /** Cote de radier pointe du ralentisseur amont [facultatif si ZD1 fourni] (m) */
    private _ZR1: ParamDefinition;

    /** Cote de radier base du ralentisseur aval [facultatif si ZD2 fourni] (m) */
    private _ZR2: ParamDefinition;

    /** Cote de déversement du ralentisseur aval [facultatif si ZR2 fourni] (m) */
    private _ZD2: ParamDefinition;

    constructor(rQ: number, rZ1: number, rZ2: number, rS: number, rP?: number, rNb?: number, rZR1?: number,
        rZD1?: number, rZR2?: number, rZD2?: number, rL?: number, ra?: number, rN?: number, rM?: number, nullParams: boolean = false) {
        super(rQ, rZ1, rZ2, undefined, rS, rP, rL, ra, rN, rM, nullParams);
        // géométrie de la passe
        this._Nb = new ParamDefinition(this, "Nb", ParamDomainValue.POS, "m", rNb, undefined, undefined, nullParams);
        this.addParamDefinition(this.Nb);
        this._ZR1 = new ParamDefinition(this, "ZR1", ParamDomainValue.ANY, "m", rZR1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZR1);
        this._ZD1 = new ParamDefinition(this, "ZD1", ParamDomainValue.ANY, "m", rZD1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZD1);
        this._ZR2 = new ParamDefinition(this, "ZR2", ParamDomainValue.ANY, "m", rZR2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZR2);
        this._ZD2 = new ParamDefinition(this, "ZD2", ParamDomainValue.ANY, "m", rZD2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZD2);
    }

    public get Nb() {
        return this._Nb;
    }

    public get ZR1() {
        return this._ZR1;
    }

    public get ZR2() {
        return this._ZR2;
    }

    public get ZD1() {
        return this._ZD1;
    }

    public get ZD2() {
        return this._ZD2;
    }
}
