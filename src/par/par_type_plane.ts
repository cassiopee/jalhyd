import { Result } from "../internal_modules";
import { ParTypePF } from "../internal_modules";
import { ParType } from "../internal_modules";
import { ParParams } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { isLowerThan, isGreaterThan } from "../internal_modules";

export class ParTypePlane extends ParTypePF {

    public constructor(prms: ParParams) {
        super(prms);
        this.type = ParType.PLANE;
    }

    public CalcAw(): number {
        const h = this.CalcH();
        return this.prms.L.v * (0.583 * h - 0.146 * this.prms.L.v);
    }

    // tslint:disable-next-line:variable-name
    public CalcZRFromZD(ZDv: number): number {
        return ZDv - (0.236 * this.prms.L.v * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v)));
    }

    // tslint:disable-next-line:variable-name
    public CalcZDFromZR(ZRv: number): number {
        return ZRv + (0.236 * this.prms.L.v * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v)));
    }

    public CalcZM(): number {
        // tslint:disable-next-line:variable-name
        const Hmin = this.CalcHmin();
        return this.CalcZR1() + Hmin * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v));
    }

    public CalcP(): number {
        return (2 / 3) * this.prms.L.V;
    }

    public addExtraResults(res: Result): void {
        super.addExtraResults(res);
        res.resultElement.values.B = 0.583 * this.prms.L.v;
        res.resultElement.values.C = 0.472 * this.prms.L.v;
        res.resultElement.values.D = 0.236 * this.prms.L.v;
        res.resultElement.values.Hmin = this.CalcHmin();
        res.resultElement.values.Hmax = 2.2 * this.prms.L.v;
    }

    public checkInput(): { fatal: boolean, messages: Message[] } {
        const status = super.checkInput();
        // S
        if (isLowerThan(this.prms.S.v, 0.06) || isGreaterThan(this.prms.S.v, 0.22)) {
            status.fatal = true;
            const m = new Message(MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.06;
            m.extraVar.max = 0.22;
            status.messages.push(m);
        } else if (isLowerThan(this.prms.S.v, 0.08) || isGreaterThan(this.prms.S.v, 0.2)) {
            const m = new Message(MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.2;
            status.messages.push(m);
        }

        return status;
    }

    protected get coef() {
        return {
            "h": {
                "c0": [16.7218, -6.09624, 0.834851],
                "c1": [-139.382, 47.2186, 0.0547598],
                "c2": [347.368, -130.698, 8.14521]
            },
            "ha": {
                "c0": [15.2115, -5.22606, 0.633654],
                "c1": [-184.043, 59.7073, -0.530737],
                "c2": [315.110, -115.164, 6.85371]
            }
        };
    }

    protected CalcHmin(): number {
        return 1.85 * this.prms.L.v;
    }
}
