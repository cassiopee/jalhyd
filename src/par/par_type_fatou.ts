import { Result } from "../internal_modules";
import { ParTypePF } from "../internal_modules";
import { ParType } from "../internal_modules";
import { ParParams } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { isLowerThan, isGreaterThan } from "../internal_modules";

export class ParTypeFatou extends ParTypePF {

    public constructor(prms: ParParams) {
        super(prms);
        this.type = ParType.FATOU;
    }

    public CalcAw(): number {
        return 0.6 * this.CalcH() * this.prms.L.v;
    }

    // tslint:disable-next-line:variable-name
    public CalcZRFromZD(ZDv: number): number {
        return ZDv
            - (Math.sqrt(2) * this.CalcA() * Math.sin(45 * Math.PI / 180 + Math.atan(this.prms.S.v)))
            + this.prms.L.V * 0.5 * this.prms.S.v;
    }

    // tslint:disable-next-line:variable-name
    public CalcZDFromZR(ZRv: number): number {
        return ZRv
            + (Math.sqrt(2) * this.CalcA() * Math.sin(45 * Math.PI / 180 + Math.atan(this.prms.S.v)))
            - this.prms.L.V * 0.5 * this.prms.S.v;
    }

    public CalcZM(): number {
        const H = 1.333 * this.prms.L.v;
        return this.ZD1 + H * Math.cos(Math.atan(this.prms.S.v));
    }

    public CalcP(): number {
        return 0.5 * this.prms.L.V;
    }

    public addExtraResults(res: Result): void {
        super.addExtraResults(res);
        res.resultElement.values.B = 0.6 * this.prms.L.V;
        res.resultElement.values.a = this.CalcA();
        res.resultElement.values.H = 1.333 * this.prms.L.V;
    }

    public checkInput(): { fatal: boolean, messages: Message[] } {
        const status = super.checkInput();
        // S
        if (isLowerThan(this.prms.S.v, 0.08) || isGreaterThan(this.prms.S.v, 0.22)) {
            status.fatal = true;
            const m = new Message(MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.22;
            status.messages.push(m);
        } else if (isGreaterThan(this.prms.S.v, 0.2)) {
            const m = new Message(MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.2;
            status.messages.push(m);
        }

        return status;
    }

    protected get coef() {
        return {
            "h": {
                "c0": [-3.56494, 0.450262, 0.0407576],
                "c1": [42.4113, -24.4941, 8.84146],
                "c2": [-73.4829, 54.6733, -14.0622]
            },
            "ha": {
                "c0": [15.8096, -5.19282, 0.465827],
                "c1": [302.623, -106.203, 13.2957],
                "c2": [-783.592, 269.991, -25.2637]
            }
        };
    }

    protected CalcA(): number {
        return 0.2 * this.prms.L.V;
    }
}
