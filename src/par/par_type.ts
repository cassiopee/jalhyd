import { ParParams } from "../internal_modules";
import { Result } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";
import { ParType } from "../internal_modules";
import { Message } from "../internal_modules";
import { ParSimulationParams } from "../internal_modules";

export abstract class ParTypeAbstract {

    protected prms: ParParams;

    /** type of the current PAR calculation class */
    protected type: ParType;

    /** Constant where acceleration is involved */
    protected abstract get RG(): number;

    public constructor(prms: ParParams) {
        this.prms = prms;
    }

    public abstract checkInput(): { fatal: boolean, messages: Message[] };

    public abstract CalcH(): number;

    public abstract CalcHa(qStar?: number): number;

    // used by CalcH and CalcHa
    public abstract CalcQStar(): number;

    /** minimum value of Q* according to abacuses */
    public abstract get minQstar(): number;

    /** maximum value of Q* according to abacuses */
    public abstract get maxQstar(): number;

    public abstract CalcQFromQStar(qStar: number): number;

    public abstract CalcAw(): number;

    /** Calculate ZR1 from ZD1, or ZR2 from ZD2 */
    // tslint:disable-next-line:variable-name
    public abstract CalcZRFromZD(ZDv: number): number;

    /** reciprocal of CalcZRFromZD() */
    // tslint:disable-next-line:variable-name
    public abstract CalcZDFromZR(ZRv: number): number;

    public CalcZM(): number {
        return this.CalcZR1() + this.maxHa;
    };

    public abstract CalcP(): number;

    public abstract CalcQFromHa(): number;

    /** calculates the number of baffles Nb based on the baffles spacing P */
    public abstract CalcNb(): number;

    public CalcZR1(): number {
        return this.CalcZRFromZD(this.ZD1);
    }

    public addExtraResults(res: Result, includeZmAndZr1: boolean = true): void {
        // hauteur d'eau dans la passe
        res.resultElement.values.h = this.CalcH();
        // débit adimensionnel
        res.resultElement.values.qStar = this.CalcQStar();
        // vitesse débitante
        res.resultElement.values.V = this.CalcVDeb();
        // espacement entre les ralentisseurs
        res.resultElement.values.P = this.P;
        // if Z1 is set
        if (this.prms.Z1.v !== undefined && includeZmAndZr1) {
            // cote d'arase minimale des murs latéraux à l'amont
            res.resultElement.values.ZM = this.CalcZM();
            // cote de radier à l'amont de la passe
            res.resultElement.values.ZR1 = this.CalcZR1();
        }
    }

    public CalcVDeb(): number {
        const aw = this.CalcAw();
        // .V uses calculated value if any
        return this.prms.Q.V / aw;
    }

    public get ZD1(): number {
        // when in ParSimulation
        if (this.prms instanceof ParSimulationParams) {
            // if ZD1 is given and not calculated, use it
            if (this.prms.ZD1.V !== undefined) {
                return this.prms.ZD1.V;
            } else {
                // else get the standard value
                return this.CalcZDFromZR(this.prms.ZR1.V);
            }
        } else {
            return this.prms.Z1.v - this.prms.ha.V; // returns undefined if Z1 (optional in Calage) is not set
        }
    }

    public get ha(): number {
        // when in ParSimulation, do not trigger a calc loop if Z1 and ZD1 are given
        if (this.prms instanceof ParSimulationParams
            && this.prms.Z1.valueMode !== ParamValueMode.CALCUL
        ) {
            return this.prms.Z1.V - this.ZD1;

        } else if (this.prms.ha.V !== undefined) {
            // when ha is a given parameter, use it
            return this.prms.ha.V;

        } else {
            // else calculate ha from abacuses
            return this.CalcHa();
        }
    }

    /** returns the given input P if any, or the standard P calculated by CalcP() */
    public get P(): number {
        if (this.prms.P.v !== undefined) {
            return this.prms.P.v;
        } else {
            return this.CalcP();
        }
    }

    /** minimum value of ha according to abacuses */
    public get minHa(): number {
        return this.CalcHa(this.minQstar);
    }

    /** maximum value of ha according to abacuses */
    public get maxHa(): number {
        return this.CalcHa(this.maxQstar);
    }

    /** Calculate the raw length of the pass based on water elevations */
    public CalcLw(): number {
        return (this.prms.Z1.V - this.prms.Z2.v) * (
            Math.sqrt(1 + this.prms.S.v * this.prms.S.v) / this.prms.S.v
        )
    }

    /** Calculate the net length of the pass based on number of baffles */
    public CalcLs(): number {
        return Math.ceil((this.CalcLw() - 0.001) / this.P) * this.P;
    }

    /** Calculate horizontal projection of pass length */
    public CalcLh(): number {
        return this.CalcLs() / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }

    // return y part of CalcQFromHa
    protected doCalcQYFromHa(x: number): number {
        return (Math.sqrt(Math.pow(this.c1ha, 2) - 4 * this.c2ha * (this.c0ha - x)) - this.c1ha) / (2 * this.c2ha);
    }

    /** coefficients for calculating c(0|1|2)ha? @see getCoeff */
    protected abstract get coef(): {
        [key: string]: {
            [key: string]: number[]
        }
    }

    /**
     * get c0, c1 or c2 coefficient for h or ha, for the current pass type
     * @param hOrHa "h" or "ha" :)
     * @param ci "c0", "c1" or "c2"
     */
    protected getCoeff(hOrHa: string, ci: string): number {
        const a = this.coef[hOrHa][ci][0];
        const b = this.coef[hOrHa][ci][1];
        const c = this.coef[hOrHa][ci][2];
        return a * Math.pow(this.prms.S.v, 2) + b * this.prms.S.v + c;
    }

    protected get c0h(): number {
        return this.getCoeff("h", "c0");
    }
    protected get c1h(): number {
        return this.getCoeff("h", "c1");
    }
    protected get c2h(): number {
        return this.getCoeff("h", "c2");
    }
    protected get c0ha(): number {
        return this.getCoeff("ha", "c0");
    }
    protected get c1ha(): number {
        return this.getCoeff("ha", "c1");
    }
    protected get c2ha(): number {
        return this.getCoeff("ha", "c2");
    }
}
