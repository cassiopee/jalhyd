import { CalculatorType } from "../internal_modules";
import { ParamCalculability, ParamDefinition } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { ParSimulationParams } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { ResultElement } from "../internal_modules";
import { ParType, Par } from "../internal_modules";
import { isEqual, round, isGreaterThan, isLowerThan } from "../internal_modules";

export enum ParFlowRegime {
    /** Free flow (unsubmerged) */
    FREE,
    /** Submerged flow */
    SUBMERGED
}

export class ParSimulation extends Par implements Observer {

    constructor(prms: ParSimulationParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.ParSimulation);
        this.prms.ha.visible = false; // show ZD1 instead
    }

    get prms(): ParSimulationParams {
        return this._prms as ParSimulationParams;
    }

    public get parType(): ParType {
        return this.getPropValue("parType");
    }

    public set parType(e: ParType) {
        this.setPropValue("parType", e);
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        // possible error result
        const r: Result = new Result(new ResultElement());
        let hasError = false;

        // If P is given, check it against standard value
        if (this.prms.P.v !== undefined) {
            const standardP = this.parCalc.CalcP();
            const minP = standardP * 0.9;
            const maxP = standardP * 1.05;
            if (isLowerThan(this.prms.P.v, minP) || isGreaterThan(this.prms.P.v, maxP)) {
                const m = new Message(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
                m.extraVar.stdP = standardP;
                r.resultElement.addMessage(m);
                hasError = true;
            }
        } // else will be calculated and displayed in results

        // Check admissible values for S, L, N, a, M
        const status: { fatal: boolean, messages: Message[] } = this.parCalc.checkInput();
        if (status.fatal) {
            for (const m of status.messages) {
                r.resultElement.addMessage(m);
            }
            hasError = true;
        }

        // Check elevations: ZR1 / ZD1 and ZR2 / ZD2
        const czRes1 = this.checkZrZd(this.prms.ZR1, this.prms.ZD1);
        const mzrd1 = czRes1.message;
        if (mzrd1 !== undefined) {
            r.resultElement.addMessage(mzrd1);
            hasError = true;
        }
        const czRes2 = this.checkZrZd(this.prms.ZR2, this.prms.ZD2);
        const mzrd2 = czRes2.message;
        if (mzrd2 !== undefined) {
            r.resultElement.addMessage(mzrd2);
            hasError = true;
        }

        // Check Nb
        const nb = this.CalcNb(czRes1.expectedZD, czRes2.expectedZD); // reference value
        if (this.prms.Nb.v === undefined) {
            this.prms.Nb.v = nb;
            this.prms.Nb.singleValue = round(nb, 3);
        } else {
            if (nb !== this.prms.Nb.v) {
                const m = new Message(MessageCode.ERROR_PAR_NB_INCONSISTENT);
                m.extraVar.stdNb = String(nb); // to prevent "14.000"
                r.resultElement.addMessage(m);
                hasError = true;
            }
        } // else will be calculated and displayed in results

        // Check that input data is in the range given by abacuses
        if (this.calculatedParam === this.prms.Z1) {
            // check qStar
            const qStar = this.parCalc.CalcQStar();
            if (isLowerThan(qStar, this.parCalc.minQstar) || isGreaterThan(qStar, this.parCalc.maxQstar)) {
                const m = new Message(MessageCode.ERROR_PAR_QSTAR_OUT_OF_RANGE);
                m.extraVar.val = this.prms.Q.V;
                m.extraVar.min = this.parCalc.CalcQFromQStar(this.parCalc.minQstar);
                m.extraVar.max = this.parCalc.CalcQFromQStar(this.parCalc.maxQstar);
                r.resultElement.addMessage(m);
                hasError = true;
            }
        } else if (this.calculatedParam === this.prms.Q) {
            const m = this.checkHaAbacus();
            if (m !== undefined) {
                r.resultElement.addMessage(m);
                hasError = true;
            }
        }

        // if any fatal error occurred
        if (hasError) {
            this.currentResultElement = r;
            return this.result;
        }

        this.nubCalc(sVarCalc, rInit);

        // add checkInput()'s non fatal warnings to current result
        if (!status.fatal) {
            for (const m of status.messages) {
                this.result.resultElement.addMessage(m);
            }
        }

        // extra results
        this.parCalc.addExtraResults(this.result, false);
        this.result.resultElement.values.ha = this.parCalc.ha;
        this.result.resultElement.values.Nb = nb;
        // add expected values for elevations that were not given
        if (this.prms.ZR1.v === undefined) {
            this.result.resultElement.values.ZR1 = czRes1.expectedZR;
        } else {
            // remove ZR1 systematically calculated by ParType.addExtraResults()
            delete this.result.resultElement.values.ZR1;
        }
        if (this.prms.ZR2.v === undefined) {
            this.result.resultElement.values.ZR2 = czRes2.expectedZR;
        }
        if (this.prms.ZD1.v === undefined) {
            this.result.resultElement.values.ZD1 = czRes1.expectedZD;
        }
        if (this.prms.ZD2.v === undefined) {
            this.result.resultElement.values.ZD2 = czRes2.expectedZD;
        }

        // do we have a fall at downstream baffle level ?
        if (this.prms.Z2.v !== undefined) {
            const ZD2 = this.prms.ZD2.v || this.result.resultElement.values.ZD2;
            // water level at downstream baffle
            const ZDB = ZD2 + this.result.resultElement.values.h;
            if (isLowerThan(this.prms.Z2.v, ZDB)) {
                const m = new Message(MessageCode.WARNING_PAR_NOT_SUBMERGED);
                m.extraVar.DH = ZDB - this.prms.Z2.v;
                this.result.resultElement.addMessage(m);
            }
            // expose fall for Verificateur
            this.result.resultElement.values.DH = Math.max(0, ZDB - this.prms.Z2.v);
        }

        return this.result;
    }

    /**
     * Checks that at least one of ZR, ZD is given; if both are given, checks that
     * each one corresponds to the expected value calculated from the other one;
     * returned message id undefined if everything is OK
     * @param ZR ZR1 or ZR2
     * @param ZD ZD1 or ZD2
     */
    protected checkZrZd(ZR: ParamDefinition, ZD: ParamDefinition): { message: Message, expectedZR: number, expectedZD: number } {
        let message: Message;
        let expectedZR: number;
        let expectedZD: number;
        if (ZR.v === undefined && ZD.v === undefined) {
            message = new Message(MessageCode.ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED);
            message.extraVar.variables = [ZR.symbol, ZD.symbol];
        } else {
            // calculate missing value
            let zrDef = true;
            let zdDef = true;
            if (ZR.v === undefined) {
                zrDef = false;
                expectedZR = this.parCalc.CalcZRFromZD(ZD.V);
                /* ZR.v = expectedZR;
                ZR.singleValue = round(expectedZR, 3); */
            }
            if (ZD.v === undefined) {
                zdDef = false;
                expectedZD = this.parCalc.CalcZDFromZR(ZR.V);
                /* ZD.v = expectedZD;
                ZD.singleValue = round(expectedZD, 3); */
            }
            // if both values were defined, check that they match
            if (zrDef && zdDef) {
                expectedZR = this.parCalc.CalcZRFromZD(ZD.V);
                expectedZD = this.parCalc.CalcZDFromZR(ZR.V);
                if (!isEqual(ZR.v, expectedZR, 1E-3)) {
                    message = new Message(MessageCode.ERROR_PAR_ZR_ZD_MISMATCH);
                    message.extraVar.var_ZR = ZR.symbol;
                    message.extraVar.var_ZD = ZD.symbol;
                    message.extraVar.expectedZR = expectedZR;
                    message.extraVar.expectedZD = expectedZD;
                }
            }
        }
        return {
            message,
            expectedZR,
            expectedZD
        }
    }

    protected CalcNb(expectedZD1?: number, expectedZD2?: number): number {
        // if parameters ZD1 and ZD2 were not given, use expected values calculated by checkZrZd
        let realZD1 = this.prms.ZD1.v;
        if (realZD1 === undefined) {
            realZD1 = expectedZD1;
        }
        let realZD2 = this.prms.ZD2.v;
        if (realZD2 === undefined) {
            realZD2 = expectedZD2;
        }
        const rLs = (realZD1 - realZD2) * Math.sqrt(1 + this.prms.S.v * this.prms.S.v) / this.prms.S.v;
        const nb = Math.floor((rLs + 0.01) / this.parCalc.P)
        switch (this.parType) {
            case ParType.PLANE:
            case ParType.FATOU:
                return nb + 1;
            case ParType.SUPERACTIVE:
            case ParType.CHEVRON:
                return nb;
        }
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Q":
                v = this.parCalc.CalcQFromHa();
                break;

            case "Z1":
                v = this.parCalc.ZD1 + this.parCalc.CalcHa();
                break;

            default:
                throw new Error("Par.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    protected setParametersCalculability() {
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.Z1.calculability = ParamCalculability.EQUATION;
        this.prms.Z2.calculability = ParamCalculability.FREE;
        this.prms.S.calculability = ParamCalculability.FIXED;
        this.prms.P.calculability = ParamCalculability.FIXED;
        this.prms.Nb.calculability = ParamCalculability.FIXED;
        this.prms.ZR1.calculability = ParamCalculability.FIXED;
        this.prms.ZD1.calculability = ParamCalculability.FIXED;
        this.prms.ZR2.calculability = ParamCalculability.FIXED;
        this.prms.ZD2.calculability = ParamCalculability.FIXED;
        this.prms.L.calculability = ParamCalculability.FIXED;
        this.prms.a.calculability = ParamCalculability.FIXED;
        this.prms.N.calculability = ParamCalculability.FIXED;
        this.prms.M.calculability = ParamCalculability.FIXED;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            h: undefined,
            ha: undefined,
            V: undefined
        }
    }

}
