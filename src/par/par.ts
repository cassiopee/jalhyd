import { CalculatorType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { ParParams } from "../internal_modules";
import { ParTypeAbstract } from "../internal_modules";
import { ParTypePlane } from "../internal_modules";
import { ParTypeFatou } from "../internal_modules";
import { ParTypeSuperactive } from "../internal_modules";
import { ParTypeChevron } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { ResultElement } from "../internal_modules";
import { isLowerThan, isGreaterThan } from "../internal_modules";
import { FishPass } from "../internal_modules";

/** Type de Passe à Ralentisseurs */
export enum ParType {
    PLANE,
    FATOU,
    SUPERACTIVE,
    CHEVRON
}

export class Par extends FishPass implements Observer {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        h: "m",
        qStar: "m³/s",
        V: "m/s",
        ZD1: "m",
        ZR1: "m",
        ZD2: "m",
        ZR2: "m",
        ZM: "m",
        LPI: "m",
        LPH: "m",
        B: "m",
        C: "m",
        D: "m",
        H: "m",
        Hmin: "m",
        Hmax: "m"
    };

    protected parCalc: ParTypeAbstract;

    constructor(prms: ParParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Par);
        this._defaultCalculatedParam = prms.Q;
        this.resetDefaultCalculatedParam();
        this._props.addObserver(this);
        this.parType = ParType.PLANE;
    }

    get prms(): ParParams {
        return this._prms as ParParams;
    }

    public get parType(): ParType {
        return this.getPropValue("parType");
    }

    public set parType(e: ParType) {
        this.setPropValue("parType", e);
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        // possible error result
        const r: Result = new Result(new ResultElement());
        let hasError = false;

        // if P is given and L is not calculated, check P value
        if (this.calculatedParam !== this.prms.L && this.prms.P.v !== undefined) {
            const standardP = this.parCalc.CalcP();
            const minP = standardP * 0.9;
            const maxP = standardP * 1.05;
            if (isLowerThan(this.prms.P.v, minP) || isGreaterThan(this.prms.P.v, maxP)) {
                const m = new Message(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
                m.extraVar.stdP = standardP;
                r.resultElement.addMessage(m);
                hasError = true;
            }
        }

        // Check admissible values for S, L, N, a, M
        const status: { fatal: boolean, messages: Message[] } = this.parCalc.checkInput();
        if (status.fatal) {
            for (const m of status.messages) {
                r.resultElement.addMessage(m);
            }
            hasError = true;
        }

        // Check that input data is in the range given by abacuses
        switch (this.calculatedParam) {
            case this.prms.ha:
                // check qStar
                const qStar = this.parCalc.CalcQStar();
                if (isLowerThan(qStar, this.parCalc.minQstar) || isGreaterThan(qStar, this.parCalc.maxQstar)) {
                    const mqstar = new Message(MessageCode.ERROR_PAR_QSTAR_OUT_OF_RANGE);
                    mqstar.extraVar.val = this.prms.Q.V;
                    mqstar.extraVar.min = this.parCalc.CalcQFromQStar(this.parCalc.minQstar);
                    mqstar.extraVar.max = this.parCalc.CalcQFromQStar(this.parCalc.maxQstar);
                    r.resultElement.addMessage(mqstar);
                    hasError = true;
                }
                break;
            case this.prms.Q:
                const m = this.checkHaAbacus();
                if (m !== undefined) {
                    r.resultElement.addMessage(m);
                    hasError = true;
                }
        }

        // if any fatal error occurred
        if (hasError) {
            this.currentResultElement = r;
            return this.result;
        }

        super.Calc(sVarCalc, rInit);

        // add checkInput()'s non fatal warnings to current result
        for (const m of status.messages) {
            this.result.resultElement.addMessage(m);
        }

        if (!this.result.ok) return this.result;

        // extra results
        this.parCalc.addExtraResults(this.result);

        // pass type independent extra results
        if (this.prms.Z1.v !== undefined) {
            // cote du déversoir à l'amont
            this.result.resultElement.values.ZD1 = this.parCalc.ZD1;

            if (this.prms.Z2.v !== undefined) {
                const P = this.result.resultElement.values.P; // calculated by addExtraResults()
                const ZR1 = this.result.resultElement.values.ZR1; // calculated by addExtraResults()
                // nombre de ralentisseurs
                // tslint:disable-next-line:variable-name
                const Nb = this.parCalc.CalcNb(); // depends on Z1 and Z2
                this.result.resultElement.values.Nb = Nb;
                // cote de déversement à l'aval de la passe
                const ZD2 = this.parCalc.ZD1 - this.parCalc.CalcLs() * this.prms.S.v / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
                this.result.resultElement.values.ZD2 = ZD2; // DEBUG
                // cote de radier de l'aval de la passe
                this.result.resultElement.values.ZR2 = ZD2 - (this.parCalc.ZD1 - ZR1);
                // longueur de la passe en suivant la pente
                const LPI = this.parCalc.CalcLs();
                this.result.resultElement.values.LPI = LPI;
                // longueur de la passe en projection horizontale
                this.result.resultElement.values.LPH = this.parCalc.CalcLh();
            }
        }

        return this.result;
    }

    protected checkHaAbacus(): Message {
        let m: Message;
        if (isLowerThan(this.parCalc.ha, this.parCalc.minHa) || isGreaterThan(this.parCalc.ha, this.parCalc.maxHa)) {
            m = new Message(MessageCode.ERROR_PAR_HA_OUT_OF_RANGE);
            m.extraVar.val = this.parCalc.ha;
            m.extraVar.min = this.parCalc.minHa;
            m.extraVar.max = this.parCalc.maxHa;
        }
        return m;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Q":
                v = this.parCalc.CalcQFromHa();
                break;

            case "ha":
                v = this.parCalc.CalcHa();
                break;

            default:
                throw new Error("Par.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    public getFirstAnalyticalParameter() {
        // use ha in dicho, when calculating L, to prevent negative sqrt() in doCalcQYFromHa()
        return this.prms.ha;
    }

    public update(sender: any, data: any) {
        if (data.action === "propertyChange") {
            if (data.name === "parType") {
                switch (data.value) {
                    case ParType.PLANE:
                        this.parCalc = new ParTypePlane(this.prms);
                        break;
                    case ParType.FATOU:
                        this.parCalc = new ParTypeFatou(this.prms);
                        break;
                    case ParType.SUPERACTIVE:
                        this.parCalc = new ParTypeSuperactive(this.prms);
                        break;
                    case ParType.CHEVRON:
                        this.parCalc = new ParTypeChevron(this.prms);
                        break;
                    default:
                        throw new Error("Par.update() : invalid Par Type name " + data.value);
                }
                this.updateParamsVisibility();
            }
        }
    }

    protected updateParamsVisibility() {
        this.prms.L.visible = ([ParType.PLANE, ParType.FATOU].includes(this.parType));
        this.prms.a.visible = ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(this.parType));
        this.prms.N.visible = ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(this.parType));
        this.prms.M.visible = (this.parType === ParType.CHEVRON);
    }

    protected setParametersCalculability() {
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.ha.calculability = ParamCalculability.EQUATION;
    }

    public static override resultsUnits() {
        return Par._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            h: undefined,
            qStar: undefined,
            V: undefined,
            Nb: undefined,
            ZD1: undefined,
            ZR1: undefined,
            ZD2: undefined,
            ZR2: undefined,
            ZM: undefined,
            LPI: undefined,
            LPH: undefined,
            L: undefined
        }
    }

}
