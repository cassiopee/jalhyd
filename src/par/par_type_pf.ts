import { ParTypeAbstract } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { ParamValueMode } from "../internal_modules";

/**
 * Intermediate class for common stuff between PLANE and FATOU pass types
 */
export abstract class ParTypePF extends ParTypeAbstract {

    /**
     * Math.sqrt(9.81)
     */
    protected get RG(): number {
        return 3.13209;
    }

    public CalcH() {
        const qStar = this.CalcQStar();
        return this.prms.L.v * (this.c2h * Math.pow(qStar, 2) + this.c1h * qStar + this.c0h);
    }

    public CalcHa(qStar?: number) {
        if (qStar === undefined) {
            qStar = this.CalcQStar();
        }
        // @TODO L.V (grand V) pour la vérification de ha p/r aux abaques ? Fait foirer le calcul de L …
        return this.prms.L.v * (this.c2ha * Math.pow(qStar, 2) + this.c1ha * qStar + this.c0ha);
    }

    public CalcQStar(): number {
        return this.prms.Q.V / (this.RG * Math.pow(this.prms.L.v, 2.5));
    }

    public get minQstar(): number {
        return 0.0065;
    }

    public get maxQstar(): number {
        return 0.35;
    }

    public CalcQFromQStar(qStar: number): number {
        return qStar * this.RG * Math.pow(this.prms.L.v, 2.5);
    }

    public CalcLFromQStar(qStar: number): number {
        return Math.pow(qStar * this.RG / this.prms.Q.v, 0.4);
    }

    public CalcQFromHa(): number {
        const x = this.ha / this.prms.L.v;
        const y = this.doCalcQYFromHa(x);
        return this.CalcQFromQStar(y);
    }

    /** calculates the number of baffles Nb based on the baffles spacing P */
    public CalcNb(): number {
        return this.CalcLs() / this.P + 1;
    }

    public checkInput(): { fatal: boolean, messages: Message[] } {
        let fatal = false;
        const messages: Message[] = [];
        // L; when calculated, see Par.Calc()
        if (this.prms.L.valueMode !== ParamValueMode.CALCUL) {
            if (this.prms.L.v < 0.3 || this.prms.L.v > 1.3) {
                fatal = true;
                const m = new Message(MessageCode.ERROR_PAR_L);
                m.extraVar.min = 0.3;
                m.extraVar.max = 1.3;
                messages.push(m);
            } else if (this.prms.L.v > 1) {
                const m = new Message(MessageCode.WARNING_PAR_L);
                m.extraVar.max = 1;
                messages.push(m);
            }
        }

        return { fatal, messages };
    }
}
