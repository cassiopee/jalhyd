import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class ParParams extends ParamsEquation {

    /** Débit dans la passe (m3/s) */
    private _Q: ParamDefinition;

    /** Cote de l'eau à l'amont de la passe (m) */
    private _Z1: ParamDefinition;

    /** Cote de l'eau à l'aval de la passe (m) [facultatif] */
    private _Z2: ParamDefinition;

    /** Charge à l'amont de la passe (m) */
    private _ha: ParamDefinition;

    /** Pente (m/m) */
    private _S: ParamDefinition;

    /** Espacement entre les ralentisseurs (m) [facultatif] */
    private _P: ParamDefinition;

    /** Largeur de la passe (PLANE et FATOU) (m) */
    private _L: ParamDefinition;

    /** Hauteur des ralentisseurs (SUPERACTIVE) ou des chevrons (CHEVRON) (m) */
    private _a: ParamDefinition;

    /** Nombre de motifs (SUPERACTIVE) ou de chevrons (CHEVRON) */
    private _N: ParamDefinition;

    /** Nombre de bandes longitudinales (CHEVRON) */
    private _M: ParamDefinition;

    constructor(rQ: number, rZ1: number, rZ2: number, rha: number, rS: number, rP?: number, rL?: number, ra?: number, rN?: number, rM?: number, nullParams: boolean = false) {
        super();
        // paramètres hydrauliques
        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS_NULL, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.addParamDefinition(this.Q);
        this._Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z1);
        this._Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z2);
        // géométrie de la passe
        this._ha = new ParamDefinition(this, "ha", ParamDomainValue.POS_NULL, "m", rha, ParamFamily.HEIGHTS, undefined, nullParams);
        this.addParamDefinition(this.ha);
        this._S = new ParamDefinition(this, "S", ParamDomainValue.POS_NULL, "m/m", rS, ParamFamily.SLOPES, undefined, nullParams);
        this.addParamDefinition(this.S);
        this._P = new ParamDefinition(this, "P", ParamDomainValue.POS_NULL, "m", rP, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this.P);
        // paramètres dépendants du type de passe
        this._L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.WIDTHS, false, nullParams);
        this.addParamDefinition(this.L);
        this._a = new ParamDefinition(this, "a", ParamDomainValue.POS, "m", ra, ParamFamily.HEIGHTS, false, nullParams);
        this.addParamDefinition(this.a);
        this._N = new ParamDefinition(this, "N", ParamDomainValue.POS_NULL, "", rN, undefined, false, nullParams);
        this.addParamDefinition(this.N);
        this._M = new ParamDefinition(this, "M", ParamDomainValue.POS_NULL, "", rM, undefined, false, nullParams);
        this.addParamDefinition(this.M);
    }

    public get Q() {
        return this._Q;
    }

    public get Z1() {
        return this._Z1;
    }

    public get Z2() {
        return this._Z2;
    }

    public get ha() {
        return this._ha;
    }

    public get S() {
        return this._S;
    }

    public get P() {
        return this._P;
    }

    public get L() {
        return this._L;
    }

    public get a() {
        return this._a;
    }

    public get N() {
        return this._N;
    }

    public get M() {
        return this._M;
    }
}
