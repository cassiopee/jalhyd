import { Result } from "../internal_modules";
import { ParTypeSC } from "../internal_modules";
import { ParType } from "../internal_modules";
import { ParParams } from "../internal_modules";

export class ParTypeSuperactive extends ParTypeSC {

    public constructor(prms: ParParams) {
        super(prms);
        this.type = ParType.SUPERACTIVE;
    }

    // tslint:disable-next-line:variable-name
    public CalcZRFromZD(ZDv: number): number {
        return ZDv + (2.6 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }

    // tslint:disable-next-line:variable-name
    public CalcZDFromZR(ZRv: number): number {
        return ZRv - (2.6 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }

    public CalcP(): number {
        return 2.6 * this.prms.a.v;
    }

    public get minQstar(): number {
        return 0.4;
    }

    public get maxQstar(): number {
        return 5.3;
    }

    public addExtraResults(res: Result): void {
        super.addExtraResults(res);
        res.resultElement.values.B = this.CalcB();
        res.resultElement.values.L = this.CalcB() / this.prms.N.v;
    }

    protected get coef() {
        return {
            "h": {
                "c0": [0, -2.62712, 0.601348],
                "c1": [0, 1.15807, 1.07554],
                "c2": [0, -0.559218, 0.000504060]
            },
            "ha": {
                "c0": [0, -2.22434, 0.596682],
                "c1": [0, 0.514953, 1.25460],
                "c2": [0, -0.354624, -0.0153156]
            }
        };
    }

    protected CalcB(): number {
        return 6 * this.prms.a.v * this.prms.N.v;
    }
}
