import { cloneDeep } from "lodash";

import { BiefRegime, CalculatorType, DivingJetSupport, FishSpecies, GrilleProfile, GrilleType, IObservable, LCMaterial, LoiDebit, MRCInclination, MethodeResolution, Observable, Observer, ParType, PressureLossType, SPPOperation, SectionType, StructureType, TrigoOperation, TrigoUnit, isNumeric, RugofondType } from "./internal_modules";

/**
 * get enum numerical value from enum class name and value as a string
 * @param enumClass enum class name
 * @param enumValueName enum value as a string
 * @returns enum numerical value
 */
export function enumValueFromString(enumClass: string, enumValueName: string): any {
    // !! property names must be unique throughout JaLHyd !!
    const enumValues = Props.enumFromProperty[enumClass];
    if (enumValues) {
        return enumValues[enumValueName];
    }
    throw new Error("unknown enum class ${enumClass}");
}

/**
 * represents a boolean: true if provided value at parameter creation must be ignore (@see nghyd/enableEmptyFieldsOnFormInit)
 */
export const Prop_NullParameters: string = "nullparams";

/**
 * Interface permettant de propager de manière transparente des opérations sur des propriétés en encapsulant celles ci
 * (implémentée par Props et les classes possédant directement ou indirectement un membre de type Props, par ex Nub)
 */
export interface IProperties extends IObservable {
    /**
     * get property value
     * @param key property name
     */
    getPropValue(key: string): any;

    /**
     * set property value
     * @param key property name
     * @param val property value to set
     * @param sender object from which modification originates
     */
    setPropValue(key: string, val: any, sender?: any): boolean;

    /**
     * determine if this object directly or indirectly has a given property
     * @param key property name
     */
    hasProperty(key: string): boolean;

    /**
     * list of properties keys
    */
    readonly keys: string[];
}

/**
 * gestion d'un ensemble de propriétés (clé/valeur) qui prévient quand
 * l'une d'entre-elles change
 */
export class Props implements IProperties {

    /** correspondance entre les noms de propriétés et les enum associés */
    public static readonly enumFromProperty: any = {
        calcType: CalculatorType,
        divingJetSupported: DivingJetSupport,
        gridProfile: GrilleProfile,
        gridType: GrilleType,
        inclinedApron: MRCInclination,
        loiDebit: LoiDebit,
        material: LCMaterial,
        methodeResolution: MethodeResolution,
        nodeType: SectionType,
        parType: ParType,
        pressureLossType: PressureLossType,
        regime: BiefRegime,
        species: FishSpecies,
        sppOperation: SPPOperation,
        structureType: StructureType,
        trigoOperation: TrigoOperation,
        trigoUnit: TrigoUnit,
        rugofondType: RugofondType
    };

    // implémentation de IObservable par délégation
    private _observable: Observable;

    // object literal representing properties keys and values
    private _map: any = {};

    constructor(prps?: any | IProperties) {
        this._observable = new Observable();

        if (prps !== undefined) {
            if (Props.implementsIProperties(prps)) {
                this.copyIProperties(prps);
            }
            else
                this._map = cloneDeep(prps);
        }
    }

    /**
     * @returns true if an object implements the IProperties interface
     */
    public static implementsIProperties(o: any): boolean {
        return "getPropValue" in o && "setPropValue" in o && "hasProperty" in o && "keys" in o;
    }

    /**
     * set properties map from an IProperties
     */
    private copyIProperties(p: IProperties) {
        this._map = {};
        for (const k of p.keys) {
            this._map[k] = p.getPropValue(k);
        }
    }

    public get keys(): string[] {
        return Object.keys(this._map);
    }

    public getPropValue(key: string): any {
        return this._map[key];
    }

    public setPropValue(key: string, val: any, sender?: any): boolean {
        if (key === undefined) {
            throw new Error("setPropValue : undefined key!");
        }
        const oldValue = this._map[key];
        const changed = oldValue !== val;
        if (changed) {
            this._map[key] = val;
            this.notifyPropChange(key, val, sender);
        }
        return changed;
    }

    public removeProp(key: string) {
        delete this._map[key];
    }

    public hasProperty(key: string): boolean {
        return true;
    }

    public getObservers(): Observer[] {
        return this._observable.getObservers();
    }

    /**
     * Remove all properties, does not notify of any change
     */
    public reset() {
        this._map = {};
    }

    /**
     * Clones properties into a new Props object
     */
    public clone(): Props {
        return new Props(this);
    }

    public toString(): string {
        let res = "[";
        for (const k in this._map) {
            if (this._map.hasOwnProperty(k)) {
                if (res !== "[") {
                    res += ", ";
                }
                res += `${k}:${this._map[k]}`;
            }
        }
        res += "]";
        return res;
    }

    // interface IObservable

    /**
     * ajoute un observateur à la liste
     */
    public addObserver(o: Observer) {
        this._observable.addObserver(o);
    }

    /**
     * supprime un observateur de la liste
     */
    public removeObserver(o: Observer) {
        this._observable.removeObserver(o);
    }

    /**
     * notifie un événement aux observateurs
     */
    public notifyObservers(data: any, sender?: any) {
        this._observable.notifyObservers(data, sender);
    }

    /**
     * notification de changement de la valeur d'une propriété
     * @param prop nom de la propriété modifiée
     * @param val nouvelle valeur
     * @param sender objet ayant changé la valeur
     */
    private notifyPropChange(prop: string, val: any, sender: any) {
        this.notifyObservers({
            action: "propertyChange",
            name: prop,
            value: val
        }, sender);
    }

    /**
     * Returns a copy of given map, inverting enum keys and values
     */
    public static invertEnumKeysAndValuesInProperties(stringProps: any, forceNumbers: boolean = false) {
        const res = JSON.parse(JSON.stringify(stringProps)); // clone
        for (const k in res) {
            if (!forceNumbers || !isNumeric(res[k])) {
                if (Object.keys(Props.enumFromProperty).includes(k)) {
                    const enumClass = Props.enumFromProperty[k];
                    res[k] = enumClass[res[k]];
                }
            }
        }
        return res;
    }

    public invertEnumKeysAndValues(forceNumbers: boolean = false) {
        return Props.invertEnumKeysAndValuesInProperties(this._map, forceNumbers);
    }
}
