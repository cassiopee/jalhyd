import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
export class TriangularTruncStructureParams extends StructureParams {
    /** Demi-ouverture du triangle (m) */
    public BT: ParamDefinition;

    /** Cote haute du triangle (m) */
    public ZT: ParamDefinition;

    /** Discharge coefficient */
    // tslint:disable-next-line:variable-name
    public CdT: ParamDefinition;

    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rBT   Demi-ouverture du triangle (m)
     * @param rZT   Cote haute du triangle (m)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(
        rQ: number, rZDV: number, rZ1: number, rZ2: number,
        rBT: number, rZT: number, rCd: number, rW: number = Infinity, nullParams: boolean = false
    ) {
        super(rQ, rZDV, rZ1, rZ2, rW, nullParams);
        this.BT = new ParamDefinition(this, "BT", ParamDomainValue.POS, "m", rBT, undefined, undefined, nullParams);
        this.addParamDefinition(this.BT);
        this.ZT = new ParamDefinition(this, "ZT", ParamDomainValue.POS, "m", rZT, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZT);
        this.CdT = new ParamDefinition(this, "CdT", new ParamDomain(ParamDomainValue.INTERVAL, 0, 10), undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdT);
    }
}
