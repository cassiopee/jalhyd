import { RectangularStructureParams } from "../internal_modules";
import { StructureFlowMode } from "../internal_modules";
import { StructureGateCem88v } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

export class StructureWeirCem88v extends StructureGateCem88v {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirCem88v);
        this.prms.W.visible = false;
    }

    protected getFlowMode(): StructureFlowMode {
        return StructureFlowMode.WEIR;
    }

}
