import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation classique seuil dénoyé
 */
export class StructureWeirFree extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirFree);
        this.prms.CdWR.visible = true;
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        // do not check h2 for derived classes (ex: StructureWeirVillemonte)
        if (this.loiDebit === LoiDebit.WeirFree && this.prms.h2.v > 0) {
            this._result.resultElement.addMessage(new Message(
                MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION,
                {
                    submergencePerc: this.computeSubmergencePercentage().toString()
                }
            ));
        }
        return this._result;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdWR.v * this.prms.L.v * Structure.R2G * Math.pow(this.prms.h1.v, 1.5);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.FREE;
    }

    protected getFlowMode() {
        return StructureFlowMode.WEIR;
    }
}
