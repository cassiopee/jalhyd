import { Result } from "../internal_modules";
import { StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";
import { TriangularStructureParams } from "../internal_modules";
import { Villemonte } from "../internal_modules";
import { StructureTriangularWeirFree } from "../internal_modules";

/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
export class StructureTriangularWeir extends StructureTriangularWeirFree {

    constructor(prms: TriangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.TriangularWeirFree); // First name of the law for backward compatibility
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {

        const r = super.CalcQ();

        if (r.values.ENUM_StructureFlowRegime !== StructureFlowRegime.FREE) {
            r.vCalc = Villemonte(this.prms.h1.v, this.prms.h2.v, 2.5) * r.vCalc;
        }

        return r;
    }

    protected getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return StructureFlowRegime.FREE;
        } else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
                // Yc = 4 / 5 * H1 (Jameson, 1925)
                return StructureFlowRegime.PARTIAL;
            }
            return StructureFlowRegime.SUBMERGED;
        }
    }
}
