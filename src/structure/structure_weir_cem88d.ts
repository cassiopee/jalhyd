import { RectangularStructureParams } from "../internal_modules";
import { StructureFlowMode } from "../internal_modules";
import { StructureGateCem88d } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

export class StructureWeirCem88d extends StructureGateCem88d {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirCem88d);
        this._isZDVcalculable = true;
        // Gestion de l'affichage l'ouverture de vanne
        this.prms.W.visible = false;
    }

    protected getFlowMode(): StructureFlowMode {
        return StructureFlowMode.WEIR;
    }

}
