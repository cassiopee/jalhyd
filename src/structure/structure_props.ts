import { ParallelStructure } from "../internal_modules";

export enum StructureType {
    SeuilRectangulaire,
    VanneFondRectangulaire,
    Orifice,
    SeuilTriangulaire,
    SeuilTriangulaireTrunc,
    SeuilOrificeRectangulaire
    // VanneCirculaire,
    // VanneTrapezoidale, SeuilTrapezoidal
}

export enum LoiDebit {
    // loi de débit Déversoir / Orifice Cemagref 1988 (pour Vanne Rectangulaire)
    GateCem88d,
    // loi de débit Déversoir / Vanne de fond Cemagref 1988 (pour Vanne Rectangulaire)
    GateCem88v,
    // loi de débit Déversoir / Orifice Cemagref 1988 (pour Seuil Rectangulaire)
    WeirCem88d,
    // loi de débit Déversoir / Vanne de fond Cemagref 1988 (pour Seuil Rectangulaire)
    WeirCem88v,
    // loi de débit Vanne Cunge 1980
    GateCunge80,
    // loi de débit pour vanne dénoyée
    RectangularOrificeFree,
    // loi de débit pour vanne noyée
    RectangularOrificeSubmerged,
    // loi de débit pour seuil dénoyé
    WeirFree,
    // Loi Kindsvater-Carter et Villemonte
    KIVI,
    // Loi de débit seuil triangulaire (Villemonte)
    TriangularWeirFree,
    // Loi de débit seuil triangulaire tronqué (Villemonte)
    TriangularTruncWeirFree,
    // Loi de débit fente noyée (Larinier 1992)
    WeirSubmergedLarinier,
    // Loi de débit fente noyée (Rajaratnam & Muralidhar 1969)
    WeirSubmerged,
    // Loi de débit orifice noyé
    OrificeSubmerged,
    // Loi de débit orifice dénoyé
    OrificeFree,
    // Loi de seuil noyée Villemonte
    WeirVillemonte,
    // Vanne levante Larinier
    VanLevLarinier,
    // Vanne levante Villemonte
    VanLevVillemonte,
    // loi de débit Seuil Cunge 1980
    WeirCunge80,
    // Loi de débit seuil triangulaire épais (Bos, Discharge measurement structures, 1989, p.137-143)
    TriangularWeirBroad,
}

export const loiAdmissiblesOuvrages: { [key: string]: LoiDebit[] } = {
    Orifice: [
        LoiDebit.OrificeSubmerged, LoiDebit.OrificeFree
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirCem88d, LoiDebit.WeirCem88v, LoiDebit.WeirSubmerged, LoiDebit.WeirSubmergedLarinier,
        LoiDebit.WeirVillemonte, LoiDebit.WeirFree, LoiDebit.KIVI, LoiDebit.WeirCunge80
    ],
    SeuilOrificeRectangulaire: [
        LoiDebit.GateCem88d, LoiDebit.GateCem88v, LoiDebit.GateCunge80
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneFondRectangulaire: [
        LoiDebit.RectangularOrificeFree, LoiDebit.RectangularOrificeSubmerged
    ]
};

export const loiAdmissiblesCloisons: { [key: string]: LoiDebit[] } = {
    Orifice: [
        LoiDebit.OrificeSubmerged
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirSubmergedLarinier, LoiDebit.WeirVillemonte, LoiDebit.WeirCem88d
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneFondRectangulaire: [
        LoiDebit.GateCem88d
    ]
};

export const loiAdmissiblesDever: { [key: string]: LoiDebit[] } = {
    SeuilRectangulaire: [
        LoiDebit.WeirFree
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ]
};

export const loiAdmissiblesCloisonAval: { [key: string]: LoiDebit[] } = {
    Orifice: [
        LoiDebit.OrificeSubmerged
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirSubmergedLarinier, LoiDebit.WeirVillemonte, LoiDebit.WeirCem88d
    ],
    VanneLevante: [
        LoiDebit.VanLevVillemonte, LoiDebit.VanLevLarinier
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneFondRectangulaire: [
        LoiDebit.GateCem88d
    ]
};

export class StructureProperties {
    /**
     * @return true si les valeurs de StructureType et LoiDebit sont compatibles
     */
    public static isCompatibleValues(struct: StructureType, loi: LoiDebit, parentNub: ParallelStructure): boolean {
        return parentNub.getLoisAdmissibles()[StructureType[struct]].includes(loi);
    }

    /**
     * @return la 1ère valeur de StructureType compatible avec la loi de débit, dans le contexte
     * du module de calcul parentNub @TODO la 1ère ? normalement il n'y en a qu'une !
     */
    public static findCompatibleStructure(loi: LoiDebit, parentNub: ParallelStructure): StructureType {
        const loisAdmissibles = parentNub?.getLoisAdmissibles();
        for (const st in loisAdmissibles) {
            if (loisAdmissibles.hasOwnProperty(st)) {
                const lds: LoiDebit[] = loisAdmissibles[st];
                for (const ld of lds) {
                    if (ld === loi) { return (StructureType as any)[st]; }
                }
            }
        }
        return undefined;
    }

    /**
     * trouve la 1ère valeur de LoiDebit compatible avec le type de structure
     * @param struct type de structure avec laquelle la loi de débit doit être compatible
     * @param subset si non vide, recherche la loi de débit compatible dans ce tableau; sinon prend la 1ere
     */
    public static findCompatibleLoiDebit(struct: StructureType, subset: LoiDebit[],
        parentNub: ParallelStructure): LoiDebit {

        const sst: string = StructureType[struct];
        const lois = parentNub.getLoisAdmissibles();

        for (const ld of lois[sst]) {
            if (subset.length === 0 || subset.includes(ld)) {
                return ld;
            }
        }
        return undefined;
    }
}
