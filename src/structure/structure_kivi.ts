import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { StructureKiviParams } from "../internal_modules";
import { LoiDebit } from "../internal_modules";
import { Villemonte } from "../internal_modules";

export class StructureKivi extends Structure {

    constructor(prms: StructureKiviParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.KIVI);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): StructureKiviParams {
        return this._prms as StructureKiviParams;
    }

    public CalcQ(): Result {
        const res: Result = new Result(0, this, this.getResultData());

        // p : pelle
        let p: number = this.prms.ZDV.v - this.prms.ZRAM.v;
        let h1p: number;

        if (p < 0.1) {
            // - p ne doit pas être inférieur à 0,10 m  (Norme NF X10-311-1983)
            res.resultElement.addMessage(new Message(MessageCode.WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE));
            p = 0.1;
        }

        h1p = this.prms.h1.v / p;
        if (h1p > 2.5) {
            // - h/p ne doit pas être supérieur à 2,5 (Norme NF X10-311-1983)
            res.resultElement.addMessage(new Message(MessageCode.WARNING_STRUCTUREKIVI_HP_TROP_ELEVE));
            h1p = 2.5;
        }

        const cd: number = this.prms.alpha.v + this.prms.beta.v * h1p;

        let Q = cd * this.prms.L.v * Structure.R2G * Math.pow(this.prms.h1.v, 1.5);

        if (res.resultElement.values.ENUM_StructureFlowRegime === StructureFlowRegime.SUBMERGED) {
            Q = Villemonte(this.prms.h1.v, this.prms.h2.v, 1.5) * Q;
        }

        res.resultElement.vCalc = Q;
        return res;

    }

    protected getFlowRegime(): StructureFlowRegime {
        if (this.prms.h2.v > 0) {
            return StructureFlowRegime.SUBMERGED;
        } else {
            return StructureFlowRegime.FREE;
        }
    }

    protected getFlowMode(): StructureFlowMode {
        return StructureFlowMode.WEIR;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = ParamCalculability.DICHO;
        this.prms.alpha.calculability = ParamCalculability.DICHO;
        this.prms.beta.calculability = ParamCalculability.FREE;
        this.prms.ZRAM.calculability = ParamCalculability.FREE;
    }
}
