import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Common parameters of hydraulic structure equations
 */
export class StructureParams extends ParamsEquation {
    /** Débit (m3/s) */
    public Q: ParamDefinition;

    /** Cote de la crête du déversoir ou du radier de la vanne (m) */
    public ZDV: ParamDefinition;

    /** Cote de l'eau amont (m) */
    public Z1: ParamDefinition;

    /** Cote de l'eau aval (m) */
    public Z2: ParamDefinition;

    /** Tirant d'eau amont (m) */
    public h1: ParamDefinition;

    /** Tirant d'eau aval (m) */
    public h2: ParamDefinition;

    /** Ouverture de la vanne
     * @note Pour un seuil cette valeur vaut Infinity
     */
    public W: ParamDefinition;

    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit (m3/s)
     * @param rZDV Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZ2 Cote de l'eau aval (m)
     * @param rW Ouverture de vanne (m) (infinity pour un seuil)
     */
    constructor(rQ: number, rZDV: number, rZ1: number, rZ2: number, rW: number = Infinity, nullParams: boolean = false) {
        super();
        this.Q = new ParamDefinition(this, "Q", ParamDomainValue.ANY, "m³/s", rQ, ParamFamily.FLOWS, false, nullParams);
        this.addParamDefinition(this.Q);
        this.ZDV = new ParamDefinition(this, "ZDV", ParamDomainValue.ANY, "m", rZDV, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZDV);
        this.Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, undefined, false, nullParams);
        this.addParamDefinition(this.Z1);
        this.Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, undefined, false, nullParams);
        this.addParamDefinition(this.Z2);
        this.h1 = new ParamDefinition(this, "h1", ParamDomainValue.POS_NULL, "m",
            Math.max(0, this.Z1.v - this.ZDV.v), undefined, false, nullParams);
        this.addParamDefinition(this.h1);
        this.h2 = new ParamDefinition(this, "h2", ParamDomainValue.POS_NULL, "m",
            Math.max(0, this.Z2.v - this.ZDV.v), undefined, false, nullParams);
        this.addParamDefinition(this.h2);
        this.W = new ParamDefinition(this, "W", ParamDomainValue.POS_NULL, undefined, rW, undefined, false, nullParams);
        this.addParamDefinition(this.W);
    }

    /** Mise à jour des paramètres h1 et h2 à partir de Z1, Z2 et ZDV */
    public update_h1h2() {
        if (!this.h1.visible) {
            // h1 paramètre caché utilisé dans les lois de débit
            this.h1.v = Math.max(0, this.Z1.v - this.ZDV.v);
        } else {
            // Else ZDV should by updated using h1 #127
            this.ZDV.v = this.Z1.v - this.h1.v;
        }
        this.h2.v = Math.max(0, this.Z2.v - this.ZDV.v);
    }
}
