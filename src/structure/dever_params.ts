import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParallelStructureParams } from "../internal_modules";

/**
 * Common parameters of hydraulic structure equations
 */
export class DeverParams extends ParallelStructureParams {
    /** Largeur du cours d'eau amont (m) */
    public BR: ParamDefinition;

    /** Cote du lit du cours d'eau amont (m) */
    public ZR: ParamDefinition;

    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rBR Largeur du cours d'eau amont (m)
     * @param rZR Cote du lit du cours d'eau amont (m)
     */
    constructor(rQ: number, rZ1: number, rBR: number, rZR: number, nullParams: boolean = false) {
        super(rQ, rZ1, - Infinity);
        this.BR = new ParamDefinition(this, "BR", ParamDomainValue.ANY, "m", rBR, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this.BR);
        this.ZR = new ParamDefinition(this, "ZR", ParamDomainValue.ANY, "m", rZR, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZR);
        // hide fields
        this.Z2.visible = false;
        this.Z2.singleValue = -Infinity;
    }
}
