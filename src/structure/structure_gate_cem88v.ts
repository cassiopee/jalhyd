import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation CEM88V : déversoir / vanne de fond (pelle faible) Cemagref 1988
 */
export class StructureGateCem88v extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.GateCem88v);
        this.prms.W.visible = true;
        this.prms.CdGR.visible = true;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) CEM88V
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        let v: number;
        const mu0: number = 2 / 3 * this.prms.CdGR.v;
        let KF: number;
        if (data.ENUM_StructureFlowRegime !== StructureFlowRegime.FREE) {
            KF = this.getKF(Math.sqrt(1 - this.prms.h2.v / this.prms.h1.v), this.getAlfa(this.prms.h2.v));
        }
        switch (data.ENUM_StructureFlowMode) {
            case StructureFlowMode.WEIR:
                const muf: number = mu0 - 0.08;
                switch (data.ENUM_StructureFlowRegime) {
                    case StructureFlowRegime.FREE:
                        v = muf * this.prms.L.v * Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
                        break;
                    case StructureFlowRegime.SUBMERGED:
                        // console.log("KF="+KF+" muf="+muf);
                        v = KF * muf * this.prms.L.v * Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
                        break;
                }
                break;
            case StructureFlowMode.ORIFICE:
                const mu: number = mu0 - 0.08 / (this.prms.h1.v / this.W);
                const mu1: number = mu0 - 0.08 / (this.prms.h1.v / this.W - 1);
                if (data.ENUM_StructureFlowRegime === StructureFlowRegime.FREE) {
                    v = this.prms.L.v * Structure.R2G
                        * (mu * Math.pow(this.prms.h1.v, 1.5)
                            - mu1 * Math.pow(this.prms.h1.v - this.W, 1.5));
                } else {
                    if (data.ENUM_StructureFlowRegime === StructureFlowRegime.PARTIAL) {
                        v = this.prms.L.v * Structure.R2G * (
                            KF * mu * Math.pow(this.prms.h1.v, 1.5)
                            - mu1 * Math.pow(this.prms.h1.v - this.W, 1.5)
                        );
                    } else {
                        const KF1 = this.getKF(
                            Math.sqrt(1 - (this.prms.h2.v - this.W) / (this.prms.h1.v - this.W)),
                            this.getAlfa(this.prms.h2.v - this.W),
                        );
                        v = this.prms.L.v * Structure.R2G * (
                            KF * mu * Math.pow(this.prms.h1.v, 1.5)
                            - KF1 * mu1 * Math.pow(this.prms.h1.v - this.W, 1.5)
                        );
                    }
                }
        }
        this.debug(
            "StructureWeirCem88v.Equation(h1=" + this.prms.h1.v
            + ",h2=" + this.prms.h2.v + ",W=" + this.W + ") => Q=" + v);

        return new Result(v, this, data);
    }

    /**
     * Give the flow regime for Equation CEM88V : free, partially submerged or submerged flow
     */
    protected getFlowRegime(): StructureFlowRegime {
        const mode: StructureFlowMode = this.getFlowMode();
        // Weir have only two flow regimes: free and submerged flow
        let alfa: number;
        switch (mode) {
            case StructureFlowMode.WEIR:
                alfa = 0.75;
                break;
            case StructureFlowMode.ORIFICE:
                alfa = this.getAlfa(this.prms.h2.v);
                break;
        }
        if (this.prms.h2.v <= alfa * this.prms.h1.v) {
            this.debug(
                "StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=FREE");
            return StructureFlowRegime.FREE;
        } else {
            alfa = this.getAlfa(this.prms.h2.v - this.W);
            if (
                mode === StructureFlowMode.ORIFICE
                && this.prms.h2.v <= alfa * this.prms.h1.v + (1 - alfa) * this.W
            ) {
                this.debug(
                    "StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                    + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=PARTIAL");
                return StructureFlowRegime.PARTIAL;
            } else {
                this.debug(
                    "StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                    + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=SUBMERGED");
                return StructureFlowRegime.SUBMERGED;
            }
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = ParamCalculability.DICHO;
    }

    private getAlfa(h2: number): number {
        let alfa: number = 1 - 0.14 * (h2) / this.W;
        alfa = Math.min(Math.max(alfa, 0.4), 0.75);
        // console.log("alfa=" + alfa);
        return alfa;
    }

    private getKF(x: number, alfa: number): number {
        const beta1 = -2 * alfa + 2.6;
        let KF: number;
        if (x > 0.2) {
            KF = 1 - Math.pow(1 - x / Math.sqrt(1 - alfa), beta1);
        } else {
            KF = 5 * x * (1 - Math.pow(1 - 0.2 / Math.sqrt(1 - alfa), beta1));
        }
        // console.log("KF(x="+x+")="+KF);
        return KF;
    }
}
