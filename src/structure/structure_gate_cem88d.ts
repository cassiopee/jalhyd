import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation CEM88D : déversoir / orifice (pelle importante) Cemagref 1988
 */
export class StructureGateCem88d extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.GateCem88d);
        this._isZDVcalculable = false;
        this.prms.W.visible = true;
        this.prms.CdWR.visible = true;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) CEM88D
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        let v: number;
        const cd: number = this.prms.CdWR.v * this.prms.L.v * Structure.R2G;
        const b1: number = Math.sqrt(this.prms.h1.v);
        const b2: number = Math.sqrt(this.prms.h1.v - this.prms.h2.v);
        const cd1: number = cd * 2.5981; // cd * 3*sqrt(3)/2
        this.debug("StructureWeirCem88d.Equation b1=" + b1 + " b2=" + b2 + " cd1=" + cd1);
        switch (data.ENUM_StructureFlowMode) {
            case StructureFlowMode.WEIR:
                switch (data.ENUM_StructureFlowRegime) {
                    case StructureFlowRegime.FREE:
                        v = cd * this.prms.h1.v * b1;
                        break;
                    case StructureFlowRegime.SUBMERGED:
                        v = cd1 * this.prms.h2.v * b2;
                        this.debug("StructureWeirCem88d.Equation WEIR SUBMERGED Q=" + v);
                        break;
                }
                break;
            case StructureFlowMode.ORIFICE:
                const b3: number = Math.pow(this.prms.h1.v - this.W, 1.5);
                switch (data.ENUM_StructureFlowRegime) {
                    case StructureFlowRegime.FREE:
                        v = cd * (this.prms.h1.v * b1 - b3);
                        break;
                    case StructureFlowRegime.PARTIAL:
                        v = cd1 * b2 * this.prms.h2.v - cd * b3;
                        break;
                    case StructureFlowRegime.SUBMERGED:
                        v = cd1 * b2 * this.W;
                        this.debug("StructureWeirCem88d.Equation ORIFICE SUBMERGED Q=" + v);
                        break;
                }
        }
        this.debug(
            "StructureWeirCem88d.Equation(h1=" + this.prms.h1.v + ",h2="
            + this.prms.h2.v + ",W=" + this.W + ") => Q=" + v);

        return new Result(v, this, data);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = ParamCalculability.DICHO;
    }
}
