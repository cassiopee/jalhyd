import { ParallelStructure } from "../internal_modules";

// Classes générales sur les structures
import { RectangularStructureParams } from "../internal_modules";
import { Structure } from "../internal_modules";
import { LoiDebit, StructureProperties } from "../internal_modules";

// Equations de débit
import { StructureGateCem88d } from "../internal_modules";
import { StructureGateCem88v } from "../internal_modules";
import { StructureGateCunge80 } from "../internal_modules";
import { StructureKivi } from "../internal_modules";
import { StructureKiviParams } from "../internal_modules";
import { StructureOrificeFree } from "../internal_modules";
import { StructureOrificeFreeParams } from "../internal_modules";
import { StructureOrificeSubmerged } from "../internal_modules";
import { StructureOrificeSubmergedParams } from "../internal_modules";
import { StructureRectangularOrificeFree } from "../internal_modules";
import { StructureRectangularOrificeSubmerged } from "../internal_modules";
import { StructureTriangularTruncWeirFree } from "../internal_modules";
import { TriangularTruncStructureParams } from "../internal_modules";
import { StructureTriangularWeir } from "../internal_modules";
import { StructureTriangularWeirBroad } from "../internal_modules";
import { TriangularStructureParams } from "../internal_modules";
import { StructureVanLevLarinier } from "../internal_modules";
import { StructureVanLevParams } from "../internal_modules";
import { StructureVanLevVillemonte } from "../internal_modules";
import { StructureWeirCem88d } from "../internal_modules";
import { StructureWeirCem88v } from "../internal_modules";
import { StructureWeirCunge80 } from "../internal_modules";
import { StructureWeirFree } from "../internal_modules";
import { StructureWeirSubmerged } from "../internal_modules";
import { StructureWeirSubmergedLarinier } from "../internal_modules";
import { StructureWeirVillemonte } from "../internal_modules";

export function CreateStructure(loiDebit: LoiDebit, parentNub?: ParallelStructure,
    dbg: boolean = false, nullParams: boolean = false
): Structure {
    let ret: Structure;
    const oCd: { [s: string]: number } = { SeuilR: 0.4, VanneR: 0.6, VanneRS: 0.8, SeuilT: 1.36 };
    const rectStructPrms: RectangularStructureParams = new RectangularStructureParams(
        0,  // Q
        100,        // ZDV
        102,        // Z1
        101.5,      // Z2
        2,          // L
        oCd.SeuilR, // Cd de seuil par défaut
        0.5, // W (pour les seuils réglé automatiquement à infinity par w.visible = false)
        nullParams
    );
    const vanLevPrms: StructureVanLevParams = new StructureVanLevParams(
        0,
        100,
        102,
        101.5,
        0.4,
        0.4,
        0.5,
        99.5,
        100.5,
        undefined,
        nullParams
    );

    // Cd pour une vanne rectangulaire
    rectStructPrms.CdGR.singleValue = oCd.VanneR;
    rectStructPrms.CdGRS.singleValue = oCd.VanneRS;
    // Instanciation of the equation
    switch (loiDebit) {
        case LoiDebit.WeirCem88d:
            ret = new StructureWeirCem88d(rectStructPrms, dbg);
            break;

        case LoiDebit.WeirCem88v:
            ret = new StructureWeirCem88v(rectStructPrms, dbg);
            break;

        case LoiDebit.GateCem88d:
            ret = new StructureGateCem88d(rectStructPrms, dbg);
            break;

        case LoiDebit.GateCem88v:
            ret = new StructureGateCem88v(rectStructPrms, dbg);
            break;

        case LoiDebit.GateCunge80:
            ret = new StructureGateCunge80(rectStructPrms, dbg);
            rectStructPrms.CdCunge.singleValue = 1;
            break;

        case LoiDebit.WeirCunge80:
            ret = new StructureWeirCunge80(rectStructPrms, dbg);
            rectStructPrms.CdCunge.singleValue = 1;
            break;

        case LoiDebit.RectangularOrificeFree:
            ret = new StructureRectangularOrificeFree(rectStructPrms, dbg);
            break;

        case LoiDebit.RectangularOrificeSubmerged:
            ret = new StructureRectangularOrificeSubmerged(rectStructPrms, dbg);
            break;

        case LoiDebit.WeirFree:
            ret = new StructureWeirFree(rectStructPrms, dbg);
            break;

        case LoiDebit.WeirSubmergedLarinier:
            if (!nullParams) {
                rectStructPrms.L.singleValue = 0.2;
                rectStructPrms.CdWSL.singleValue = 0.75;
                rectStructPrms.ZDV.singleValue = 100;
                rectStructPrms.h1.singleValue = 2;
            }
            ret = new StructureWeirSubmergedLarinier(rectStructPrms, dbg);
            break;

        case LoiDebit.WeirSubmerged:
            rectStructPrms.CdWS.singleValue = 0.9;
            ret = new StructureWeirSubmerged(rectStructPrms, dbg);
            break;

        case LoiDebit.KIVI:
            const structKiviPrm: StructureKiviParams = new StructureKiviParams(
                8.516,      // Q
                101.5,        // ZDV
                103,        // Z1
                102,        // Z2
                2,          // L
                oCd.SeuilR, // alpha
                0.001,      // béta
                100,       // ZRAM : cote Radier Amont
                nullParams
            );
            if (!nullParams) {
                structKiviPrm.h1.singleValue = 1.5;
            }
            ret = new StructureKivi(structKiviPrm, dbg);
            break;

        case LoiDebit.TriangularWeirFree:
        case LoiDebit.TriangularWeirBroad:
            const structTriangPrms: TriangularStructureParams = new TriangularStructureParams(
                0,          // Q
                100,        // ZDV
                102,        // Z1
                100,        // Z2
                45,         // Alpha2
                oCd.SeuilT,  // Cd pour un seuil triangulaire
                Infinity, // W = Infinity par défaut pour un seuil
                nullParams
            );
            if (loiDebit === LoiDebit.TriangularWeirFree) {
                ret = new StructureTriangularWeir(structTriangPrms, dbg);
            } else {
                ret = new StructureTriangularWeirBroad(structTriangPrms, dbg);
            }
            break;

        case LoiDebit.TriangularTruncWeirFree:
            const structTriTruncPrms: TriangularTruncStructureParams = new TriangularTruncStructureParams(
                0,          // Q
                100.1,        // ZDV
                102,        // Z1
                100.1,        // Z2
                0.9,        // BT
                101,        // ZT
                oCd.SeuilT,  // Cd pour un seuil triangulaire
                Infinity, // W = Infinity par défaut pour un seuil
                nullParams
            );
            ret = new StructureTriangularTruncWeirFree(structTriTruncPrms, dbg);
            break;

        case LoiDebit.OrificeSubmerged:
            ret = new StructureOrificeSubmerged(
                new StructureOrificeSubmergedParams(
                    0,      // Q
                    102,    // Z1
                    101.5,  // Z2
                    0.7,    // Cd
                    0.1,     // S
                    nullParams
                ),
                dbg
            );
            break;

        case LoiDebit.OrificeFree:
            ret = new StructureOrificeFree(
                new StructureOrificeFreeParams(
                    0,      // Q
                    102,    // Z1
                    101.5,  // Z2
                    0.7,    // Cd
                    0.1,    // S
                    101,     // Zco
                    nullParams
                ),
                dbg
            );
            break;

        case LoiDebit.WeirVillemonte:
            ret = new StructureWeirVillemonte(rectStructPrms, dbg);
            break;

        case LoiDebit.VanLevVillemonte:
            ret = new StructureVanLevVillemonte(vanLevPrms, dbg);
            break;

        case LoiDebit.VanLevLarinier:
            if (!nullParams) {
                vanLevPrms.CdWSL.singleValue = 0.75;
            }
            ret = new StructureVanLevLarinier(vanLevPrms, dbg);
            break;

        default:
            throw new Error(`type de LoiDebit ${LoiDebit[loiDebit]} non pris en charge`);
    }

    // set reference to parent
    if (parentNub) {
        ret.setParent(parentNub);
        // Set Structure Type
        ret.setPropValue("structureType", StructureProperties.findCompatibleStructure(loiDebit, parentNub));
    }

    return ret;
}
