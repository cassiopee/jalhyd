import { Result } from "../internal_modules";
import { StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";
import { TriangularStructureParams } from "../internal_modules";
import { StructureTriangularWeirFree } from "../internal_modules";

/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
export class StructureTriangularWeirBroad extends StructureTriangularWeirFree {

    constructor(prms: TriangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.TriangularWeirBroad); // First name of the law for backward compatibility
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {

        const r = super.CalcQ();

        if (this.prms.h1.v > 0 && r.values.ENUM_StructureFlowRegime !== StructureFlowRegime.FREE) {
            r.vCalc = r.vCalc * Math.sin(3.962902 * Math.pow((1 - this.prms.h2.v / this.prms.h1.v), 0.574979))
        }

        return r;
    }

    protected getFlowRegime() {
        if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
            // Yc = 4 / 5 * H1 (Jameson, 1925)
            return StructureFlowRegime.FREE;
        }
        return StructureFlowRegime.SUBMERGED;
    }
}
