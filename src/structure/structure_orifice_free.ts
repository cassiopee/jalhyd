import { ParamCalculability } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { StructureOrificeFreeParams } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation classique orifice dénoyé
 */
export class StructureOrificeFree extends Structure {

    constructor(prms: StructureOrificeFreeParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.OrificeFree);
        this._isZDVcalculable = false;
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): StructureOrificeFreeParams {
        return this._prms as StructureOrificeFreeParams;
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        if (this.loiDebit === LoiDebit.OrificeFree && this.prms.Z2.v > this.prms.Zco.v) {
            this._result.resultElement.addMessage(new Message(
                MessageCode.WARNING_ORIFICE_FREE_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION,
                {
                    submergencePerc: this.computeSubmergencePercentage(this.prms.Z2.v - this.prms.Zco.v).toString()
                }
            ));
        }
        return this._result;
    }

    /**
     * Calcul du débit avec l'équation classique d'un orifice dénoyé
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdO.v * this.prms.S.v * Structure.R2G * Math.sqrt(Math.max(0, this.prms.h1.v));

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.FREE;
    }

    protected getFlowMode() {
        return StructureFlowMode.ORIFICE;
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.S.calculability = ParamCalculability.DICHO;
        this.prms.CdO.calculability = ParamCalculability.DICHO;
        this.prms.Zco.calculability = ParamCalculability.DICHO;
        this.prms.ZDV.visible = false;
    }
}
