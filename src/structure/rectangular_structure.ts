import { ParamCalculability } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure } from "../internal_modules";

/**
 * Classe mère pour toutes les structures ayant une base rectangulaire (vannes, seuils)
 */
export abstract class RectangularStructure extends Structure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.prms.CdGR.visible = false;
        this.prms.CdGRS.visible = false;
        this.prms.CdWR.visible = false;
        this.prms.CdWS.visible = false;
        this.prms.CdWSL.visible = false;
        this.prms.CdCunge.visible = false;
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): RectangularStructureParams {
        return this._prms as RectangularStructureParams;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = ParamCalculability.DICHO;
        this.prms.CdGR.calculability = ParamCalculability.DICHO;
        this.prms.CdGRS.calculability = ParamCalculability.DICHO;
        this.prms.CdWR.calculability = ParamCalculability.DICHO;
    }

}
