import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

/**
 * Paramètres pour une équation de seuil rectangulaire Kindsvater-Carter & Villemonte (KIVI)
 */
export class StructureKiviParams extends StructureParams {

    /** Largeur du seuil (m) */
    public L: ParamDefinition;

    /** Coefficient alpha de la formule de Kindsvater */
    public alpha: ParamDefinition;

    /** Coefficient béta de la formule de Kindsvater */
    public beta: ParamDefinition;

    /** Cote du radier amont */
    public ZRAM: ParamDefinition;

    constructor(
        rQ: number,
        rZDV: number,
        rZ1: number,
        rZ2: number,
        rL: number,
        rAlpha: number,
        rBeta: number,
        rZRAM: number,
        nullParams: boolean = false
    ) {
        super(rQ, rZDV, rZ1, rZ2, Infinity, nullParams);
        this.L = new ParamDefinition(this, "L", ParamDomainValue.POS_NULL, "m", rL, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this.L);
        this.alpha = new ParamDefinition(this, "alpha", ParamDomainValue.POS, undefined, rAlpha, undefined, undefined, nullParams);
        this.addParamDefinition(this.alpha);
        this.beta = new ParamDefinition(this, "beta", ParamDomainValue.POS_NULL, undefined, rBeta, undefined, undefined, nullParams);
        this.addParamDefinition(this.beta);
        this.ZRAM = new ParamDefinition(this, "ZRAM", ParamDomainValue.ANY, "m", rZRAM, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZRAM);
    }
}
