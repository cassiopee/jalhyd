import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules"
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation classique orifice noyé ("Vanne noyé")
 */
export class StructureRectangularOrificeSubmerged extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.RectangularOrificeSubmerged);
        if (prms.W.v !== Infinity) {
            this._isZDVcalculable = false;
        }
        this.prms.W.visible = true;
        this.prms.CdGRS.visible = true;
    }

    /**
     * Calcul du débit avec l'équation classique d'un orifice noyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdGRS.v * Math.min(this.prms.W.v, this.prms.h1.v) * this.prms.L.v
            * Structure.R2G * Math.sqrt(this.prms.h1.v - this.prms.h2.v);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.SUBMERGED;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = ParamCalculability.DICHO;
    }
}
