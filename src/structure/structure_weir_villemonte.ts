import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";
import { StructureWeirFree } from "../internal_modules";
import { Villemonte } from "../internal_modules";

export class StructureWeirVillemonte extends StructureWeirFree {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirVillemonte);
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        if ((this.prms.h2.v / this.prms.h1.v) > 0.7) {
            this._result.resultElement.addMessage(new Message(
                MessageCode.WARNING_NOTCH_SUBMERGENCE_GREATER_THAN_07,
                {
                    submergencePerc: this.computeSubmergencePercentage().toString()
                }
            ));
        }
        return this._result;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const r: Result = super.CalcQ();
        if (r.ok && r.resultElement.values.ENUM_StructureFlowRegime !== StructureFlowRegime.FREE) {
            r.vCalc = Villemonte(this.prms.h1.v, this.prms.h2.v, 1.5) * r.vCalc;
        }
        return r;
    }

    protected getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return StructureFlowRegime.FREE;
        } else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 2 / 3 * this.prms.h1.v) {
                // Yc = 2 / 3 * H1 (Jameson, 1925)
                return StructureFlowRegime.PARTIAL;
            }
            return StructureFlowRegime.SUBMERGED;
        }
    }
}
