import { Message, MessageCode, ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation de la fente noyée
 * d'après Rajaratnam, N., et D. Muralidhar.
 * « Flow below deeply submerged rectangular weirs ».
 * Journal of Hydraulic Research 7, nᵒ 3 (1969): 355–374.
 */
export class StructureWeirSubmerged extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirSubmerged);
        this.prms.CdWS.visible = true;
    }

    /**
     * vérification que l'ennoiement est supérieur à une valeur donnée
     */
    public checkSubmergence(res: Result) {
        this.checkSubmergenceMin(res, 0.6);
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        const h2h1ratio = this.prms.h2.v / this.prms.h1.v;
        if (h2h1ratio < 0.8) {
            this._result.resultElement.addMessage(new Message(
                MessageCode.WARNING_WEIR_SUBMERGENCE_LOWER_THAN_08,
                {
                    submergencePerc: this.computeSubmergencePercentage().toString()
                }
            ));
        }
        return this._result;
    }

    /**
     * Calcul analytique Q
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdWS.v * this.prms.L.v * Structure.R2G
            * this.prms.h2.v * Math.sqrt(this.prms.h1.v - this.prms.h2.v);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.SUBMERGED;
    }

    protected getFlowMode() {
        return StructureFlowMode.WEIR;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = ParamCalculability.DICHO;
        this.prms.CdWS.calculability = ParamCalculability.DICHO;
    }
}
