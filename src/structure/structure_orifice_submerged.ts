import { ParamCalculability } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { Result } from "../internal_modules";
import { StructureOrificeSubmergedParams } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation classique orifice noyé
 */
export class StructureOrificeSubmerged extends Structure {

    constructor(prms: StructureOrificeSubmergedParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.OrificeSubmerged);
        this._isZDVcalculable = false;
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): StructureOrificeSubmergedParams {
        return this._prms as StructureOrificeSubmergedParams;
    }

    /**
     * Calcul du débit avec l'équation classique d'un orifice noyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdO.v * this.prms.S.v * Structure.R2G * Math.sqrt(this.prms.Z1.v - this.prms.Z2.v);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.SUBMERGED;
    }

    protected getFlowMode() {
        return StructureFlowMode.ORIFICE;
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.S.calculability = ParamCalculability.DICHO;
        this.prms.CdO.calculability = ParamCalculability.DICHO;
        this.prms.ZDV.visible = false;
    }
}
