import { ParamDefinition } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
export class TriangularStructureParams extends StructureParams {
    /** half angle of the triangle top (degrees) */
    public alpha2: ParamDefinition;

    /** Discharge coefficient */
    // tslint:disable-next-line:variable-name
    public CdT: ParamDefinition;

    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rAlpha2    Demi-angle au sommet du triangle (degrés)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(
        rQ: number, rZDV: number, rZ1: number, rZ2: number,
        rAlpha2: number, rCd: number, rW: number = Infinity, nullParams: boolean = false
    ) {
        super(rQ, rZDV, rZ1, rZ2, rW, nullParams);
        this.alpha2 =
            new ParamDefinition(this, "alpha2", new ParamDomain(ParamDomainValue.INTERVAL, 0, 90), "°", rAlpha2, undefined, undefined, nullParams);
        this.addParamDefinition(this.alpha2);
        this.CdT = new ParamDefinition(this, "CdT", new ParamDomain(ParamDomainValue.INTERVAL, 0, 10), undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdT);
    }
}
