import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";
import { TriangularTruncStructureParams } from "../internal_modules";
import { Villemonte } from "../internal_modules";

/**
 * Equation classique seuil triangulaire (Villemonte)
 */
export class StructureTriangularTruncWeirFree extends Structure {

    constructor(prms: TriangularTruncStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.TriangularTruncWeirFree);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): TriangularTruncStructureParams {
        return this._prms as TriangularTruncStructureParams;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        let Q: number = this.prms.CdT.v * this.prms.BT.v / (this.prms.ZT.v - this.prms.ZDV.v);
        if (this.prms.Z1.v <= this.prms.ZT.v) {
            Q = Q * Math.pow(this.prms.h1.v, 2.5);
        } else {
            Q = Q * (Math.pow(this.prms.h1.v, 2.5) - Math.pow(this.prms.Z1.v - this.prms.ZT.v, 2.5));
        }

        if (data.ENUM_StructureFlowRegime !== StructureFlowRegime.FREE) {
            Q = Villemonte(this.prms.h1.v, this.prms.h2.v, 2.5) * Q;
        }

        return new Result(Q, this, data);
    }

    protected getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return StructureFlowRegime.FREE;
        } else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
                // Yc = 4 / 5 * H1 (Jameson, 1925)
                return StructureFlowRegime.PARTIAL;
            }
            return StructureFlowRegime.SUBMERGED;
        }
    }

    protected getFlowMode() {
        return StructureFlowMode.WEIR;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.BT.calculability = ParamCalculability.DICHO;
        this.prms.ZT.calculability = ParamCalculability.DICHO;
        this.prms.CdT.calculability = ParamCalculability.DICHO;
    }

}
