import { Message, MessageCode, ParallelStructure, ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation de la fente noyé
 * d'après Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992.
 * Passes à poissons : expertise et conception des ouvrages de franchissement
 */
export class StructureWeirSubmergedLarinier extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirSubmergedLarinier);
        this.prms.CdWSL.visible = true;
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): RectangularStructureParams {
        return this._prms as RectangularStructureParams;
    }

    /**
     * vérification que l'ennoiement est supérieur à une valeur donnée
     */
    public checkSubmergence(res: Result) {
        this.checkSubmergenceMin(res, 0.5);
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        const h2h1ratio = this.prms.h2.v / this.prms.h1.v;
        if (!(this.parent as ParallelStructure)?.inhibitSubmergenceError)
            if (h2h1ratio < 0.7 || h2h1ratio > 0.9) {
                this._result.resultElement.addMessage(new Message(
                    MessageCode.WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09,
                    {
                        submergencePerc: this.computeSubmergencePercentage().toString()
                    }
                ));
            }
        return this._result;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdWSL.v * this.prms.L.v * Structure.R2G
            * this.prms.h1.v * Math.sqrt(this.prms.h1.v - this.prms.h2.v);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.SUBMERGED;
    }

    protected getFlowMode() {
        return StructureFlowMode.WEIR;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = ParamCalculability.DICHO;
        this.prms.CdWSL.calculability = ParamCalculability.DICHO;
    }
}
