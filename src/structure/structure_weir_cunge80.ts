import { RectangularStructureParams } from "../internal_modules";
import { StructureFlowMode } from "../internal_modules";
import { StructureGateCunge80 } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

export class StructureWeirCunge80 extends StructureGateCunge80 {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.WeirCunge80);
        this.prms.W.visible = false;
    }

    protected getFlowMode(): StructureFlowMode {
        return StructureFlowMode.WEIR;
    }

}
