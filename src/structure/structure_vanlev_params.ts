import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";

/**
 * Parameters of an automatic weir with crest elevation regulation
 */
export class StructureVanLevParams extends RectangularStructureParams {

    /** Maximum weir crest elevation (m) */
    public minZDV: ParamDefinition;

    /** Minimum weir crest elevation (m) */
    public maxZDV: ParamDefinition;

    /** Imposed fall (m) */
    public DH: ParamDefinition;

    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rL    Largeur de la vanne ou du déversoir (m)
     * @param rCd   Coefficient de débit (-)
     * @param rMinZDV Minimum weir crest elevation (m)
     * @param rMaxZDV Maximum weir crest elevation (m)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(
        rQ: number, rZDV: number, rZ1: number, rZ2: number, rL: number, rCd: number, rDH: number,
        rMinZDV: number, rMaxZDV: number,
        rW: number = Infinity, nullParams: boolean = false) {
        super(rQ, rZDV, rZ1, rZ2, rL, rCd, rW, nullParams);
        this.minZDV = new ParamDefinition(this, "minZDV", ParamDomainValue.ANY, "m", rMinZDV, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.minZDV);
        this.maxZDV = new ParamDefinition(this, "maxZDV", ParamDomainValue.ANY, "m", rMaxZDV, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.maxZDV);
        this.DH = new ParamDefinition(this, "DH", ParamDomainValue.ANY, "m", rDH, ParamFamily.BASINFALLS, undefined, nullParams);
        this.addParamDefinition(this.DH);
        this.ZDV.visible = false;
    }
}
