import { ParamDefinition } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

export class StructureOrificeFreeParams extends StructureParams {

    /** Area of the orifice (m2) */
    public S: ParamDefinition;

    /** Discharge coefficient */
    // tslint:disable-next-line:variable-name
    public CdO: ParamDefinition; // @TODO rename ?

    /** Elevation of orifice center */
    // tslint:disable-next-line:variable-name
    public Zco: ParamDefinition;

    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rCd   Coefficient de débit (-)
     * @param rS    Surface de l'orifice (m2)
     * @param rZco  Cote du centre de l'orifice (m)
     */
    constructor(rQ: number, rZ1: number, rZ2: number, rCd: number, rS: number, rZco: number, nullParams: boolean = false) {
        super(rQ, 100, rZ1, rZ2, undefined, nullParams);
        this.S = new ParamDefinition(this, "S", ParamDomainValue.POS_NULL, "m²", rS, undefined, undefined, nullParams);
        this.addParamDefinition(this.S);
        this.CdO = new ParamDefinition(this, "CdO", new ParamDomain(ParamDomainValue.INTERVAL, 0, 10), undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdO);
        this.Zco = new ParamDefinition(this, "Zco", new ParamDomain(ParamDomainValue.ANY), "m", rZco, undefined, undefined, nullParams);
        this.addParamDefinition(this.Zco);
        // hide params
        this.ZDV.visible = false;
    }

    /** Mise à jour de h1 */
    public update_h1h2() {
        this.h1.v = this.Z1.v - this.Zco.v;
    }

}
