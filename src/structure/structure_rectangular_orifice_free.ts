import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation classique orifice dénoyé ("Vanne dénoyé")
 */
export class StructureRectangularOrificeFree extends RectangularStructure {

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.RectangularOrificeFree);
        this.prms.W.visible = true;
        this.prms.CdGR.visible = true;
    }

    public Calc(sVarCalc: string, rInit?: number): Result {
        this.currentResultElement = super.Calc(sVarCalc, rInit);
        if (this.prms.h2.v > 0) {
            this._result.resultElement.addMessage(new Message(
                MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION,
                {
                    submergencePerc: this.computeSubmergencePercentage().toString()
                }
            ));
        }
        return this._result;
    }

    /**
     * Calcul du débit avec l'équation classique d'un orifice dénoyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const v = this.prms.CdGR.v * Math.min(this.prms.W.v, this.prms.h1.v) * this.prms.L.v
            * Structure.R2G * Math.sqrt(this.prms.h1.v);

        return new Result(v, this, data);
    }

    protected getFlowRegime() {
        return StructureFlowRegime.FREE;
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = ParamCalculability.DICHO;
    }
}
