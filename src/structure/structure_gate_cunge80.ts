import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { RectangularStructure } from "../internal_modules";
import { RectangularStructureParams } from "../internal_modules";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../internal_modules";
import { LoiDebit } from "../internal_modules";

/**
 * Equation Cunge80
 */
export class StructureGateCunge80 extends RectangularStructure {

    /** Contraction coefficient on underflow gates From Henderson, F.M., 1966. Open channel flow. MacMillan, New York.  */
    // tslint:disable-next-line:variable-name
    private static Cc: number = 0.611;

    constructor(prms: RectangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.GateCunge80);
        if (prms.W.v !== Infinity) {
            this._isZDVcalculable = false;
        }
        this.prms.W.visible = true;
        this.prms.CdCunge.visible = true;
    }

    /**
     * Calcul du débit avec l'équation Cunge80
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    public CalcQ(): Result {
        const data = this.getResultData();
        let v: number;

        switch (data.ENUM_StructureFlowRegime) {
            case StructureFlowRegime.FREE:
                if (data.ENUM_StructureFlowMode === StructureFlowMode.WEIR) {
                    const R32: number = 3 * Math.sqrt(3) / 2;
                    v = this.prms.CdCunge.v * this.prms.L.v * Structure.R2G / R32 * Math.pow(this.prms.h1.v, 1.5);
                    this.debug("StructureCunge80.Equation WEIR FREE Q=" + v);
                } else {
                    // Cd from Henderson, F.M., 1966. Open channel flow. MacMillan, New York.
                    // tslint:disable-next-line:variable-name
                    const Cd = StructureGateCunge80.Cc / Math.sqrt(1 + StructureGateCunge80.Cc * this.W / this.prms.h1.v)
                    v = this.prms.CdCunge.v * Cd * this.prms.L.v * Structure.R2G
                        * this.W * Math.pow(this.prms.h1.v, 0.5);
                    this.debug("StructureCunge80.Equation ORIFICE FREE Q=" + v);
                }
                break;
            case StructureFlowRegime.SUBMERGED:
                if (data.ENUM_StructureFlowMode === StructureFlowMode.WEIR) {
                    v = this.prms.CdCunge.v * this.prms.L.v * Structure.R2G * this.prms.h2.v
                        * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
                    this.debug("StructureCunge80.Equation WEIR SUBMERGED Q=" + v);
                } else {
                    v = this.prms.CdCunge.v * this.prms.L.v * Structure.R2G
                        * this.W * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
                    this.debug("StructureCunge80.Equation ORIFICE SUBMERGED Q=" + v);
                }
        }

        return new Result(v, this, data);
    }

    protected getFlowRegime(): StructureFlowRegime {
        if (this.prms.h1.v >= 1.5 * this.prms.h2.v) {
            return StructureFlowRegime.FREE;
        } else {
            return StructureFlowRegime.SUBMERGED;
        }
    }

    protected getFlowMode(): StructureFlowMode {
        const regime: StructureFlowRegime = this.getFlowRegime();
        if (regime === StructureFlowRegime.FREE) {
            if (this.W <= 2 / 3 * this.prms.h1.v) {
                return StructureFlowMode.ORIFICE;
            } else {
                return StructureFlowMode.WEIR;
            }
        } else {
            if (this.W <= this.prms.h2.v) {
                return StructureFlowMode.ORIFICE;
            } else {
                return StructureFlowMode.WEIR;
            }
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = ParamCalculability.DICHO;
        this.prms.CdCunge.calculability = ParamCalculability.DICHO;
    }
}
