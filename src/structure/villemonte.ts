/**
 *
 * @param h1 hauteur d'eau amont au dessus de la crête du seuil
 * @param h2 hauteur d'eau aval au dessus de la crête du seuil
 * @param n n est l'exposant dans les relations d'écoulement dénoyé :
 *          déversoir proportionnel : n=1 déversoir rectangulaire : n=1,5
 *          déversoir parabolique : n=2 déversoir triangulaire : n=2,5
 */
export function Villemonte(h1: number, h2: number, n: number): number {
    if (h1 === 0 || h2 > h1) {
        return 0;
    }
    if (n < 1 || n > 2.5) {
        throw new Error("Villemonte n doit être compris entre 1 et 2.5");
    }
    return Math.pow(1 - Math.pow(h2 / h1, n), 0.385);
}
