import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
export class RectangularStructureParams extends StructureParams {
    /** Width of the gate or length of the sill (m) */
    public L: ParamDefinition;

    /**
     * Discharge coefficients
     * @note Each implemented law must make visible its Cd parameter
     */
    // tslint:disable-next-line:variable-name
    public CdGR: ParamDefinition;
    // tslint:disable-next-line:variable-name
    public CdGRS: ParamDefinition;
    // tslint:disable-next-line:variable-name
    public CdWR: ParamDefinition;
    // tslint:disable-next-line:variable-name
    public CdWSL: ParamDefinition;
    // tslint:disable-next-line:variable-name
    public CdWS: ParamDefinition;
    // tslint:disable-next-line:variable-name
    public CdCunge: ParamDefinition;


    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rL    Largeur de la vanne ou du déversoir (m)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(rQ: number, rZDV: number, rZ1: number, rZ2: number, rL: number, rCd: number, rW: number = Infinity, nullParams: boolean = false) {
        super(rQ, rZDV, rZ1, rZ2, rW, nullParams);
        this.L = new ParamDefinition(this, "L", ParamDomainValue.POS, "m", rL, ParamFamily.WIDTHS, undefined, nullParams);
        this.addParamDefinition(this.L);
        const domainCd = new ParamDomain(ParamDomainValue.INTERVAL, 0, 10);
        this.CdGR = new ParamDefinition(this, "CdGR", domainCd, undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdGR);
        this.CdGRS = new ParamDefinition(this, "CdGRS", domainCd, undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdGRS);
        this.CdWR = new ParamDefinition(this, "CdWR", domainCd, undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdWR);
        this.CdWSL = new ParamDefinition(this, "CdWSL", domainCd, undefined, rCd, undefined, undefined, nullParams);
        this.addParamDefinition(this.CdWSL);
        this.CdWS = new ParamDefinition(this, "CdWS", domainCd, undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdWS);
        this.CdCunge = new ParamDefinition(this, "CdCunge", domainCd, undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdCunge);
    }
}
