import { LoiDebit } from "../internal_modules";
import { StructureVanLevParams } from "../internal_modules";
import { StructureWeirSubmergedLarinier } from "../internal_modules";

export class StructureVanLevLarinier extends StructureWeirSubmergedLarinier {

    constructor(prms: StructureVanLevParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.VanLevLarinier);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): StructureVanLevParams {
        return this._prms as StructureVanLevParams;
    }

    protected get isVanneLevante(): boolean {
        return true;
    }
}
