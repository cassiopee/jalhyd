import { CalculatorType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { SessionSettings } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { DeverParams } from "../internal_modules";
import { ParallelStructure } from "../internal_modules";
import { loiAdmissiblesDever, LoiDebit } from "../internal_modules";

export class Dever extends ParallelStructure {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        Ec: "m",
        V: "m/s"
    };

    private bQcorrected: boolean;
    private bZcorrected: boolean;

    constructor(prms: DeverParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Dever);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): DeverParams {
        return this._prms as DeverParams;
    }

    public getLoisAdmissibles(): { [key: string]: LoiDebit[]; } {
        return loiAdmissiblesDever;
    }

    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer :
     *                 "Q", "Z1", "Z2"
     *                 ou { uid: "abcdef", symbol: "X" } avec "abcdef" l'index de l'ouvrage et "X" son paramètre
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string | any, rInit?: number): Result {
        if (["Q", "Z1"].includes(sVarCalc)) {
            this.bQcorrected = true;
        } else {
            this.bQcorrected = false;
        }
        this.bZcorrected = true;
        this.prms.Q.v = (this.prms.Q.V !== undefined && !isNaN(this.prms.Q.V)) ? this.prms.Q.V : 0;
        const r = super.Calc(sVarCalc, rInit);
        if (!r.ok) {
            return r;
        }
        const QT = this.prms.Q.V;

        // Vérifier que les cotes de radier des ouvrages ne sont pas sous la cote de fond du lit
        for (const s of this.structures) {
            if (s.prms.ZDV.v < this.prms.ZR.v) {
                const m = new Message(MessageCode.WARNING_DEVER_ZDV_INF_ZR);
                m.extraVar.number = String(s.findPositionInParent() + 1); // String to avoid decimals
                r.resultElement.addMessage(m);
            }
        }

        if (this.prms.Z1.v > this.prms.ZR.v) {
            // Vitesse dans le cours d'eau amont
            r.resultElement.values.V = QT / (this.prms.BR.v * (this.prms.Z1.v - this.prms.ZR.v));
            // Energie cinétique dans le cours d'eau amont
            r.resultElement.values.Ec = r.resultElement.values.V * r.resultElement.values.V / (2 * 9.81);
            // Calcul de Cv
            this.bZcorrected = false;
            const Q0 = super.CalcQ().vCalc;
            r.resultElement.values.Cv = (Q0 > 0) ? QT / Q0 : 1;
        } else {
            r.resultElement.addMessage(new Message(MessageCode.WARNING_DEVER_ZR_SUP_Z1));
        }
        return r;
    }

    /**
     * Calcul de la somme des débits de chaque structure
     * @param iExcept Index de la structure à ne pas additionner (optionnel)
     */
    public CalcQ(iExcept?: number): Result {
        if (this.bQcorrected) {
            let r: Result;
            let i: number = SessionSettings.maxIterations;
            do {
                if (r?.ok) { this.prms.Q.v = r.vCalc; }
                r = super.CalcQ(iExcept);
                i--;
            } while (i && Math.abs(r.vCalc - this.prms.Q.v) > SessionSettings.precision);
            if (i === 0) {
                r = new Result(new Message(MessageCode.ERROR_DICHO_CONVERGE, { lastApproximation: r.vCalc }));
            }
            return r;
        } else {
            return super.CalcQ(iExcept);
        }
    }

    /**
     * Mise à jour de Z1, h1 pour tous les ouvrages
     */
    protected updateStructuresH1H2() {
        if (this.bZcorrected) {
            for (const structure of this.structures) {
                const rEc: number = (this.prms.Z1.v - this.prms.ZR.v > 0) ?
                    0.5 * this.prms.Q.v / ((this.prms.Z1.v - this.prms.ZR.v) * this.prms.BR.v * 9.81) : 0;
                structure.prms.Z1.v = this.prms.Z1.v + rEc;
                structure.prms.Z2.v = this.prms.Z2.v;
                structure.prms.update_h1h2();
            }
        } else {
            super.updateStructuresH1H2();
        }
    }

    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.BR.calculability = ParamCalculability.FREE;
        this.prms.ZR.calculability = ParamCalculability.FREE;
        // On supprime Z2 de l'interface car régime dénoyé uniquement
        this.prms.Z2.calculability = ParamCalculability.FIXED;
        this.prms.Z2.visible = false;
    }

    public static override resultsUnits() {
        return Dever._resultsUnits;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            V: undefined,
            Ec: undefined,
            Cv: undefined
        };
    }

}
