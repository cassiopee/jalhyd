import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { Structure, StructureFlowMode } from "../internal_modules";
import { TriangularStructureParams } from "../internal_modules";

/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
export class StructureTriangularWeirFree extends Structure {

    constructor(prms: TriangularStructureParams, dbg: boolean = false) {
        super(prms, dbg);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): TriangularStructureParams {
        return this._prms as TriangularStructureParams;
    }

    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    public CalcQ(): Result {
        const data = this.getResultData();

        const Q = this.prms.CdT.v * this.getTanFromDegrees(this.prms.alpha2.v)
            * Math.pow(this.prms.h1.v, 2.5);

        return new Result(Q, this, data);
    }

    protected getFlowMode() {
        return StructureFlowMode.WEIR;
    }

    /**
     * Compute Tangent with angle in degrees
     * @param degrees Angle (degrees)
     */
    protected getTanFromDegrees(degrees: number) {
        return Math.tan(degrees * Math.PI / 180);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.alpha2.calculability = ParamCalculability.DICHO;
        this.prms.CdT.calculability = ParamCalculability.DICHO;
    }
}
