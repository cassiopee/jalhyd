import { LoiDebit } from "../internal_modules";
import { StructureVanLevParams } from "../internal_modules";
import { StructureWeirVillemonte } from "../internal_modules";

export class StructureVanLevVillemonte extends StructureWeirVillemonte {

    constructor(prms: StructureVanLevParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setLoiDebit(LoiDebit.VanLevVillemonte);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): StructureVanLevParams {
        return this._prms as StructureVanLevParams;
    }

    protected get isVanneLevante(): boolean {
        return true;
    }
}
