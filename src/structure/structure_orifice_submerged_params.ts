import { ParamDefinition } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { StructureParams } from "../internal_modules";

/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
export class StructureOrificeSubmergedParams extends StructureParams {

    /** Area of the orifice (m2) */
    public S: ParamDefinition;

    /** Discharge coefficient */
    // tslint:disable-next-line:variable-name
    public CdO: ParamDefinition;

    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rCd   Coefficient de débit (-)
     * @param rS    Surface de l'orifice (m2)
     */
    constructor(rQ: number, rZ1: number, rZ2: number, rCd: number, rS: number, nullParams: boolean = false) {
        super(rQ, 100, rZ1, rZ2, undefined, nullParams);
        this.S = new ParamDefinition(this, "S", ParamDomainValue.POS_NULL, "m²", rS, undefined, undefined, nullParams);
        this.addParamDefinition(this.S);
        this.CdO = new ParamDefinition(this, "CdO", new ParamDomain(ParamDomainValue.INTERVAL, 0, 10), undefined, rCd, undefined, undefined, false);
        this.addParamDefinition(this.CdO);
        // hide params
        this.ZDV.visible = false;
    }

    /**
     * Mise à jour de h1 et h2
     */
    public update_h1h2() {
        // Inutile pour cette équation qui ne fait pas intervenir ces variables
    }

}
