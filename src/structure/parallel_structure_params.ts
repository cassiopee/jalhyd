import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Common parameters of hydraulic structure equations
 */
export class ParallelStructureParams extends ParamsEquation {
    /** Débit (m3/s) */
    public Q: ParamDefinition;

    /** Cote de l'eau amont (m) */
    public Z1: ParamDefinition;

    /** Cote de l'eau aval (m) */
    public Z2: ParamDefinition;

    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZ2 Cote de l'eau aval (m)
     */
    constructor(rQ: number, rZ1: number, rZ2: number, nullParams: boolean = false) {
        super();
        this.Q = new ParamDefinition(this, "Q", ParamDomainValue.ANY, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.addParamDefinition(this.Q);
        this.Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z1);
        this.Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z2);
    }
}
