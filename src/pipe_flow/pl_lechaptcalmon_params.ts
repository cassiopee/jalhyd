import { ParamDefinition } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { PressureLossLawParams } from "../internal_modules";

/**
 * paramètres pour le calcul Lechapt et Calmon
 */
export class PL_LechaptCalmonParams extends PressureLossLawParams {

    /** Paramètre de rugosité L */
    private _L: ParamDefinition;

    /** Paramètre de rugosité M */
    private _M: ParamDefinition;

    /** Paramètre de rugosité N */
    private _N: ParamDefinition;

    constructor(rL: number, rM: number, rN: number, nullParams: boolean = false) {
        super();
        this._L = new ParamDefinition(this, "L", new ParamDomain(ParamDomainValue.INTERVAL, 0.8, 2), undefined, rL,
            undefined, false, nullParams);
        this._M = new ParamDefinition(this, "M", new ParamDomain(ParamDomainValue.INTERVAL, 1.5, 2.5), undefined, rM,
            undefined, false, nullParams);
        this._N = new ParamDefinition(this, "N", new ParamDomain(ParamDomainValue.INTERVAL, 4.5, 5.5), undefined, rN,
            undefined, false, nullParams);

        this.addParamDefinition(this._L);
        this.addParamDefinition(this._M);
        this.addParamDefinition(this._N);
    }

    get L() {
        return this._L;
    }

    get M() {
        return this._M;
    }

    get N() {
        return this._N;
    }
}
