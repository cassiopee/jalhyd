import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { PressureLossLawParams } from "../internal_modules";

/**
 * paramètres pour le calcul de la loi de Strickler
 */
export class PL_StricklerParams extends PressureLossLawParams {

    /** coefficient de rugosité de Strickler Ks */
    private _Ks: ParamDefinition;

    constructor(rKs: number, nullParams: boolean = false) {
        super();
        this._Ks = new ParamDefinition(this, "Ks", ParamDomainValue.POS, "SI", rKs, ParamFamily.STRICKLERS, undefined, nullParams);
        this.addParamDefinition(this._Ks);
    }

    get Ks() {
        return this._Ks;
    }
}
