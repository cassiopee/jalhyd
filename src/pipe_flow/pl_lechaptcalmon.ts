import { CalculatorType, PressureLossParams } from "../internal_modules";
import { LCMaterial } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { PL_LechaptCalmonParams } from "../internal_modules";
import { PressureLossLaw, PressureLossType } from "../internal_modules";

/**
 * Calcul des pertes de charge dans un tube à partir des tables de Lechapt et Calmon
 */
export class PL_LechaptCalmon extends PressureLossLaw implements Observer {

    private static _materials: { L: number, M: number, N: number, title: string }[] = [
        {
            L: 1.863,
            M: 2,
            N: 5.33,
            title: "Unlined cast iron - Coarse concrete (corrosive water)"
        },
        {
            L: 1.601,
            M: 1.975,
            N: 5.25,
            title: "Cast steel or uncoated - Coarse concrete (somewhat corrosive water)"
        },
        {
            L: 1.40,
            M: 1.96,
            N: 5.19,
            title: "Cast steel or cement coating"
        },
        {
            L: 1.16,
            M: 1.93,
            N: 5.11,
            title: "Cast iron or steel coating bitumen - Centrifuged concrete"
        },
        {
            L: 1.1,
            M: 1.89,
            N: 5.01,
            title: "Rolled steel - Smooth concrete"
        },
        {
            L: 1.049,
            M: 1.86,
            N: 4.93,
            title: "Cast iron or steel coating centrifuged"
        },
        {
            L: 1.01,
            M: 1.84,
            N: 4.88,
            title: "PVC - Polyethylene"
        },
        {
            L: 0.916,
            M: 1.78,
            N: 4.78,
            title: "Hydraulically smooth pipe - 0.05 ≤ D ≤ 0.2"
        },
        {
            L: 0.971,
            M: 1.81,
            N: 4.81,
            title: "Hydraulically smooth pipe - 0.25 ≤ D ≤ 1"
        }
    ];

    constructor(prms: PL_LechaptCalmonParams, dbg: boolean = false) {
        super(prms, dbg);
        this.pressureLossType = PressureLossType.LechaptCalmon;
        this._props.addObserver(this);
        this.material = LCMaterial.PVCPolyethylene;
        this._intlType = "LechaptCalmon";
    }

    public static get materials() {
        return PL_LechaptCalmon._materials;
    }

    public get material(): LCMaterial {
        return this.getPropValue("material");
    }

    public set material(m: LCMaterial) {
        this.setPropValue("material", m);
    }

    public Equation(sVarCalc: string): Result {
        if (sVarCalc !== undefined && sVarCalc !== "Jlin") {
            throw new Error("PL_LechaptCalmon.Equation() : invalid variable name " + sVarCalc);
        }
        const r: Result = this._result;

        // récupération des paramètres Q,D et Lg dans le parent
        const parentPrms = this.parent.prms as PressureLossParams;
        const Q = parentPrms.Q.v;
        const D = parentPrms.D.v;
        const Lg = parentPrms.Lg.v;

        // Calcul de la perte de charge linéaire spécifique à chaque module de perte de charge.
        r.values.Jlin = this.prms.L.v * Math.pow(Q, this.prms.M.v) / Math.pow(D, this.prms.N.v) * (Lg / 1000);

        return r;
    }

    public finalChecks(r: Result) {
        if (r.values.V < 0.4 || r.values.V > 2) {
            r.resultElement.log.add(new Message(MessageCode.WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2));
        }
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PL_LechaptCalmonParams {
        return this._prms as PL_LechaptCalmonParams;
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (data.action === "propertyChange" && data.name === "material") {
            this.applyMaterialPreset();
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.L.calculability = ParamCalculability.FIXED;
        this.prms.M.calculability = ParamCalculability.FIXED;
        this.prms.N.calculability = ParamCalculability.FIXED;
    }

    protected exposeResults() {
        this._resultsFamilies = {
            Klin: undefined,
            fD: undefined,
            V: undefined,
            Jlin: undefined
        };
    }

    /**
     * Modifies values of L, M, N depending on the given material,
     * according to this._materials presets
     */
    private applyMaterialPreset() {
        const m = this.getPropValue("material");
        const values = PL_LechaptCalmon._materials[m];
        this.prms.L.singleValue = values.L;
        this.prms.M.singleValue = values.M;
        this.prms.N.singleValue = values.N;
    }

    // interface IProperties

    public hasProperty(key: string): boolean {
        if (super.hasProperty(key)) {
            return true;
        }

        return key === "material";
    }
}
