import { PL_StricklerParams, PressureLossParams, PressureLossType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { PressureLossLaw } from "../internal_modules";

/**
 * Calcul des pertes de charge par la loi de Strickler
 */
export class PL_Strickler extends PressureLossLaw {

    constructor(prms: PL_StricklerParams, dbg: boolean = false) {
        super(prms, dbg);
        this.pressureLossType = PressureLossType.Strickler;
        this._intlType = "Strickler";
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PL_StricklerParams {
        return this._prms as PL_StricklerParams;
    }

    public Equation(sVarCalc: string): Result {
        if (sVarCalc !== undefined && sVarCalc !== "Jlin") {
            throw new Error("PL_Strickler.Equation() : invalid variable name " + sVarCalc);
        }
        const r: Result = this._result;

        // récupération des paramètres Q,D et Lg dans le parent
        const parentPrms = this.parent.prms as PressureLossParams;
        const Q = parentPrms.Q.v;
        const D = parentPrms.D.v;
        const Lg = parentPrms.Lg.v;

        r.values.Jlin = Lg * Math.pow(Q, 2)
            / Math.pow(this.prms.Ks.v * Math.PI * Math.pow(D, 2) / 4, 2)
            / Math.pow(D / 4, 4 / 3);

        return r;
    }

    protected setParametersCalculability(): void {
        this.prms.Ks.calculability = ParamCalculability.DICHO;
    }
}
