import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * paramètres pour la conduite distributrice
 */
export class ConduiteDistribParams extends ParamsEquation {
    /** Débit */
    public Q: ParamDefinition;

    /** Diamètre */
    public D: ParamDefinition;

    /** Perte de charge */
    public J: ParamDefinition;

    /** Longueur de la conduite */
    // tslint:disable-next-line:variable-name
    public Lg: ParamDefinition;

    /** Viscosité dynamique nu */
    // tslint:disable-next-line:variable-name
    public Nu: ParamDefinition;

    constructor(rQ: number, rD: number, rJ: number, rLg: number, rNu: number, nullParams: boolean = false) {
        super();
        this.Q = new ParamDefinition(this, "Q", ParamDomainValue.POS, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.D = new ParamDefinition(this, "D", ParamDomainValue.POS, "m", rD, ParamFamily.DIAMETERS, undefined, nullParams);
        this.J = new ParamDefinition(this, "J", ParamDomainValue.POS, "m", rJ, undefined, undefined, nullParams);
        this.Lg = new ParamDefinition(this, "Lg", ParamDomainValue.POS, "m", rLg, ParamFamily.LENGTHS, undefined, nullParams);
        this.Nu = new ParamDefinition(this, "Nu", ParamDomainValue.POS, "Pa·s", rNu, undefined, undefined, nullParams);

        this.addParamDefinition(this.Q);
        this.addParamDefinition(this.D);
        this.addParamDefinition(this.J);
        this.addParamDefinition(this.Lg);
        this.addParamDefinition(this.Nu);
    }
}
