import { Nub, ParamCalculability, ParamDefinition } from "../internal_modules";
import { Result } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { PressureLossParams } from "../internal_modules";
import { PressureLossLaw } from "../internal_modules";

export class PressureLoss extends Nub {

    /**
     * { symbol => string } map that defines units for extra results
     */
    private static _resultsUnits = {
        Q: "m³/s",
        D: "m",
        J: "m/m",
        Lg: "m"
    };

    constructor(prms: PressureLossParams, law: PressureLossLaw, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PressureLoss);
        this.calculatedParam = prms.J;
        this.setLaw(law);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PressureLossParams {
        return this._prms as PressureLossParams;
    }

    public get child(): PressureLossLaw {
        return this._children[0] as PressureLossLaw;
    }

    public getParameter(name: string): ParamDefinition {
        if (typeof name !== "string") {
            // dirty hack because calculated param descriptor for section params is an object { uid: , symbol: }
            name = (name as any).symbol;
        }

        return super.getParameter(name); // includes child parameters
    }

    /**
     * Set/replaces the current section
     */
    public setLaw(pll: PressureLossLaw) {
        if (pll) {
            // prevent index out of bounds at first time
            if (this._children.length === 0) {
                this._children[0] = undefined;
            }
            this.replaceChild(0, pll);
            this._props.removeProp("pressureLossType"); // because this property belongs to child pressure loss law and is set as long as law is not defined
            this._props.removeProp("material"); // because this property belongs to child Lechapt-Calmon law and is set as long as law is not defined
            /**
             * Y linkability managed in child classes through 'visible' flag
             * @see Bief,CourbeRemous
             */
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.Q.calculability = ParamCalculability.DICHO;
        this.prms.D.calculability = ParamCalculability.DICHO;
        this.prms.J.calculability = ParamCalculability.EQUATION;
        this.prms.Lg.calculability = ParamCalculability.DICHO;
        this.prms.Kloc.calculability = ParamCalculability.DICHO;
    }

    public Calc(sVarCalc: any, rInit?: number): Result {
        if (typeof sVarCalc !== "string") {
            // sVarCalc may be a calculated parameter descriptor (cf. Nub.calculatedParamDescriptor)
            if (sVarCalc.uid !== this.uid && sVarCalc.uid !== this.child.uid) {
                throw new Error("invalid nub");
            }
            sVarCalc = sVarCalc.symbol;
        }

        const r: Result = super.Calc(sVarCalc, rInit);

        const V2 = Math.pow(r.values.V, 2);
        const Jlin = this.child.result.values.Jlin;
        r.values.Klin = 19.62 * Jlin / V2;
        r.values.fD = this.prms.J.V / this.prms.Lg.V / V2 * 19.62 * this.prms.D.V;

        this.child.finalChecks(r);

        return r;
    }

    public Equation(sVarCalc: string): Result {
        if (sVarCalc !== "J") {
            throw new Error("PressureLoss.Equation() : invalid variable name " + sVarCalc);
        }

        if (this._result === undefined) {
            this.initNewResultElement();
        }

        const r: Result = this._result;

        // vitesse
        r.values.V = this.prms.Q.v / (Math.PI * Math.pow(this.prms.D.v / 2, 2));

        // perte de charge linéaire propre à chaque module
        const rc: Result = this.child.Equation("Jlin");
        if (!rc.ok) {
            r.addLog(rc.log);
            return r;
        }

        // perte de charge totale (J)
        r.vCalc = rc.values.Jlin + this.prms.Kloc.v / 19.62 * Math.pow(r.values.V, 2);

        return r;
    }

    public get result(): Result {
        return this._result;  // redéfini (à l'identique) car on a redéfini le setter (et un setter seul fait que le getter n'est pas hérité)
    }

    public set result(r: Result) {
        throw new Error("PressureLoss.set result() called!");
    }

    public static override resultsUnits() {
        return PressureLoss._resultsUnits;
    }

    // interface IProperties

    public getPropValue(key: string): any {
        if (this.childHasProperty(key)) {
            return this.child.getPropValue(key);
        }
        return this._props.getPropValue(key);
    }

    public setPropValue(key: string, val: any, sender?: any): boolean {
        if (this.childHasProperty(key)) {
            return this.child.setPropValue(key, val, sender);
        }
        return this._props.setPropValue(key, val, sender);
    }
}
