import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PressureLossParams extends ParamsEquation {
    /** Débit */
    private _Q: ParamDefinition;

    /** Diamètre */
    private _D: ParamDefinition;

    /** Perte de charge */
    private _J: ParamDefinition;

    /** Longueur de la conduite */
    private _Lg: ParamDefinition;

    /** Coefficient de perte de charge singulière Kloc */
    private _Kloc: ParamDefinition;

    constructor(rQ: number, rD: number, rJ: number, rLg: number, rKloc: number, nullParams: boolean = false) {
        super();
        this._Q = new ParamDefinition(this, "Q", ParamDomainValue.POS, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this._D = new ParamDefinition(this, "D", new ParamDomain(ParamDomainValue.INTERVAL, 0, 20), "m", rD, ParamFamily.DIAMETERS, undefined, nullParams);
        this._J = new ParamDefinition(this, "J", ParamDomainValue.POS_NULL, "m", rJ, undefined, undefined, nullParams);
        this._Lg = new ParamDefinition(this, "Lg", ParamDomainValue.POS, "m", rLg, ParamFamily.LENGTHS, undefined, nullParams);
        this._Kloc = new ParamDefinition(this, "Kloc", ParamDomainValue.POS_NULL, undefined, rKloc, undefined, undefined, nullParams);

        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._D);
        this.addParamDefinition(this._J);
        this.addParamDefinition(this._Lg);
        this.addParamDefinition(this._Kloc);
    }

    get Q() {
        return this._Q;
    }

    get D() {
        return this._D;
    }

    get J() {
        return this._J;
    }

    get Lg() {
        return this._Lg;
    }

    get Kloc() {
        return this._Kloc;
    }
}
