import { ParamDefinition, ParamDomain, ParamDomainValue, ParamsEquation } from "../internal_modules";

/**
 * generic pressure loss law parameters
 */
export class PressureLossLawParams extends ParamsEquation {
}
