import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Result } from "../internal_modules";
import { ConduiteDistribParams } from "../internal_modules";

/**
 * classe de calcul sur la conduite distributrice
 */
export class ConduiteDistrib extends Nub {

    constructor(prms: ConduiteDistribParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.ConduiteDistributrice);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): ConduiteDistribParams {
        return this._prms as ConduiteDistribParams;
    }

    /**
     * calcul direct des différents paramètres
     * @param sVarCalc nom du paramètre
     * @return valeur calculée
     */
    public Equation(sVarCalc: string): Result {
        let v: number;

        const K = 0.3164 * Math.pow(4, 1.75) / (5.5 * 9.81 * Math.pow(3.1415, 1.75)); // Constante de la formule

        switch (sVarCalc) {
            case "J":
                v = K * Math.pow(this.prms.Nu.v, 0.25) * Math.pow(this.prms.Q.v, 1.75)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75);
                break;

            case "D":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * Math.pow(this.prms.Q.v, 1.75) * this.prms.Lg.v), 1 / 4.75);
                break;

            case "Q":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75)), 1 / 1.75);
                break;

            case "Lg":
                v = this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * Math.pow(this.prms.Q.v, 1.75) / Math.pow(this.prms.D.v, 4.75));
                break;

            case "Nu":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Q.v, 1.75)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75)), 1 / 0.25);
                break;

            default:
                throw new Error("ConduiteDistrib.Equation() : invalid parameter name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.J.calculability = ParamCalculability.EQUATION;
        this.prms.D.calculability = ParamCalculability.EQUATION;
        this.prms.Q.calculability = ParamCalculability.EQUATION;
        this.prms.Lg.calculability = ParamCalculability.EQUATION;
        this.prms.Nu.calculability = ParamCalculability.EQUATION;
    }

}
