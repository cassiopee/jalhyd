import { CalculatorType, ChildNub, Nub, PressureLossLawParams } from "../internal_modules";
import { Result } from "../internal_modules";

/**
 * Lois de perte de charge
 */
export enum PressureLossType {
    LechaptCalmon,
    Strickler
}

/**
 * generic pressure loss law (analogous to acSection with respect to SectionParametree nub)
 */
export abstract class PressureLossLaw extends ChildNub {
    constructor(prms: PressureLossLawParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PressureLossLaw);
    }

    public get pressureLossType(): PressureLossType {
        return this.getPropValue("pressureLossType");
    }

    public set pressureLossType(plt: PressureLossType) {
        this.setPropValue("pressureLossType", plt);
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PressureLossLawParams {
        return this._prms as PressureLossLawParams;
    }

    /**
     * vérifications post-calcul
     */
    public finalChecks(r: Result) {
    }

    // interface IProperties

    public hasProperty(key: string): boolean {
        return key === "pressureLossType";
    }
}
