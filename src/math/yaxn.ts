import { ChildNub } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { YAXNParams } from "../internal_modules";

/**
 * Y = A.X^N
 */
export class YAXN extends ChildNub {

    constructor(prms: YAXNParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.YAXN);
        this._defaultCalculatedParam = prms.Y;
        this._intlType = "Puissance";
        this.resetDefaultCalculatedParam();
    }

    /** paramètres castés au bon type */
    get prms(): YAXNParams {
        return this._prms as YAXNParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Y":
                if (this.prms.X.v < 0 && !Number.isInteger(this.prms.N.v)) {
                    const m = new Message(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
                    m.extraVar.X = this.prms.X.v;
                    m.extraVar.N = this.prms.N.v;
                    return new Result(m);
                }
                v = this.prms.A.v * Math.pow(this.prms.X.v, this.prms.N.v);
                break;

            default:
                throw new Error("YAXN.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /** paramétrage de la calculabilité des paramètres */
    protected setParametersCalculability() {
        this.prms.Y.calculability = ParamCalculability.EQUATION;
        this.prms.X.calculability = ParamCalculability.DICHO;
    }

}
