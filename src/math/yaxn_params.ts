import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Y = A.X^N
 */
export class YAXNParams extends ParamsEquation {

    /** Y */
    private _Y: ParamDefinition;

    /** A */
    private _A: ParamDefinition;

    /** X */
    private _X: ParamDefinition;

    /** N */
    private _N: ParamDefinition;

    constructor(rA: number, rX: number, rN: number, rY?: number, nullParams: boolean = false) {
        super();
        this._A = new ParamDefinition(this, "A", ParamDomainValue.ANY, undefined, rA, ParamFamily.ANY, undefined, nullParams);
        this._X = new ParamDefinition(this, "X", ParamDomainValue.ANY, undefined, rX, ParamFamily.ANY, undefined, nullParams);
        this._N = new ParamDefinition(this, "N", ParamDomainValue.ANY, undefined, rN, ParamFamily.ANY, undefined, nullParams);
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, undefined, rY, undefined, false, nullParams);

        this.addParamDefinition(this._A);
        this.addParamDefinition(this._X);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._Y);
    }

    get Y() {
        return this._Y;
    }

    get A() {
        return this._A;
    }

    get X() {
        return this._X;
    }

    get N() {
        return this._N;
    }
}
