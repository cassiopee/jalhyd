import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { SPPParams } from "../internal_modules";
import { YAXN } from "../internal_modules";

export enum SPPOperation {
    SUM,        // Somme
    PRODUCT     // Produit
}

/**
 * Somme et produit de puissances
 * Y = Σ ou π (a1.x1^p1, … an.xn^pn)
 */
export class SPP extends Nub {

    constructor(prms: SPPParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.SPP);
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
        this.operation = SPPOperation.SUM;
    }

    /** paramètres castés au bon type */
    get prms(): SPPParams {
        return this._prms as SPPParams;
    }

    public get operation(): SPPOperation {
        return this.getPropValue("sppOperation");
    }

    public set operation(o: SPPOperation) {
        this.setPropValue("sppOperation", o);
    }

    /**
     * @param sVarCalc Nom du paramètre à calculer : "Y", ou
     *  { uid: "abcdef", symbol: "X" } avec "abcdef" l'UID du module YAXN et "X" son paramètre
     * @param rInit Valeur initiale
     */
    public Calc(sVarCalc: string | any, rInit?: number): Result {
        // if Calc() is called outside of CalcSerie(), _result might not be initialized
        if (!this.result) {
            this.initNewResultElement();
        }
        switch (sVarCalc) {
            case "Y":
                this.currentResultElement = super.Calc(sVarCalc, rInit);
                if (this.result.ok) {
                    this.getParameter(sVarCalc).v = this.result.resultElement.vCalc;
                }
                break;
            default:
                if (typeof sVarCalc === "string") {
                    throw new Error("SPP.Calc() : unknow parameter to calculate " + sVarCalc);
                }

                let vPartielle;
                // 1. calcul de la somme / du produit des modules YAXN, sauf celui qui est en calcul;
                // 2. soustraction / division de Y par le résultat de 1.
                if (this.operation === SPPOperation.SUM) {
                    vPartielle = 0;
                    for (const c of this._children) {
                        if (c.uid !== sVarCalc.uid) {
                            const cRes = c.Calc("Y");
                            if (cRes.ok) {
                                vPartielle += cRes.vCalc;
                            } else {
                                const m = new Message(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                                m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                                this.result.log.add(m);
                                this.result.log.addLog(cRes.log);
                                return this.result;
                            }
                        }
                    }
                    vPartielle = this.prms.Y.v - vPartielle;
                } else if (this.operation === SPPOperation.PRODUCT) {
                    vPartielle = 1;
                    for (const c of this._children) {
                        if (c.uid !== sVarCalc.uid) {
                            const cRes = c.Calc("Y");
                            if (cRes.ok) {
                                vPartielle *= cRes.vCalc;
                            } else {
                                const m = new Message(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                                m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                                this.result.log.add(m);
                                this.result.log.addLog(cRes.log);
                                return this.result;
                            }
                        }
                    }
                    if (vPartielle === 0) {
                        const m = new Message(MessageCode.ERROR_DIVISION_BY_ZERO);
                        m.extraVar.symbol = "vPartielle";
                        this.result.resultElement.addMessage(m);
                        return this.result;
                    }
                    vPartielle = this.prms.Y.v / vPartielle;
                }

                // 3. injection de vPartielle dans le Y du module YAXN en calcul
                const yaxnEnCalcul = this.getChildren()[this.getIndexForChild(sVarCalc.uid)] as YAXN;
                yaxnEnCalcul.prms.Y.v = vPartielle;
                const r: Result = yaxnEnCalcul.Calc(sVarCalc.symbol, rInit); // sVarCalc.symbol should always be "X"

                // "copy" result
                this.result.symbol = r.symbol;
                this.result.vCalc = r.vCalc;
                this.result.log.addLog(r.log);
                this.result.globalLog.addLog(r.globalLog);
        }

        return this.result;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Y":
                if (this.operation === SPPOperation.SUM) {
                    v = 0;
                    for (const c of this._children) {
                        const cRes = c.Calc("Y");
                        if (cRes.ok) {
                            v += cRes.vCalc;
                        } else {
                            const m = new Message(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                            m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                            this.result.log.add(m);
                            this.result.log.addLog(cRes.log);
                            return this.result;
                        }
                    }
                } else if (this.operation === SPPOperation.PRODUCT) {
                    v = 1;
                    for (const c of this._children) {
                        const cRes = c.Calc("Y");
                        if (cRes.ok) {
                            v *= cRes.vCalc;
                        } else {
                            const m = new Message(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                            m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                            this.result.log.add(m);
                            this.result.log.addLog(cRes.log);
                            return this.result;
                        }
                    }
                }
                break;

            default:
                throw new Error("YAXN.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /** paramétrage de la calculabilité des paramètres */
    protected setParametersCalculability() {
        this.prms.Y.calculability = ParamCalculability.EQUATION;
    }

}
