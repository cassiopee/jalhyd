import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";
import { Result } from "../internal_modules";
import { YAXBParams } from "../internal_modules";

/**
 * Y = A.X + B
 */
export class YAXB extends Nub {

    constructor(prms: YAXBParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.YAXB);
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
    }

    /** paramètres castés au bon type */
    get prms(): YAXBParams {
        return this._prms as YAXBParams;
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Y":
                v = this.prms.A.v * this.prms.X.v + this.prms.B.v;
                break;

            case "A":
                if (this.prms.X.v === 0) {
                    const m = new Message(MessageCode.ERROR_DIVISION_BY_ZERO);
                    m.extraVar.symbol = "X";
                    return new Result(m);
                }
                v = (this.prms.Y.v - this.prms.B.v) / this.prms.X.v;
                break;

            case "X":
                if (this.prms.A.v === 0) {
                    const m = new Message(MessageCode.ERROR_DIVISION_BY_ZERO);
                    m.extraVar.symbol = "A";
                    return new Result(m);
                }
                v = (this.prms.Y.v - this.prms.B.v) / this.prms.A.v;
                break;

            case "B":
                v = this.prms.Y.v - (this.prms.A.v * this.prms.X.v);
                break;

            default:
                throw new Error("YAXB.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    /** paramétrage de la calculabilité des paramètres */
    protected setParametersCalculability() {
        this.prms.Y.calculability = ParamCalculability.EQUATION;
        this.prms.A.calculability = ParamCalculability.EQUATION;
        this.prms.X.calculability = ParamCalculability.EQUATION;
        this.prms.B.calculability = ParamCalculability.EQUATION;
    }

}
