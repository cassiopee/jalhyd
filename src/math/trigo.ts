import { CalculatorType } from "../internal_modules";
import { Nub } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { ParamDomain, ParamDomainValue } from "../internal_modules";
import { Observer } from "../internal_modules";
import { Result } from "../internal_modules";
import { TrigoParams } from "../internal_modules";

export enum TrigoOperation {
    COS,
    SIN,
    TAN,
    COSH,
    SINH,
    TANH
}

export enum TrigoUnit {
    DEG,    // Degrees
    RAD     // Radians
}

/**
 * cos, sin, tan, cosh, sinh, tanh
 */
export class Trigo extends Nub implements Observer {

    constructor(prms: TrigoParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.Trigo);
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
        this._props.addObserver(this);
        this.operation = TrigoOperation.COS;
        this.unit = TrigoUnit.DEG;
    }

    /** paramètres castés au bon type */
    get prms(): TrigoParams {
        return this._prms as TrigoParams;
    }

    public get operation(): TrigoOperation {
        return this.getPropValue("trigoOperation");
    }

    public set operation(o: TrigoOperation) {
        this.setPropValue("trigoOperation", o);
    }

    public get unit(): TrigoUnit {
        return this.getPropValue("trigoUnit");
    }

    public set unit(u: TrigoUnit) {
        this.setPropValue("trigoUnit", u);
    }

    public Equation(sVarCalc: string): Result {
        let v: number;

        switch (sVarCalc) {
            case "Y":
                let xv = this.prms.X.v;
                if (this.unit === TrigoUnit.DEG) {
                    xv = xv * Math.PI / 180;
                }
                switch (this.operation) {
                    case TrigoOperation.COS:
                        v = Math.cos(xv);
                        break;
                    case TrigoOperation.SIN:
                        v = Math.sin(xv);
                        break;
                    case TrigoOperation.TAN:
                        v = Math.tan(xv);
                        break;
                    case TrigoOperation.COSH:
                        v = Math.cosh(xv);
                        break;
                    case TrigoOperation.SINH:
                        v = Math.sinh(xv);
                        break;
                    case TrigoOperation.TANH:
                        v = Math.tanh(xv);
                        break;
                    default:
                        throw new Error("Trigo.Equation() : invalid operation " + this.operation);
                }
                break;

            case "X":
                const yv = this.prms.Y.v;
                switch (this.operation) {
                    case TrigoOperation.COS:
                        v = Math.acos(yv);
                        break;
                    case TrigoOperation.SIN:
                        v = Math.asin(yv);
                        break;
                    case TrigoOperation.TAN:
                        v = Math.atan(yv);
                        break;
                    case TrigoOperation.COSH:
                        v = Math.acosh(yv);
                        break;
                    case TrigoOperation.SINH:
                        v = Math.asinh(yv);
                        break;
                    case TrigoOperation.TANH:
                        v = Math.atanh(yv);
                        break;
                    default:
                        throw new Error("Trigo.Equation() : invalid operation " + this.operation);
                }
                if (this.unit === TrigoUnit.DEG) {
                    v = v / Math.PI * 180;
                }
                break;

            default:
                throw new Error("Trigo.Equation() : invalid variable name " + sVarCalc);
        }

        return new Result(v, this);
    }

    // interface Observer

    public update(sender: any, data: any) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "trigoOperation":
                    // adjust definition domains
                    const un = this.unit;
                    switch (this.operation) {
                        case TrigoOperation.COS:
                        case TrigoOperation.SIN:
                            // [-1,1]
                            this.prms.Y.setDomain(new ParamDomain(ParamDomainValue.INTERVAL, -1, 1));
                            break;
                        case TrigoOperation.TANH:
                            // ]-1,1[
                            // @TODO manage boundaries exclusion in Interval
                            this.prms.Y.setDomain(new ParamDomain(ParamDomainValue.INTERVAL, -0.99999999, 0.99999999));
                            break;
                        case TrigoOperation.TAN:
                        case TrigoOperation.SINH:
                            // R
                            this.prms.Y.setDomain(ParamDomainValue.ANY);
                            break;
                        case TrigoOperation.COSH:
                            // [1,+∞[
                            this.prms.Y.setDomain(new ParamDomain(ParamDomainValue.INTERVAL, 1, Infinity));
                            break;
                    }
                    break;
            }
        }
    }

    /** paramétrage de la calculabilité des paramètres */
    protected setParametersCalculability() {
        this.prms.Y.calculability = ParamCalculability.EQUATION;
        this.prms.X.calculability = ParamCalculability.EQUATION;
    }

}
