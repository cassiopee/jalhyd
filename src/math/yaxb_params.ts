import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Y = A.X + B
 */
export class YAXBParams extends ParamsEquation {

    /** Y */
    private _Y: ParamDefinition;

    /** A */
    private _A: ParamDefinition;

    /** X */
    private _X: ParamDefinition;

    /** B */
    private _B: ParamDefinition;

    constructor(rY: number, rA: number, rX: number, rB: number, nullParams: boolean = false) {
        super();
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, undefined, rY, ParamFamily.ANY, undefined, nullParams);
        this._A = new ParamDefinition(this, "A", ParamDomainValue.ANY, undefined, rA, ParamFamily.ANY, undefined, nullParams);
        this._X = new ParamDefinition(this, "X", ParamDomainValue.ANY, undefined, rX, ParamFamily.ANY, undefined, nullParams);
        this._B = new ParamDefinition(this, "B", ParamDomainValue.ANY, undefined, rB, ParamFamily.ANY, undefined, nullParams);

        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._A);
        this.addParamDefinition(this._X);
        this.addParamDefinition(this._B);
    }

    get Y() {
        return this._Y;
    }

    get A() {
        return this._A;
    }

    get X() {
        return this._X;
    }

    get B() {
        return this._B;
    }
}
