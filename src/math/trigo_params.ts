import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Y = f(X)
 */
export class TrigoParams extends ParamsEquation {

    /** Y */
    private _Y: ParamDefinition;

    /** X */
    private _X: ParamDefinition;

    constructor(rY: number, rX: number, nullParams: boolean = false) {
        super();
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, undefined, rY, ParamFamily.ANY, undefined, nullParams);
        this._X = new ParamDefinition(this, "X", ParamDomainValue.ANY, undefined, rX, ParamFamily.ANY, undefined, nullParams);

        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._X);
    }

    get Y() {
        return this._Y;
    }

    get X() {
        return this._X;
    }
}
