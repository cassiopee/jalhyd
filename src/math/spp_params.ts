import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

/**
 * Y = Σ ou π (a1.x1^p1, … an.xn^pn)
 */
export class SPPParams extends ParamsEquation {

    /** Y */
    private _Y: ParamDefinition;

    constructor(rY: number, nullParams: boolean = false) {
        super();
        this._Y = new ParamDefinition(this, "Y", ParamDomainValue.ANY, undefined, rY, ParamFamily.ANY, undefined, nullParams);

        this.addParamDefinition(this._Y);
    }

    get Y() {
        return this._Y;
    }
}
