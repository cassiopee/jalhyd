import { Debug, IDebug } from "./base";
import { JalhydObject } from "./internal_modules";
import { ParamDefinition } from "./internal_modules";
import { IParamDefinitionIterator } from "./internal_modules";
import { ParamsEquation } from "./internal_modules";

/**
 * type de calculette
 */
export enum CalculatorType {
    ConduiteDistributrice,
    LechaptCalmon,
    SectionParametree,
    RegimeUniforme,
    CourbeRemous,
    PabDimensions,      // passe à bassin rectangulaire
    PabPuissance,       // passe à bassin : puissance dissipée
    Structure,          // ouvrages hydrauliques simples
    ParallelStructure,  // ouvrages hydrauliques en parallèle
    Dever,              // Outil Cassiopée Dever
    Cloisons,           // Outil Cassiopée PAB Cloisons
    MacroRugo,          // Passe à enrochement simple (Cassan et al., 2016)
    PabChute,
    PabNombre,
    Section,
    Pab,                // Passe à bassins;
    CloisonAval,
    MacroRugoCompound,  // Passe à enrochement composée
    Jet,                // Impact de jet
    Grille,             // Pertes de charge grille de prise d'eau
    Pente,
    Bief,
    Solveur,            // Nub qui résout des chaînes de Nubs par dichotomie
    YAXB,               // Y = A.X + B
    Trigo,              // Fonctions trigonométriques
    SPP,                // Somme et produit de puissances
    YAXN,               // Y = A.X^N
    ConcentrationBlocs,
    Par,                // Passe à ralentisseurs, calage
    ParSimulation,      // Passe à ralentisseurs, simulation
    PreBarrage,         // Pré-barrage
    PbCloison,          // Cloison de pré-barrage
    PbBassin,           // Bassin de pré-barrage
    Espece,             // Critères de vérification pour une espèce de poissons
    Verificateur,       // Vérificateur de contraintes sur une passe pour une ou plusieurs espèces
    PressureLoss,
    PressureLossLaw,    // Lois de perte de charge (Lechapt-Calmon, Colebrook-White, Strickler, ...)       // Perte de charge   // Lois de perte de charge (Lechapt-Calmon, Colebrook-White, Strickler, ...)
    MacrorugoRemous,    // Courbe de remous d'une passe à macro rugosités simple         // Passe à rugosité de fond 
    RugoFondMultiple    // Passe à rugosité de fond multiple
}

/** types de sections */
export enum SectionType {
    SectionCercle, SectionRectangle, SectionTrapeze, SectionPuissance
}

/**
 * noeud de calcul
 */
export abstract class ComputeNode extends JalhydObject implements IDebug {
    /**
     * Set of ParamDefinition, used as input of this node
     */
    protected _prms: ParamsEquation;

    /**
     * { symbol => ParamFamily } map for results; defines a priori which
     * future result can be linked to other Nub's parameters
     */
    protected _resultsFamilies: any;

    private _debug: Debug;

    constructor(prms: ParamsEquation, dbg: boolean = false) {
        super();
        this._debug = new Debug(dbg);
        this._prms = prms;
        this.debug("PARAMS", prms);

        // important for Param uid
        this._prms.parent = this;

        this.setParametersCalculability();
        this.exposeResults();
    }

    public get prms(): ParamsEquation {
        return this._prms;
    }

    /**
     * Retrieves a parameter from its symbol (unique in a given Nub)
     * @WARNING also retrieves **extra results** and returns them as ParamDefinition !
     */
    public getParameter(name: string): ParamDefinition {
        for (const p of this.parameterIterator) {
            if (p.symbol === name) {
                return p;
            }
        }
        return undefined;
    }

    public getFirstAnalyticalParameter(): ParamDefinition {
        for (const p of this.parameterIterator) {
            if (p.isAnalytical()) {
                return p;
            }
        }
        return undefined;
    }

    /**
     * Returns an iterator over own parameters (this._prms)
     */
    public get parameterIterator(): IParamDefinitionIterator {
        return this._prms.iterator;
    }

    // interface IDebug
    public debug(...args: any[]) {
        this._debug.debug(...args);
    }

    public get DBG(): boolean {
        return this._debug.DBG;
    }

    public set DBG(d: boolean) {
        this._debug.DBG = d;
    }

    /**
     * Define ParamFamily (@see ParamDefinition) for extra results; also
     * used to declare extra results that are not linkable but may be
     * used by Solveur : set family to undefined
     */
    protected exposeResults() {
        this._resultsFamilies = {};
    }

    public get resultsFamilies() {
        return this._resultsFamilies;
    }

    public static resultsUnits(): any {
        return {};
    }

    protected abstract setParametersCalculability(): void;

}
