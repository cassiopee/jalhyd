import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";
import { ParamsEquation } from "../internal_modules";

export class PreBarrageParams extends ParamsEquation {

    /** Débit entrant à l'amont de la passe (m3/s) */
    public Q: ParamDefinition;

    /** Cote de l'eau amont (m) */
    public Z1: ParamDefinition;

    /** Cote de l'eau aval (m) */
    public Z2: ParamDefinition;

    constructor(rQ: number, rZ1: number, rZ2: number, nullParams: boolean = false) {
        super();
        this.Q = new ParamDefinition(this, "Q", ParamDomainValue.POS_NULL, "m³/s", rQ, ParamFamily.FLOWS, undefined, nullParams);
        this.addParamDefinition(this.Q);
        this.Z1 = new ParamDefinition(this, "Z1", ParamDomainValue.ANY, "m", rZ1, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z1);
        this.Z2 = new ParamDefinition(this, "Z2", ParamDomainValue.ANY, "m", rZ2, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.Z2);
    }
}
