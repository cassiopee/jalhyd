import { ParamsEquation } from "../internal_modules";
import { ParamDefinition, ParamFamily } from "../internal_modules";
import { ParamDomainValue } from "../internal_modules";

export class PbBassinParams extends ParamsEquation {

    /** Surface du bassin en m2 */
    public S: ParamDefinition;

    /** Cote de fond du bassin en m */
    public ZF: ParamDefinition;

    constructor(rS: number, rZF: number, nullParams: boolean = false) {
        super();
        this.S = new ParamDefinition(this, "S", ParamDomainValue.POS, "m²", rS, undefined, undefined, nullParams);
        this.addParamDefinition(this.S);
        this.ZF = new ParamDefinition(this, "ZF", ParamDomainValue.ANY, "m", rZF, ParamFamily.ELEVATIONS, undefined, nullParams);
        this.addParamDefinition(this.ZF);
    }
}
