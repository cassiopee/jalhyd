import { Nub } from "../internal_modules";
import { Result } from "../internal_modules";
import { PbBassinParams } from "../internal_modules";
import { ParamCalculability } from "../internal_modules";
import { PbCloison } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { PreBarrage } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

export interface IPbBassinZstat {
    moy: number,
    min: number,
    max: number
}
export class PbBassin extends Nub {

    /** Liste des cloisons amont */
    public cloisonsAmont: PbCloison[];

    /** Liste des cloisons aval */
    public cloisonsAval: PbCloison[];

    /** Débit transitant dans le bassin en m³/s */
    public Q: number;

    /** Cote de l'eau dans le bassin en m */
    public Z: number;

    constructor(prms: PbBassinParams, dbg: boolean = false) {
        super(prms, dbg);
        this.setCalculatorType(CalculatorType.PbBassin);
        this.cloisonsAmont = [];
        this.cloisonsAval = [];
        this._intlType = "Bassin";
    }

    /**
     * paramètres castés au bon type
     */
    get prms(): PbBassinParams {
        return this._prms as PbBassinParams;
    }

    public Calc(sVarCalc?: string | any, rInit?: number): Result {
        // if Calc() is called outside of CalcSerie(), _result might not be initialized
        if (!this.result) {
            this.initNewResultElement();
        }
        const r = this.result;

        r.resultElement.values.Z = this.CalcZ().moy;
        r.resultElement.values.Q = this.CalcQ();
        r.resultElement.values.YMOY = r.resultElement.values.Z - this.prms.ZF.v;
        // Puissance dissipée
        let energy = 0; // sum of Energy of upstream walls
        for (const w of this.cloisonsAmont) {
            energy += w.prms.Q.v * (w.prms.Z1.V - w.prms.Z2.v);
        }
        const ro: number = 1000;     // masse volumique de l'eau en kg/m3
        const g: number = 9.81;     // accélération de la gravité terrestre en m/s2.
        r.resultElement.values.PV = ro * g * energy / r.resultElement.values.YMOY / this.prms.S.v;

        return r;
    }

    public Equation(sVarCalc: string): Result {
        switch (sVarCalc) {
            case "Q":
            case "Z":
                const r = new Result();
                r.values.Q = this.CalcQ();
                r.values.Z = this.CalcZ().moy;
                return r;
            default:
                throw new Error("PbBassin.Equation() : invalid variable name " + sVarCalc);
        }
    }

    /**
     * paramétrage de la calculabilité des paramètres
     */
    protected setParametersCalculability() {
        this.prms.S.calculability = ParamCalculability.FREE;
        this.prms.ZF.calculability = ParamCalculability.FREE;
    }

    public get parent(): PreBarrage {
        return super.parent as PreBarrage;
    }

    public CalcQ(): number {
        this.Q = 0;
        for (const c of this.cloisonsAmont) {
            this.Q += Math.max(0, c.prms.Q.v);
        }
        return this.Q;
    }

    public CalcZ(): IPbBassinZstat {
        const zStat = this.parent.CalcZ1Cloisons(this.cloisonsAval);
        this.Z = zStat.moy;
        return zStat;
    }

    public getMinZDV(): number {
        return this.parent.getMinZDV(this.cloisonsAval);
    }

    /** Returns order of current basin in parent PreBarrage, starting at 1 */
    public get order(): number {
        return this.parent.findBasinPosition(this.uid) + 1;
    }

    /**
     * Returns a translatable message for basin description, containing order number
     */
    public get description(): Message {
        return new Message(MessageCode.INFO_PB_BASSIN_DESCRIPTION, {
            order: String(this.order)
        });
    }
}
