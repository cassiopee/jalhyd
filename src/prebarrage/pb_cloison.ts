import { ParallelStructure } from "../internal_modules";
import { PbBassin } from "../internal_modules";
import { PreBarrage } from "../internal_modules";
import { ParallelStructureParams } from "../internal_modules";
import { CalculatorType } from "../internal_modules";
import { Result } from "../internal_modules";
import { LoiDebit, loiAdmissiblesOuvrages } from "../internal_modules";
import { Message, MessageCode } from "../internal_modules";

export class PbCloison extends ParallelStructure {

    constructor(bassinAmont: PbBassin, bassinAval: PbBassin, dbg: boolean = false, nullParams: boolean = false) {
        super(new ParallelStructureParams(0, 0, 0, nullParams), dbg);
        // prevent multiple CALC params in session file
        this.prms.Q.visible = false;
        this.prms.Z1.visible = false;
        this.prms.Z2.visible = false;
        this.setPropValue("upstreamBasin", bassinAmont === undefined ? "" : bassinAmont.uid);
        this.setPropValue("downstreamBasin", bassinAval === undefined ? "" : bassinAval.uid);
        this.setCalculatorType(CalculatorType.PbCloison);
        this._intlType = "Cloison";
    }

    /** pointer to parent Nub */
    public get parent(): PreBarrage {
        return super.parent as PreBarrage;
    }

    /** Bassin à l'amont de la cloison ou undefined pour condition limite amont */
    public get bassinAmont(): PbBassin {
        let basin: PbBassin;
        const basinUID: string = this._props.getPropValue("upstreamBasin");
        if (basinUID !== undefined && basinUID !== "") {
            basin = this.parent.findChild(basinUID) as PbBassin;
            if (basin === undefined) {
                // silent fail
            }
        }
        return basin;
    }

    /** sets the upstream basin by setting property "upstreamBasin" to the UID of the given PbBassin */
    public set bassinAmont(b: PbBassin) {
        let uid = ""; // empty value
        if (b !== undefined) {
            uid = b.uid;
        }
        this.setPropValue("upstreamBasin", uid);
        this.parent.updatePointers();
    }

    /** Bassin à l'aval de la cloison ou undefined pour condition limite aval */
    public get bassinAval(): PbBassin {
        let basin: PbBassin;
        const basinUID: string = this._props.getPropValue("downstreamBasin");
        if (basinUID !== undefined && basinUID !== "") {
            basin = this.parent.findChild(basinUID) as PbBassin;
            if (basin === undefined) {
                // silent fail
            }
        }
        return basin;
    }

    /** sets the downstream basin by setting property "downstreamBasin" to the UID of the given PbBassin */
    public set bassinAval(b: PbBassin) {
        let uid = ""; // empty value
        if (b !== undefined) {
            uid = b.uid;
        }
        this.setPropValue("downstreamBasin", uid);
        this.parent.updatePointers();
    }

    public get Z1(): number {
        if (this.bassinAmont !== undefined) {
            return this.bassinAmont.Z;
        } else {
            return this.parent.prms.Z1.v;
        }
    }

    public get Z2(): number {
        if (this.bassinAval !== undefined) {
            return this.bassinAval.Z;
        } else {
            return this.parent.prms.Z2.v;
        }
    }

    public Calc(sVarCalc?: string, rInit?: number): Result {
        this.updateZ1Z2();
        const r = super.Calc(sVarCalc, rInit);
        switch (sVarCalc) {
            case "Z1":
                // Upstream water elevation should be at least equal minZDV
                r.vCalc = Math.max(this.getMinZDV(), r.vCalc);
                break;

            case "Q":
                r.vCalc = Math.max(0, r.vCalc);
        }
        return r;
    }

    public finalCalc() {
        this.calculatedParam = this.prms.Q;
        this.Calc("Q");
        // add Z1, Z2 & DH extra results for GUI convenience
        this.result.resultElement.values.Z1 = this.prms.Z1.v;
        this.result.resultElement.values.Z2 = this.prms.Z2.v;
        this.result.resultElement.values.DH = this.prms.Z1.v - this.prms.Z2.v;
    }

    public updateZ1Z2() {
        this.prms.Z1.v = this.Z1;
        this.prms.Z2.v = this.Z2;
    }

    /**
     * Give the minimum ZDV on all structures. Undefined if only orifice without ZDV.
     */
    public getMinZDV(): number {
        let minZDV: number;
        for (const s of this.structures) {
            if (s.prms.ZDV.visible) {
                if (minZDV === undefined) {
                    minZDV = s.prms.ZDV.v;
                } else {
                    minZDV = Math.min(minZDV, s.prms.ZDV.v);
                }
            }
        }
        return minZDV;
    }

    public getLoisAdmissibles(): { [key: string]: LoiDebit[]; } {
        return loiAdmissiblesOuvrages; // @TODO loiAdmissiblesCloisons plutôt, mais penser à y intégrer Cunge80
    }

    /**
     * Returns a translatable message for wall description, containing order numbers of
     * upstream and downstream basin, or "upstream" / "downstream" mention if wall is
     * directly connected to the river
     */
    public get description(): Message {
        return new Message(MessageCode.INFO_PB_CLOISON_DESCRIPTION, {
            ub: this.bassinAmont === undefined ? "MSG_INFO_LIB_AMONT" : "B" + String(this.bassinAmont.order),
            db: this.bassinAval === undefined ? "MSG_INFO_LIB_AVAL" : "B" + String(this.bassinAval.order),
        });
    }
}
