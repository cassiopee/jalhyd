'use strict';

const fs = require('fs');
const rimraf = require('rimraf');

const path = './build';

if (fs.existsSync(path)) {
    rimraf.sync(path);
}
