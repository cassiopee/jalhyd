// lance jasmine sans recompiler
// Plus précisément, s'assure que build/spec existe et lance simplement jasmine. Sinon, lance npm run jasmine.

const fs = require('fs');
const path = require('path');
const spawn = require('cross-spawn');

if (!fs.existsSync(path.resolve('build/spec'))) {
  console.log("build/spec not found, npm run jasmine");
  const result = spawn.sync(
    'npm',
    ['run', 'jasmine'],
    {
      stdio: 'inherit'
    }
  );
} else {
  console.log("build/spec found, run jasmine");
  const result = spawn.sync(
    'node_modules/.bin/jasmine',
    {
      stdio: 'inherit'
    }
  );
}
