import { formattedValue } from "../src/base";

const cases: { o: number, f: string }[] = [
    {
        o: 1,
        f: "1.000"
    },
    {
        o: 0.123,
        f: "0.123"
    },
    {
        o: 0.0000012365,
        f: "0.00000124"
    },
    {
        o: 321.0000012365,
        f: "321.000"
    },
    {
        o: 0.000001,
        f: "0.000001"
    },
    {
        o: 700.000001,
        f: "700.000"
    }
];

describe("precision of formatted values (3 digits)", () => {
    for (const c of cases) {
        it("", () => {
            expect(formattedValue(c.o, 3)).toBe(c.f);
        });
    }
});
