import { Session } from "../src/session";
import { ParallelStructure } from "../src/structure/parallel_structure";
import { MessageCode } from "../src/util/message";

describe("cLog −", () => {

    it("in ParallelStructure, Structure log message's grand-parent should be the Structure's result, not the ParallelStructure's one", () => {
        const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-08-11T08:04:59.930Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"dnhvbz","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages"},"children":[{"uid":"aHkzNj","props":{"calcType":"Structure","structureType":"VanneFondRectangulaire","loiDebit":"RectangularOrificeFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"SINGLE","value":2},{"symbol":"CdGR","mode":"SINGLE","value":0.6}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"Z2","mode":"SINGLE","value":101.5}]}]}`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const ps = Session.getInstance().findNubByUid("dnhvbz") as ParallelStructure;
        ps.CalcSerie();
        const s = ps.structures[0];
        expect(s.result.log.messages.length).toBe(1, "Kenobi");
        expect(s.result.log.messages[0].code).toBe(MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION);
        expect(s.result.log.messages[0].parent).toBe(s.result.log);
        expect(s.result.log.messages[0].parent.parent.uid).toBe(s.result.uid);
    });

});
