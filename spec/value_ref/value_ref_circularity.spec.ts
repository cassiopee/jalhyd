import { CreateStructure, LoiDebit, ParallelStructure,
    ParallelStructureParams, Session, Structure } from "../../src/index";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { NubTest, NubTestParams } from "../nubtest";

let nub1: NubTest;
let nub2: NubTest;
let nub3: NubTest;
let prm1: NubTestParams;
let prm2: NubTestParams;
let prm3: NubTestParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    nub1 = new NubTest(new NubTestParams());
    prm1 = nub1.prms as NubTestParams;

    nub2 = new NubTest(new NubTestParams());
    prm2 = nub2.prms as NubTestParams;

    nub3 = new NubTest(new NubTestParams());
    prm3 = nub3.prms as NubTestParams;

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
    Session.getInstance().registerNub(nub3);
}

describe("référence d'un paramètre à un autre : ", () => {
    describe("vérification des références circulaires : ", () => {
        it("test 1", () => {
            // cas de figure (ne doit pas échouer) :
            // nub2.A référence nub1.A
            createEnv();
            expect(() => {
                prm2.A.defineReference(nub1, "A"); // ne doit pas échouer
            }).not.toThrow();
        });

        it("test 2", () => {
            // cas de figure (ne doit pas échouer) :
            // nub1.A référence nub2.A qui référence nub3.A
            createEnv();
            expect(() => {
                prm1.A.defineReference(nub2, "A"); // ne doit pas échouer
                prm2.A.defineReference(nub3, "A"); // ne doit pas échouer
            }).not.toThrow();
        });

        it("test 3", () => {
            // cas de figure (doit échouer) :
            // nub2.A référence nub1.A qui référence nub2.A
            createEnv();
            expect(() => {
                prm2.A.defineReference(nub1, "A"); // ne doit pas échouer
            }).not.toThrow();
            expect(() => {
                prm1.A.defineReference(nub2, "A"); // doit échouer
            }).toThrow();
        });

        it("test 4", () => {
            // cas de figure (doit échouer) :
            // param1 référence param2 (OK)
            // param3 référence param1 (OK)
            // param2 référence param3 (doit échouer)
            createEnv();
            expect(() => {
                prm1.A.defineReference(nub2, "A"); // ne doit pas échouer
                prm3.A.defineReference(nub2, "A"); // ne doit pas échouer
            }).not.toThrow();
            expect(() => {
                prm2.A.defineReference(nub3, "A"); // doit échouer
            }).toThrow();
        });

        it("test 5 : ouvrages en parallèle", () => {
            // cas de figure :
            // - le param. de l'ouvrage 2 référence le param. de l'ouvrage 1 (ne doit pas échouer)
            // - le param. de l'ouvrage 2 référence le param. en calcul de l'ouvrage 1,
            // via le résultat du Nub ParallelStructure (doit échouer)

            const psp: ParallelStructureParams = new ParallelStructureParams(1, 2, 3);
            const pst = new ParallelStructure(psp);
            const st1: Structure = CreateStructure(LoiDebit.GateCem88v, pst);
            pst.addChild(st1);
            const prmS1 = (st1.prms as RectangularStructureParams);
            const st2: Structure = CreateStructure(LoiDebit.GateCem88v, pst);
            const prmS2 = (st2.prms as RectangularStructureParams);
            pst.addChild(st2);
            pst.calculatedParam = prmS1.L;

            Session.getInstance().clear();
            Session.getInstance().registerNub(pst);

            expect(() => {
                prmS2.W.defineReference(st1, "W"); // ne doit pas échouer
            }).not.toThrow();

            expect(() => {
                prmS2.L.defineReference(pst, "L"); // doit échouer
            }).toThrow();
        });
    });
});
