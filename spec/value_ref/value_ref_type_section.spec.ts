import { CreateStructure, cSnCirc, LinkedValue, LoiDebit, ParallelStructure, ParallelStructureParams,
        Session } from "../../src/index";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { ParamsSectionCirc } from "../../src/open-channel/section/section_circulaire_params";
import { SessionSettings } from "../../src/session_settings";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";

let nub1: RegimeUniforme;
let nub2: ParallelStructure;
let prm1: ParamsSectionCirc;
let prm2: ParallelStructureParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    // Nub 1 : Régime Uniforme
    const paramSect = new ParamsSectionCirc(2, 0.6613, 40, 1.2, 0.001, 1);
    SessionSettings.precision = 0.01;
    const sect = new cSnCirc(paramSect);
    nub1 = new RegimeUniforme(sect);
    prm1 = nub1.prms as ParamsSectionCirc;

    // Nub 2 : Lois d'ouvrages
    prm2 = new ParallelStructureParams(0.5, 102, 101.5);
    nub2 = new ParallelStructure(prm2);
    nub2.addChild(
        CreateStructure(
            LoiDebit.GateCunge80,
            nub2
        )
    );
    nub2.addChild(
        CreateStructure(
            LoiDebit.TriangularWeirFree,
            nub2
        )
    );

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
}

describe("régime uniforme : référence à paramètre absent du type de section utilisé : ", () => {

    it("LargeurBerge (n'est pas dans section circulaire)", () => {
        // cas de figure :
        // nub1 utilise une section circulaire, qui n'a pas de LargeurBerge
        // nub2 ne devrait pouvoir lier aucun paramètre à la largeur L de son ouvrage

        createEnv();

        const L = (nub2.structures[0].prms as RectangularStructureParams).L;
        const lv: LinkedValue[] = Session.getInstance().getLinkableValues(L);
        // console.log(lv);
        expect(lv.length).toBe(0);
    });

});
