import { Result, Session } from "../../src/index";
import { SessionSettings } from "../../src/session_settings";
import { NubTest, NubTestParams } from "../nubtest";
import { precDigits } from "../test_config";

let nub1: NubTest;
let nub2: NubTest;
let prm1: NubTestParams;
let prm2: NubTestParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    // Nub maître (dont on référence la valeur du paramètre A)
    nub1 = new NubTest(new NubTestParams());
    prm1 = nub1.prms as NubTestParams;

    // Nub esclave (qui utilise la valeur de A du Nub maître)
    nub2 = new NubTest(new NubTestParams());
    prm2 = nub2.prms as NubTestParams;

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
}

describe("référence d'un paramètre à un autre : ", () => {
    describe("lien au paramètre du même nom : ", () => {
        it("test 1", () => {
            // cas de figure :
            // nub2.A est lié à nub1.A (valeur fixe)
            // lecture des valeurs de tous les paramètres

            createEnv();

            // valeur esclave, doit être masquée par la valeur maître (cad prm1.A, normalement 1)
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "A");

            expect(prm1.A.singleValue).toEqual(1);
            expect(prm1.B.singleValue).toEqual(2);
            expect(prm1.C.singleValue).toEqual(3);
            expect(prm2.A.singleValue).toEqual(1);
            expect(prm2.B.singleValue).toEqual(2);
            expect(prm2.C.singleValue).toEqual(3);
        });

        it("test 2", () => {
            // cas de figure :
            // nub2.B est lié à nub1.B (valeur fixe)
            // lecture des valeurs de tous les paramètres

            createEnv();

            // valeur esclave, doit être masquée par la valeur maître (cad prm1.B, normalement 2)
            prm2.B.singleValue = 0;
            prm2.B.defineReference(nub1, "B");

            expect(prm1.A.singleValue).toEqual(1);
            expect(prm1.B.singleValue).toEqual(2);
            expect(prm1.C.singleValue).toEqual(3);
            expect(prm2.A.singleValue).toEqual(1);
            expect(prm2.B.singleValue).toEqual(2);
            expect(prm2.C.singleValue).toEqual(3);
        });

        it("test 3", () => {
            // cas de figure :
            // nub2.C est lié à nub1.C (valeur fixe)
            // lecture des valeurs de tous les paramètres

            createEnv();

            // valeur esclave, doit être masquée par la valeur maître (cad prm1.C, normalement 3)
            prm2.C.singleValue = 0;
            prm2.C.defineReference(nub1, "C");

            // as C is a calculated param
            nub2.triggerChainCalculation();

            expect(prm1.A.singleValue).toEqual(1);
            expect(prm1.B.singleValue).toEqual(2);
            expect(prm1.C.singleValue).toEqual(3);
            expect(prm2.A.singleValue).toEqual(1);
            expect(prm2.B.singleValue).toEqual(2);
            expect(prm2.C.singleValue).toEqual(3);
        });

        it("test 4", () => {
            // cas de figure :
            // nub2.A est lié à nub1.A (valeur fixe)
            // calcul de tous les paramètres

            createEnv();

            // valeur esclave, doit être masquée par la valeur maître (cad prm1.A, normalement 1)
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "A");

            expect(nub1.Calc("A").vCalc).toBeCloseTo(1, precDigits);
            expect(nub1.Calc("B").vCalc).toBeCloseTo(2, precDigits);
            expect(nub1.Calc("C").vCalc).toBeCloseTo(3, precDigits);

            expect(nub2.Calc("A").vCalc).toBeCloseTo(1, precDigits);
            expect(nub2.Calc("B").vCalc).toBeCloseTo(2, precDigits);
            expect(nub2.Calc("C").vCalc).toBeCloseTo(3, precDigits);
        });

        it("test 5", () => {
            // cas de figure :
            // nub2.B est lié à nub1.B (valeur fixe)
            // calcul de tous les paramètres

            createEnv();

            prm1.B.singleValue = 3;  // valeur maître
            prm2.B.singleValue = 0;  // valeur esclave (doit être masquée par la valeur maître)
            prm2.B.defineReference(nub1, "B");

            nub1.calculatedParam = nub1.prms.A;
            expect(nub1.CalcSerie().vCalc).toBeCloseTo(0, precDigits);
            nub1.calculatedParam = nub1.prms.B;
            expect(nub1.CalcSerie().vCalc).toBeCloseTo(2, precDigits);
            nub1.calculatedParam = nub1.prms.C;
            expect(nub1.CalcSerie().vCalc).toBeCloseTo(4, precDigits);

            nub2.calculatedParam = nub2.prms.A;
            expect(nub2.CalcSerie().vCalc).toBeCloseTo(0, precDigits);
            nub2.calculatedParam = nub2.prms.C;
            expect(nub2.CalcSerie().vCalc).toBeCloseTo(4, precDigits);
            // définir B en calcul fait sauter le lien, donc on calcule C
            // avant, pour ne pas avoir à redéfinir le lien
            nub2.calculatedParam = nub2.prms.B;
            expect(nub2.CalcSerie().vCalc).toBeCloseTo(2, precDigits);

        });

        it("test 6", () => {
            // cas de figure :
            // nub2.C est lié à nub1.C (valeur fixe)
            // calcul de tous les paramètres

            createEnv();

            // valeur esclave, doit être masquée par la valeur maître (cad prm1.C, normalement 3)
            prm2.C.singleValue = 0;
            prm2.C.defineReference(nub1, "C");

            expect(nub1.Calc("A").vCalc).toBeCloseTo(1, precDigits);
            expect(nub1.Calc("B").vCalc).toBeCloseTo(2, precDigits);
            expect(nub1.Calc("C").vCalc).toBeCloseTo(3, precDigits);

            expect(nub2.Calc("A").vCalc).toBeCloseTo(1, precDigits);
            expect(nub2.Calc("B").vCalc).toBeCloseTo(2, precDigits);
            expect(nub2.Calc("C").vCalc).toBeCloseTo(3, precDigits);
        });
    });

    describe("lien à un paramètre non fixé : ", () => {
        it("test 1", () => {
            // cas de figure :
            // nub2.A est lié à nub1.C (valeur calculée)
            // lecture de nub2.A

            createEnv();

            prm1.B.singleValue = 5;
            nub1.calculatedParam = prm1.C;
            // valeur esclave (doit être masquée par la valeur maître, cad prm1.C, normalement 3)
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "C");

            // as C is a calculated param
            nub2.triggerChainCalculation();

            expect(prm2.A.singleValue).toBeCloseTo(6, precDigits);
        });

        it("test 2", () => {
            // cas de figure :
            // nub2.A est lié à nub1.C (valeur calculée)
            // calcul de nub2.C

            createEnv();

            prm1.C.singleValue = 0;  // valeur bidon, doit être 3 après calcul
            nub1.calculatedParam = prm1.C;
            prm2.C.singleValue = 0;  // valeur bidon, doit être 5 après calcul
            // valeur esclave bidon, doit être masquée par la valeur maître (cad prm1.C, normalement 3)
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "C");

            nub2.calculatedParam = nub2.prms.C;
            expect(nub2.CalcSerie().vCalc).toBeCloseTo(5, precDigits);
        });

        it("test 3", () => {
            // cas de figure :
            // nub2.A est lié à nub1.A (valeur variée)
            // lecture de nub2.A

            createEnv();

            const min = 1;
            const max = 5;
            const step = 1;

            prm1.A.setValues(min, max, step);

            // valeur esclave bidon, doit être masquée par la valeur maître (cad prm1.A, normalement [1,2,3,4,5])
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "A");

            let n = 0;
            let i = min;
            for (const v of prm2.A.paramValues.getValuesIterator()) {
                expect(v).toEqual(i);
                n++;
                i += step;
            }
            expect(n).toEqual((max - min) / step + 1);
        });

        it("test 4", () => {
            // cas de figure :
            // nub2.A est lié à nub1.A (valeur variée)
            // calcul de nub2.C

            createEnv();

            const input = [2, 3, 4, 5, 6];
            prm1.A.setValues(input);

            // valeur esclave bidon, doit être masquée par la valeur maître (cad prm1.A, normalement [2,3,4,5,6])
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "A");
            SessionSettings.precision = 0.001;

            nub2.calculatedParam = nub2.prms.C;
            const r: Result = nub2.CalcSerie(0.1);

            let n = 0;
            for (const re of r.resultElements) {
                expect(re.vCalc).toEqual(input[n] + 2);
                n++;
            }
            expect(n).toEqual(input.length);
        });

        it("test 5", () => {
            // cas de figure :
            // nub2.A est lié à nub1.A (valeur variée)
            // lecture de nub2.A

            createEnv();

            const input = [2, 3, 4, 5, 6];
            prm1.A.setValues(input);

            // valeur esclave bidon, doit être masquée par la valeur maître (cad prm1.A, normalement [2,3,4,5,6])
            prm2.A.singleValue = 0;
            prm2.A.defineReference(nub1, "A");

            let ndx = 0;
            for (const v of prm2.A.paramValues.getValuesIterator()) {
                expect(v).toEqual(input[ndx]);
                ndx++;
            }
            expect(ndx).toEqual(input.length);
        });
    });
});
