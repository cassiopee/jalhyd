import { Session } from "../../src/index";
import { ConduiteDistrib } from "../../src/pipe_flow/cond_distri";
import { ConduiteDistribParams } from "../../src/pipe_flow/cond_distri_params";
import { SessionSettings } from "../../src/session_settings";
import { Result } from "../../src/util/result";

let nub1: ConduiteDistrib;
let nub2: ConduiteDistrib;
let prm1: ConduiteDistribParams;
let prm2: ConduiteDistribParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    // Nub maître
    nub1 = new ConduiteDistrib(new ConduiteDistribParams(3, 1.2, 0.6, 100, 0.000001));
    prm1 = nub1.prms as ConduiteDistribParams;

    // Nub esclave
    nub2 = new ConduiteDistrib(new ConduiteDistribParams(3, 1.2, 0.6, 100, 0.000001));
    prm2 = nub2.prms as ConduiteDistribParams;

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
}

describe("référence d'un paramètre à un résultat : ", () => {
    it("ConduiteDistri Q fixe => Calc(J) => Importe(J) => Calc(Q)", () => {
        // cas de figure :
        // nub2.Q est lié au résultat J de nub1
        // lecture de nub2.Q

        createEnv();

        prm1.Q.v = 2;
        nub1.calculatedParam = prm1.J;
        SessionSettings.precision = 0.001;
        const res1: Result = nub1.CalcSerie(0.6);

        expect(res1.resultElements.length).toEqual(1); // nombre de valeurs du Result

        prm2.J.defineReference(nub1, "J");

        nub2.calculatedParam = prm2.Q;
        const res2 = nub2.CalcSerie(0.6);
        expect(res2.resultElements.length).toEqual(1);  // nombre de valeurs du Result

        expect(res2.vCalc).toBeCloseTo(prm1.Q.v, 3);
    });

    it("ConduiteDistri Q varie => Calc(J) => Importe(J) => Calc(Q)", () => {
        // cas de figure :
        // nub2.Q est lié au résultat J de nub1
        // lecture de nub2.Q

        createEnv();

        prm1.Q.setValues(1.5, 6, 0.5);
        nub1.calculatedParam = prm1.J;
        SessionSettings.precision = 0.001;
        const res1: Result = nub1.CalcSerie(0.6);

        prm2.J.defineReference(nub1, "J");

        nub2.calculatedParam = prm2.Q;
        const res2 = nub2.CalcSerie(0.6);

        expect(res1.resultElements.length).toEqual(10);  // nombre de valeurs du Result
        expect(res2.resultElements.length).toEqual(10);

        let QREF = prm1.Q.paramValues.min;
        for (const re of res2.resultElements) {
            expect(re.vCalc).toBeCloseTo(QREF, 0.001);
            QREF += prm1.Q.paramValues.step;
        }
    });
});
