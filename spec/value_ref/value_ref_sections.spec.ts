import { BiefParams } from "../../src/open-channel/bief_params";
import { Bief } from "../../src/open-channel/bief";
import { SectionParametree } from "../../src/open-channel/section/section_parametree";
import { ParamsSectionRectang } from "../../src/open-channel/section/section_rectang_params";
import { cSnRectang } from "../../src/open-channel/section/section_rectang";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { ParamsSectionCirc } from "../../src/open-channel/section/section_circulaire_params";
import { cSnCirc } from "../../src/open-channel/section/section_circulaire";
import { Session } from "../../src/session";

let nub1: SectionParametree;
let prm1: ParamsSectionRectang

let nub2: Bief;
let prm2: BiefParams;
let prm2s: ParamsSectionRectang;

let nub3: RegimeUniforme;
let prm3: ParamsSectionCirc;

describe("liens entre paramètres de sections, sans familles −", () => {

    beforeEach(() => {
        nub1 = new SectionParametree(new cSnRectang(new ParamsSectionRectang(0.8, 0.9, 41, 1.2, 0.01, 1.15)));
        prm1 = nub1.section.prms as ParamsSectionRectang;

        nub2 = new Bief(
            new cSnRectang(new ParamsSectionRectang(0.8, 0.9, 41, 1.2, 0.01, 1.15)),
            new BiefParams(100, 90, 99.5, 89.5, 50, 2)
        );
        prm2 = nub2.prms as BiefParams;
        prm2s = nub2.section.prms as ParamsSectionRectang;

        nub3 = new RegimeUniforme(new cSnCirc(new ParamsSectionCirc(0.75, 0.8, 42, 1.2, 0.01, 0.9)));
        prm3 = nub3.section.prms as ParamsSectionCirc;

        Session.getInstance().clear();
        Session.getInstance().registerNub(nub1);
        Session.getInstance().registerNub(nub2);
        Session.getInstance().registerNub(nub3);
    });

    // vérifier qu'on peut lier le strickler dans tous les sens
    describe("link Ks should work from any nub to any other −", () => {

        it("from nub1 to nub2", () => {
            expect(() => {
                prm1.Ks.defineReference(nub2.section, "Ks");
            }).not.toThrow();
        });

        it("from nub1 to nub3", () => {
            expect(() => {
                prm1.Ks.defineReference(nub3.section, "Ks");
            }).not.toThrow();
        });

        it("from nub2 to nub1", () => {
            expect(() => {
                prm2s.Ks.defineReference(nub1.section, "Ks");
            }).not.toThrow();
        });

        it("from nub2 to nub3", () => {
            expect(() => {
                prm2s.Ks.defineReference(nub3.section, "Ks");
            }).not.toThrow();
        });

        it("from nub3 to nub1", () => {
            expect(() => {
                prm3.Ks.defineReference(nub1.section, "Ks");
            }).not.toThrow();
        });

        it("from nub3 to nub2", () => {
            expect(() => {
                prm3.Ks.defineReference(nub2.section, "Ks");
            }).not.toThrow();
        });

    });

    // vérifier qu'on ne peut lier la largeur de berge qu'entre nub1 et nub2 (sections rectangulaires)
    describe("link LargeurBerge should work only between nub2 and nub3 −", () => {

        it("from nub1 to nub2, should work", () => {
            expect(() => {
                prm1.LargeurBerge.defineReference(nub2.section, "LargeurBerge");
            }).not.toThrow();
        });

        it("from nub2 to nub1, should work", () => {
            expect(() => {
                prm2s.LargeurBerge.defineReference(nub1.section, "LargeurBerge");
            }).not.toThrow();
        });

        it("from nub2 to nub3, should fail", () => {
            expect(() => {
                prm1.LargeurBerge.defineReference(nub3.section, "LargeurBerge");
            }).toThrow();
        });

        it("from nub2 to nub3, should fail", () => {
            expect(() => {
                prm2s.LargeurBerge.defineReference(nub3.section, "LargeurBerge");
            }).toThrow();
        });

    });

    // vérifier qu'on ne peut lier le diamètre du nub3 à rien (seule section circulaire)
    describe("link D should not be possible", () => {

        it("from nub3 to nub1, should fail", () => {
            expect(() => {
                prm3.D.defineReference(nub1.section, "D");
            }).toThrow();
        });

        it("from nub3 to nub2, should fail", () => {
            expect(() => {
                prm3.D.defineReference(nub2.section, "D");
            }).toThrow();
        });

    });

});
