import {
    CourbeRemous,
    CourbeRemousParams,
    cSnRectang,
    ParamsSectionRectang,
    ParamValueMode,
    Session
} from "../../src/index";
import { ConduiteDistrib } from "../../src/pipe_flow/cond_distri";
import { ConduiteDistribParams } from "../../src/pipe_flow/cond_distri_params";
import { SessionSettings } from "../../src/session_settings";
import { Result } from "../../src/util/result";

let nub1: ConduiteDistrib;
let nub2: ConduiteDistrib;
let nub3: CourbeRemous;
let prm1: ConduiteDistribParams;
let prm2: ConduiteDistribParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    // Nub maître
    nub1 = new ConduiteDistrib(new ConduiteDistribParams(3, 1.2, 0.6, 100, 0.000001));
    prm1 = nub1.prms as ConduiteDistribParams;

    // Nub esclave
    nub2 = new ConduiteDistrib(new ConduiteDistribParams(3, 1.2, 0.6, 100, 0.000001));
    prm2 = nub2.prms as ConduiteDistribParams;

    // Nub avec paramètres non variables
    nub3 = new CourbeRemous(
        new cSnRectang(
            new ParamsSectionRectang(0.8, 2.5, 40, 1.2, 0.01, 1.2)
        ),
        new CourbeRemousParams(101, 100.05, 100.9, 100, 50, 2)
    );

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
    Session.getInstance().registerNub(nub3);
}

describe("Référence d'un paramètre à un paramètre varié : ", () => {
    it("ConduiteDistri Q varie sur nub1, nub2.Q => nub1.Q, calcul et comparaison de J pour les deux", () => {
        // cas de figure :
        // nub2.Q est lié au résultat J de nub1
        // lecture de nub2.Q

        createEnv();

        prm1.Q.setValues(1.5, 6, 0.5);
        SessionSettings.precision = 0.001;
        nub1.calculatedParam = prm1.J;
        const res1: Result = nub1.CalcSerie(0.6);

        prm2.Q.defineReference(nub1, "Q");
        SessionSettings.precision = 0.001;
        nub2.calculatedParam = prm2.J;
        const res2 = nub2.CalcSerie(0.6);

        for (let i = 0; i < res1.resultElements.length; i++) {
            expect(res1.resultElements[i].vCalc).toEqual(res2.resultElements[i].vCalc);
        }

    });

    it("un paramètre non variable ne doit pas être liable à un paramètre varié", () => {
        // cas de figure :
        // nub3.Q est lié à nub2.Q, fixé
        // nub2.Q passe en mode varié
        // nub3.Q ne doit plus être liable à nub2.Q

        createEnv();

        nub2.calculatedParam = prm2.Lg;
        prm2.Q.singleValue = 1.25;

        let linkables = Session.getInstance().getLinkableValues(nub3.section.prms.Q);
        let found = false;
        for (const l of linkables) {
            found = (l.nub === nub2) && (l.symbol === "Q");
        }
        expect(found).toBe(true);

        nub3.section.prms.Q.defineReference(nub2, "Q");
        expect(nub3.section.prms.Q.valueMode).toBe(ParamValueMode.LINK);
        expect(nub3.section.prms.Q.hasMultipleValues).toBe(false);

        prm2.Q.setValues(1, 1.5, 0.1);

        linkables = Session.getInstance().getLinkableValues(nub3.section.prms.Q);
        found = false;
        for (const l of linkables) {
            found = (l.nub === nub2) && (l.symbol === "Q");
        }
        expect(found).toBe(false);
    });

    it("un paramètre non variable ne doit pas être liable à un résultat varié", () => {
        // cas de figure :
        // nub3.Q est lié à nub2.Q, résultat de calcul
        // nub2.Lg passe en mode varié
        // nub3.Q ne doit plus être liable à nub2.Q

        createEnv();

        nub2.calculatedParam = prm2.Q;
        prm2.Lg.singleValue = 90;

        let linkables = Session.getInstance().getLinkableValues(nub3.section.prms.Q);
        let found = false;
        for (const l of linkables) {
            found = (l.nub === nub2) && (l.symbol === "Q");
        }
        expect(found).toBe(true);

        nub3.section.prms.Q.defineReference(nub2, "Q");
        expect(nub3.section.prms.Q.valueMode).toBe(ParamValueMode.LINK);
        expect(nub3.section.prms.Q.hasMultipleValues).toBe(false);

        prm2.Lg.setValues(80, 120, 1.5);

        linkables = Session.getInstance().getLinkableValues(nub3.section.prms.Q);
        found = false;
        for (const l of linkables) {
            found = (l.nub === nub2) && (l.symbol === "Q");
        }
        expect(found).toBe(false);
    });
});
