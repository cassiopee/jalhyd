import { PabChute, PabChuteParams, ParamValueMode, Props, Session } from "../../src/index";

describe("référence à un nub supprimé par la suite : ", () => {
    it("test 1", () => {
        // cas de figure : cf. merge request 177 (nghyd#571,jalhyd#329)

        const session = Session.getInstance();
        //        const pc1: PabChute = new PabChute(new PabChuteParams(2, 0.5, 666));
        const props = new Props({ calcType: 12, nullParams: false });
        const pc1: PabChute = <PabChute>session.createSessionNub(props);

        let serialisedNub: string = pc1.serialise({ title: "newcalc" });
        const pc2 = session.unserialiseSingleNub(serialisedNub).nub;

        const DH_2 = pc2.getParameter("DH");
        DH_2.defineReference(pc1, "DH");
        session.deleteNub(pc1);

        expect(DH_2.valueMode).toBe(ParamValueMode.SINGLE); // pas de but du test, mais ça mange pas de pain
        DH_2.setCalculated();

        serialisedNub = pc2.serialise({ title: "newcalc2" });
        session.unserialiseSingleNub(serialisedNub).nub;

        expect(DH_2.valueMode).toBe(ParamValueMode.CALCUL);
    });
});
