import { Cloisons, CreateStructure, cSnRectang, LoiDebit,
         MessageCode, SectionParametree, Session } from "../../src/index";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { ParamsSectionRectang } from "../../src/open-channel/section/section_rectang_params";
import { CloisonsParams } from "../../src/pab/cloisons_params";
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";
import { PabDimension } from "../../src/pab/pab_dimension";
import { PabDimensionParams } from "../../src/pab/pab_dimensions_params";
import { PabNombre } from "../../src/pab/pab_nombre";
import { PabNombreParams } from "../../src/pab/pab_nombre_params";
import { PabPuissance } from "../../src/pab/pab_puissance";
import { PabPuissanceParams } from "../../src/pab/pab_puissance_params";
import { Jet } from "../../src/devalaison/jet";

describe("chained computation of linked Nubs : ", () => {

    it("links through results should be computed in chain but links through parameters should not", () => {
        // PAB: Dimensions
        const prmsDim = new PabDimensionParams(
            4,      // Longueur L
            1,      // Largeur W
            0.5,    // Tirant d'eau Y
            2       // Volume V
        );
        const dim = new PabDimension(prmsDim);
        dim.calculatedParam = prmsDim.Y;

        // PAB: Puissance
        const prmsPui = new PabPuissanceParams(
            0.3,      // Chute entre bassins DH (m)
            0.1,      // Débit Q (m3/s)
            0.5,    // Volume V (m3)
            588.6   // Puissance dissipée PV (W/m3)
        );
        const pui = new PabPuissance(prmsPui);
        pui.calculatedParam = prmsPui.V;

        // PAB: Cloisons
        const prmsClo = new CloisonsParams(1.5, 102, 10, 1, 1, 0.5);
        const clo = new Cloisons(prmsClo);
        clo.addChild(CreateStructure(LoiDebit.OrificeSubmerged, clo));
        clo.calculatedParam = prmsClo.Q;

        Session.getInstance().clear();
        Session.getInstance().registerNub(dim);
        Session.getInstance().registerNub(pui);
        Session.getInstance().registerNub(clo);

        prmsClo.LB.defineReference(dim, "L");
        prmsClo.BB.defineReference(dim, "W");
        prmsClo.DH.defineReference(pui, "DH");
        prmsDim.V.defineReference(pui, "V");

        clo.CalcSerie();
        expect(dim.result).toBeUndefined();
        expect(pui.result).toBeUndefined();

        dim.CalcSerie();
        expect(pui.result).toBeDefined();
    });

    it("2 Nubs linked to each other through 1 result and 1 parameter should not lead to infinite recursion", () => {
        expect(() => {
            // Section Parametree
            const paramSp = new ParamsSectionRectang(
                undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1 // YB=hauteur de berge
            );
            const sectSp = new cSnRectang(paramSp);
            const sp = new SectionParametree(sectSp);

            // Regime Uniforme
            const paramRu = new ParamsSectionRectang(
                undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1 // YB=hauteur de berge
            );
            const sectRu = new cSnRectang(paramRu);
            const ru = new RegimeUniforme(sectRu);

            Session.getInstance().clear();
            Session.getInstance().registerNub(sp);
            Session.getInstance().registerNub(ru);

            paramRu.LargeurBerge.defineReference(sp, "B");
            paramSp.If.defineReference(sectRu, "If");

            ru.CalcSerie(); // should not fail

            sp.CalcSerie(); // should not fail
        }).not.toThrow();
    });

    it("modifying a value in an upstream Nub should change the downstream Nub result (linked param)", () => {
        const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
        pc.calculatedParam = pc.prms.Z2;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        Session.getInstance().clear();
        Session.getInstance().registerNub(pc);
        Session.getInstance().registerNub(pn);
        pn.prms.DHT.defineReference(pc, "DH");

        pc.prms.DH.singleValue = 1.7;
        const res1 = pn.CalcSerie();
        const r1 = res1.vCalc;
        pc.prms.DH.singleValue = 2.4;
        const res2 = pn.CalcSerie();
        const r2 = res2.vCalc;
        expect(r1).not.toBe(r2);
    });

    it("modifying a value in an upstream Nub should change the downstream Nub result (linked result)", () => {
        const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        Session.getInstance().clear();
        Session.getInstance().registerNub(pc);
        Session.getInstance().registerNub(pn);
        pn.prms.DHT.defineReference(pc, "DH");

        pc.prms.Z1.singleValue = 1.3;
        const res1 = pn.CalcSerie();
        const r1 = res1.vCalc;
        pc.prms.Z1.singleValue = 2.2;
        const res2 = pn.CalcSerie();
        const r2 = res2.vCalc;
        expect(r1).not.toBe(r2);
    });

    it("partial PAB chain, bug nghyd#325", () => {
        // tslint:disable-next-line:max-line-length
        const sess = `{ "header": { "source": "jalhyd", "format_version": "1.0", "created": "2019-07-30T08:24:10.711Z" }, "session": [ { "uid": "NjdmM3", "props": { "calcType": 12, "nodeType": 0 }, "meta": { "title": "PAB : chute" }, "children": [], "parameters": [ { "symbol": "Z1", "mode": "SINGLE", "value": 29.99 }, { "symbol": "Z2", "mode": "SINGLE", "value": 26.81 }, { "symbol": "DH", "mode": "CALCUL" } ] }, { "uid": "eWNjdG", "props": { "calcType": 13, "nodeType": 0 }, "meta": { "title": "PAB : nombre" }, "children": [], "parameters": [ { "symbol": "DHT", "mode": "LINK", "targetNub": "NjdmM3", "targetParam": "DH" }, { "symbol": "N", "mode": "SINGLE", "value": 14 }, { "symbol": "DH", "mode": "CALCUL" } ] }, { "uid": "dXM4em", "props": { "calcType": 6, "nodeType": 0 }, "meta": { "title": "PAB : puissance" }, "children": [], "parameters": [ { "symbol": "DH", "mode": "LINK", "targetNub": "eWNjdG", "targetParam": "DH" }, { "symbol": "Q", "mode": "SINGLE", "value": 2.8 }, { "symbol": "V", "mode": "CALCUL" }, { "symbol": "PV", "mode": "SINGLE", "value": 140 } ] }, { "uid": "bzNlaX", "props": { "calcType": 5, "nodeType": 0 }, "meta": { "title": "PAB : dimensions" }, "children": [], "parameters": [ { "symbol": "L", "mode": "SINGLE", "value": 5 }, { "symbol": "W", "mode": "SINGLE", "value": 5 }, { "symbol": "Y", "mode": "CALCUL" }, { "symbol": "V", "mode": "LINK", "targetNub": "dXM4em", "targetParam": "V" } ] }, { "uid": "eG9nM2", "props": { "calcType": 10, "nodeType": 0 }, "meta": { "title": "Cloisons" }, "children": [ { "uid": "ZjMxYT", "props": { "calcType": 7, "nodeType": 5, "structureType": 0, "loiDebit": 11 }, "children": [], "parameters": [ { "symbol": "h1", "mode": "CALCUL" }, { "symbol": "L", "mode": "SINGLE", "value": 0.9 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE", "value": 2.8 }, { "symbol": "Z1", "mode": "LINK", "targetNub": "NjdmM3", "targetParam": "Z1" }, { "symbol": "LB", "mode": "LINK", "targetNub": "bzNlaX", "targetParam": "L" }, { "symbol": "BB", "mode": "LINK", "targetNub": "bzNlaX", "targetParam": "W" }, { "symbol": "PB", "mode": "LINK", "targetNub": "bzNlaX", "targetParam": "Y" }, { "symbol": "DH", "mode": "LINK", "targetNub": "eWNjdG", "targetParam": "DH" } ] } ] }`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        // calc partial chain from PAB-Dimensions
        const pabDim = Session.getInstance().findNubByUid("bzNlaX");
        pabDim.CalcSerie();
        expect(pabDim.result.vCalc).toBeDefined();
        // calc complete chain from Cloisons
        const cloisons = Session.getInstance().findNubByUid("eG9nM2");
        cloisons.CalcSerie();
        expect(cloisons.result.vCalc).toBeDefined();
    });
});

describe("failure in calc chain", () => {

    it("calc chain with no failure", () => {
        const pc = new PabChute(new PabChuteParams(1, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
        pp.calculatedParam = pp.prms.PV;
        Session.getInstance().clear();
        Session.getInstance().registerNubs([ pc, pn, pp ]);
        pn.prms.DHT.defineReference(pc, "DH");
        pp.prms.DH.defineReference(pn, "DH");
        const res = pp.CalcSerie();
        expect(res.globalLog.messages.length).toBe(0);
        expect(res.resultElement.log.messages.length).toBe(0);
    });

    it("calc chain with a failure", () => {
        const pc = new PabChute(new PabChuteParams(1, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
        pp.calculatedParam = pp.prms.PV;
        Session.getInstance().clear();
        Session.getInstance().registerNubs([ pc, pn, pp ]);
        pn.prms.DHT.defineReference(pc, "DH");
        pp.prms.DH.defineReference(pn, "DH");
        pc.prms.Z1.singleValue = 0.1; // Z1 < Z2 : error
        const res = pp.CalcSerie();
        expect(res.globalLog.messages.length).toBe(1);
        expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_IN_CALC_CHAIN);
    });

    it("calc chain with variation, some failures", () => {
        const pc = new PabChute(new PabChuteParams(1, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
        pp.calculatedParam = pp.prms.PV;
        Session.getInstance().clear();
        Session.getInstance().registerNubs([ pc, pn, pp ]);
        pn.prms.DHT.defineReference(pc, "DH");
        pp.prms.DH.defineReference(pn, "DH");
        pc.prms.Z1.singleValue = 1;
        pc.prms.Z2.setValues(0.4, 1.9, 0.5); // 2 first steps succeed, 2 last steps fail (Z1 < Z2)
        const res = pp.CalcSerie();
        expect(res.globalLog.messages.length).toBe(1);
        expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_ERROR_IN_CALC_CHAIN_STEPS);
    });

    it("calc chain with variation, only failures", () => {
        const pc = new PabChute(new PabChuteParams(1, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
        pp.calculatedParam = pp.prms.PV;
        Session.getInstance().clear();
        Session.getInstance().registerNubs([ pc, pn, pp ]);
        pn.prms.DHT.defineReference(pc, "DH");
        pp.prms.DH.defineReference(pn, "DH");
        pc.prms.Z1.singleValue = 1;
        pc.prms.Z2.setValues(1.4, 2.9, 0.5);
        const res = pp.CalcSerie();
        expect(res.globalLog.messages.length).toBe(1);
        expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_IN_CALC_CHAIN);
    });

    describe("iterator bugs −", () => {

        it("jalhyd#224, NaN", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-18T08:05:58.374Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"## Calcul la longueur d'un jet provenant d'un déversoir dénoyé### Exposé du problèmeOn cherche à savoir pour différents débits quelle est la distance parcourue par la nappe d'eau au dessus d'un déversoir au moment où la nappe atteint la crête du déversoir.On va considérer un jet horizontal représentant la surface libre de la nappe au dessus du déversoir et calculer la distance horizontale atteinte lorsque le jet atteint la cote de la crête du déversoir.### Étapes du calculLe module déversoir dénoyé permet de calculer la cote de l'eau à l'amont du déversoir pour débits. Cette cote de l'eau correspond à la cote de départ du jet.La cote de l'eau représentant le point d'impact du jet est égale à la cote de la crête du déversoir.Pour obtenir la vitesse initiale du jet, il faut passer par plusieurs calculs intermédiaires : - Calcul du tirant d'eau entre la crête du déversoir et la cote de l'eau à l'amont du déversoir.- Calcul de la surface hydraulique de l'écoulement au dessus de la crête du déversoir en multipliant le tirant d'eau par la largeur du déversoir- Calcul de la vitesse moyenne de l'écoulement en divisant le débit par la surface hydrauliqueCette vitesse est ensuite utilisée comme vitesse initiale du jet.","session":[{"uid":"eXphMG","props":{"calcType":"Jet"},"meta":{"title":"Longueur du jet"},"children":[],"parameters":[{"symbol":"V0","mode":"LINK","targetNub":"eGxvcn","targetParam":"X"},{"symbol":"S","mode":"SINGLE","value":0},{"symbol":"ZJ","mode":"LINK","targetNub":"dGVrMX","targetParam":"Z1"},{"symbol":"ZW","mode":"LINK","targetNub":"NTRjcn","targetParam":"ZDV"},{"symbol":"ZF","mode":"SINGLE","value":1},{"symbol":"D","mode":"CALCUL"}]},{"uid":"dGVrMX","props":{"calcType":"Dever"},"meta":{"title":"Déver. dénoyés"},"children":[{"uid":"NTRjcn","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1.4},{"symbol":"L","mode":"SINGLE","value":1.75},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"MINMAX","min":0.07,"max":0.1,"step":0.01,"extensionStrategy":0},{"symbol":"Z1","mode":"CALCUL","value":0},{"symbol":"BR","mode":"SINGLE","value":0.7},{"symbol":"ZR","mode":"SINGLE","value":0}]},{"uid":"ZDdraH","props":{"calcType":"YAXB"},"meta":{"title":"Tirant d'eau"},"children":[],"parameters":[{"symbol":"Y","mode":"LINK","targetNub":"dGVrMX","targetParam":"Z1"},{"symbol":"A","mode":"SINGLE","value":1},{"symbol":"X","mode":"CALCUL"},{"symbol":"B","mode":"LINK","targetNub":"NTRjcn","targetParam":"ZDV"}]},{"uid":"NXJzdn","props":{"calcType":"YAXB"},"meta":{"title":"Surface hydraulique"},"children":[],"parameters":[{"symbol":"Y","mode":"CALCUL"},{"symbol":"A","mode":"LINK","targetNub":"NTRjcn","targetParam":"L"},{"symbol":"X","mode":"LINK","targetNub":"ZDdraH","targetParam":"X"},{"symbol":"B","mode":"SINGLE","value":0}]},{"uid":"eGxvcn","props":{"calcType":"YAXB"},"meta":{"title":"Vitesse initiale"},"children":[],"parameters":[{"symbol":"Y","mode":"LINK","targetNub":"dGVrMX","targetParam":"Q"},{"symbol":"A","mode":"LINK","targetNub":"NXJzdn","targetParam":"Y"},{"symbol":"X","mode":"CALCUL"},{"symbol":"B","mode":"SINGLE","value":0}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const jet = Session.getInstance().findNubByUid("eXphMG") as Jet;
            const res = jet.CalcSerie();
            for (const re of res.resultElements) {
                expect(isNaN(re.values.t)).toBe(false);
                expect(isNaN(re.values.Vx)).toBe(false);
                expect(isNaN(re.values.Vz)).toBe(false);
                expect(isNaN(re.values.Vt)).toBe(false);
            }
        });
    });

});
