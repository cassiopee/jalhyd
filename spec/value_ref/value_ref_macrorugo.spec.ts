import { SectionParametree } from "../../src/open-channel/section/section_parametree";
import { CalculatorType } from "../../src/compute-node";
import { cSnRectang } from "../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../src/open-channel/section/section_rectang_params";
import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { Props } from "../../src/props";
import { Session } from "../../src/session";
import { precDist } from "../test_config";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { MethodeResolution } from "../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../src/open-channel/remous";
import { CourbeRemousParams } from "../../src/open-channel/remous_params";
import { Bief } from "../../src/open-channel/bief";
import { BiefParams, BiefRegime } from "../../src/open-channel/bief_params";
import { PenteParams } from "../../src/open-channel/pente_params";
import { Pente } from "../../src/open-channel/pente";
import { Nub } from "../../src/nub";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";

let nub1: MacroRugo;

function createSectionRect(prec: number): cSnRectang {
    return new cSnRectang(
        new ParamsSectionRectang(
            0.8, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        )
    );
}

/**
 * check parameters linkability
 * @param from parameter name linking to ZF1/ZF2
 * @param nub2 source nub holding "from" parameter
 * @param prm1 first target parameter name
 * @param prm2 second target parameter name
 */
function checkLinks(from: string, nub2: Nub, prm1: string, prm2: string) {
    const p = nub2.getParameter(from);
    const lv = nub1.getLinkableValues(p);
    expect(lv.length).toEqual(2);

    expect(lv[0].nub.uid).toEqual(nub1.uid);
    expect(lv[0].symbol).toEqual(prm1);
    expect(lv[0].element.symbol).toEqual(prm1);

    expect(lv[1].nub.uid).toEqual(nub1.uid);
    expect(lv[1].element).toEqual(undefined);
    expect(lv[1].symbol).toEqual(prm2);
}

describe("liens vers une passe à macrorugosités −", () => {
    beforeEach(() => {
        nub1 = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
        Session.getInstance().clear();
        Session.getInstance().registerNub(nub1);
    });

    it("Ks depuis 'section paramétrée'", () => {
        let nub2: SectionParametree = new SectionParametree(createSectionRect(precDist));
        const p = nub2.getParameter("Ks");
        const lv = nub1.getLinkableValues(p);
        expect(lv.length).toEqual(2);

        // bottom roughness, equivalent Strickler (in calculation)
        checkLinks("Ks", nub2, "Ks", "Strickler");
    });

    it("Ks depuis 'régime uniforme'", () => {
        let nub2: RegimeUniforme = new RegimeUniforme(createSectionRect(precDist));

        const p = nub2.getParameter("Ks");
        const lv = nub1.getLinkableValues(p);
        expect(lv.length).toEqual(2);

        // bottom roughness, equivalent Strickler (in calculation)
        checkLinks("Ks", nub2, "Ks", "Strickler");
    });

    it("Ks depuis 'courbe de remous'", () => {
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            10,     // Long= Longueur du bief
            5,      // Dx=Pas d'espace
        );
        const nub2 = new CourbeRemous(createSectionRect(precDist), prem, MethodeResolution.RungeKutta4);

        // bottom roughness, equivalent Strickler (in calculation)
        checkLinks("Ks", nub2, "Ks", "Strickler");
    });

    it("Ks depuis 'cote amont/aval d'un bief'", () => {
        const prms = new BiefParams(
            undefined,  // Z1
            100.4,      // Z2
            100.1,     // ZF1
            100,     // ZF2
            100,        // Long
            5           // Dx
        );

        const nub2 = new Bief(createSectionRect(precDist), prms);
        nub2.regime = BiefRegime.Fluvial;

        // bottom roughness, equivalent Strickler (in calculation)
        checkLinks("Ks", nub2, "Ks", "Strickler");
    });

    it("Y depuis 'régime uniforme'", () => {
        let nub2: RegimeUniforme = new RegimeUniforme(createSectionRect(precDist));

        const p = nub2.getParameter("Y");
        const lv = nub1.getLinkableValues(p);
        expect(lv.length).toEqual(2);

        expect(lv[0].nub.uid).toEqual(nub1.uid);
        expect(lv[0].symbol).toEqual("Y");
        expect(lv[0].element.symbol).toEqual("Y");

        expect(lv[1].nub.uid).toEqual(nub1.uid);
        expect(lv[1].symbol).toEqual("PBH");
        expect(lv[1].element.symbol).toEqual("PBH");
    });

    it("Y depuis 'section paramétrée'", () => {
        let nub2: SectionParametree = new SectionParametree(createSectionRect(precDist));
        const p = nub2.getParameter("Y");
        const lv = nub1.getLinkableValues(p);
        expect(lv.length).toEqual(2);

        expect(lv[0].nub.uid).toEqual(nub1.uid);
        expect(lv[0].symbol).toEqual("Y");
        expect(lv[0].element.symbol).toEqual("Y");

        expect(lv[1].nub.uid).toEqual(nub1.uid);
        expect(lv[1].symbol).toEqual("PBH");
        expect(lv[1].element.symbol).toEqual("PBH");
    });

    it("ZF1/ZF2 depuis 'cote amont/aval d'un bief'", () => {
        const prms = new BiefParams(
            undefined,  // Z1
            100.4,      // Z2
            100.1,     // ZF1
            100,     // ZF2
            100,        // Long
            5           // Dx
        );
        const nub2 = new Bief(createSectionRect(precDist), prms);
        nub2.regime = BiefRegime.Fluvial;

        checkLinks("ZF1", nub2, "ZF1", "ZF2");
        checkLinks("ZF2", nub2, "ZF1", "ZF2");
        checkLinks("Z1", nub2, "ZF1", "ZF2");
        checkLinks("Z2", nub2, "ZF1", "ZF2");
    });

    it("ZF1/ZF2 depuis 'pente'", () => {
        const prms = new PenteParams(
            101,      // Cote amont Z1
            99.5,    // Cote aval Z2
            10,     // Longueur L
            0.15    // Pente I
        );
        const nub2 = new Pente(prms);

        checkLinks("Z1", nub2, "ZF1", "ZF2");
        checkLinks("Z2", nub2, "ZF1", "ZF2");
    });

    it("ZF1/ZF2 depuis 'loi d'ouvrage'", () => {
        const nub2 = new ParallelStructure(new ParallelStructureParams(1.2, 102, 101));
        nub2.addChild(CreateStructure(LoiDebit.GateCem88d, nub2));

        // checkZF12("Z1", nub2);
        checkLinks("Z1", nub2, "ZF1", "ZF2");
        checkLinks("Z2", nub2, "ZF1", "ZF2");
        checkLinks("ZDV", nub2.getChildren()[0], "ZF1", "ZF2");
    });
});
