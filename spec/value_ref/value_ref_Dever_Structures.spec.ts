import { Session } from "../../src/session";
import { DeverParams } from "../../src/structure/dever_params";
import { LoiDebit } from "../../src/structure/structure_props";
import { Dever } from "../../src/structure/dever";
import { CreateStructure } from "../../src/structure/factory_structure";
import { YAXBParams } from "../../src/math/yaxb_params";
import { YAXB } from "../../src/math/yaxb";

describe("Dever and Structures", () => {

    it("Dever results with family undefined, and Structure flow Q, should be linkable to param with family ANY", () => {
        const dever = new Dever(new DeverParams(0.5, 102, 10, 99));
        const s1 = CreateStructure(LoiDebit.RectangularOrificeFree, dever);
        dever.addChild(s1);
        const affine = new YAXB(new YAXBParams(0, 1, 2, 3));
        Session.getInstance().clear();
        Session.getInstance().registerNubs([ dever, affine ]);

        const axlp = Session.getInstance().getLinkableValues(affine.prms.X);
        expect(axlp.length).toBe(12); // Q, Z1, BR, ZR (params) + V, Ec, Cv (results) + ZDV, W, L CdGR (structure params) + Q (structure result)
        expect(axlp.map((p) => p.symbol)).toEqual([ "Q", "Z1", "BR", "ZR", "V", "Ec", "Cv", "ZDV", "W", "L", "CdGR", "Q" ]);
    });

    xit("Structure flow Q in PreBarrage should (not ?) be linkable", () => {
        // @TODO
    });

});
