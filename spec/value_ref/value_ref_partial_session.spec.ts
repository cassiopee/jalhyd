import { ExtensionStrategy, ParamValueMode } from "../../src/index";
import { Session } from "../../src/index";
import { Nub } from "../../src/nub";
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";
import { PabNombre } from "../../src/pab/pab_nombre";
import { PabNombreParams } from "../../src/pab/pab_nombre_params";

function saveAndReload(nubToSave: Nub): Nub {
    // partial saving
    const options: any = {};
    options[nubToSave.uid] = true; // save only this Nub
    const json = Session.getInstance().serialise(options);
    // reloading
    Session.getInstance().clear();
    Session.getInstance().unserialise(json);
    const reloadedNub = Session.getInstance().findNubByUid(nubToSave.uid);
    return reloadedNub;
}

describe("partial session saving/reloading : ", () => {

    it("link to single param", () => {
        // init scenario
        Session.getInstance().clear();
        const pabChute = new PabChute(new PabChuteParams(3, 0.7, 2.3));
        Session.getInstance().registerNub(pabChute);
        const pabNombre = new PabNombre(new PabNombreParams(7, 20, 0.35));
        Session.getInstance().registerNub(pabNombre);
        pabChute.calculatedParam = pabChute.prms.Z2;
        pabNombre.prms.DHT.defineReference(pabChute, "DH");

        const loadedPabNombre = saveAndReload(pabNombre);
        // check values
        expect(loadedPabNombre.getParameter("DHT").valueMode).toEqual(ParamValueMode.SINGLE);
        expect(loadedPabNombre.getParameter("DHT").singleValue).toEqual(2.3);
    });

    it("link to minmax param", () => {
        // init scenario
        Session.getInstance().clear();
        const pabChute = new PabChute(new PabChuteParams(3, 0.7, 2.3));
        Session.getInstance().registerNub(pabChute);
        const pabNombre = new PabNombre(new PabNombreParams(7, 20, 0.35));
        Session.getInstance().registerNub(pabNombre);
        pabChute.calculatedParam = pabChute.prms.Z2;
        pabChute.prms.DH.setValues(2, 2.5, 0.1);
        pabChute.prms.DH.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        pabNombre.prms.DHT.defineReference(pabChute, "DH");

        const loadedPabNombre = saveAndReload(pabNombre);
        // check values
        expect(loadedPabNombre.getParameter("DHT").valueMode).toEqual(ParamValueMode.MINMAX);
        expect(loadedPabNombre.getParameter("DHT").min).toEqual(2);
        expect(loadedPabNombre.getParameter("DHT").max).toEqual(2.5);
        expect(loadedPabNombre.getParameter("DHT").step).toEqual(0.1);
        expect(loadedPabNombre.getParameter("DHT").extensionStrategy).toEqual(ExtensionStrategy.REPEAT_LAST);
    });

    it("link to list param", () => {
        // init scenario
        Session.getInstance().clear();
        const pabChute = new PabChute(new PabChuteParams(3, 0.7, 2.3));
        Session.getInstance().registerNub(pabChute);
        const pabNombre = new PabNombre(new PabNombreParams(7, 20, 0.35));
        Session.getInstance().registerNub(pabNombre);
        pabChute.calculatedParam = pabChute.prms.Z2;
        pabChute.prms.DH.setValues([3.1, 3.2, 3.3]);
        pabChute.prms.DH.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        pabNombre.prms.DHT.defineReference(pabChute, "DH");

        const loadedPabNombre = saveAndReload(pabNombre);
        // check values
        expect(loadedPabNombre.getParameter("DHT").valueMode).toEqual(ParamValueMode.LISTE);
        expect(loadedPabNombre.getParameter("DHT").valueList).toEqual([3.1, 3.2, 3.3]);
        expect(loadedPabNombre.getParameter("DHT").extensionStrategy).toEqual(ExtensionStrategy.REPEAT_LAST);
    });

    it("link to calculated param", () => {
        // init scenario
        Session.getInstance().clear();
        const pabChute = new PabChute(new PabChuteParams(3, 0.6, 2));
        Session.getInstance().registerNub(pabChute);
        const pabNombre = new PabNombre(new PabNombreParams(7, 20, 0.35));
        Session.getInstance().registerNub(pabNombre);
        pabChute.calculatedParam = pabChute.prms.DH;
        pabNombre.prms.DHT.defineReference(pabChute, "DH");

        const loadedPabNombre = saveAndReload(pabNombre);
        // check values
        expect(loadedPabNombre.getParameter("DHT").valueMode).toEqual(ParamValueMode.SINGLE);
        expect(loadedPabNombre.getParameter("DHT").singleValue).toEqual(2.4); // 3 - 0.6
    });

    it("link to calculated variated param", () => {
        // init scenario
        Session.getInstance().clear();
        const pabChute = new PabChute(new PabChuteParams(3, 0.6, 2));
        Session.getInstance().registerNub(pabChute);
        const pabNombre = new PabNombre(new PabNombreParams(7, 20, 0.35));
        Session.getInstance().registerNub(pabNombre);
        pabChute.calculatedParam = pabChute.prms.DH;
        pabChute.prms.Z1.setValues(2.9, 3.1, 0.1);
        pabNombre.prms.DHT.defineReference(pabChute, "DH");

        const loadedPabNombre = saveAndReload(pabNombre);
        // check values
        expect(loadedPabNombre.getParameter("DHT").valueMode).toEqual(ParamValueMode.LISTE);
        expect(loadedPabNombre.getParameter("DHT").valueList).toEqual([2.3, 2.4, 2.5]); // [ 2.9, 3.0, 3.1 ] - 0.6
    });

});
