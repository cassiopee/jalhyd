import { Session } from "../../src/index";
import { PabDimension } from "../../src/pab/pab_dimension";
import { PabDimensionParams } from "../../src/pab/pab_dimensions_params";

let nub1: PabDimension;
let nub2: PabDimension;
let nub3: PabDimension;
let prm1: PabDimensionParams;
let prm2: PabDimensionParams;
let prm3: PabDimensionParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    prm1 = new PabDimensionParams(
        4,      // Longueur L
        1,      // Largeur W
        0.5,    // Tirant d'eau Y
        2       // Volume V
    );
    nub1 = new PabDimension(prm1);

    prm2 = new PabDimensionParams(
        4,      // Longueur L
        1,      // Largeur W
        0.5,    // Tirant d'eau Y
        2       // Volume V
    );
    nub2 = new PabDimension(prm2);

    prm3 = new PabDimensionParams(
        4,      // Longueur L
        1,      // Largeur W
        0.5,    // Tirant d'eau Y
        2       // Volume V
    );
    nub3 = new PabDimension(prm3);

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
    Session.getInstance().registerNub(nub3);

    // liaison 3.L => 2.L => 1.L
    prm3.L.defineReference(nub2, "L");
    prm2.L.defineReference(nub1, "L");
}

describe("références en cascade : ", () => {

    it("paramètre simple", () => {
        createEnv();
        nub1.calculatedParam = prm1.V; // something else than "L"
        expect(prm3.L.referencedValue.isCalculated()).toEqual(false);
        expect(prm3.L.hasMultipleValues).toEqual(false);
    });

    it("paramètre varié", () => {
        createEnv();
        nub1.calculatedParam = prm1.V; // something else than "L"
        prm1.L.setValues(1, 4, 0.15); // vary "L"
        expect(prm3.L.referencedValue.isCalculated()).toEqual(false);
        expect(prm3.L.hasMultipleValues).toEqual(true);
    });

    it("résultat simple", () => {
        createEnv();
        nub1.calculatedParam = prm1.L; // calculate "L"
        expect(prm3.L.referencedValue.isCalculated()).toEqual(true);
        expect(prm3.L.hasMultipleValues).toEqual(false);
    });

    it("paramètre varié", () => {
        createEnv();
        nub1.calculatedParam = prm1.L; // calculate "L"
        prm1.V.setValues(1, 4, 0.15); // vary "V"
        expect(prm3.L.referencedValue.isCalculated()).toEqual(true);
        expect(prm3.L.hasMultipleValues).toEqual(true);
    });

});
