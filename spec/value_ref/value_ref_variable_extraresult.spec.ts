import { JetParams } from "../../src/devalaison/jet_params";
import {  cSnCirc, Jet, Session } from "../../src/index";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { ParamsSectionCirc } from "../../src/open-channel/section/section_circulaire_params";

describe("référence d'un paramètre à un résultat complémentaire multivalué : ", () => {
    it("test 1", () => {
        // master Nub
        const prms1 = new ParamsSectionCirc(
            2, // diamètre
            0.6613,  // tirant d'eau
            40, //  Ks=Strickler
            1.2,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        const sect = new cSnCirc(prms1);
        const nub1 = new RegimeUniforme(sect);
        // variating Z1
        prms1.D.setValues(1, 2, 0.1);
        nub1.calculatedParam = prms1.Q;

        // slave Nub
        const prms2 = new JetParams(5, 0.03, 30, 29.2, 28.5, 3);
        const nub2 = new Jet(prms2);

        Session.getInstance().clear();
        Session.getInstance().registerNub(nub1);
        Session.getInstance().registerNub(nub2);

        // link and calc
        nub2.calculatedParam = prms2.D;
        nub2.prms.V0.defineReference(nub1, "V");
        nub2.CalcSerie();

        const refVals = [ 0.554, 0.574, 0.590, 0.602, 0.613, 0.622, 0.630, 0.636, 0.642, 0.647, 0.652 ];

        let i = 0;
        for (const v of nub2.getParameter("V0").valueList) {
            expect(v).toBeDefined();
            expect(v).toBeCloseTo(refVals[i]);
            i++;
        }
    });
});
