import { CalculatorType } from "../../src/compute-node";
import { Session } from "../../src/index";
import { PabPuissance } from "../../src/pab/pab_puissance";
import { Props } from "../../src/props";

describe("Class PabPuissance", () => {
    describe("jalhyd#132", () => {
        it("Calc(V) should return a successful result", () => {
            const nub =
                Session.getInstance()
                    .createNub(
                        new Props({ calcType: CalculatorType.PabPuissance })
                    ) as PabPuissance;
            nub.prms.DH.setValue(0.227);
            nub.prms.Q.setValue(1.4);
            nub.prms.PV.setValue(140);
            nub.prms.V.setCalculated();
            expect(nub.CalcSerie().ok).toBe(true);
        });
    });
});
