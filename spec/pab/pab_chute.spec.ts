
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";
import { checkResult } from "../test_func";

function pabChuteTest(varTest: string, expected: number) {
    describe("Calc(): ", () => {
        it(varTest + " should be " + expected, () => {
            const prms = new PabChuteParams(
                2,      // Cote Hamont Z1
                0.5,    // Cote aval Z2
                1.5,    // Chute DH
            );

            const nub = new PabChute(prms);
            nub.getParameter(varTest).v = undefined;

            checkResult(nub.Calc(varTest, 0), expected);
        });
    });
}

describe("Class PabChute: ", () => {

    pabChuteTest("Z1", 2);
    pabChuteTest("Z2", 0.5);
    pabChuteTest("DH", 1.5);

    it("Z1 < Z2 should lead to log error entry", () => {
        const prms = new PabChuteParams(
            100,      // Cote amont Z1
            100.5,    // Cote aval Z2
            1.5,      // Chute DH
        );

        const nub = new PabChute(prms);

        expect(nub.Calc("DH", 0).log.messages.length).toBeGreaterThan(0);
    });

});
