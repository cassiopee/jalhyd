import { Session } from "../../src/index";
import { Pab } from "../../src/pab/pab";

describe("Class Pab", () => {
    describe("NgHyd #392", () => {
        it("result.sourceNub should not be undefined", () => {
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-04-23T07:32:02.229Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"bDFwYj","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"d2N6dm","props":{"calcType":"Cloisons"},"children":[{"uid":"MDV5cj","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":1.5},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"Z2","mode":"SINGLE","value":100}],"downWall":{"uid":"MnU0az","props":{"calcType":"CloisonAval"},"children":[{"uid":"cTg4Mn","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}}]}`);
            const pab = Session.getInstance().findNubByUid("bDFwYj") as Pab;
            // pab.prms.Z2.singleValue = 99;
            const res = pab.CalcSerie();
            expect(res).toBeDefined();
            expect(res.ok).toBe(true);
            expect(res.sourceNub).toBeDefined();
        });
    });
});
