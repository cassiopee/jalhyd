import { Session } from "../../src/index";
import { Cloisons } from "../../src/pab/cloisons";

export function getNubJalhyd127(): Cloisons {
    const sessionJson = `{
    "header": {
        "source": "jalhyd",
        "format_version": "1.1",
        "created": "2019-07-30T12:56:46.617Z"
    },
    "session": [
        {
            "uid": "eG9nM2",
            "props": {
                "calcType": "Cloisons",
                "nodeType": 0
            },
            "meta": {
                "title": "Cloisons"
            },
            "children": [
                {
                    "uid": "ZjMxYT",
                    "props": {
                        "calcType": "Structure",
                        "structureType": "SeuilRectangulaire",
                        "loiDebit": "WeirSubmergedLarinier"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "h1",
                            "mode": "CALCUL"
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 0.9
                        },
                        {
                            "symbol": "CdWSL",
                            "mode": "SINGLE",
                            "value": 0.79
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "SINGLE",
                    "value": 2.8
                },
                {
                    "symbol": "Z1",
                    "mode": "SINGLE",
                    "value": 29.99
                },
                {
                    "symbol": "LB",
                    "mode": "SINGLE",
                    "value": 5
                },
                {
                    "symbol": "BB",
                    "mode": "SINGLE",
                    "value": 5
                },
                {
                    "symbol": "PB",
                    "mode": "SINGLE",
                    "value": 1.783
                },
                {
                    "symbol": "DH",
                    "mode": "SINGLE",
                    "value": 0.227
                }
            ]
        }
    ]
}`;
    Session.getInstance().unserialise(sessionJson);
    return Session.getInstance().findNubByUid("eG9nM2") as Cloisons;
}

describe("Class Cloisons jalhyd#127", () => {
    it("Calc(h1) should return 1.866", () => {
        const r = getNubJalhyd127().CalcSerie();
        expect(r.vCalc).toBeCloseTo(1.866, 3);
    });
});
