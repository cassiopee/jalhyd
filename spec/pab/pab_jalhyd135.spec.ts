import { Session } from "../../src/index";
import { Pab } from "../../src/pab/pab";

describe("Pab", () => {
    describe("jalhyd#135", () => {
        it("CalcSerie() should return a correct result", () => {
            const sessionJson = `{
    "header": {
        "source": "jalhyd",
        "format_version": "1.1",
        "created": "2019-08-02T12:14:57.316Z"
    },
    "session": [
        {
            "uid": "cnBhOW",
            "props": {
                "calcType": "Pab"
            },
            "meta": {
                "title": "PAB"
            },
            "children": [
                {
                    "uid": "bGFvOW",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "dm9qc2",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 27.935
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 27.823
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 27.936
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "OXJtZz",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "NXF3cz",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 27.708
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 27.596
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 27.709
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "bTdqZj",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "cHpnY2",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 27.481
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 27.369
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 27.482
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "dXptdn",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "bHJoeX",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 27.254
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 27.141
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 27.255
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "YXFhMG",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "ZHpiY2",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 27.027
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 26.914
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 27.028
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "c3U3Mz",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "Yzk5ej",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 26.8
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 26.687
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 26.801
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "a2Jhem",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "anFvOD",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 26.573
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 26.46
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 26.574
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "eHN3bG",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "ajU5N3",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 26.345
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 26.233
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 26.346
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "bjNsaj",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "cGF5M3",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 26.118
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 26.006
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 26.119
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "a20wZH",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "cTk1YT",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 25.891
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 25.779
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 25.892
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "YTlkdj",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "ZjRzND",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 25.664
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 25.551
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 25.665
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "NzI1aG",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "dGJtdH",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 25.437
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 25.324
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 25.438
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                },
                {
                    "uid": "cmttZj",
                    "props": {
                        "calcType": "Cloisons",
                        "nodeType": "None"
                    },
                    "children": [
                        {
                            "uid": "bTBtcm",
                            "props": {
                                "calcType": "Structure",
                                "structureType": "SeuilRectangulaire",
                                "loiDebit": "WeirSubmergedLarinier"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 25.21
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 0.5
                                },
                                {
                                    "symbol": "CdWSL",
                                    "mode": "SINGLE",
                                    "value": 0.83
                                }
                            ]
                        }
                    ],
                    "parameters": [
                        {
                            "symbol": "LB",
                            "mode": "SINGLE",
                            "value": 4.5
                        },
                        {
                            "symbol": "BB",
                            "mode": "SINGLE",
                            "value": 3.6
                        },
                        {
                            "symbol": "ZRMB",
                            "mode": "SINGLE",
                            "value": 25.097
                        },
                        {
                            "symbol": "ZRAM",
                            "mode": "SINGLE",
                            "value": 25.211
                        },
                        {
                            "symbol": "QA",
                            "mode": "SINGLE",
                            "value": 0
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "SINGLE",
                    "value": 1.53
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z2",
                    "mode": "SINGLE",
                    "value": 26.81
                }
            ],
            "downWall": {
                "uid": "OXhtam",
                "props": {
                    "calcType": "CloisonAval"
                },
                "children": [
                    {
                        "uid": "cDEyaW",
                        "props": {
                            "calcType": "Structure",
                            "structureType": "SeuilRectangulaire",
                            "loiDebit": "WeirSubmergedLarinier"
                        },
                        "children": [],
                        "parameters": [
                            {
                                "symbol": "ZDV",
                                "mode": "SINGLE",
                                "value": 24.983
                            },
                            {
                                "symbol": "L",
                                "mode": "SINGLE",
                                "value": 0.5
                            },
                            {
                                "symbol": "CdWSL",
                                "mode": "SINGLE",
                                "value": 0.83
                            }
                        ]
                    }
                ],
                "parameters": []
            }
        }
    ]
}`;
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("cnBhOW") as Pab;
            expect(nub.CalcSerie().ok).toBe(true);
        });
    });
});
