import { Session } from "../../src/index";
import { Pab } from "../../src/pab/pab";

describe("Pab", () => {
    describe("jalhyd#196", () => {
        it("Jet types should never be undefined", () => {
            const sessionJson = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-01-29T10:53:53.412Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"NzM5YT","props":{"calcType":"Pab"},"meta":{"title":"Doubs-Orchamps-ARTELIA 1"},"children":[{"uid":"bmxzdD","props":{"calcType":"Cloisons"},"children":[{"uid":"Y3hwc2","props":{"calcType":"Structure","structureType":"SeuilTriangulaireTrunc","loiDebit":"TriangularTruncWeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":208.5},{"symbol":"BT","mode":"SINGLE","value":2.4},{"symbol":"ZT","mode":"SINGLE","value":209.3},{"symbol":"CdT","mode":"SINGLE","value":1.36}]},{"uid":"a3k0Y2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":208},{"symbol":"L","mode":"SINGLE","value":1.2},{"symbol":"CdWR","mode":"SINGLE","value":0.39}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":5.2},{"symbol":"BB","mode":"SINGLE","value":6},{"symbol":"ZRMB","mode":"SINGLE","value":207},{"symbol":"ZRAM","mode":"SINGLE","value":207.1},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ajhyaH","props":{"calcType":"Cloisons"},"children":[{"uid":"YnUxcX","props":{"calcType":"Structure","structureType":"SeuilTriangulaireTrunc","loiDebit":"TriangularTruncWeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":207.25},{"symbol":"BT","mode":"SINGLE","value":2.4},{"symbol":"ZT","mode":"SINGLE","value":207.9},{"symbol":"CdT","mode":"SINGLE","value":1.36}]},{"uid":"eTU3Zm","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":206.1},{"symbol":"L","mode":"SINGLE","value":1.2},{"symbol":"CdWR","mode":"SINGLE","value":0.39}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.2},{"symbol":"BB","mode":"SINGLE","value":6},{"symbol":"ZRMB","mode":"SINGLE","value":205.5},{"symbol":"ZRAM","mode":"SINGLE","value":205.55},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":1.317},{"symbol":"Z1","mode":"LISTE","values":[208.72,208.76],"extensionStrategy":0},{"symbol":"Z2","mode":"LISTE","values":[206.34,206.59],"extensionStrategy":0}],"downWall":{"uid":"ZTkwOD","props":{"calcType":"CloisonAval"},"children":[{"uid":"cjJuYX","props":{"calcType":"Structure","structureType":"SeuilTriangulaireTrunc","loiDebit":"TriangularTruncWeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":207.2},{"symbol":"BT","mode":"SINGLE","value":2.4},{"symbol":"ZT","mode":"SINGLE","value":207.9},{"symbol":"CdT","mode":"SINGLE","value":1.36}]},{"uid":"YTA0ND","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":205.7},{"symbol":"L","mode":"SINGLE","value":1.2},{"symbol":"CdWR","mode":"SINGLE","value":0.39}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":205.45}]}}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("NzM5YT") as Pab;
            const res = nub.CalcSerie();
            let childIndex = 0;
            // cloisons
            for (const c of nub.children.concat()) {
                childIndex++;
                let deviceIndex = 0;
                for (const device of c.getChildren()) {
                    deviceIndex++;
                    let reIndex = 0;
                    for (const re of device.result.resultElements) {
                        reIndex++;
                        expect(re.values.ENUM_StructureFlowMode).toBeDefined(`StructureFlowMode at resultElement ${reIndex}, device ${deviceIndex} of child ${childIndex}`);
                        expect(re.values.ENUM_StructureFlowRegime).toBeDefined(`StructureFlowRegime at resultElement ${reIndex}, device ${deviceIndex} of child ${childIndex}`);
                        expect(re.values.ENUM_StructureJetType).toBeDefined(`StructureJetType at resultElement ${reIndex}, device ${deviceIndex} of child ${childIndex}`);
                    }
                }
            }
            // downwall
            let dwDeviceIndex = 0;
            for (const device of nub.downWall.getChildren()) {
                dwDeviceIndex++;
                let reIndex = 0;
                for (const re of device.result.resultElements) {
                    reIndex++;
                    expect(re.values.ENUM_StructureFlowMode).toBeDefined(`StructureFlowMode at resultElement ${reIndex}, device ${dwDeviceIndex} of downwall`);
                    expect(re.values.ENUM_StructureFlowRegime).toBeDefined(`StructureFlowRegime at resultElement ${reIndex}, device ${dwDeviceIndex} of downwall`);
                    expect(re.values.ENUM_StructureJetType).toBeDefined(`StructureJetType at resultElement ${reIndex}, device ${dwDeviceIndex} of downwall`);
                }
            }
        });
    });
});
