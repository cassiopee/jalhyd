import { getEmptyCloisonsTest } from "./cloisons.spec";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";

describe("Cloison Jalhyd #242", () => {
    it("Orifice in cloison should not return hauteur de pelle", () => {
        const cloisons = getEmptyCloisonsTest();
        cloisons.addChild(CreateStructure(LoiDebit.OrificeSubmerged));
        cloisons.prms.Q.setCalculated();
        expect(cloisons.CalcSerie().values.P).toBeUndefined();
    });
});