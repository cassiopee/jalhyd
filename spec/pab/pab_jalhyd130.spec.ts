import { CalculatorType } from "../../src/compute-node";
import { Session } from "../../src/index";
import { Pab } from "../../src/pab/pab";
import { Props } from "../../src/props";
import { getNubJalhyd127 } from "./cloisons_jalhyd127.spec";

describe("Class Pab", () => {
    describe("Jalhyd #130", () => {
        it("first wall ZDV of created Pab should return 28.124", () => {
            const cl = getNubJalhyd127();
            cl.CalcSerie();
            const pab = Session.getInstance().createNub(new Props({ calcType: CalculatorType.Pab })) as Pab;
            pab.prms.Q.singleValue = cl.prms.Q.V;
            pab.prms.Z1.singleValue = cl.prms.Z1.V;
            pab.prms.Z2.singleValue = 26.810;
            pab.addCloisonsFromModel(cl, 14);
            expect(pab.children[0].structures[0].prms.ZDV.currentValue).toBeCloseTo(28.124, 3);
        });
    });
});
