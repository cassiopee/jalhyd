import { CloisonAval } from "../../src/pab/cloison_aval";
import { CloisonsAvalParams } from "../../src/pab/cloison_aval_params";
import { StructureVanLevLarinier } from "../../src/structure/structure_vanlev_larinier";
import { StructureVanLevParams } from "../../src/structure/structure_vanlev_params";
import { StructureVanLevVillemonte } from "../../src/structure/structure_vanlev_villemonte";
import { MessageCode } from "../../src/util/message";

function getCloisonAvalTest(Q: number, Z2: number): CloisonAval {
    return new CloisonAval(
        new CloisonsAvalParams(Q, 0, Z2, 0)
    );
}

let ca: CloisonAval;
let s: StructureVanLevVillemonte | StructureVanLevLarinier;
describe("class CloisonAval", () => {
    describe("StructureVanLevVillemonte", () => {
        beforeEach(() => {
            ca = getCloisonAvalTest(0.773, 74.86);
            ca.addChild(
                new StructureVanLevVillemonte(
                    new StructureVanLevParams(0.773, 73, 75.090, 74.86, 0.6, 0.4, 0.217, 73.5, 74),
                    false
                )
            );
            ca.calculatedParam = ca.prms.Z1;
            s = ca.structures[0] as StructureVanLevVillemonte;
        });
        it("Calc(Z1) should return 75.077 and extraResults.ZDV should be 73.95", () => {
            const r = ca.CalcSerie();
            expect(r.vCalc).toBeCloseTo(75.077, 3);
            expect(r.values.ZDV).toBeCloseTo(73.95, 2);
        });
        it("ZDV min bound Calc(Z1) should return 75.059 and extraResults.ZDV should be 73.9", () => {
            s.prms.maxZDV.singleValue = 73.90;
            const r = ca.CalcSerie();
            expect(r.vCalc).toBeCloseTo(75.059, 3);
            expect(r.values.ZDV).toBeCloseTo(73.9, 2);
            expect(r.log.messages[0].code).toBe(MessageCode.WARNING_VANLEV_ZDV_SUP_MAX);
        });
        it("ZDV max bound Calc(Z1) should return 75.096 and extraResults.ZDV should be 74", () => {
            s.prms.minZDV.singleValue = 74;
            const r = ca.CalcSerie();
            expect(r.vCalc).toBeCloseTo(75.096, 3);
            expect(r.values.ZDV).toBeCloseTo(74, 2);
            expect(r.log.messages[0].code).toBe(MessageCode.WARNING_VANLEV_ZDV_INF_MIN);
        });
    });

    describe("StructureVanLevLarinier", () => {
        beforeEach(() => {
            ca = getCloisonAvalTest(0.773, 74.86);
            ca.addChild(
                new StructureVanLevLarinier(
                    new StructureVanLevParams(0.773, 73, 75.090, 74.86, 0.35, 0.65, 0.217, 73.3, 73.5),
                    false
                )
            );
            ca.calculatedParam = ca.prms.Z1;
            s = ca.structures[0] as StructureVanLevLarinier;
        });
        it("Calc(Z1) should return 75.077 and extraResults.ZDV should be 73.431", () => {
            expect(ca.CalcSerie().vCalc).toBeCloseTo(75.077, 3);
            expect(ca.CalcSerie().values.ZDV).toBeCloseTo(73.431, 2);
        });
        it("ZDV min bound Calc(Z1) should return 75.071 and extraResults.ZDV should be 73.4", () => {
            s.prms.maxZDV.singleValue = 73.40;
            const r = ca.CalcSerie();
            expect(r.vCalc).toBeCloseTo(75.071, 3);
            expect(r.values.ZDV).toBeCloseTo(73.4, 2);
            expect(r.log.messages[0].code).toBe(MessageCode.WARNING_VANLEV_ZDV_SUP_MAX);
        });
        it("ZDV max bound Calc(Z1) should return 75.092 and extraResults.ZDV should be 73.5", () => {
            s.prms.minZDV.singleValue = 73.5;
            const r = ca.CalcSerie();
            expect(r.vCalc).toBeCloseTo(75.092, 3);
            expect(r.values.ZDV).toBeCloseTo(73.5, 2);
            expect(r.log.messages[0].code).toBe(MessageCode.WARNING_VANLEV_ZDV_INF_MIN);
        });
    });
});
