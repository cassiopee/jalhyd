import { CreateStructure, MessageCode, Session } from "../../src/index";
import { LoiDebit } from "../../src/structure/structure_props";
import { Cloisons } from "../../src/pab/cloisons";
import { CloisonsParams } from "../../src/pab/cloisons_params";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { CreateParalleleStructureTest, testParallelStructures } from "../structure/functions";

export function getEmptyCloisonsTest(bParallelTest: boolean = false): Cloisons {
    const cl = new Cloisons(
        new CloisonsParams(
            0,      // Débit total (m3/s)
            102,    // Cote de l'eau amont (m)
            10,     // Longueur des bassins (m)
            1,      // Largeur des bassins (m)
            1,      // Profondeur moyenne (m)
            0.5     // Hauteur de chute (m)
        ),
        false       // debug
    );
    if(bParallelTest) {
        cl.prms.DH.setValue(0.6);
    }
    return cl;
}

function getCloisonsTest(): Cloisons {
    const cloisons: Cloisons = getEmptyCloisonsTest();
    cloisons.addChild(CreateStructure(LoiDebit.WeirSubmergedLarinier));
    cloisons.calculatedParam = cloisons.prms.Q;
    cloisons.structures[0].prms.h1.singleValue = 2;
    return cloisons;
}

describe("Class Cloisons: ", () => {
    describe("Calc(Q) Fente noyée (Larinier 1992)", () => {
        let c: Cloisons;
        beforeEach(() => {
            c = getCloisonsTest();
        });
        it("vCalc should return 0.94", () => {
            expect(c.CalcSerie().vCalc).toBeCloseTo(0.94, 3);
        });
        it("extraResults.PV should return 460.887", () => {
            expect(c.CalcSerie().values.PV).toBeCloseTo(460.887, 1);
        });
        it("ZRMB should be 100.5", () => {
            c.CalcSerie();
            expect(c.prms.ZRMB.v).toBeCloseTo(100.5, 3);
        });
        it("Calc(DH) should return 0.5", () => {
            c.calculatedParam = c.prms.Q;
            c.prms.Q.singleValue = c.CalcSerie().vCalc;
            c.calculatedParam = c.prms.DH;
            c.calculatedParam.singleValue = 100;
            expect(c.CalcSerie().vCalc).toBeCloseTo(0.5, 3);
        });
    });

    describe("testPS", () => {
        testParallelStructures(CreateParalleleStructureTest(getEmptyCloisonsTest(true)));
    });

    describe("Exemple Formation Cassiopée 2018-09", () => {
        it("Calc(Z1) Exemple Formation Cassiopée 2018-09", () => {
            // Modèle de cloison
            const modelCloisons = new Cloisons(
                new CloisonsParams(
                    0,      // Débit total (m3/s)
                    78.27,    // Cote de l'eau amont (m)
                    3.1,     // Longueur des bassins (m)
                    2.5,      // Largeur des bassins (m)
                    1.5,      // Profondeur moyenne (m)
                    0.23     // Hauteur de chute (m)
                )
            );

            // Ajout d'ouvrage dans la cloison
            modelCloisons.addChild(CreateStructure(LoiDebit.WeirSubmergedLarinier));
            const fPrms = modelCloisons.structures[0].prms as RectangularStructureParams;
            fPrms.h1.singleValue = 1.6;
            fPrms.L.singleValue = 0.35;
            fPrms.CdWSL.singleValue = 0.65;
            modelCloisons.calculatedParam = modelCloisons.prms.Q;
            const res = modelCloisons.CalcSerie();
            expect(res.vCalc).toBeCloseTo(0.773, 2);
            expect(res.values.PV).toBeCloseTo(150.1, 1);
            expect(res.values.ZRMB).toBeCloseTo(76.54, 2);
            expect(modelCloisons.structures[0].result.values.ZDV).toBeCloseTo(76.67, 2);
            expect(modelCloisons.structures[0].result.values.P).toBeCloseTo(0.015, 3);
            expect(modelCloisons.structures[0].result.log.messages.length).toBe(0);
        });
    });

    describe("Pelle négative − ", () => {
        it("un avertissement doit être présent dans le log", () => {
            // Modèle de cloison
            const modelCloisons = new Cloisons(
                new CloisonsParams(
                    0,      // Débit total (m3/s)
                    78.27,    // Cote de l'eau amont (m)
                    3.1,     // Longueur des bassins (m)
                    2.5,      // Largeur des bassins (m)
                    0.5,      // Profondeur moyenne (m)
                    0.23     // Hauteur de chute (m)
                )
            );

            // Ajout d'ouvrage dans la cloison
            modelCloisons.addChild(CreateStructure(LoiDebit.WeirSubmergedLarinier));
            const fPrms = modelCloisons.structures[0].prms as RectangularStructureParams;
            fPrms.h1.singleValue = 1.6;
            fPrms.L.singleValue = 0.35;
            fPrms.CdWSL.singleValue = 0.65;
            modelCloisons.calculatedParam = modelCloisons.prms.Q;
            const res = modelCloisons.CalcSerie();
            expect(res.vCalc).toBeCloseTo(0.773, 2);
            // pelle négative
            expect(modelCloisons.structures[0].result.values.ZDV).toBeCloseTo(76.67, 2);
            expect(modelCloisons.prms.ZRAM.v).toBeCloseTo(77.655, 3);
            expect(modelCloisons.structures[0].result.values.P).toBeCloseTo(-0.985, 3);
            expect(modelCloisons.structures[0].result.log.messages.length).toBe(1);
            expect(modelCloisons.structures[0].result.log.messages[0].code).toBe(MessageCode.WARNING_NEGATIVE_SILL);
        });

        it("deux avertissements doivent être présents dans le log", () => {
            // tslint:disable-next-line:max-line-length
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-12-02T09:15:48.011Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"anN5cW","props":{"calcType":"Cloisons"},"meta":{"title":"Cloisons"},"children":[{"uid":"M3lmeH","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"h1","mode":"SINGLE","value":1},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.65}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"PB","mode":"SINGLE","value":0.1},{"symbol":"DH","mode":"SINGLE","value":0.5}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const cloisons = Session.getInstance().findNubByUid("anN5cW") as Cloisons;
            const res = cloisons.CalcSerie();
            expect(cloisons.structures[0].result.values.P).toBeCloseTo(-0.65, 3);
            expect(cloisons.structures[0].result.log.messages.length).toBe(2);
            expect(cloisons.structures[0].result.log.messages[0].code).toBe(
                MessageCode.WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09
            );
            expect(cloisons.structures[0].result.log.messages[1].code).toBe(MessageCode.WARNING_NEGATIVE_SILL);
        });
    });
    describe("jalhyd#355", () => {
        it("Z1 should be undefined when nullParams = true", () => {
            const cl = new Cloisons(
                new CloisonsParams(
                    1.5,      // Débit total (m3/s)
                    102,    // Cote de l'eau amont (m)
                    10,     // Longueur des bassins (m)
                    1,      // Largeur des bassins (m)
                    1,      // Profondeur moyenne (m)
                    0.5,     // Hauteur de chute (m)
                    true
                )
            );
            expect(cl.prms.Z1.v).toBe(undefined);
        });
    });
});
