
import { PabNombre } from "../../src/pab/pab_nombre";
import { PabNombreParams } from "../../src/pab/pab_nombre_params";
import { checkResult } from "../test_func";

function pabNombreTest(varTest: string, expected: number) {
    describe("Calc(): ", () => {
        it(varTest + " should be " + expected, () => {
            const prms = new PabNombreParams(
                7.5,      // Chute totale DHT
                10,    // Nombre de bassins N
                0.75,    // Chute entre bassins DH
            );

            const nub = new PabNombre(prms);
            prms.get(varTest).v = undefined;

            checkResult(nub.Calc(varTest, 0), expected);
        });
    });
}

describe("Class PabNombre: ", () => {

    pabNombreTest("DHT", 7.5);
    pabNombreTest("N", 10);
    pabNombreTest("DH", 0.75);

    it("DHR should be 0.3, ", () => {
        const prms = new PabNombreParams(
            7.5,      // Chute totale DHT
            666,    // Nombre de bassins N
            0.8,    // Chute entre bassins DH
        );

        const nub = new PabNombre(prms);

        nub.Calc("N", 0);
        expect(nub.result.vCalc).toBe(9);
        expect(nub.result.getValue("DHR")).toBeCloseTo(0.3, 3);
        expect(nub.result.getValue("NB")).toBe(9);
        expect(nub.result.getValue("DHB")).toBeCloseTo(0.833, 3);
        expect(nub.result.getValue("NH")).toBe(10);
        expect(nub.result.getValue("DHH")).toBeCloseTo(0.75, 3);
    });

    it("DHR should be 0, NB, NH, DHB and DHH should not be defined", () => {
        const prms = new PabNombreParams(
            3,      // Chute totale DHT
            666,    // Nombre de bassins N
            0.2,    // Chute entre bassins DH
        );

        const nub = new PabNombre(prms);

        nub.Calc("N", 0);
        expect(nub.result.vCalc).toBe(15);
        expect(nub.result.getValue("DHR")).toBe(0);
        expect(nub.result.getValue("NB")).toBeUndefined();
        expect(nub.result.getValue("DHB")).toBeUndefined();
        expect(nub.result.getValue("NH")).toBeUndefined();
        expect(nub.result.getValue("DHH")).toBeUndefined();
    });

    it("non-integer number of basins should lead to log error entry", () => {
        const prms = new PabNombreParams(
            3,      // Chute totale DHT
            4.5,    // Nombre de bassins N
            0.2,    // Chute entre bassins DH
        );

        const nub = new PabNombre(prms);

        expect(nub.Calc("DH", 0).log.messages.length).toBeGreaterThan(0);
    });

    it("when N is 0, NB and DHB should not be defined", () => {
        const prms = new PabNombreParams(
            7.5,      // Chute totale DHT
            666,    // Nombre de bassins N
            8,    // Chute entre bassins DH
        );

        const nub = new PabNombre(prms);

        nub.Calc("N", 0);
        expect(nub.result.vCalc).toBe(0);
        expect(nub.result.getValue("DHR")).toBeCloseTo(7.5, 3);
        expect(nub.result.getValue("NB")).toBeUndefined();
        expect(nub.result.getValue("DHB")).toBeUndefined();
        expect(nub.result.getValue("NH")).toBe(1);
        expect(nub.result.getValue("DHH")).toBeCloseTo(7.5, 3);
    });
});
