import { CloisonAval } from "../../src/pab/cloison_aval";
import { CloisonsAvalParams } from "../../src/pab/cloison_aval_params";
import { Cloisons } from "../../src/pab/cloisons";
import { CloisonsParams } from "../../src/pab/cloisons_params";
import { Pab } from "../../src/pab/pab";
import { PabParams } from "../../src/pab/pab_params";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureVanLevParams } from "../../src/structure/structure_vanlev_params";
import { StructureVanLevVillemonte } from "../../src/structure/structure_vanlev_villemonte";
import { StructureWeirSubmergedLarinier } from "../../src/structure/structure_weir_submerged_larinier";
import { StructureWeirVillemonte } from "../../src/structure/structure_weir_villemonte";
import { MessageCode } from "../../src/util/message";
import { ParamValueMode } from "../../src/param/param-value-mode";
import { Session } from "../../src/session";

const dbg: boolean = false;

/**
 * Exemple formation Cassiopée 2018-09
 */

function createModelCloisonTest(): Cloisons {
    // Modèle de cloison
    const modelCloisons = new Cloisons(
        new CloisonsParams(
            0,      // Débit total (m3/s)
            78.27,    // Cote de l'eau amont (m)
            3.1,     // Longueur des bassins (m)
            2.5,      // Largeur des bassins (m)
            1.5,      // Profondeur moyenne (m)
            0.23     // Hauteur de chute (m)
        )
    );

    const rectStructPrms = new RectangularStructureParams(
        0,  // Q
        76.67,        // ZDV
        78.27,        // Z1 entered here for good assignation of h1
        0,      // Z2
        0.35,          // L
        0.65        // Cd pour un seuil rectangulaire
        // W = Infinity par défaut pour un seuil
    );

    // Ajout d'ouvrage dans la cloison
    modelCloisons.addChild(new StructureWeirSubmergedLarinier(rectStructPrms));

    return modelCloisons;
}

function createPabTest(): Pab {
    // Création de la cloison aval
    const downWall = new CloisonAval(new CloisonsAvalParams(0, 0, 0, 0));
    const kiviPrms = new RectangularStructureParams (
        0,  // Q
        73.95,        // ZDV
        0,        // Z1
        0,      // Z2
        0.6,          // L
        0.4        // Cd pour un seuil rectangulaire
    );
    downWall.addChild(new StructureWeirVillemonte(kiviPrms));

    // Création de la passe
    const p: Pab = new Pab(
        new PabParams(
            0.773,
            78.27,
            74.86
        ),
        downWall,
        dbg
    );

    // Ajout des cloisons
    p.addCloisonsFromModel(createModelCloisonTest(), 15);
    p.downWall = downWall;
    return p;
}

function TestGeometry(b: Cloisons, ZRMB: number, ZRAM: number, ZDV: number) {
    it(`ZRMB should be equal to ${ZRMB}`, () => {
        expect(b.prms.ZRMB.currentValue).toBeCloseTo(ZRMB, 3);
    });
    it(`ZRAM should be equal to ${ZRAM}`, () => {
        expect(b.prms.ZRAM.currentValue).toBeCloseTo(ZRAM, 3);
    });
    it(`ZDV should be equal to ${ZDV}`, () => {
        expect(b.structures[0].prms.ZDV.currentValue).toBeCloseTo(ZDV, 3);
    });
}

function checkPabResults(p: Pab, vCalc: number) {
    p.CalcSerie();
    // Résultat du calcul principal (Z1 ou Q)
    expect(p.result.vCalc).toBeCloseTo(vCalc, 2);
    // Résultat Ligne d'eau (Cote de l'eau, P/V, Tmoy, DH dans les 14 bassins)
    const tRef = [
        [78.270, 150.032, 1.500, 0.230],
        [78.040, 149.932, 1.501, 0.230],
        [77.811, 149.932, 1.501, 0.230],
        [77.581, 149.932, 1.501, 0.230],
        [77.351, 149.181, 1.502, 0.229],
        [77.122, 149.081, 1.503, 0.229],
        [76.893, 148.982, 1.504, 0.229],
        [76.664, 148.883, 1.505, 0.229],
        [76.435, 148.036, 1.507, 0.228],
        [76.207, 147.840, 1.509, 0.228],
        [75.979, 146.900, 1.512, 0.227],
        [75.752, 145.867, 1.516, 0.226],
        [75.526, 144.744, 1.521, 0.225],
        [75.301, 143.534, 1.527, 0.224]
    ];
    // Cote de l'eau dernier bassin à l'amont de la cloison aval
    expect(p.downWall.result.vCalc).toBeCloseTo(75.077, 2);
    expect(p.downWall.result.values.DH).toBeCloseTo(0.217, 2);
    expect(p.downWall.result.values.Q).toBeCloseTo(0.773, 2);
    expect(p.downWall.result.values.x).toBeCloseTo(3.1 * 14, 2);
    for (let i = 0; i < 14; i++) {
        // Cote de l'eau à l'amont de la cloison amont du bassin
        expect(p.children[i].result.vCalc).toBeCloseTo(tRef[i][0], 2);
        // Puissance volumique dissipée
        expect(p.children[i].result.values.PV).toBeCloseTo(tRef[i][1], 0);
        // Tirant d'eau moyen
        expect(p.children[i].result.values.YMOY).toBeCloseTo(tRef[i][2], 2);
        // Chute
        expect(p.children[i].result.values.DH).toBeCloseTo(tRef[i][3], 2);
        // Débits
        expect(p.children[i].result.values.Q).toBeCloseTo(0.773, 2);
        expect(p.children[i].result.values.QA).toBeCloseTo(0, 2);
        // Cote radier amont
        expect(p.children[i].result.values.ZRAM).toBeCloseTo(73.665 + (13 - i) * 0.23, 2);
        // Cote radier mi-bassin
        expect(p.children[i].result.values.ZRMB).toBeCloseTo(73.550 + (13 - i) * 0.23, 2);
        expect(p.children[i].result.values.x).toBeCloseTo(3.1 * i, 2);
    }
}

// Tests
let pab: Pab = createPabTest();
describe("Class Pab: ", () => {
    beforeEach( () => {
        pab = createPabTest();
    });
    describe("Exemple Formation 2018-09 p.14", () => {
        let ZRMB = 76.54;
        let ZDV = 76.67;
        let i: number = 0;
        for (const b of pab.children) {
            describe(`Geometry Basin #${i}`, () => {
                TestGeometry(
                    b, ZRMB,
                    ZRMB + 0.23 / 2,
                    ZDV);
            });
            i++;
            ZRMB -= 0.23;
            ZDV -= 0.23;
        }
        it("CalcSerie(Z1) should return 78.27", () => {
            pab.calculatedParam = pab.prms.Z1;
            checkPabResults(pab, 78.27);
        });
        it("CalcSerie(Q) should return 0.773", () => {
            pab.calculatedParam = pab.prms.Q;
            checkPabResults(pab, 0.773);
        });
    });

    describe("Vanne levante sur Cloison aval", () => {
        beforeEach( () => {
            pab = createPabTest();
            pab.downWall = new CloisonAval(new CloisonsAvalParams(0, 0, 0, 0));
            pab.downWall.addChild(
                new StructureVanLevVillemonte(
                    new StructureVanLevParams(0.773, 73, 75.090, 74.86, 0.6, 0.4, 0.217, 73.5, 74)
                )
            );
        });
        it("CalcSerie(Z1) should return 78.27", () => {
            pab.calculatedParam = pab.prms.Z1;
            checkPabResults(pab, 78.27);
        });
        it("CalcSerie(Q) should return 0.773", () => {
            pab.calculatedParam = pab.prms.Q;
            checkPabResults(pab, 0.773);
        });
        it("CalcSerie(Z1) should return an error", () => {
            pab.downWall.addChild(
                new StructureVanLevVillemonte(
                    new StructureVanLevParams(0.773, 73, 75.090, 74.86, 0.6, 0.4, 0.217, 73.5, 74)
                )
            );
            pab.calculatedParam = pab.prms.Z1;
            expect(pab.CalcSerie().globalLog.messages[0].code)
                .toBe(MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE);
        });
    });

    describe("when Z1 is too low compared to ZDV", () => {
        it("logs should appear if dichotomy does not converge", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.singleValue = 76.60;
            pab.CalcSerie();
            expect(pab.result.hasLog()).toBe(true);
            expect(pab.result.resultElement.log.messages[0].code)
                .toBe(MessageCode.ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL); // @TODO or other error message ?
        });
    });

    describe("variating Z1", () => {
        it("logs should appear when Z1 < Z2", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.setValues(73, 85, 6);
            pab.CalcSerie();
            expect(pab.result.resultElements.length).toBe(3);
            expect(pab.result.hasLog()).toBe(true);
            expect(pab.result.resultElements[0].log.messages[0].code).toBe(MessageCode.ERROR_PAB_Z1_LOWER_THAN_Z2);
        });

        it("erroneous first cases should not prevent subsequent cases from succeeding", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.setValues(73, 85, 6);
            pab.CalcSerie();
            // for ex. Cloisons n°1
            expect(pab.children[0].result.resultElements.length).toBe(3);
            expect(Object.keys(pab.children[0].result.resultElements[1].extraResults).length).toBe(8);
        });
    });

    describe("calculated parameter should always be Q or Z1 - ", () => {
        it("try setting Q and Z1 to SINGLE", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.valueMode = ParamValueMode.SINGLE;
            expect(pab.calculatedParam.symbol).toBe("Q");
            pab.prms.Q.valueMode = ParamValueMode.SINGLE;
            expect(pab.calculatedParam.symbol).toBe("Z1");
        });

        it("try setting Q and Z1 to MINMAX", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.valueMode = ParamValueMode.MINMAX;
            expect(pab.calculatedParam.symbol).toBe("Q");
            pab.prms.Q.valueMode = ParamValueMode.MINMAX;
            expect(pab.calculatedParam.symbol).toBe("Z1");
        });

        it("try setting Q and Z1 to LISTE", () => {
            pab.calculatedParam = pab.prms.Q;
            pab.prms.Z1.valueMode = ParamValueMode.LISTE;
            expect(pab.calculatedParam.symbol).toBe("Q");
            pab.prms.Q.valueMode = ParamValueMode.LISTE;
            expect(pab.calculatedParam.symbol).toBe("Z1");
        });

        it("try setting Q and Z1 to LINK", () => {
            // clone PAB
            const pab2 = createPabTest();
            Session.getInstance().clear();
            Session.getInstance().registerNub(pab);
            Session.getInstance().registerNub(pab2);
            // link
            pab.prms.Z1.defineReference(pab2, "Z1");
            expect(pab.calculatedParam.symbol).toBe("Q");
            expect(pab.prms.Z1.valueMode).toBe(ParamValueMode.LINK);
            pab.prms.Q.defineReference(pab2, "Q");
            expect(pab.calculatedParam.symbol).toBe("Z1");
            expect(pab.prms.Q.valueMode).toBe(ParamValueMode.LINK);
        });
    });

    describe("errors inherited from Cloisons −", () => {

        it("negative sill warning should be present", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-06T14:44:44.505Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"M29hc3","props":{"calcType":"Pab"},"meta":{"title":"PAB 1"},"children":[{"uid":"eDllOD","props":{"calcType":"Cloisons"},"children":[{"uid":"ZGMwc3","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":98},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"cGs5OX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":2.349},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101.1}],"downWall":{"uid":"NzBlcD","props":{"calcType":"CloisonAval"},"children":[{"uid":"cG5xdX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":97.5},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"aGM1ZG","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100.5},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.25}]}}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pab2 = Session.getInstance().findNubByUid("M29hc3") as Pab;
            pab2.CalcSerie();
            expect(pab2.children[0].result.log.messages.length).toBe(2);
            expect(pab2.children[0].result.log.messages[0].code).toBe(MessageCode.WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09);
            expect(pab2.children[0].result.log.messages[1].code).toBe(MessageCode.WARNING_NEGATIVE_SILL);
        });
    });

    describe("linking QA", () => {

        it("when there are more than 1 Cloison, QA should be linkable to its siblings", () => {
            Session.getInstance().clear();
            Session.getInstance().registerNub(pab);
            const lv = Session.getInstance().getLinkableValues(pab.children[0].prms.QA);
            expect(lv.length).toBe(13); // example PAB has 14 walls
        });

        it("when there are more than 1 Pab, QA should be linkable to other Pab's QAs", () => {
            const pab2 = createPabTest();
            Session.getInstance().clear();
            // keep only one wall
            for (let i=0; i < 13; i++) {
                pab.deleteChild(13 - i);
                pab2.deleteChild(13 - i);
            }
            expect(pab.children.length).toBe(1);
            expect(pab2.children.length).toBe(1);
            Session.getInstance().registerNub(pab);
            Session.getInstance().registerNub(pab2);
            const lv = Session.getInstance().getLinkableValues(pab.children[0].prms.QA);
            expect(lv.length).toBe(1); // the only wall of the 2nd PAB
        });
    });
});
