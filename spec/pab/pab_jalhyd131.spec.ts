import { BoolIdentity } from "../../src/base";
import { Session } from "../../src/index";
import { Pab } from "../../src/pab/pab";

function getPabJalhyd131(): Pab {
    const sessionJson = `{
        "header": {
            "source": "jalhyd",
            "format_version": "1.1",
            "created": "2019-08-01T10:02:00.255Z"
        },
        "session": [
            {
                "uid": "Y2p6eW",
                "props": {
                    "calcType": "Pab"
                },
                "meta": {
                    "title": "PAB"
                },
                "children": [
                    {
                        "uid": "dXk1MH",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "MngyYW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularTruncWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 518801.401
                                    },
                                    {
                                        "symbol": "BT",
                                        "mode": "SINGLE",
                                        "value": 742993.351
                                    },
                                    {
                                        "symbol": "ZT",
                                        "mode": "SINGLE",
                                        "value": 903631.767
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "Y3Uxc2",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1893811.575
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "YjNzcG",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 316673.44
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 910947.967
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 312319.265
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 945910.009
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 80751.508
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 660145.622
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1802348.165
                            }
                        ]
                    },
                    {
                        "uid": "NnBnMn",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "dzZhd3",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularTruncWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 610726.577
                                    },
                                    {
                                        "symbol": "BT",
                                        "mode": "SINGLE",
                                        "value": 257380.136
                                    },
                                    {
                                        "symbol": "ZT",
                                        "mode": "SINGLE",
                                        "value": 424359.944
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 658024.883
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 765771.949
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 218586.805
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1831016.817
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1369861.066
                            }
                        ]
                    },
                    {
                        "uid": "bWQzan",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "cXoyeW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 978697.074
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "NWdsbz",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirSubmergedLarinier"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 586585.217
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 146567.709
                                    },
                                    {
                                        "symbol": "CdWSL",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "aGlieW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 988317.886
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 396094.358
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 741721.216
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 121395.993
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 467055.795
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 773838.002
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1994063.754
                            }
                        ]
                    },
                    {
                        "uid": "Y3hzYT",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "eWt6c3",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 304509.338
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 529021.64
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 593737.766
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1316042.829
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1118971.551
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 629474.192
                            }
                        ]
                    },
                    {
                        "uid": "MnBvaj",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "NHR0a2",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 88333.265
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "cHlwNm",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 288318.744
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "Mm9mbG",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 129618.309
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 925239.668
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 622355.397
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 758301.283
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1632436.136
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1744853.777
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1748091.688
                            }
                        ]
                    },
                    {
                        "uid": "bXhxNz",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "Yjg1bm",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 925869.41
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "c2dwbH",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 510638.816
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 215083.889
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 973906.671
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1188684.903
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1178932.68
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1248481.05
                            }
                        ]
                    },
                    {
                        "uid": "dHlpYm",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "ZW45cn",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1585711.547
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 550383.453
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "Zjh3Zz",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirSubmergedLarinier"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1497223.412
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 364790.918
                                    },
                                    {
                                        "symbol": "CdWSL",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 122957.019
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 274937.593
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1393833.898
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1212417.557
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 883889.043
                            }
                        ]
                    },
                    {
                        "uid": "dGxvc3",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "OGFvbm",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 903896.522
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "Y2x4bH",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirSubmergedLarinier"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1395571.127
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 432341.071
                                    },
                                    {
                                        "symbol": "CdWSL",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "bGNmdG",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1460623.282
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 768593.921
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 232421.312
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 983556.329
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 404184.656
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1162075.389
                            }
                        ]
                    },
                    {
                        "uid": "cWY0eT",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "dzB3aj",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 289672.379
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 516723.103
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 565501.767
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 284492.107
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1514204.101
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1541200.294
                            }
                        ]
                    },
                    {
                        "uid": "YTg0dG",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "a21saG",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 573891.435
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 506131.782
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "dGFwdW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1855004.221
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "cHdpcD",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "TriangularWeirFree"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1685779.706
                                    },
                                    {
                                        "symbol": "alpha2",
                                        "mode": "SINGLE",
                                        "value": 90
                                    },
                                    {
                                        "symbol": "CdT",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 438001.914
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 27133.406
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1928572.103
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1886540.519
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 140691.247
                            }
                        ]
                    },
                    {
                        "uid": "cHBkZG",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "Z213cW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirSubmergedLarinier"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 1695838.586
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 256938.906
                                    },
                                    {
                                        "symbol": "CdWSL",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            },
                            {
                                "uid": "cHNzOT",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "WeirVillemonte"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "ZDV",
                                        "mode": "SINGLE",
                                        "value": 964289.693
                                    },
                                    {
                                        "symbol": "L",
                                        "mode": "SINGLE",
                                        "value": 849245.6
                                    },
                                    {
                                        "symbol": "CdWR",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 534086.705
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 719038.785
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 924485.13
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 1665205.663
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 197678.562
                            }
                        ]
                    },
                    {
                        "uid": "MnVwNG",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "dGtwaW",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 761892.491
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 50289.502
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 911318.129
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1417697.08
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 204087.18
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 1522163.289
                            }
                        ]
                    },
                    {
                        "uid": "dTIxMG",
                        "props": {
                            "calcType": "Cloisons"
                        },
                        "children": [
                            {
                                "uid": "cHdoan",
                                "props": {
                                    "calcType": "Structure",
                                    "loiDebit": "OrificeSubmerged"
                                },
                                "children": [],
                                "parameters": [
                                    {
                                        "symbol": "S",
                                        "mode": "SINGLE",
                                        "value": 450333.98
                                    },
                                    {
                                        "symbol": "CdO",
                                        "mode": "SINGLE",
                                        "value": 10
                                    }
                                ]
                            }
                        ],
                        "parameters": [
                            {
                                "symbol": "LB",
                                "mode": "SINGLE",
                                "value": 729214.975
                            },
                            {
                                "symbol": "BB",
                                "mode": "SINGLE",
                                "value": 956705.339
                            },
                            {
                                "symbol": "ZRMB",
                                "mode": "SINGLE",
                                "value": 1524624.194
                            },
                            {
                                "symbol": "ZRAM",
                                "mode": "SINGLE",
                                "value": 403716.438
                            },
                            {
                                "symbol": "QA",
                                "mode": "SINGLE",
                                "value": 746677.81
                            }
                        ]
                    }
                ],
                "parameters": [
                    {
                        "symbol": "Q",
                        "mode": "SINGLE"
                    },
                    {
                        "symbol": "Z1",
                        "mode": "CALCUL",
                        "value": 102
                    },
                    {
                        "symbol": "Z2",
                        "mode": "SINGLE",
                        "value": 823926.0373911007
                    }
                ],
                "downWall": {
                    "uid": "c2x0bT",
                    "props": {
                        "calcType": "CloisonAval"
                    },
                    "children": [
                        {
                            "uid": "bGp4aG",
                            "props": {
                                "calcType": "Structure",
                                "loiDebit": "GateCem88d"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "ZDV",
                                    "mode": "SINGLE",
                                    "value": 414957.709
                                },
                                {
                                    "symbol": "W",
                                    "mode": "SINGLE",
                                    "value": 564648.092
                                },
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 731171.61
                                },
                                {
                                    "symbol": "CdWR",
                                    "mode": "SINGLE",
                                    "value": 10
                                }
                            ]
                        },
                        {
                            "uid": "cGtybm",
                            "props": {
                                "calcType": "Structure",
                                "loiDebit": "VanLevVillemonte"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "L",
                                    "mode": "SINGLE",
                                    "value": 840478.264
                                },
                                {
                                    "symbol": "CdWR",
                                    "mode": "SINGLE",
                                    "value": 10
                                },
                                {
                                    "symbol": "minZDV",
                                    "mode": "SINGLE",
                                    "value": 1020100.116
                                },
                                {
                                    "symbol": "maxZDV",
                                    "mode": "SINGLE",
                                    "value": 1261351.575
                                },
                                {
                                    "symbol": "DH",
                                    "mode": "SINGLE",
                                    "value": 707521.118
                                }
                            ]
                        },
                        {
                            "uid": "bDJwcG",
                            "props": {
                                "calcType": "Structure",
                                "loiDebit": "OrificeSubmerged"
                            },
                            "children": [],
                            "parameters": [
                                {
                                    "symbol": "S",
                                    "mode": "SINGLE",
                                    "value": 170291.549
                                },
                                {
                                    "symbol": "CdO",
                                    "mode": "SINGLE",
                                    "value": 10
                                }
                            ]
                        }
                    ],
                    "parameters": []
                }
            }
        ]
    }`;
    Session.getInstance().unserialise(sessionJson);
    return Session.getInstance().findNubByUid("Y2p6eW") as Pab;
}

describe("Class Pab", () => {
    describe("Jalhyd #131", () => {
        it("CalcSerie() should return a consistent result", () => {
            const r = getPabJalhyd131().CalcSerie();
            expect(BoolIdentity(!r.ok, r.hasErrorMessages())).toBe(true);
        });
    });
});
