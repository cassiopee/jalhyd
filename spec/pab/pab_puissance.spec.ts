import { PabPuissance } from "../../src/pab/pab_puissance";
import { PabPuissanceParams } from "../../src/pab/pab_puissance_params";
import { checkResult } from "../test_func";

function PabPuissanceTest(varTest: string) {
    describe("Calc(): ", () => {
        it(`${varTest} should be ${prms.get(varTest).v}`, () => {
            prms = createPabPuisPrms();
            const res: number = prms.get(varTest).v;
            const nub = new PabPuissance(prms);
            prms.get(varTest).v = undefined;
            checkResult(nub.Calc(varTest, 1e-9), res);
        });
    });
}

function createPabPuisPrms(): PabPuissanceParams {
    return new PabPuissanceParams(
        0.3,      // Chute entre bassins DH (m)
        0.1,      // Débit Q (m3/s)
        0.5,    // Volume V (m3)
        588.6   // Puissance dissipée PV (W/m3)
    );
}

let prms: PabPuissanceParams = createPabPuisPrms();

describe("Class PabPuissance: ", () => {
    // beforeAll(() => {
    // });
    PabPuissanceTest("DH");
    PabPuissanceTest("Q");
    PabPuissanceTest("V");
    PabPuissanceTest("PV");
});
