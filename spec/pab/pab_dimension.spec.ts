import { PabDimension } from "../../src/pab/pab_dimension";
import { PabDimensionParams } from "../../src/pab/pab_dimensions_params";
import { checkResult } from "../test_func";

function pabDimensionTest(varTest: string) {
    describe("Calc(): ", () => {
        it("V should be 1", () => {
            const prms = new PabDimensionParams(
                4,      // Longueur L
                1,      // Largeur W
                0.5,    // Tirant d'eau Y
                2       // Volume V
            );

            const res: number = prms.get(varTest).v;

            const nub = new PabDimension(prms);
            prms.get(varTest).v = undefined;

            checkResult(nub.Calc(varTest, 0), res);
        });
    });
}

describe("Class PabDimension: ", () => {
    // beforeEach(() => {
    // });
    // beforeAll(() => {
    // });
    pabDimensionTest("L");
    pabDimensionTest("W");
    pabDimensionTest("Y");
    pabDimensionTest("V");

});
