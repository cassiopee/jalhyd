import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";
import { cSnTrapez } from "../../../src/open-channel/section/section_trapez";
import { ParamsSectionTrapez } from "../../../src/open-channel/section/section_trapez_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

describe("Class RegimeUniforme / section trapèze :", () => {
    describe("pas de débordement :", () => {
        it("LargeurFond should be 2.5", () => {
            const prms = new ParamsSectionTrapez(undefined, // largeur de fond
                0.56, // fruit
                0.8, // tirant d'eau
                40, //  Ks=Strickler
                1.988428,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);

            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("LargeurFond", 0), 2.5);
        });

        it("Fruit should be 0.56", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                undefined, // fruit
                0.587, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Fruit", 0), 0.56);
        });

        it("Ks should be 24.14", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                0.8, // tirant d'eau
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Ks", 1e-8), 24.14);
        });

        it("If should be 0.001", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                0.587, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("If", 0.001), 0.001);
        });

        it("Q should be 1.2", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, //  fruit
                0.587, // tirant d'eau
                40, //  Ks=Strickler
                0,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 1.2);
        });

        it("Y should be 0.587", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, //  fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Y", 0), 0.587);
        });
    });

    describe("débordement :", () => {
        it("LargeurFond should be 0.03", () => {
            const prms = new ParamsSectionTrapez(undefined, // largeur de fond
                0.56, // fruit
                2, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("LargeurFond", 0), 0.03);
        });

        it("Fruit should be 0.56", () => {
            const prms = new ParamsSectionTrapez(0.03, // largeur de fond
                undefined, // fruit
                2, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Fruit", 0), 0.56);
        });

        it("Ks should be 5.744", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                2, // tirant d'eau
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Ks", 1e-8), 5.744);
        });

        it("If should be 0.001", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                2, // tirant d'eau
                40, //  Ks=Strickler
                8.356,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("If", 0.001), 0.001);
        });

        it("Q should be 8.356", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, //  fruit
                2, // tirant d'eau
                40, //  Ks=Strickler
                undefined,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 8.356);
        });

        it("Y should be 0.587", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, //  fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Y", 0), 0.587);
        });
    });
    describe("#301 non convergence du calcul du tirant d'eau critique", () => {
        it("Yn should be successfully calculated", () => {
            const prms = new ParamsSectionTrapez(
                20, // largeur de fond
                2, // fruit
                1, // tirant d'eau
                40, //  Ks=Strickler
                20,  //  Q=Débit
                0.001, //  If=pente du fond
                3, // YB= hauteur de berge
            );
            SessionSettings.precision = 1E-7;

            const sect = new cSnTrapez(prms);
            const ru = new RegimeUniforme(sect);
            ru.calculatedParam = ru.section.prms.Y;
            SessionSettings.precision = precDist;
            checkResult(ru.CalcSerie(), 0.858); // Ref from Canal9
        });
    });
});
