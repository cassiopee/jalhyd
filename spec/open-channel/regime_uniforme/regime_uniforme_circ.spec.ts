import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";
import { cSnCirc } from "../../../src/open-channel/section/section_circulaire";
import { ParamsSectionCirc } from "../../../src/open-channel/section/section_circulaire_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";
import { MessageCode } from "../../../src/util/message";

describe("Class RegimeUniforme / section circulaire :", () => {
    describe("pas de débordement : ", () => {
        it("Diamètre should be 6.001", () => {
            const paramSect = new ParamsSectionCirc(undefined, // diamètre
                0.6613,  // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSect);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("D", 1e-8), 6.001);
        });

        it("Ks should be 40.003", () => {
            const paramSection = new ParamsSectionCirc(6, // diamètre
                0.6613, // tirant d'eau
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Ks", 1e-8), 40.003);
        });

        it("If should be 0.001", () => {
            const paramSection = new ParamsSectionCirc(6, // diamètre
                0.6613, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("If", 0.0001), 0.001);
        });

        it("Q should be 1.2", () => {
            const paramSection = new ParamsSectionCirc(6, // diamètre
                0.6613, // tirant d'eau
                40, //  Ks=Strickler
                undefined,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 1.2);
        });

        it("Y should be 0.6613", () => {
            const paramSection = new ParamsSectionCirc(6, // diamètre
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Y", 0), 0.6613);
        });
    });

    describe("débordement : ", () => {
        it("Diamètre should be 2", () => {
            const paramSect = new ParamsSectionCirc(undefined, // diamètre
                2,  // tirant d'eau
                13.551, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSect);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("D", 1e-8), 2);
        });

        it("Ks should be 13.551", () => {
            const paramSection = new ParamsSectionCirc(2, // diamètre
                2, // tirant d'eau
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Ks", 1e-8), 13.551);
        });

        it("If should be 0.001", () => {
            const paramSection = new ParamsSectionCirc(2, // diamètre
                2, // tirant d'eau
                13.551, //  Ks=Strickler
                1.2,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("If", 0), 0.001);
        });

        it("Q should be 1.2", () => {
            const paramSection = new ParamsSectionCirc(2, // diamètre
                2, // tirant d'eau
                13.551, //  Ks=Strickler
                undefined,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 1.2);
        });

        it("Y should be 2", () => {
            const paramSection = new ParamsSectionCirc(2, // diamètre
                undefined, // tirant d'eau
                13.551, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnCirc(paramSection);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Y", 0), 2);
        });
    });

    describe("en charge −", () => {
        it("an error should be thrown if sectionis circular, Y > D and YB >= D", () => {
            const paramSection = new ParamsSectionCirc(
                2, // diamètre
                2.1, // tirant d'eau
                13.551, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                2.5, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const ru = new RegimeUniforme(new cSnCirc(paramSection));
            ru.calculatedParam = paramSection.Q;
            const res = ru.CalcSerie();
            expect(res.ok).toBe(false);
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_RU_CIRC_LEVEL_TOO_HIGH);
        });
    });
});
