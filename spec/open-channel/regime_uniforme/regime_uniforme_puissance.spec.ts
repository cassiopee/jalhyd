import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";
import { cSnPuiss } from "../../../src/open-channel/section/section_puissance";
import { ParamsSectionPuiss } from "../../../src/open-channel/section/section_puissance_params";
import { SessionSettings } from "../../../src/session_settings";
import { MessageCode } from "../../../src/util/message";
import { Result } from "../../../src/util/result";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

describe("Class RegimeUniforme / section puissance :", () => {
    describe("pas de débordement :", () => {
        it("k should be 0.635", () => {
            const prms = new ParamsSectionPuiss(undefined, // coef
                0.8, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("k", 0), 0.635);
        });

        it("LargeurBerge should be 3.473", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                0.8, // tirant d'eau
                undefined, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("LargeurBerge", 0), 3.473);
        });

        it("Strickler should be 33.774", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                0.8, // tirant d'eau
                4, // largeur de berge
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Ks", 1e-8), 33.774);
        });

        it("If should be 0.002", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                0.8, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            SessionSettings.precision = 0.00001;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("If", 0.001), 0.00071, 0.00001);
        });

        it("Q should be 1.421", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                0.8, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                undefined,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Q", 0), 1.421);
        });

        it("Y should be 0.742", () => {
            const paramCnl = new ParamsSectionPuiss(0.5, // coef
                undefined, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(paramCnl);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Y", 0), 0.742);
        });
    });

    describe("débordement :", () => {
        it("k should be undefined", () => {
            const prms = new ParamsSectionPuiss(undefined, // coef
                2, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            const res: Result = ru.Calc("k", 0);
            expect(res.vCalc).toBeUndefined();
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_DICHO_INIT_DOMAIN);
        });

        it("k should be 0.635", () => {
            const prms = new ParamsSectionPuiss(undefined, // coef
                2, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                10,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("k", 0), 0.933);
        });

        it("LargeurBerge should be 0.721", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                undefined, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("LargeurBerge", 0), 0.721);
        });

        it("Strickler should be 4.367", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                4, // largeur de berge
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Ks", 1e-8), 4.367);
        });

        it("Ks should be undefined", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                4, // largeur de berge
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect);
            ru.dichoStartIntervalMaxSteps = 3;

            const res: Result = ru.Calc("Ks", 5000000);
            expect(res.vCalc).toBeUndefined();
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_DICHO_TARGET_TOO_LOW);
        });

        it("Ks should be undefined", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                4, // largeur de berge
                undefined, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect);
            ru.dichoStartIntervalMaxSteps = 1;

            const res: Result = ru.Calc("Ks", 1e-8);
            expect(res.vCalc).toBeUndefined();
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_DICHO_TARGET_TOO_HIGH);
        });

        it("If should be 0.001", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                4, // largeur de berge
                4.366, //  Ks=Strickler
                1.2,  //  Q=Débit
                undefined, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("If", 0.001), 0.001);
        });

        it("Q should be 10.993", () => {
            const prms = new ParamsSectionPuiss(0.5, // coef
                2, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                undefined,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(prms);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Q", 0), 10.993);
        });

        it("Y should be 0.742", () => {
            const paramCnl = new ParamsSectionPuiss(0.5, // coef
                undefined, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnPuiss(paramCnl);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("Y", 0), 0.742);
        });
    });
});
