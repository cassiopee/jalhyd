import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";
import { acSection } from "../../../src/open-channel/section/section_type";

function generateRectangularSection(): cSnRectang {
    const prms = new ParamsSectionRectang(
        0.8, // tirant d'eau
        2.5, // largeur de fond
        40, //  Ks=Strickler
        1.5676680, // Q=Débit
        0.001, // If=pente du fond
        1, // YB=hauteur de berge
    );
    return new cSnRectang(prms, false);
}

function generateRegimeUniforme(sect: acSection): RegimeUniforme {
    return new RegimeUniforme(sect, false);
}

let ruRect1 = generateRegimeUniforme(generateRectangularSection());

describe("Class RegimeUniforme / section rectangulaire :", () => {
    describe("pas de débordement : ", () => {
        beforeEach(() => {
            ruRect1 = generateRegimeUniforme(generateRectangularSection());
            SessionSettings.precision = 1E-7;
        })
        for (const p0 of ruRect1.calculableParameters) {
            it(`${p0.symbol} should be ${p0.singleValue}`, () => {
                const p = ruRect1.section.prms.get(p0.symbol);
                const ref: number = p.singleValue;
                p.singleValue = 1e-8;
                p.v = p.initValue;
                ruRect1.calculatedParam = p;
                checkResult(ruRect1.CalcSerie(), ref);
                expect(ruRect1.result.values.V).toBeCloseTo(0.784, 3);
            });
        }

        it("Q should be 0.731", () => {
            const prms = new ParamsSectionRectang(1.1, // tirant d'eau
                3, // largeur de fond
                30, //  Ks=Strickler
                undefined, // Q=Débit
                0.0001, // If=pente du fond
                1.2, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 0.731);
        });

        it("Y should be 0.663", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                1.2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            // nom variable à calculer, valeur de Ks
            checkResult(ru.Calc("Y", 0), 0.663);
        });
    });

    describe("débordement : ", () => {
        /**
         * test de la largeur de fond (= largeur de berge pour le rectangulaire)
         */
        it("LargeurBerge should be 2.5", () => {
            const prms = new ParamsSectionRectang(2, // tirant d'eau
                undefined, // largeur de fond
                40, //  Ks=Strickler
                5.31, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms, false);
            const ru = new RegimeUniforme(sect, false);

            checkResult(ru.Calc("LargeurBerge", 0), 2.5);
        });

        it("Strickler should be 9.04", () => {
            const prms = new ParamsSectionRectang(2, // tirant d'eau
                2.5, // largeur de fond
                undefined, //  Ks=Strickler
                1.2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Ks", 1e-8), 9.04);
        });

        it("If should be 0.001", () => {
            const prms = new ParamsSectionRectang(2, // tirant d'eau
                2.5, // largeur de fond
                9.04, //  Ks=Strickler
                1.2, // Q=Débit
                undefined, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect, false);

            // nom variable à calculer, valeur de Ks
            checkResult(ru.Calc("If", 0.001), 0.001);
        });

        it("Q should be 5.31", () => {
            const prms = new ParamsSectionRectang(2, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                undefined, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 5.31);
        });

        it("Q should be 1.624", () => {
            const prms = new ParamsSectionRectang(2, // tirant d'eau
                3, // largeur de fond
                30, //  Ks=Strickler
                undefined, // Q=Débit
                0.0001, // If=pente du fond
                1.2, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            checkResult(ru.Calc("Q", 0), 1.624);
        });

        it("Y should be 0.663", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                1.2, // Q=Débit
                0.001, // If=pente du fond
                0.1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);

            // nom variable à calculer, valeur de Ks
            checkResult(ru.Calc("Y", 0), 0.663);
        });

        it("there should be a log message stating that it's overflowing", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                1.2, // Q=Débit
                0.001, // If=pente du fond
                0.1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;
            const sect = new cSnRectang(prms);
            const ru = new RegimeUniforme(sect);
            ru.calculatedParam = prms.Y;
            ru.CalcSerie();
            expect(prms.YB.singleValue).toBe(0.1);
            expect(ru.result.vCalc).toBeCloseTo(0.663, 3);
            expect(ru.result.log.messages.length).toBe(1);
            expect(ru.result.log.messages[0].code).toBe(MessageCode.WARNING_SECTION_OVERFLOW);
        });
    });
});
