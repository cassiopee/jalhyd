import { Session } from "../../../src/index";

const sessionJson = `
{
    "header": {
        "source": "jalhyd",
        "format_version": "1.1",
        "created": "2019-07-30T09:20:09.021Z"
    },
    "session": [
        {
            "uid": "ZGF5eT",
            "props": {
                "calcType": "CourbeRemous",
                "methodeResolution": "RungeKutta4"
            },
            "children": [
                {
                    "uid": "aGwzb2",
                    "props": {
                        "calcType": "Section",
                        "nodeType": "SectionTrapeze"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "Ks",
                            "mode": "SINGLE",
                            "value": 441098.4377908428
                        },
                        {
                            "symbol": "Q",
                            "mode": "SINGLE",
                            "value": 0.00000922136480077249
                        },
                        {
                            "symbol": "If",
                            "mode": "SINGLE",
                            "value": 6.892049720566374e-7
                        },
                        {
                            "symbol": "YB",
                            "mode": "SINGLE",
                            "value": 496154.5708281834
                        },
                        {
                            "symbol": "Y",
                            "mode": "SINGLE",
                            "value": 0.0000011347684018573246
                        },
                        {
                            "symbol": "LargeurFond",
                            "mode": "SINGLE",
                            "value": 0.0000013566385534465054
                        },
                        {
                            "symbol": "Fruit",
                            "mode": "SINGLE",
                            "value": 721007.6117608328
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Yamont",
                    "mode": "SINGLE",
                    "value": 264048.4798431795
                },
                {
                    "symbol": "Yaval",
                    "mode": "SINGLE",
                    "value": 0.000002930952132356441
                },
                {
                    "symbol": "Long",
                    "mode": "SINGLE",
                    "value": 968613.735314439
                },
                {
                    "symbol": "Dx",
                    "mode": "SINGLE",
                    "value": 191298.40499008956
                }
            ]
        }
    ]
}`;

describe("Class Remous", () => {
    describe("jalhyd#125", () => {
        it("CalcSerie() should return a consistent result", () => {
            Session.getInstance().unserialise(sessionJson);
            const r = Session.getInstance().findNubByUid("ZGF5eT").CalcSerie();
            expect(r).toBeDefined();
        });
    });
});
