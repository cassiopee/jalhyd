import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";

function generateRemous() {
    const prms = new ParamsSectionRectang(
        undefined, // tirant d'eau
        2.5, // largeur de fond
        40, //  Ks=Strickler
        1.2, // Q=Débit
        undefined, // If=pente du fond
        1, // YB=hauteur de berge
    );
    const sect = new cSnRectang(prms);
    const prem = new CourbeRemousParams(
        100.25, // Z1
        100.4,  // Z2
        100.3,  // ZF1
        100,    // ZF2
        100,    // Long= Longueur du bief
        5,      // Dx=Pas d'espace
    );
    const cr = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);
    return cr;
}

describe("longueur, pas de discrétisation, iterations (jalhyd#153) − ", () => {

    it("avec Long=100, Dx=5", () => {
        const cr = generateRemous();
        cr.prms.Dx.singleValue = 5;
        cr.prms.Long.singleValue = 100;
        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(21);
    });

    it("avec Long=103, Dx=5", () => {
        const cr = generateRemous();
        cr.prms.Dx.singleValue = 5;
        cr.prms.Long.singleValue = 103;
        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(22);
    });

    it("avec Long=101, Dx=4", () => {
        const cr = generateRemous();
        cr.prms.Dx.singleValue = 4;
        cr.prms.Long.singleValue = 101;
        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(27);
    });

});
