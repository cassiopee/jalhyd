import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { cLog } from "../../../src/util/log";
import { Message, MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";
import { compareExtraResult, compareLog, extraResultLength } from "../../test_func";

/*
   cas 1 :
   Certaines valeurs de ligne d'eau torrentielle étaient auparavant remplacées par une valeur fluviale
   pour la représentation graphique du ressaut (mais fausse car torrentielle).
   Idem pour la réciproque fluvial/torrentiel.
   Le code ne faisant plus ça, les valeurs de validation ont été remplacée par les bonnes.
 */

/*
  cas 2 :
  Le code de modification des lignes fluviale et torrentielle a été modifié, on enlève un point de plus
  ligne d'eau complète : if (iSens * (rXCC - rX) < 0) remplacé par if (iSens * (rXCC - rX) <= 0)
  lign d'eau partielle : if (iSens * (rXCN - xRst) > 0)  remplacé par if (iSens * (rXCN - xRst) >= 0)
  Les données de validation ont été modifiées en conséquence
*/

describe("Class Remous / section rectangulaire :", () => {
    describe("méthode trapèzes :", () => {
        // désactivé suite au changement de format de résultat
        it("forte pente, ressaut avant l'amont", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.15, 106, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            const f: any = {
                100.000: 6,
                95.000: 5.75,
                90.000: 5.5,
                85.000: 5.25,
                80.000: 4.999,
                75.000: 4.749,
                70.000: 4.499,
                65.000: 4.249,
                60.000: 3.998,
                55.000: 3.748,
                50.000: 3.498,
                45.000: 3.248,
                40.000: 2.997,
                35.000: 2.747,
                30.000: 2.497,
                25.000: 2.246,
                20.000: 1.995,
                15.000: 1.744,
                10.000: 1.491,
                5.000: 1.237,
                0.000: 0.977
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = {};
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));
            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "amont";
            m.extraVar.x = 0;
            expLog.add(m);

            m = new Message(MessageCode.WARNING_SECTION_OVERFLOW_ABSC);
            m.extraVar.xa = 0;
            m.extraVar.xb = 100;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        it("forte pente, ressaut après l'aval", () => {
            // TODO algo à reprendre
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.15, 100.45, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            const f = {};
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            // tslint:disable-next-line:max-line-length
            // let t = { 0.000: 0.15, 5.000: 0.207, 10.000: 0.235, 15.000: 0.246, 20.000: 0.25, 25.000: 0.252, 30.000: 0.253, 35.000: 0.253, 40.000: 0.253, 45.000: 0.253, 50.000: 0.253, 55.000: 0.253, 60.000: 0.253, 65.000: 0.253, 70.000: 0.253, 75.000: 0.253, 80.000: 0.253, 85.000: 0.253, 90.000: 0.253, 95.000: 0.253, 100.000: 0.253 };
            // tslint:disable-next-line:max-line-length
            // let t = { 0.000: 0.15, 5.000: 0.207, 10.000: 0.235, 15.000: 0.246, 20.000: 0.25, 25.000: 0.252, 30.000: 0.253, 35.000: 0.253, 40.000: 0.253, 45.000: 0.253, 50.000: 0.253, 55.000: 0.253, 60.000: 0.253, 65.000: 0.253, 70.000: 0.253, 75.000: 0.253, 80.000: 0.253, 85.000: 0.253, 90.000: 0.253, 95.000: 0.253, 100.000: 0.45 }; // dernière valeur modifiée pour la raison 1 en tête de fichier
            const t: any = {
                0.000: 0.15,
                5.000: 0.207,
                10.000: 0.235,
                15.000: 0.246,
                20.000: 0.25,
                25.000: 0.252,
                30.000: 0.253,
                35.000: 0.253,
                40.000: 0.253,
                45.000: 0.253,
                50.000: 0.253,
                55.000: 0.253,
                60.000: 0.253,
                65.000: 0.253,
                70.000: 0.253,
                75.000: 0.253,
                80.000: 0.253,
                85.000: 0.253,
                90.000: 0.253,
                95.000: 0.253,
                100.000: 0.253
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 95;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "aval";
            m.extraVar.x = 100;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        xit("forte pente, ressaut (1 point) à l'intérieur du bief", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.15, 101, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            const f = { 100.000: 1, 95.000: 0.729 };
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            // tslint:disable-next-line:max-line-length
            // let t = { 0.000: 0.15, 5.000: 0.207, 10.000: 0.235, 15.000: 0.246, 20.000: 0.25, 25.000: 0.252, 30.000: 0.253, 35.000: 0.253, 40.000: 0.253, 45.000: 0.253, 50.000: 0.253, 55.000: 0.253, 60.000: 0.253, 65.000: 0.253, 70.000: 0.253, 75.000: 0.253, 80.000: 0.253, 85.000: 0.253, 90.000: 0.253, 95.000: 0.729 };
            const t: any = {
                0.000: 0.15,
                5.000: 0.207,
                10.000: 0.235,
                15.000: 0.246,
                20.000: 0.25,
                25.000: 0.252,
                30.000: 0.253,
                35.000: 0.253,
                40.000: 0.253,
                45.000: 0.253,
                50.000: 0.253,
                55.000: 0.253,
                60.000: 0.253,
                65.000: 0.253,
                70.000: 0.253,
                75.000: 0.253,
                80.000: 0.253,
                85.000: 0.253,
                90.000: 0.253
            }; // dernière valeur supprimée pour la raison 2 en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 90;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 90;
            m.extraVar.xmax = 95;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        xit("forte pente, ressaut (plusieurs points) à l'intérieur du bief", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.425, 101, 100.275, 100,
                5.5,  // Long= Longueur du bief
                0.25,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            // tslint:disable-next-line:max-line-length
            // let f = { 5.500: 1, 5.250: 0.987, 5.000: 0.974, 4.750: 0.96, 4.500: 0.947, 4.250: 0.933, 4.000: 0.92, 3.750: 0.906, 3.500: 0.893, 3.250: 0.88, 3.000: 0.866, 2.750: 0.853, 2.500: 0.839, 2.250: 0.826, 2.000: 0.812, 1.750: 0.798, 1.500: 0.784, 1.250: 0.77, 1.000: 0.756, 0.750: 0.742, 0.500: 0.727, 0.250: 0.712, 0.000: 0.15 };
            const f: any = {
                5.500: 1,
                5.250: 0.987,
                5.000: 0.974,
                4.750: 0.96,
                4.500: 0.947,
                4.250: 0.933,
                4.000: 0.92,
                3.750: 0.906,
                3.500: 0.893,
                3.250: 0.88,
                3.000: 0.866,
                2.750: 0.853,
                2.500: 0.839,
                2.250: 0.826,
                2.000: 0.812,
                1.750: 0.798,
                1.500: 0.784,
                1.250: 0.77,
                1.000: 0.756,
                0.750: 0.742,
                0.500: 0.727,
                0.250: 0.712,
                0.000: 0.699
            };  // dernière valeur modifiée pour la raison 1 en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t: any = {
                0.000: 0.15,
                0.250: 0.153,
                0.500: 0.156,
                0.750: 0.158,
                1.000: 0.161,
                1.250: 0.163,
                1.500: 0.166,
                1.750: 0.168,
                2.000: 0.17,
                2.250: 0.173,
                2.500: 0.175,
                2.750: 0.177,
                3.000: 0.18,
                3.250: 0.182,
                3.500: 0.184,
                3.750: 0.906
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 0.250, 0.500, 0.750, 1.000, 1.250, 1.500, 1.750, 2.000, 2.250, 2.500, 2.750, 3.000, 3.250, 3.500, 3.750, 4.000, 4.250, 4.500, 4.750, 5.000, 5.250, 5.500];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 3.75;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        it("forte pente, pas de ressaut, Yaval < Yc, Yamont < Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.1, 100.3, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            expect(extraResultLength(res, "flu") === 0).toBeTruthy(
                "la ligne d'eau fluviale ne devrait comporter aucune valeur");

            const t: any = {
                0.000: 0.1, 5.000: 0.205, 10.000: 0.234, 15.000: 0.246, 20.000: 0.25, 25.000: 0.252,
                30.000: 0.253, 35.000: 0.253, 40.000: 0.253, 45.000: 0.253, 50.000: 0.253, 55.000: 0.253,
                60.000: 0.253, 65.000: 0.253, 70.000: 0.253, 75.000: 0.253, 80.000: 0.253, 85.000: 0.253,
                90.000: 0.253, 95.000: 0.253, 100.000: 0.253
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.01);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.01);

            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000,
            //     55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        xit("forte pente, pas de ressaut, Yaval < Yc, Yn < Yamont < Yc", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.85, 100.3, 100.5, 100,
                10,  // Long= Longueur du bief
                0.1,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            expect(extraResultLength(res, "flu") === 0)
                .toBeTruthy("la ligne d'eau fluviale ne devrait comporter aucune valeur");

            const t: any = {
                0.000: 0.35,
                0.100: 0.345,
                0.200: 0.34,
                0.300: 0.336,
                0.400: 0.332,
                0.500: 0.329,
                0.600: 0.326,
                0.700: 0.323,
                0.800: 0.32,
                0.900: 0.318,
                1.000: 0.316,
                1.100: 0.313,
                1.200: 0.311,
                1.300: 0.309,
                1.400: 0.308,
                1.500: 0.306,
                1.600: 0.305,
                1.700: 0.303,
                1.800: 0.302,
                1.900: 0.3,
                2.000: 0.298,
                2.100: 0.297,
                2.200: 0.295,
                2.300: 0.294,
                2.400: 0.293,
                2.500: 0.292,
                2.600: 0.291,
                2.700: 0.291,
                2.800: 0.29,
                2.900: 0.289,
                3.000: 0.288,
                3.100: 0.287,
                3.200: 0.287,
                3.300: 0.286,
                3.400: 0.285,
                3.500: 0.284,
                3.600: 0.283,
                3.700: 0.283,
                3.800: 0.282,
                3.900: 0.281,
                4.000: 0.28,
                4.100: 0.28,
                4.200: 0.279,
                4.300: 0.278,
                4.400: 0.277,
                4.500: 0.276,
                4.600: 0.276,
                4.700: 0.275,
                4.800: 0.274,
                4.900: 0.273,
                5.000: 0.272,
                5.100: 0.272,
                5.200: 0.271,
                5.300: 0.27,
                5.400: 0.269,
                5.500: 0.269,
                5.600: 0.269,
                5.700: 0.269,
                5.800: 0.269,
                5.900: 0.269,
                6.000: 0.269,
                6.100: 0.269,
                6.200: 0.269,
                6.300: 0.269,
                6.400: 0.269,
                6.500: 0.269,
                6.600: 0.269,
                6.700: 0.269,
                6.800: 0.269,
                6.900: 0.269,
                7.000: 0.269,
                7.100: 0.269,
                7.200: 0.269,
                7.300: 0.269,
                7.400: 0.269,
                7.500: 0.269,
                7.600: 0.269,
                7.700: 0.269,
                7.800: 0.269,
                7.900: 0.269,
                8.000: 0.269,
                8.100: 0.269,
                8.200: 0.269,
                8.300: 0.269,
                8.400: 0.269,
                8.500: 0.269,
                8.600: 0.269,
                8.700: 0.269,
                8.800: 0.269,
                8.900: 0.269,
                9.000: 0.269,
                9.100: 0.269,
                9.200: 0.269,
                9.300: 0.269,
                9.400: 0.269,
                9.500: 0.269,
                9.600: 0.269,
                9.700: 0.269,
                9.800: 0.269,
                9.900: 0.269,
                10.000: 0.269
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.01);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.01);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 0.100, 0.200, 0.300, 0.400, 0.500, 0.600, 0.700, 0.800, 0.900, 1.000, 1.100, 1.200, 1.300, 1.400, 1.500, 1.600, 1.700, 1.800, 1.900, 2.000, 2.100, 2.200, 2.300, 2.400, 2.500, 2.600, 2.700, 2.800, 2.900, 3.000, 3.100, 3.200, 3.300, 3.400, 3.500, 3.600, 3.700, 3.800, 3.900, 4.000, 4.100, 4.200, 4.300, 4.400, 4.500, 4.600, 4.700, 4.800, 4.900, 5.000, 5.100, 5.200, 5.300, 5.400, 5.500, 5.600, 5.700, 5.800, 5.900, 6.000, 6.100, 6.200, 6.300, 6.400, 6.500, 6.600, 6.700, 6.800, 6.900, 7.000, 7.100, 7.200, 7.300, 7.400, 7.500, 7.600, 7.700, 7.800, 7.900, 8.000, 8.100, 8.200, 8.300, 8.400, 8.500, 8.600, 8.700, 8.800, 8.900, 9.000, 9.100, 9.200, 9.300, 9.400, 9.500, 9.600, 9.700, 9.800, 9.900, 10.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            compareLog(res.globalLog, expLog);
        });
    });
});
