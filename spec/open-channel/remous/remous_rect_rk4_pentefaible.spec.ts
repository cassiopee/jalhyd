import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { cLog } from "../../../src/util/log";
import { Message, MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";
import { compareExtraResult, compareLog, extraResultLength } from "../../test_func";

/*
  Le code de modification des lignes fluviale et torrentielle a été modifié, on enlève un point de plus
  ligne d'eau complète : if (iSens * (rXCC - rX) < 0) remplacé par if (iSens * (rXCC - rX) <= 0)
  lign d'eau partielle : if (iSens * (rXCN - xRst) > 0)  remplacé par if (iSens * (rXCN - xRst) >= 0)
  Les données de validation ont été modifiées en conséquence
*/

describe("Class Remous / section rectangulaire :", () => {
    describe("méthode Runge-Kutta ordre 4 :", () => {
        it("faible pente, ressaut avant l'amont", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.4, 100.41, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode RungeKutta4

            const f: any = {
                100.000: 0.41, 95.000: 0.876, 90.000: 0.877, 85.000: 0.879, 80.000: 0.88, 75.000: 0.881,
                70.000: 0.883, 65.000: 0.884, 60.000: 0.885, 55.000: 0.886, 50.000: 0.888, 45.000: 0.889,
                40.000: 0.89, 35.000: 0.891, 30.000: 0.892, 25.000: 0.893, 20.000: 0.894, 15.000: 0.895,
                10.000: 0.896, 5.000: 0.897, 0.000: 0.898
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = {};
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000,
            //     55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 5;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "amont";
            m.extraVar.x = 0;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        xit("faible pente, ressaut (1 point) à l'intérieur du bief", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.25, 100.42, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode RungeKutta4

            // tslint:disable-next-line:max-line-length
            // let f = { 100.000: 0.42, 95.000: 0.583, 90.000: 0.604, 85.000: 0.621, 80.000: 0.636, 75.000: 0.65, 70.000: 0.662, 65.000: 0.673, 60.000: 0.683, 55.000: 0.692, 50.000: 0.701, 45.000: 0.709, 40.000: 0.717, 35.000: 0.724, 30.000: 0.731, 25.000: 0.737, 20.000: 0.743, 15.000: 0.749, 10.000: 0.755, 5.000: 0.76, 0.000: 0.15 };
            const f: any = {
                100.000: 0.42,
                95.000: 0.583,
                90.000: 0.604,
                85.000: 0.621,
                80.000: 0.636,
                75.000: 0.65,
                70.000: 0.662,
                65.000: 0.673,
                60.000: 0.683,
                55.000: 0.692,
                50.000: 0.701,
                45.000: 0.709,
                40.000: 0.717,
                35.000: 0.724,
                30.000: 0.731,
                25.000: 0.737,
                20.000: 0.743,
                15.000: 0.749,
                10.000: 0.755,
                5.000: 0.76
            }; // dernière valeur supprimée pour la raison en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            // let t = { 0.000: 0.15, 5.000: 0.76 };
            const t = { 0.000: 0.15 }; // dernière valeur supprimée pour la raison en tête de fichier
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 15;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 5;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        xit("faible pente, ressaut (plusieurs points) à l'intérieur du bief", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.015, 100.42, 100.005, 100,
                5,  // Long= Longueur du bief
                0.25,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode RungeKutta4

            const f: any = {
                5.000: 0.42,
                4.750: 0.434,
                4.500: 0.443,
                4.250: 0.45,
                4.000: 0.457,
                3.750: 0.462,
                3.500: 0.467,
                3.250: 0.471,
                3.000: 0.476,
                2.750: 0.48,
                2.500: 0.483,
                2.250: 0.487,
                2.000: 0.49,
                1.750: 0.493,
                1.500: 0.496,
                1.250: 0.499,
                1.000: 0.502,
                0.750: 0.504,
                0.500: 0.507,
                0.250: 0.509,
                0.000: 0.01
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = { 0.000: 0.01, 0.250: 0.017, 0.500: 0.022, 0.750: 0.028, 1.000: 0.502 };
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 0.250, 0.500, 0.750, 1.000, 1.250, 1.500, 1.750, 2.000, 2.250, 2.500, 2.750, 3.000, 3.250, 3.500, 3.750, 4.000, 4.250, 4.500, 4.750, 5.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 1;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        it("faible pente, pas de ressaut, Yamont > Yc, Yaval > Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.6, 101, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode RungeKutta4

            const f: any = {
                100.000: 1,
                95.000: 0.999,
                90.000: 0.999,
                85.000: 0.998,
                80.000: 0.997,
                75.000: 0.997,
                70.000: 0.996,
                65.000: 0.996,
                60.000: 0.995,
                55.000: 0.994,
                50.000: 0.994,
                45.000: 0.993,
                40.000: 0.993,
                35.000: 0.992,
                30.000: 0.991,
                25.000: 0.991,
                20.000: 0.99,
                15.000: 0.99,
                10.000: 0.989,
                5.000: 0.989,
                0.000: 0.988
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            expect(extraResultLength(res, "tor") === 0)
                .toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

            compareLog(res.globalLog, expLog);
        });

        it("faible pente, pas de ressaut, Yamont > Yc, Yc < Yaval < Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.6, 100.7, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode RungeKutta4

            const f: any = {
                100.000: 0.7,
                95.000: 0.708,
                90.000: 0.716,
                85.000: 0.723,
                80.000: 0.73,
                75.000: 0.737,
                70.000: 0.743,
                65.000: 0.749,
                60.000: 0.754,
                55.000: 0.759,
                50.000: 0.764,
                45.000: 0.769,
                40.000: 0.774,
                35.000: 0.779,
                30.000: 0.783,
                25.000: 0.787,
                20.000: 0.791,
                15.000: 0.795,
                10.000: 0.799,
                5.000: 0.802,
                0.000: 0.806
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            expect(extraResultLength(res, "tor") === 0)
                .toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

            compareLog(res.globalLog, expLog);
        });
    });
});
