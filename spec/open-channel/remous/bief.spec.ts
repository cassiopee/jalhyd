import { Bief } from "../../../src/open-channel/bief";
import { BiefParams, BiefRegime } from "../../../src/open-channel/bief_params";
import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { Session } from "../../../src/session";
import { SessionSettings } from "../../../src/session_settings";
import { MessageCode } from "../../../src/util/message";

// generate a Bief with parameters for a fluvial-only curve
function generateBiefFluvial() {
    const prmSect = new ParamsSectionRectang(
        undefined,  // tirant d'eau
        2.4,        // largeur de berge
        42,         // Ks=Strickler
        1.2,        // Q=Débit
        undefined,  // If=pente du fond
        1.2,        // YB=hauteur de berge
    );
    const sect = new cSnRectang(prmSect);
    const prms = new BiefParams(
        undefined,  // Z1
        100.4,      // Z2
        100.12,     // ZF1
        100.02,     // ZF2
        110,        // Long
        5           // Dx
    );
    const bf = new Bief(sect, prms);
    bf.regime = BiefRegime.Fluvial;
    return bf;
}

// generate a Bief with parameters for a torrential-only curve
function generateBiefTorrentiel() {
    const prmSect = new ParamsSectionRectang(
        undefined,  // tirant d'eau
        2.4,        // largeur de berge
        42,         // Ks=Strickler
        1.2,        // Q=Débit
        undefined,  // If=pente du fond
        1.2,        // YB=hauteur de berge
    );
    const sect = new cSnRectang(prmSect);
    const prms = new BiefParams(
        105.2,      // Z1
        undefined,  // Z2
        105,        // ZF1
        100.02,     // ZF2
        110,        // Long
        5           // Dx
    );
    const bt = new Bief(sect, prms);
    bt.regime = BiefRegime.Torrentiel;
    return bt;
}

describe("bief − ", () => {

    it("parameters of internal section should not be linkable", () => {
        Session.getInstance().clear();
        // register a Bief
        const bief = generateBiefFluvial();
        Session.getInstance().registerNub(bief);
        // register another Nub with similar parameters
        const prmsSectRu = new ParamsSectionRectang(2, 1.5, 40, 1.2, 0.001, 1.2);
        const ru = new RegimeUniforme(new cSnRectang(prmsSectRu));
        Session.getInstance().registerNub(ru);
        // calculate the Bief
        bief.calculatedParam = bief.prms.Z1;
        const res = bief.CalcSerie();
        // check linkable params for RU
        expect(Session.getInstance().getLinkableValues(prmsSectRu.LargeurBerge).length).toBe(1, "Kenobi");
        expect(Session.getInstance().getLinkableValues(prmsSectRu.YB).length).toBe(1);
        expect(Session.getInstance().getLinkableValues(prmsSectRu.Q).length).toBe(1);
        expect(Session.getInstance().getLinkableValues(prmsSectRu.If).length).toBe(0); // If is hidden
    });

    describe("section rectangulaire, valeurs fixes − ", () => {

        it("calcul Z1 (fluvial)", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(100.697, 3);
        });

        it("calcul Z1 impossible (fluvial)", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.section.prms.Q.singleValue = 2.2;
            const res = bief.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL);
            expect(res.log.messages[1].code).toBe(MessageCode.ERROR_BIEF_Z1_CALC_FAILED);
        });

        it("calcul Z2 (fluvial)", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z2;
            expect(() => {
                bief.CalcSerie();
            }).toThrowError();
        });

        it("calcul Z1 (torrentiel)", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z1;
            expect(() => {
                bief.CalcSerie();
            }).toThrowError();
        });

        it("calcul Z2 (torrentiel)", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(100.208, 3);
        });

        it("calcul Z2 impossible (torrentiel)", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.section.prms.Q.singleValue = 0.2;
            const res = bief.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT);
            expect(res.log.messages[1].code).toBe(MessageCode.ERROR_BIEF_Z2_CALC_FAILED);
        });

        it("calcul Z2 impossible 2 (torrentiel)", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.section.prms.LargeurBerge.singleValue = 0.1;
            const res = bief.CalcSerie();
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL);
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            expect(res.log.messages[2].code).toBe(MessageCode.ERROR_BIEF_Z2_CALC_FAILED);
        });

        it("calcul Ks (torrentiel)", () => {
            SessionSettings.precision = 1e-7;
            const bief = generateBiefTorrentiel();
            bief.section.prms.Ks.singleValue = 40; // init value for Dichotomie
            bief.prms.Z2.singleValue = 100.2061872;
            bief.calculatedParam = bief.section.prms.Ks;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(42.6, 1); // un Strickler à 10-1 près, c'est déjà bien !
        });

        it("calcul Ks (fluvial)", () => {
            SessionSettings.precision = 1e-7;
            const bief = generateBiefFluvial();
            bief.section.prms.Ks.singleValue = 40; // init value for Dichotomie
            bief.prms.Z1.singleValue = 100.6914265;
            bief.calculatedParam = bief.section.prms.Ks;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(42.82, 2);
        });

        it("calcul Q (torrentiel)", () => {
            SessionSettings.precision = 1e-7;
            const bief = generateBiefTorrentiel();
            bief.section.prms.Q.singleValue = 2; // init value for Dichotomie
            bief.prms.Z2.singleValue = 100.2061872;
            bief.calculatedParam = bief.section.prms.Q;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(1.183, 1);
        });

        it("calcul Q (fluvial)", () => {
            const bief = generateBiefFluvial();
            bief.section.prms.Q.singleValue = 1; // init value for Dichotomie
            bief.prms.Z1.singleValue = 100.7016176;
            bief.calculatedParam = bief.section.prms.Q;
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(1.218, 3);
        });

        it("calcul Q impossible (fluvial)", () => {
            const bief = generateBiefFluvial();
            bief.section.prms.Q.singleValue = 3; // init value for Dichotomie
            bief.prms.Z1.singleValue = 100.7016176;
            bief.calculatedParam = bief.section.prms.Q;
            const res = bief.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL);
            expect(res.log.messages[1].code).toBe(MessageCode.ERROR_BIEF_Z1_CALC_FAILED);
        });

    });

    describe("section rectangulaire, valeurs variées, calcul analytique − ", () => {

        it("calcul Z1 (fluvial), Long varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.prms.Long.setValues(90, 120, 5);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(7);
        });

        it("calcul Z1 (fluvial), Long varié (2)", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.prms.Long.setValues(90, 120, 4);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });

        it("calcul Z2 (torrentiel), Long varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.prms.Long.setValues(90, 120, 4);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });

        it("calcul Z2 (torrentiel), Z1 varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.prms.Z1.setValues(105.1, 105.25, 0.02);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });

        it("calcul Z2 (torrentiel), ZF1 varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.prms.ZF1.setValues(105, 105.12, 0.02);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(7);
        });

        it("calcul Z2 (torrentiel), ZF2 varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.prms.Z2;
            bief.prms.ZF2.setValues(99.1, 100, 0.1);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(10);
        });

        it("calcul Z1 (fluvial), Q varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.section.prms.Q.setValues(1, 1.2, 0.05);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(5);
        });

        it("calcul Z1 (fluvial), Ks varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.section.prms.Ks.setValues(38, 45, 1);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(8);
        });

        it("calcul Z1 (fluvial), LargeurBerge variée", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.section.prms.LargeurBerge.setValues(1.8, 2.8, 0.1);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(11);
        });

        it("calcul Z1 (fluvial), Q et Long variés", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.prms.Z1;
            bief.section.prms.Q.setValues(1, 1.2, 0.05);
            bief.prms.Long.setValues(90, 120, 4);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });
    });

    describe("section rectangulaire, valeurs variées, calcul dichotomique − ", () => {

        it("calcul Q (fluvial), Long varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.section.prms.Q;
            bief.prms.Z1.singleValue = 100.697; // équivaut à Q = 1.2
            bief.prms.Long.setValues(90, 120, 5);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(7);
        });

        it("calcul Q (torrentiel), Long varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.section.prms.Q;
            bief.prms.Z2.singleValue = 100.208; // équivaut à Q = 1.2
            bief.prms.Long.setValues(90, 120, 4);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });

        it("calcul Q (torrentiel), Z1 varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.section.prms.Q;
            bief.prms.Z2.singleValue = 100.208;
            bief.prms.Z1.setValues(105.1, 105.25, 0.02);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });

        it("calcul Q (fluvial), Ks varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.section.prms.Q;
            bief.prms.Z1.singleValue = 100.697;
            bief.section.prms.Ks.setValues(38, 45, 1);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(8);
        });

        it("calcul Ks (torrentiel), ZF1 varié", () => {
            const bief = generateBiefTorrentiel();
            bief.calculatedParam = bief.section.prms.Ks;
            bief.prms.Z2.singleValue = 100.208;
            bief.prms.ZF1.setValues(105, 105.12, 0.02);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(7);
        });

        it("calcul Ks (fluvial), Q varié", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.section.prms.Ks;
            bief.prms.Z1.singleValue = 100.697;
            bief.section.prms.Q.setValues(1, 1.2, 0.05);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(5);
        });

        it("calcul Ks (fluvial), LargeurBerge variée", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.section.prms.Ks;
            bief.prms.Z1.singleValue = 100.697;
            bief.section.prms.LargeurBerge.setValues(1.8, 2.8, 0.1);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(11);
        });

        it("calcul Ks (fluvial), Q et Long variés", () => {
            const bief = generateBiefFluvial();
            bief.calculatedParam = bief.section.prms.Ks;
            bief.prms.Z1.singleValue = 100.697;
            bief.section.prms.Q.setValues(1, 1.2, 0.05);
            bief.prms.Long.setValues(90, 120, 4);
            const res = bief.CalcSerie();
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.log.messages.length).toBe(0);
            expect(res.resultElements.length).toBe(9);
        });
    });
});
