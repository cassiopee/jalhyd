import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { cLog } from "../../../src/util/log";
import { Message, MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";
import { compareExtraResult, compareLog, extraResultLength } from "../../test_func";

/*
   Certaines valeurs de ligne d'eau torrentielle étaient auparavant remplacées par une valeur fluviale
   pour la représentation graphique du ressaut (mais fausse car torrentielle).
   Idem pour la réciproque fluvial/torrentiel.
   Le code ne faisant plus ça, les valeurs de validation ont été remplacée par les bonnes.
 */

/*
  De nombreux tests ont été désactivés suite au changement de format des résultats de calcul de remous.
  Avant : un Result contenant un ResultElement par variable (valeurs stockées dans un résultat complémentaire)
  Après : un Result contenant un ResultElement par valeur de x avec des résultats complémentaires pour chaque variable
  Ces tests sont annotés par "désactivé suite au changement de format de résultat"
*/

describe("Class Remous / section rectangulaire :", () => {
    describe("méthode Euler explicite :", () => {
        // désactivé suite au changement de format de résultat
        it("forte pente, ressaut avant l'amont", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.15, 106, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode des trapèzes

            const f: any = {
                0: 0.9767500000000044,
                5: 1.2360000000000044,
                10: 1.4910000000000045,
                15: 1.7437500000000046,
                20: 1.9955000000000045,
                25: 2.2465000000000046,
                30: 2.497500000000005,
                35: 2.7485000000000053,
                40: 2.9985000000000053,
                45: 3.2485000000000053,
                50: 3.4985000000000053,
                55: 3.7485000000000053,
                60: 3.9985000000000053,
                65: 4.248500000000004,
                70: 4.498750000000004,
                75: 4.749000000000003,
                80: 4.999250000000003,
                85: 5.249500000000002,
                90: 5.4997500000000015,
                95: 5.750000000000001,
                100: 6
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = {};
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // const x = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));
            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "amont";
            m.extraVar.x = 0;
            expLog.add(m);

            m = new Message(MessageCode.WARNING_SECTION_OVERFLOW_ABSC);
            m.extraVar.xa = 0;
            m.extraVar.xb = 100;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        it("forte pente, ressaut après l'aval", () => {
            // TODO algo à reprendre
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(105.15, 100.45, 105, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode des trapèzes

            const f = {};
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t: any = {
                0: 0.15,
                5: 0.20725000000000002,
                10: 0.23500000000000001,
                15: 0.24675000000000002,
                20: 0.251,
                25: 0.25225,
                30: 0.25249999999999995,
                35: 0.2527499999999999,
                40: 0.2529999999999999,
                45: 0.2527499999999999,
                50: 0.2529999999999999,
                55: 0.2527499999999999,
                60: 0.2529999999999999,
                65: 0.2527499999999999,
                70: 0.2529999999999999,
                75: 0.2527499999999999,
                80: 0.2529999999999999,
                85: 0.2527499999999999,
                90: 0.2529999999999999,
                95: 0.2527499999999999,
                100: 0.253
            }; // dernière valeur modifiée pour la raison en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // const x = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 95;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "aval";
            m.extraVar.x = 100;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        xit("forte pente, ressaut (plusieurs points) à l'intérieur du bief", () => {
        // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)

            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.425, 101, 100.275, 100,
                5.5,  // Long= Longueur du bief
                0.25,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode des trapèzes

            // let f = { "0": 0.15, "1": 0.7560000000000007, "2": 0.8120000000000005, "3": 0.8670000000000002,
            // "4": 0.9205000000000001, "5": 0.9735, "5.5": 1, "5.25": 0.98675, "4.75": 0.96025,
            // "4.5": 0.9470000000000001, "4.25": 0.9337500000000001, "3.75": 0.9072500000000001,
            // "3.5": 0.8940000000000001, "3.25": 0.8807500000000001, "2.75": 0.8532500000000003,
            // "2.5": 0.8395000000000004, "2.25": 0.8257500000000004, "1.75": 0.7982500000000006,
            // "1.5": 0.7845000000000006, "1.25": 0.7702500000000007, "0.75": 0.7417500000000007,
            // "0.5": 0.7275000000000007, "0.25": 0.7132500000000007 };
            const f: any = {
                0: 0.698,
                1: 0.7560000000000007,
                2: 0.8120000000000005,
                3: 0.8670000000000002,
                4: 0.9205000000000001,
                5: 0.9735,
                5.5: 1,
                5.25: 0.98675,
                4.75: 0.96025,
                4.5: 0.9470000000000001,
                4.25: 0.9337500000000001,
                3.75: 0.9072500000000001,
                3.5: 0.8940000000000001,
                3.25: 0.8807500000000001,
                2.75: 0.8532500000000003,
                2.5: 0.8395000000000004,
                2.25: 0.8257500000000004,
                1.75: 0.7982500000000006,
                1.5: 0.7845000000000006,
                1.25: 0.7702500000000007,
                0.75: 0.7417500000000007,
                0.5: 0.7275000000000007,
                0.25: 0.7132500000000007
            }; // première valeur modifiée pour la raison en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = {
                0: 0.15,
                1: 0.161,
                2: 0.17200000000000001,
                3: 0.18100000000000002,
                0.25: 0.15275,
                0.5: 0.1555,
                0.75: 0.15825,
                1.25: 0.16375,
                1.5: 0.1665,
                1.75: 0.16925,
                2.25: 0.17425000000000002,
                2.5: 0.17650000000000002,
                2.75: 0.17875000000000002,
                3.25: 0.18325000000000002,
                3.5: 0.18550000000000003,
                3.75: 0.9072500000000001
            };
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5, 4.75, 5, 5.25, 5.5];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 3.75;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        it("forte pente, pas de ressaut, Yaval < Yc, Yamont < Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(101.6, 100.3, 101.5, 100,
                30,  // Long= Longueur du bief
                1,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode trapèzes

            expect(extraResultLength(res, "flu") === 0).toBeTruthy(
                "la ligne d'eau fluviale ne devrait comporter aucune valeur");

            const t: any = {
                0: 0.1, 1: 0.11425000000000002, 2: 0.12750000000000003, 3: 0.14025000000000004,
                4: 0.15200000000000005, 5: 0.16325000000000006, 6: 0.17350000000000007, 7: 0.18325000000000008,
                8: 0.1920000000000001, 9: 0.1997500000000001, 10: 0.2070000000000001, 11: 0.2137500000000001,
                12: 0.2195000000000001, 13: 0.22475000000000012, 14: 0.22900000000000012, 15: 0.23275000000000012,
                16: 0.23600000000000013, 17: 0.23875000000000013, 18: 0.24100000000000013, 19: 0.24325000000000013,
                20: 0.24500000000000013, 21: 0.24625000000000014, 22: 0.24750000000000014, 23: 0.2485000000000001,
                24: 0.2492500000000001, 25: 0.2502500000000001, 26: 0.25050000000000006, 27: 0.25075000000000003,
                28: 0.251, 29: 0.25125, 30: 0.25149999999999995
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.01);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.01);

            // const x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
            //     19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        xit("forte pente, pas de ressaut, Yaval < Yc, Yn < Yamont < Yc", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.85, 100.3, 100.5, 100,
                10,  // Long= Longueur du bief
                0.1,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            expect(extraResultLength(res, "flu") === 0)
                .toBeTruthy("la ligne d'eau fluviale ne devrait comporter aucune valeur");

            const t = {
                0.000: 0.35,
                0.100: 0.345,
                0.200: 0.34,
                0.300: 0.336,
                0.400: 0.332,
                0.500: 0.329,
                0.600: 0.326,
                0.700: 0.323,
                0.800: 0.32,
                0.900: 0.318,
                1.000: 0.316,
                1.100: 0.313,
                1.200: 0.311,
                1.300: 0.309,
                1.400: 0.308,
                1.500: 0.306,
                1.600: 0.305,
                1.700: 0.303,
                1.800: 0.302,
                1.900: 0.3,
                2.000: 0.298,
                2.100: 0.297,
                2.200: 0.295,
                2.300: 0.294,
                2.400: 0.293,
                2.500: 0.292,
                2.600: 0.291,
                2.700: 0.291,
                2.800: 0.29,
                2.900: 0.289,
                3.000: 0.288,
                3.100: 0.287,
                3.200: 0.287,
                3.300: 0.286,
                3.400: 0.285,
                3.500: 0.284,
                3.600: 0.283,
                3.700: 0.283,
                3.800: 0.282,
                3.900: 0.281,
                4.000: 0.28,
                4.100: 0.28,
                4.200: 0.279,
                4.300: 0.278,
                4.400: 0.277,
                4.500: 0.276,
                4.600: 0.276,
                4.700: 0.275,
                4.800: 0.274,
                4.900: 0.273,
                5.000: 0.272,
                5.100: 0.272,
                5.200: 0.271,
                5.300: 0.27,
                5.400: 0.269,
                5.500: 0.269,
                5.600: 0.269,
                5.700: 0.269,
                5.800: 0.269,
                5.900: 0.269,
                6.000: 0.269,
                6.100: 0.269,
                6.200: 0.269,
                6.300: 0.269,
                6.400: 0.269,
                6.500: 0.269,
                6.600: 0.269,
                6.700: 0.269,
                6.800: 0.269,
                6.900: 0.269,
                7.000: 0.269,
                7.100: 0.269,
                7.200: 0.269,
                7.300: 0.269,
                7.400: 0.269,
                7.500: 0.269,
                7.600: 0.269,
                7.700: 0.269,
                7.800: 0.269,
                7.900: 0.269,
                8.000: 0.269,
                8.100: 0.269,
                8.200: 0.269,
                8.300: 0.269,
                8.400: 0.269,
                8.500: 0.269,
                8.600: 0.269,
                8.700: 0.269,
                8.800: 0.269,
                8.900: 0.269,
                9.000: 0.269,
                9.100: 0.269,
                9.200: 0.269,
                9.300: 0.269,
                9.400: 0.269,
                9.500: 0.269,
                9.600: 0.269,
                9.700: 0.269,
                9.800: 0.269,
                9.900: 0.269,
                10.000: 0.269
            };
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 0.100, 0.200, 0.300, 0.400, 0.500, 0.600, 0.700, 0.800, 0.900, 1.000, 1.100, 1.200, 1.300, 1.400, 1.500, 1.600, 1.700, 1.800, 1.900, 2.000, 2.100, 2.200, 2.300, 2.400, 2.500, 2.600, 2.700, 2.800, 2.900, 3.000, 3.100, 3.200, 3.300, 3.400, 3.500, 3.600, 3.700, 3.800, 3.900, 4.000, 4.100, 4.200, 4.300, 4.400, 4.500, 4.600, 4.700, 4.800, 4.900, 5.000, 5.100, 5.200, 5.300, 5.400, 5.500, 5.600, 5.700, 5.800, 5.900, 6.000, 6.100, 6.200, 6.300, 6.400, 6.500, 6.600, 6.700, 6.800, 6.900, 7.000, 7.100, 7.200, 7.300, 7.400, 7.500, 7.600, 7.700, 7.800, 7.900, 8.000, 8.100, 8.200, 8.300, 8.400, 8.500, 8.600, 8.700, 8.800, 8.900, 9.000, 9.100, 9.200, 9.300, 9.400, 9.500, 9.600, 9.700, 9.800, 9.900, 10.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.253;
            expLog.add(m);

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            compareLog(res.globalLog, expLog);
        });
    });
});
