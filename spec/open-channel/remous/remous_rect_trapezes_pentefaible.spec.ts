import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { cLog } from "../../../src/util/log";
import { Message, MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";
import { compareExtraResult, compareLog, extraResultLength } from "../../test_func";

/*
  Le code de modification des lignes fluviale et torrentielle a été modifié, on enlève un point de plus
  ligne d'eau complète : if (iSens * (rXCC - rX) < 0) remplacé par if (iSens * (rXCC - rX) <= 0)
  lign d'eau partielle : if (iSens * (rXCN - xRst) > 0)  remplacé par if (iSens * (rXCN - xRst) >= 0)
  Les données de validation ont été modifiées en conséquence
*/

describe("Class Remous / section rectangulaire :", () => {
    describe("méthode trapèzes :", () => {
        it("faible pente, ressaut avant l'amont", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.4, 100.403, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            const f: any = {
                100.000: 0.403,
                95.000: 0.524,
                90.000: 0.558,
                85.000: 0.584,
                80.000: 0.604,
                75.000: 0.621,
                70.000: 0.637,
                65.000: 0.65,
                60.000: 0.662,
                55.000: 0.673,
                50.000: 0.684,
                45.000: 0.693,
                40.000: 0.701,
                35.000: 0.709,
                30.000: 0.717,
                25.000: 0.725,
                20.000: 0.731,
                15.000: 0.738,
                10.000: 0.744,
                5.000: 0.75,
                0.000: 0.755
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = {};
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 5;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
            m.extraVar.sens = "amont";
            m.extraVar.x = 0;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        // désactivé suite au changement de format de résultat
        xit("faible pente, ressaut (1 point) à l'intérieur du bief", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.25, 100.403, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            // tslint:disable-next-line:max-line-length
            // let f = { 100.000: 0.403, 95.000: 0.524, 90.000: 0.558, 85.000: 0.584, 80.000: 0.604, 75.000: 0.621, 70.000: 0.637, 65.000: 0.65, 60.000: 0.662, 55.000: 0.673, 50.000: 0.684, 45.000: 0.693, 40.000: 0.701, 35.000: 0.709, 30.000: 0.717, 25.000: 0.725, 20.000: 0.731, 15.000: 0.738, 10.000: 0.744, 5.000: 0.75, 0.000: 0.15 };
            const f: any = {
                100.000: 0.403,
                95.000: 0.524,
                90.000: 0.558,
                85.000: 0.584,
                80.000: 0.604,
                75.000: 0.621,
                70.000: 0.637,
                65.000: 0.65,
                60.000: 0.662,
                55.000: 0.673,
                50.000: 0.684,
                45.000: 0.693,
                40.000: 0.701,
                35.000: 0.709,
                30.000: 0.717,
                25.000: 0.725,
                20.000: 0.731,
                15.000: 0.738,
                10.000: 0.744,
                5.000: 0.75
            };  // dernière valeur supprimée pour la raison en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            // let t = { 0.000: 0.15, 5.000: 0.75 };
            const t = { 0.000: 0.15 }; // dernière valeur supprimée pour la raison en tête de fichier
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
            m.extraVar.x = 15;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 5;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        xit("faible pente, ressaut (plusieurs points) à l'intérieur du bief (1)", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.015, 100.403, 100.005, 100,
                5,  // Long= Longueur du bief
                0.25,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            // //dx = 0.05
            // tslint:disable-next-line:max-line-length
            // let f = { 5.000: 0.403, 4.950: 0.414, 4.900: 0.419, 4.850: 0.423, 4.800: 0.426, 4.750: 0.429, 4.700: 0.432, 4.650: 0.434, 4.600: 0.436, 4.550: 0.438, 4.500: 0.439, 4.450: 0.441, 4.400: 0.443, 4.350: 0.445, 4.300: 0.446, 4.250: 0.447, 4.200: 0.449, 4.150: 0.45, 4.100: 0.451, 4.050: 0.452, 4.000: 0.454, 3.950: 0.455, 3.900: 0.456, 3.850: 0.457, 3.800: 0.458, 3.750: 0.46, 3.700: 0.461, 3.650: 0.462, 3.600: 0.463, 3.550: 0.465, 3.500: 0.466, 3.450: 0.467, 3.400: 0.468, 3.350: 0.469, 3.300: 0.469, 3.250: 0.47, 3.200: 0.471, 3.150: 0.471, 3.100: 0.472, 3.050: 0.472, 3.000: 0.473, 2.950: 0.474, 2.900: 0.474, 2.850: 0.475, 2.800: 0.476, 2.750: 0.476, 2.700: 0.477, 2.650: 0.477, 2.600: 0.478, 2.550: 0.479, 2.500: 0.479, 2.450: 0.48, 2.400: 0.48, 2.350: 0.481, 2.300: 0.482, 2.250: 0.482, 2.200: 0.483, 2.150: 0.483, 2.100: 0.484, 2.050: 0.485, 2.000: 0.485, 1.950: 0.486, 1.900: 0.486, 1.850: 0.487, 1.800: 0.488, 1.750: 0.488, 1.700: 0.489, 1.650: 0.49, 1.600: 0.49, 1.550: 0.491, 1.500: 0.491, 1.450: 0.492, 1.400: 0.493, 1.350: 0.493, 1.300: 0.494, 1.250: 0.494, 1.200: 0.495, 1.150: 0.496, 1.100: 0.496, 1.050: 0.497, 1.000: 0.497, 0.950: 0.498, 0.900: 0.499, 0.850: 0.499, 0.800: 0.5, 0.750: 0.501, 0.700: 0.501, 0.650: 0.502, 0.600: 0.502, 0.550: 0.503, 0.500: 0.504, 0.450: 0.504, 0.400: 0.505, 0.350: 0.505, 0.300: 0.506, 0.250: 0.507, 0.200: 0.507, 0.150: 0.508, 0.100: 0.508, 0.050: 0.509, 0.000: 0.01 };
            // compareObject("Yfluvial", res["flu"], f, 0.03);

            // tslint:disable-next-line:max-line-length
            // //let t = { 0.000: 0.01, 0.050: 0.011, 0.100: 0.013, 0.150: 0.015, 0.200: 0.016, 0.250: 0.018, 0.300: 0.019, 0.350: 0.02, 0.400: 0.021, 0.450: 0.022, 0.500: 0.022, 0.550: 0.023, 0.600: 0.024, 0.650: 0.025, 0.700: 0.026, 0.750: 0.026, 0.800: 0.027, 0.850: 0.028, 0.900: 0.031750000000000014, 0.95: 0.5042500000000006 }; // ok
            // tslint:disable-next-line:max-line-length
            // let t = { 0.000: 0.01, 0.050: 0.011, 0.100: 0.013, 0.150: 0.015, 0.200: 0.016, 0.250: 0.018, 0.300: 0.019, 0.350: 0.02, 0.400: 0.021, 0.450: 0.022, 0.500: 0.022, 0.550: 0.023, 0.600: 0.024, 0.650: 0.025, 0.700: 0.026, 0.750: 0.026, 0.800: 0.027, 0.850: 0.028, 0.900: 0.499 };
            // compareObject("Ytorrentiel", res["tor"], t, 0.03);

            // tslint:disable-next-line:max-line-length
            // let x = [0.000, 0.050, 0.100, 0.150, 0.200, 0.250, 0.300, 0.350, 0.400, 0.450, 0.500, 0.550, 0.600, 0.650, 0.700, 0.750, 0.800, 0.850, 0.900, 0.950, 1.000, 1.050, 1.100, 1.150, 1.200, 1.250, 1.300, 1.350, 1.400, 1.450, 1.500, 1.550, 1.600, 1.650, 1.700, 1.750, 1.800, 1.850, 1.900, 1.950, 2.000, 2.050, 2.100, 2.150, 2.200, 2.250, 2.300, 2.350, 2.400, 2.450, 2.500, 2.550, 2.600, 2.650, 2.700, 2.750, 2.800, 2.850, 2.900, 2.950, 3.000, 3.050, 3.100, 3.150, 3.200, 3.250, 3.300, 3.350, 3.400, 3.450, 3.500, 3.550, 3.600, 3.650, 3.700, 3.750, 3.800, 3.850, 3.900, 3.950, 4.000, 4.050, 4.100, 4.150, 4.200, 4.250, 4.300, 4.350, 4.400, 4.450, 4.500, 4.550, 4.600, 4.650, 4.700, 4.750, 4.800, 4.850, 4.900, 4.950, 5.000];
            // compareArray("abscisses", res["trX"], x);

            // dx = 0.25
            const f: any = {
                5.000: 0.403,
                4.750: 0.43,
                4.500: 0.44,
                4.250: 0.448,
                4.000: 0.455,
                3.750: 0.46,
                3.500: 0.465,
                3.250: 0.47,
                3.000: 0.474,
                2.750: 0.479,
                2.500: 0.482,
                2.250: 0.486,
                2.000: 0.489,
                1.750: 0.492,
                1.500: 0.495,
                1.250: 0.498,
                1.000: 0.501,
                0.750: 0.504,
                0.500: 0.506,
                0.250: 0.508,
                0.000: 0.01
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t = { 0.000: 0.01, 0.250: 0.022, 0.500: 0.027, 0.750: 0.033, 1.000: 0.501 };
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.000, 0.250, 0.500, 0.750, 1.000, 1.250, 1.500, 1.750, 2.000, 2.250, 2.500, 2.750, 3.000, 3.250, 3.500, 3.750, 4.000, 4.250, 4.500, 4.750, 5.000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 1;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });

        xit("faible pente, ressaut (plusieurs points) à l'intérieur du bief (2)", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.015, 100.403, 100.005, 100,
                5,  // Long= Longueur du bief
                0.05,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            // données de validation : version PHP (Oct 2017) méthode trapèzes

            const f: any = {
                5.0000: 0.403,
                4.9500: 0.415,
                4.9000: 0.42,
                4.8500: 0.423,
                4.8000: 0.426,
                4.7500: 0.429,
                4.7000: 0.432,
                4.6500: 0.434,
                4.6000: 0.436,
                4.5500: 0.438,
                4.5000: 0.44,
                4.4500: 0.441,
                4.4000: 0.443,
                4.3500: 0.445,
                4.3000: 0.446,
                4.2500: 0.448,
                4.2000: 0.449,
                4.1500: 0.45,
                4.1000: 0.452,
                4.0500: 0.453,
                4.0000: 0.454,
                3.9500: 0.455,
                3.9000: 0.457,
                3.8500: 0.458,
                3.8000: 0.459,
                3.7500: 0.46,
                3.7000: 0.461,
                3.6500: 0.462,
                3.6000: 0.463,
                3.5500: 0.464,
                3.5000: 0.465,
                3.4500: 0.466,
                3.4000: 0.467,
                3.3500: 0.468,
                3.3000: 0.469,
                3.2500: 0.47,
                3.2000: 0.471,
                3.1500: 0.471,
                3.1000: 0.472,
                3.0500: 0.473,
                3.0000: 0.474,
                2.9500: 0.475,
                2.9000: 0.476,
                2.8500: 0.476,
                2.8000: 0.477,
                2.7500: 0.478,
                2.7000: 0.479,
                2.6500: 0.479,
                2.6000: 0.48,
                2.5500: 0.481,
                2.5000: 0.482,
                2.4500: 0.482,
                2.4000: 0.483,
                2.3500: 0.484,
                2.3000: 0.484,
                2.2500: 0.485,
                2.2000: 0.486,
                2.1500: 0.487,
                2.1000: 0.487,
                2.0500: 0.488,
                2.0000: 0.489,
                1.9500: 0.489,
                1.9000: 0.49,
                1.8500: 0.49,
                1.8000: 0.491,
                1.7500: 0.492,
                1.7000: 0.492,
                1.6500: 0.493,
                1.6000: 0.494,
                1.5500: 0.494,
                1.5000: 0.495,
                1.4500: 0.495,
                1.4000: 0.496,
                1.3500: 0.497,
                1.3000: 0.497,
                1.2500: 0.498,
                1.2000: 0.498,
                1.1500: 0.499,
                1.1000: 0.499,
                1.0500: 0.5,
                1.0000: 0.5,
                0.9500: 0.501,
                0.9000: 0.502,
                0.8500: 0.502,
                0.8000: 0.503,
                0.7500: 0.503,
                0.7000: 0.504,
                0.6500: 0.504,
                0.6000: 0.505,
                0.5500: 0.505,
                0.5000: 0.506,
                0.4500: 0.506,
                0.4000: 0.507,
                0.3500: 0.507,
                0.3000: 0.508,
                0.2500: 0.508,
                0.2000: 0.509,
                0.1500: 0.509,
                0.1000: 0.51,
                0.0500: 0.51,
                0.0000: 0.01
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            const t: any = {
                0.0000: 0.01,
                0.0500: 0.011,
                0.1000: 0.013,
                0.1500: 0.014,
                0.2000: 0.015,
                0.2500: 0.017,
                0.3000: 0.018,
                0.3500: 0.019,
                0.4000: 0.02,
                0.4500: 0.021,
                0.5000: 0.022,
                0.5500: 0.024,
                0.6000: 0.025,
                0.6500: 0.026,
                0.7000: 0.027,
                0.7500: 0.028,
                0.8000: 0.029,
                0.8500: 0.03,
                0.9000: 0.502
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.03);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.03);

            // tslint:disable-next-line:max-line-length
            // const x = [0.0000, 0.0500, 0.1000, 0.1500, 0.2000, 0.2500, 0.3000, 0.3500, 0.4000, 0.4500, 0.5000, 0.5500, 0.6000, 0.6500, 0.7000, 0.7500, 0.8000, 0.8500, 0.9000, 0.9500, 1.0000, 1.0500, 1.1000, 1.1500, 1.2000, 1.2500, 1.3000, 1.3500, 1.4000, 1.4500, 1.5000, 1.5500, 1.6000, 1.6500, 1.7000, 1.7500, 1.8000, 1.8500, 1.9000, 1.9500, 2.0000, 2.0500, 2.1000, 2.1500, 2.2000, 2.2500, 2.3000, 2.3500, 2.4000, 2.4500, 2.5000, 2.5500, 2.6000, 2.6500, 2.7000, 2.7500, 2.8000, 2.8500, 2.9000, 2.9500, 3.0000, 3.0500, 3.1000, 3.1500, 3.2000, 3.2500, 3.3000, 3.3500, 3.4000, 3.4500, 3.5000, 3.5500, 3.6000, 3.6500, 3.7000, 3.7500, 3.8000, 3.8500, 3.9000, 3.9500, 4.0000, 4.0500, 4.1000, 4.1500, 4.2000, 4.2500, 4.3000, 4.3500, 4.4000, 4.4500, 4.5000, 4.5500, 4.6000, 4.6500, 4.7000, 4.7500, 4.8000, 4.8500, 4.9000, 4.9500, 5.0000];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

            m = new Message(MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
            m.extraVar.xmin = 0;
            m.extraVar.xmax = 0.9;
            expLog.add(m);

            compareLog(res.globalLog, expLog);
        });
    });

    it("faible pente, pas de ressaut, Yamont > Yc, Yaval > Yn", () => {
        const prms = new ParamsSectionRectang(undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            2, // Q=Débit
            0.001, // If=pente du fond
            1, // YB=hauteur de berge
        );
        SessionSettings.precision = precDist;

        const sect = new cSnRectang(prms);

        const prem = new CourbeRemousParams(100.6, 101, 100.1, 100,
            100,  // Long= Longueur du bief
            5,  // Dx=Pas d'espace
        );

        const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = rem.calculRemous(undefined);

        // données de validation : version PHP (Oct 2017) méthode trapèzes

        const f: any = {
            100.000: 1,
            95.000: 0.999,
            90.000: 0.999,
            85.000: 0.998,
            80.000: 0.997,
            75.000: 0.997,
            70.000: 0.996,
            65.000: 0.996,
            60.000: 0.995,
            55.000: 0.994,
            50.000: 0.994,
            45.000: 0.993,
            40.000: 0.992,
            35.000: 0.992,
            30.000: 0.991,
            25.000: 0.991,
            20.000: 0.99,
            15.000: 0.989,
            10.000: 0.989,
            5.000: 0.988,
            0.000: 0.988
        };
        // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
        for (const fk of Object.keys(f)) {
            f[fk] = f[fk] + rem.getCoteFond(Number(fk));
        }
        // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
        compareExtraResult("Yfluvial", res, "flu", f, 0.03);

        expect(extraResultLength(res, "tor") === 0)
            .toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

        // tslint:disable-next-line:max-line-length
        // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
        // compareArray("abscisses", res.getValue("trX"), x);

        const expLog = new cLog();

        let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
        m.extraVar.Yc = 0.403;
        expLog.add(m);

        m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
        m.extraVar.Yn = 0.953;
        expLog.add(m);

        expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

        expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

        compareLog(res.globalLog, expLog);
    });

    it("faible pente, pas de ressaut, Yamont > Yc, Yc < Yaval < Yn", () => {
        const prms = new ParamsSectionRectang(undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            2, // Q=Débit
            0.001, // If=pente du fond
            1, // YB=hauteur de berge
        );
        SessionSettings.precision = precDist;

        const sect = new cSnRectang(prms);

        const prem = new CourbeRemousParams(100.6, 100.7, 100.1, 100,
            100,  // Long= Longueur du bief
            5,  // Dx=Pas d'espace
        );

        const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = rem.calculRemous(undefined);

        // données de validation : version PHP (Oct 2017) méthode trapèzes

        const f: any = {
            100.000: 0.7,
            95.000: 0.708,
            90.000: 0.716,
            85.000: 0.723,
            80.000: 0.73,
            75.000: 0.737,
            70.000: 0.743,
            65.000: 0.749,
            60.000: 0.754,
            55.000: 0.76,
            50.000: 0.765,
            45.000: 0.77,
            40.000: 0.775,
            35.000: 0.779,
            30.000: 0.783,
            25.000: 0.787,
            20.000: 0.792,
            15.000: 0.795,
            10.000: 0.799,
            5.000: 0.803,
            0.000: 0.806
        };
        // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
        for (const fk of Object.keys(f)) {
            f[fk] = f[fk] + rem.getCoteFond(Number(fk));
        }
        // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
        compareExtraResult("Yfluvial", res, "flu", f, 0.03);

        expect(extraResultLength(res, "tor") === 0)
            .toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

        // tslint:disable-next-line:max-line-length
        // const x = [0.000, 5.000, 10.000, 15.000, 20.000, 25.000, 30.000, 35.000, 40.000, 45.000, 50.000, 55.000, 60.000, 65.000, 70.000, 75.000, 80.000, 85.000, 90.000, 95.000, 100.000];
        // compareArray("abscisses", res.getValue("trX"), x);

        const expLog = new cLog();

        let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
        m.extraVar.Yc = 0.403;
        expLog.add(m);

        m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
        m.extraVar.Yn = 0.953;
        expLog.add(m);

        expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

        expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

        compareLog(res.globalLog, expLog);
    });

    it("faible pente, pas de fluvial, torrentiel tronqué, calcul Hs", () => {
        const prms = new ParamsSectionRectang(undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            2, // Q=Débit
            0.001, // If=pente du fond
            1, // YB=hauteur de berge
        );
        SessionSettings.precision = precDist;

        const sect = new cSnRectang(prms);

        const prem = new CourbeRemousParams(100.25, 100.4, 100.1, 100,
            100,  // Long= Longueur du bief
            5,  // Dx=Pas d'espace
        );

        const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = rem.calculRemous("Hs");

        // données de validation : version PHP (Oct 2017) méthode trapèzes

        expect(extraResultLength(res, "flu") === 0)
            .toBeTruthy("la ligne d'eau fluviale ne devrait comporter aucune valeur");

        const t: any = { 0.000: 0.15, 5.000: 0.239, 10.000: 0.34 };
        // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
        for (const tk of Object.keys(t)) {
            t[tk] = t[tk] + rem.getCoteFond(Number(tk));
        }
        // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.008);
        compareExtraResult("Ytorrentiel", res, "tor", t, 0.008);

        // const x = [0.000, 5.000, 10.000];
        // compareArray("abscisses", res.getValue("trX"), x);

        const extraHS: any = { 0.000: 101.7, 5.000: 100.903, 10.000: 100.708 };
        // compareObject("extra (Hs)", res.getValue("Hs"), extraHS, 0.001);
        compareExtraResult("extra (Hs)", res, "Hs", extraHS, 0.001);

        const expLog = new cLog();

        let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
        m.extraVar.Yc = 0.403;
        expLog.add(m);

        m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
        m.extraVar.Yn = 0.953;
        expLog.add(m);

        expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));

        expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));

        m = new Message(MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
        m.extraVar.x = 15;
        expLog.add(m);

        compareLog(res.globalLog, expLog);
    });
});
