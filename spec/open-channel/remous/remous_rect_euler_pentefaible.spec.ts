import { Session } from "../../../src/index";
import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { cLog } from "../../../src/util/log";
import { Message, MessageCode } from "../../../src/util/message";
import { Result } from "../../../src/util/result";
import { precDist } from "../../test_config";
import { compareExtraResult, compareLog, extraResultLength } from "../../test_func";

describe("Class Remous / section rectangulaire :", () => {
    describe("méthode Euler explicite :", () => {
        it("faible pente, pas de ressaut, Yamont > Yc, Yaval > Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1 // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.6, 101, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res: Result = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode des trapèzes

            const f: any = {
                0: 0.9872500000000014, 5: 0.9872500000000014, 10: 0.9872500000000014,
                15: 0.9872500000000014, 20: 0.9880000000000013, 25: 0.9887500000000012,
                30: 0.9895000000000012, 35: 0.9902500000000011, 40: 0.991000000000001,
                45: 0.9917500000000009, 50: 0.9925000000000008, 55: 0.9932500000000007,
                60: 0.9940000000000007, 65: 0.9947500000000006, 70: 0.9955000000000005,
                75: 0.9962500000000004, 80: 0.9970000000000003, 85: 0.9977500000000002,
                90: 0.9985000000000002, 95: 0.9992500000000001, 100: 1
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            expect(
                extraResultLength(res, "tor") === 0
            ).toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

            // const x = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

            compareLog(res.globalLog, expLog);
        });

        it("faible pente, pas de ressaut, Yamont > Yc, Yc < Yaval < Yn", () => {
            const prms = new ParamsSectionRectang(undefined, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1 // YB=hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnRectang(prms);

            const prem = new CourbeRemousParams(100.6, 100.7, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.EulerExplicite);

            const res = rem.calculRemous(undefined);

            // données de validation : version Typescript (Oct 2017) méthode trapèzes

            const f: any = {
                0: 0.805499999999999, 5: 0.802249999999999, 10: 0.7984999999999991,
                15: 0.7947499999999992, 20: 0.7909999999999993, 25: 0.7867499999999993,
                30: 0.7824999999999993, 35: 0.7782499999999993, 40: 0.7739999999999994,
                45: 0.7692499999999994, 50: 0.7644999999999995, 55: 0.7592499999999995,
                60: 0.7539999999999996, 65: 0.7482499999999996, 70: 0.7424999999999997,
                75: 0.7362499999999997, 80: 0.7299999999999998, 85: 0.7232499999999998,
                90: 0.7159999999999999, 95: 0.7082499999999999, 100: 0.7
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const fk of Object.keys(f)) {
                f[fk] = f[fk] + rem.getCoteFond(Number(fk));
            }
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.03);
            compareExtraResult("Yfluvial", res, "flu", f, 0.03);

            expect(
                extraResultLength(res, "tor") === 0
            ).toBeTruthy("la ligne d'eau torrentielle ne devrait comporter aucune valeur");

            // const x = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
            // compareArray("abscisses", res.getValue("trX"), x);

            const expLog = new cLog();

            let m = new Message(MessageCode.INFO_REMOUS_H_CRITIQUE);
            m.extraVar.Yc = 0.403;
            expLog.add(m);

            m = new Message(MessageCode.INFO_REMOUS_H_NORMALE);
            m.extraVar.Yn = 0.953;
            expLog.add(m);

            expLog.add(new Message(MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));

            expLog.add(new Message(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));

            compareLog(res.globalLog, expLog);
        });

        it("débordement entre les abscisses 0 et 65", () => {
            // tslint:disable-next-line:max-line-length
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-12-02T13:36:06.932Z"},"settings":{"precision":0.0001,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MzEyMG","props":{"methodeResolution":"Trapezes","varCalc":"","calcType":"CourbeRemous"},"meta":{"title":"Remous"},"children":[{"uid":"NXAwaT","props":{"methodeResolution":"Trapezes","varCalc":"","calcType":"Section","nodeType":"SectionRectangle"},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"SINGLE","value":1.2},{"symbol":"YB","mode":"SINGLE","value":0.5},{"symbol":"Y","mode":"SINGLE","value":0.8},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.5}]}],"parameters":[{"symbol":"Long","mode":"SINGLE","value":100},{"symbol":"Dx","mode":"SINGLE","value":5},{"symbol":"Z1","mode":"SINGLE","value":100.25},{"symbol":"Z2","mode":"SINGLE","value":100.4},{"symbol":"ZF1","mode":"SINGLE","value":100.1},{"symbol":"ZF2","mode":"SINGLE","value":100}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const rem = Session.getInstance().findNubByUid("MzEyMG") as CourbeRemous;
            const res = rem.calculRemous(undefined);
            expect(rem.section.prms.YB.singleValue).toBe(0.5);
            expect(res.globalLog.messages.length).toBe(7); // 4 infos, 1 haut. critique, 1 ressaut, 1 débordement
            expect(res.globalLog.messages[6].code).toBe(MessageCode.WARNING_SECTION_OVERFLOW_ABSC);
            expect(res.globalLog.messages[6].extraVar.xa).toBe(0);
            expect(res.globalLog.messages[6].extraVar.xb).toBe(65);
        });
    });
});
