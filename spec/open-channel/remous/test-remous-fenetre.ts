// // tslint:disable:no-console
// import { CourbeRemous, CourbeRemousParams, MethodeResolution } from "../../src/remous/remous";
// import { cSnRectang, ParamsSectionRectang } from "../../src/section/section_rectang";
// import { equalEpsilon, compareArray } from "../test_func";
// import { Result } from "../../src/util/result";
// import { ResultElement } from "../../src/util/resultelement";

// /*
//    Tentative de validation automatique du calcul des courbes de remous.
//    Le principe :
//     - calculer d'abord les remous sur un bief (avec par exemple un ressaut au milieu) qui servira de référence
//     - déplacer dans ce bief une fenêtre dans laquelle on calcule les courbes de remous
//     - comparer les courbes obtenus dans la fenêtre avec celles se trouvant au même endroit dans le bief de référence

//    Ce code de validation n'est pas sous la forme d'un test unitaire, il doit être lancé à la main :
//    $ npm run runtestremous
//  */

// export function compareObject(
//     s: string,
//     objTest: { [key: number]: number },
//     objValid: { [key: number]: number },
//     epsilon: number,
// ): boolean {
//     const n1 = Object.keys(objTest).length;
//     const n2 = Object.keys(objValid).length;
//     let b: boolean = n1 === n2;
//     expect(b).toBeTruthy(s + ": longueur incorrecte " + n1 + ", devrait etre " + n2);
//     if (!b) { return false; }

//     const kTest = Object.keys(objTest);
//     kTest.sort((v1, v2) => {
//         if (+v1 > +v2) { return 1; }
//         if (+v1 < +v2) { return -1; }
//         return 0;
//     });

//     const kValid = Object.keys(objValid);
//     kValid.sort((v1, v2) => {
//         if (+v1 > +v2) { return 1; }
//         if (+v1 < +v2) { return -1; }
//         return 0;
//     });

//     for (let i = 0; i < n1; i++) {
//         // let v1: number = objTest[+Object.keys(objTest)[i]];
//         const v1: number = objTest[+kTest[i]];
//         // let v2: number = objValid[+Object.keys(objValid)[i]];
//         const v2: number = objValid[+kValid[i]];
//         b = equalEpsilon(v1, v2, epsilon);
//         expect(b).toBeTruthy(s + " : " + i + "ieme valeur incorrecte " + v1 + ", devrait etre " + v2);
//         if (!b) { return false; }
//     }

//     return true;
// }

// function logObject(obj: {}, m?: string) {
//     // évite le message "Value below was evaluated just now" dans le debugger de Chrome
//     if (m === undefined) {
//         console.log(JSON.stringify(obj));
//     } else {
//         console.log(m + " " + JSON.stringify(obj));
//     }
// }

// function logArray(m: string, arr: { [key: string]: number }) {
//     console.log(m);

//     const keys = Object.keys(arr);
//     keys.sort((a, b) => {
//         if (+a > +b) { return 1; }
//         if (+a < +b) { return -1; }
//         return 0;
//     });

//     for (const k of keys) {
//         console.log("[" + (+k).toFixed(3) + "]=" + arr[k]);
//     }
// }

// // function computeFenetreTrapez(yAmont: number, yAval: number, longBief: number, dxBief: number, prec: number) {
// //     let prms = new ParamsSectionTrapez(
// //         2.5, // largeur de fond
// //         0.56, // fruit
// //         undefined, // tirant d'eau
// //         40, //  Ks=Strickler
// //         2.5, // Q=Débit
// //         0.05, // If=pente du fond
// //         prec, // précision
// //         1 // YB=hauteur de berge
// //     );

// //     let sect = new cSnTrapez(prms);

// //     let prem = new CourbeRemousParams(sect,
// //         yAmont, // Yamont = tirant amont
// //         yAval, // Yaval = tirant aval
// //         longBief,  // Long = Longueur du bief
// //         dxBief,  // Dx = Pas d'espace
// //         MethodeResolution.Trapezes
// //     );

// //     // console.log("computeFenetre longBief=" + longBief + " y am/av=" + yAmont + " " + yAval);
// //     console.log("computeFenetre y am/av=" + yAmont + " " + yAval);

// //     let rem = new CourbeRemous(sect, prem);
// //     return rem.calculRemous(undefined);
// // }

// function saveCSV(res: any) {
//     const fs = require("fs");

//     let csvFlu = "x;flu;tor";
//     for (const x of res.trX) {
//         let flu = "";
//         if (res.flu[x] !== undefined) {
//             flu = String(res.flu[x]).replace(".", ",");
//         }
//         let tor = "";
//         if (res.tor[x] !== undefined) {
//             tor = String(res.tor[x]).replace(".", ",");
//         }
//         csvFlu += "\n" + x.replace(".", ",") + ";" + flu + ";" + tor;
//     }

//     fs.writeFile("/tmp/flu.csv", csvFlu, (err: any) => {
//         if (err) {
//             return console.log(err);
//         }
//         console.log("The file was saved!");
//     });
// }

// function createSectionRect(prec2: number, dbg: boolean = false): cSnRectang {
//     const prms = new ParamsSectionRectang(
//         undefined, // tirant d'eau
//         2, // largeur de fond
//         40, //  Ks=Strickler
//         2.5, // Q=Débit
//         0.001, // If=pente du fond
//         prec2, // précision
//         1, // YB=hauteur de berge
//     );

//     return new cSnRectang(prms, dbg);
// }

// function computeFenetreRect(yAmont: number, yAval: number, longBief2: number, dbg: boolean = false) {
//     // let prms = new ParamsSectionRectang(
//     //     undefined, // tirant d'eau
//     //     2, // largeur de fond
//     //     40, //  Ks=Strickler
//     //     2.5, // Q=Débit
//     //     0.001, // If=pente du fond
//     //     prec, // précision
//     //     1 // YB=hauteur de berge
//     // );
//     // let sect = new cSnRectang(prms);
//     const sect = createSectionRect(prec);

//     const prem = new CourbeRemousParams(sect,
//         yAmont, // Yamont = tirant amont
//         yAval, // Yaval = tirant aval
//         longBief2,  // Long = Longueur du bief
//         dxBief,  // Dx = Pas d'espace
//         MethodeResolution.Trapezes,
//     );

//     const rem = new CourbeRemous(sect, prem, dbg);
//     return { remous: rem, res: rem.calculRemous(undefined) };
// }

// // ressaut entre x=8 et x=12.5
// const prec = 1e-3; // précision de calcul
// const longBief = 20; // longueur totale du bief
// const Yaval = 1.1; // tirant aval
// const Yamont = 0.1; // tirant amont
// const dxBief = 0.25; // pas de discrétisation

// function testFenetre() {
//     console.log("CALCUL DU BIEF\n\n");

//     // calcul du bief

//     const r = computeFenetreRect(Yamont, Yaval, longBief, false);
//     // const res: {
//     //     "flu": { [key: number]: number; },
//     //     "tor": { [key: number]: number; },
//     //     "trX": string[],
//     //     "tRes": { [key: number]: number },
//     // } = r.res;
//     const res: Result = r.res;
//     expect(res.ok).toBeTruthy("erreur de calcul de la courbe globale");

//     const remous = r.remous;

//     // ligne fluviale totale, utilisée pour les conditions initiales
//     // const ligneFluviale: { [key: number]: number; } = remous.calculFluvial();
//     const rLigneFluviale: ResultElement = remous.calculFluvial();
//     expect(rLigneFluviale.ok).toBeTruthy("erreur de calcul de la ligne fluviale");
//     const ligneFluviale: { [key: number]: number; } = rLigneFluviale.getValue("trY");

//     // ligne torrentielle totale, utilisée pour les conditions initiales
//     // const ligneTorrentielle: { [key: number]: number; } = remous.calculTorrentiel();
//     const rLigneTorrentielle: ResultElement = remous.calculTorrentiel();
//     expect(rLigneTorrentielle.ok).toBeTruthy("erreur de calcul de la ligne torrentielle");
//     const ligneTorrentielle: { [key: number]: number; } = rLigneTorrentielle.getValue("trY");

//     console.log(remous.log.toString());
//     // let ms = r["remous"].log.messages;

//     const trX = res.getValue("trX");
//     const flu = res.getValue("flu");
//     const tor = res.getValue("tor");

//     //  saveCSV(res);

//     console.log("X");
//     logObject(trX);
//     logArray("flu", flu);
//     logArray("tor", tor);

//     // mouvement de la fenêtre

//     console.log("\n\nMOUVEMENT DE LA FENETRE\n\n");

//     const sizeFenetre = Math.round(trX.length / 3.5);
//     console.log("taille de la fenetre (points) " + sizeFenetre);
//     if (sizeFenetre < 3) {
//         throw new Error("pas de discrétisation trop grand !");
//     }

//     const nFlu = Object.keys(flu).length;
//     if (nFlu < sizeFenetre * 2) {
//         throw new Error("pb de calcul de la ligne fluviale : nb de points (" + nFlu + ") < fenetre * 2");
//     }

//     const nTor = Object.keys(tor).length;
//     if (nTor < sizeFenetre * 2) {
//         throw new Error("pb de calcul de la ligne torrentielle : nb de points (" + nTor + ") < fenetre * 2");
//     }

//     const nX = trX.length;
//     console.log("taille du bief (points)=" + nX);

//     const ni = 13;
//     for (let i1 = 0; i1 <= nX - sizeFenetre; i1++) {
//         console.log();
//         console.log("i1", i1);

//         const i2 = i1 + sizeFenetre - 1;
//         const xBief1: number = Number(trX[i1]);
//         const xBief2: number = Number(trX[i2]);
//         console.log("test fenetre xBief=[" + xBief1 + "," + xBief2 + "]");
//         console.log("longueur fenetre " + (+xBief2 - +xBief1));

//         // let yAmont: number = res.tor[xBief1];
//         let yAmont: number = ligneTorrentielle[xBief1];
//         if (yAmont === undefined) {
//             // yAmont = res.flu[xBief1];
//             yAmont = ligneFluviale[xBief1];
//             // console.log("yAmont (flu)=" + yAmont);
//         }
//         // else
//         // console.log("yAmont (tor)=" + yAmont);

//         // let yAval = res.flu[xBief2];
//         let yAval = ligneFluviale[xBief2];
//         if (yAval === undefined) {
//             // yAval = res.tor[xBief2];
//             yAval = ligneTorrentielle[xBief2];
//             // console.log("yAval (tor)=" + yAval);
//         }
//         // else
//         // console.log("yAval (flu)=" + yAval   );

//         if (yAmont === undefined || yAval === undefined) {
//             throw new Error("pb ! yAmont == undefined || yAval == undefined");
//         }

//         // console.log("computeFenetre longBief=" + longBief + " y am/av=" + yAmont + " " + yAval);
//         console.log("computeFenetre y am/av=" + yAmont + " " + yAval);

//         const r2 = computeFenetreRect(yAmont, yAval, +xBief2 - +xBief1, i1 === ni);
//         const resFenetre: Result = r2.res;
//         expect(resFenetre.ok).toBeTruthy("erreur de calcul de la fenêtre");
//         let trX_fenetre = resFenetre.getValue("trX");
//         let flu_fenetre = resFenetre.getValue("flu");
//         let tor_fenetre = resFenetre.getValue("tor");

//         // validation du tableau d'abscisses

//         const validX: number[] = [];
//         for (let i = i1; i <= i2; i++) {
//             validX.push(+trX[i] - dxBief * i1);
//         }
//         if (!compareArray("X", trX_fenetre, validX)) {
//             console.log("X valid");
//             logObject(validX);
//             console.log("X fenetre");
//             logObject(trX_fenetre);
//             break;
//         }
//         /**/

//         // if (xBief1 == "3.1")
//         //     console.log();

//         // validation de la courbe fluviale

//         const validFlu: { [key: number]: number; } = {};
//         for (let i = i1; i <= i2; i++) {
//             const x: number = +trX[i];
//             const y: number = flu[x];
//             if (y !== undefined) {
//                 validFlu[(i - i1) * dxBief] = y;
//             }
//             // validFlu[x] = y;
//         }
//         if (Object.keys(validFlu).length > 0) {
//             if (!compareObject("Flu", flu_fenetre, validFlu, prec)) {
//                 console.log("flu valid");
//                 logObject(validFlu);
//                 console.log("flu fenetre");
//                 logObject(flu_fenetre);
//                 // break;
//             }
//         }
//         /**/

//         // validation de la courbe torrentielle

//         const validTor: { [key: number]: number; } = {};
//         for (let i = i1; i <= i2; i++) {
//             const x: number = +trX[i];
//             const y: number = tor[x];
//             if (y !== undefined) {
//                 validTor[(i - i1) * dxBief] = y;
//             }
//             // validTor[x] = y;
//         }
//         if (Object.keys(validTor).length > 0) {
//             if (!compareObject("Tor", tor_fenetre, validTor, prec)) {
//                 console.log("tor valid");
//                 logObject(validTor);
//                 console.log("tor fenetre");
//                 logObject(tor_fenetre);
//                 // break;
//             }
//         }
//     }
// }

// testFenetre();
