import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnTrapez } from "../../../src/open-channel/section/section_trapez";
import { ParamsSectionTrapez } from "../../../src/open-channel/section/section_trapez_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { compareExtraResult } from "../../test_func";

/*
   Certaines valeurs de ligne d'eau torrentielle étaient auparavant remplacées par une valeur fluviale
   pour la représentation graphique du ressaut (mais fausse car torrentielle).
   Idem pour la réciproque fluvial/torrentiel.
   Le code ne faisant plus ça, les valeurs de validation ont été remplacée par les bonnes.
 */

describe("Class Remous / section trapèze :", () => {
    describe("méthode trapèzes :", () => {
        xit("pente faible, fluvial hauteur normale, ", () => {
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);

            const prem = new CourbeRemousParams(100.155, 100.803, 100.005, 100,
                5,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            const t = { 0: 0.15 };
            compareExtraResult("YTorrentiel", res, "tor", t, 0.001);
            const f = { 5: 0.803 };
            compareExtraResult("Yfluvial", res, "flu", f, 0.001);
        });
        xit("pente faible, ressaut dans le bief sur plusieurs points", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            );
            SessionSettings.precision = precDist;

            const sect = new cSnTrapez(prms);

            const prem = new CourbeRemousParams(100.16, 100.4, 100.01, 100,
                10,  // Long= Longueur du bief
                1,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous(undefined);

            const f = { 9: 0.278, 10: 0.4 };
            // compareObject("Yfluvial", res.getValue("flu"), f, 0.002);
            compareExtraResult("Yfluvial", res, "flu", f, 0.002);

            // tslint:disable-next-line:max-line-length
            // let t = { 0.000: 0.15, 1.000: 0.16369914454109, 2.000: 0.17743613485223, 3.000: 0.19117312516337, 4.000: 0.20491011547451, 5.000: 0.21864710578565, 6.000: 0.23238409609679, 7.000: 0.24688425253633, 8.000: 0.26214757510426, 9.000: 0.27817406380059, 10.000: 0.4 };
            const t: any = {
                0.000: 0.15,
                1.000: 0.16369914454109,
                2.000: 0.17743613485223,
                3.000: 0.19117312516337,
                4.000: 0.20491011547451,
                5.000: 0.21864710578565,
                6.000: 0.23238409609679,
                7.000: 0.24688425253633,
                8.000: 0.26214757510426,
                9.000: 0.27817406380059,
                10.000: 0.293
            };  // dernière valeur modifiée pour la raison en tête de fichier
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const tk of Object.keys(t)) {
                t[tk] = t[tk] + rem.getCoteFond(Number(tk));
            }
            // compareObject("Ytorrentiel", res.getValue("tor"), t, 0.002);
            compareExtraResult("Ytorrentiel", res, "tor", t, 0.002);

            // const x = [0.000, 1.000, 2.000, 3.000, 4.000, 5.000, 6.000, 7.000, 8.000, 9.000, 10.000];
            // compareArray("abcisses", res.getValue("trX"), x);
        });

    });

    describe("paramètre à calculer :", () => {
        // désactivé suite au changement de format de résultat
        it("Hs (test 1)", () => {
            const prms = new ParamsSectionTrapez(
                2.5, // largeur de fond
                0.56, // fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                2, // Q=Débit
                0.001, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = 0.001;

            const sect = new cSnTrapez(prms);

            const prem = new CourbeRemousParams(100.25, 100.4, 100.1, 100,
                100,  // Long= Longueur du bief
                5,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous("Hs");

            // données de validation : version PHP (oct 2017)
            const hs: any = {
                0.000: 1.507,
                5.000: 0.735,
                10.000: 0.731,
                15.000: 0.727,
                20.000: 0.723,
                25.000: 0.719,
                30.000: 0.715,
                35.000: 0.71,
                40.000: 0.704,
                45.000: 0.699,
                50.000: 0.693,
                55.000: 0.687,
                60.000: 0.68,
                65.000: 0.673,
                70.000: 0.665,
                75.000: 0.656,
                80.000: 0.646,
                85.000: 0.634,
                90.000: 0.62,
                95.000: 0.603,
                100.000: 0.572
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const hsk of Object.keys(hs)) {
                hs[hsk] = hs[hsk] + rem.getCoteFond(Number(hsk));
            }
            // compareObject("Hs", res.getValue("Hs"), hs, 0.002);
            compareExtraResult("Hs", res, "Hs", hs, 0.002);
        });

        xit("Hs (test 2)", () => {
            // désactivé car échoue depuis les modifs apportées depuis la version PHP (calcul du ressaut hydraulique)
            const prms = new ParamsSectionTrapez(
                2.5, // largeur de fond
                0.56, // fruit
                undefined, // tirant d'eau
                40, //  Ks=Strickler
                2, // Q=Débit
                0.05, // If=pente du fond
                1, // YB=hauteur de berge
            );
            SessionSettings.precision = 0.001;

            const sect = new cSnTrapez(prms);

            const prem = new CourbeRemousParams(100.55, 101, 100.4, 100,
                8,  // Long= Longueur du bief
                0.1,  // Dx=Pas d'espace
            );

            const rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

            const res = rem.calculRemous("Hs");

            // données de validation : version PHP (oct 2017)
            // ici j'ai supprimé un point (x=2.5)
            const hs: any = {
                0.000: 1.507,
                2.600: 0.753,
                2.700: 0.757,
                2.800: 0.762,
                2.900: 0.767,
                3.000: 0.771,
                3.100: 0.776,
                3.200: 0.781,
                3.300: 0.786,
                3.400: 0.79,
                3.500: 0.795,
                3.600: 0.8,
                3.700: 0.805,
                3.800: 0.81,
                3.900: 0.814,
                4.000: 0.819,
                4.100: 0.824,
                4.200: 0.829,
                4.300: 0.834,
                4.400: 0.839,
                4.500: 0.844,
                4.600: 0.849,
                4.700: 0.854,
                4.800: 0.859,
                4.900: 0.864,
                5.000: 0.869,
                5.100: 0.874,
                5.200: 0.879,
                5.300: 0.884,
                5.400: 0.889,
                5.500: 0.894,
                5.600: 0.899,
                5.700: 0.904,
                5.800: 0.909,
                5.900: 0.914,
                6.000: 0.919,
                6.100: 0.924,
                6.200: 0.929,
                6.300: 0.934,
                6.400: 0.939,
                6.500: 0.944,
                6.600: 0.95,
                6.700: 0.955,
                6.800: 0.96,
                6.900: 0.965,
                7.000: 0.97,
                7.100: 0.975,
                7.200: 0.98,
                7.300: 0.986,
                7.400: 0.991,
                7.500: 0.996,
                7.600: 1.001,
                7.700: 1.006,
                7.800: 1.012,
                7.900: 1.017,
                8.000: 1.022
            };
            // màj pour jalhyd#146 : ajout de la cote de fond aux valeurs attendues
            for (const hsk of Object.keys(hs)) {
                hs[hsk] = hs[hsk] + rem.getCoteFond(Number(hsk));
            }
            // compareObject("Hs", res.getValue("Hs"), hs, 0.009);
            compareExtraResult("Hs", res, "Hs", hs, 0.009);
        });
    });
});
