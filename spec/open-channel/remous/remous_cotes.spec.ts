import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { MessageCode } from "../../../src/util/message";

describe("cote de l'eau par rapport à la cote du fond", () => {

    it("test 1", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.3,  // ZF1
            100,    // ZF2
            100,    // Long= Longueur du bief
            5,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        let res = cr.CalcSerie();
        expect(res.hasLog()).toBe(true);
        expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_UPSTREAM_BOTTOM_HIGHER_THAN_WATER);

        cr.prms.ZF1.singleValue = 100.1;
        cr.prms.ZF2.singleValue = 100.41;
        res = cr.CalcSerie();
        expect(res.hasLog()).toBe(true);
        expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_DOWNSTREAM_BOTTOM_HIGHER_THAN_WATER);
    });

    it("cotes amont et aval de l'eau sous le fond", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            99, // Z1
            99,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            100,    // Long= Longueur du bief
            5,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

        let res = cr.CalcSerie();
        expect(res.hasErrorMessages()).toBe(true);
    });
});

describe("cote de berge", () => {
    it("valeur normale", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            100,    // Long= Longueur du bief
            5,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);

        let res = cr.CalcSerie();
        expect(res.hasErrorMessages()).toBe(false);
    });

    it("valeur importante", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            20, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            100,    // Long= Longueur du bief
            5,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.RungeKutta4);
        let res = cr.CalcSerie();
        expect(res.hasErrorMessages()).toBe(false);
    });
});
