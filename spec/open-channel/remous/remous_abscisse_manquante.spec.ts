import { MethodeResolution } from "../../../src/open-channel/methode-resolution";
import { CourbeRemous } from "../../../src/open-channel/remous";
import { CourbeRemousParams } from "../../../src/open-channel/remous_params";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";

describe("bug jalhyd#147 − ", () => {

    it("abscisse présente (Dx=1)", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            10,     // Long= Longueur du bief
            1,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(11);
    });

    it("abscisse manquante (Dx=2.01)", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            10,     // Long= Longueur du bief
            2.01,   // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(6); // 0, 2.01, 4.02, 6.03, 8.04, 10
    });

    it("abscisse manquante (Dx=2)", () => {
        const prms = new ParamsSectionRectang(
            undefined, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            1.2, // Q=Débit
            undefined, // If=pente du fond
            1, // YB=hauteur de berge
        );
        const sect = new cSnRectang(prms);
        const prem = new CourbeRemousParams(
            100.25, // Z1
            100.4,  // Z2
            100.1,  // ZF1
            100,    // ZF2
            10,     // Long= Longueur du bief
            2,      // Dx=Pas d'espace
        );
        const cr = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

        const res = cr.CalcSerie();
        expect(res.resultElements.length).toBe(6); // 0, 2, 4, 6, 8, 10
    });
});
