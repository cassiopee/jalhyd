import { Pente } from "../../src/open-channel/pente";
import { PenteParams } from "../../src/open-channel/pente_params";
import { checkResult } from "../test_func";

function penteTest(varTest: string, expected: number) {
    describe("Calc(): ", () => {
        it(varTest + " should be " + expected, () => {
            const prms = new PenteParams(
                2,      // Cote amont Z1
                0.5,    // Cote aval Z2
                10,     // Longueur L
                0.15    // Pente I
            );

            const nub = new Pente(prms);
            nub.getParameter(varTest).v = undefined;

            checkResult(nub.Calc(varTest, 0), expected);
        });
    });
}

describe("Class Pente: ", () => {

    penteTest("Z1", 2);
    penteTest("Z2", 0.5);
    penteTest("L", 10);
    penteTest("I", 0.15);

});
