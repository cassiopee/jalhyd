import { cSnTrapez } from "../../../src/open-channel/section/section_trapez";
import { ParamsSectionTrapez } from "../../../src/open-channel/section/section_trapez_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramCnl: ParamsSectionTrapez;
let sect: cSnTrapez;

describe("Section paramétrée trapèze :", () => {
    beforeEach(() => {
        paramCnl = new ParamsSectionTrapez(2.5, // largeur de fond
            0.56, // fruit
            0.8, // tirant d'eau
            40, //  Ks=Strickler
            10,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        SessionSettings.precision = precDist;

        sect = new cSnTrapez(paramCnl);
    });

    describe("torrentiel :", () => {
        // charge spécifique
        it("Hs should equal to 1.716", () => {
            checkResult(sect.CalcSection("Hs"), 1.716);
        });

        // charge critique
        it("Hsc should equal to 1.534", () => {
            checkResult(sect.CalcSection("Hsc"), 1.534);
        });

        // largeur au miroir
        it("B should equal to 3.396", () => {
            checkResult(sect.CalcSection("B"), 3.396);
        });

        // périmètre mouillé
        it("P should equal to 4.334", () => {
            checkResult(sect.CalcSection("P"), 4.334);
        });

        // surface mouillée
        it("S should equal to 2.358", () => {
            checkResult(sect.CalcSection("S"), 2.358);
        });

        // rayon hydraulique
        it("R should equal to 0.544", () => {
            checkResult(sect.CalcSection("R"), 0.544);
        });

        // vitesse moyenne
        it("V should equal to 4.24", () => {
            checkResult(sect.CalcSection("V"), 4.24);
        });

        // nombre de Froude
        it("Fr should equal to 1.625", () => {
            checkResult(sect.CalcSection("Fr"), 1.625);
        });

        // tirant d'eau critique
        it("Yc should equal to 1.074", () => {
            checkResult(sect.CalcSection("Yc"), 1.074);
        });

        // tirant d'eau normal
        it("Yn should equal to 2.275", () => {
            checkResult(sect.CalcSection("Yn"), 2.275);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 1.502", () => {
            checkResult(sect.CalcSection("Ycor"), 1.502);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 1.398", () => {
            checkResult(sect.CalcSection("Ycon"), 1.398);
        });

        // perte de charge
        it("J should equal to 0.025", () => {
            // paramCnl.v.Prec = 0.00001;
            checkResult(sect.CalcSection("J"), 0.025);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to -0.024", () => {
            checkResult(sect.CalcSection("I-J"), -0.024);
        });

        // impulsion hydraulique
        it("Imp should equal to 51187.203", () => {
            checkResult(sect.CalcSection("Imp"), 51187.203);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 135.020", () => {
            checkResult(sect.CalcSection("Tau0"), 135.020);
        });
    });
});
