import { cSnCirc } from "../../../src/open-channel/section/section_circulaire";
import { ParamsSectionCirc } from "../../../src/open-channel/section/section_circulaire_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let sect: cSnCirc;

const resultSectCirc: { [name: string]: number } = {
    Hs: 0.853, Hsc: 0.694, B: 1.959, P: 2.738, S: 1.173, R: 0.428, V: 1.023,
    Fr: 0.422, Yc: 0.512, Yn: 0.976, Ycor: 0.361, Ycon: 0.307, J: 0.002,
    'I-J': -0.00102, Imp: 5076.304, Tau0: 8.505
}

describe("Section paramétrée circulaire ouverte: ", () => {
    beforeEach(() => {
        SessionSettings.precision = precDist;
    });

    describe("fluvial / pas de débordement :", () => {
        beforeEach(() => {
            sect = new cSnCirc(
                new ParamsSectionCirc(
                    2, // diamètre
                    0.8, // tirant d'eau
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    1, // YB= hauteur de berge
                )
            );
        });

        for (const v in resultSectCirc) {
            if (resultSectCirc.hasOwnProperty(v))
                it(`${v} should equal to ${resultSectCirc[v]}`, () => {
                    checkResult(sect.CalcSection(v), resultSectCirc[v]);
                });
        }
    });

    describe("fluvial / debordement :", () => {

        beforeEach(() => {
            sect = new cSnCirc(
                new ParamsSectionCirc(
                    2, // diamètre
                    2, // tirant d'eau
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    1, // YB= hauteur de berge
                )
            );
        });

        const resultSectCircDeb: { [name: string]: number } = {
            Hs: 2.006, Hsc: 0.694, B: 2, P: Math.PI + 2, S: Math.PI / 2 + 2, R: 0.694, V: 0.336,
            Fr: 0.08, Yc: 0.512, Yn: 0.976, Ycor: 0.232, Ycon: 0.24, J: 0.0001,
            'I-J': 0.001, Imp: 6943.271, Tau0: 0.782
        }

        for (const v in resultSectCircDeb) {
            if (resultSectCircDeb.hasOwnProperty(v))
                it(`${v} should equal to ${resultSectCircDeb[v]}`, () => {
                    checkResult(sect.CalcSection(v), resultSectCircDeb[v]);
                });
        }

    });
});

describe("Section paramétrée circulaire fermée: ", () => {
    beforeEach(() => {
        SessionSettings.precision = precDist;
    });

    describe("fluvial / pas de débordement :", () => {
        beforeEach(() => {
            sect = new cSnCirc(
                new ParamsSectionCirc(
                    2, // diamètre
                    0.8, // tirant d'eau
                    40, //  Ks=Strickler
                    1.2,  //  Q=Débit
                    0.001, //  If=pente du fond
                    2, // YB= hauteur de berge
                )
            );
        });

        for (const v in resultSectCirc) {
            if (resultSectCirc.hasOwnProperty(v))
                it(`${v} should equal to ${resultSectCirc[v]}`, () => {
                    checkResult(sect.CalcSection(v), resultSectCirc[v]);
                });
        }
    });
});
