import { cSnTrapez } from "../../../src/open-channel/section/section_trapez";
import { ParamsSectionTrapez } from "../../../src/open-channel/section/section_trapez_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionTrapez;
let sect: cSnTrapez;

function createSection(prec: number): cSnTrapez {
    paramSection = new ParamsSectionTrapez(2.5, // largeur de fond
        0.56, // fruit
        0.8, // tirant d'eau
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = precDist;

    return new cSnTrapez(paramSection);
}

function createSectionDebordement(prec: number): cSnTrapez {
    paramSection = new ParamsSectionTrapez(2.5, // largeur de fond
        0.56, // fruit
        2, // tirant d'eau
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = precDist;

    return new cSnTrapez(paramSection);
}

describe("Section paramétrée trapèze : ", () => {
    beforeEach(() => {
        sect = createSection(precDist);
    });

    describe("fluvial / pas de débordement :", () => {
        // charge spécifique
        it("Hs should equal to 0.813", () => {
            checkResult(sect.CalcSection("Hs"), 0.813);
        });

        // charge critique
        it("Hsc should equal to 0.413", () => {
            checkResult(sect.CalcSection("Hsc"), 0.413);
        });

        // largeur au miroir
        it("B should equal to 3.396", () => {
            checkResult(sect.CalcSection("B"), 3.396);
        });

        // périmètre mouillé
        it("P should equal to 4.334", () => {
            checkResult(sect.CalcSection("P"), 4.334);
        });

        // surface mouillée
        it("S should equal to 2.358", () => {
            checkResult(sect.CalcSection("S"), 2.358);
        });

        // rayon hydraulique
        it("R should equal to 0.544", () => {
            checkResult(sect.CalcSection("R"), 0.544);
        });

        // vitesse moyenne
        it("V should equal to 0.509", () => {
            checkResult(sect.CalcSection("V"), 0.509);
        });

        // nombre de Froude
        it("Fr should equal to 0.195", () => {
            checkResult(sect.CalcSection("Fr"), 0.195);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.28", () => {
            checkResult(sect.CalcSection("Yc"), 0.28);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.587", () => {
            checkResult(sect.CalcSection("Yn"), 0.587);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.127", () => {
            checkResult(sect.CalcSection("Ycor"), 0.127);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.061", () => {
            checkResult(sect.CalcSection("Ycon"), 0.061);
        });

        // perte de charge
        it("J should equal to 0.00036", () => {
            const sect2 = createSection(0.00001);
            checkResult(sect2.CalcSection("J"), 0.00036);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.001", () => {
            checkResult(sect.CalcSection("I-J"), 0.001);
        });

        // impulsion hydraulique
        it("Imp should equal to 9396.158", () => {
            checkResult(sect.CalcSection("Imp"), 9396.158);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 1.944", () => {
            checkResult(sect.CalcSection("Tau0"), 1.944);
        });
    });
});

describe("Section paramétrée trapèze : ", () => {
    beforeEach(() => {
        sect = createSectionDebordement(precDist);
    });

    describe("fluvial / débordement :", () => {
        // charge spécifique
        it("Hs should equal to 2.002", () => {
            checkResult(sect.CalcSection("Hs"), 2.002);
        });

        // charge critique
        it("Hsc should equal to 0.413", () => {
            checkResult(sect.CalcSection("Hsc"), 0.413);
        });

        // largeur au miroir
        it("B should equal to 3.62", () => {
            checkResult(sect.CalcSection("B"), 3.62);
        });

        // périmètre mouillé
        it("P should equal to 6.792", () => {
            checkResult(sect.CalcSection("P"), 6.792);
        });

        // surface mouillée
        it("S should equal to 6.68", () => {
            checkResult(sect.CalcSection("S"), 6.68);
        });

        // rayon hydraulique
        it("R should equal to 0.983", () => {
            checkResult(sect.CalcSection("R"), 0.983);
        });

        // vitesse moyenne
        it("V should equal to 0.18", () => {
            checkResult(sect.CalcSection("V"), 0.18);
        });

        // nombre de Froude
        it("Fr should equal to 0.042", () => {
            checkResult(sect.CalcSection("Fr"), 0.042);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.28", () => {
            checkResult(sect.CalcSection("Yc"), 0.28);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.587", () => {
            checkResult(sect.CalcSection("Yn"), 0.587);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.077", () => {
            checkResult(sect.CalcSection("Ycor"), 0.077);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.009", () => {
            checkResult(sect.CalcSection("Ycon"), 0.009);
        });

        // perte de charge
        it("J should equal to 0.00002", () => {
            const sect2 = createSection(0.00001);
            checkResult(sect2.CalcSection("J"), 0.00002);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.001", () => {
            checkResult(sect.CalcSection("I-J"), 0.001);
        });

        // impulsion hydraulique
        it("Imp should equal to 63915.169", () => {
            checkResult(sect.CalcSection("Imp"), 63915.169);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 0.199", () => {
            checkResult(sect.CalcSection("Tau0"), 0.199);
        });
    });
    describe("#301 non convergence du calcul du tirant d'eau critique", () => {
        it("Yc should be successfully calculated", () => {
            paramSection = new ParamsSectionTrapez(
                20, // largeur de fond
                2, // fruit
                1, // tirant d'eau
                40, //  Ks=Strickler
                20,  //  Q=Débit
                0.001, //  If=pente du fond
                3, // YB= hauteur de berge
            );
            SessionSettings.precision = 1E-7;
            sect = new cSnTrapez(paramSection);
            expect(sect.CalcSection("Yc").hasErrorMessages()).toBe(false);
            SessionSettings.precision = precDist;
            checkResult(sect.CalcSection("Yc"), 0.4599); // Ref from Canal9
        });
    });
});
