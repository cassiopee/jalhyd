import { cSnCirc } from "../../../src/open-channel/section/section_circulaire";
import { ParamsSectionCirc } from "../../../src/open-channel/section/section_circulaire_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionCirc;
let sect: cSnCirc;

function createSection(prec: number): cSnCirc {
    paramSection = new ParamsSectionCirc(2, // diamètre
        0.8, // tirant d'eau
        40, //  Ks=Strickler
        10,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnCirc(paramSection);
}

describe("Section paramétrée circulaire : ", () => {
    beforeEach(() => {
        sect = createSection(precDist);
    });

    describe("torrentiel :", () => {
        // charge spécifique
        it("Hs should equal to 4.501", () => {
            checkResult(sect.CalcSection("Hs"), 4.501);
        });

        // charge critique
        it("Hsc should equal to 2.263", () => {
            checkResult(sect.CalcSection("Hsc"), 2.263);
        });

        // largeur au miroir
        it("B should equal to 1.960", () => {
            checkResult(sect.CalcSection("B"), 1.960);
        });

        // périmètre mouillé
        it("P should equal to 2.739", () => {
            checkResult(sect.CalcSection("P"), 2.739);
        });

        // surface mouillée
        it("S should equal to 1.173", () => {
            checkResult(sect.CalcSection("S"), 1.173);
        });

        // rayon hydraulique
        it("R should equal to 0.428", () => {
            checkResult(sect.CalcSection("R"), 0.428);
        });

        // vitesse moyenne
        it("V should equal to 8.522", () => {
            checkResult(sect.CalcSection("V"), 8.522);
        });

        // nombre de Froude
        it("Fr should equal to 3.516", () => {
            checkResult(sect.CalcSection("Fr"), 3.516);
        });

        // tirant d'eau critique
        it("Yc should equal to 1.581", () => {
            checkResult(sect.CalcSection("Yc"), 1.581);
        });

        // tirant d'eau normal
        it("Yn should equal to 4.624", () => {
            checkResult(sect.CalcSection("Yn"), 4.624);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 4.43", () => {
            checkResult(sect.CalcSection("Ycor"), 4.43);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.8", () => {
            checkResult(sect.CalcSection("Ycon"), 0.8);
        });

        // perte de charge
        it("J should equal to 0.141", () => {
            // sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.141);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to -0.14", () => {
            // sect = createSection(0.00001);
            checkResult(sect.CalcSection("I-J"), -0.14);
        });

        // impulsion hydraulique
        it("Imp should equal to 89065.861", () => {
            checkResult(sect.CalcSection("Imp"), 89065.861);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 590.605", () => {
            checkResult(sect.CalcSection("Tau0"), 590.605);
        });
    });
});
