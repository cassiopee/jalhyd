import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionRectang;
let sect: cSnRectang;

describe("Section paramétrée rectangulaire : ", () => {
    beforeEach(() => {
        paramSection = new ParamsSectionRectang(0.8, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            10,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        SessionSettings.precision = precDist;

        sect = new cSnRectang(paramSection);
    });

    describe("torrentiel :", () => {
        // charge spécifique
        it("Hs should equal to 2.074", () => {
            checkResult(sect.CalcSection("Hs"), 2.074);
        });

        // charge critique
        it("Hsc should equal to 1.766", () => {
            checkResult(sect.CalcSection("Hsc"), 1.766);
        });

        // largeur au miroir
        it("B should equal to 2.5", () => {
            checkResult(sect.CalcSection("B"), 2.5);
        });

        // périmètre mouillé
        it("P should equal to 4.1", () => {
            checkResult(sect.CalcSection("P"), 4.1);
        });

        // surface mouillée
        it("S should equal to 2", () => {
            checkResult(sect.CalcSection("S"), 2);
        });

        // rayon hydraulique
        it("R should equal to 0.488", () => {
            checkResult(sect.CalcSection("R"), 0.488);
        });

        // vitesse moyenne
        it("V should equal to 5", () => {
            checkResult(sect.CalcSection("V"), 5);
        });

        // nombre de Froude
        it("Fr should equal to 1.785", () => {
            checkResult(sect.CalcSection("Fr"), 1.785);
        });

        // tirant d'eau critique
        it("Yc should equal to 1.177", () => {
            checkResult(sect.CalcSection("Yc"), 1.177);
        });

        // tirant d'eau normal
        it("Yn should equal to 3.364", () => {
            checkResult(sect.CalcSection("Yn"), 3.364);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 1.831", () => {
            checkResult(sect.CalcSection("Ycor"), 1.831);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 1.659", () => {
            checkResult(sect.CalcSection("Ycon"), 1.659);
        });

        // perte de charge
        it("J should equal to 0.041", () => {
            // paramCnl.v.Prec = 0.00001;
            checkResult(sect.CalcSection("J"), 0.041);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to -0.04", () => {
            // paramCnl.v.Prec = 0.00001;
            checkResult(sect.CalcSection("I-J"), -0.04);
        });

        // impulsion hydraulique
        it("Imp should equal to 57848", () => {
            checkResult(sect.CalcSection("Imp"), 57848);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 194.718", () => {
            checkResult(sect.CalcSection("Tau0"), 194.718);
        });
    });
});
