import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionRectang;
let sect: cSnRectang;

function createSection(prec: number): cSnRectang {
    paramSection = new ParamsSectionRectang(0.8, // tirant d'eau
        2.5, // largeur de fond
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnRectang(paramSection);
}

function createSectionDebordement(prec: number): cSnRectang {
    paramSection = new ParamsSectionRectang(2, // tirant d'eau
        2.5, // largeur de fond
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnRectang(paramSection);
}

describe("Section paramétrée rectangulaire : ", () => {
    beforeEach(() => {
        sect = createSection(precDist);
    });

    describe("fluvial / pas de débordement :", () => {
        // charge spécifique
        it("Hs should equal to 0.818", () => {
            checkResult(sect.CalcSection("Hs"), 0.818);
        });

        // charge critique
        it("Hsc should equal to 0.43", () => {
            checkResult(sect.CalcSection("Hsc"), 0.43);
        });

        // largeur au miroir
        it("B should equal to 2.5", () => {
            checkResult(sect.CalcSection("B"), 2.5);
        });

        // périmètre mouillé
        it("P should equal to 4.1", () => {
            checkResult(sect.CalcSection("P"), 4.1);
        });

        // surface mouillée
        it("S should equal to 2", () => {
            checkResult(sect.CalcSection("S"), 2);
        });

        // rayon hydraulique
        it("R should equal to 0.488", () => {
            checkResult(sect.CalcSection("R"), 0.488);
        });

        // vitesse moyenne
        it("V should equal to 0.6", () => {
            checkResult(sect.CalcSection("V"), 0.6);
        });

        // nombre de Froude
        it("Fr should equal to 0.214", () => {
            checkResult(sect.CalcSection("Fr"), 0.214);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.286", () => {
            checkResult(sect.CalcSection("Yc"), 0.286);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.663", () => {
            checkResult(sect.CalcSection("Yn"), 0.663);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.131", () => {
            checkResult(sect.CalcSection("Ycor"), 0.131);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.068", () => {
            checkResult(sect.CalcSection("Ycon"), 0.068);
        });

        // perte de charge
        it("J should equal to 0.00059", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.00059);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.00041", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("I-J"), 0.00041);
        });

        // impulsion hydraulique
        it("Imp should equal to 8568", () => {
            checkResult(sect.CalcSection("Imp"), 8568);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 2.804", () => {
            checkResult(sect.CalcSection("Tau0"), 2.804);
        });
    });
});

describe("Section paramétrée rectangulaire : ", () => {
    beforeEach(() => {
        sect = createSectionDebordement(precDist);
    });

    describe("fluvial / débordement :", () => {
        // charge spécifique
        it("Hs should equal to 2.003", () => {
            checkResult(sect.CalcSection("Hs"), 2.003);
        });

        // charge critique
        it("Hsc should equal to 0.43", () => {
            checkResult(sect.CalcSection("Hsc"), 0.43);
        });

        // largeur au miroir
        it("B should equal to 2.5", () => {
            checkResult(sect.CalcSection("B"), 2.5);
        });

        // périmètre mouillé
        it("P should equal to 6.5", () => {
            checkResult(sect.CalcSection("P"), 6.5);
        });

        // surface mouillée
        it("S should equal to 5", () => {
            checkResult(sect.CalcSection("S"), 5);
        });

        // rayon hydraulique
        it("R should equal to 0.769", () => {
            checkResult(sect.CalcSection("R"), 0.769);
        });

        // vitesse moyenne
        it("V should equal to 0.24", () => {
            checkResult(sect.CalcSection("V"), 0.24);
        });

        // nombre de Froude
        it("Fr should equal to 0.0542", () => {
            checkResult(sect.CalcSection("Fr"), 0.0542);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.286", () => {
            checkResult(sect.CalcSection("Yc"), 0.286);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.663", () => {
            checkResult(sect.CalcSection("Yn"), 0.663);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.078", () => {
            checkResult(sect.CalcSection("Ycor"), 0.078);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.012", () => {
            checkResult(sect.CalcSection("Ycon"), 0.012);
        });

        // perte de charge
        it("J should equal to 0.00059", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.00059);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.0009", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("I-J"), 0.0009);
        });

        // impulsion hydraulique
        it("Imp should equal to 49338", () => {
            checkResult(sect.CalcSection("Imp"), 49338);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 0.385", () => {
            checkResult(sect.CalcSection("Tau0"), 0.385);
        });
    });
});
