import { SectionParametree } from "../../../src/open-channel/section/section_parametree";
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { ParamDefinition } from "../../../src/param/param-definition";
import { MessageCode } from "../../../src/util/message";
import { precDist } from "../../test_config";

function createSection(prec: number) {
    return new SectionParametree(
        new cSnRectang(
            new ParamsSectionRectang(
                0.8, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                1.2,  //  Q=Débit
                0.001, //  If=pente du fond
                1, // YB= hauteur de berge
            )
        )
    );
}

function testVarier(nub: SectionParametree, p: ParamDefinition) {
    // variation de p
    // vérifier que le calcul se déroule correctement et que les résultats complémentaires sont tous calculés
    it("en variant " + p.symbol + ", tous les résultats complémentaires devraient être présents", () => {
        // console.log(">>>>>>>>>>>>>>>> variating", p.symbol);
        p.setValues(1, 5, 0.5);
        nub.CalcSerie();
        expect(nub.result).toBeDefined();
        expect(nub.result.resultElements.length).toBe(9);

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < nub.result.resultElements.length; i++) {
            expect(Object.keys(nub.result.resultElements[i].values).length).toBe(16);
        }
    });
}

describe("Section paramétrée rectangulaire, paramètres fixés : ", () => {

    it("tous les résultats complémentaires devraient être présents", () => {
        const nub = createSection(precDist);
        nub.CalcSerie();
        expect(nub.result).toBeDefined();
        expect(Object.keys(nub.result.resultElement.values).length).toBe(16);
    });

    it("Section paramétrée rectangulaire, débordement : ", () => {
        const nub = createSection(precDist);
        nub.section.prms.YB.singleValue = 0.7;
        nub.CalcSerie();
        expect(nub.section.prms.YB.singleValue).toBe(0.7);
        expect(nub.section.prms.Y.singleValue).toBe(0.8);
        expect(nub.result.log.messages.length).toBe(1);
        expect(nub.result.log.messages[0].code).toBe(MessageCode.WARNING_SECTION_OVERFLOW);
    });
});

describe("Section paramétrée rectangulaire, paramètres variés : ", () => {

    const nub = createSection(precDist);
    for (const p of nub.parameterIterator) {
        testVarier(nub, p);
    }
});

describe("Section paramétrée, modification d'un paramètre : ", () => {
    it("vérifier que les résultats changent", () => {
        const nub = createSection(precDist);
        // set param
        nub.section.prms.Q.singleValue = 1.2;
        nub.CalcSerie();
        expect(nub.result).toBeDefined();
        const oldValues = JSON.parse(JSON.stringify(nub.result.values));
        // modify param
        nub.section.prms.Q.singleValue = 4.3;
        nub.CalcSerie();
        expect(nub.result).toBeDefined();
        const newValues = JSON.parse(JSON.stringify(nub.result.values));
        // check that results are different
        expect(newValues).not.toEqual(oldValues);
    });
});
