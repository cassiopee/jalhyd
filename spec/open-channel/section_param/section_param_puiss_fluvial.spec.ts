import { cSnPuiss } from "../../../src/open-channel/section/section_puissance";
import { ParamsSectionPuiss } from "../../../src/open-channel/section/section_puissance_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionPuiss;
let sect: cSnPuiss;

function createSection(prec: number): cSnPuiss {
    paramSection = new ParamsSectionPuiss(0.5, // coefficient
        0.8, // tirant d'eau
        4, // largeur de berge
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnPuiss(paramSection);
}

function createSectionDebordement(prec: number): cSnPuiss {
    paramSection = new ParamsSectionPuiss(0.5, // coefficient
        2, // tirant d'eau
        4, // largeur de berge
        40, //  Ks=Strickler
        1.2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnPuiss(paramSection);
}

describe("Section paramétrée puissance :", () => {
    beforeEach(() => {
        sect = createSection(precDist);
    });

    describe("fluvial / pas de débordement :", () => {
        // charge spécifique
        it("Hs should equal to 0.82", () => {
            checkResult(sect.CalcSection("Hs"), 0.82);
        });

        // charge critique
        it("Hsc should equal to 0.559", () => {
            checkResult(sect.CalcSection("Hsc"), 0.559);
        });

        // largeur au miroir
        it("B should equal to 3.578", () => {
            checkResult(sect.CalcSection("B"), 3.578);
        });

        // périmètre mouillé
        it("P should equal to 4.223", () => {
            checkResult(sect.CalcSection("P"), 4.223);
        });

        // surface mouillée
        it("S should equal to 1.908", () => {
            checkResult(sect.CalcSection("S"), 1.908);
        });

        // rayon hydraulique
        it("R should equal to 0.452", () => {
            checkResult(sect.CalcSection("R"), 0.452);
        });

        // vitesse moyenne
        it("V should equal to 0.629", () => {
            checkResult(sect.CalcSection("V"), 0.629);
        });

        // nombre de Froude
        it("Fr should equal to 0.275", () => {
            checkResult(sect.CalcSection("Fr"), 0.275);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.419", () => {
            checkResult(sect.CalcSection("Yc"), 0.419);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.742", () => {
            checkResult(sect.CalcSection("Yn"), 0.742);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.265", () => {
            checkResult(sect.CalcSection("Ycor"), 0.265);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.189", () => {
            checkResult(sect.CalcSection("Ycon"), 0.189);
        });

        // perte de charge
        it("J should equal to 0.0007", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.0007);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.000335", () => {
            sect = createSection(0.000001);
            checkResult(sect.CalcSection("I-J"), 0.000335);
        });

        // impulsion hydraulique
        it("Imp should equal to 6744.616", () => {
            checkResult(sect.CalcSection("Imp"), 6744.616);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 3.16", () => {
            checkResult(sect.CalcSection("Tau0"), 3.16);
        });
    });
});

describe("Section paramétrée puissance :", () => {
    beforeEach(() => {
        sect = createSectionDebordement(precDist);
    });

    describe("fluvial / débordement :", () => {
        // charge spécifique
        it("Hs should equal to 2.001", () => {
            checkResult(sect.CalcSection("Hs"), 2.001);
        });

        // charge critique
        it("Hsc should equal to 0.559", () => {
            checkResult(sect.CalcSection("Hsc"), 0.559);
        });

        // largeur au miroir
        it("B should equal to 4", () => {
            checkResult(sect.CalcSection("B"), 4);
        });

        // périmètre mouillé
        it("P should equal to 6.098", () => {
            checkResult(sect.CalcSection("P"), 6.098);
        });

        // surface mouillée
        it("S should equal to 7.542", () => {
            checkResult(sect.CalcSection("S"), 7.542);
        });

        // rayon hydraulique
        it("R should equal to 1.237", () => {
            checkResult(sect.CalcSection("R"), 1.237);
        });

        // vitesse moyenne
        it("V should equal to 0.159", () => {
            checkResult(sect.CalcSection("V"), 0.159);
        });

        // nombre de Froude
        it("Fr should equal to 0.037", () => {
            checkResult(sect.CalcSection("Fr"), 0.037);
        });

        // tirant d'eau critique
        it("Yc should equal to 0.419", () => {
            checkResult(sect.CalcSection("Yc"), 0.419);
        });

        // tirant d'eau normal
        it("Yn should equal to 0.742", () => {
            checkResult(sect.CalcSection("Yn"), 0.742);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 0.178", () => {
            checkResult(sect.CalcSection("Ycor"), 0.178);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 0.044", () => {
            checkResult(sect.CalcSection("Ycon"), 0.044);
        });

        // perte de charge
        it("J should equal to 0.00059", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.00059);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to 0.00041", () => {
            sect = createSection(0.00001);
            checkResult(sect.CalcSection("I-J"), 0.00041);
        });

        // impulsion hydraulique
        it("Imp should equal to 59384.242", () => {
            checkResult(sect.CalcSection("Imp"), 59384.242);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 0.145", () => {
            checkResult(sect.CalcSection("Tau0"), 0.145);
        });
    });
});
