// tslint:disable:no-console
import { cSnRectang } from "../../../src/open-channel/section/section_rectang";
import { ParamsSectionRectang } from "../../../src/open-channel/section/section_rectang_params";
import { SessionSettings } from "../../../src/session_settings";
import { MessageCode } from "../../../src/util/message";
import { Result } from "../../../src/util/result";

let paramSection: ParamsSectionRectang;
let sect: cSnRectang;
let oldMaxIter: number;

describe("Section paramétrée rectangulaire : ", () => {

    beforeAll(() => {
        oldMaxIter = SessionSettings.maxIterations;
        SessionSettings.maxIterations = 2;
    });

    afterAll(() => {
        SessionSettings.maxIterations = oldMaxIter;
    });

    beforeEach(() => {
        paramSection = new ParamsSectionRectang(0.8, // tirant d'eau
            2.5, // largeur de fond
            40, //  Ks=Strickler
            10,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        SessionSettings.precision = 1e-10; // précision

        sect = new cSnRectang(paramSection);
    });

    describe("non convergence de la méthode de Newton :", () => {

        it("hauteur normale", () => {
            const r: Result = sect.CalcSection("Yn");
            expect(r.ok).toBeFalsy("le calcul devrait avoir échoué");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE);
        });

        it("hauteur normale, pente nulle", () => {
            sect.prms.If.v = 0;
            const r: Result = sect.CalcSection("Yn");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF);
        });

        it("hauteur normale, pente négative", () => {
            sect.prms.If.v = -0.001;
            const r: Result = sect.CalcSection("Yn");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF);
        });

        it("hauteur fluviale, Y < Yc", () => {
            const r: Result = sect.CalcSection("Ycor");
            // expect(r.log.messages.length).toEqual(3);
            expect(r.ok).toBeFalsy("le calcul devrait avoir échoué");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });

        it("hauteur fluviale, Y > Yc", () => {
            sect.prms.Y.v = 2;
            const r: Result = sect.CalcSection("Ycor");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });

        it("hauteur torrentielle, Y < Yc", () => {
            const r: Result = sect.CalcSection("Ycor");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });

        it("hauteur torrentielle, Y > Yc", () => {
            sect.prms.Y.v = 2;
            const r: Result = sect.CalcSection("Ycor");
            //            expect(r.log.messages.length).toEqual(2);
            expect(r.ok).toBeFalsy("le calcul devrait avoir échoué");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });

        it("hauteur conjuguée, Froude < 1", () => {
            sect.prms.Y.v = 2;
            const r: Result = sect.CalcSection("Ycon");
            // console.log(r.log.toString());
            // expect(r.log.messages.length).toEqual(3);
            expect(r.ok).toBeFalsy("le calcul devrait avoir échoué");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });

        it("hauteur conjuguée, Froude > 1", () => {
            const r: Result = sect.CalcSection("Ycon");
            // expect(r.log.messages.length).toEqual(4);
            expect(r.ok).toBeFalsy("le calcul devrait avoir échoué");
            expect(r.log.messages.length).toEqual(1);
            expect(r.log.messages[0].code).toEqual(MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
        });
    });
});
