import {
    CalculatorType,
    CourbeRemous,
    cSnCirc,
    cSnPuiss,
    cSnTrapez,
    ParamsSectionCirc,
    ParamsSectionPuiss,
    ParamsSectionTrapez,
    Props,
    SectionParametree,
    Session
} from "../../../src/index";
import { RegimeUniforme } from "../../../src/open-channel/regime_uniforme";

describe("section nubs and nodeTye property", () => {

    beforeEach(() => {
        Session.getInstance().clear();
    });

    it("Section paramétrée", () => {
        const sp = Session.getInstance().createSessionNub(
            new Props({ calcType: CalculatorType.SectionParametree })
        ) as SectionParametree;
        sp.setSection(new cSnTrapez(
            new ParamsSectionTrapez(2.6, 0.55, 1.1, 42, 1.3, 0.002, 0.9)
        ));
        const data = Session.getInstance().serialise();
        // console.log(data);
        // stringified Nub should contain only 1 occurrence of "nodeType"
        const ntOcc = (data.match(/"nodeType"/g) || []).length;
        expect(ntOcc).toBe(1);
    });

    it("Courbe de remous", () => {
        const sp = Session.getInstance().createSessionNub(
            new Props({ calcType: CalculatorType.CourbeRemous })
        ) as CourbeRemous;
        sp.setSection(new cSnCirc(
            new ParamsSectionCirc(2.6, 0.55, 42, 1.3, 0.002, 1.1)
        ));
        const data = Session.getInstance().serialise();
        // console.log(data);
        // stringified Nub should contain only 1 occurrence of "nodeType"
        const ntOcc = (data.match(/"nodeType"/g) || []).length;
        expect(ntOcc).toBe(1);
    });

    it("Régime uniforme", () => {
        const sp = Session.getInstance().createSessionNub(
            new Props({ calcType: CalculatorType.RegimeUniforme })
        ) as RegimeUniforme;
        sp.setSection(new cSnPuiss(
            new ParamsSectionPuiss(0.35, 2.6, 0.55, 1.1, 42, 1.3, 0.002)
        ));
        const data = Session.getInstance().serialise();
        // console.log(data);
        // stringified Nub should contain only 1 occurrence of "nodeType"
        const ntOcc = (data.match(/"nodeType"/g) || []).length;
        expect(ntOcc).toBe(1);
    });

});
