import { cSnPuiss } from "../../../src/open-channel/section/section_puissance";
import { ParamsSectionPuiss } from "../../../src/open-channel/section/section_puissance_params";
import { SessionSettings } from "../../../src/session_settings";
import { precDist } from "../../test_config";
import { checkResult } from "../../test_func";

let paramSection: ParamsSectionPuiss;
let sect: cSnPuiss;

function createSection(prec: number): cSnPuiss {
    paramSection = new ParamsSectionPuiss(0.5, // coefficient
        0.8, // tirant d'eau
        4, // largeur de berge
        40, //  Ks=Strickler
        10,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    SessionSettings.precision = prec;

    return new cSnPuiss(paramSection);
}

describe("Section paramétrée puissance :", () => {
    beforeEach(() => {
        sect = createSection(precDist);
    });

    describe("torrentiel / pas de débordement :", () => {
        // charge spécifique
        it("Hs should equal to 2.2", () => {
            checkResult(sect.CalcSection("Hs"), 2.2);
        });

        // charge critique
        it("Hsc should equal to 1.616", () => {
            checkResult(sect.CalcSection("Hsc"), 1.616);
        });

        // largeur au miroir
        it("B should equal to 3.578", () => {
            checkResult(sect.CalcSection("B"), 3.578);
        });

        // périmètre mouillé
        it("P should equal to 4.223", () => {
            checkResult(sect.CalcSection("P"), 4.223);
        });

        // surface mouillée
        it("S should equal to 1.908", () => {
            checkResult(sect.CalcSection("S"), 1.908);
        });

        // rayon hydraulique
        it("R should equal to 0.452", () => {
            checkResult(sect.CalcSection("R"), 0.452);
        });

        // vitesse moyenne
        it("V should equal to 5.241", () => {
            checkResult(sect.CalcSection("V"), 5.241);
        });

        // nombre de Froude
        it("Fr should equal to 2.291", () => {
            checkResult(sect.CalcSection("Fr"), 2.291);
        });

        // tirant d'eau critique
        it("Yc should equal to 1.186", () => {
            checkResult(sect.CalcSection("Yc"), 1.186);
        });

        // tirant d'eau normal
        it("Yn should equal to 1.916", () => {
            checkResult(sect.CalcSection("Yn"), 1.916);
        });

        // tirant d'eau correspondant
        it("Ycor should equal to 2.126", () => {
            checkResult(sect.CalcSection("Ycor"), 2.126);
        });

        // tirant d'eau conjugué
        it("Ycon should equal to 1.746", () => {
            checkResult(sect.CalcSection("Ycon"), 1.746);
        });

        // perte de charge
        it("J should equal to 0.05", () => {
            // sect = createSection(0.00001);
            checkResult(sect.CalcSection("J"), 0.05);
        });

        // Variation linéaire de l'énergie spécifique
        it("I-J should equal to -0.049", () => {
            // sect = createSection(0.00001);
            checkResult(sect.CalcSection("I-J"), -0.049);
        });

        // impulsion hydraulique
        it("Imp should equal to 58397.786", () => {
            checkResult(sect.CalcSection("Imp"), 58397.786);
        });

        // force tractrice (contrainte de cisaillement)
        it("Tau0 should equal to 219.455", () => {
            checkResult(sect.CalcSection("Tau0"), 219.455);
        });
    });
});
