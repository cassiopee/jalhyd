import { ParamCalculability, ParamDefinition, ParamDomainValue, ParamFamily } from "../src/index";
import { Nub } from "../src/nub";
import { ParamsEquation } from "../src/param/params-equation";
import { Result } from "../src/util/result";

export class NubTestParams extends ParamsEquation {
    private _A: ParamDefinition;
    private _B: ParamDefinition;
    private _C: ParamDefinition;

    constructor() {
        super();
        this._A = new ParamDefinition(this, "A", ParamDomainValue.POS_NULL, undefined, 1, ParamFamily.WIDTHS);
        this._B = new ParamDefinition(this, "B", ParamDomainValue.POS_NULL, undefined, 2);
        this._C = new ParamDefinition(this, "C", ParamDomainValue.POS_NULL, undefined, 3, ParamFamily.WIDTHS);

        this.addParamDefinition(this._A);
        this.addParamDefinition(this._B);
        this.addParamDefinition(this._C);
    }

    get A() {
        return this._A;
    }

    get B() {
        return this._B;
    }

    get C() {
        return this._C;
    }
}

// tslint:disable-next-line:max-classes-per-file
export class NubTest extends Nub {
    constructor(prms: NubTestParams, dbg: boolean = false) {
        super(prms, dbg); // sets calculaedParam to "C"
    }

    public Equation(): Result {
        // C = A+B
        return new Result(this.prms.A.v + this.prms.B.v, this);
    }

    protected setParametersCalculability() {
        this.getParameter("A").calculability = ParamCalculability.DICHO;
        this.getParameter("B").calculability = ParamCalculability.DICHO;
        this.getParameter("C").calculability = ParamCalculability.EQUATION;
    }

    get prms(): NubTestParams {
        return this._prms as NubTestParams;
    }

}

export let nub = new NubTest(new NubTestParams());
