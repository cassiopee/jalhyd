import { MessageCode } from "../../src/index";
import { YAXN } from "../../src/math/yaxn";
import { YAXNParams } from "../../src/math/yaxn_params";

describe("Class YAXN: ", () => {

    it("Y should be 65.625", () => {
        const nub = new YAXN(new YAXNParams(4.2, 2.5, 3));
        nub.calculatedParam = nub.prms.Y;
        nub.CalcSerie();
        expect(nub.result.vCalc).toBe(65.625);
    });

    it("X should be 2.5", () => {
        const nub = new YAXN(new YAXNParams(4.2, 666, 3, 65.625));
        nub.calculatedParam = nub.prms.X;
        nub.CalcSerie();
        expect(nub.result.vCalc).toBeCloseTo(2.5, 3);
    });

    it("non-integer N on negative X should lead to error log", () => {
        const nub = new YAXN(new YAXNParams(10, -4, 3.2));
        nub.calculatedParam = nub.prms.Y;
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeUndefined();
        expect(res.log.messages.length).toBe(1);
        expect(res.log.messages[0].code).toBe(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
    });

});
