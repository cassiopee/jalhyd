import { Trigo, TrigoOperation, TrigoUnit } from "../../src/math/trigo";
import { TrigoParams } from "../../src/math/trigo_params";
import { Session } from "../../src/session";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { LoiDebit } from "../../src/structure/structure_props";

describe("Class Trigo: ", () => {

    describe("Calc Y with degrees: ", () => {

        it("Y = cos(X) should be 0.985", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.COS;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.985, 3);
        });

        it("Y = sin(X) should be 0.174", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.SIN;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.174, 3);
        });

        it("Y = tan(X) should be 0.176", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.TAN;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.176, 3);
        });

        it("Y = cosh(X) should be 1.015", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.COSH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(1.015, 3);
        });

        it("Y = sinh(X) should be 0.175", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.SINH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.175, 3);
        });

        it("Y = tanh(X) should be 0.173", () => {
            const nub = new Trigo(new TrigoParams(1.5, 10));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.DEG;
            nub.operation = TrigoOperation.TANH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.173, 3);
        });

    });

    describe("Calc Y with radians: ", () => {

        it("Y = cos(X) should be 0.540", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.COS;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.540, 3);
        });

        it("Y = sin(X) should be 0.841", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.SIN;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.841, 3);
        });

        it("Y = tan(X) should be 1.557", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.TAN;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(1.557, 3);
        });

        it("Y = cosh(X) should be 1.543", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.COSH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(1.543, 3);
        });

        it("Y = sinh(X) should be 1.175", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.SINH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(1.175, 3);
        });

        it("Y = tanh(X) should be 0.762", () => {
            const nub = new Trigo(new TrigoParams(1.5, 1));
            nub.calculatedParam = nub.prms.Y;
            nub.unit = TrigoUnit.RAD;
            nub.operation = TrigoOperation.TANH;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(0.762, 3);
        });

    });

    describe("Calc X: ", () => {
        for (const un of [ TrigoUnit.DEG, TrigoUnit.RAD ]) {
            for (const op of [
                TrigoOperation.COS, TrigoOperation.SIN, TrigoOperation.TAN,
                TrigoOperation.COSH, TrigoOperation.SINH, TrigoOperation.TANH
            ]) {
                it("X = acos(Y = cos(X)) should be X", () => {
                    const nub = new Trigo(new TrigoParams(0, 1.5));
                    nub.calculatedParam = nub.prms.Y;
                    nub.unit = un;
                    nub.operation = op;
                    nub.CalcSerie();
                    nub.prms.Y.singleValue = nub.result.vCalc;
                    nub.prms.X.singleValue = 666;
                    nub.calculatedParam = nub.prms.X;
                    nub.CalcSerie();
                    expect(nub.result.vCalc).toBeCloseTo(1.5, 5);
                });
            }
        }
    });

    describe("Link: ", () => {

        it("all parameters must be linkable to Y and X in both ways", () => {
            const trig = new Trigo(new TrigoParams(1.5, 10));
            const lo = new ParallelStructure(new ParallelStructureParams(1.2, 102, 101));
            lo.addChild(CreateStructure(LoiDebit.GateCem88d, lo));

            Session.getInstance().clear();
            Session.getInstance().registerNubs([ trig, lo ]);

            // each Trigo param should be linkable to all ParallelStructures params
            expect(Session.getInstance().getLinkableValues(trig.prms.Y).length).toBe(8);
            expect(Session.getInstance().getLinkableValues(trig.prms.X).length).toBe(8);

            // each ParallelStructures param should be linkable to all Trigo params
            for (const p of lo.parameterIterator) {
                expect(Session.getInstance().getLinkableValues(p).length).toBe(2);
            }
        });

    });

    it("Calculated param should be Y", () => {
        const trig = new Trigo(new TrigoParams(1.5, 10));
        expect(trig.calculatedParam.symbol).toBe("Y");
    });

    it ("definition domains depending on operation", () => {
        const nub = new Trigo(new TrigoParams(1.5, 1));
        nub.calculatedParam = nub.prms.Y;
        nub.unit = TrigoUnit.RAD;
        // RAD / COS (COS is default operation)
        expect(nub.prms.Y.domain.interval.min).toBe(-1);
        expect(nub.prms.Y.domain.interval.max).toBe(1);
        // RAD / TAN
        nub.operation = TrigoOperation.TAN;
        expect(nub.prms.Y.domain.interval.min).toBe(-Infinity);
        expect(nub.prms.Y.domain.interval.max).toBe(Infinity);
        // RAD / SIN
        nub.operation = TrigoOperation.SIN;
        expect(nub.prms.Y.domain.interval.min).toBe(-1);
        expect(nub.prms.Y.domain.interval.max).toBe(1);
        // RAD / SINH
        nub.operation = TrigoOperation.SINH;
        expect(nub.prms.Y.domain.interval.min).toBe(-Infinity);
        expect(nub.prms.Y.domain.interval.max).toBe(Infinity);
        // RAD / TANH
        nub.operation = TrigoOperation.TANH;
        expect(nub.prms.Y.domain.interval.min).toBe(-0.99999999);
        expect(nub.prms.Y.domain.interval.max).toBe(0.99999999);
        // RAD / COSH
        nub.operation = TrigoOperation.COSH;
        expect(nub.prms.Y.domain.interval.min).toBe(1);
        expect(nub.prms.Y.domain.interval.max).toBe(Infinity);
    });

});
