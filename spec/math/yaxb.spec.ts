import { YAXB } from "../../src/math/yaxb";
import { YAXBParams } from "../../src/math/yaxb_params";
import { Session } from "../../src/session";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { LoiDebit } from "../../src/structure/structure_props";
import { MessageCode } from "../../src/util/message";

describe("Class YAXB: ", () => {

    describe("Calc: ", () => {

        it("Y should be 10", () => {
            const nub = new YAXB(new YAXBParams(666, 2, 3, 4));
            nub.calculatedParam = nub.prms.Y;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(10);
        });

        it("A should be 2", () => {
            const nub = new YAXB(new YAXBParams(10, 666, 3, 4));
            nub.calculatedParam = nub.prms.A;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(2);
        });

        it("X should be 3", () => {
            const nub = new YAXB(new YAXBParams(10, 2, 666, 4));
            nub.calculatedParam = nub.prms.X;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(3);
        });

        it("B should be 4", () => {
            const nub = new YAXB(new YAXBParams(10, 2, 3, 666));
            nub.calculatedParam = nub.prms.B;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(4);
        });

        it("calc A with X = 0 should trigger error", () => {
            const nub = new YAXB(new YAXBParams(10, 666, 0, 4));
            nub.calculatedParam = nub.prms.A;
            nub.CalcSerie();
            expect(nub.result.ok).toBe(false);
            expect(nub.result.resultElement.log.messages.length).toBe(1);
            expect(nub.result.resultElement.log.messages[0].code).toBe(MessageCode.ERROR_DIVISION_BY_ZERO);
        });

        it("calc X with A = 0 should trigger error", () => {
            const nub = new YAXB(new YAXBParams(10, 0, 666, 4));
            nub.calculatedParam = nub.prms.X;
            nub.CalcSerie();
            expect(nub.result.ok).toBe(false);
            expect(nub.result.resultElement.log.messages.length).toBe(1);
            expect(nub.result.resultElement.log.messages[0].code).toBe(MessageCode.ERROR_DIVISION_BY_ZERO);
        });

    });

    describe("Link: ", () => {

        it("all parameters must be linkable to Y,A,X,B in both ways", () => {
            const yaxb = new YAXB(new YAXBParams(10, 2, 3, 4));
            const lo = new ParallelStructure(new ParallelStructureParams(1.2, 102, 101));
            lo.addChild(CreateStructure(LoiDebit.GateCem88d, lo));

            Session.getInstance().clear();
            Session.getInstance().registerNubs([ yaxb, lo ]);

            // each YAXB param should be linkable to all ParallelStructures params
            expect(Session.getInstance().getLinkableValues(yaxb.prms.Y).length).toBe(8);
            expect(Session.getInstance().getLinkableValues(yaxb.prms.A).length).toBe(8);
            expect(Session.getInstance().getLinkableValues(yaxb.prms.X).length).toBe(8);
            expect(Session.getInstance().getLinkableValues(yaxb.prms.B).length).toBe(8);

            // each ParallelStructures param should be linkable to all YAXB params
            for (const p of lo.parameterIterator) {
                expect(Session.getInstance().getLinkableValues(p).length).toBe(4);
            }
        });

    });

});
