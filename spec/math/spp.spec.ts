import { SPP, SPPOperation } from "../../src/math/spp";
import { SPPParams } from "../../src/math/spp_params";
import { YAXN } from "../../src/math/yaxn";
import { YAXNParams } from "../../src/math/yaxn_params";
import { Session } from "../../src/session";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { LoiDebit } from "../../src/structure/structure_props";
import { MessageCode } from "../../src/util/message";

describe("Class SPP: ", () => {

    describe("Calc Y (sum): ", () => {

        it("Y = sum([ 3*2^4, 7*1^0 ]) should be 55", () => {
            const nub = new SPP(new SPPParams(1));
            nub.addChild(new YAXN(new YAXNParams(3, 2, 4)));
            nub.addChild(new YAXN(new YAXNParams(7, 1, 0)));
            nub.operation = SPPOperation.SUM;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(55);
        });

    });

    describe("Calc Y (product): ", () => {

        it("Y = product([ 3*2^4, 7*1^0 ]) should be 336", () => {
            const nub = new SPP(new SPPParams(1));
            nub.addChild(new YAXN(new YAXNParams(3, 2, 4)));
            nub.addChild(new YAXN(new YAXNParams(7, 1, 0)));
            nub.operation = SPPOperation.PRODUCT;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBe(336);
        });

    });

    describe("Calc some X (sum): ", () => {

        it("sum([ 3*X^4, 7*1^0 ]) = 55 - X should be 2", () => {
            const nub = new SPP(new SPPParams(55));
            nub.addChild(new YAXN(new YAXNParams(3, 5, 4)));
            nub.addChild(new YAXN(new YAXNParams(7, 1, 0)));
            nub.operation = SPPOperation.SUM;
            const c = nub.getChildren()[0] as YAXN;
            nub.calculatedParam = c.prms.X;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(2, 3);
        });

    });

    describe("Calc some Y (product): ", () => {

        it("product([ 3*X^4, 7*1^0 ]) = 336 - X should be 2", () => {
            const nub = new SPP(new SPPParams(336));
            nub.addChild(new YAXN(new YAXNParams(3, 17, 4)));
            nub.addChild(new YAXN(new YAXNParams(7, 1, 0)));
            nub.operation = SPPOperation.PRODUCT;
            const c = nub.getChildren()[0] as YAXN;
            nub.calculatedParam = c.prms.X;
            nub.CalcSerie();
            expect(nub.result.vCalc).toBeCloseTo(2, 3);
        });

    });

    describe("Link: ", () => {

        it("all parameters must be linkable to Y and children A, X, N in both ways", () => {
            const spp = new SPP(new SPPParams(1));
            spp.addChild(new YAXN(new YAXNParams(3, 666, 4)));
            spp.addChild(new YAXN(new YAXNParams(7, 1, 0)));
            spp.addChild(new YAXN(new YAXNParams(5, 4, 3)));

            const lo = new ParallelStructure(new ParallelStructureParams(1.2, 102, 101));
            lo.addChild(CreateStructure(LoiDebit.GateCem88d, lo));

            Session.getInstance().clear();
            Session.getInstance().registerNubs([ spp, lo ]);

            // SPP.Y should be linkable to all ParallelStructures params
            const lpY = Session.getInstance().getLinkableValues(spp.prms.Y);
            expect(lpY.length).toBe(8);

            // all 3 YAXN.X should be linkable to all ParallelStructures params
            for (const c of spp.getChildren()) {
                const lp = Session.getInstance().getLinkableValues((c as YAXN).prms.X);
                expect(lp.length).toBe(14); // 8, plus siblings params
            }

            // each ParallelStructures param should be linkable to SPP.Y and all 3 YAXN.X
            for (const p of lo.parameterIterator) {
                const lpLO = Session.getInstance().getLinkableValues(p);
                expect(lpLO.length).toBe(10);
            }
        });

    });

    it("Calc(X) find negative C for integer N", () => {
        const spp = new SPP(new SPPParams(-27));
        const c1 = new YAXN(new YAXNParams(1, 666, 3));
        spp.addChild(c1);
        spp.calculatedParam = c1.prms.X;
        expect(spp.CalcSerie().vCalc).toBeCloseTo(-3, 3);
    });

    describe("error cases − ", () => {

        it("Calc(Y), non-integer N on negative X in child should lead to error log", () => {
            const spp = new SPP(new SPPParams(666));
            const c1 = new YAXN(new YAXNParams(1, -4, 3.2));
            spp.addChild(c1);
            spp.calculatedParam = spp.prms.Y;
            const res = spp.CalcSerie();
            expect(res.vCalc).toBeUndefined();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
            expect(c1.result.log.messages.length).toBe(1);
            expect(c1.result.log.messages[0].code).toBe(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
        });

        it("Calc(X), non-integer N on negative X in non-calculated child should lead to error log", () => {
            const spp = new SPP(new SPPParams(536636.0711158155));
            const c1 = new YAXN(new YAXNParams(-495973.326926908, 986950.0910993216, 3.8095699869980315));
            const c2 = new YAXN(new YAXNParams(694194.424256592, -985042.6786260167, -0.22419216942446418));
            spp.addChild(c1);
            spp.addChild(c2);
            spp.calculatedParam = c1.prms.X;
            const res = spp.CalcSerie();
            expect(res.vCalc).toBeUndefined();
            expect(res.ok).toBe(false);
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
            expect(c2.result.log.messages.length).toBe(1);
            expect(c2.result.log.messages[0].code).toBe(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
        });

        it("Calc(X), non-integer N on negative X in calculated child should lead to error log", () => {
            const spp = new SPP(new SPPParams(536636.0711158155));
            const c1 = new YAXN(new YAXNParams(-495973.326926908, 986950.0910993216, 3.8095699869980315));
            const c2 = new YAXN(new YAXNParams(694194.424256592, -985042.6786260167, -0.22419216942446418));
            spp.addChild(c1);
            spp.addChild(c2);
            spp.calculatedParam = c2.prms.X;
            const res = spp.CalcSerie();
            expect(res.vCalc).toBeUndefined();
            expect(res.ok).toBe(false);
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
            expect(c2.result.log.messages.length).toBe(1);
            expect(c2.result.log.messages[0].code).toBe(MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
        });

        it("division by zero in parent should lead to error log", () => {
            const spp = new SPP(new SPPParams(1));
            spp.operation = SPPOperation.PRODUCT;
            const c1 = new YAXN(new YAXNParams(2, 666, 3));
            spp.addChild(c1);
            const c2 = new YAXN(new YAXNParams(0, 1, 1));
            spp.addChild(c2);
            spp.calculatedParam = c1.prms.X;
            const res = spp.CalcSerie();
            expect(res.vCalc).toBeUndefined();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_DIVISION_BY_ZERO);
        });

    });

});
