import { CalculatorType, MessageCode, Props, Session } from "../../src/index";
import { RugoFond } from "../../src/rugofond/rugofond";
import { RugoFondParams } from "../../src/rugofond/rugofond_params";
import { RugoFondMultiple, RugofondType } from "../../src/rugofond/rugofond_multiple";
import { RugoFondMultipleParams } from "../../src/rugofond/rugofond_multiple_params";

/**
 * Return default instance of Rugofond as reference for one apron
 */
function getRugoFondRef(): RugoFond {
    const rugoFond = new RugoFond(
        new RugoFondParams(
            3.328, // Débit total (m3/s)
            12.8,   // Cote de l'eau amont (m) 
            3,    // Largeur totale de la rampe  (m)
            0.024,  // Pente longitudinale de la rampe (m/m)
            0.35, // Coefficient de débit
            0.3,   // d65 des enrochements du fond (m)
            15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
            12   // Cote de fond amont (m)
        )
    );
    rugoFond.prms.Z1.singleValue = 12.8;
    rugoFond.prms.ZF1.singleValue = 12;
    rugoFond.prms.L.singleValue = 3;
    rugoFond.prms.Cd.singleValue = 0.35;
    rugoFond.prms.Q.setCalculated();
    // Calcul du débit dans le cas d'un radier horizontal
    rugoFond.CalcSerie();
    return rugoFond;
}

describe("RugofondMultiple: ", () => {
    describe("Default 1 flow section", () => {
        
        it("should return same result as Rugofond", () => {
            const rugofondMultiple = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.RugoFondMultiple })
            ) as RugoFondMultiple;
            // change Nbt rugofondMultiple default to be in same configuration as Rugofond
            rugofondMultiple.prms.NbT.singleValue = 1;
            rugofondMultiple.addDefaultChild();
            const rugoFond = getRugoFondRef();
            expect(rugofondMultiple.CalcSerie().vCalc).toBeCloseTo(rugoFond.result.vCalc, 3);
            expect(rugofondMultiple.CalcSerie().extraResults.LIncl).toBe(0);
            //Trigger warning waterline message
        });

        it("should trigger a warning message on slope value", () => {
            const rugofondMultiple = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.RugoFondMultiple })
            ) as RugoFondMultiple;
            // set a slope value lower than recommanded value to trigger warning message;
            rugofondMultiple.prms.If.singleValue = 0.01;
            expect(rugofondMultiple.CalcSerie().log.messages[0].code).toBe(MessageCode.WARNING_RUGOFOND_SLOPE_OUT_OF_RANGE);
        });

        it("should trigger a warning or an error message when normal depth is too hight ", () => {
            const rugofondMultiple = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.RugoFondMultiple })
            ) as RugoFondMultiple;
            // set a slope value to compute Yn > 2/3 * (Z1_ ZF1) and trigger warning message;
            expect(rugofondMultiple.CalcSerie().log.messages[0].code).toBe(MessageCode.WARNING_RUGOFOND_WATERLINE_TOO_HIGHT);
            // set a slope value to compute Yn > (Z1_ ZF1) and trigger error message;
            rugofondMultiple.prms.If.singleValue = 0.005;
            expect(rugofondMultiple.CalcSerie().log.messages[1].code).toBe(MessageCode.ERROR_RUGOFOND_WATERLINE_TOO_HIGHT);
        });

        it("Calc Q, inclined, Z1 and Cd", () => {
            const rugofondMultiple = new RugoFondMultiple(
                new RugoFondMultipleParams(
                    undefined, // Débit total (m3/s)
                   12.8,   // Cote de l'eau amont (m) 
                    3,    // Largeur totale de la rampe  (m)
                    0.024,  // Pente longitudinale de la rampe (m/m)
                    0.35, // Coefficient de débit
                    0.3,   // d65 des enrochements du fond (m)
                    15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                    56.7,   // Cote de fond amont (m)
                    12, // Cote de fond bas amont (m)
                    13,   // Cote de fond haut amont (m) 
                    1   // Nombre de tranche d'écoulement
                )
            );
            // Choix du radier incliné
            rugofondMultiple.setPropValue("rugofondType",RugofondType.INCLINED);
            // Calcul de Q
            expect(rugofondMultiple.CalcSerie().vCalc).toBeCloseTo(	0.764, 3);
            expect(rugofondMultiple.children[0].prms.ZF1.singleValue).toBe(12.5);
            expect(rugofondMultiple.CalcSerie().extraResults.LIncl).toBeCloseTo(0.333, 3);
            // Calcul de Z1 à partir du débit calculé précédemment( on doit trouver la valeur du paramètre d'entrée)
            rugofondMultiple.prms.Q.v = rugofondMultiple.CalcSerie().vCalc;
            expect(rugofondMultiple.Calc("Z1").vCalc).toBe(12.8);
            // Calcul de Cd à partir du débit calculé précédemment( on doit trouver la valeur du paramètre d'entrée)
            rugofondMultiple.prms.Z1.v = rugofondMultiple.Calc("Z1").vCalc;
            expect(rugofondMultiple.Calc("Cd").vCalc).toBe(0.35);
        });

        it ("calc Q when Z1 < ZF1 give an error log", () => {
            const rugofondMultiple = new RugoFondMultiple(
                new RugoFondMultipleParams(
                    undefined, // Débit total (m3/s)
                    12.8,   // Cote de l'eau amont (m) 
                    3,    // Largeur totale de la rampe  (m)
                    0.024,  // Pente longitudinale de la rampe (m/m)
                    0.35, // Coefficient de débit
                    0.3,   // d65 des enrochements du fond (m)
                    15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                    56.7,   // Cote de fond amont (m)
                    12.5, // Cote de fond bas amont (m)
                    14,   // Cote de fond haut amont (m) 
                    1   // Nombre de tranche d'écoulement
                )
            );
            // Choix du radier incliné
            rugofondMultiple.setPropValue("rugofondType",RugofondType.INCLINED);
            const res = rugofondMultiple.CalcSerie()
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_LOWER_THAN_ZF1);
        });
        it ("calc Q when Z1 = ZF1 give an error log", () => {
            const rugofondMultiple = new RugoFondMultiple(
                new RugoFondMultipleParams(
                    undefined, // Débit total (m3/s)
                    12,   // Cote de l'eau amont (m) 
                    3,    // Largeur totale de la rampe  (m)
                    0.024,  // Pente longitudinale de la rampe (m/m)
                    0.35, // Coefficient de débit
                    0.3,   // d65 des enrochements du fond (m)
                    15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                    56.7,   // Cote de fond amont (m)
                    12, // Cote de fond bas amont (m)
                    12,   // Cote de fond haut amont (m) 
                    1   // Nombre de tranche d'écoulement)
                )
            );
            // Choix du radier incliné
            rugofondMultiple.setPropValue("rugofondType",RugofondType.INCLINED);
            const res = rugofondMultiple.CalcSerie()
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1);
        });
    })
    describe("inclined apron with 3 flow sections", () => {
        it("Calc each ZF1 from ZRR and ZRT, total flow and child flows", () => {
            const rugofondMultiple = new RugoFondMultiple(
                new RugoFondMultipleParams(
                    undefined, // Débit total (m3/s)
                    12.8,   // Cote de l'eau amont (m) 
                    3,    // Largeur totale de la rampe  (m)
                    0.024,  // Pente longitudinale de la rampe (m/m)
                    0.35, // Coefficient de débit
                    0.3,   // d65 des enrochements du fond (m)
                    15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                    undefined,   // Cote de fond amont (m)
                    12, // Cote de fond bas amont (m)
                    13,   // Cote de fond haut amont (m) 
                    3   // Nombre de tranche d'écoulement
                )
            );
            // Choix du radier incliné
            rugofondMultiple.setPropValue("rugofondType",RugofondType.INCLINED);
            expect(rugofondMultiple.CalcSerie().vCalc).toBeCloseTo(	1.036, 3);
            expect(rugofondMultiple.children[0].prms.ZF1.singleValue).toBeCloseTo(12.833, 3);
            expect(rugofondMultiple.children[0].prms.Q.V).toBe(0);
            expect(rugofondMultiple.children[1].prms.Q.V).toBeCloseTo(0.255, 3);
            expect(rugofondMultiple.children[2].prms.Q.V).toBeCloseTo(0.781, 3);
            expect(rugofondMultiple.children[1].prms.ZF1.singleValue).toBe(12.5);
            expect(rugofondMultiple.children[2].prms.ZF1.singleValue).toBeCloseTo(12.167, 3);
            expect(rugofondMultiple.children[0].prms.L.v).toBe(1);
            expect(rugofondMultiple.CalcSerie().extraResults.LIncl).toBeCloseTo(0.333, 3);
        });

        it("calc Q when Z1 = ZF1 give an error log on each child (Q =0) ", () => {
            const rugofondMultiple = new RugoFondMultiple(
                new RugoFondMultipleParams(
                    undefined, // Débit total (m3/s)
                    12,   // Cote de l'eau amont (m) 
                    3,    // Largeur totale de la rampe  (m)
                    0.024,  // Pente longitudinale de la rampe (m/m)
                    0.35, // Coefficient de débit
                    0.3,   // d65 des enrochements du fond (m)
                    15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                    undefined,   // Cote de fond amont (m)
                    12, // Cote de fond bas amont (m)
                    12,   // Cote de fond haut amont (m) 
                    3   // Nombre de tranche d'écoulement
                )
            );
            // Choix du radier incliné
            rugofondMultiple.setPropValue("rugofondType",RugofondType.INCLINED);
            const res = rugofondMultiple.CalcSerie()
            expect(res.log.messages.length).toBe(3);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1);
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1);
            expect(res.log.messages[2].code).toBe(MessageCode.WARNING_RUGOFOND_MULTIPLE_ELEVATION_Z1_EQUAL_TO_ZF1);
            expect(rugofondMultiple.children[0].prms.Q.V).toBe(0);
            expect(rugofondMultiple.children[1].prms.Q.V).toBe(0);
            expect(rugofondMultiple.children[2].prms.Q.V).toBe(0);
        });
    });
});
