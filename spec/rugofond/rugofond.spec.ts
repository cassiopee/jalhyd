import { RugoFond } from "../../src/internal_modules";
import { RugoFondParams } from "../../src/internal_modules";

let nub: RugoFond;

describe("Class RugoFond − ", () => {
    beforeEach(() => {
        nub = new RugoFond(
            new RugoFondParams(
                3.328, // Débit total (m3/s)
                12.8,   // Cote de l'eau amont (m) 
                3,    // Largeur totale de la rampe  (m)
                0.024,  // Pente longitudinale de la rampe (m/m)
                0.35, // Coefficient de débit
                0.3,   // d65 des enrochements du fond (m)
                15.5,    // Coefficient de rugosité de Strickler Ks (m1/3/s)
                12   // Cote de fond amont (m)
            )
        );
    });

    it("calc Q and extra results(Yn,Vdeb)", () => {
        nub.prms.Q.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(3.328, 2);
    });

    it("calc Z1", () => {
        nub.prms.Z1.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(12.8, 2);
    });

    it("calc ZF1", () => {
        nub.prms.ZF1.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(12, 2);
    });

    it("calc L", () => {
        nub.prms.L.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(3,2);
    });

    it("calc Cd", () => {
        nub.prms.Cd.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(0.35, 2);
    });
});
