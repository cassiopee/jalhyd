import { JetParams } from "../../src/devalaison/jet_params";
import {
    cSnTrapez, Jet, LinkedValue, Nub, ParallelStructure,
    ParallelStructureParams, ParamValueMode,
    SectionParametree,
    Session
} from "../../src/index";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { cSnCirc } from "../../src/open-channel/section/section_circulaire";
import { ParamsSectionCirc } from "../../src/open-channel/section/section_circulaire_params";
import { ParamsSectionTrapez } from "../../src/open-channel/section/section_trapez_params";
import { Cloisons } from "../../src/pab/cloisons";
import { CloisonsParams } from "../../src/pab/cloisons_params";
import { SessionSettings } from "../../src/session_settings";
import { Dever } from "../../src/structure/dever";
import { DeverParams } from "../../src/structure/dever_params";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";

let nub1: RegimeUniforme;
let prm1: ParamsSectionCirc;
let nub2: ParallelStructure;
let prm2: ParallelStructureParams;
let nub3: Cloisons;
let prm3: CloisonsParams;
let nub4: Dever;
let prm4: DeverParams;
let nub5: Dever;
let prm5: DeverParams;
let nub6: SectionParametree;
let prm6: ParamsSectionTrapez;
let nub7: Jet;
let prm7: JetParams;

/**
 * crée l'environnement de test.
 * répété à chaque test car il manque un mock de beforeEach
 */
function createEnv() {
    // Nub 1 : Régime Uniforme
    const paramSect = new ParamsSectionCirc(2, 0.6613, 40, 1.2, 0.001, 1);
    SessionSettings.precision = 0.01;
    const sect = new cSnCirc(paramSect);
    nub1 = new RegimeUniforme(sect);
    prm1 = nub1.section.prms as ParamsSectionCirc;

    // Nub 2 : Lois d'ouvrages
    prm2 = new ParallelStructureParams(0.5, 102, 101.5);
    SessionSettings.precision = 0.01;
    nub2 = new ParallelStructure(prm2);
    nub2.addChild(
        CreateStructure(
            LoiDebit.GateCunge80,
            nub2
        )
    );
    nub2.addChild(
        CreateStructure(
            LoiDebit.TriangularWeirFree,
            nub2
        )
    );

    // Nub 3 : Passe à Bassin : Cloisons
    prm3 = new CloisonsParams(1.5, 102, 10, 1, 1, 0.5);
    nub3 = new Cloisons(prm3);
    nub3.addChild(
        CreateStructure(
            LoiDebit.OrificeSubmerged,
            nub3
        )
    );
    nub3.addChild(
        CreateStructure(
            LoiDebit.KIVI,
            nub3
        )
    );

    // Nub 4 : Lois de déversoirs Dénoyés
    prm4 = new DeverParams(0.5, 102, 10, 99);
    SessionSettings.precision = 0.01;
    nub4 = new Dever(prm4);
    nub4.addChild(
        CreateStructure(
            LoiDebit.WeirFree,
            nub4,
            false
        )
    );
    nub4.addChild(
        CreateStructure(
            LoiDebit.TriangularTruncWeirFree,
            nub4,
            false
        )
    );

    // Nub 5 : Lois de déversoirs Dénoyés (2)
    prm5 = new DeverParams(0.5, 102, 10, 99);
    SessionSettings.precision = 0.01;
    nub5 = new Dever(prm5);
    nub5.addChild(
        CreateStructure(
            LoiDebit.WeirFree,
            nub5,
            false
        )
    );
    nub5.addChild(
        CreateStructure(
            LoiDebit.TriangularTruncWeirFree,
            nub5,
            false
        )
    );

    // Nub 6 : Section Paramétrée
    prm6 = new ParamsSectionTrapez(1, 0.5, 1, 0.01, 1, 0.01, 2);
    SessionSettings.precision = 0.01;
    nub6 = new SectionParametree(new cSnTrapez(prm6));

    // Nub 8 : Jet
    prm7 = new JetParams(5, 0.03, 30, 29.2, 28.5, 3);
    SessionSettings.precision = 0.01;
    nub7 = new Jet(prm7);

    // populate Session (for links)
    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);
    Session.getInstance().registerNub(nub3);
    Session.getInstance().registerNub(nub4);
    Session.getInstance().registerNub(nub5);
    Session.getInstance().registerNub(nub6);
}

/**
 * Check that nub has :
 *  - exactly 1 parameter in CALC mode
 *  - a defined calculatedParam
 *  - a valid result after calculation
 */
function checkConsistency(nubToCheck: Nub) {
    expect(nubToCheck.calculatedParam).toBeDefined();
    let calcCount = 0;
    for (const p of nubToCheck.parameterIterator) {
        if (p.valueMode === ParamValueMode.CALCUL) {
            calcCount++;
        }
    }
    expect(calcCount).toBe(1);

    nubToCheck.CalcSerie();
    expect(nubToCheck.result).toBeDefined();
}

/**
 * Tests multiple modes permutations and checks consistency
 * after each modification
 */
function testModesPermutations(nubToTest: Nub) {

    // set every parameter to CALC mode
    for (const p of nubToTest.calculableParameters) {
        p.setCalculated();
        checkConsistency(nubToTest);
    }

    // set every parameter to MINMAX / LISTE mode
    let i = 0;
    for (const p of nubToTest.parameterIterator) {
        if (!p.visible) {
            continue;
        }
        if (i % 2 === 0) {
            p.setValues(1, 5, 0.5); // sets valueMode to MINMAX
        } else {
            p.setValues([1, 2, 3, 4, 5]); // sets valueMode to LISTE
        }
        checkConsistency(nubToTest);
        i++;
    }

    // set every parameter to CALC then to SINGLE mode
    for (const p of nubToTest.calculableParameters) {
        p.setCalculated();
        checkConsistency(nubToTest);
        p.valueMode = ParamValueMode.SINGLE;
        checkConsistency(nubToTest);
    }

    // set every parameter to LINK mode then to SINGLE mode
    for (const p of nubToTest.parameterIterator) {
        if (!p.visible) {
            continue;
        }
        const lv: LinkedValue[] = Session.getInstance().getLinkableValues(p);
        if (lv.length > 0) {
            p.defineReference(lv[0].nub, lv[0].symbol); // sets mode to LINK
            checkConsistency(nubToTest);
            p.valueMode = ParamValueMode.SINGLE;
            checkConsistency(nubToTest);
        }
    }
}

describe("cohérence des modes de paramètres : ", () => {

    it("paramètre simple", () => {
        createEnv();
        testModesPermutations(nub1);
    });

    it("ouvrages en parallèle : Lois d'Ouvrages", () => {
        createEnv();
        testModesPermutations(nub2);
    });

    it("ouvrages en parallèle : Cloisons", () => {
        createEnv();
        testModesPermutations(nub3);
    });

    it("ouvrages en parallèle : Déversoirs", () => {
        createEnv();
        testModesPermutations(nub4);
    });

    it("paramètre varié lié", () => {
        createEnv();
        // vary Q on nub1
        prm1.Q.setValues(0.6, 2.4, 0.09);

        // link other Nubs Q to nub1.Q
        for (const n of [nub2, nub3, nub4]) {
            n.prms.Q.defineReference(nub1.section, "Q");
            // set every parameter to MINMAX / LISTE mode
            let i = 0;
            for (const p of n.parameterIterator) {
                if (p.symbol !== "Q") {
                    if (i % 2 === 0) {
                        p.setValues(1, 5, 0.5); // sets valueMode to MINMAX
                    } else {
                        p.setValues([1, 2, 3, 4, 5]); // sets valueMode to LISTE
                    }
                }
                checkConsistency(n);
                i++;
            }
        }
    });

    it("résultat varié lié", () => {
        createEnv();
        // vary LargeurBerge and compute Q on nub1
        prm1.D.setValues(1, 4, 0.15);
        prm1.Q.setCalculated();

        // link other Nubs Q to nub1.Q
        for (const n of [nub2, nub3, nub4]) {
            n.prms.Q.defineReference(nub1, "Q");
            // set every parameter to MINMAX / LISTE mode
            let i = 0;
            for (const p of n.parameterIterator) {
                if (p.symbol !== "Q") {
                    if (i % 2 === 0) {
                        p.setValues(1, 5, 0.5); // sets valueMode to MINMAX
                    } else {
                        p.setValues([1, 2, 3, 4, 5]); // sets valueMode to LISTE
                    }
                }
                checkConsistency(n);
                i++;
            }
        }
    });

    it("résultat complémentaire varié lié", () => {
        createEnv();
        // vary LargeurBerge and compute Q on nub1
        prm1.LargeurBerge.setValues(50, 200, 10);
        prm1.Q.setCalculated();

        // link Jet V0 to nub1.V
        nub7.prms.V0.defineReference(nub1, "V");
        // set every parameter to MINMAX / LISTE mode
        let i = 0;
        for (const p of nub7.parameterIterator) {
            if (p.symbol !== "V0") {
                if (i % 2 === 0) {
                    p.setValues(1, 5, 0.5); // sets valueMode to MINMAX
                } else {
                    p.setValues([1, 2, 3, 4, 5]); // sets valueMode to LISTE
                }
            }
            checkConsistency(nub7);
            i++;
        }
    });
});

describe("Structure and calculated parameter(s) : ", () => {
    it("when all other params are linked, changing Structure should not lead to > 1 param. in CALC mode", () => {
        const pc1 = new CloisonsParams(1.5, 102, 10, 1, 1, 0.5);
        const c1 = new Cloisons(pc1);
        c1.addChild(CreateStructure(LoiDebit.OrificeSubmerged, c1));

        const pc2 = new CloisonsParams(1.5, 102, 10, 1, 1, 0.5);
        const c2 = new Cloisons(pc2);
        const c2s1 = CreateStructure(LoiDebit.OrificeSubmerged, c2);
        c2.addChild(c2s1);

        Session.getInstance().clear();
        Session.getInstance().registerNub(c1);
        Session.getInstance().registerNub(c2);

        pc2.Q.defineReference(c1, "Q");
        pc2.Z1.defineReference(c1, "Z1");
        pc2.LB.defineReference(c1, "LB");
        pc2.BB.defineReference(c1, "BB");
        pc2.PB.defineReference(c1, "PB");
        pc2.DH.defineReference(c1, "DH");

        checkConsistency(c2);

        const c2s2 = CreateStructure(LoiDebit.KIVI, c2);
        c2.replaceChildInplace(c2s1, c2s2);

        checkConsistency(c2);
    });
});

describe("jalhyd#440 :", () => {
    it("when setting initial value of a DICHO calculated param, value mode should not go back to SINGLE", () => {
        const ps = new ParallelStructure(new ParallelStructureParams(1, 100, 99));
        ps.calculatedParam = ps.prms.Z1;
        expect(ps.calculatedParam).toBe(ps.prms.Z1);
        expect(ps.prms.Z1.valueMode).toBe(ParamValueMode.CALCUL);
        ps.prms.Z1.setInitValue(99.8);
        expect(ps.calculatedParam).toBe(ps.prms.Z1);
        expect(ps.prms.Z1.valueMode).toBe(ParamValueMode.CALCUL);
    });
});
