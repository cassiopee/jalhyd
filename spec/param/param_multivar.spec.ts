import { CreateStructure, ExtensionStrategy, LoiDebit, ParallelStructure, ParallelStructureParams,
         ParamValueMode, Session, Structure, PabDimensionParams, PabDimension, PabPuissanceParams, PabPuissance, Result, MessageCode} from "../../src/index";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";

let prms1: ParallelStructureParams;
let nub1: ParallelStructure;
let struct1: Structure;
let prmsStruct1: RectangularStructureParams;

let prms2: ParallelStructureParams;
let nub2: ParallelStructure;
let struct2: Structure;
let prmsStruct2: RectangularStructureParams;

let prms3: PabDimensionParams;
let nub3: PabDimension;

let prms4: PabPuissanceParams;
let nub4: PabPuissance;

function createSingleNubEnv() {
    prms1 = new ParallelStructureParams(0.5, 102, 101.5);
    nub1 = new ParallelStructure(prms1);
    struct1 = CreateStructure(LoiDebit.GateCunge80, nub1);
    prmsStruct1 = struct1.prms as RectangularStructureParams;
    nub1.addChild(struct1);
}

function createLinkedNubEnv() {
    prms1 = new ParallelStructureParams(0.5, 102, 101.5);
    nub1 = new ParallelStructure(prms1);
    struct1 = CreateStructure(LoiDebit.GateCunge80, nub1);
    prmsStruct1 = struct1.prms as RectangularStructureParams;
    nub1.addChild(struct1);

    prms2 = new ParallelStructureParams(0.5, 102, 101.5);
    nub2 = new ParallelStructure(prms2);
    struct2 = CreateStructure(LoiDebit.GateCunge80, nub2);
    prmsStruct2 = struct2.prms as RectangularStructureParams;
    nub2.addChild(struct2);

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub1);
    Session.getInstance().registerNub(nub2);

    prms2.Z1.defineReference(nub1, "Z1");
    prmsStruct2.L.defineReference(struct1, "L");
}

function createLinkedNubEnv2() {
    prms3 = new PabDimensionParams(666, 1, 0.5, 666);
    nub3 = new PabDimension(prms3);
    nub3.calculatedParam = prms3.V;

    prms4 = new PabPuissanceParams(0.3, 666, 666, 666);
    nub4 = new PabPuissance(prms4);
    nub4.calculatedParam = prms4.PV;

    Session.getInstance().clear();
    Session.getInstance().registerNub(nub3);
    Session.getInstance().registerNub(nub4);

    prms4.V.defineReference(nub3, "V");
}

/**
 * Generates a random-length list of numbers close
 * @param refVal reference value
 * @param minSize minimum size of the list to be returned
 * @param maxSize maximum size of the list to be returned
 * @param maxDev maximum deviation, so that generated numbers
 *               value v verifies (refVal - maxDev) < v < (refVal + maxDev)
 * @param sort if true, list will be ordered
 */
function randomList(
    refVal: number, minSize: number = 2, maxSize: number = 20, maxDev: number = 3, sort: boolean = true
): number[] {
    let list: number[] = [];
    const size = minSize + Math.floor(Math.random() * ((maxSize - minSize) + 1)); // between minSize and maxSize
    for (let i = 0; i < size; i++) {
        let dev = (Math.random() * (2 * maxDev)) - (maxDev + 1); // between -maxDev and maxDev
        dev = Number(Math.round(Number(dev + "e3")) + "e-3"); // trick to properly round to 3 decimals
        list.push(refVal + dev);
    }
    if (sort) {
        list = list.sort((a, b) => a - b);
    }
    return list;
}

describe("multiple variated parameters - ", () => {

    describe("on the same Nub, with REPEAT_LAST strategy - ", () => {

        it("test 1 : minmax < list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 2 : minmax < minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues(1.89, 2.1, 0.03); // 8 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(8);
            checkNanUndef(res);
        });

        it("test 3 : list < minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.1); // 21 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(21);
            checkNanUndef(res);
        });

        it("test 4 : list < list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues([ 100, 101, 102 ]); // 3 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 5 : list = list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues([ 100, 100.3, 100.7, 101, 101.3, 101.7, 102, 102.3, 115 ]); // 9 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 6 : list = minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues(1.89, 2.13, 0.06); // 5 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(5);
            checkNanUndef(res);
        });

        it("test 7 : all parameters varying with random lengths", () => {
            createSingleNubEnv();
            let longest = 0;
            let sparedParam = null;
            // set all parameters to LIST mode with random length (except the first that has to stay in CALC mode)
            for (const p of nub1.parameterIterator) {
                if (p.visible) {
                    if (! sparedParam) {
                        sparedParam = p;
                        nub1.calculatedParam = p;
                    } else {
                        const rl = randomList(p.singleValue);
                        longest = Math.max(longest, rl.length);
                        p.setValues(rl);
                        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
                    }
                }
            }
            // check that all parameters are varying
            for (const p of nub1.parameterIterator) {
                if (p.visible && p !== sparedParam) {
                    expect(p.hasMultipleValues).toBe(true);
                }
            }
            // check that calculation works and length of result is length of the longest values list
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(longest);
            checkNanUndef(res);
        });
    });

    describe("on the same Nub, with RECYCLE strategy - ", () => {

        it("test 1 : minmax < list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 2 : minmax < minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues(1.89, 2.1, 0.03); // 8 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(8);
            checkNanUndef(res);
        });

        it("test 3 : list < minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.1); // 21 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(21);
            checkNanUndef(res);
        });

        it("test 4 : list < list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues([ 100, 101, 102 ]); // 3 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 5 : list = list", () => {
            createSingleNubEnv();
            prms1.Z1.setValues([ 100, 100.3, 100.7, 101, 101.3, 101.7, 102, 102.3, 115 ]); // 9 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 6 : list = minmax", () => {
            createSingleNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.RECYCLE;
            prmsStruct1.L.setValues(1.89, 2.13, 0.06); // 5 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(5);
            checkNanUndef(res);
        });

        it("test 7 : all parameters varying with random lengths", () => {
            createSingleNubEnv();
            let longest = 0;
            let sparedParam = null;
            // set all parameters to LIST mode with random length (except the first that has to stay in CALC mode)
            for (const p of nub1.parameterIterator) {
                if (p.visible) {
                    if (! sparedParam) {
                        sparedParam = p;
                        nub1.calculatedParam = p;
                    } else {
                        const rl = randomList(p.singleValue);
                        longest = Math.max(longest, rl.length);
                        p.setValues(rl);
                        p.extensionStrategy = ExtensionStrategy.RECYCLE;
                    }
                }
            }
            // check that all parameters are varying
            for (const p of nub1.parameterIterator) {
                if (p.visible && p !== sparedParam) {
                    expect(p.hasMultipleValues).toBe(true);
                }
            }
            // check that calculation works and length of result is length of the longest values list
            const res = nub1.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(longest);
            checkNanUndef(res);
        });

        it("test 8 : recycling bug !?", () => {
            const prms = new PabPuissanceParams(0.3, 666, 666, 666);
            const nub = new PabPuissance(prms);
            prms.Q.setValues(0.05, 0.2, 0.1); // 3 values, (max - min) not a multiple of step
            prms.Q.extensionStrategy = ExtensionStrategy.RECYCLE;
            prms.V.setValues(0.5, 2, 0.25); // 7 values
            nub.calculatedParam = prms.PV;
            const res = nub.CalcSerie();
            expect(res.resultElements.length).toBe(7);
            // check vCalc
            const expectedVCalc = [ 294.3, 588.6, 588.6, 117.72, 294.3, 336.343, 73.575 ];
            for (let i = 0; i < res.resultElements.length; i++) {
                expect(res.resultElements[i].vCalc).toBeCloseTo(expectedVCalc[i], 2, `res.resultElements[${i}].vCalc`);
            }
            // check Q recycled values list
            const expectedQ = [ 0.05, 0.15, 0.2, 0.05, 0.15, 0.2, 0.05 ];
            const qValues = prms.Q.getInferredValuesList(7);
            for (let i = 0; i < qValues.length; i++) {
                expect(qValues[i]).toBeCloseTo(expectedQ[i], 2, `qValues[${i}]`);
            }
        });
    });

    describe("on different linked Nubs, with REPEAT_LAST strategy - ", () => {

        it("test 1 : minmax < list", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 2 : minmax < minmax", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues(1.89, 2.1, 0.03); // 8 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(8);
            checkNanUndef(res);
        });

        it("test 3 : list < minmax", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues(100, 102, 0.1); // 21 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(21);
            checkNanUndef(res);
        });

        it("test 4 : list < list", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues([ 100, 101, 102 ]); // 3 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 5 : list = list", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues([ 100, 100.3, 100.7, 101, 101.3, 101.7, 102, 102.3, 115 ]); // 9 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues([ 1.89, 1.91, 1.99, 2, 1.7, 2.1, 2.18, 2.23, 2.6 ]); // 9 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.LISTE);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.LISTE);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(9);
            checkNanUndef(res);
        });

        it("test 6 : list = minmax", () => {
            createLinkedNubEnv();
            prms1.Z1.setValues(100, 102, 0.5); // 5 values
            prms1.Z1.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prmsStruct1.L.setValues(1.89, 2.13, 0.06); // 5 values
            prmsStruct1.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that both parameters are varying
            expect(prms1.Z1.valueMode).toBe(ParamValueMode.MINMAX);
            expect(prmsStruct1.L.valueMode).toBe(ParamValueMode.MINMAX);
            // check that calculation works
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(5);
            checkNanUndef(res);
        });

        it("test 7 : all parameters varying with random lengths", () => {
            createLinkedNubEnv();
            let longest = 0;
            let sparedParam = null;
            // set all parameters to LIST mode with random length (except the first that has to stay in CALC mode)
            for (const p of nub2.parameterIterator) {
                if (p.visible) {
                    if (! sparedParam) {
                        sparedParam = p;
                        nub2.calculatedParam = p;
                    } else {
                        if (p.valueMode !== ParamValueMode.LINK) {
                            const rl = randomList(p.singleValue);
                            longest = Math.max(longest, rl.length);
                            p.setValues(rl);
                            p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
                        }
                    }
                }
            }
            // check that all parameters are varying
            for (const p of nub2.parameterIterator) {
                if (p.visible && p !== sparedParam && p.valueMode !== ParamValueMode.LINK) {
                    expect(p.hasMultipleValues).toBe(true);
                }
            }
            // check that calculation works and length of result is length of the longest values list
            const res = nub2.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(longest);
            checkNanUndef(res);
        });

        it("test 8 : case of #115", () => {
            Session.getInstance().clear();
            // tslint:disable-next-line:max-line-length
            const sess = `{"header":{"source":"jalhyd","format_version":"1.0","created":"2019-06-03T12:45:20.774Z"},"session":[{"uid":"amd2OG","props":{"calcType":8,"nodeType":0},"meta":{"title":"Ouvrages"},"children":[{"uid":"YzNhaT","props":{"calcType":7,"nodeType":5,"structureType":0,"loiDebit":2},"children":[],"parameters":[{"symbol":"ZDV","mode":"LINK","targetNub":"anZjdD","targetParam":"ZDV"},{"symbol":"L","mode":"SINGLE","value":2}]}],"parameters":[{"symbol":"Q","mode":"LINK","targetNub":"aTgwMm","targetParam":"Q"},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"MINMAX","min":100,"max":102,"step":0.05}]},{"uid":"aTgwMm","props":{"calcType":8,"nodeType":0},"meta":{"title":"Ouvrages 1"},"children":[{"uid":"anZjdD","props":{"calcType":7,"nodeType":5,"structureType":0,"loiDebit":8},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":2},{"symbol":"alpha","mode":"SINGLE","value":0.4},{"symbol":"beta","mode":"SINGLE","value":0.001},{"symbol":"ZRAM","mode":"SINGLE","value":100}]}],"parameters":[{"symbol":"Q","mode":"MINMAX","min":1,"max":20,"step":0.1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101.5}]}]}`;
            Session.getInstance().unserialise(sess);
            const ouvrages = Session.getInstance().findNubByUid("amd2OG");
            const prmsO = ouvrages.prms as ParallelStructureParams;

            const ouvrages2 = Session.getInstance().findNubByUid("aTgwMm");
            const prmsO2 = ouvrages2.prms as ParallelStructureParams;

            // check varying parameters : slave Nub
            expect(prmsO2.Q.hasMultipleValues).toBe(true);
            expect(prmsO2.Q.getInferredValuesList().length).toBe(191);

            // check varying parameters : calculated Nub
            expect(prmsO.Z2.hasMultipleValues).toBe(true);
            expect(prmsO.Z2.getInferredValuesList().length).toBe(41);
            expect(prmsO.Q.hasMultipleValues).toBe(true);
            expect(prmsO.Q.getInferredValuesList().length).toBe(191);

            // check that calculation works
            const res = ouvrages.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(191);
            checkNanUndef(res);
        });
    });

    describe("variated linked result and variated param at the same time - ", () => {

        it("test 1 : param < result, repeat", () => {
            createLinkedNubEnv2();
            prms3.L.setValues(1, 4, 0.5); // 7 values
            prms3.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prms4.Q.setValues(0.05, 0.2, 0.1); // 3 values
            prms4.Q.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that calculation works
            const res = nub4.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(7);
            checkNanUndef(res);
            // check absence of warning message
            expect(res.globalLog.messages.length).toBe(0);
        });

        it("test 2 : param < result, recycle", () => {
            createLinkedNubEnv2();
            prms3.L.setValues(1, 4, 0.5); // 7 values
            prms3.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prms4.Q.setValues(0.05, 0.2, 0.1); // 3 values
            prms4.Q.extensionStrategy = ExtensionStrategy.RECYCLE;
            // check that calculation works
            const res = nub4.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(7);
            checkNanUndef(res);
            // check absence of warning message
            expect(res.globalLog.messages.length).toBe(0);
        });

        it("test 3 : param > result", () => {
            createLinkedNubEnv2();
            prms3.L.setValues(1, 4, 0.5); // 7 values
            prms3.L.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            prms4.Q.setValues(0.05, 0.9, 0.1); // 10 values
            prms4.Q.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
            // check that calculation works
            const res = nub4.CalcSerie();
            expect(res).toBeDefined();
            expect(res.resultElements.length).toBe(7);
            checkNanUndef(res);
            // check presence of warning message
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT);
            expect(res.globalLog.messages[0].extraVar.symbol).toBe("V");
            expect(res.globalLog.messages[0].extraVar.size).toBe(7);
        });

    });
});

// check there is no NaN or undefined among calculated values
function checkNanUndef(res: Result) {
    for (const re of res.resultElements) {
        expect(re.vCalc).toBeDefined();
        expect(re.vCalc).not.toBeNaN();
    }
}
