import { MessageCode, ParamDefinition, ParamDomainValue, ExtensionStrategy, Session, Cloisons } from "../../src/index";
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";

describe("when a parameter is varying, ", () => {

    describe("when some iterations have log messages, ", () => {

        it("general log should show an abstract message", () => {
            const prms = new PabChuteParams(2, 1, 0.1);
            const chute = new PabChute(prms);
            chute.calculatedParam = prms.DH;
            prms.Z2.setValues(1, 4.2, 0.8); // should lead to 2 valid iterations and 3 errors
            const res = chute.CalcSerie();
            // check local logs
            expect(res.resultElements[0].log.messages.length).toBe(0);
            expect(res.resultElements[1].log.messages.length).toBe(0);
            expect(res.resultElements[2].log.messages.length).toBe(1);
            expect(res.resultElements[3].log.messages.length).toBe(1);
            expect(res.resultElements[4].log.messages.length).toBe(1);
            // check global log
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_ERRORS_ABSTRACT_PLUR);
            expect(res.globalLog.messages[0].extraVar.nb).toBe("3"); // number is a string here
        });
    });

    it("check inferredValuesList, repeat mode", () => {
        const foo = new ParamDefinition(undefined, "FOO", ParamDomainValue.ANY);
        foo.setValues(0.05, 0.2, 0.1); // 3 values, (max - min) not a multiple of step
        foo.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        // check recycled values list
        const expected = [ 0.05, 0.15, 0.2, 0.2, 0.2, 0.2, 0.2 ];
        const values = foo.getInferredValuesList(7);
        for (let i = 0; i < values.length; i++) {
            expect(values[i]).toBeCloseTo(expected[i], 2, `values[${i}]`);
        }
    });

    it("check inferredValuesList, recycle mode", () => {
        const foo = new ParamDefinition(undefined, "FOO", ParamDomainValue.ANY);
        foo.setValues(0.05, 0.2, 0.1); // 3 values, (max - min) not a multiple of step
        foo.extensionStrategy = ExtensionStrategy.RECYCLE;
        // check recycled values list
        const expected = [ 0.05, 0.15, 0.2, 0.05, 0.15, 0.2, 0.05 ];
        const values = foo.getInferredValuesList(7);
        for (let i = 0; i < values.length; i++) {
            expect(values[i]).toBeCloseTo(expected[i], 2, `values[${i}]`);
        }
    });

    it("jalhyd#222 iterator should not be shared by linked parameter", () => {
        const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-14T12:32:41.289Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"ODBiMG","props":{"calcType":"ParallelStructure"},"meta":{"title":"Orifice -> seuil"},"children":[{"uid":"aGR2eW","props":{"calcType":"Structure","structureType":"VanneFondRectangulaire","loiDebit":"GateCunge80"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"MINMAX","min":0.7,"max":1.01,"step":0.01,"extensionStrategy":0},{"symbol":"L","mode":"SINGLE","value":1}]},{"uid":"dnhrNW","props":{"calcType":"Structure","structureType":"VanneFondRectangulaire","loiDebit":"GateCem88d"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"LINK","targetNub":"aGR2eW","targetParam":"W"},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":101},{"symbol":"Z2","mode":"SINGLE","value":100.8}]}]}`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const n = Session.getInstance().findNubByUid("ODBiMG") as Cloisons;
        const res = n.CalcSerie();
        expect(res.resultElements.length).toBe(32);
    });
});
