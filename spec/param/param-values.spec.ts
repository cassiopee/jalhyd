import { ParamValueMode } from "../../src/param/param-value-mode";
import { ParamValues } from "../../src/param/param-values";

describe("paramvalues : ", () => {
    it("check single (1)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.SINGLE;
        expect(() => p.check()).toThrow();
    });

    it("check single (1)", () => {
        const p = new ParamValues();
        p.setValues(0);
        expect(() => p.check()).not.toThrow();
    });

    it("check minmax (1)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.MINMAX;
        expect(() => p.check()).toThrow();
    });

    it("check minmax (2)", () => {
        const p = new ParamValues();
        p.setValues(0, -1, 1);
        expect(() => p.check()).toThrow();
    });

    it("check minmax (3)", () => {
        const p = new ParamValues();
        p.setValues(0, 1, 1);
        expect(() => p.check()).not.toThrow();
    });

    it("check liste (1)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.LISTE;
        expect(() => p.check()).toThrow();
    });

    it("check liste (2)", () => {
        const p = new ParamValues();
        p.setValues([]);
        expect(() => p.check()).not.toThrow();
    });

    it("check liste (3)", () => {
        const p = new ParamValues();
        p.setValues([0]);
        expect(() => p.check()).not.toThrow();
    });

    it("check calcul (1)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.CALCUL;
        expect(() => p.check()).not.toThrow();
    });

    it("check link (1)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.LINK;
        expect(() => p.check()).not.toThrow();
    });
});
