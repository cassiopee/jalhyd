import { CalculatorType } from "../src/compute-node";
import { Grille, ParamDomainValue, Session, SPP, YAXN, YAXNParams, Par, ParSimulation, PressureLoss, PL_LechaptCalmonParams, PL_LechaptCalmon, PL_StricklerParams, PL_Strickler } from "../src/index";
import { MacrorugoCompound } from "../src/macrorugo/macrorugo_compound";
import { Trigo, TrigoOperation } from "../src/math/trigo";
import { Nub } from "../src/nub";
import { SectionNub } from "../src/open-channel/section/section_nub";
import { CloisonAval } from "../src/pab/cloison_aval";
import { Pab } from "../src/pab/pab";
import { ParamDefinition } from "../src/param/param-definition";
import { PressureLossLaw, PressureLossType } from "../src/pipe_flow/pressureloss_law";
import { Props } from "../src/props";
import { CreateStructure } from "../src/structure/factory_structure";
import { ParallelStructure } from "../src/structure/parallel_structure";
import { checkResultConsistency, JasmineResult, SetJasmineCurrentSpec } from "./test_func";

/**
 * Fuzzing or fuzz testing is an automated software testing technique that involves providing invalid,
 * unexpected, or random data as inputs to a computer program.
 * Here each calculator is fed with random parameters and each parameter is calculated.
 * We inspect the result that should return finite numbers or at least one error message.
 */

/**
 * Number of test entries by Calculator (Each parameter is calculated for each test entrie)
 */
const fuzzyCfg = {
    nTests: 4, // Number of test entries by Calculator (Each parameter is calculated for each test entrie)
    propInvertedPrm: 0, // Number of parameters inverted (for getting very small numbers on a proportion of parameters)
    Pab: {
        poolMax: 10, // Maximum number of pools in a Pab
        structureMax: 3 // Maximum number of structures in each cloison
    },
    ParallelStructure: {
        structureMax: 1 // Maximum number of structure in ParallelStructure and its derivated
    }
};

// Modules that should not be tested
const nubsNotTested: CalculatorType[] = [
    CalculatorType.Structure,
    CalculatorType.Section,
    CalculatorType.CloisonAval,
    CalculatorType.Solveur,
    CalculatorType.YAXN,
    CalculatorType.Verificateur,
    CalculatorType.Espece,
    CalculatorType.PbBassin,
    CalculatorType.PbCloison,
    CalculatorType.PreBarrage, // TODO: Add special treatments for creating fuzzy PreBarrage
    CalculatorType.LechaptCalmon,
    CalculatorType.PressureLossLaw
];

const nubsWithStructures: CalculatorType[] = [
    CalculatorType.ParallelStructure,
    CalculatorType.Dever,
    CalculatorType.Cloisons
];

const nubsWithSection: CalculatorType[] = [
    CalculatorType.RegimeUniforme,
    CalculatorType.SectionParametree,
    CalculatorType.CourbeRemous
];

const nubsWithPressureLossLaw: CalculatorType[] = [
    CalculatorType.PressureLoss
]

const calTypes =
    Object.keys(CalculatorType)
        .filter((key: any) =>
            isNaN(Number(CalculatorType[key]))
        ).map((e) => +e);

function randomizeParameter(p: ParamDefinition) {
    let min = p.domain.minValue;
    let max = p.domain.maxValue;
    // special case for SPP to avoid +/-Infinity with high powers
    if (p.parentNub instanceof YAXN && p.symbol === "N") {
        min = -10;
        max = 10;
    }
    // special case for Trigo to avoid +/-Infinity with cosh/sinh
    if (
        p.parentNub instanceof Trigo
        && p.symbol === "X"
        && [ TrigoOperation.COSH, TrigoOperation.SINH ].includes((p.parentNub as Trigo).operation)
    ) {
        min = -500;
        max = 500;
    }
    p.singleValue = Math.max(-1E6, min)
        + Math.random()
        * (
            Math.min(1E6, max)
            - Math.max(-1E6, min)
        );
    if (Math.random() < fuzzyCfg.propInvertedPrm) {
        p.singleValue = 1 / p.currentValue;
    }
    if (p.domain.domain === ParamDomainValue.INTEGER) {
        p.singleValue = Math.floor(p.singleValue);
    }
}

function printPrms(n: Nub): string {
    let s = "";
    if (n.calculatedParam !== undefined) {
        s = "Calc(" + n.calculatedParam.symbol + ") ";
    }
    for (const p of n.parameterIterator) {
        if (p.visible) {
            s += p.symbol + "=" + p.currentValue + " ";
        }
    }
    return s;
}

function addRandomStructures(ps: ParallelStructure, nStMax: number = 10) {
    const nSt = Math.floor(Math.random() * nStMax) + 1;
    for (let i = 0; i < nSt; i++) {
        const lois = ps.getLoisAdmissiblesArray();
        const rndLoi = lois[Math.floor(Math.random() * lois.length)];
        ps.addChild(CreateStructure(rndLoi));
    }
}

function setRandomSection(sn: SectionNub) {
    const newSect = Session.getInstance().createSection(
        Math.floor(Math.random() * 4)
    );
    sn.setSection(newSect);
}

function setRandomPressureLossLaw(pl: PressureLoss) {
    const pressureLossLaws = [PressureLossType.LechaptCalmon, PressureLossType.Strickler];
    const i = Math.floor(Math.random() * pressureLossLaws.length);
    let pll: PressureLossLaw;
    switch (pressureLossLaws[i]) {
        case PressureLossType.LechaptCalmon:
            const lcp = new PL_LechaptCalmonParams(1.863, 2, 5.33); // peu importe les valeurs, elles sont randomisées
            randomizeParameter(lcp.L);
            randomizeParameter(lcp.M);
            randomizeParameter(lcp.N);
            const lc = new PL_LechaptCalmon(lcp);
            pll = lc;
            break;

        case PressureLossType.Strickler:
            const sp = new PL_StricklerParams(1);
            randomizeParameter(sp.Ks);
            const s = new PL_Strickler(sp);
            pll = s;
            break;

        default:
            throw new Error("invalid pressure loss law");
    }
    pl.setLaw(pll);
}

function setRandomTrigoOperation(sn: Trigo) {
    const op = Math.floor(Math.random() * 6);
    sn.operation = op;
}

function setRandomTrigoUnit(sn: Trigo) {
    const un = Math.floor(Math.random() * 2);
    sn.unit = un;
}

function setRandomSppOperation(sn: SPP) {
    const op = Math.floor(Math.random() * 2);
    sn.operation = op;
}

function setRandomParType(sn: Par | ParSimulation) {
    const pt = Math.floor(Math.random() * 4);
    sn.parType = pt;
}

function addRandomYAXNs(n: SPP, nYMax: number = 3) {
    const nY = Math.floor(Math.random() * nYMax) + 1;
    for (let i = 0; i < nY; i++) {
        n.addChild(
            new YAXN(
                new YAXNParams(Math.random() * 10, Math.random() * 10, Math.floor(Math.random() * 10))
            )
        );
    }
}

function setPab(pab: Pab, nClMax = 30, nStMax = 3) {
    const nCl = Math.floor(Math.random() * nClMax) + 1;
    for (let i = 0; i < nCl; i++) {
        pab.addChild(Session.getInstance().createNub(new Props({ calcType: CalculatorType.Cloisons })));
        addRandomStructures(pab.children[pab.children.length - 1], nStMax);
    }
    pab.downWall = Session.getInstance().createNub(new Props({ calcType: CalculatorType.CloisonAval })) as CloisonAval;
    addRandomStructures(pab.downWall, nStMax);
}

function setMacrorugoCompound(n: MacrorugoCompound) {
    n.setPropValue("inclinedApron", Math.floor(Math.random() * 2));
}

function setGrille(g: Grille) {
    const type = Math.floor(Math.random() * 3);
    const profile = Math.floor(Math.random() * 2);
    g.type = type;
    g.profile = profile;
}

function CreateTestNub(iCalType: number): Nub {
    const n = Session.getInstance().createNub(
        new Props({ calcType: iCalType, pressureLossType: PressureLossType.LechaptCalmon })
    );
    if (nubsWithStructures.includes(iCalType)) {
        addRandomStructures(n as ParallelStructure, fuzzyCfg.ParallelStructure.structureMax);
    }
    if (nubsWithSection.includes(iCalType)) {
        setRandomSection(n as SectionNub);
    }
    if (nubsWithPressureLossLaw.includes(iCalType)) {
        setRandomPressureLossLaw(n as PressureLoss);
    }
    if (iCalType === CalculatorType.CourbeRemous) {
        n.setPropValue(
            "methodeResolution",
            Math.floor(Math.random() * 3) // Euler, RK4, Trapèzes
        );
    }
    if (iCalType === CalculatorType.Pab) {
        setPab(n as Pab, fuzzyCfg.Pab.poolMax, fuzzyCfg.Pab.structureMax);
    }
    if (iCalType === CalculatorType.MacroRugoCompound) {
        setMacrorugoCompound(n as MacrorugoCompound);
    }
    if (iCalType === CalculatorType.Grille) {
        setGrille(n as Grille);
    }
    if (iCalType === CalculatorType.Bief) {
        setRandomSection(n as SectionNub);
    }
    if (iCalType === CalculatorType.Trigo) {
        setRandomTrigoOperation(n as Trigo);
        setRandomTrigoUnit(n as Trigo);
    }
    if (iCalType === CalculatorType.SPP) {
        addRandomYAXNs(n as SPP);
        setRandomSppOperation(n as SPP);
    }
    if (iCalType === CalculatorType.Par) {
        setRandomParType(n as Par);
    }
    if (iCalType === CalculatorType.ParSimulation) {
        setRandomParType(n as ParSimulation);
    }
    for (const p of n.parameterIterator) {
        if (p.visible) {
            randomizeParameter(p);
        }
    }
    return n;
}

SetJasmineCurrentSpec();

const nubs: Nub[] = [];
let iNub: number = 0;
let bHouston: boolean = false; // Houston, we have a problem...
describe("Fuzz testing", () => {
    beforeAll(() => {
        iNub = 0;
        Session.getInstance().clear();
    });
    afterAll(() => {
        if (bHouston) {
            const sessionJson = Session.getInstance().serialise();
            // tslint:disable-next-line:no-console
            console.debug(sessionJson);
        }
    });
    for (const iCalType of calTypes) {
        if (!nubsNotTested.includes(iCalType) /* && iCalType === CalculatorType.Trigo */) {
            describe(CalculatorType[iCalType], () => {
                for (let i = 0; i < fuzzyCfg.nTests; i++) {
                    describe(`Test ${i}`, () => {
                        beforeAll(() => {
                            // tslint:disable-next-line:no-console
                            /* console.debug(
                                `iNub=${iNub} Class=${nubs[iNub].constructor.name}`
                                + ` Type=${CalculatorType[nubs[iNub].calcType]} uid=${nubs[iNub].uid}`
                            ); */
                        });
                        afterAll(() => {
                            iNub++;
                        });
                        nubs.push(CreateTestNub(iCalType));
                        // List available calculable parameters
                        const calcPrms: ParamDefinition[] = nubs[iNub].calculableParameters;
                        if (calcPrms.length === 0) {
                            // No calculable parameter such as in Section  Paramétrée
                            // Use one fake parameter for running the thing once
                            for (const p of nubs[iNub].parameterIterator) {
                                calcPrms.push(p);
                                break;
                            }
                        }
                        for (const p of calcPrms) {
                            describe(`Calc(${p.symbol})`, () => {
                                p.setCalculated();
                                beforeAll(() => {
                                    p.setCalculated();
                                });
                                afterEach(() => {
                                    const currentSpec: any = JasmineResult.currentResult;
                                    if (currentSpec.failedExpectations.length > 0) {
                                        Session.getInstance().unserialiseSingleNub(nubs[iNub].serialise());
                                        bHouston = true;
                                    }
                                });
                                it(printPrms(nubs[iNub]), () => {
                                    const r = nubs[iNub].CalcSerie();
                                    checkResultConsistency(nubs[iNub], r);
                                });
                            });
                        }
                    });
                    iNub++;
                }
            });
        }
    }
});
