import { ConduiteDistrib } from "../../src/pipe_flow/cond_distri";
import { ConduiteDistribParams } from "../../src/pipe_flow/cond_distri_params";
import { checkResult } from "../test_func";

describe("Class ConduiteDistrib: ", () => {
    // beforeEach(() => {
    // });
    // beforeAll(() => {
    // });

    describe("Calc(): ", () => {
        it("Q should be 9.393", () => {
            const prms = new ConduiteDistribParams(undefined, // débit Q
                1.2, // diamètre D
                0.6, // perte de charge J
                100, // Longueur de la conduite Lg
                1e-6, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);

            checkResult(nub.Calc("Q", 0), 9.393);
        });
    });

    describe("Calc(): ", () => {
        it("Q should be 152.992", () => {
            const prms = new ConduiteDistribParams(undefined, // débit Q
                2, // diamètre D
                0.7, // perte de charge J
                10, // Longueur de la conduite Lg
                1e-6, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);
            nub.prms.D.v = 2;
            nub.prms.J.v = 0.7;
            nub.prms.Lg.v = 10;
            nub.prms.Nu.v = 1e-6;

            checkResult(nub.Calc("Q", 0), 152.992);
        });
    });

    describe("Calc(): ", () => {
        it("D should be 2.12847", () => {
            const prms = new ConduiteDistribParams(3, // débit Q
                undefined, // diamètre D
                0.7, // perte de charge J
                10, // Longueur de la conduite Lg
                1e-6, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);

            const r = nub.Calc("D", 0);
            checkResult(r, 2.12847);
        });
    });

    describe("Calc(): ", () => {
        it("J should be 0.00814", () => {
            const prms = new ConduiteDistribParams(3, // débit Q
                1.2, // diamètre D
                undefined, // perte de charge J
                10, // Longueur de la conduite Lg
                1e-6, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);

            checkResult(nub.Calc("J", 0), 0.00814);
        });
    });

    describe("Calc(): ", () => {
        it("Lg should be 737.021", () => {
            const prms = new ConduiteDistribParams(3, // débit Q
                1.2, // diamètre D
                0.6, // perte de charge J
                undefined, // Longueur de la conduite Lg
                1e-6, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);

            checkResult(nub.Calc("Lg", 0), 737.021);
        });
    });

    describe("Calc(): ", () => {
        it("Nu should be 0.00295", () => {
            const prms = new ConduiteDistribParams(3, // débit Q
                1.2, // diamètre D
                0.6, // perte de charge J
                100, // Longueur de la conduite Lg
                undefined, // Viscosité dynamique Nu
            );

            const nub = new ConduiteDistrib(prms);

            checkResult(nub.Calc("Nu", 0), 0.00295);
        });
    });
});
