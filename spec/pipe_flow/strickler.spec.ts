// tslint:disable:no-console
import { PressureLoss, SessionSettings } from "../../src/internal_modules";
import { PL_Strickler, PL_StricklerParams } from "../../src/internal_modules";
import { PressureLossParams } from "../../src/internal_modules";

let oldMaxIter: number;

describe("Loi de perte de charge Strickler : ", () => {

    beforeAll(() => {
        oldMaxIter = SessionSettings.maxIterations;
        SessionSettings.maxIterations = 100;
    });

    afterAll(() => {
        SessionSettings.maxIterations = oldMaxIter;
    });

    beforeEach(() => {
        SessionSettings.precision = 1e-7; // précision
    });

    describe("Conduite d'égout, S. Bennis 'Hydraulique et hydrologie', exercice 6 :", () => {
        it("à t=0", () => {
            const plParams = new PressureLossParams(
                0.2, // débit Q
                0.61, // diamètre D
                0.075, /// perte de charge J
                100, // longueur du toyo Lg
                0 // coef de perte de charge singulière Kloc
            );

            const stricklerParams = new PL_StricklerParams(
                1 // coefficient de rugosité de Strickler Ks
            );
            const plStrickler = new PL_Strickler(stricklerParams);
            const pl: PressureLoss = new PressureLoss(plParams, plStrickler);
            pl.calculatedParam = stricklerParams.Ks;

            const res = pl.CalcSerie();
            expect(res.vCalc).toBeCloseTo(87.546, 3);
        });

        it("à t=30 ans", () => {
            const plParams = new PressureLossParams(
                0.2, // débit Q
                0.61, // diamètre D
                0.5, /// perte de charge J
                100, // longueur du toyo Lg
                0 // coef de perte de charge singulière Kloc
            );

            const stricklerParams = new PL_StricklerParams(
                1 // coefficient de rugosité de Strickler Ks
            );
            const plStrickler = new PL_Strickler(stricklerParams);
            const pl: PressureLoss = new PressureLoss(plParams, plStrickler);
            pl.calculatedParam = stricklerParams.Ks;

            const res = pl.CalcSerie();
            expect(res.vCalc).toBeCloseTo(33.9064, 4);
        });

        it("tubage PVC", () => {
            const plParams = new PressureLossParams(
                0.2, // débit Q
                0.59, // diamètre D
                0.5, /// perte de charge J
                100, // longueur du toyo Lg
                0 // coef de perte de charge singulière Kloc
            );

            const stricklerParams = new PL_StricklerParams(
                111.111 // coefficient de rugosité de Strickler Ks
            );
            const plStrickler = new PL_Strickler(stricklerParams);
            const pl: PressureLoss = new PressureLoss(plParams, plStrickler);
            pl.calculatedParam = plParams.J;

            const res = pl.CalcSerie();
            expect(res.vCalc).toBeCloseTo(0.05562, 5);
        });
    });
});
