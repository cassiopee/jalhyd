import { PressureLoss, PressureLossParams } from "../../src/internal_modules";
import { LCMaterial } from "../../src/lc-material";
import { ParamCalculability } from "../../src/param/param-definition";
import { PL_LechaptCalmon } from "../../src/pipe_flow/pl_lechaptcalmon";
import { PL_LechaptCalmonParams } from "../../src/pipe_flow/pl_lechaptcalmon_params";
import { SessionSettings } from "../../src/session_settings";
import { MessageCode } from "../../src/util/message";

function getLechapt(): PressureLoss {
    const plParams = new PressureLossParams(
        0.3, // débit
        0.5, // diamètre
        0.1, /// perte de charge
        20, // longueur du toyo
        1 // Kloc
    );
    const lc = new PL_LechaptCalmon(
        new PL_LechaptCalmonParams(
            1.863, // paramètre L du matériau
            2, // paramètre M du matériau
            5.33 // paramètre N du matériau
        )
    );
    const pl: PressureLoss = new PressureLoss(plParams, lc);

    pl.calculatedParam = pl.prms.J;
    pl.prms.J.singleValue = pl.CalcSerie().vCalc;
    return pl;
}

function getLechaptWith(Q: number, D: number, Lg: number, Kloc: number, mat: LCMaterial): PressureLoss {
    const plParams = new PressureLossParams(
        Q, // débit
        D, // diamètre
        0, /// perte de charge
        Lg, // longueur du toyo
        Kloc, // coef de perte de charge singulière
    );
    const lc = new PL_LechaptCalmon(
        new PL_LechaptCalmonParams(
            1.863, // paramètre L du matériau
            2, // paramètre M du matériau
            5.33 // paramètre N du matériau
        )
    );
    lc.material = mat;
    const pl: PressureLoss = new PressureLoss(plParams, lc);
    pl.calculatedParam = pl.prms.J;
    pl.prms.J.singleValue = pl.CalcSerie().vCalc;
    return pl;
}

let lechapt: PressureLoss = getLechapt();

describe("Class LechaptCalmon : ", () => {
    beforeAll(() => {
        SessionSettings.precision = 1E-7;
    });
    beforeEach(() => {
        lechapt = getLechapt();
    });
    for (const p of lechapt.parameterIterator) {
        if ([ParamCalculability.EQUATION, ParamCalculability.DICHO].includes(p.calculability) && p.visible) {
            it(`Calc(${p.symbol}) should return ${p.currentValue}`, () => {
                lechapt.calculatedParam = lechapt.getParameter(p.symbol);
                lechapt.CalcSerie();
                const ref: number = lechapt.result.vCalc;
                const V: number = lechapt.result.values.V;
                const Jlin: number = lechapt.child.result.values.Jlin;
                const Klin: number = lechapt.result.values.Klin;
                const fD: number = lechapt.result.values.fD;
                lechapt.calculatedParam.singleValue = lechapt.calculatedParam.singleValue / 2;
                expect(lechapt.CalcSerie().vCalc).toBeCloseTo(ref, 2);
                expect(lechapt.result.values.V).toBeCloseTo(V, 3);
                expect(lechapt.child.result.values.Jlin).toBeCloseTo(Jlin, 3);
                expect(lechapt.result.values.Klin).toBeCloseTo(Klin, 3);
                expect(lechapt.result.values.fD).toBeCloseTo(fD, 3);
            });
        }
    }

    describe("warning on speed value -", () => {

        it("case 1: there should not be any warning", () => {
            const res = lechapt.result;
            expect(res.log.messages.length).toBe(0);
        });

        it("case 2: there should be a warning", () => {
            lechapt.prms.Q.singleValue = 0.7;
            const res = lechapt.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2);
        });
    });
});

describe("article original", () => {
    it("", () => {
        const oldMaxIter = SessionSettings.maxIterations;
        const oldPrec = SessionSettings.precision;

        try {
            SessionSettings.maxIterations = 100;
            SessionSettings.precision = 1e-7;

            const plParams = new PressureLossParams(
                0.0085, // débit
                0.175, // diamètre
                0, /// perte de charge
                1000, // longueur du toyo
                0, // Kloc
            );

            // L,M,N : cf. pl_lechaptcalmon.ts
            const lechaptPrms = new PL_LechaptCalmonParams(
                1.863, // paramètre L du matériau
                2, // paramètre M du matériau
                5.33, // paramètre N du matériau
            );

            const lechapt = new PL_LechaptCalmon(lechaptPrms);
            lechapt.material = LCMaterial.HydraulicallySmoothPipe005D02;

            const pl: PressureLoss = new PressureLoss(plParams, lechapt);

            const res = pl.CalcSerie();
            expect(res.vCalc).toBeCloseTo(0.784, 3);
        }
        finally {
            SessionSettings.maxIterations = oldMaxIter;
            SessionSettings.precision = oldPrec;
        }
    });
});

describe("specific values -", () => {
    it("test 1", () => {
        const pl: PressureLoss = getLechaptWith(3, 1.2, 100, 0, LCMaterial.HydraulicallySmoothPipe025D1);
        let r = pl.result;
        let cr = pl.child.result;
        expect(r.vCalc).toBeCloseTo(0.295085, 6); // J
        expect(r.values.V).toBeCloseTo(2.652582, 6);
        expect(cr.values.Jlin).toBeCloseTo(0.295085, 6);
        expect(r.values.Klin).toBeCloseTo(0.822826, 6);
        expect(r.values.fD).toBeCloseTo(0.009874, 6);

        const lc: PL_LechaptCalmon = pl.child as PL_LechaptCalmon;
        lc.material = LCMaterial.UnlinedCastIronCoarseConcrete;
        pl.CalcSerie();
        r = pl.result;
        cr = pl.child.result;
        expect(r.vCalc).toBeCloseTo(0.634482, 6); // J
        expect(r.values.V).toBeCloseTo(2.652582, 6);
        expect(cr.values.Jlin).toBeCloseTo(0.634482, 6);
        expect(r.values.Klin).toBeCloseTo(1.769215, 6);
        expect(r.values.fD).toBeCloseTo(0.021231, 6);
    });

    it("test 2", () => {
        const pl: PressureLoss = getLechaptWith(0.05, 0.5, 100, 1, LCMaterial.RolledSteelSmoothConcrete);
        const r = pl.result;
        const cr = pl.child.result;
        expect(r.vCalc).toBeCloseTo(0.015625, 6); // J  
        expect(r.values.V).toBeCloseTo(0.254648, 6);
        expect(cr.values.Jlin).toBeCloseTo(0.012320, 6);
        expect(r.values.Klin).toBeCloseTo(3.727563, 6);
        expect(r.values.fD).toBeCloseTo(0.023638, 6);
    });
});
