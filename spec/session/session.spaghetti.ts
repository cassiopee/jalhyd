export const sessionSpaghetti = {
    header: {
        source: "jalhyd",
        format_version: "1.0",
        created: "2019-04-26T10:25:29.891Z"
    },
    session: [
        {
            uid: "ZHh1YW",
            props: {
                calcType: 2,
                nodeType: 1
            },
            meta: {
                title: "Sec. param."
            },
            children: [
                {
                    uid: "bXB1Y3",
                    props: {
                        calcType: 14,
                        nodeType: 1
                    },
                    parameters: [
                        {
                            symbol: "Ks",
                            mode: "SINGLE",
                            value: 40
                        },
                        {
                            symbol: "Q",
                            mode: "SINGLE",
                            value: 1.2
                        },
                        {
                            symbol: "If",
                            mode: "SINGLE",
                            value: 0.001
                        },
                        {
                            symbol: "YB",
                            mode: "SINGLE",
                            value: 1
                        },
                        {
                            symbol: "Y",
                            mode: "SINGLE",
                            value: 0.8
                        },
                        {
                            symbol: "LargeurBerge",
                            mode: "LINK",
                            targetNub: "YTBjcm",
                            targetParam: "L"
                        }
                    ]
                }
            ]
        },
        {
            uid: "eTgwMG",
            props: {
                calcType: 11,
                nodeType: 0
            },
            meta: {
                title: "Macro-rugo."
            },
            parameters: [
                {
                    symbol: "ZF1",
                    mode: "LINK",
                    targetNub: "dGx0em",
                    targetParam: "Z1"
                },
                {
                    symbol: "L",
                    mode: "SINGLE",
                    value: 6
                },
                {
                    symbol: "B",
                    mode: "CALCUL"
                },
                {
                    symbol: "If",
                    mode: "SINGLE",
                    value: 0.05
                },
                {
                    symbol: "Q",
                    mode: "LINK",
                    targetNub: "bXB1Y3",
                    targetParam: "Q"
                },
                {
                    symbol: "Y",
                    mode: "SINGLE",
                    value: 0.6
                },
                {
                    symbol: "Ks",
                    mode: "SINGLE",
                    value: 0.01
                },
                {
                    symbol: "C",
                    mode: "SINGLE",
                    value: 0.05
                },
                {
                    symbol: "PBD",
                    mode: "SINGLE",
                    value: 0.5
                },
                {
                    symbol: "PBH",
                    mode: "MINMAX",
                    min: 0.4,
                    max: 1.6,
                    step: 0.06000000000000001
                },
                {
                    symbol: "Cd0",
                    mode: "SINGLE",
                    value: 1.5
                }
            ]
        },
        {
            uid: "dGx0em",
            props: {
                calcType: 8,
                nodeType: 0
            },
            meta: {
                title: "Ouvrages"
            },
            children: [
                {
                    uid: "ZmZ3bX",
                    props: {
                        calcType: 7,
                        nodeType: 4,
                        structureType: 1,
                        loiDebit: 1
                    },
                    parameters: [
                        {
                            symbol: "ZDV",
                            mode: "SINGLE",
                            value: 100
                        },
                        {
                            symbol: "W",
                            mode: "SINGLE",
                            value: 0.5
                        },
                        {
                            symbol: "L",
                            mode: "CALCUL"
                        },
                        {
                            symbol: "CdGR",
                            mode: "SINGLE",
                            value: 0.6
                        }
                    ]
                },
                {
                    uid: "aWo0M2",
                    props: {
                        calcType: 7,
                        nodeType: 4,
                        structureType: 1,
                        loiDebit: 1
                    },
                    parameters: [
                        {
                            symbol: "ZDV",
                            mode: "SINGLE",
                            value: 100
                        },
                        {
                            symbol: "W",
                            mode: "SINGLE",
                            value: 0.5
                        },
                        {
                            symbol: "L",
                            mode: "LINK",
                            targetNub: "YTBjcm",
                            targetParam: "L"
                        },
                        {
                            symbol: "CdGR",
                            mode: "SINGLE",
                            value: 0.6
                        }
                    ]
                },
                {
                    uid: "YTBjcm",
                    props: {
                        calcType: 7,
                        nodeType: 4,
                        structureType: 1,
                        loiDebit: 1
                    },
                    parameters: [
                        {
                            symbol: "ZDV",
                            mode: "SINGLE",
                            value: 100
                        },
                        {
                            symbol: "W",
                            mode: "LINK",
                            targetNub: "aWo0M2",
                            targetParam: "W"
                        },
                        {
                            symbol: "L",
                            mode: "SINGLE",
                            value: 2
                        },
                        {
                            symbol: "CdGR",
                            mode: "LINK",
                            targetNub: "ZmZ3bX",
                            targetParam: "CdGR"
                        }
                    ]
                }
            ],
            parameters: [
                {
                    symbol: "Q",
                    mode: "SINGLE",
                    value: 0.5
                },
                {
                    symbol: "Z1",
                    mode: "SINGLE",
                    value: 102
                },
                {
                    symbol: "Z2",
                    mode: "LINK",
                    targetNub: "cXFraW",
                    targetParam: "Z1"
                }
            ]
        },
        {
            uid: "dzA1OX",
            props: {
                calcType: 3,
                nodeType: 1
            },
            meta: {
                title: "R. uniforme"
            },
            children: [
                {
                    uid: "Ynlna2",
                    props: {
                        calcType: 14,
                        nodeType: 1
                    },
                    parameters: [
                        {
                            symbol: "Ks",
                            mode: "SINGLE",
                            value: 40
                        },
                        {
                            symbol: "If",
                            mode: "MINMAX",
                            min: 0.0005,
                            max: 0.002,
                            step: 0.00007500000000000001
                        },
                        {
                            symbol: "YB",
                            mode: "SINGLE",
                            value: 1
                        },
                        {
                            symbol: "Y",
                            mode: "SINGLE",
                            value: 0.8
                        },
                        {
                            symbol: "LargeurBerge",
                            mode: "CALCUL"
                        }
                    ]
                }
            ]
        },
        {
            uid: "cXFraW",
            props: {
                calcType: 9,
                nodeType: 0
            },
            meta: {
                title: "Déver. dénoyés"
            },
            children: [
                {
                    uid: "Zzd1cH",
                    props: {
                        calcType: 7,
                        nodeType: 4,
                        structureType: 0,
                        loiDebit: 7
                    },
                    parameters: [
                        {
                            symbol: "ZDV",
                            mode: "SINGLE",
                            value: 100
                        },
                        {
                            symbol: "L",
                            mode: "SINGLE",
                            value: 2
                        },
                        {
                            symbol: "CdWR",
                            mode: "SINGLE",
                            value: 0.4
                        }
                    ]
                }
            ],
            parameters: [
                {
                    symbol: "Q",
                    mode: "CALCUL"
                },
                {
                    symbol: "Z1",
                    mode: "SINGLE",
                    value: 102
                },
                {
                    symbol: "BR",
                    mode: "LINK",
                    targetNub: "ZHh1YW",
                    targetParam: "B"
                },
                {
                    symbol: "ZR",
                    mode: "SINGLE",
                    value: 99
                }
            ]
        }
    ]
};
