export const session6Calc = {
    "session": [
        {
            "uid": "Zmpsej",
            "props": {
                "calcType": 5,
                "nodeType": 0
            },
            "meta": {
                "title": "PAB : dimensions"
            },
            "children": [],
            "parameters": [
                {
                    "symbol": "L",
                    "mode": "SINGLE",
                    "value": 2
                },
                {
                    "symbol": "W",
                    "mode": "SINGLE",
                    "value": 1
                },
                {
                    "symbol": "Y",
                    "mode": "SINGLE",
                    "value": 0.5
                },
                {
                    "symbol": "V",
                    "mode": "CALCUL"
                }
            ]
        },
        {
            "uid": "djBoZ3",
            "props": {
                "calcType": 11,
                "nodeType": 0
            },
            "meta": {
                "title": "Macro-rugo."
            },
            "children": [],
            "parameters": [
                {
                    "symbol": "ZF1",
                    "mode": "SINGLE",
                    "value": 12.5
                },
                {
                    "symbol": "L",
                    "mode": "SINGLE",
                    "value": 6
                },
                {
                    "symbol": "B",
                    "mode": "SINGLE",
                    "value": 1
                },
                {
                    "symbol": "If",
                    "mode": "SINGLE",
                    "value": 0.05
                },
                {
                    "symbol": "Q",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Y",
                    "mode": "SINGLE",
                    "value": 0.6
                },
                {
                    "symbol": "Ks",
                    "mode": "SINGLE",
                    "value": 0.01
                },
                {
                    "symbol": "C",
                    "mode": "SINGLE",
                    "value": 0.05
                },
                {
                    "symbol": "PBD",
                    "mode": "SINGLE",
                    "value": 0.5
                },
                {
                    "symbol": "PBH",
                    "mode": "SINGLE",
                    "value": 0.8
                },
                {
                    "symbol": "Cd0",
                    "mode": "SINGLE",
                    "value": 1.5
                }
            ]
        },
        {
            "uid": "dG1hZD",
            "props": {
                "calcType": 8,
                "nodeType": 0
            },
            "meta": {
                "title": "Ouvrages"
            },
            "children": [
                {
                    "uid": "OW1yMH",
                    "props": {
                        "calcType": 7,
                        "nodeType": 5,
                        "structureType": 1,
                        "loiDebit": 1
                    },
                    "children": [] as any[],
                    "parameters": [
                        {
                            "symbol": "ZDV",
                            "mode": "SINGLE",
                            "value": 100
                        },
                        {
                            "symbol": "W",
                            "mode": "SINGLE",
                            "value": 0.5
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 2
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z1",
                    "mode": "SINGLE",
                    "value": 102
                },
                {
                    "symbol": "Z2",
                    "mode": "SINGLE",
                    "value": 101.5
                }
            ]
        },
        {
            "uid": "OWpqMW",
            "props": {
                "calcType": 2,
                "nodeType": 2
            },
            "meta": {
                "title": "Sec. param."
            },
            "children": [
                {
                    "uid": "bGVwZH",
                    "props": {
                        "calcType": 14,
                        "nodeType": 2
                    },
                    "children": [] as any[],
                    "parameters": [
                        {
                            "symbol": "Ks",
                            "mode": "SINGLE",
                            "value": 40
                        },
                        {
                            "symbol": "Q",
                            "mode": "SINGLE",
                            "value": 1.2
                        },
                        {
                            "symbol": "If",
                            "mode": "SINGLE",
                            "value": 0.001
                        },
                        {
                            "symbol": "YB",
                            "mode": "SINGLE",
                            "value": 1
                        },
                        {
                            "symbol": "Y",
                            "mode": "SINGLE",
                            "value": 0.8
                        },
                        {
                            "symbol": "LargeurBerge",
                            "mode": "SINGLE",
                            "value": 2.5
                        }
                    ]
                }
            ],
            "parameters": [] as any[]
        },
        {
            "uid": "dXNzYW",
            "props": {
                "methodeResolution": 0,
                "calcType": 4,
                "nodeType": 2
            },
            "meta": {
                "title": "Remous"
            },
            "children": [
                {
                    "uid": "aGFudW",
                    "props": {
                        "methodeResolution": 0,
                        "calcType": 14,
                        "nodeType": 2
                    },
                    "children": [] as any[],
                    "parameters": [
                        {
                            "symbol": "Ks",
                            "mode": "SINGLE",
                            "value": 40
                        },
                        {
                            "symbol": "Q",
                            "mode": "SINGLE",
                            "value": 1.2
                        },
                        {
                            "symbol": "If",
                            "mode": "SINGLE",
                            "value": 0.001
                        },
                        {
                            "symbol": "YB",
                            "mode": "SINGLE",
                            "value": 1
                        },
                        {
                            "symbol": "Y",
                            "mode": "SINGLE",
                            "value": 0.2863766123093061
                        },
                        {
                            "symbol": "LargeurBerge",
                            "mode": "SINGLE",
                            "value": 2.5
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Yamont",
                    "mode": "SINGLE",
                    "value": 0.15
                },
                {
                    "symbol": "Yaval",
                    "mode": "SINGLE",
                    "value": 0.4
                },
                {
                    "symbol": "Long",
                    "mode": "SINGLE",
                    "value": 100
                },
                {
                    "symbol": "Dx",
                    "mode": "SINGLE",
                    "value": 5
                }
            ]
        },
        {
            "uid": "dWpsMT",
            "props": {
                "calcType": 1,
                "nodeType": 0
            },
            "meta": {
                "title": "Lechapt-Calmon"
            },
            "children": [],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "SINGLE",
                    "value": 3
                },
                {
                    "symbol": "D",
                    "mode": "SINGLE",
                    "value": 1.2
                },
                {
                    "symbol": "J",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Lg",
                    "mode": "SINGLE",
                    "value": 100
                },
                {
                    "symbol": "L",
                    "mode": "SINGLE",
                    "value": 1.601
                },
                {
                    "symbol": "M",
                    "mode": "SINGLE",
                    "value": 1.975
                },
                {
                    "symbol": "N",
                    "mode": "SINGLE",
                    "value": 5.25
                }
            ]
        }
    ]
};
