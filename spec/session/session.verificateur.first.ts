export const sessionVerificateurFirst = {
    header: {
        source: "jalhyd",
        format_version: "1.3",
        created: "2020-04-21T14:12:56.429Z"
    },
    settings: {
        precision: 1e-7,
        maxIterations: 100,
        displayPrecision: 3
    },
    documentation: "",
    session: [
        {
            uid: "N2RkeD",
            props: {
                calcType: "Verificateur",
                nubToVerify: "MG45Yj",
                speciesList: [
                    "SPECIES_2",
                    "SPECIES_6",
                    "SPECIES_7b",
                    "Nms4Nn"
                ]
            },
            meta: {
                title: "Vérification"
            }
        },
        {
            uid: "bnhvZH",
            props: {
                calcType: "MacroRugo"
            },
            meta: {
                title: "Macro-rugo."
            },
            parameters: [
                {
                    symbol: "ZF1",
                    mode: "SINGLE",
                    value: 12.5
                },
                {
                    symbol: "L",
                    mode: "SINGLE",
                    value: 6
                },
                {
                    symbol: "B",
                    mode: "SINGLE",
                    value: 1
                },
                {
                    symbol: "If",
                    mode: "SINGLE",
                    value: 0.05
                },
                {
                    symbol: "Q",
                    mode: "CALCUL"
                },
                {
                    symbol: "Y",
                    mode: "SINGLE",
                    value: 0.6
                },
                {
                    symbol: "Ks",
                    mode: "SINGLE",
                    value: 0.01
                },
                {
                    symbol: "C",
                    mode: "SINGLE",
                    value: 0.13
                },
                {
                    symbol: "PBD",
                    mode: "SINGLE",
                    value: 0.4
                },
                {
                    symbol: "PBH",
                    mode: "SINGLE",
                    value: 0.4
                },
                {
                    symbol: "Cd0",
                    mode: "SINGLE",
                    value: 1.2
                }
            ]
        },
        {
            uid: "a3ozYm",
            props: {
                calcType: "Espece",
                species: "SPECIES_CUSTOM"
            },
            meta: {
                title: "Ablette"
            },
            parameters: [
                {
                    symbol: "OK",
                    mode: "CALCUL"
                },
                {
                    symbol: "DHMaxS",
                    mode: "SINGLE",
                    value: 0.35
                },
                {
                    symbol: "DHMaxP",
                    mode: "SINGLE",
                    value: 0.35
                },
                {
                    symbol: "BMin",
                    mode: "SINGLE",
                    value: 0.3
                },
                {
                    symbol: "PMinS",
                    mode: "SINGLE",
                    value: 1
                },
                {
                    symbol: "PMinP",
                    mode: "SINGLE",
                    value: 1
                },
                {
                    symbol: "LMinS",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "LMinP",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "HMin",
                    mode: "SINGLE",
                    value: 0.3
                },
                {
                    symbol: "YMin",
                    mode: "SINGLE",
                    value: 0.4
                },
                {
                    symbol: "VeMax",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "YMinSB",
                    mode: "SINGLE",
                    value: 0.2
                },
                {
                    symbol: "YMinPB",
                    mode: "SINGLE",
                    value: 0.3
                }
            ]
        },
        {
            uid: "MG45Yj",
            props: {
                calcType: "Pab"
            },
            meta: {
                title: "PAB"
            },
            children: [
                {
                    uid: "MXU1eX",
                    props: {
                        calcType: "Cloisons"
                    },
                    children: [
                        {
                            uid: "MzRnd2",
                            props: {
                                calcType: "Structure",
                                loiDebit: "OrificeSubmerged"
                            },
                            parameters: [
                                {
                                    symbol: "S",
                                    mode: "SINGLE",
                                    value: 0.1
                                },
                                {
                                    symbol: "CdO",
                                    mode: "SINGLE",
                                    value: 0.7
                                }
                            ]
                        }
                    ],
                    parameters: [
                        {
                            symbol: "LB",
                            mode: "SINGLE",
                            value: 10
                        },
                        {
                            symbol: "BB",
                            mode: "SINGLE",
                            value: 1
                        },
                        {
                            symbol: "ZRMB",
                            mode: "SINGLE",
                            value: 0
                        },
                        {
                            symbol: "ZRAM",
                            mode: "SINGLE",
                            value: 0
                        },
                        {
                            symbol: "QA",
                            mode: "SINGLE",
                            value: 0
                        }
                    ]
                }
            ],
            parameters: [
                {
                    symbol: "Q",
                    mode: "SINGLE",
                    value: 1.5
                },
                {
                    symbol: "Z1",
                    mode: "CALCUL"
                },
                {
                    symbol: "Z2",
                    mode: "SINGLE",
                    value: 99
                }
            ],
            downWall: {
                uid: "cXlrbH",
                props: {
                    calcType: "CloisonAval"
                },
                children: [
                    {
                        uid: "cmJ2ZT",
                        props: {
                            calcType: "Structure",
                            loiDebit: "OrificeSubmerged"
                        },
                        parameters: [
                            {
                                symbol: "S",
                                mode: "SINGLE",
                                value: 0.1
                            },
                            {
                                symbol: "CdO",
                                mode: "SINGLE",
                                value: 0.7
                            }
                        ]
                    }
                ],
                parameters: [
                    {
                        symbol: "ZRAM",
                        mode: "SINGLE",
                        value: 0
                    }
                ]
            }
        },
        {
            uid: "Nms4Nn",
            props: {
                calcType: "Espece",
                species: "SPECIES_CUSTOM"
            },
            meta: {
                title: "Truite"
            },
            parameters: [
                {
                    symbol: "OK",
                    mode: "CALCUL"
                },
                {
                    symbol: "DHMaxS",
                    mode: "SINGLE",
                    value: 0.35
                },
                {
                    symbol: "DHMaxP",
                    mode: "SINGLE",
                    value: 0.35
                },
                {
                    symbol: "BMin",
                    mode: "SINGLE",
                    value: 0.3
                },
                {
                    symbol: "PMinS",
                    mode: "SINGLE",
                    value: 1
                },
                {
                    symbol: "PMinP",
                    mode: "SINGLE",
                    value: 1
                },
                {
                    symbol: "LMinS",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "LMinP",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "HMin",
                    mode: "SINGLE",
                    value: 0.3
                },
                {
                    symbol: "YMin",
                    mode: "SINGLE",
                    value: 0.4
                },
                {
                    symbol: "VeMax",
                    mode: "SINGLE",
                    value: 2.5
                },
                {
                    symbol: "YMinSB",
                    mode: "SINGLE",
                    value: 0.2
                },
                {
                    symbol: "YMinPB",
                    mode: "SINGLE",
                    value: 0.3
                }
            ]
        }
    ]
};
