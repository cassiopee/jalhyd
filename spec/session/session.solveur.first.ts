export const sessionSolveurFirst = {
    header: {
        source: "jalhyd",
        format_version: "1.3",
        created: "2019-10-21T07:03:04.090Z"
    },
    settings: {
        precision: 0.0001,
        maxIterations: 100,
        displayPrecision: 3
    },
    documentation: "",
    session: [
        {
            uid: "ODM0Z2",
            props: {
                calcType: "Solveur",
                nubToCalculate: "OHBpcz",
                searchedParameter: "NjRvcG/Z1"
            },
            meta: {
                title: "Solveur"
            },
            parameters: [
                {
                    symbol: "Xinit",
                    mode: "SINGLE",
                    value: 1.9
                },
                {
                    symbol: "Ytarget",
                    mode: "SINGLE",
                    value: 324
                }
            ]
        },
        {
            uid: "NjRvcG",
            props: {
                calcType: "PabChute"
            },
            meta: {
                title: "PAB : chute"
            },
            parameters: [
                {
                    symbol: "Z1",
                    mode: "SINGLE",
                    value: 2.1513761467889907
                },
                {
                    symbol: "Z2",
                    mode: "SINGLE",
                    value: 0.5
                },
                {
                    symbol: "DH",
                    mode: "CALCUL"
                }
            ]
        },
        {
            uid: "dnV4bD",
            props: {
                calcType: "PabNombre"
            },
            meta: {
                title: "PAB : nombre"
            },
            parameters: [
                {
                    symbol: "DHT",
                    mode: "LINK",
                    targetNub: "NjRvcG",
                    targetParam: "DH"
                },
                {
                    symbol: "N",
                    mode: "SINGLE",
                    value: 10
                },
                {
                    symbol: "DH",
                    mode: "CALCUL"
                }
            ]
        },
        {
            uid: "OHBpcz",
            props: {
                calcType: "PabPuissance"
            },
            meta: {
                title: "PAB : puissance"
            },
            parameters: [
                {
                    symbol: "DH",
                    mode: "LINK",
                    targetNub: "dnV4bD",
                    targetParam: "DH"
                },
                {
                    symbol: "Q",
                    mode: "SINGLE",
                    value: 0.1
                },
                {
                    symbol: "V",
                    mode: "SINGLE",
                    value: 0.5
                },
                {
                    symbol: "PV",
                    mode: "CALCUL"
                }
            ]
        }
    ]
};
