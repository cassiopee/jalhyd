import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { Nub } from "../../src/nub";
import { MethodeResolution } from "../../src/open-channel/methode-resolution";
import { RegimeUniforme } from "../../src/open-channel/regime_uniforme";
import { CourbeRemous } from "../../src/open-channel/remous";
import { CourbeRemousParams } from "../../src/open-channel/remous_params";
import { cSnCirc } from "../../src/open-channel/section/section_circulaire";
import { ParamsSectionCirc } from "../../src/open-channel/section/section_circulaire_params";
import { SectionParametree } from "../../src/open-channel/section/section_parametree";
import { cSnTrapez } from "../../src/open-channel/section/section_trapez";
import { ParamsSectionTrapez } from "../../src/open-channel/section/section_trapez_params";
import { Cloisons } from "../../src/pab/cloisons";
import { CloisonsParams } from "../../src/pab/cloisons_params";
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";
import { PabNombre } from "../../src/pab/pab_nombre";
import { PabNombreParams } from "../../src/pab/pab_nombre_params";
import { PabPuissance } from "../../src/pab/pab_puissance";
import { PabPuissanceParams } from "../../src/pab/pab_puissance_params";
import { ParamDefinition } from "../../src/param/param-definition";
import { ParamValueMode } from "../../src/param/param-value-mode";
import { ConduiteDistrib } from "../../src/pipe_flow/cond_distri";
import { ConduiteDistribParams } from "../../src/pipe_flow/cond_distri_params";
import { Session } from "../../src/session";
import { SessionSettings } from "../../src/session_settings";
import { Solveur } from "../../src/solveur/solveur";
import { SolveurParams } from "../../src/solveur/solveur_params";
import { Dever } from "../../src/structure/dever";
import { DeverParams } from "../../src/structure/dever_params";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { RectangularStructure } from "../../src/structure/rectangular_structure";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { LoiDebit } from "../../src/structure/structure_props";
import { StructureWeirSubmergedLarinier, } from "../../src/structure/structure_weir_submerged_larinier";
import { PreBarrage } from "../../src/prebarrage/pre_barrage";
import { PbCloison } from "../../src/prebarrage/pb_cloison";
import { PreBarrageParams } from "../../src/prebarrage/pre_barrage_params";
import { PbBassin } from "../../src/prebarrage/pb_bassin";
import { PbBassinParams } from "../../src/prebarrage/pb_bassin_params";
import { TriangularStructureParams } from "../../src/structure/structure_triangular_weir_params";
import { StructureOrificeFreeParams } from "../../src/structure/structure_orifice_free_params";

import { TriangularTruncStructureParams } from "../../src/structure/structure_triangular_trunc_weir_params";
import { CloisonsAvalParams } from "../../src/pab/cloison_aval_params";
import { Verificateur } from "../../src/verification/verificateur";
import { CloisonAval } from "../../src/pab/cloison_aval";
import { PabParams } from "../../src/pab/pab_params";
import { Pab } from "../../src/pab/pab";
import { EspeceParams } from "../../src/verification/espece_params";
import { Espece } from "../../src/verification/espece";

import { sessionSolveurFirst } from "./session.solveur.first";
import { sessionSolveurLast } from "./session.solveur.last";
import { sessionSpaghetti } from "./session.spaghetti";
import { sessionVerificateurLast } from "./session.verificateur.last";
import { sessionVerificateurFirst } from "./session.verificateur.first";
import { session6Calc } from "./session-6-calc";
import { Structure } from "../../src/structure/structure";
import { createMacrorugoRemousTest } from "../macrorugo/macrorugo_util";
import { CalculatorType, MacrorugoRemous } from "../../src/internal_modules";

let dever: Dever;
let cloisons: Cloisons;
let conduite: ConduiteDistrib;
let rem: CourbeRemous;
let sp: SectionParametree;

function createEnv() {
    // create complex session
    dever = new Dever(
        new DeverParams(
            0,     // rQ Débit total (m3/s)
            102,   // rZ1 Cote de l'eau amont (m)
            2,     // rBR Largeur du cours d'eau amont (m)
            100    // rZR Cote du lit du cours d'eau amont (m)
        ),
        false // debug
    );
    dever.addChild(CreateStructure(LoiDebit.TriangularTruncWeirFree, dever, false));

    cloisons = new Cloisons(
        new CloisonsParams(
            0,      // Débit total (m3/s)
            102,    // Cote de l'eau amont (m)
            10,     // Longueur des bassins (m)
            1,      // Largeur des bassins (m)
            1,      // Profondeur moyenne (m)
            0.5     // Hauteur de chute (m)
        ),
        false       // debug
    );
    const fente: StructureWeirSubmergedLarinier = new StructureWeirSubmergedLarinier(
        new RectangularStructureParams(
            0,
            101,
            102,
            101.5,
            0.2,
            0.65
        )
    );
    cloisons.addChild(fente);

    const prmsCD = new ConduiteDistribParams(undefined, // débit Q
        1.2, // diamètre D
        0.6, // perte de charge J
        100, // Longueur de la conduite Lg
        1e-6, // Viscosité dynamique Nu
    );
    conduite = new ConduiteDistrib(prmsCD);

    const prmsST = new ParamsSectionTrapez(2.5, // largeur de fond
        0.56, // fruit
        undefined, // tirant d'eau
        40, //  Ks=Strickler
        2,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    const sect = new cSnTrapez(prmsST);
    const prem = new CourbeRemousParams(100.155, 100.803, 100.005, 100,
        5,  // Long= Longueur du bief
        5,  // Dx=Pas d'espace
    );
    rem = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);

    const paramSection = new ParamsSectionCirc(2, // diamètre
        0.8, // tirant d'eau
        40, //  Ks=Strickler
        10,  //  Q=Débit
        0.001, //  If=pente du fond
        1, // YB= hauteur de berge
    );
    const section = new cSnCirc(paramSection);
    sp = new SectionParametree(section);

    Session.getInstance().clear();
    Session.getInstance().registerNub(dever);
    Session.getInstance().registerNub(cloisons);
    Session.getInstance().registerNub(conduite);
    Session.getInstance().registerNub(rem);
    Session.getInstance().registerNub(sp);
}

function checkLink(param: ParamDefinition, nub: Nub, symbol: string) {
    expect(param.valueMode).toBe(ParamValueMode.LINK);
    expect(param.referencedValue).toBeDefined();
    if (param.referencedValue) {
        expect(param.referencedValue.nub.uid).toBe(nub.uid);
        expect(param.referencedValue.symbol).toBe(symbol);
    }
}

describe("serialising / deserialising session - ", () => {

    it("serialized file should contain 5 Nubs", () => {
        createEnv();
        const session = Session.getInstance().serialise();
        const js = JSON.parse(session);
        expect(Object.keys(js)).toContain("session");
        expect(js.session.length).toBe(5);
    });

    it("reloading serialized file should give 5 Nubs", () => {
        createEnv();
        const session = Session.getInstance().serialise();

        Session.getInstance().clear();
        expect(Session.getInstance().getNumberOfNubs()).toBe(0);

        Session.getInstance().unserialise(session);
        expect(Session.getInstance().getNumberOfNubs()).toBe(5);
    });

    it("serialized files should contain 8 LINK parameters", () => {
        createEnv();
        dever.addChild(CreateStructure(LoiDebit.RectangularOrificeFree, dever, false));
        // define links
        dever.prms.Z1.defineReference(rem, "Z2");
        (dever.structures[0].prms as TriangularTruncStructureParams).ZT.defineReference(cloisons, "Z1");
        cloisons.prms.BB.defineReference(rem.section, "LargeurFond");
        conduite.prms.D.defineReference(sp.section, "D");
        conduite.prms.Lg.defineReference(rem, "Long");
        rem.prms.ZF1.defineReference(cloisons, "Z1");
        rem.section.prms.Q.defineReference(dever, "Q");
        sp.section.prms.YB.defineReference(cloisons, "PB");

        // serialise / unserialise
        const session = Session.getInstance().serialise();
        const js = JSON.parse(session);
        expect(Object.keys(js)).toContain("session");
        expect(js.session.length).toBe(5);

        // check that links are still here and pointing to the right targets
        checkLink(dever.prms.Z1, rem, "Z2");
        checkLink((dever.structures[0].prms as TriangularTruncStructureParams).ZT, cloisons, "Z1");
        cloisons.prms.BB.defineReference(rem.section, "LargeurFond");
        checkLink(conduite.prms.D, sp.section, "D");
        checkLink(conduite.prms.Lg, rem, "Long");
        checkLink(rem.prms.ZF1, cloisons, "Z1");
        checkLink(rem.section.prms.Q, dever, "Q");
        checkLink(sp.section.prms.YB, cloisons, "PB");

        // count parameters in LINK mode in session file
        expect(session.split("LINK").length - 1).toBe(8);

        // serialise / unserialise, giving parent Nubs UIDs
        const options: any = {};
        options[dever.uid] = "Super déversoir";
        options[cloisons.uid] = "Super cloisons";
        options[conduite.uid] = "Super conduite";
        options[rem.uid] = "Super remous";
        options[sp.uid] = "Super section paramétrée";
        const session2 = Session.getInstance().serialise(options);
        // count parameters in LINK mode in session file
        expect(session2.split("LINK").length - 1).toBe(8);
    });

    it("when saving a SectionParametree, the edited values should be present in the file", () => {
        // d
        const paramSection = new ParamsSectionCirc(
            2, // diamètre
            0.8, // tirant d'eau
            40, //  Ks=Strickler
            10,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        const section = new cSnCirc(paramSection);
        const sp2 = new SectionParametree(section);
        sp2.section.prms.Ks.singleValue = 42;

        const serialised = sp2.serialise();
        expect(serialised).toContain('{"symbol":"Ks","mode":"SINGLE","value":42}');
    });

    it("loaded serialized RegimeUniforme should be calculable", () => {
        Session.getInstance().clear();
        // tslint:disable-next-line:max-line-length
        const session = `{"session":[{"uid":"YTEwZG","props":{"calcType":3,"nodeType":2},"meta":{"title":"R. uniforme"},"children":[{"uid":"NDcxN3","props":{"calcType":14,"nodeType":2},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"CALCUL"},{"symbol":"If","mode":"SINGLE","value":0.001},{"symbol":"YB","mode":"SINGLE","value":1},{"symbol":"Y","mode":"SINGLE","value":0.89},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.5}]}],"parameters":[]}]}`;
        Session.getInstance().unserialise(session);

        expect(Session.getInstance().getNumberOfNubs()).toBe(1);

        const RU = Session.getInstance().findNubByUid("YTEwZG");
        expect(RU).toBeDefined();

        const calculatedParam = RU.calculatedParam;
        expect(calculatedParam.symbol).toBe("Q");

        let nbCalc = 0;
        for (const p of RU.parameterIterator) {
            if (p.valueMode === ParamValueMode.CALCUL) {
                nbCalc++;
            }
        }
        expect(nbCalc).toBe(1);

        RU.CalcSerie();
        expect(RU.result).toBeDefined();
    });

    it("serialized file should contain 6 Nubs", () => {
        Session.getInstance().clear();
        const res = Session.getInstance().unserialise(JSON.stringify(session6Calc));
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getNumberOfNubs()).toBe(6);
    });

    it("loaded serialized Ouvrages should be calculable", () => {
        Session.getInstance().clear();
        // tslint:disable-next-line:max-line-length
        const session = `{"session":[{"uid":"NGVzdz","props":{"calcType":8,"nodeType":0},"meta":{"title":"Ouvrages"},"children":[{"uid":"Z2F4dz","props":{"calcType":7,"nodeType":5,"structureType":1,"loiDebit":1},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"SINGLE","value":2}]},{"uid":"ZDR4cX","props":{"calcType":7,"nodeType":5,"structureType":1,"loiDebit":1},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"CALCUL"}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":3.5},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"Z2","mode":"SINGLE","value":101.5}]}]}`;
        Session.getInstance().unserialise(session);

        expect(Session.getInstance().getNumberOfNubs()).toBe(1);

        const OUV = Session.getInstance().findNubByUid("NGVzdz");
        expect(OUV).toBeDefined();

        const calculatedParam = OUV.calculatedParam;
        expect(calculatedParam.symbol).toBe("L");

        let nbCalc = 0;
        for (const p of OUV.parameterIterator) {
            if (p.valueMode === ParamValueMode.CALCUL) {
                nbCalc++;
            }
        }
        expect(nbCalc).toBe(1);

        OUV.CalcSerie();
        expect(OUV.result).toBeDefined();
        expect(OUV.result.vCalc).toBeCloseTo(1.031);
    });

    it("loaded serialized spaghetti session should have the right target for every link", () => {
        Session.getInstance().clear();
        Session.getInstance().unserialise(JSON.stringify(sessionSpaghetti));

        expect(Session.getInstance().getNumberOfNubs()).toBe(5);

        const SP = (Session.getInstance().findNubByUid("ZHh1YW") as SectionParametree);
        const OU = (Session.getInstance().findNubByUid("dGx0em") as ParallelStructure);
        const MR = (Session.getInstance().findNubByUid("eTgwMG") as MacroRugo);
        const RU = (Session.getInstance().findNubByUid("dzA1OX") as RegimeUniforme);
        const DD = (Session.getInstance().findNubByUid("cXFraW") as Dever);

        // 1. check Section Paramétrée
        expect(SP).toBeDefined();
        checkLink(SP.section.prms.LargeurBerge, OU.structures[2], "L");

        // 2. check Macro-rugo
        expect(MR).toBeDefined();
        checkLink(MR.prms.ZF1, OU, "Z1");
        checkLink(MR.prms.Q, SP.section, "Q");

        // 3. check Ouvrages
        expect(OU).toBeDefined();
        checkLink(OU.prms.Z2, DD, "Z1");
        const struct2 = (OU.structures[1] as RectangularStructure);
        checkLink(struct2.prms.L, OU.structures[2], "L");
        const struct3 = (OU.structures[2] as RectangularStructure);
        checkLink(struct3.prms.W, OU.structures[1], "W");
        checkLink(struct3.prms.CdGR, OU.structures[0], "CdGR");

        // 4. check Régime Uniforme
        expect(RU).toBeDefined();

        // 5. check Déversoirs dénoyés
        expect(DD).toBeDefined();
        checkLink(DD.prms.BR, SP, "B");

    });

    it("nghyd#263 - errors without consequences should not appear", () => {
        Session.getInstance().clear();
        // tslint:disable-next-line:max-line-length
        const json = `{"header":{"source":"jalhyd","format_version":"1.1","created":"2019-07-31T12:08:20.284Z"},"session":[{"uid":"NjdmM3","props":{"calcType":"PabChute","nodeType":"None"},"meta":{"title":"PAB : chute"},"children":[],"parameters":[{"symbol":"Z1","mode":"SINGLE","value":29.99},{"symbol":"Z2","mode":"SINGLE","value":26.81},{"symbol":"DH","mode":"CALCUL"}]},{"uid":"eWNjdG","props":{"calcType":"PabNombre","nodeType":"None"},"meta":{"title":"PAB : nombre"},"children":[],"parameters":[{"symbol":"DHT","mode":"LINK","targetNub":"NjdmM3","targetParam":"DH"},{"symbol":"N","mode":"SINGLE","value":14},{"symbol":"DH","mode":"CALCUL"}]},{"uid":"dXM4em","props":{"calcType":"PabPuissance","nodeType":"None"},"meta":{"title":"PAB : puissance"},"children":[],"parameters":[{"symbol":"DH","mode":"LINK","targetNub":"eWNjdG","targetParam":"DH"},{"symbol":"Q","mode":"SINGLE","value":1.8},{"symbol":"V","mode":"CALCUL"},{"symbol":"PV","mode":"SINGLE","value":140}]},{"uid":"bzNlaX","props":{"calcType":"PabDimensions","nodeType":"None"},"meta":{"title":"PAB : dimensions"},"children":[],"parameters":[{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"W","mode":"SINGLE","value":3.6},{"symbol":"Y","mode":"CALCUL"},{"symbol":"V","mode":"LINK","targetNub":"dXM4em","targetParam":"V"}]},{"uid":"cGI5d3","props":{"calcType":"Cloisons","nodeType":"None"},"meta":{"title":"Cloisons"},"children":[{"uid":"ZzZzbD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"h1","mode":"CALCUL"},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"Q","mode":"LINK","targetNub":"dXM4em","targetParam":"Q"},{"symbol":"Z1","mode":"SINGLE","value":30.14},{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"LINK","targetNub":"bzNlaX","targetParam":"W"},{"symbol":"PB","mode":"SINGLE","value":2.5},{"symbol":"DH","mode":"LINK","targetNub":"eWNjdG","targetParam":"DH"}]}]}`;
        const res = Session.getInstance().unserialise(json);
        expect(res.hasErrors).toBe(false);
    });

    it("settings should be saved in session file", () => {
        Session.getInstance().clear();
        SessionSettings.precision = 1e-8,
            SessionSettings.maxIterations = 123;
        const serialized = Session.getInstance().serialise();
        expect(serialized).toContain('"precision":1e-8');
        expect(serialized).toContain('"maxIterations":123');
    });

    it("settings should be restored from session file", () => {
        Session.getInstance().clear();
        // tslint:disable-next-line:max-line-length
        const json = `{"header":{"source":"jalhyd","format_version":"1.1","created":"2019-09-12T08:46:20.079Z"},"settings":{"precision":1e-6,"maxIterations":94},"session":[]}`;
        const res = Session.getInstance().unserialise(json);
        expect(res.hasErrors).toBe(false);
        expect(SessionSettings.precision).toBe(1e-6);
        expect(SessionSettings.maxIterations).toBe(94);
    });

    it("variated parameter not propertly reloaded nghyd#603", () => {
        Session.getInstance().clear();
        const json = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-02-09T12:31:02.306Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":5},"documentation":"","session":[{"uid":"OTg2d2","props":{"calcType":"ParallelStructure","nullparams":false},"meta":{"title":"Ouvrages"},"children":[{"uid":"bHduOG","props":{"calcType":"Structure","loiDebit":"GateCem88v","structureType":"SeuilOrificeRectangulaire","nullparams":false},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"CALCUL","value":2},{"symbol":"CdGR","mode":"SINGLE","value":0.6}]}],"parameters":[{"symbol":"Q","mode":"MINMAX","min":0.25,"max":1,"step":0.0375,"extensionStrategy":0},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"Z2","mode":"SINGLE","value":101.5}]}]}`;
        const res = Session.getInstance().unserialise(json);
        expect(res.nubs[0].nub.prms["Q"].valueMode).toEqual(1);
    });
});

describe("nodeType property - ", () => {

    it("serialized SectionNub should have a nodeType property", () => {
        const prmsST = new ParamsSectionTrapez(
            2.5, // largeur de fond
            0.56, // fruit
            undefined, // tirant d'eau
            40, //  Ks=Strickler
            2,  //  Q=Débit
            0.001, //  If=pente du fond
            1, // YB= hauteur de berge
        );
        const sect = new cSnTrapez(prmsST);
        const prem = new CourbeRemousParams(100.155, 100.803, 100.005, 100,
            5,  // Long= Longueur du bief
            5,  // Dx=Pas d'espace
        );
        const rem2 = new CourbeRemous(sect, prem, MethodeResolution.Trapezes);
        Session.getInstance().clear();
        Session.getInstance().registerNub(rem2);
        const serialized = Session.getInstance().serialise();
        expect(serialized).toContain('"nodeType":"SectionTrapeze"');
    });

    it("serialized Nub with no Section should not have a nodeType property", () => {
        const prms = new PabChuteParams(
            2,      // Cote Hamont Z1
            0.5,    // Cote aval Z2
            1.5,    // Chute DH
        );
        const pc = new PabChute(prms);
        Session.getInstance().clear();
        Session.getInstance().registerNub(pc);
        const serialized = Session.getInstance().serialise();
        expect(serialized).not.toContain('"nodeType"');
    });

});

describe("sessions containing Solveur - ", () => {

    it("serialised session should contain 'nubTocalculate' and 'searchedParameter' properties", () => {
        const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
        pc.calculatedParam = pc.prms.DH;
        const pn = new PabNombre(new PabNombreParams(666, 10, 666));
        pn.calculatedParam = pn.prms.DH;
        const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
        pp.calculatedParam = pp.prms.PV;
        Session.getInstance().clear();
        Session.getInstance().registerNubs([pc, pn, pp]);
        pn.prms.DHT.defineReference(pc, "DH");
        pp.prms.DH.defineReference(pn, "DH");
        const s = new Solveur(new SolveurParams(324.907), false);
        s.nubToCalculate = pp;
        s.searchedParameter = pc.prms.Z1;
        Session.getInstance().registerNub(s);

        const serialised = Session.getInstance().serialise();
        expect(serialised).toContain(`searchedParameter":"`);
        expect(serialised).toContain(`"nubToCalculate":"`);
    });

    it("session with a Solveur in last position should be loaded without errors", () => {
        Session.getInstance().clear();
        const res = Session.getInstance().unserialise(JSON.stringify(sessionSolveurLast));
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getNumberOfNubs()).toBe(4);

        const solveur = (Session.getInstance().findNubByUid("ODM0Z2") as Solveur);
        expect(solveur.uid).toBe("ODM0Z2");
        expect(solveur.prms.Ytarget.singleValue).toBe(324);
        const ntc = solveur.nubToCalculate;
        expect(ntc).toBeDefined();
        expect(ntc.uid).toBe("OHBpcz");
        const sp3 = solveur.searchedParameter;
        expect(sp3).toBeDefined();
        expect(sp3.symbol).toBe("Z1");
        expect(sp3.parentNub.uid).toBe("NjRvcG");
    });

    it("session with a Solveur in non-last position should be loaded without errors", () => {
        Session.getInstance().clear();
        const res = Session.getInstance().unserialise(JSON.stringify(sessionSolveurFirst));
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getNumberOfNubs()).toBe(4);

        const solveur = (Session.getInstance().findNubByUid("ODM0Z2") as Solveur);
        expect(solveur.uid).toBe("ODM0Z2");
        expect(solveur.prms.Ytarget.singleValue).toBe(324);
        const ntc = solveur.nubToCalculate;
        expect(ntc).toBeDefined();
        expect(ntc.uid).toBe("OHBpcz");
        const sp4 = solveur.searchedParameter;
        expect(sp4).toBeDefined();
        expect(sp4.symbol).toBe("Z1");
        expect(sp4.parentNub.uid).toBe("NjRvcG");
    });

});

describe("PreBarrage - ", () => {

    it("serialise", () => {
        const pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
        pb.addChild(new PbCloison(undefined, undefined)); // amont-aval
        const b1 = new PbBassin(new PbBassinParams(0.1, 42));
        pb.addChild(b1);
        pb.addChild(new PbCloison(undefined, b1)); // amont-B1
        const b2 = new PbBassin(new PbBassinParams(0.15, 38));

        const c2 = new PbCloison(undefined, b2); // amont-B2
        const s1: Structure = CreateStructure(LoiDebit.WeirCunge80);
        s1.prms.ZDV.singleValue = 95.30;
        s1.getParameter("L").singleValue = 0.4;
        s1.getParameter("CdGR").singleValue = 1.04;
        c2.addChild(s1);
        pb.addChild(c2);

        pb.addChild(b2);
        pb.addChild(new PbCloison(b1, b2)); // B1-B2
        pb.addChild(new PbCloison(b2, undefined)); // B2-aval

        Session.getInstance().clear();
        Session.getInstance().registerNub(pb);
        const json = Session.getInstance().serialise();
        expect(json).toContain(`"props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":""}`);

        // check that no parameter is in CALC mode in children
        const calcCount = (json.match(/"mode":"CALCUL"/g) || []).length;
        expect(calcCount).toBe(1);
    });

    it("unserialise", () => {
        Session.getInstance().clear();
        const json = `{ "header": { "source": "jalhyd", "format_version": "1.3", "created": "2020-06-29T07:58:18.237Z" }, "settings": { "precision": 1e-7, "maxIterations": 100, "displayPrecision": 3 }, "documentation": "", "session": [ { "uid": "bG5oYW", "props": { "calcType": "PreBarrage" }, "meta": { "title": "Prébarrages" }, "children": [ { "uid": "a3c1eH", "props": { "calcType": "PbCloison", "upstreamBasin": "", "downstreamBasin": "" }, "children": [ { "uid": "bHdvOW", "props": { "calcType": "Structure", "structureType": "SeuilRectangulaire", "loiDebit": "WeirSubmergedLarinier" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 101.11 }, { "symbol": "L", "mode": "SINGLE", "value": 0.211 }, { "symbol": "CdWSL", "mode": "SINGLE", "value": 0.7511 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE" }, { "symbol": "Z1", "mode": "SINGLE", "value": 0 }, { "symbol": "Z2", "mode": "SINGLE", "value": 0 } ] }, { "uid": "M3AxbT", "props": { "calcType": "PbBassin" }, "children": [], "parameters": [ { "symbol": "S", "mode": "SINGLE", "value": 0.111 }, { "symbol": "ZF", "mode": "SINGLE", "value": 42.11 } ] }, { "uid": "bjRzNG", "props": { "calcType": "PbCloison", "upstreamBasin": "", "downstreamBasin": "M3AxbT" }, "children": [ { "uid": "MGYycm", "props": { "calcType": "Structure", "structureType": "SeuilTriangulaire", "loiDebit": "TriangularWeirBroad" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 101.22 }, { "symbol": "alpha2", "mode": "SINGLE", "value": 45.22 }, { "symbol": "CdT", "mode": "SINGLE", "value": 1.3622 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE" }, { "symbol": "Z1", "mode": "SINGLE", "value": 0 }, { "symbol": "Z2", "mode": "SINGLE", "value": 0 } ] }, { "uid": "OTg1en", "props": { "calcType": "PbCloison", "upstreamBasin": "", "downstreamBasin": "d2kxcD" }, "children": [ { "uid": "Ym1qZH", "props": { "calcType": "Structure", "structureType": "Orifice", "loiDebit": "OrificeFree" }, "children": [], "parameters": [ { "symbol": "S", "mode": "SINGLE", "value": 0.133 }, { "symbol": "CdO", "mode": "SINGLE", "value": 0.733 }, { "symbol": "Zco", "mode": "SINGLE", "value": 101.33 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE" }, { "symbol": "Z1", "mode": "SINGLE", "value": 0 }, { "symbol": "Z2", "mode": "SINGLE", "value": 0 } ] }, { "uid": "d2kxcD", "props": { "calcType": "PbBassin" }, "children": [], "parameters": [ { "symbol": "S", "mode": "SINGLE", "value": 0.1522 }, { "symbol": "ZF", "mode": "SINGLE", "value": 38.22 } ] }, { "uid": "Z3J2cD", "props": { "calcType": "PbCloison", "upstreamBasin": "M3AxbT", "downstreamBasin": "d2kxcD" }, "children": [ { "uid": "aHpubT", "props": { "calcType": "Structure", "structureType": "VanneFondRectangulaire", "loiDebit": "RectangularOrificeSubmerged" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 101.44 }, { "symbol": "W", "mode": "SINGLE", "value": 0.544 }, { "symbol": "L", "mode": "SINGLE", "value": 0.244 }, { "symbol": "CdGR", "mode": "SINGLE", "value": 0.644 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE" }, { "symbol": "Z1", "mode": "SINGLE", "value": 0 }, { "symbol": "Z2", "mode": "SINGLE", "value": 0 } ] }, { "uid": "anQ0Zn", "props": { "calcType": "PbCloison", "upstreamBasin": "d2kxcD", "downstreamBasin": "" }, "children": [ { "uid": "ZzJtan", "props": { "calcType": "Structure", "structureType": "VanneFondRectangulaire", "loiDebit": "GateCunge80" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 101.55 }, { "symbol": "W", "mode": "SINGLE", "value": 0.555 }, { "symbol": "L", "mode": "SINGLE", "value": 0.255 }, { "symbol": "CdCunge", "mode": "SINGLE", "value": 1.55 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE" }, { "symbol": "Z1", "mode": "SINGLE", "value": 0 }, { "symbol": "Z2", "mode": "SINGLE", "value": 0 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE", "value": 1.0101 }, { "symbol": "Z1", "mode": "CALCUL" }, { "symbol": "Z2", "mode": "SINGLE", "value": 90.0101 } ] } ] }`;
        const res = Session.getInstance().unserialise(json);
        expect(res.hasErrors).toBe(false);

        const pb = Session.getInstance().findNubByUid("bG5oYW") as PreBarrage;
        expect(pb).toBeDefined();
        expect(pb.children.length).toBe(7); // 2 basins, 5 walls
        expect(pb.bassins.length).toBe(2);

        // paramètres de la rivière
        expect(pb.prms.Q.singleValue).toBe(1.0101);
        expect(pb.prms.Z2.singleValue).toBe(90.0101);
        expect(pb.prms.Z1.valueMode).toBe(ParamValueMode.CALCUL);
        expect(pb.calculatedParam.symbol).toBe("Z1");

        const b1: PbBassin = pb.findChild("M3AxbT") as PbBassin;
        expect(b1.prms.S.singleValue).toBe(0.111);
        expect(b1.prms.ZF.singleValue).toBe(42.11);
        const b2: PbBassin = pb.findChild("d2kxcD") as PbBassin;
        expect(b2.prms.S.singleValue).toBe(0.1522);
        expect(b2.prms.ZF.singleValue).toBe(38.22);

        const c1: PbCloison = pb.findChild("a3c1eH") as PbCloison; // amont-aval
        expect(c1.bassinAmont).toBeUndefined();
        expect(c1.bassinAval).toBeUndefined();
        expect(c1.structures.length).toBe(1);
        expect(c1.structures[0].getPropValue("loiDebit")).toBe(LoiDebit.WeirSubmergedLarinier);
        const s1 = c1.structures[0].prms as RectangularStructureParams;
        expect(s1.ZDV.singleValue).toBe(101.11);
        expect(s1.L.singleValue).toBe(0.211);
        expect(s1.CdWSL.singleValue).toBe(0.7511);

        const c2: PbCloison = pb.findChild("bjRzNG") as PbCloison; // amont-B1
        expect(c2.bassinAmont).toBeUndefined();
        expect(c2.bassinAval.uid).toBe("M3AxbT");
        expect(c2.structures.length).toBe(1);
        expect(c2.structures[0].getPropValue("loiDebit")).toBe(LoiDebit.TriangularWeirBroad);
        const s2 = c2.structures[0].prms as TriangularStructureParams;
        expect(s2.ZDV.singleValue).toBe(101.22);
        expect(s2.CdT.singleValue).toBe(1.3622);
        expect(s2.alpha2.singleValue).toBe(45.22);

        const c3: PbCloison = pb.findChild("OTg1en") as PbCloison; // amont-B2
        expect(c3.bassinAmont).toBeUndefined();
        expect(c3.bassinAval.uid).toBe("d2kxcD");
        expect(c3.structures.length).toBe(1);
        expect(c3.structures[0].getPropValue("loiDebit")).toBe(LoiDebit.OrificeFree);
        const s3 = c3.structures[0].prms as StructureOrificeFreeParams;
        expect(s3.S.singleValue).toBe(0.133);
        expect(s3.CdO.singleValue).toBe(0.733);
        expect(s3.Zco.singleValue).toBe(101.33);

        const c4: PbCloison = pb.findChild("Z3J2cD") as PbCloison; // B1-B2
        expect(c4.bassinAmont.uid).toBe("M3AxbT");
        expect(c4.bassinAval.uid).toBe("d2kxcD");
        expect(c4.structures.length).toBe(1);
        expect(c4.structures[0].getPropValue("loiDebit")).toBe(LoiDebit.RectangularOrificeSubmerged);
        const s4 = c4.structures[0].prms as RectangularStructureParams;
        expect(s4.ZDV.singleValue).toBe(101.44);
        expect(s4.L.singleValue).toBe(0.244);
        expect(s4.W.singleValue).toBe(0.544);
        expect(s4.CdGR.singleValue).toBe(0.644);

        const c5: PbCloison = pb.findChild("anQ0Zn") as PbCloison; // B2-aval
        expect(c5.bassinAmont.uid).toBe("d2kxcD");
        expect(c5.bassinAval).toBeUndefined();
        expect(c5.structures.length).toBe(1);
        expect(c5.structures[0].getPropValue("loiDebit")).toBe(LoiDebit.GateCunge80);
        const s5 = c5.structures[0].prms as RectangularStructureParams;
        expect(s5.ZDV.singleValue).toBe(101.55);
        expect(s5.L.singleValue).toBe(0.255);
        expect(s5.W.singleValue).toBe(0.555);
        expect(s5.CdCunge.singleValue).toBe(1.55);

        // calculate
        const resPb = pb.CalcSerie();
        expect(resPb).toBeDefined();
    });

    it("unserialise multiple times", () => {
        Session.getInstance().clear();
        const json = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-07-03T11:58:35.049Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"YmZ6eW","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"ZW8yaX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"bXkwZW","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"ZW8yaX"},"children":[{"uid":"cmw3eH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"OHV2cH","props":{"calcType":"PbCloison","upstreamBasin":"ZW8yaX","downstreamBasin":""},"children":[{"uid":"OGM3bm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":100}]}]}`;
        // unserialise
        const res = Session.getInstance().unserialise(json);
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getAllNubs().length).toBe(1);

        const pb = Session.getInstance().findNubByUid("YmZ6eW") as PreBarrage;
        expect(pb).toBeDefined();
        expect(pb.children.length).toBe(3); // 1 basins, 2 walls
        expect(pb.bassins.length).toBe(1);

        const c1: PbCloison = pb.findChild("bXkwZW") as PbCloison; // amont-B1
        expect(c1.bassinAmont).toBeUndefined();
        expect(c1.bassinAval.uid).toBe("ZW8yaX");

        const c2: PbCloison = pb.findChild("OHV2cH") as PbCloison; // B1-aval
        expect(c2.bassinAmont.uid).toBe("ZW8yaX");
        expect(c2.bassinAval).toBeUndefined();

        // unserialise the same Nub a second time
        const res2 = Session.getInstance().unserialise(json);
        expect(res2.hasErrors).toBe(false);
        expect(Session.getInstance().getAllNubs().length).toBe(2);

        const pb2 = Session.getInstance().getAllNubs()[1] as PreBarrage;
        expect(pb2).toBeDefined();
        expect(pb2.uid).not.toBe("YmZ6eW");
        expect(pb2.children.length).toBe(3); // 1 basins, 2 walls
        expect(pb2.bassins.length).toBe(1);
        // check that UIDs changed
        expect(pb2.uid).not.toBe(pb.uid);
        expect(pb2.bassins[0].uid).not.toBe(pb.bassins[0].uid);

        const c12: PbCloison = pb2.getChildren()[1] as PbCloison; // amont-B1
        expect(c12.bassinAmont).toBeUndefined();
        expect(c12.bassinAval.uid).toBe(pb2.bassins[0].uid);

        const c22: PbCloison = pb2.getChildren()[2] as PbCloison; // B1-aval
        expect(c22.bassinAmont.uid).toBe(pb2.bassins[0].uid);
        expect(c22.bassinAval).toBeUndefined();
    });

});

describe("sessions containing Verificateur - ", () => {

    it("serialised session should contain 'nubToVerify' and 'speciesList' properties", () => {
        Session.getInstance().clear();
        const p = new Pab(new PabParams(1, 100, 99), new CloisonAval(new CloisonsAvalParams(1, 100, 99, 99.5)));
        const e = new Espece(new EspeceParams(2));
        Session.getInstance().registerNubs([e, p]);
        const v = new Verificateur();
        Session.getInstance().registerNub(v);
        v.nubToVerify = p;
        v.speciesList = ["4", e.uid];

        const serialised = Session.getInstance().serialise();
        expect(serialised).toContain(`"nubToVerify":"`);
        expect(serialised).toContain(`"speciesList":[`);
    });

    it("session with a Verificateur in last position should be loaded without errors", () => {
        Session.getInstance().clear();
        const res = Session.getInstance().unserialise(JSON.stringify(sessionVerificateurLast));
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getNumberOfNubs()).toBe(5);

        const verificateur = (Session.getInstance().findNubByUid("N2RkeD") as Verificateur);
        expect(verificateur.uid).toBe("N2RkeD");
        const ntv = verificateur.nubToVerify;
        expect(ntv).toBeDefined();
        expect(ntv.uid).toBe("MG45Yj");
        const spl = verificateur.speciesList;
        expect(spl).toBeDefined();
        expect(spl).toEqual(["SPECIES_2", "SPECIES_6", "SPECIES_7b", "Nms4Nn"]);
    });

    it("session with a Verificateur in non-last position should be loaded without errors", () => {
        Session.getInstance().clear();
        const res = Session.getInstance().unserialise(JSON.stringify(sessionVerificateurFirst));
        expect(res.hasErrors).toBe(false);
        expect(Session.getInstance().getNumberOfNubs()).toBe(5);

        const verificateur = (Session.getInstance().findNubByUid("N2RkeD") as Verificateur);
        expect(verificateur.uid).toBe("N2RkeD");
        const ntv = verificateur.nubToVerify;
        expect(ntv).toBeDefined();
        expect(ntv.uid).toBe("MG45Yj");
        const spl = verificateur.speciesList;
        expect(spl).toBeDefined();
        expect(spl).toEqual(["SPECIES_2", "SPECIES_6", "SPECIES_7b", "Nms4Nn"]);
    });

    describe("sessions containing MacrorugoRemous - ", () => {
        it("check MacroRugo is serialised", () => {
            Session.getInstance().clear();
            createMacrorugoRemousTest();
            const session = Session.getInstance().serialise();
            const js = JSON.parse(session);
            expect(js.session.length).toBe(2);
            expect(js.session[0].props.calcType).toBe("MacroRugo");
            expect(js.session[1].props.calcType).toBe("MacrorugoRemous");
            const uid1 = js.session[0].uid; // MacroRugo
            const uid2 = js.session[1].props.nubMacroRugo;
            expect(uid1).toBe(uid2);
        });

        it("check MacroRugo deserialisation", () => {
            const session = Session.getInstance();
            session.clear();
            createMacrorugoRemousTest();
            const ser = Session.getInstance().serialise();
            const js = JSON.parse(ser);
            const uid1 = js.session[0].uid; // MacroRugo
            const uid2 = js.session[1].uid; // MacrorugoRemous

            session.clear();
            session.unserialise(ser);
            expect(session.getAllNubs().length).toBe(2);
            const n1 = session.getAllNubs()[0];
            expect(n1.getPropValue("calcType")).toBe(CalculatorType.MacroRugo);
            expect(n1.uid).toBe(uid1);
            const n2 = session.getAllNubs()[1];
            expect(n2.getPropValue("calcType")).toBe(CalculatorType.MacrorugoRemous);
            expect(n2.uid).toBe(uid2);
            const mrr = n2 as MacrorugoRemous;
            expect(mrr.getPropValue("nubMacroRugo")).toEqual(uid1);
        });
    });
});
