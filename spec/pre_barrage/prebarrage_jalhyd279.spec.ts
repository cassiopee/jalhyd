import { PreBarrage, Session } from "../../src/index";
import { checkResultConsistency } from "../test_func";

let sessionJson: string;
describe("PreBarrage", () => {
    beforeEach(()=> {
        sessionJson = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-10-01T06:43:50.495Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"cG9yaG","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"c29nYW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"ajB2cn","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"c29nYW"},"children":[{"uid":"dGtvMW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"a2t5eG","props":{"calcType":"PbCloison","upstreamBasin":"c29nYW","downstreamBasin":""},"children":[{"uid":"enJhZT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"ODUxdn","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":""},"children":[{"uid":"YWFqdm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":100}]}]}`;
    });
    describe("jalhyd#279", () => {
        it("Discharge in first bassin should be greater than 0.4", () => {
            // tslint:disable-next-line:max-line-length
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("cG9yaG") as PreBarrage;
            // nub.DBG = true;
            nub.CalcSerie();
            expect(nub.bassins[0].result.values.Q).toBeGreaterThan(0.4);
        });
    });
    describe("jalhyd#280", () => {
        it("With Z2 > Z1, calculation should succeed", () => {
            // tslint:disable-next-line:max-line-length
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("cG9yaG") as PreBarrage;
            // nub.DBG = true;
            nub.prms.Z2.setValue(102);
            nub.CalcSerie();
            expect(nub.result.ok).toBeTrue();
        });
    });
});
