import { PreBarrage } from "../../src/prebarrage/pre_barrage";
import { PreBarrageParams } from "../../src/prebarrage/pre_barrage_params";
import { PbBassin } from "../../src/prebarrage/pb_bassin";
import { PbBassinParams } from "../../src/prebarrage/pb_bassin_params";
import { PbCloison } from "../../src/prebarrage/pb_cloison";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureWeirVillemonte } from "../../src/structure/structure_weir_villemonte";
import { StructureWeirCunge80 } from "../../src/structure/structure_weir_cunge80";
import { SectionParametree } from "../../src/open-channel/section/section_parametree";
import { PabChute } from "../../src/pab/pab_chute";
import { Session } from "../../src/session";
import { MessageCode } from "../../src/util/message";

function createPbCloisonTest(ZDV: number) {
    const rectStructPrms = new RectangularStructureParams(
        0,  // Q
        ZDV,        // ZDV
        0,        // Z1 entered here for good assignation of h1
        0,      // Z2
        0.25,          // L
        0.4        // Cd pour un seuil rectangulaire
        // W = Infinity par défaut pour un seuil
    );
    return new StructureWeirVillemonte(rectStructPrms);
}

function createPreBarrageTest(bMeshed: boolean = false): PreBarrage {
    // Pré-barrage Z1 = 101 m, Z2 = 100 m
    let Q: number;
    if (bMeshed) {
        Q = 1.350722;
    } else {
        Q = 0.601266;
    }
    const pb = new PreBarrage(new PreBarrageParams(Q, 101, 100), false);
    pb.calculatedParam = pb.prms.Z1;

    // 1 bassin intermédiaire à la cote de fond 99 m
    pb.addChild(new PbBassin(new PbBassinParams(20, 99)));

    // 1 cloison entre l'amont et le bassin avec une fente cote de radier 99.5
    pb.addChild(new PbCloison(undefined, pb.children[0] as PbBassin));
    pb.children[pb.children.length-1].addChild(createPbCloisonTest(99.5));

    // 1 cloison entre le bassin et l'aval avec une fente cote de radier 99
    pb.addChild(new PbCloison(pb.children[0] as PbBassin, undefined));
    pb.children[pb.children.length-1].addChild(createPbCloisonTest(99));

    if (bMeshed) {
        // 1 cloison entre l'amont et l'aval avec une fente cote de radier 99.5
        pb.addChild(new PbCloison(undefined, undefined));
        pb.children[pb.children.length-1].addChild(createPbCloisonTest(99.5));
    }

    return pb;
}

function uidsOfAllPointedNubs(pb: PreBarrage): string[] {
    let uids: string[] = [];
    for (const c of pb.children) {
        if (c instanceof PbCloison) {
            if (c.bassinAmont !== undefined) {
                uids.push(c.bassinAmont.uid)
            }
            if (c.bassinAval !== undefined) {
                uids.push(c.bassinAval.uid)
            }
        } else if (c instanceof PbBassin) {
            uids = uids.concat(c.cloisonsAval.map(ca => ca.uid));
            uids = uids.concat(c.cloisonsAmont.map(ca => ca.uid));
        }
    }
    for (const c of pb.cloisonsAmont) {
        if (c.bassinAval !== undefined) {
            uids.push(c.bassinAval.uid)
        }
    }
    for (const c of pb.bassins) {
        uids = uids.concat(c.cloisonsAval.map(ca => ca.uid));
        uids = uids.concat(c.cloisonsAmont.map(ca => ca.uid));
    }
    // deduplicate
    uids = uids.filter(
        (element, index, self) => self.indexOf(element) === index
    );
    return uids;
}

describe("Class PreBarrage:", () => {

    describe("Basic non meshed:", () => {
        it("Calc(Z1) should return 101", () => {
            const pb = createPreBarrageTest();
            const res = pb.CalcSerie();
            expect(res.vCalc).toBeCloseTo(101, 3);
        });
        it("Calc(Q) should return 0.601", () => {
            const pb = createPreBarrageTest();
            pb.calculatedParam = pb.prms.Q;
            pb.prms.Q.singleValue = 0;
            expect(pb.CalcSerie().vCalc).toBeCloseTo(0.601266, 3);
        });
    });

    describe("Basic meshed:", () => {
        it("Calc(Z1) should return 101", () => {
            const pb = createPreBarrageTest(true);
            expect(pb.CalcSerie().vCalc).toBeCloseTo(101, 3);
        });
        it("Calc(Q) should return 1.351", () => {
            const pb = createPreBarrageTest(true);
            pb.calculatedParam = pb.prms.Q;
            pb.prms.Q.singleValue = 0;
            expect(pb.CalcSerie().vCalc).toBeCloseTo(1.350722, 3);
        });
    });

    describe("Complex meshed:", () => {
        let pb: PreBarrage;
        beforeEach(() => {
            pb = createPreBarrageTest(true);
            pb.maxIterations = 100;
            pb.prms.Q.singleValue = pb.prms.Q.singleValue + 0.601266;
            pb.addChild(new PbBassin(new PbBassinParams(20, 99)));
            // 1 cloison entre l'amont et le bassin avec une fente cote de radier 99.5
            pb.addChild(new PbCloison(undefined, pb.bassins[1] as PbBassin));
            pb.children[pb.children.length-1].addChild(createPbCloisonTest(99.5));
            // 1 cloison entre le bassin et l'aval avec une fente cote de radier 99
            pb.addChild(new PbCloison(pb.bassins[1] as PbBassin, undefined));
            pb.children[pb.children.length-1].addChild(createPbCloisonTest(99));
            // 1 cloison entre les 2 bassins avec une fente cote de radier 99
            pb.addChild(new PbCloison(pb.bassins[0] as PbBassin, pb.bassins[1] as PbBassin));
            pb.children[pb.children.length-1].addChild(createPbCloisonTest(99));
        });
        it("Calc(Z1) should return 101", () => {
            const r = pb.CalcSerie();
            expect(r.ok).toBe(true);
            expect(Math.abs(r.vCalc - 101)).toBeLessThanOrEqual(1E-4);
        });
        it("Calc(Q) should return 1.951988", () => {
            pb.calculatedParam = pb.prms.Q;
            pb.prms.Q.singleValue = 0;
            const r = pb.CalcSerie();
            expect(r.ok).toBe(true);
            expect(Math.abs(r.vCalc - 1.951988)).toBeLessThanOrEqual(1E-4);
        });
    });

    describe("Real case:", () => {
        let pbc: PreBarrage;
        beforeEach(()=> {
            pbc = new PreBarrage(new PreBarrageParams(0.869, 95.25, 94.45), false);
            // add basins
            pbc.addChild(new PbBassin(new PbBassinParams(13.80, 95)));
            pbc.addChild(new PbBassin(new PbBassinParams(15.40, 94.70)));
            pbc.addChild(new PbBassin(new PbBassinParams(16.20, 94.70)));
            pbc.addChild(new PbBassin(new PbBassinParams(17.50, 94.40)));
            pbc.addChild(new PbBassin(new PbBassinParams(32.10, 94.25)));
            pbc.addChild(new PbBassin(new PbBassinParams(35.00, 94.10)));
            // add walls
            // Wall between upstream and basin 1
            pbc.addChild(new PbCloison(undefined, pbc.children[0] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.30, 0, 0, 0.4, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.25, 0, 0, 4.40, 1.04)
                )
            );
            //  Wall between upstream and basin 2
            pbc.addChild(new PbCloison(undefined, pbc.children[1] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.00, 0, 0, 1.00, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.25, 0, 0, 5.00, 0.91)
                )
            );
            // Wall between upstream and basin 5
            pbc.addChild(new PbCloison(undefined, pbc.children[4] as PbBassin));
             pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.25, 0, 0, 3.50, 0.99)
                )
            );
            // Wall between upstream and basin 6
            pbc.addChild(new PbCloison(undefined, pbc.children[5] as PbBassin));
             pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.25, 0, 0, 3.60, 0.99)
                )
            );
            // Wall between basin 1 & 3
            pbc.addChild(new PbCloison(pbc.children[0] as PbBassin, pbc.children[2] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.00, 0, 0, 0.40, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 96.25, 0, 0, 5.20, 0.99)
                )
            );
            // Wall between basin 2 & 3
            pbc.addChild(new PbCloison(pbc.children[1] as PbBassin, pbc.children[2] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.85, 0, 0, 4.38, 0.91)
                )
            );
            // Wall between basin 2 & 4
            pbc.addChild(new PbCloison(pbc.children[1] as PbBassin, pbc.children[3] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.85, 0, 0, 3.00, 0.99)
                )
            );
            // Wall between basin 2 & 5
            pbc.addChild(new PbCloison(pbc.children[1] as PbBassin, pbc.children[4] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.50, 0, 0, 1.00, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.75, 0, 0, 3.00, 0.99)
                )
            );
            // Wall between basin 3 & 4
            pbc.addChild(new PbCloison(pbc.children[2] as PbBassin, pbc.children[3] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 94.70, 0, 0, 0.40, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.65, 0, 0, 5.74, 0.99)
                )
            );
            // Wall between basin 4 & 5
            pbc.addChild(new PbCloison(pbc.children[3] as PbBassin, pbc.children[4] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 94.40, 0, 0, 0.40, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.35, 0, 0, 6.00, 0.99)
                )
            );
            // Wall between basin 5 & 6
            pbc.addChild(new PbCloison(pbc.children[4] as PbBassin, pbc.children[5] as PbBassin));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 94.25, 0, 0, 0.70, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 95.05, 0, 0, 9.50, 0.99)
                )
            );
            // Wall between basin 6 & downstream
            pbc.addChild(new PbCloison(pbc.children[5] as PbBassin, undefined));
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 94.10, 0, 0, 0.95, 1.04)
                )
            );
            pbc.children[pbc.children.length-1].addChild(
                new StructureWeirCunge80(
                    new RectangularStructureParams(0, 94.75, 0, 0, 10.20, 0.99)
                )
            );
        });
        it("Calc(Z1) should converge to 96.25", () => {
            pbc.calculatedParam = pbc.prms.Z1;
            const r = pbc.CalcSerie()
            expect(r.ok).toBeTrue();
            expect(r.vCalc).toBeCloseTo(96.25, 2);
        });
        it("Calc(Z1, Q=0) should converge to 95.30", () => {
            pbc.prms.Q.singleValue = 0;
            pbc.calculatedParam = pbc.prms.Z1;
            const r = pbc.CalcSerie()
            expect(r.ok).toBeTrue();
            expect(r.vCalc).toBeCloseTo(95.30, 1);
        });
        it("Calc(Z1, Q=1E-5) should converge to 95.301", () => {
            pbc.prms.Q.singleValue = 1E-5;
            pbc.calculatedParam = pbc.prms.Z1;
            const r = pbc.CalcSerie()
            expect(r.ok).toBeTrue();
            expect(r.vCalc).toBeCloseTo(95.301, 3);
        });
        it("Calc(Z1, Q=0.1265) should converge to 95.616859", () => {
            pbc.prms.Q.singleValue = 0.1265;
            pbc.calculatedParam = pbc.prms.Z1;
            const r = pbc.CalcSerie()
            expect(r.ok).toBeTrue();
            expect(r.vCalc).toBeCloseTo(95.616859, 3);
        });
        it("Calc(Q) should converge to 0.869", () => {
            pbc.calculatedParam = pbc.prms.Q;
            pbc.prms.Z1.singleValue = 96.25;
            pbc.prms.Q.singleValue = 0;
            const r = pbc.CalcSerie()
            expect(r.ok).toBeTrue();
            expect(r.vCalc).toBeCloseTo(0.869, 1);
        });
    });

    describe("variating PreBarrage:", () => {

        it("basic variation", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-18T13:42:34.135Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"YjJhMX","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"amdscG","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"aHVpen","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"amdscG"},"children":[{"uid":"aWp4M3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"cGthM2","props":{"calcType":"PbCloison","upstreamBasin":"amdscG","downstreamBasin":""},"children":[{"uid":"amhnaT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"MINMAX","min":98,"max":102,"step":1,"extensionStrategy":0}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pb = Session.getInstance().findNubByUid("YjJhMX") as PreBarrage;
            const res = pb.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.resultElements.length).toBe(5);
        });
    });

    describe("links:", () => {

        it("only river parameters should be exposed", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-23T09:09:37.952Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b2kxNm","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"Zjczdz","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"dWJrZX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":15.4},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"dzMxN2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":16.2},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"ZXZua2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":17.5},{"symbol":"ZF","mode":"SINGLE","value":94.4}]},{"uid":"YmR5aD","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":32.1},{"symbol":"ZF","mode":"SINGLE","value":94.25}]},{"uid":"bDN2OW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":35},{"symbol":"ZF","mode":"SINGLE","value":94.1}]},{"uid":"MWxycH","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"Zjczdz"},"children":[{"uid":"MmZ2aG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"eW9jZH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":4.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"c3Zpb2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"dWJrZX"},"children":[{"uid":"bTQ2cG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"N24zM2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MG4wNG","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"YmR5aD"},"children":[{"uid":"azV4dj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"YjJra2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"bDN2OW"},"children":[{"uid":"aDd4Y3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"enE4cn","props":{"calcType":"PbCloison","upstreamBasin":"Zjczdz","downstreamBasin":"dzMxN2"},"children":[{"uid":"ZHczZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aXRjZ3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"NXNoNG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"dzMxN2"},"children":[{"uid":"eXFqeH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":4.38},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bGhodG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"ZXZua2"},"children":[{"uid":"b3Z3OD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MDZ3aH","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"YmR5aD"},"children":[{"uid":"NTBzZm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.5},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"b3pjZX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.75},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"emZ2bX","props":{"calcType":"PbCloison","upstreamBasin":"dzMxN2","downstreamBasin":"ZXZua2"},"children":[{"uid":"cGllNj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.7},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aWFseW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.65},{"symbol":"L","mode":"SINGLE","value":5.74},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bG1ucj","props":{"calcType":"PbCloison","upstreamBasin":"ZXZua2","downstreamBasin":"YmR5aD"},"children":[{"uid":"MDJsaD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.4},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZzY2bT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.35},{"symbol":"L","mode":"SINGLE","value":6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dGNnZz","props":{"calcType":"PbCloison","upstreamBasin":"YmR5aD","downstreamBasin":"bDN2OW"},"children":[{"uid":"dnNqeX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.25},{"symbol":"L","mode":"SINGLE","value":0.7},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ODFkZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.05},{"symbol":"L","mode":"SINGLE","value":9.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"amprcm","props":{"calcType":"PbCloison","upstreamBasin":"bDN2OW","downstreamBasin":""},"children":[{"uid":"OG02MT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.1},{"symbol":"L","mode":"SINGLE","value":0.95},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"MHVxZn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.75},{"symbol":"L","mode":"SINGLE","value":10.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.844},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":94.45}]},{"uid":"b2xvMG","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages 1"},"children":[{"uid":"bW91cW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"eHhmc3","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":15.4},{"symbol":"ZF","mode":"LINK","targetNub":"b2kxNm","targetParam":"Z1"}]},{"uid":"anJiOX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":16.2},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"NmR4dT","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":17.5},{"symbol":"ZF","mode":"SINGLE","value":94.4}]},{"uid":"Zmp4OD","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":32.1},{"symbol":"ZF","mode":"SINGLE","value":94.25}]},{"uid":"OWZmeT","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":35},{"symbol":"ZF","mode":"SINGLE","value":94.1}]},{"uid":"OHQxMj","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"bW91cW"},"children":[{"uid":"dm81dm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"CALCUL","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZDYxYn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":4.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"N21zZm","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"eHhmc3"},"children":[{"uid":"ZWNrb2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"LINK","targetNub":"b2kxNm","targetParam":"Z1"},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"YXhnNX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"OHN0ND","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"Zmp4OD"},"children":[{"uid":"emI1az","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dnlmOT","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"OWZmeT"},"children":[{"uid":"aG1obT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"eXN4Zn","props":{"calcType":"PbCloison","upstreamBasin":"bW91cW","downstreamBasin":"anJiOX"},"children":[{"uid":"d2dhd2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"cW9kMG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"aGd3YT","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"anJiOX"},"children":[{"uid":"MTJ4c3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":4.38},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"NDJleW","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"NmR4dT"},"children":[{"uid":"c2VsY3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"ZzQ5dG","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"Zmp4OD"},"children":[{"uid":"dGFsY3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.5},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"Nmtxcn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.75},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MDFpaH","props":{"calcType":"PbCloison","upstreamBasin":"anJiOX","downstreamBasin":"NmR4dT"},"children":[{"uid":"YTVrNW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.7},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"M3EyOG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.65},{"symbol":"L","mode":"SINGLE","value":5.74},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"YnZwaD","props":{"calcType":"PbCloison","upstreamBasin":"NmR4dT","downstreamBasin":"Zmp4OD"},"children":[{"uid":"dWp1dn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.4},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZnA3Z2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.35},{"symbol":"L","mode":"SINGLE","value":6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"OGd3cj","props":{"calcType":"PbCloison","upstreamBasin":"Zmp4OD","downstreamBasin":"OWZmeT"},"children":[{"uid":"cXdsbG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.25},{"symbol":"L","mode":"SINGLE","value":0.7},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"OHhvMG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.05},{"symbol":"L","mode":"SINGLE","value":9.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bmQ4eX","props":{"calcType":"PbCloison","upstreamBasin":"OWZmeT","downstreamBasin":""},"children":[{"uid":"cmdoNm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.1},{"symbol":"L","mode":"SINGLE","value":0.95},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"cDhhdm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.75},{"symbol":"L","mode":"SINGLE","value":10.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":"0.844"},{"symbol":"Z1","mode":"SINGLE","value":101},{"symbol":"Z2","mode":"SINGLE","value":94.45}]},{"uid":"YW8ycn","props":{"calcType":"PabChute"},"meta":{"title":"PAB : chute"},"children":[],"parameters":[{"symbol":"Z1","mode":"SINGLE","value":2},{"symbol":"Z2","mode":"SINGLE","value":0.5},{"symbol":"DH","mode":"CALCUL"}]},{"uid":"aHZxan","props":{"calcType":"SectionParametree"},"meta":{"title":"Sec. param."},"children":[{"uid":"NDc4Ym","props":{"calcType":"Section","nodeType":"SectionRectangle"},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"SINGLE","value":1.2},{"symbol":"If","mode":"SINGLE","value":0.001},{"symbol":"YB","mode":"SINGLE","value":1},{"symbol":"Y","mode":"SINGLE","value":0.8},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.5}]}],"parameters":[]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pb1 = Session.getInstance().findNubByUid("b2kxNm") as PreBarrage;
            const pb2 = Session.getInstance().findNubByUid("b2xvMG") as PreBarrage;
            const pabChute = Session.getInstance().findNubByUid("YW8ycn") as PabChute;
            const sp = Session.getInstance().findNubByUid("aHZxan") as SectionParametree;
            // check linkable values
            // 1. PreBarrage should only expose elevations related to river
            const linkableToPabChuteZ1 = Session.getInstance().getLinkableValues(pabChute.prms.Z1);
            expect(linkableToPabChuteZ1.length).toBe(4);
            expect(linkableToPabChuteZ1.map((l) => l.nub.uid + "/" + l.symbol)).toEqual(["b2kxNm/Z1", "b2kxNm/Z2", "b2xvMG/Z1", "b2xvMG/Z2" ]);
            // 2. PreBarrage should not expose walls falls
            const linkableToPabChuteDH = Session.getInstance().getLinkableValues(pabChute.prms.DH);
            expect(linkableToPabChuteDH.length).toBe(0);
            // 3. PreBarrage should only expose flow related to river
            const linkableToSecParamQ = Session.getInstance().getLinkableValues(sp.section.prms.Q);
            expect(linkableToSecParamQ.length).toBe(2);
            expect(linkableToSecParamQ.map((l) => l.nub.uid + "/" + l.symbol)).toEqual(["b2kxNm/Q", "b2xvMG/Q"]);
            // 4. Nothing should be linkable to a basin's surface
            const linkableToBasinS = Session.getInstance().getLinkableValues(pb1.bassins[2].prms.S);
            expect(linkableToBasinS.length).toBe(0);
            // 5. Only elevations from other nubs should be linkable to a basin's bottom elevation
            const linkableToBasinZF = Session.getInstance().getLinkableValues(pb1.bassins[2].prms.ZF);
            expect(linkableToBasinZF.length).toBe(4);
            expect(linkableToBasinZF.map((l) => l.nub.uid + "/" + l.symbol)).toEqual(["b2xvMG/Z1", "b2xvMG/Z2", "YW8ycn/Z1", "YW8ycn/Z2" ]);
            // 6. Only elevations from other nubs should be linkable to a wall device's apron elevation
            const linkableToWallDeviceZDV = Session.getInstance().getLinkableValues((pb1.children[7] as PbCloison).structures[0].prms.ZDV);
            expect(linkableToWallDeviceZDV.length).toBe(4);
            expect(linkableToWallDeviceZDV.map((l) => l.nub.uid + "/" + l.symbol)).toEqual(["b2xvMG/Z1", "b2xvMG/Z2", "YW8ycn/Z1", "YW8ycn/Z2"]);
        });
    });

    describe("Basins and Walls results:", () => {

        it("basins results", () => {
            const pb = createPreBarrageTest();
            pb.CalcSerie();
            expect(pb.bassins[0].result).toBeDefined();
            expect(pb.bassins[0].result.resultElements.length).toBeGreaterThan(0);
            expect(Object.keys(pb.bassins[0].result.resultElements[0].values).length).toBeGreaterThan(0);
        });
    });

    describe("Add, move and remove children:", () => {
        let pb: PreBarrage;

        it("add then remove basin", () => {
            pb = createPreBarrageTest(true);
            expect(pb.children.length).toBe(4); // 3 walls, 1 basin
            expect(pb.bassins.length).toBe(1);

            // add 1 basin with 2 walls
            const nb = new PbBassin(new PbBassinParams(15, 94));
            pb.addChild(nb);
            pb.addChild(new PbCloison(pb.children[0] as PbBassin, nb));
            pb.children[pb.children.length-1].addChild(createPbCloisonTest(99.5));
            pb.addChild(new PbCloison(nb, undefined));
            pb.children[pb.children.length-1].addChild(createPbCloisonTest(99));

            expect(pb.children.length).toBe(7); // 5 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            pb.deleteChild(pb.bassins[pb.bassins.length - 1].findPositionInParent()); // newly added basin

            expect(pb.children.length).toBe(6); // 5 walls, 1 basin
            expect(pb.bassins.length).toBe(1);
        });

        it("remove basin", () => {
            pb = createPreBarrageTest(true);
            expect(pb.children.length).toBe(4); // 3 walls, 1 basin
            expect(pb.bassins.length).toBe(1);

            pb.deleteChild(0); // basin 1

            expect(pb.children.length).toBe(3); // 3 walls, 0 basin
            expect(pb.bassins.length).toBe(0);
        });

        it("remove walls", () => {
            pb = createPreBarrageTest(true);
            expect(pb.children.length).toBe(4); // 3 walls, 1 basin
            expect(pb.bassins.length).toBe(1);

            pb.deleteChild(1); // wall 1

            expect(pb.children.length).toBe(3); // 2 walls, 1 basin
            expect(pb.bassins.length).toBe(1);

            pb.deleteChild(1); // wall 2 (now 1)

            expect(pb.children.length).toBe(2); // 1 walls, 1 basin
            expect(pb.bassins.length).toBe(1);
        });

        it("basin removal in GUI example", () => {
            pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
            const b1 = new PbBassin(new PbBassinParams(0.1, 42));
            pb.addChild(b1);
            const b2 = new PbBassin(new PbBassinParams(0.15, 38));
            pb.addChild(b2);
            pb.addChild(new PbCloison(undefined, b1));
            pb.addChild(new PbCloison(b1, b2));
            pb.addChild(new PbCloison(b2, undefined));
            pb.addChild(new PbCloison(b1, undefined));

            expect(pb.children.length).toBe(6); // 4 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            pb.deleteChild(0); // remove b1

            expect(pb.children.length).toBe(5); // 4 walls, 1 basin
            expect(pb.bassins.length).toBe(1);

            expect(pb.cloisonsAmont[0].bassinAval).toBeUndefined();

            expect(uidsOfAllPointedNubs(pb)).not.toContain(b1.uid);
        });

        it("move basin up (1)", () => {
            pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
            const b1 = new PbBassin(new PbBassinParams(0.1, 42));
            pb.addChild(b1);
            const b2 = new PbBassin(new PbBassinParams(0.15, 38));
            pb.addChild(b2);
            pb.addChild(new PbCloison(undefined, b1));
            pb.addChild(new PbCloison(b1, b2));
            pb.addChild(new PbCloison(b2, undefined));
            pb.addChild(new PbCloison(b1, undefined));

            expect(pb.children.length).toBe(6); // 4 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            // move b2 to become 1st basin (basin #0)
            pb.moveBasin(b2.uid, 0);

            expect(pb.children.length).toBe(6); // 4 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            expect(pb.children[0].uid).toBe(b2.uid);
            expect(pb.children[1].uid).toBe(b1.uid);
        });

        it("move basin up (2)", () => {
            pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
            const b1 = new PbBassin(new PbBassinParams(0.1, 42));
            pb.addChild(b1);
            pb.addChild(new PbCloison(undefined, b1));
            pb.addChild(new PbCloison(b1, undefined));
            const b2 = new PbBassin(new PbBassinParams(0.15, 38));
            pb.addChild(b2);
            pb.addChild(new PbCloison(b1, b2));
            pb.addChild(new PbCloison(b2, undefined));
            const b3 = new PbBassin(new PbBassinParams(0.17, 35));
            pb.addChild(b3);
            pb.addChild(new PbCloison(b1, b3));
            pb.addChild(new PbCloison(b3, undefined));

            expect(pb.children.length).toBe(9); // 6 walls, 3 basins
            expect(pb.bassins.length).toBe(3);

            expect(pb.bassins[0].uid).toBe(b1.uid);
            expect(pb.bassins[1].uid).toBe(b2.uid);
            expect(pb.bassins[2].uid).toBe(b3.uid);

            // console.log("AVANT", pb.children.map(c => c.uid));
            // move b3 to become 2nd basin (basin #1)
            pb.moveBasin(b3.uid, 1);

            expect(pb.children.length).toBe(9); // 6 walls, 3 basins
            expect(pb.bassins.length).toBe(3);

            expect(pb.children[0].uid).toBe(b1.uid);
            expect(pb.children[3].uid).toBe(b3.uid);
            expect(pb.children[4].uid).toBe(b2.uid);

            expect(pb.bassins[0].uid).toBe(b1.uid);
            expect(pb.bassins[1].uid).toBe(b3.uid);
            expect(pb.bassins[2].uid).toBe(b2.uid);
        });

        it("move basin down (1)", () => {
            pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
            const b1 = new PbBassin(new PbBassinParams(0.1, 42));
            pb.addChild(b1);
            const b2 = new PbBassin(new PbBassinParams(0.15, 38));
            pb.addChild(b2);
            pb.addChild(new PbCloison(undefined, b1));
            pb.addChild(new PbCloison(b1, b2));
            pb.addChild(new PbCloison(b2, undefined));
            pb.addChild(new PbCloison(b1, undefined));

            expect(pb.children.length).toBe(6); // 4 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            // move b1 to become 2nd basin (basin #1)
            pb.moveBasin(b1.uid, 1);

            expect(pb.children.length).toBe(6); // 4 walls, 2 basins
            expect(pb.bassins.length).toBe(2);

            expect(pb.children[0].uid).toBe(b2.uid);
            expect(pb.children[1].uid).toBe(b1.uid);
        });

        it("move basin down (2)", () => {
            pb = new PreBarrage(new PreBarrageParams(1, 100, 90));
            const b1 = new PbBassin(new PbBassinParams(0.1, 42));
            pb.addChild(b1);
            pb.addChild(new PbCloison(undefined, b1));
            pb.addChild(new PbCloison(b1, undefined));
            const b2 = new PbBassin(new PbBassinParams(0.15, 38));
            pb.addChild(b2);
            pb.addChild(new PbCloison(b1, b2));
            pb.addChild(new PbCloison(b2, undefined));
            const b3 = new PbBassin(new PbBassinParams(0.17, 35));
            pb.addChild(b3);
            pb.addChild(new PbCloison(b1, b3));
            pb.addChild(new PbCloison(b3, undefined));

            expect(pb.children.length).toBe(9); // 6 walls, 3 basins
            expect(pb.bassins.length).toBe(3);

            expect(pb.bassins[0].uid).toBe(b1.uid);
            expect(pb.bassins[1].uid).toBe(b2.uid);
            expect(pb.bassins[2].uid).toBe(b3.uid);

            // console.log("AVANT", pb.children.map(c => c.uid));
            // move b1 to become 3rd basin (basin #2)
            pb.moveBasin(b1.uid, 2);

            expect(pb.children.length).toBe(9); // 6 walls, 3 basins
            expect(pb.bassins.length).toBe(3);

            expect(pb.children[2].uid).toBe(b2.uid);
            expect(pb.children[5].uid).toBe(b3.uid);
            expect(pb.children[6].uid).toBe(b1.uid);

            expect(pb.bassins[0].uid).toBe(b2.uid);
            expect(pb.bassins[1].uid).toBe(b3.uid);
            expect(pb.bassins[2].uid).toBe(b1.uid);
        });

    });

    describe("merge duplicate walls −", () => {

        it("no duplicate walls", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-06-29T13:15:21.029Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b2kxNm","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"Zjczdz","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"dWJrZX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":15.4},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"dzMxN2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":16.2},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"ZXZua2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":17.5},{"symbol":"ZF","mode":"SINGLE","value":94.4}]},{"uid":"YmR5aD","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":32.1},{"symbol":"ZF","mode":"SINGLE","value":94.25}]},{"uid":"bDN2OW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":35},{"symbol":"ZF","mode":"SINGLE","value":94.1}]},{"uid":"MWxycH","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"Zjczdz"},"children":[{"uid":"MmZ2aG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"eW9jZH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":4.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"c3Zpb2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"dWJrZX"},"children":[{"uid":"bTQ2cG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"N24zM2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MG4wNG","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"YmR5aD"},"children":[{"uid":"azV4dj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"YjJra2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"bDN2OW"},"children":[{"uid":"aDd4Y3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"enE4cn","props":{"calcType":"PbCloison","upstreamBasin":"Zjczdz","downstreamBasin":"dzMxN2"},"children":[{"uid":"ZHczZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aXRjZ3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"NXNoNG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"dzMxN2"},"children":[{"uid":"eXFqeH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":4.38},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bGhodG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"ZXZua2"},"children":[{"uid":"b3Z3OD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MDZ3aH","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"YmR5aD"},"children":[{"uid":"NTBzZm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.5},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"b3pjZX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.75},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"emZ2bX","props":{"calcType":"PbCloison","upstreamBasin":"dzMxN2","downstreamBasin":"ZXZua2"},"children":[{"uid":"cGllNj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.7},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aWFseW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.65},{"symbol":"L","mode":"SINGLE","value":5.74},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bG1ucj","props":{"calcType":"PbCloison","upstreamBasin":"ZXZua2","downstreamBasin":"YmR5aD"},"children":[{"uid":"MDJsaD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.4},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZzY2bT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.35},{"symbol":"L","mode":"SINGLE","value":6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dGNnZz","props":{"calcType":"PbCloison","upstreamBasin":"YmR5aD","downstreamBasin":"bDN2OW"},"children":[{"uid":"dnNqeX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.25},{"symbol":"L","mode":"SINGLE","value":0.7},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ODFkZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.05},{"symbol":"L","mode":"SINGLE","value":9.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"amprcm","props":{"calcType":"PbCloison","upstreamBasin":"bDN2OW","downstreamBasin":""},"children":[{"uid":"OG02MT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.1},{"symbol":"L","mode":"SINGLE","value":0.95},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"MHVxZn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.75},{"symbol":"L","mode":"SINGLE","value":10.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.844},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":94.45}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pb = Session.getInstance().findNubByUid("b2kxNm") as PreBarrage;
            const nbBassins1 = pb.bassins.length;
            const nbCloisons1 = pb.children.length - nbBassins1;
            pb.CalcSerie();
            const nbBassins2 = pb.bassins.length;
            const nbCloisons2 = pb.children.length - nbBassins2;
            expect(nbBassins2).toEqual(nbBassins1);
            expect(nbCloisons2).toEqual(nbCloisons1);
        });

        it("complex case", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-29T07:51:16.492Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b2kxNm","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"Zjczdz","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"dWJrZX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":15.4},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"dzMxN2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":16.2},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"ZXZua2","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":17.5},{"symbol":"ZF","mode":"SINGLE","value":94.4}]},{"uid":"YmR5aD","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":32.1},{"symbol":"ZF","mode":"SINGLE","value":94.25}]},{"uid":"bDN2OW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":35},{"symbol":"ZF","mode":"SINGLE","value":94.1}]},{"uid":"MWxycH","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"Zjczdz"},"children":[{"uid":"MmZ2aG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"eW9jZH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":4.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"c3Zpb2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"dWJrZX"},"children":[{"uid":"bTQ2cG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"N24zM2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MG4wNG","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"YmR5aD"},"children":[{"uid":"azV4dj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"YjJra2","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"bDN2OW"},"children":[{"uid":"aDd4Y3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"enE4cn","props":{"calcType":"PbCloison","upstreamBasin":"Zjczdz","downstreamBasin":"dzMxN2"},"children":[{"uid":"ZHczZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aXRjZ3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"NXNoNG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"dzMxN2"},"children":[{"uid":"eXFqeH","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":4.38},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bGhodG","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"ZXZua2"},"children":[{"uid":"b3Z3OD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MDZ3aH","props":{"calcType":"PbCloison","upstreamBasin":"dWJrZX","downstreamBasin":"YmR5aD"},"children":[{"uid":"NTBzZm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.5},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"b3pjZX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.75},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"emZ2bX","props":{"calcType":"PbCloison","upstreamBasin":"dzMxN2","downstreamBasin":"ZXZua2"},"children":[{"uid":"cGllNj","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.7},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"aWFseW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.65},{"symbol":"L","mode":"SINGLE","value":5.74},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bG1ucj","props":{"calcType":"PbCloison","upstreamBasin":"ZXZua2","downstreamBasin":"YmR5aD"},"children":[{"uid":"MDJsaD","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.4},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZzY2bT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.35},{"symbol":"L","mode":"SINGLE","value":6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dGNnZz","props":{"calcType":"PbCloison","upstreamBasin":"YmR5aD","downstreamBasin":"bDN2OW"},"children":[{"uid":"dnNqeX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.25},{"symbol":"L","mode":"SINGLE","value":0.7},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ODFkZ2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.05},{"symbol":"L","mode":"SINGLE","value":9.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"amprcm","props":{"calcType":"PbCloison","upstreamBasin":"bDN2OW","downstreamBasin":""},"children":[{"uid":"OG02MT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.1},{"symbol":"L","mode":"SINGLE","value":0.95},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"MHVxZn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.75},{"symbol":"L","mode":"SINGLE","value":10.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MWhyOG","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"dWJrZX"},"children":[{"uid":"c2E3MH","props":{"calcType":"Structure","loiDebit":"GateCunge80","structureType":"VanneFondRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"ZzIzen","props":{"calcType":"PbCloison","upstreamBasin":"dzMxN2","downstreamBasin":"ZXZua2"},"children":[{"uid":"Y2FtaH","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[]},{"uid":"ODEwN2","props":{"calcType":"PbCloison","upstreamBasin":"bDN2OW","downstreamBasin":""},"children":[{"uid":"dGNuaX","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"NWxsbj","props":{"calcType":"Structure","loiDebit":"TriangularTruncWeirFree","structureType":"SeuilTriangulaireTrunc"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"BT","mode":"SINGLE","value":0.9},{"symbol":"ZT","mode":"SINGLE","value":101},{"symbol":"CdT","mode":"SINGLE","value":1.36}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.844},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":94.45}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pb = Session.getInstance().findNubByUid("b2kxNm") as PreBarrage;
            const nbBassins1 = pb.bassins.length;
            const nbCloisons1 = pb.children.length - nbBassins1;
            const b2 = pb.bassins[1];
            const b3 = pb.bassins[2];
            const b6 = pb.bassins[5];
            expect(b2.cloisonsAmont.length).toBe(2);
            expect(b3.cloisonsAval.length).toBe(2);
            expect(b6.cloisonsAval.length).toBe(2);
            pb.CalcSerie();
            const nbBassins2 = pb.bassins.length;
            const nbCloisons2 = pb.children.length - nbBassins2;
            expect(nbBassins2).toEqual(nbBassins1);
            // 3 walls are supposed to be removed (merged)
            expect(nbCloisons2).toEqual(nbCloisons1 - 3);
            expect(b2.cloisonsAmont.length).toBe(1);
            expect(b2.cloisonsAmont[0].structures.length).toBe(3);
            expect(b3.cloisonsAval.length).toBe(1);
            expect(b3.cloisonsAval[0].structures.length).toBe(3);
            expect(b6.cloisonsAval.length).toBe(1);
            expect(b6.cloisonsAval[0].structures.length).toBe(4);
        });

        it("duplicate walls", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-29T08:24:40.715Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"cHM5am","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages"},"children":[{"uid":"bGZuOT","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":""},"children":[{"uid":"ZXhrZ3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dHIwem","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":""},"children":[{"uid":"NnpjeT","props":{"calcType":"Structure","loiDebit":"TriangularWeirBroad","structureType":"SeuilTriangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"alpha2","mode":"SINGLE","value":45},{"symbol":"CdT","mode":"SINGLE","value":1.36}]},{"uid":"dTdsMD","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[]},{"uid":"d3BoZG","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":""},"children":[{"uid":"dTA3Mm","props":{"calcType":"Structure","loiDebit":"OrificeFree","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7},{"symbol":"Zco","mode":"SINGLE","value":101}]}],"parameters":[]},{"uid":"OWVvNn","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":20},{"symbol":"ZF","mode":"SINGLE","value":99}]},{"uid":"MHpwNH","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"OWVvNn"},"children":[{"uid":"bW50ZT","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[]},{"uid":"dXNzcW","props":{"calcType":"PbCloison","upstreamBasin":"OWVvNn","downstreamBasin":""},"children":[{"uid":"bW53ZG","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":100}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pb = Session.getInstance().findNubByUid("cHM5am") as PreBarrage;
            const nbBassins1 = pb.bassins.length;
            const nbCloisons1 = pb.children.length - nbBassins1;
            expect(nbBassins1).toBe(1);
            expect(nbCloisons1).toBe(5);
            pb.CalcSerie();
            const nbBassins2 = pb.bassins.length;
            const nbCloisons2 = pb.children.length - nbBassins2;
            expect(nbBassins2).toEqual(nbBassins1);
            // 3 walls are merged into 1
            expect(nbCloisons2).toEqual(nbCloisons1 - 2);
            let c1: PbCloison;
            for (const c of pb.children) {
                if (c instanceof PbCloison && c.bassinAval === undefined && c.bassinAmont === undefined) {
                    c1 = c;
                }
            }
            expect(c1).toBeDefined();
            expect(c1.structures.length).toBe(4);
        });
    });

    describe("error cases −", () => {

        it("downstream water elevation > upstream water elevation", () => {
            const pb = createPreBarrageTest();
            pb.prms.Z2.singleValue = pb.prms.Z1.singleValue + 1;
            pb.prms.Q.setCalculated();
            const res = pb.CalcSerie();
            expect(res.ok).toBe(false);
            expect(res.resultElement.log.messages.length).toBe(1);
            expect(res.resultElement.log.messages[0].code).toBe(MessageCode.ERROR_PREBARRAGE_Z2_SUP_Z1);
        });

        it("basin apron elevation > upstream water elevation", () => {
            const pb = createPreBarrageTest();
            pb.prms.Z1.singleValue = pb.bassins[0].prms.ZF.singleValue - 1;
            pb.prms.Z2.singleValue = pb.prms.Z1.singleValue - 1;
            const res = pb.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.resultElement.log.messages.length).toBe(1);
            expect(res.resultElement.log.messages[0].code).toBe(MessageCode.WARNING_PREBARRAGE_BASSIN_ZF_SUP_Z1);
            expect(res.resultElement.log.messages[0].extraVar.n).toBe("1");
        });

        it("device ZDV < ZF of upstream basin", () => {
            const pb = createPreBarrageTest();
            pb.bassins[0].cloisonsAval[0].structures[0].prms.ZDV.singleValue = pb.bassins[0].prms.ZF.singleValue - 1;
            const res = pb.CalcSerie();
            expect(res.ok).toBe(false);
            expect(res.resultElement.log.messages.length).toBe(1);
            expect(res.resultElement.log.messages[0].code).toBe(MessageCode.ERROR_PREBARRAGE_STRUCTURE_ZDV_INF_ZF);
            expect(res.resultElement.log.messages[0].extraVar.cub).toBe("B1", "wall upstream basin");
            expect(res.resultElement.log.messages[0].extraVar.cdb).toBe("MSG_INFO_LIB_AVAL", "wall downstream basin");
            expect(res.resultElement.log.messages[0].extraVar.ns).toBe("1", "structure number in wall");
        });

        it("PreBarrage must have at least one path from upstream to downstream", () => {
            const pb = createPreBarrageTest();
            pb.bassins[0].cloisonsAval[0].bassinAval = pb.bassins[0]; // nonsense wall having the same basin at upstream and downstream
            expect(() => {
                pb.CalcSerie();
            }).toThrowError("PreBarrage.checkGeometry(): must have at least one path from upstream to downstream");
        })

        it("PreBarrage must have at least one upstream wall", () => {
            const pb = new PreBarrage(new PreBarrageParams(1, 101, 100), false);
            expect(() => {
                pb.CalcSerie();
            }).toThrowError("PreBarrage.checkGeometry(): must have at least one upstream wall (has 0)");
        })

        it("each basin must have at least one upstream wall and one downstream wall", () => {
            const pb = createPreBarrageTest();
            pb.bassins[0].cloisonsAmont = [];
            expect(() => {
                pb.CalcSerie();
            }).toThrowError("PreBarrage.checkGeometry(): basin 0 (starting at 0) must have at least one upstream wall (has 0) and one downstream wall (has 1)");
        })

        it("jalhyd#274", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-23T13:06:04.234Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b2xvMG","props":{"calcType":"PreBarrage"},"meta":{"title":"Prébarrages 1"},"children":[{"uid":"bW91cW","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":13.8},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"eHhmc3","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":15.4},{"symbol":"ZF","mode":"SINGLE","value":95}]},{"uid":"anJiOX","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":16.2},{"symbol":"ZF","mode":"SINGLE","value":94.7}]},{"uid":"NmR4dT","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":17.5},{"symbol":"ZF","mode":"SINGLE","value":94.4}]},{"uid":"Zmp4OD","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":32.1},{"symbol":"ZF","mode":"SINGLE","value":94.25}]},{"uid":"OWZmeT","props":{"calcType":"PbBassin"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":35},{"symbol":"ZF","mode":"SINGLE","value":94.1}]},{"uid":"OHQxMj","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"bW91cW"},"children":[{"uid":"dm81dm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZDYxYn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":4.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"N21zZm","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"eHhmc3"},"children":[{"uid":"ZWNrb2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"YXhnNX","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"OHN0ND","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"Zmp4OD"},"children":[{"uid":"emI1az","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"dnlmOT","props":{"calcType":"PbCloison","upstreamBasin":"","downstreamBasin":"OWZmeT"},"children":[{"uid":"aG1obT","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":3.6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"eXN4Zn","props":{"calcType":"PbCloison","upstreamBasin":"bW91cW","downstreamBasin":"anJiOX"},"children":[{"uid":"d2dhd2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"cW9kMG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":96.25},{"symbol":"L","mode":"SINGLE","value":5.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"aGd3YT","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"anJiOX"},"children":[{"uid":"MTJ4c3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":4.38},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"NDJleW","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"NmR4dT"},"children":[{"uid":"c2VsY3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.85},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"ZzQ5dG","props":{"calcType":"PbCloison","upstreamBasin":"eHhmc3","downstreamBasin":"Zmp4OD"},"children":[{"uid":"dGFsY3","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.5},{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"Nmtxcn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.75},{"symbol":"L","mode":"SINGLE","value":3},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"MDFpaH","props":{"calcType":"PbCloison","upstreamBasin":"anJiOX","downstreamBasin":"NmR4dT"},"children":[{"uid":"YTVrNW","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.7},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"M3EyOG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.65},{"symbol":"L","mode":"SINGLE","value":5.74},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"YnZwaD","props":{"calcType":"PbCloison","upstreamBasin":"NmR4dT","downstreamBasin":"Zmp4OD"},"children":[{"uid":"dWp1dn","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.4},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"ZnA3Z2","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.35},{"symbol":"L","mode":"SINGLE","value":6},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"OGd3cj","props":{"calcType":"PbCloison","upstreamBasin":"Zmp4OD","downstreamBasin":"OWZmeT"},"children":[{"uid":"cXdsbG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.25},{"symbol":"L","mode":"SINGLE","value":0.7},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"OHhvMG","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":95.05},{"symbol":"L","mode":"SINGLE","value":9.5},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]},{"uid":"bmQ4eX","props":{"calcType":"PbCloison","upstreamBasin":"OWZmeT","downstreamBasin":""},"children":[{"uid":"cmdoNm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.1},{"symbol":"L","mode":"SINGLE","value":0.95},{"symbol":"CdCunge","mode":"SINGLE","value":1}]},{"uid":"cDhhdm","props":{"calcType":"Structure","loiDebit":"WeirCunge80","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":94.75},{"symbol":"L","mode":"SINGLE","value":10.2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":0.844},{"symbol":"Z1","mode":"SINGLE","value":101},{"symbol":"Z2","mode":"SINGLE","value":94.45}]},{"uid":"aHZxan","props":{"calcType":"SectionParametree"},"meta":{"title":"Sec. param."},"children":[{"uid":"NDc4Ym","props":{"calcType":"Section","nodeType":"SectionRectangle"},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"LINK","targetNub":"b2xvMG","targetParam":"Q"},{"symbol":"If","mode":"SINGLE","value":0.001},{"symbol":"YB","mode":"SINGLE","value":1},{"symbol":"Y","mode":"SINGLE","value":0.8},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.5}]}],"parameters":[]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const sp = Session.getInstance().findNubByUid("aHZxan") as SectionParametree;
            expect(() => {
                sp.CalcSerie();
            }).not.toThrow();
        });

    })
});
