import { initRandom, random } from "./random";

describe("random", () => {
    it("random seed reuse", () => {
        const n = 50;
        const seed = initRandom();
        // console.log("seed", seed);
        const vals = [];
        for (let i = 0; i < n; i++) {
            vals[i] = random();
        }

        initRandom(seed);
        for (let i = 0; i < n; i++) {
            expect(random()).toEqual(vals[i]);
        }
    });
});
