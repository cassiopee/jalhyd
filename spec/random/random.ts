// Lehmer / LCG random number generator
// https://stackoverflow.com/a/72732727/11050010

let rng: any;
let useCustomGenerator: boolean = false;
let currentSeed: number;

export function initRandom(seed: number = Date.now()) {
    useCustomGenerator = true;
    const m = 2 ** 35 - 31
    const a = 185852
    let s = seed % m
    currentSeed = s;
    rng = function () {
        return (s = s * a % m) / m
    }
    return currentSeed;
}

export function getRandomSeed(): number {
    return currentSeed;
}

export function random(): number {
    if (useCustomGenerator) {
        // seedable random generator
        return rng();
    }
    else {
        // standard random generator
        return Math.random();
    }
}
