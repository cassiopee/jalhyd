import { Dichotomie } from "../src/dichotomie";
import { nub } from "./nubtest";

const dicho: Dichotomie = new Dichotomie(nub, "A");

describe("Class Dichotomie: ", () => {
    describe("Dichotomie(3, 1E-6, 0): ", () => {
        it("should return a result close to 1", () => {
            expect(dicho.Dichotomie(3, 1E-6, 0).vCalc).toBeCloseTo(1, 1E-6);
        });
    });
});
