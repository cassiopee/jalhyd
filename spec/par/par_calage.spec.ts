import { Par, ParType } from "../../src/par/par";
import { ParParams } from "../../src/par/par_params";
import { Result } from "../../src/util/result";
import { MessageCode } from "../../src/util/message";
import { SessionSettings, Session } from "../../src/index";

function expectExtraResPlane(res: Result, precPente: number = 2) {
    expect(res.values.P).toBeCloseTo(0.4, 2, "P");
    expect(res.values.B).toBeCloseTo(0.35, 2, "B");
    expect(res.values.C).toBeCloseTo(0.283, 2, "C");
    expect(res.values.D).toBeCloseTo(0.142, 2, "D");
    expect(res.values.h).toBeCloseTo(0.644, 2, "h");
    expect(res.values.ZD1).toBeCloseTo(9.36, 2, "ZD1");
    expect(res.values.Hmin).toBeCloseTo(1.11, 2, "Hmin"); // Conforme à Larinier 1992, mais différent de Cv3 (0.711)
    expect(res.values.Hmax).toBeCloseTo(1.32, 2, "Hmax");
    expect(res.values.V).toBeCloseTo(1.45, 2, "V"); // Conforme à Larinier 1992, mais différent de Cv3 (1.87)
    expect(res.values.ZR1).toBeCloseTo(9.24, 2, "ZR1");
    expect(res.values.ZD2).toBeCloseTo(8.34, 2, "ZD2");
    expect(res.values.ZR2).toBeCloseTo(8.22, 2, "ZR2");
    // Hauteur minimale des murs latéraux ? (Hmin * cos(45°) + 0.1m de marge)
    expect(res.values.Nb).toBe(14, "Nb");
    // expect(res.values.ZM).toBeCloseTo(9.93, 2, "ZM"); // X manque 0.1 (+ 0.1m de marge dans le cahier des charges)
    expect(res.values.LPI).toBeCloseTo(5.2, precPente, "LPI"); // Cv3 : 5.3
    expect(res.values.LPH).toBeCloseTo(5.1, precPente, "LPH"); // Cv3 : 5.2
}

function expectExtraResFatou(res: Result, precPente: number = 2) {
    expect(res.values.P).toBeCloseTo(0.3, 2, "P");
    expect(res.values.a).toBeCloseTo(0.12, 2, "a");
    expect(res.values.B).toBeCloseTo(0.36, 2, "B");
    expect(res.values.H).toBeCloseTo(0.798, 2, "H");
    expect(res.values.ZD1).toBeCloseTo(9.379, 2, "ZD1");
    expect(res.values.h).toBeCloseTo(0.664, 2, "h");
    expect(res.values.V).toBeCloseTo(1.045, 2, "V"); // Conforme à Larinier 1992, mais différent de Cv3 (1.347)
    expect(res.values.ZR1).toBeCloseTo(9.3, 2, "ZR1");
    expect(res.values.ZD2).toBeCloseTo(8.38, 2, "ZD2");
    expect(res.values.ZR2).toBeCloseTo(8.30, 2, "ZR2");
    expect(res.values.Nb).toBe(18, "Nb");
    // expect(res.values.ZM).toBeCloseTo(10.19, 2, "ZM"); // X manque 0.1
    // Hauteur minimale des murs latéraux ? (Hmin * cos(45°) + ?? de marge)
    expect(res.values.LPI).toBeCloseTo(5.1, precPente, "LPI"); // Cv3 : 5.5
    expect(res.values.LPH).toBeCloseTo(5, precPente, "LPH"); // Cv3 : 5.39
}

function expectExtraResSuperactive(res: Result) {
    expect(res.values.P).toBeCloseTo(0.26, 2, "P");
    expect(res.values.B).toBeCloseTo(0.6, 2, "B");
    expect(res.values.L).toBeCloseTo(0.6, 2, "L");
    expect(res.values.h).toBeCloseTo(0.427, 2, "h");
    expect(res.values.ZD1).toBeCloseTo(9.491, 2, "ZD1");
    expect(res.values.V).toBeCloseTo(1.568, 2, "V"); // conforme à Cv3
    expect(res.values.ZR1).toBeCloseTo(9.43, 2, "ZR1");
    expect(res.values.ZD2).toBeCloseTo(8.49, 2, "ZD2");
    expect(res.values.ZR2).toBeCloseTo(8.43, 2, "ZR2"); // X 0.04 de trop (0.037)
    expect(res.values.Nb).toBe(26, "Nb");
    // expect(res.values.ZM).toBeCloseTo(10.39, 2, "ZM"); // X @TODO
    expect(res.values.LPI).toBeCloseTo(6.76, 2, "LPI"); // Cv3 : 7.12
    expect(res.values.LPH).toBeCloseTo(6.68, 2, "LPH"); // Cv3 : 7.04
}

function expectExtraResChevron(res: Result) {
    expect(res.values.P).toBeCloseTo(0.4, 2, "P");
    expect(res.values.B).toBeCloseTo(0.65, 2, "B");
    expect(res.values.h).toBeCloseTo(0.406, 2, "h");
    expect(res.values.ZD1).toBeCloseTo(9.4, 2, "ha");
    expect(res.values.V).toBeCloseTo(1.9, 1, "V");
    expect(res.values.ZR1).toBeCloseTo(9.34, 2, "ZR1");
    expect(res.values.ZD2).toBeCloseTo(8.37, 2, "ZD2");
    expect(res.values.ZR2).toBeCloseTo(8.31, 2, "ZR2"); // X 0.06 de trop
    expect(res.values.Nb).toBe(26, "Nb");
    // expect(res.values.ZM).toBeCloseTo(10.39, 2, "ZM"); // X @TODO
    expect(res.values.LPI).toBeCloseTo(10.4, 2, "LPI");
    expect(res.values.LPH).toBeCloseTo(10.35, 2, "LPH");
}

describe("Class PAR −", () => {

    describe("PLANE, Cassiopée v3 default −", () => {

        it("calc ha, P fourni", () => {
            const prms = new ParParams(0.25, 10, 9, 666, 0.20, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.64, 2, "vCalc (ha)"); // Z1 = 9.36
            // check extra results
            expectExtraResPlane(res);
        });

        it("calc ha, P fourni, Z1 non fourni", () => {
            const prms = new ParParams(0.25, undefined, 9, 666, 0.20, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.64, 2, "vCalc (ha)"); // Z1 = 9.36
            // check extra results
            expect(res.values.P).toBeCloseTo(0.4, 2, "P");
            expect(res.values.B).toBeCloseTo(0.35, 2, "B");
            expect(res.values.C).toBeCloseTo(0.283, 2, "C");
            expect(res.values.D).toBeCloseTo(0.142, 2, "D");
            expect(res.values.h).toBeCloseTo(0.644, 2, "h");
            expect(res.values.Hmin).toBeCloseTo(1.11, 2, "Hmin"); // Conforme à Larinier 1992, mais différent de Cv3 (0.711)
            expect(res.values.Hmax).toBeCloseTo(1.32, 2, "Hmax");
            expect(res.values.V).toBeCloseTo(1.45, 2, "V"); // Conforme à Larinier 1992, mais différent de Cv3 (1.87)
            expect(res.values.ZM).toBeUndefined();
            expect(res.values.ZR1).toBeUndefined();
            expect(res.values.ZD1).toBeUndefined();
            expect(res.values.ZD2).toBeUndefined();
            expect(res.values.ZR2).toBeUndefined();
            expect(res.values.Nb).toBeUndefined();
            expect(res.values.LPI).toBeUndefined();
            expect(res.values.LPH).toBeUndefined();
        });

        it("calc Q, P fourni", () => {
            const prms = new ParParams(666, 10, 9, 0.64, 0.20, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.25, 2, "vCalc (Q)");
            // check extra results
            expectExtraResPlane(res);
        });

        it("calc ha, P non fourni", () => {
            const prms = new ParParams(0.25, 10, 9, 666, 0.20, undefined, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.64, 2, "vCalc (ha)");
            // check extra results
            expectExtraResPlane(res);
        });

        it("calc Q, P non fourni", () => {
            const prms = new ParParams(666, 10, 9, 0.64, 0.20, undefined, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.25, 2, "vCalc (Q)");
            // check extra results
            expectExtraResPlane(res);
        });
    });

    describe("FATOU, Cassiopée v3 default −", () => {

        it("calc ha, P fourni", () => {
            const prms = new ParParams(0.25, 10, 9.01, 666, 0.2, 0.3, 0.6);
            const par = new Par(prms);
            par.parType = ParType.FATOU;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.621, 2, "vCalc (ha)");
            // check extra results
            expectExtraResFatou(res);
        });

        it("calc Q, P fourni", () => {
            const prms = new ParParams(666, 10, 9.01, 0.621, 0.2, 0.3, 0.6);
            const par = new Par(prms);
            par.parType = ParType.FATOU;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.25, 2, "vCalc (Q)");
            // check extra results
            expectExtraResFatou(res);
        });

        it("calc ha, P non fourni", () => {
            const prms = new ParParams(0.25, 10, 9.01, 666, 0.2, undefined, 0.6);
            const par = new Par(prms);
            par.parType = ParType.FATOU;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.621, 2, "vCalc (ha)");
            // check extra results
            expectExtraResFatou(res);
        });

        it("calc Q, P non fourni", () => {
            const prms = new ParParams(666, 10, 9.01, 0.621, 0.2, undefined, 0.6);
            const par = new Par(prms);
            par.parType = ParType.FATOU;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.25, 2, "vCalc (Q)");
            // check extra results
            expectExtraResFatou(res);
        });
    });

    describe("SUPERACTIVE, Cassiopée v3 default −", () => {

        it("calc ha, P fourni", () => {
            const prms = new ParParams(0.4, 10, 9, 666, 0.15, 0.26, undefined, 0.1, 1);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.509, 2, "vCalc (ha)");
            // check extra results
            expectExtraResSuperactive(res);
        });

        it("calc Q, P fourni", () => {
            const prms = new ParParams(666, 10, 9, 0.509, 0.15, 0.26, undefined, 0.1, 1);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.4, 2, "vCalc (Q)");
            // check extra results
            expectExtraResSuperactive(res);
        });

        it("calc ha, P non fourni", () => {
            const prms = new ParParams(0.4, 10, 9, 666, 0.15, undefined, undefined, 0.1, 1);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.ha;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.509, 2, "vCalc (ha)");
            // check extra results
            expectExtraResSuperactive(res);
        });

        it("calc Q, P non fourni", () => {
            const prms = new ParParams(666, 10, 9, 0.509, 0.15, undefined, undefined, 0.1, 1);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.4, 2, "vCalc (Q)");
            // check extra results
            expectExtraResSuperactive(res);
        });

        it("calc Q, P non fourni, pente 10%, hauteur 0.15, 2 motifs", () => {
            const prms = new ParParams(666, 10, 9, 0.508, 0.1, undefined, undefined, 0.15, 2);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(1.186, 2, "vCalc (Q)");
            // check extra results
            expect(res.values.P).toBeCloseTo(0.39, 2, "P");
            expect(res.values.B).toBeCloseTo(1.8, 2, "B");
            expect(res.values.L).toBeCloseTo(0.9, 2, "L");
            expect(res.values.h).toBeCloseTo(0.455, 2, "h");
            expect(res.values.ZD1).toBeCloseTo(9.492, 2, "ZD1");
            expect(res.values.V).toBeCloseTo(1.448, 2, "V");
            expect(res.values.ZR1).toBeCloseTo(9.38, 2, "ZR1");
            expect(res.values.ZD2).toBeCloseTo(8.48, 2, "ZD2");
            expect(res.values.ZR2).toBeCloseTo(8.37, 2, "ZR2"); // X 0.04 de trop (0.037)
            expect(res.values.Nb).toBe(26, "Nb");
            // expect(res.values.ZM).toBeCloseTo(10.39, 2, "ZM"); // X @TODO
            expect(res.values.LPI).toBeCloseTo(10.14, 2, "LPI");
            expect(res.values.LPH).toBeCloseTo(10.09, 2, "LPH");
        });

        it("calc Q, P non fourni, pente 15%, hauteur 0.125, 1.5 motifs", () => {
            const prms = new ParParams(666, 10, 9, 0.64, 0.15, undefined, undefined, 0.125, 1.5);
            const par = new Par(prms);
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            // check extra results
            expect(res.values.P).toBeCloseTo(0.325, 2, "P");
            expect(res.values.B).toBeCloseTo(1.125, 2, "B");
            expect(res.values.L).toBeCloseTo(0.75, 2, "L");
            expect(res.values.h).toBeCloseTo(0.539, 2, "h");
            expect(res.values.ZD1).toBeCloseTo(9.36, 2, "ZD1");
            expect(res.values.ZR1).toBeCloseTo(9.285, 2, "ZR1");
            expect(res.values.ZD2).toBeCloseTo(8.348, 2, "ZD2");
            expect(res.values.ZR2).toBeCloseTo(8.272, 2, "ZR2");
            expect(res.values.Nb).toBe(21, "Nb");
            // expect(res.values.ZM).toBeCloseTo(10.39, 2, "ZM"); // X @TODO
            expect(res.values.LPI).toBeCloseTo(6.825, 2, "LPI");
            expect(res.values.LPH).toBeCloseTo(6.749, 2, "LPH");
        });
    });

    describe("CHEVRON, Cassiopée v3 default −", () => {

        it("calc Q", () => {
            const prms = new ParParams(666, 10, 9, 0.6, 0.10, 0.4, undefined, 0.1, 1, 1);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(0);
            expect(res.vCalc).toBeCloseTo(0.496, 2, "vCalc (Q)");
            // check extra results
            expectExtraResChevron(res);
        });

    });

    describe("P facultatif −", () => {

        it("P fourni, égal à la valeur standard", () => {
            const prms = new ParParams(0.25, 10, 9, 0.64, 0.20, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.values.P).toBeCloseTo(0.4, 2, "P");
        });

        it("P fourni, différent de la valeur standard", () => {
            const prms = new ParParams(0.25, 10, 9, 0.64, 0.20, 0.39, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.values.P).toBeCloseTo(0.39, 2, "P");
        });

        it("P fourni, trop éloigné de la valeur standard", () => {
            const prms = new ParParams(0.25, 10, 9, 0.64, 0.20, 0.2, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
            expect(res.values.P).toBeUndefined("P");
        });

        it("P non fourni", () => {
            const prms = new ParParams(0.25, 10, 9, 0.64, 0.20, undefined, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.values.P).toBeCloseTo(0.4, 2, "P");
        });

    });

    describe("limites diverses −", () => {

        it("Calc Q, Z1 trop élevé, ha en dehors des clous", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.74, 0.20, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_HA_OUT_OF_RANGE);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("S trop faible", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.05, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_S);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("S anormalement élevé", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.201, 0.6, 0.9);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_S);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("S trop élevé", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.23, 0.4, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_S);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Calc Q, L trop faible", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.2, 0.1333, 0.2);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_L);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Calc Q, L anormalement élevé", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.2, 0.8, 1.2);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_L);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("Calc Q, L trop élevé", () => {
            const prms = new ParParams(0.25, 10.2, 9, 0.64, 0.2, 1, 1.5);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_L);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("P trop faible", () => {
            const prms = new ParParams(0.25, 666, 9, 0.64, 0.20, 0.359, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Z1;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
            expect(res.vCalc).toBeUndefined("vCalc (Z1)");
        });

        it("P trop élevé", () => {
            const prms = new ParParams(0.25, 666, 9, 0.64, 0.20, 0.421, 0.6);
            const par = new Par(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Z1;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
            expect(res.vCalc).toBeUndefined("vCalc (Z1)");
        });

        it("a trop faible", () => {
            const prms = new ParParams(666, 10, 9, 0.666, 0.15, 0.16, undefined, 0.04, 1, 1);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_A);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("a trop élevé", () => {
            const prms = new ParParams(666, 10, 9, 0.666, 0.15, 1, undefined, 0.26, 1, 1);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_A);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("N arrondi", () => {
            const prms = new ParParams(666, 10, 9, 0.666, 0.15, 0.4, undefined, 0.1, 1.3, 1);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_N_ROUNDED_TO_05);
            expect(res.log.messages[0].extraVar.val).toBe(1.5);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("M arrondi", () => {
            const prms = new ParParams(666, 10, 9, 0.666, 0.15, 0.4, undefined, 0.1, 1, 1.12);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_M_ROUNDED_TO_1);
            expect(res.log.messages[0].extraVar.val).toBe(1);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("M trop élevé", () => {
            const prms = new ParParams(666, 10, 9, 0.666, 0.15, 0.4, undefined, 0.1, 1.5, 4);
            const par = new Par(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_M_GREATER_THAN_2_N);
            expect(res.log.messages[0].extraVar.max).toBe(3);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("précision des inégalités : variation de S", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-07-27T10:32:22.510Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"dHFlcH","props":{"calcType":"Par","parType":"SUPERACTIVE"},"meta":{"title":"PAR : calage"},"children":[],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":10},{"symbol":"Z2","mode":"SINGLE","value":9},{"symbol":"ha","mode":"SINGLE","value":0.5},{"symbol":"S","mode":"MINMAX","min":0.09,"max":0.11,"step":0.01,"extensionStrategy":0},{"symbol":"P","mode":"SINGLE"},{"symbol":"a","mode":"SINGLE","value":0.1},{"symbol":"N","mode":"SINGLE","value":1}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const par = Session.getInstance().findNubByUid("dHFlcH");
            const res = par.CalcSerie();
            expect(res.resultElements.length).toBe(3);
            expect(res.resultElements[1].log.messages.length).toBe(0, "0.999999 should not be considered < 1 (epsilon inequality)");
        });
    });

});
