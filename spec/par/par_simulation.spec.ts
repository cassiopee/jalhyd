import { ParType } from "../../src/par/par";
import { MessageCode } from "../../src/util/message";
import { ParSimulationParams } from "../../src/par/par_simulation_params";
import { ParSimulation, ParFlowRegime } from "../../src/par/par_simulation";
import { Result } from "../../src/util/result";

function testParSimResults(res: Result, v: any[]) {
    let i = 0;
    for (const r of res.resultElements) {
        if (v[i].message !== undefined) {
            expect(r.log.messages.length).toBe(1, "values[" + i + "], log");
            expect(r.log.messages[0].code).toBe(v[i].message, "values[" + i + "], log message code");
        } else {
            expect(r.log.messages.length).toBe(0, "values[" + i + "], log");
            expect(r.vCalc).toBeCloseTo(v[i][r.vCalcSymbol], 2, `values[${i}], ${r.vCalcSymbol}`);
            expect(r.values.h).toBeCloseTo(v[i].h, 2, "values[" + i + "], h");
            expect(r.values.ha).toBeCloseTo(v[i].ha, 2, "values[" + i + "], ha");
            // expect(r.values.V).toBeCloseTo(values[i].V, 2, "values[" + i + "], V");
            // expect(r.values.ENUM_ParFlowRegime).toBe(values[i].regime, "values[" + i + "], ENUM_ParFlowRegime");
            // @TODO ha/L
        }
        i++;
    }
}

function testParSimGeometry(
    res: Result, logLength: number, logCode: number, logExtra: string,
    nb: number, ZR1: number, ZR2: number
) {
    expect(res.globalLog.messages.length).toBe(logLength, "global log");
    if (logCode !== undefined) {
        expect(res.globalLog.messages[0].code).toBe(logCode);
        expect(res.globalLog.messages[0].extraVar.nb).toBe(logExtra);
    }
    if (prms.Nb.singleValue !== undefined) {
        expect(prms.Nb.singleValue).toBe(nb, "Nb");
    } else {
        // result might be variating and some iterations might have failed
        for (const re of res.resultElements) {
            if (re.values.Nb !== undefined) {
                expect(re.values.Nb).toBe(nb, "Nb");
            }
        }
    }
    if (prms.ZR1.singleValue !== undefined) {
        expect(prms.ZR1.singleValue).toBeCloseTo(ZR1, 2, "ZR1");
    } else {
        // result might be variating and some iterations might have failed
        for (const re of res.resultElements) {
            if (re.values.ZR1 !== undefined) {
                expect(re.values.ZR1).toBeCloseTo(ZR1, 2, "ZR1");
            }
        }
    }
    if (prms.ZR2.singleValue !== undefined) {
        expect(prms.ZR2.singleValue).toBeCloseTo(ZR2, 2, "ZR2");
    } else {
        // result might be variating and some iterations might have failed
        for (const re of res.resultElements) {
            if (re.values.ZR2 !== undefined) {
                expect(re.values.ZR2).toBeCloseTo(ZR2, 2, "ZR2");
            }
        }
    }
}

let prms: ParSimulationParams;
let par: ParSimulation;
let values: any[] = [];

describe("Class ParSimulation −", () => {

    describe("PLANE, Cassiopée v3 −", () => {

        beforeEach(() => {
            prms = new ParSimulationParams(666, 10, 9, 0.15, 0.45, undefined, undefined, 9.36, undefined, 8.32, 0.7);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            values = [
                {
                    Z1: 7, // (Z1 < Z2 mais ça ne change rien dans Cv3)
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                },
                {
                    Z1: 9.9,
                    Q: 0.197,
                    h: 0.579,
                    ha: 0.54,
                    // V: 1.568, // foo-track
                    // regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10,
                    Q: 0.257,
                    h: 0.667,
                    ha: 0.64,
                    // V: 1.663,
                    // regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10.1,
                    Q: 0.322,
                    h: 0.75,
                    ha: 0.74,
                    // V: 1.778,
                    // regime: ParFlowRegime.FREE
                    message: MessageCode.WARNING_PAR_NOT_SUBMERGED
                },
                {   Z1: 14, // (dans Cv3, "calcul" est "off", sous-entendant que le calcul a raté ? Mais il y a tout de même des valeurs…)
                    /* vCalc: 0.549,
                    h: 0.937,
                    ha: 4.64,
                    V: 2.114, */
                    regime: ParFlowRegime.FREE,
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                }
            ];
        });

        it("calc Q, Z1 varié", () => {
            par.calculatedParam = prms.Q;
            prms.Z1.setValues(values.map(a => a.Z1));
            const res = par.CalcSerie();
            testParSimGeometry(res, 2, MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, "2", 16, 9.227, 8.187);
            testParSimResults(res, values)
        });

        it("calc Z1, Q varié", () => {
            par.calculatedParam = prms.Z1;
            values.shift();
            values.pop();
            prms.Q.setValues(values.map(a => a.Q));
            const res = par.CalcSerie();
            testParSimGeometry(res, 1, MessageCode.WARNING_WARNINGS_ABSTRACT, "1", 16, 9.227, 8.187);
            testParSimResults(res, values);
        });

    });

    describe("FATOU, Cassiopée v3 −", () => {

        beforeEach(() => {
            prms = new ParSimulationParams(666, 10, 9, 0.15, 0.34, undefined, undefined, 9.36, undefined, 8.32, 0.7);
            par = new ParSimulation(prms);
            par.parType = ParType.FATOU;
            values = [
                {
                    Z1: 7, // (Z1 < Z2 mais ça ne change rien dans Cv3)
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                },
                {
                    Z1: 9.9,
                    Q: 0.253,
                    h: 0.661,
                    ha: 0.54,
                    V: 1.1117
                },
                {
                    Z1: 10,
                    Q: 0.311,
                    h: 0.751,
                    ha: 0.64,
                    V: 1.186,
                    message: MessageCode.WARNING_PAR_NOT_SUBMERGED
                },
                {
                    Z1: 10.1,
                    Q: 0.374,
                    h: 0.824,
                    ha: 0.74,
                    V: 1.247,
                    message: MessageCode.WARNING_PAR_NOT_SUBMERGED
                },
                {
                    Z1: 14, // (dans Cv3, "calcul" est "off", sous-entendant que le calcul a raté ? Mais il y a tout de même des valeurs…)
                    /* vCalc: 0.549,
                    h: 0.892,
                    ha: 4.64,
                    V: 1.334, */
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                }
            ];
        });
        it("calc Q, Z1 varié", () => {
            par.calculatedParam = prms.Q;
            prms.Z1.setValues(values.map(a => a.Z1));
            const res = par.CalcSerie();
            testParSimGeometry(res, 2, MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, "2", 21, 9.252, 8.212);
            testParSimResults(res, values)
        });

        it("calc Z1, Q varié", () => {
            par.calculatedParam = prms.Z1;
            values.shift();
            values.pop();
            prms.Q.setValues(values.map(a => a.Q));
            const res = par.CalcSerie();
            testParSimGeometry(res, 1, MessageCode.WARNING_WARNINGS_ABSTRACT, "2", 21, 9.252, 8.212);
            testParSimResults(res, values);
        });
    });

    describe("SUPERACTIVE, Cassiopée v3 −", () => {
        beforeEach(() => {
            prms = new ParSimulationParams(666, 10, 9, 0.15, 0.325, undefined, undefined, 9.36, undefined, 8.348, undefined, 0.125, 1.5);
            par = new ParSimulation(prms);
            par.parType = ParType.SUPERACTIVE;
            values = [
                {
                    Z1: 7, // (Z1 < Z2 mais ça ne change rien dans Cv3)
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                },
                {
                    Z1: 9.5,
                    Q: 0.146,
                    h: 0.127,
                    ha: 0.14,
                    V: 1.072,
                    regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10,
                    Q: 1.071,
                    h: 0.537,
                    ha: 0.64,
                    V: 1.759,
                    regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10.1,
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                }
            ];
        });
        it("calc Q, Z1 varié", () => {
            par.calculatedParam = prms.Q;
            prms.Z1.setValues(values.map(a => a.Z1));
            const res = par.CalcSerie();
            testParSimGeometry(res, 1, MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, "2", 21, 9.285, 8.272);
            testParSimResults(res, values)
        });

        it("calc Z1, Q varié", () => {
            par.calculatedParam = prms.Z1;
            values.shift();
            values.pop();
            prms.Q.setValues(values.map(a => a.Q));
            const res = par.CalcSerie();
            testParSimGeometry(res, 0, undefined, undefined, 21, 9.285, 8.272);
            testParSimResults(res, values);
        });
    });

    describe("CHEVRON, Cassiopée v3 −", () => {
        beforeEach(() => {
            prms = new ParSimulationParams(666, 10, 9, 0.1, 0.52, undefined, undefined, 9.4, undefined, 8.365, undefined, 0.13, 1.5, 3);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            values = [
                {
                    Z1: 7, // (Z1 < Z2 mais ça ne change rien dans Cv3)
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                },
                {
                    Z1: 9.9,
                    Q: 0.796,
                    h: 0.336,
                    ha: 0.5,
                    V: 1.691,
                    regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10,
                    Q: 1.049,
                    h: 0.404,
                    ha: 0.6,
                    V: 1.878,
                    regime: ParFlowRegime.SUBMERGED
                },
                {
                    Z1: 10.1,
                    Q: 1.321,
                    h: 0.473,
                    ha: 0.7,
                    V: 2.05,
                    regime: ParFlowRegime.FREE
                },
                {
                    Z1: 14,
                    message: MessageCode.ERROR_PAR_HA_OUT_OF_RANGE
                }
            ];
        });
        it("calc Q, Z1 varié", () => {
            par.calculatedParam = prms.Q;
            prms.Z1.setValues(values.map(a => a.Z1));
            const res = par.CalcSerie();
            testParSimGeometry(res, 1, MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, "2", 20, 9.322, 8.287);
            testParSimResults(res, values)
        });

        it("calc Z1, Q varié", () => {
            par.calculatedParam = prms.Z1;
            values.shift();
            values.pop();
            prms.Q.setValues(values.map(a => a.Q));
            const res = par.CalcSerie();
            testParSimGeometry(res, 0, undefined, undefined, 20, 9.322, 8.287);
            testParSimResults(res, values);
        });
    });

    describe("limites diverses −", () => {

        it("Calc Q, Z1 trop élevé, ha en dehors des clous", () => {
            prms = new ParSimulationParams(0.25, 10.2, 9, 0.15, 0.4, 18, 9.24, 9.36, 8.2, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_HA_OUT_OF_RANGE);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("S trop faible", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.05, 0.4, 15, 9.24, 9.36, 8.2, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(4);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_S);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("S anormalement élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.201, 0.4, 14, 9.24, 9.36, 8.2, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_S);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("S trop élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.23, 0.4, 13, 9.24, 9.36, 8.2, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_S);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Calc Q, L trop faible", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.1, 0.13, 14, 9.24, 9.36, 8.2, 8.32, 0.2);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(5);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_L);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Calc Q, L anormalement élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.1, 0.8, 14, 9.14, 9.36, 8.1, 8.32, 1.2);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_L);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("Calc Q, L trop élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.1, 1, 14, 9.24, 9.36, 8.2, 8.32, 1.5);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(4);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_L);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("P trop faible", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.1, 0.359, 14, 9.14, 9.36, 8.1, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Z1;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(4);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
            expect(res.vCalc).toBeUndefined("vCalc (Z1)");
        });

        it("P trop élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.1, 0.421, 14, 9.14, 9.36, 8.1, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Z1;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(4);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
            expect(res.vCalc).toBeUndefined("vCalc (Z1)");
        });

        it("a trop faible", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, 0.16, 14, 9.14, 9.36, 8.1, 8.32, undefined, 0.04, 1, 1);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(5);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_A);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("a trop élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, 1, 14, 9.14, 9.36, 8.1, 8.32, undefined, 0.26, 1, 1);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(4);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_A);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("N arrondi", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.10, undefined, undefined, undefined, 9.36, undefined, 8.32, undefined, 0.1, 1.3, 1);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_N_ROUNDED_TO_05);
            expect(res.log.messages[0].extraVar.val).toBe(1.5);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("M arrondi", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.305, undefined, 8.265, undefined, undefined, 0.1, 1.5, 1.12);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_M_ROUNDED_TO_1);
            expect(res.log.messages[0].extraVar.val).toBe(1);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
        });

        it("M trop élevé", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, 0.4, undefined, 9.305, undefined, 8.2, undefined, undefined, 0.1, 1.5, 4);
            par = new ParSimulation(prms);
            par.parType = ParType.CHEVRON;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_M_GREATER_THAN_2_N);
            expect(res.log.messages[0].extraVar.max).toBe(3);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Nb non fourni", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, 0.41, undefined, 9.24, undefined, 8.2, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
            expect(res.values.Nb).toBe(18, "Nb");
        });

        it("Nb incohérent", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, 0.41, 14, 9.24, 9.36, 8.2, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_NB_INCONSISTENT);
            expect(res.log.messages[0].extraVar.stdNb).toBe("18");
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("P non fourni", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.24, undefined, 8.2, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
            expect(res.values.P).toBeCloseTo(0.4, 3, "P");
        });

        it("Cotes incohérentes ZR1/ZD1", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.2, 9.36, 8.2, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_ZR_ZD_MISMATCH);
            expect(res.log.messages[0].extraVar.var_ZR).toBe("ZR1");
            expect(res.log.messages[0].extraVar.var_ZD).toBe("ZD1");
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Cotes incohérentes ZR2/ZD2", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.24, undefined, 8.19, 8.32, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_PAR_ZR_ZD_MISMATCH);
            expect(res.log.messages[0].extraVar.var_ZR).toBe("ZR2");
            expect(res.log.messages[0].extraVar.var_ZD).toBe("ZD2");
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Cotes manquantes : ZR1 / ZD1", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, undefined, undefined, 8.2, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED);
            expect(res.log.messages[0].extraVar.variables).toEqual(["ZR1", "ZD1"]);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Cotes manquantes : ZR2 / ZD2", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.24, undefined, undefined, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED);
            expect(res.log.messages[0].extraVar.variables).toEqual(["ZR2", "ZD2"]);
            expect(res.vCalc).toBeUndefined("vCalc (Q)");
        });

        it("Cotes : ZR1 et ZR2 fournis, calcul de ZD1 et ZD2", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, 9.24, undefined, 8.2, undefined, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
            expect(res.values.ZD1).toBeCloseTo(9.354, 3, "ZD1");
            expect(res.values.ZD2).toBeCloseTo(8.314, 3, "ZD2");
        });

        it("Cotes : ZD1 et ZD2 fournis, calcul de ZR1 et ZR2", () => {
            prms = new ParSimulationParams(0.25, 10, 9, 0.15, undefined, undefined, undefined, 9.354, undefined, 8.314, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            expect(res.vCalc).toBeDefined("vCalc (Q)");
            expect(res.values.ZR1).toBeCloseTo(9.24, 3, "ZR1");
            expect(res.values.ZR2).toBeCloseTo(8.2, 3, "ZR2");
        });

        it("Ennoiement par l'aval insuffisant", () => {
            prms = new ParSimulationParams(0.25, 10, 8, 0.15, undefined, undefined, undefined, 9.354, undefined, 8.314, 0.6);
            par = new ParSimulation(prms);
            par.parType = ParType.PLANE;
            par.calculatedParam = prms.Q;
            const res = par.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_PAR_NOT_SUBMERGED);
            expect(res.log.messages[0].extraVar.DH).toBeCloseTo(0.966, 3);
        });
    });

});
