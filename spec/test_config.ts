// tslint:disable-next-line:no-reference
/// <reference path="../node_modules/@types/jasmine/index.d.ts" />

/**
 * précision de calcul (nombre de décimales)
 */
export let precDigits = 3;

/**
 * précision de calcul (max de abs(v-v'))
 */
export let precDist: number = Math.pow(10, -precDigits);
