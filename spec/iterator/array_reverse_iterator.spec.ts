import { ArrayReverseIterator } from "../../src/util/array_reverse_iterator";

describe("array reverse iterator : ", () => {
    it("reverse ( undefined )", () => {
        const arr: object[] = undefined;
        const it = new ArrayReverseIterator<object>(arr);
        expect(it.next().done).toBeTruthy();
    });

    it("reverse( [] )", () => {
        const arr: object[] = [];
        const it = new ArrayReverseIterator<object>(arr);
        expect(it.next().done).toBeTruthy();
    });

    it("reverse( [1] )", () => {
        const arr: number[] = [1];
        const it = new ArrayReverseIterator<number>(arr);
        const v1 = it.next();
        expect(v1.done).toBeFalsy();
        expect(v1.value).toEqual(1);
        const v2 = it.next();
        expect(v2.done).toBeTruthy();
    });

    it("reverse( [1,2] )", () => {
        const arr: number[] = [1, 2];
        const it = new ArrayReverseIterator<number>(arr);
        const v1 = it.next();
        expect(v1.done).toBeFalsy();
        expect(v1.value).toEqual(2);
        const v2 = it.next();
        expect(v2.done).toBeFalsy();
        expect(v2.value).toEqual(1);
        const v3 = it.next();
        expect(v3.done).toBeTruthy();
    });
});
