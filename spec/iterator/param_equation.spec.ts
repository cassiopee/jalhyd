import { IParamDefinitionIterator } from "../../src/param/param_definition_iterator";
import { ConduiteDistribParams } from "../../src/pipe_flow/cond_distri_params";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { Structure } from "../../src/structure/structure";
import { LoiDebit } from "../../src/structure/structure_props";

function checkParams(pdi: IParamDefinitionIterator, symbols: string[], values: number[]) {
    let n = 0;
    for (const p of pdi) {
        expect(p.symbol).toBeDefined();
        expect(p.singleValue).toBeDefined();
        n++;
    }
    expect(n).toEqual(symbols.length);
}

describe("iterator  : ", () => {
    it("ConduiteDistribParams", () => {
        const peq: ConduiteDistribParams = new ConduiteDistribParams(1, 2, 3, 4, 5);
        const symbs = ["Q", "D", "J", "Lg", "Nu"];
        const vals = [1, 2, 3, 4, 5];
        checkParams(peq.iterator, symbs, vals);
    });

    it("ParallelStructureParams 1", () => {
        const peq: ParallelStructureParams = new ParallelStructureParams(1, 2, 3);
        const symbs = ["Q", "Z1", "Z2"];
        const vals = [1, 2, 3];
        checkParams(peq.iterator, symbs, vals);
    });

    it("ParallelStructureParams 2", () => {
        const psp: ParallelStructureParams = new ParallelStructureParams(1, 2, 3);
        const pst = new ParallelStructure(psp);

        const st: Structure = CreateStructure(LoiDebit.WeirCem88d, pst);
        pst.addChild(st);

        const symbs = [
            "Q", "Z1", "Z2", "CdWR", "CdGR", "CdGRS", "CdCunge", "CdWSL", "CdWS", "h1", "h2", "L", "Q", "W", "Z1", "Z2", "ZDV"
        ];
        const vals = [1, 2, 3];
        checkParams(pst.parameterIterator, symbs, vals);
    });
});
