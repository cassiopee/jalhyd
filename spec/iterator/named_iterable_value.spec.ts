import { ParamDefinition, ParamDomainValue, Result, ResultElement } from "../../src/index";

function testParamDefinitionValues(vals: number[]): ParamDefinition {
    const p: ParamDefinition = new ParamDefinition(null, "aa", ParamDomainValue.ANY);

    p.paramValues.setValues(vals);

    let n = 0;
    for (const v of p.valuesIterator) {
        expect(v).toEqual(vals[n++]);
    }

    expect(n).toEqual(vals.length);

    return p;
}

function testResultValues(vals: number[]): Result {
    const r: Result = new Result();

    for (const v of vals) {
        r.addResultElement(new ResultElement(v));
    }

    let n = 0;
    for (const v of r.getCalculatedValues()) {
        expect(v).toEqual(vals[n++]);
    }

    expect(n).toEqual(vals.length);

    return r;
}

/* function testExtraResultsValues(vals: number[]): ExtraResults {
    const ers: ExtraResults = new ExtraResults("aa");

    for (const v of vals) {
        ers.addValue(v);
    }

    let n = 0;
    for (const v of ers.valuesIterator) {
        expect(v).toEqual(vals[n++]);
    }

    expect(n).toEqual(vals.length);

    return ers;
} */

describe("INamedIterableValues  : ", () => {
    describe("ParamDefinition  : ", () => {
        it("test 1", () => {
            const name = "aa";
            const p: ParamDefinition = new ParamDefinition(null, name, ParamDomainValue.ANY);
            expect(p.symbol).toEqual(name);
        });

        it("test 2", () => {
            const vals: number[] = [];
            const p = testParamDefinitionValues(vals);
            expect(p.hasMultipleValues).toBeFalsy();
        });

        it("test 3", () => {
            const vals: number[] = [0];
            const p = testParamDefinitionValues(vals);
            expect(p.hasMultipleValues).toBeFalsy();
        });

        it("test 4", () => {
            const vals: number[] = [0, 1];
            const p = testParamDefinitionValues(vals);
            expect(p.hasMultipleValues).toBeTruthy();
        });
    });

    describe("Result  : ", () => {
        it("test 1", () => {
            const name = "aa";
            const r: Result = new Result(0);
            r.symbol = name;
            expect(r.symbol).toEqual(name);
        });

        it("test 2", () => {
            const vals: number[] = [];
            const r = testResultValues(vals);
            expect(r.hasMultipleValues()).toBeFalsy();
        });

        it("test 3", () => {
            const vals: number[] = [0];
            const r = testResultValues(vals);
            expect(r.hasMultipleValues()).toBeFalsy();
        });

        it("test 3", () => {
            const vals: number[] = [0, 1];
            const r = testResultValues(vals);
            expect(r.hasMultipleValues()).toBeTruthy();
        });
    });
});
