import { MapIterator } from "../../src/util/map_iterator";

describe("param map iterator : ", () => {
    it("sur undefined (1)", () => {
        const it: MapIterator<any> = new MapIterator(undefined);

        const v1 = it.next();
        expect(v1.done).toBeTruthy();
        expect(v1.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();

        const v2 = it.next();
        expect(v2.done).toBeTruthy();
        expect(v2.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();
    });

    it("sur undefined (2)", () => {
        const it: MapIterator<any> = new MapIterator(undefined);
        let n = 0;
        for (const e of it) {
            n++;
        }

        expect(n).toEqual(0);
    });

    it("sur {}", () => {
        const it: MapIterator<any> = new MapIterator({});

        const v1 = it.next();
        expect(v1.done).toBeTruthy();
        expect(v1.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();

        const v2 = it.next();
        expect(v2.done).toBeTruthy();
        expect(v2.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();
    });

    it("sur {} (2)", () => {
        const it: MapIterator<any> = new MapIterator({});
        let n = 0;
        for (const e of it) {
            n++;
        }

        expect(n).toEqual(0);
    });

    it("sur {'a':1} (1)", () => {
        const o = { a: 1 };
        const it: MapIterator<number> = new MapIterator(o);

        const v1 = it.next();
        expect(v1.done).toBeFalsy();
        expect(v1.value).toEqual(1);
        expect(it.key).toEqual("a");
        expect(it.index).toEqual(0);

        const v2 = it.next();
        expect(v2.done).toBeTruthy();
        expect(v2.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();
    });

    it("sur {'a':1} (2)", () => {
        const o = { a: 1 };
        const it: MapIterator<any> = new MapIterator(o);
        let n = 0;
        for (const e of it) {
            n++;
            expect(e).toEqual(n);
        }

        expect(n).toEqual(1);
    });

    it("sur {'a':1, 'b':2} (1)", () => {
        const o = { a: 1, b: 2 };
        const it: MapIterator<number> = new MapIterator(o);

        const v1 = it.next();
        expect(v1.done).toBeFalsy();
        expect(v1.value).toEqual(1);
        expect(it.key).toEqual("a");

        const v2 = it.next();
        expect(v2.done).toBeFalsy();
        expect(v2.value).toEqual(2);
        expect(it.key).toEqual("b");

        const v3 = it.next();
        expect(v3.done).toBeTruthy();
        expect(v3.value).toBeUndefined();
        expect(it.key).toBeUndefined();
        expect(it.index).toBeUndefined();
    });

    it("sur {'a':1, 'b':2} (2)", () => {
        const o = { a: 1, b: 2 };
        const it: MapIterator<any> = new MapIterator(o);
        let n = 0;
        for (const e of it) {
            n++;
            expect(e).toEqual(n);
        }

        expect(n).toEqual(2);
    });
});
