import { ExtensionStrategy } from "../../src/index";
import { INumberIterator } from "../../src/param/param-value-iterator";
import { ParamValueMode } from "../../src/param/param-value-mode";
import { ParamValues } from "../../src/param/param-values";

function checkNumberList(it: INumberIterator, exp: number[]) {
    let n = 0;
    for (const v of it) {
        expect(v).toEqual(exp[n]);
        n++;
    }
    expect(n).toEqual(exp.length);
}

describe("paramvalues iterator : ", () => {
    it("vide (1)", () => {
        const p = new ParamValues();
        expect(() => p.getValuesIterator()).toThrow(
            new Error("ParamValueIterator : mode undefined incorrect")
        );
    });

    it("vide (2)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.SINGLE;
        expect(() => p.getValuesIterator()).toThrow(new Error("ParamValues : valeur fixe non définie"));
    });

    it("vide (3)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.MINMAX;
        expect(() => p.getValuesIterator()).toThrow(new Error("ParamValues : valeur min non définie"));
    });

    it("vide (4)", () => {
        const p = new ParamValues();
        p.valueMode = ParamValueMode.LISTE;
        expect(() => p.getValuesIterator()).toThrow(new Error("ParamValues : liste de valeurs non définie"));
    });

    it("single (1)", () => {
        const p = new ParamValues();
        p.setValues(0);
        const it = p.getValuesIterator();
        expect(it.hasNext).toBeTruthy();
        const n1 = it.next();
        expect(n1.value).toEqual(0);
        expect(n1.done).toBeFalsy();
        expect(it.hasNext).toBeFalsy();
        const n2 = it.next();
        expect(n2.value).toBeUndefined();
        expect(n2.done).toBeTruthy();
    });

    it("single (2)", () => {
        const p = new ParamValues();
        p.setValues(0);
        checkNumberList(p.getValuesIterator(), [0]);
    });

    it("minmax (1)", () => {
        const p = new ParamValues();
        p.setValues(0, 1, 2);
        checkNumberList(p.getValuesIterator(), [0]);
    });

    it("minmax (2)", () => {
        const p = new ParamValues();
        p.setValues(0, 1, 0.5);
        checkNumberList(p.getValuesIterator(), [0, 0.5, 1]);
    });

    it("liste (1)", () => {
        const p = new ParamValues();
        p.setValues([]);
        const it = p.getValuesIterator();
        expect(it.next().done).toBeTruthy();
        checkNumberList(p.getValuesIterator(), []);
    });

    it("liste (2)", () => {
        const p = new ParamValues();
        p.setValues([0]);
        checkNumberList(p.getValuesIterator(), [0]);
    });

    it("liste (3)", () => {
        const p = new ParamValues();
        p.setValues([1, 0.1]);
        checkNumberList(p.getValuesIterator(), [1, 0.1]);
    });
});

describe("extended paramvalues iterator : ", () => {

    it("minmax (multiple, forward, no extension, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        const i = p.initValuesIterator(false, undefined, true);
        expect(i.count()).toBe(5);
        expect(p.getInferredValuesList(false, undefined, true)).toEqual([ 0, 5, 10, 15, 20 ]);
    });

    it("minmax (not multiple, forward, no extension, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        const i = p.initValuesIterator(false, undefined, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(false, undefined, true)).toEqual([ 0, 5, 10, 15, 20, 22 ]);
    });

    it("minmax (multiple, forward, extension REPEAT, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(false, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(false, 8, false)).toEqual([ 0, 5, 10, 15, 20, 20, 20, 20 ]);
    });

    it("minmax (not multiple, forward, extension REPEAT, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(false, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(false, 8, false)).toEqual([ 0, 5, 10, 15, 20, 20, 20, 20 ]);
    });

    it("minmax (multiple, forward, extension REPEAT, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(false, 6, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(false, 6, true)).toEqual([ 0, 5, 10, 15, 20, 20 ]);
    });

    it("minmax (not multiple, forward, extension REPEAT, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(false, 7, true);
        expect(i.count()).toBe(7);
        expect(p.getInferredValuesList(false, 7, true)).toEqual([ 0, 5, 10, 15, 20, 22, 22 ]);
    });

    it("minmax (multiple, forward, extension RECYCLE, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(false, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(false, 8, false)).toEqual([ 0, 5, 10, 15, 20, 0, 5, 10 ]);
    });

    it("minmax (not multiple, forward, extension RECYCLE, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(false, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(false, 8, false)).toEqual([ 0, 5, 10, 15, 20, 0, 5, 10 ]);
    });

    it("minmax (multiple, forward, extension RECYCLE, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(false, 6, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(false, 6, true)).toEqual([ 0, 5, 10, 15, 20, 0 ]);
    });

    it("minmax (not multiple, forward, extension RECYCLE, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(false, 7, true);
        expect(i.count()).toBe(7);
        expect(p.getInferredValuesList(false, 7, true)).toEqual([ 0, 5, 10, 15, 20, 22, 0 ]);
    });

    it("minmax (multiple, reverse, no extension, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        const i = p.initValuesIterator(true, undefined, true);
        expect(i.count()).toBe(5);
        expect(p.getInferredValuesList(true, undefined, true)).toEqual([ 20, 15, 10, 5, 0 ]);
    });

    it("minmax (not multiple, reverse, no extension, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        const i = p.initValuesIterator(true, undefined, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(true, undefined, true)).toEqual([ 22, 17, 12, 7, 2, 0 ]);
    });

    it("minmax (multiple, reverse, extension REPEAT, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(true, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(true, 8, false)).toEqual([ 20, 15, 10, 5, 0, 0, 0, 0 ]);
    });

    it("minmax (not multiple, reverse, extension REPEAT, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(true, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(true, 8, false)).toEqual([ 22, 17, 12, 7, 2, 2, 2, 2 ]);
    });

    it("minmax (multiple, reverse, extension REPEAT, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(true, 6, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(true, 6, true)).toEqual([ 20, 15, 10, 5, 0, 0 ]);
    });

    it("minmax (not multiple, reverse, extension REPEAT, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        const i = p.initValuesIterator(true, 7, true);
        expect(i.count()).toBe(7);
        expect(p.getInferredValuesList(true, 7, true)).toEqual([ 22, 17, 12, 7, 2, 0, 0 ]);
    });

    it("minmax (multiple, reverse, extension RECYCLE, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(true, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(true, 8, false)).toEqual([ 20, 15, 10, 5, 0, 20, 15, 10 ]);
    });

    it("minmax (not multiple, reverse, extension RECYCLE, no last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(true, 8, false);
        expect(i.count()).toBe(8);
        expect(p.getInferredValuesList(true, 8, false)).toEqual([ 22, 17, 12, 7, 2, 22, 17, 12 ]);
    });

    it("minmax (multiple, reverse, extension RECYCLE, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 20, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(true, 6, true);
        expect(i.count()).toBe(6);
        expect(p.getInferredValuesList(true, 6, true)).toEqual([ 20, 15, 10, 5, 0, 20 ]);
    });

    it("minmax (not multiple, reverse, extension RECYCLE, last step)", () => {
        const p = new ParamValues();
        p.setValues(0, 22, 5);
        p.extensionStrategy = ExtensionStrategy.RECYCLE;
        const i = p.initValuesIterator(true, 7, true);
        expect(i.count()).toBe(7);
        expect(p.getInferredValuesList(true, 7, true)).toEqual([ 22, 17, 12, 7, 2, 0, 22 ]);
    });

});
