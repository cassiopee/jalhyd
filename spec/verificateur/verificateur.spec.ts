import { Session } from "../../src/session";
import { Verificateur } from "../../src/verification/verificateur";
import { Pab } from "../../src/pab/pab";
import { Props } from "../../src/props";
import { CalculatorType } from "../../src/compute-node";
import { CloisonAval } from "../../src/pab/cloison_aval";
import { FishSpecies } from "../../src/verification/fish_species";
import { ParSimulation } from "../../src/par/par_simulation";
import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { MacrorugoCompound } from "../../src/macrorugo/macrorugo_compound";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureWeirVillemonte } from "../../src/structure/structure_weir_villemonte";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";
import { Cloisons } from "../../src/pab/cloisons";
import { MessageCode } from "../../src/util/message";
import { ParType } from "../../src/par/par";
import { MRCInclination } from "../../src/macrorugo/mrc-inclination";
import { Espece } from "../../src/verification/espece";
import { StructureJetType } from "../../src/structure/structure";
import { EspeceParams } from "../../src/verification/espece_params";
import { DivingJetSupport } from "../../src/verification/diving-jet-support";

function createPab(): Pab {
    const pab = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.Pab })
    ) as Pab;
    const cl = Session.getInstance().createNub(
        new Props({ calcType: CalculatorType.Cloisons })
    ) as Cloisons;
    cl.setParent(pab);
    pab.children.push(cl);
    pab.children[0].structures[0] = CreateStructure(LoiDebit.WeirSubmergedLarinier, cl);
    const dw = Session.getInstance().createNub(
        new Props({ calcType: CalculatorType.CloisonAval })
    ) as CloisonAval;
    dw.addChild(Session.getInstance().createNub(
        new Props({
            calcType: CalculatorType.Structure,
            loiDebit: dw.getDefaultLoiDebit()
        })
    ));
    pab.downWall = dw
    return pab;
}

function loadExamplePab(): Pab {
    const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-04-03T07:34:45.155Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Y3k4bj","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"ampiN3","props":{"calcType":"Cloisons"},"children":[{"uid":"Yzgxa2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":28.085},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.413},{"symbol":"ZRAM","mode":"SINGLE","value":27.526},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"c3RmMz","props":{"calcType":"Cloisons"},"children":[{"uid":"dmwyem","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.858},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.186},{"symbol":"ZRAM","mode":"SINGLE","value":27.299},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"cTlydj","props":{"calcType":"Cloisons"},"children":[{"uid":"cWQ5aX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.631},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.959},{"symbol":"ZRAM","mode":"SINGLE","value":27.072},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"emRkMX","props":{"calcType":"Cloisons"},"children":[{"uid":"aG1xbj","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.404},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.731},{"symbol":"ZRAM","mode":"SINGLE","value":26.845},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"eG9hdT","props":{"calcType":"Cloisons"},"children":[{"uid":"M3Z5ZX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.177},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.504},{"symbol":"ZRAM","mode":"SINGLE","value":26.618},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.8},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":28.778}],"downWall":{"uid":"bWExN2","props":{"calcType":"CloisonAval"},"children":[{"uid":"bm0zcD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":26.95},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":26.391}]}}]}`;
    Session.getInstance().clear();
    Session.getInstance().unserialise(sess);
    const p = Session.getInstance().findNubByUid("Y3k4bj") as Pab;
    return p;
}

function createPar(): ParSimulation {
    const par = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.ParSimulation })
    ) as ParSimulation;
    return par;
}

function createMR(): MacroRugo {
    const mr = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.MacroRugo })
    ) as MacroRugo;
    return mr;
}

function createMRC(): MacrorugoCompound {
    const mrc = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.MacroRugoCompound })
    ) as MacrorugoCompound;
    mrc.inclinedApron = MRCInclination.INCLINED;
    return mrc;
}

describe("vérificateur de franchissement −", () => {

    describe("PAB −", () => {

        it("0 espèces", () => {
            // contexte
            Session.getInstance().clear();
            const pab = createPab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        it("Surface, 1 espèce prédéfinie", () => {
            // contexte
            // jets de SURFACE seulement
            const pab = loadExamplePab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        it("Plongeant, 1 espèce prédéfinie", () => {
            // contexte
            Session.getInstance().clear();
            // jets plongeants seulement
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-04-17T10:44:42.561Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"eGx2aG","props":{"calcType":"Pab"},"meta":{"title":"PAB 1"},"children":[{"uid":"OGxiN3","props":{"calcType":"Cloisons"},"children":[{"uid":"YXd1ZD","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.1},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101}],"downWall":{"uid":"OXdpbm","props":{"calcType":"CloisonAval"},"children":[{"uid":"OW04cn","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100.6},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.25}]}}]}`);
            const pab = Session.getInstance().findNubByUid("eGx2aG") as Pab;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        it("espèce prédéfinie, jet plongeant supporté", () => {
            // contexte
            // jets de SURFACE seulement
            const pab = loadExamplePab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // run CalcSerie() just to init Espece nubs
            v.CalcSerie();
            // check diving jet support
            const sp = v.species[0];
            expect(sp.divingJetSupported).toBe(DivingJetSupport.SUPPORTED);
        });

        it("espèce prédéfinie, jet plongeant non supporté", () => {
            // contexte
            // jets de SURFACE seulement
            const pab = loadExamplePab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_5]);
            // run CalcSerie() just to init Espece nubs
            v.CalcSerie();
            // check diving jet support
            const sp = v.species[0];
            expect(sp.divingJetSupported).toBe(DivingJetSupport.NOT_SUPPORTED);
        });

        it("espèce prédéfinie, jet plongeant non supporté, critères manquants sur PAB à jet plongeant", () => {
            // contexte
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-07-30T12:21:02.502Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Y3k4bj","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"ampiN3","props":{"calcType":"Cloisons"},"children":[{"uid":"Yzgxa2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":28.085},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.413},{"symbol":"ZRAM","mode":"SINGLE","value":27.526},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"c3RmMz","props":{"calcType":"Cloisons"},"children":[{"uid":"dmwyem","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.858},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.186},{"symbol":"ZRAM","mode":"SINGLE","value":27.299},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"cTlydj","props":{"calcType":"Cloisons"},"children":[{"uid":"cWQ5aX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.631},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.959},{"symbol":"ZRAM","mode":"SINGLE","value":27.072},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"emRkMX","props":{"calcType":"Cloisons"},"children":[{"uid":"aG1xbj","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.404},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.731},{"symbol":"ZRAM","mode":"SINGLE","value":26.845},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"eG9hdT","props":{"calcType":"Cloisons"},"children":[{"uid":"M3Z5ZX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.177},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.504},{"symbol":"ZRAM","mode":"SINGLE","value":26.618},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.8},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":100}],"downWall":{"uid":"bWExN2","props":{"calcType":"CloisonAval"},"children":[{"uid":"cjFiND","props":{"calcType":"Structure","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":2},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":26.391}]}},{"uid":"empnYX","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE"},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":150},{"symbol":"PVMaxLim","mode":"SINGLE","value":200}]},{"uid":"NGxpc3","props":{"calcType":"Verificateur","nubToVerify":"Y3k4bj","speciesList":["empnYX"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("Y3k4bj");
            const e = Session.getInstance().findNubByUid("empnYX") as Espece;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(e.uid);
            // check diving jet support
            const res = v.CalcSerie();
            const sp = v.species[0];
            expect(sp.divingJetSupported).toBe(DivingJetSupport.NOT_SUPPORTED);
            expect(res.ok).toBe(false); // fails because does not support diving jets, not because of missing criteria
            expect(e.result.log.messages.map((m) => m.code)).not.toContain(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages.length).toBe(2);
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
            expect(e.result.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_DW_NOT_CROSSABLE);
        });

        it("espèce prédéfinie, jet plongeant non supporté, critère limitant fourni sur PAB à jet plongeant", () => {
            // contexte
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-07-30T12:21:02.502Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Y3k4bj","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"ampiN3","props":{"calcType":"Cloisons"},"children":[{"uid":"Yzgxa2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":28.085},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.413},{"symbol":"ZRAM","mode":"SINGLE","value":27.526},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"c3RmMz","props":{"calcType":"Cloisons"},"children":[{"uid":"dmwyem","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.858},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.186},{"symbol":"ZRAM","mode":"SINGLE","value":27.299},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"cTlydj","props":{"calcType":"Cloisons"},"children":[{"uid":"cWQ5aX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.631},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.959},{"symbol":"ZRAM","mode":"SINGLE","value":27.072},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"emRkMX","props":{"calcType":"Cloisons"},"children":[{"uid":"aG1xbj","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.404},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.731},{"symbol":"ZRAM","mode":"SINGLE","value":26.845},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"eG9hdT","props":{"calcType":"Cloisons"},"children":[{"uid":"M3Z5ZX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.177},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.504},{"symbol":"ZRAM","mode":"SINGLE","value":26.618},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.8},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":100}],"downWall":{"uid":"bWExN2","props":{"calcType":"CloisonAval"},"children":[{"uid":"cjFiND","props":{"calcType":"Structure","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":2},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":26.391}]}},{"uid":"empnYX","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE"},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":150},{"symbol":"PVMaxLim","mode":"SINGLE","value":200}]},{"uid":"NGxpc3","props":{"calcType":"Verificateur","nubToVerify":"Y3k4bj","speciesList":["empnYX"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("Y3k4bj");
            const e = Session.getInstance().findNubByUid("empnYX") as Espece;
            e.prms.DHMaxP.singleValue = 0.1;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(e.uid);
            // check diving jet support
            const res = v.CalcSerie();
            const sp = v.species[0];
            expect(sp.divingJetSupported).toBe(DivingJetSupport.NOT_SUPPORTED);
            expect(res.ok).toBe(false); // fails because does not support diving jets, not because of criterion not met
            expect(e.result.log.messages.length).toBe(2);
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
            expect(e.result.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_DW_NOT_CROSSABLE);
        });

        // falls on Cloison / downwall
        it("Chute trop importante", () => {
            // contexte
            const pab = loadExamplePab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_9b]); // 0.2 < 0.227
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(6); // 5 cloisons + 1 downwall
            expect(r0.log.messages[2].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            expect(r0.log.messages[5].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
        });

        // weirs and slots witdh
        it("Largeur des fentes et échancrures trop faible (surface)", () => {
            // contexte
            const pab = loadExamplePab();
            pab.children[2].structures[0].getParameter("L").singleValue = 0.35; // Fente noyée (Larinier 1992)
            pab.children[4].structures[0] = new StructureWeirVillemonte( // Échancrure (Villemonte 1957)
                pab.children[4].structures[0].prms as RectangularStructureParams
            );
            pab.children[4].structures[0].setParent(pab.children[4]);
            pab.children[4].structures[0].getParameter("L").singleValue = 0.35;
            pab.prms.Z2.singleValue = 30.4; // évite une chute trop importante à la cloison 5
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_3b]); // 0.35 < 0.4
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(2);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_BMIN);
            expect(r0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_BMIN);
        });

        // basins depth
        it("Profondeur des bassins insuffisante", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-04-17T10:51:31.030Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"enZvdm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"d2llZG","props":{"calcType":"Cloisons"},"children":[{"uid":"cGdmNn","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.4},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":101},{"symbol":"ZRAM","mode":"SINGLE","value":101.25},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.4},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101}],"downWall":{"uid":"NzYwbG","props":{"calcType":"CloisonAval"},"children":[{"uid":"dW9pbD","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.75}]}}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("enZvdm") as Pab;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(2); // ignore 3rd error about PVMaxLim
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_YMOY_JET);
            expect(r0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_YMOY_2_DH);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        // head on weirs
        it("Charge trop faible sur échancrures (plongeant)", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-04-23T12:26:14.356Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"azk2cm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"Z2QwOG","props":{"calcType":"Cloisons"},"children":[{"uid":"YTJqZW","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.8},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.032},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101}],"downWall":{"uid":"YzZ0dX","props":{"calcType":"CloisonAval"},"children":[{"uid":"cHgyYm","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.3},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.25}]}}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("azk2cm") as Pab;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(4);
            expect(r0.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_HMIN);
            expect(r0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
            expect(r0.log.messages[2].code).toBe(MessageCode.WARNING_VERIF_PAB_HMIN);
            expect(r0.log.messages[3].code).toBe(MessageCode.ERROR_VERIF_PAB_DW_NOT_CROSSABLE);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        // head on weirs
        it("Charge trop faible sur échancrures (surface)", () => {
            // contexte
            // jets de SURFACE seulement
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-08-11T13:16:17.677Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Z2ZpYm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"a2doNj","props":{"calcType":"Cloisons"},"children":[{"uid":"cTZjbD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.8},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"eHZzc2","props":{"calcType":"Cloisons"},"children":[{"uid":"N2J4Z2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.3},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100},{"symbol":"ZRAM","mode":"SINGLE","value":100.25},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":0.059},{"symbol":"Z1","mode":"SINGLE","value":102},{"symbol":"Z2","mode":"SINGLE","value":101.5}],"downWall":{"uid":"b2JobG","props":{"calcType":"CloisonAval"},"children":[{"uid":"ZnlodG","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100.8},{"symbol":"L","mode":"SINGLE","value":0.4},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":99.75}]}}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("Z2ZpYm") as Pab;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_3a]); // HMin : 0.4
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(5); // 1: diving jet not supported, head too low, not crossable; 2: head too low, not crossable
            expect(r0.log.messages[3].code).toBe(MessageCode.WARNING_VERIF_PAB_HMIN);
            expect(r0.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        // basins length
        it("Longueur de bassin trop faible", () => {
            // contexte
            const pab = loadExamplePab();
            pab.children[1].prms.LB.singleValue = 2.25;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]); // 2.25 < 2.5
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_LMIN_JET);
        });

        it("Plusieurs warnings entraînent la non-franchissabilité d'une cloison", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-04-17T12:02:45.180Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"bjlnc2","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Croustibat®"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.6},{"symbol":"BMin","mode":"SINGLE","value":0.18},{"symbol":"PMinS","mode":"SINGLE","value":0.9},{"symbol":"PMinP","mode":"SINGLE","value":0.9},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":25},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"M29hc3","props":{"calcType":"Pab"},"meta":{"title":"PAB 1"},"children":[{"uid":"eDllOD","props":{"calcType":"Cloisons"},"children":[{"uid":"ZGMwc3","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":98},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"bzM2ZG","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.1},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":2.349},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101}],"downWall":{"uid":"NzBlcD","props":{"calcType":"CloisonAval"},"children":[{"uid":"cG5xdX","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":97.5},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"aGM1ZG","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100.49},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.25}]}}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("M29hc3") as Pab;
            const espece = Session.getInstance().findNubByUid("bjlnc2") as Espece;
            // vérificateur et espèce personnalisée
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(espece.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
            expect(res.log.messages[0].extraVar.uid).toBe("bjlnc2");
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(4);
            expect(r0.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
            expect(r0.log.messages[1].code).toBe(MessageCode.WARNING_VERIF_PAB_DHMAX_JET);
            expect(r0.log.messages[1].extraVar.jetType).toBe(String(StructureJetType.SURFACE));
            expect(r0.log.messages[2].code).toBe(MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
            expect(r0.log.messages[2].extraVar.N).toBe("1");
            expect(r0.log.messages[3].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET); // downwall jets are both Surface (not the object of the test but still fails)
            expect(r0.log.messages[3].extraVar.jetType).toBe(String(StructureJetType.SURFACE));
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        it("Plusieurs warnings entraînent la non-franchissabilité d'une cloison (2)", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-04-17T11:40:14.310Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"bjlnc2","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Croustibat®"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"M29hc3","props":{"calcType":"Pab"},"meta":{"title":"PAB 1"},"children":[{"uid":"eDllOD","props":{"calcType":"Cloisons"},"children":[{"uid":"ZGMwc3","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100.75},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"NWRpcG","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.8},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.5},{"symbol":"ZRAM","mode":"SINGLE","value":100.75},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":2},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101.2}],"downWall":{"uid":"NzBlcD","props":{"calcType":"CloisonAval"},"children":[{"uid":"cG5xdX","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"dWRhdD","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.2}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100}]}}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("M29hc3") as Pab;
            const espece = Session.getInstance().findNubByUid("bjlnc2") as Espece;
            // vérificateur et espèce personnalisée
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(espece.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
            expect(res.log.messages[0].extraVar.uid).toBe("bjlnc2");
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(7);
            expect(r0.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
            expect(r0.log.messages[1].code).toBe(MessageCode.WARNING_VERIF_PAB_DHMAX_JET);
            expect(r0.log.messages[2].code).toBe(MessageCode.WARNING_VERIF_PAB_BMIN);
            expect(r0.log.messages[3].code).toBe(MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
            expect(r0.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET); // downwall jets are both Surface (not the object of the test but still fails)
            expect(r0.log.messages[4].extraVar.jetType).toBe(String(StructureJetType.SURFACE));
            expect(r0.log.messages[5].code).toBe(MessageCode.ERROR_VERIF_PAB_BMIN);
            expect(r0.log.messages[6].code).toBe(MessageCode.ERROR_VERIF_PAB_BMIN);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        /* it("Puissance dissipée très élevée", () => {
            // contexte
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-11T13:11:24.549Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MDdjbW","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"Mno1eG","props":{"calcType":"Cloisons"},"children":[{"uid":"ZXQ3N2","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"MWhoMX","props":{"calcType":"CloisonAval"},"children":[{"uid":"NXRnbm","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"cWJ4cz","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":50},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":200},{"symbol":"PVMaxLim","mode":"SINGLE","value":1200}]},{"uid":"bjlidj","props":{"calcType":"Verificateur","nubToVerify":"MDdjbW","speciesList":["cWJ4cz"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`);
            const v = Session.getInstance().findNubByUid("bjlidj") as Verificateur;
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_SPECIES_NUB_OK_BUT);
            const r0 = v.species[0].result;
            expect(r0.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_PVMAX);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_VERIF_OK_BUT);
        }); */


        /* it("Puissance dissipée trop élevée", () => {
            // contexte
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-11T13:11:24.549Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MDdjbW","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"Mno1eG","props":{"calcType":"Cloisons"},"children":[{"uid":"ZXQ3N2","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"MWhoMX","props":{"calcType":"CloisonAval"},"children":[{"uid":"NXRnbm","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"cWJ4cz","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":50},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxLim","mode":"SINGLE","value":200}]},{"uid":"bjlidj","props":{"calcType":"Verificateur","nubToVerify":"MDdjbW","speciesList":["cWJ4cz"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`);
            const v = Session.getInstance().findNubByUid("bjlidj") as Verificateur; // LOL "bjlidj"
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_PVMAX);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        }); */

        it("Orifices (structures dont le mode d'écoulement est en charge) non franchissables", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2023-04-17T11:54:53.352Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"bzRjY3","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"c25hMW","props":{"calcType":"Cloisons"},"children":[{"uid":"MWN2aH","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]},{"uid":"bm10ZG","props":{"calcType":"Structure","loiDebit":"WeirSubmergedLarinier","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":99},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"cm54MW","props":{"calcType":"CloisonAval"},"children":[{"uid":"dGhleD","props":{"calcType":"Structure","loiDebit":"GateCunge80"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":100},{"symbol":"W","mode":"SINGLE","value":0.5},{"symbol":"L","mode":"SINGLE","value":2},{"symbol":"CdCunge","mode":"SINGLE","value":1}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"NnB5b2","props":{"calcType":"Espece","divingJetSupported":"SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":50},{"symbol":"DHMaxP","mode":"SINGLE","value":2},{"symbol":"BMin","mode":"SINGLE","value":0.1},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("bzRjY3") as Pab;
            const espece = Session.getInstance().findNubByUid("NnB5b2") as Espece;
            // vérificateur et espèce personnalisée
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(espece.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_VERIF_OK_BUT);

            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_ORIFICE);
        });

        it("jalhyd#265 - charge minimale, avec une passe variée", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-07T13:47:13.778Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"cHBtaD","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"dTd6bW","props":{"calcType":"Cloisons"},"children":[{"uid":"OHFxNW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1312.19},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"cm15MG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1311.133},{"symbol":"ZRAM","mode":"SINGLE","value":1311.279},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"a2g4Ym","props":{"calcType":"Cloisons"},"children":[{"uid":"M3A4NW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.898},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"dDk1em","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.84},{"symbol":"ZRAM","mode":"SINGLE","value":1310.986},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ZDZxMD","props":{"calcType":"Cloisons"},"children":[{"uid":"N2k5NW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.605},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ajFken","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.548},{"symbol":"ZRAM","mode":"SINGLE","value":1310.694},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"Z3NxYW","props":{"calcType":"Cloisons"},"children":[{"uid":"MXpzZz","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.313},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"OTNiaj","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.255},{"symbol":"ZRAM","mode":"SINGLE","value":1310.401},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"aHZ6em","props":{"calcType":"Cloisons"},"children":[{"uid":"Nmxwem","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.02},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ZXJvcG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.963},{"symbol":"ZRAM","mode":"SINGLE","value":1310.109},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"OGJ5Mn","props":{"calcType":"Cloisons"},"children":[{"uid":"bmhnN2","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.728},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"Nm9ob3","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.67},{"symbol":"ZRAM","mode":"SINGLE","value":1309.816},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"NXV1OH","props":{"calcType":"Cloisons"},"children":[{"uid":"cjlxYm","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.435},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"YjEzZm","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.378},{"symbol":"ZRAM","mode":"SINGLE","value":1309.524},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"Njdtcn","props":{"calcType":"Cloisons"},"children":[{"uid":"bzczNG","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.143},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"YnQxcD","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.085},{"symbol":"ZRAM","mode":"SINGLE","value":1309.231},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ajV6Yj","props":{"calcType":"Cloisons"},"children":[{"uid":"eWNmeW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.85},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"d21haT","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.793},{"symbol":"ZRAM","mode":"SINGLE","value":1308.939},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"YndkYm","props":{"calcType":"Cloisons"},"children":[{"uid":"cTdlM2","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.558},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"bHBnOD","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.5},{"symbol":"ZRAM","mode":"SINGLE","value":1308.646},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ZzhnM2","props":{"calcType":"Cloisons"},"children":[{"uid":"ZzdiaW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.265},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ZXpzaG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.208},{"symbol":"ZRAM","mode":"SINGLE","value":1308.354},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":0.15},{"symbol":"Z1","mode":"LISTE","values":[1312.18,1312.48,1312.8],"extensionStrategy":0},{"symbol":"Z2","mode":"SINGLE","value":1308.97}],"downWall":{"uid":"bnQ5Yn","props":{"calcType":"CloisonAval"},"children":[{"uid":"YjRzcD","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1308.973},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"dnM2YT","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":1308.061}]}},{"uid":"Z2hrNH","props":{"calcType":"Verificateur","nubToVerify":"cHBtaD","speciesList":["SPECIES_4a","SPECIES_4b","SPECIES_6","SPECIES_7a","SPECIES_9a","SPECIES_9b"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("cHBtaD") as Pab;
            const v = Session.getInstance().findNubByUid("Z2hrNH") as Verificateur;
            expect(v.nubToVerify).toBe(pab);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);
            const r0 = v.species[0].result;
            // test de deux modalités : pas d'eau dans le premier ouvrage, pour la première modalité seulement
            const r0m1 = r0.resultElements[0];
            const r0m2 = r0.resultElements[1];
            expect(r0m1.log.messages.length).toBe(3 * 12); // pour les 12 cloisons : charge insuffisante (o1), orifice non franchissable (o2), cloison non franchissable (résumé)
            expect(r0m1.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAB_HMIN);
            expect(r0m2.log.messages.length).toBe(1 * 12); // pour les 12 cloisons : orifice non franchissable (o2)
            expect(r0m2.log.messages[0].code).not.toBe(MessageCode.WARNING_VERIF_PAB_HMIN);
        });

        it("jalhyd#265 (2) - charge minimale sur cloison aval régulée, avec une passe variée", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-08T08:58:52.333Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MnRyeT","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"OWEwZn","props":{"calcType":"Cloisons"},"children":[{"uid":"bHVyZ3","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":101.8},{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWSL","mode":"SINGLE","value":0.75}]},{"uid":"aGd1MX","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":100.8},{"symbol":"ZRAM","mode":"SINGLE","value":100.9},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"MINMAX","min":0.14,"max":0.3,"step":0.15,"extensionStrategy":0},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":101.5}],"downWall":{"uid":"ZWxnMz","props":{"calcType":"CloisonAval"},"children":[{"uid":"ZHJwcD","props":{"calcType":"Structure","loiDebit":"VanLevVillemonte"},"children":[],"parameters":[{"symbol":"L","mode":"SINGLE","value":0.2},{"symbol":"CdWR","mode":"SINGLE","value":0.4},{"symbol":"minZDV","mode":"SINGLE","value":100.7},{"symbol":"maxZDV","mode":"SINGLE","value":101.6},{"symbol":"DH","mode":"SINGLE","value":0.2}]},{"uid":"cWw4aX","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":100.7}]}},{"uid":"YXBxbn","props":{"calcType":"Verificateur","nubToVerify":"MnRyeT","speciesList":["SPECIES_7b"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("MnRyeT") as Pab;
            const v = Session.getInstance().findNubByUid("YXBxbn") as Verificateur;
            expect(v.nubToVerify).toBe(pab);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);
            const r0 = v.species[0].result;
            // test de deux modalités : pas d'eau dans le premier ouvrage de la cloison aval, pour la première modalité seulement
            const r0m1 = r0.resultElements[0];
            const r0m2 = r0.resultElements[1];
            let found = false;
            for (const m of r0m1.log.messages) {
                if (
                    m.code === MessageCode.WARNING_VERIF_PAB_HMIN
                    && m.parent.parent.sourceNub.findPositionInParent() === -1 // -1 = cloison aval
                ) {
                    found = true;
                }
            }
            expect(found).toBe(true, "Modalité 1: un message WARNING_VERIF_PAB_HMIN concernant le premier ouvrage de la cloison aval doit être présent");
            found = false;
            for (const m of r0m2.log.messages) {
                found = (
                    m.code === MessageCode.WARNING_VERIF_PAB_HMIN
                    && m.parent.parent.sourceNub.findPositionInParent() === -1
                );
            }
            expect(found).toBe(false, "Modalité 2: aucun message WARNING_VERIF_PAB_HMIN concernant le premier ouvrage de la cloison aval ne doit être présent");
        });

        it("jalhyd#266 - critères obligatoires d'une espèce personnalisée, avec une passe variée", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-08T08:07:52.911Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"cHBtaD","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"dTd6bW","props":{"calcType":"Cloisons"},"children":[{"uid":"OHFxNW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1312.19},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"cm15MG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1311.133},{"symbol":"ZRAM","mode":"SINGLE","value":1311.279},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"a2g4Ym","props":{"calcType":"Cloisons"},"children":[{"uid":"M3A4NW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.898},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"dDk1em","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.84},{"symbol":"ZRAM","mode":"SINGLE","value":1310.986},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ZDZxMD","props":{"calcType":"Cloisons"},"children":[{"uid":"N2k5NW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.605},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ajFken","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.548},{"symbol":"ZRAM","mode":"SINGLE","value":1310.694},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"Z3NxYW","props":{"calcType":"Cloisons"},"children":[{"uid":"MXpzZz","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.313},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"OTNiaj","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1310.255},{"symbol":"ZRAM","mode":"SINGLE","value":1310.401},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"aHZ6em","props":{"calcType":"Cloisons"},"children":[{"uid":"Nmxwem","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1311.02},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ZXJvcG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.963},{"symbol":"ZRAM","mode":"SINGLE","value":1310.109},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"OGJ5Mn","props":{"calcType":"Cloisons"},"children":[{"uid":"bmhnN2","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.728},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"Nm9ob3","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.67},{"symbol":"ZRAM","mode":"SINGLE","value":1309.816},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"NXV1OH","props":{"calcType":"Cloisons"},"children":[{"uid":"cjlxYm","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.435},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"YjEzZm","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.378},{"symbol":"ZRAM","mode":"SINGLE","value":1309.524},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"Njdtcn","props":{"calcType":"Cloisons"},"children":[{"uid":"bzczNG","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1310.143},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"YnQxcD","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1309.085},{"symbol":"ZRAM","mode":"SINGLE","value":1309.231},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ajV6Yj","props":{"calcType":"Cloisons"},"children":[{"uid":"eWNmeW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.85},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"d21haT","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.793},{"symbol":"ZRAM","mode":"SINGLE","value":1308.939},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"YndkYm","props":{"calcType":"Cloisons"},"children":[{"uid":"cTdlM2","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.558},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"bHBnOD","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.5},{"symbol":"ZRAM","mode":"SINGLE","value":1308.646},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"ZzhnM2","props":{"calcType":"Cloisons"},"children":[{"uid":"ZzdiaW","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1309.265},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"ZXpzaG","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":1.7},{"symbol":"BB","mode":"SINGLE","value":1.2},{"symbol":"ZRMB","mode":"SINGLE","value":1308.208},{"symbol":"ZRAM","mode":"SINGLE","value":1308.354},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"CALCUL","value":0.15},{"symbol":"Z1","mode":"LISTE","values":[1312.18,1312.48,1312.8,1312.9],"extensionStrategy":0},{"symbol":"Z2","mode":"SINGLE","value":1308.97}],"downWall":{"uid":"bnQ5Yn","props":{"calcType":"CloisonAval"},"children":[{"uid":"YjRzcD","props":{"calcType":"Structure","loiDebit":"WeirVillemonte","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1308.973},{"symbol":"L","mode":"SINGLE","value":0.3},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]},{"uid":"dnM2YT","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged","structureType":"Orifice"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.04},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":1308.061}]}},{"uid":"Z2hrNH","props":{"calcType":"Verificateur","nubToVerify":"cHBtaD","speciesList":["Z3N1M2"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]},{"uid":"Z3N1M2","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE"},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE"},{"symbol":"PMinS","mode":"SINGLE"},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE"},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE"},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":150},{"symbol":"PVMaxLim","mode":"SINGLE","value":200}]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("cHBtaD") as Pab;
            const v = Session.getInstance().findNubByUid("Z2hrNH") as Verificateur;
            const esp = Session.getInstance().findNubByUid("Z3N1M2") as Espece;
            expect(v.nubToVerify).toBe(pab);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);
            // les 2 premières modalités devraient tiquer sur HMin seulement (pas de jet de surface)
            for (let i = 0; i < 2; i++) {
                const r = esp.result.resultElements[i]
                expect(r.log.messages.length).toBe(2); // DHMaxS, BMin, PMinS, LMinS, HMin
                expect(r.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[0].extraVar.var_criterion).toBe("HMin");
                expect(r.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[1].extraVar.var_criterion).toBe("BMin");
            }
            // les 2 dernières modalités devraient tiquer sur HMin, DHMaxS, BMin, PMinS, LMinS, HMin
            for (let i = 2; i < 4; i++) {
                const r = esp.result.resultElements[i]
                expect(r.log.messages.length).toBe(5); // DHMaxS, BMin, PMinS, LMinS, HMin
                expect(r.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[0].extraVar.var_criterion).toBe("HMin");
                expect(r.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[1].extraVar.var_criterion).toBe("BMin");
                expect(r.log.messages[2].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[2].extraVar.var_criterion).toBe("DHMaxS");
                expect(r.log.messages[3].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[3].extraVar.var_criterion).toBe("PMinS");
                expect(r.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
                expect(r.log.messages[4].extraVar.var_criterion).toBe("LMinS");
            }
        });

    });

    describe("PAB variée −", () => {

        it("0 espèces", () => {
            // contexte
            Session.getInstance().clear();
            const pab = createPab();
            pab.prms.Z2.setValues(98, 100, 1); // 3 values
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_VARYING_OK);
        });

        it("Surface, 1 espèce prédéfinie", () => {
            // contexte
            // jets de SURFACE seulement
            const pab = loadExamplePab();
            pab.prms.Z2.setValues(98, 100, 1); // 3 values
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect (res.resultElements.length).toBe(3);
            for (const re of res.resultElements) {
                expect(re.log.messages.length).toBe(1);
                expect(re.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            }
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_VARYING_OK);
        });

        it("Échec sur certaines itérations mais pas toutes", () => {
            // contexte
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-07T13:45:19.352Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Y3k4bj","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"ampiN3","props":{"calcType":"Cloisons"},"children":[{"uid":"Yzgxa2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":28.085},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.413},{"symbol":"ZRAM","mode":"SINGLE","value":27.526},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"c3RmMz","props":{"calcType":"Cloisons"},"children":[{"uid":"dmwyem","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.858},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":27.186},{"symbol":"ZRAM","mode":"SINGLE","value":27.299},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"cTlydj","props":{"calcType":"Cloisons"},"children":[{"uid":"cWQ5aX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.631},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.959},{"symbol":"ZRAM","mode":"SINGLE","value":27.072},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"emRkMX","props":{"calcType":"Cloisons"},"children":[{"uid":"aG1xbj","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.404},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.731},{"symbol":"ZRAM","mode":"SINGLE","value":26.845},{"symbol":"QA","mode":"SINGLE","value":0}]},{"uid":"eG9hdT","props":{"calcType":"Cloisons"},"children":[{"uid":"M3Z5ZX","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":27.177},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":4.5},{"symbol":"BB","mode":"SINGLE","value":3.6},{"symbol":"ZRMB","mode":"SINGLE","value":26.504},{"symbol":"ZRAM","mode":"SINGLE","value":26.618},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.8},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"MINMAX","min":28,"max":30,"step":1,"extensionStrategy":0}],"downWall":{"uid":"bWExN2","props":{"calcType":"CloisonAval"},"children":[{"uid":"bm0zcD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirSubmergedLarinier"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":26.95},{"symbol":"L","mode":"SINGLE","value":0.5},{"symbol":"CdWSL","mode":"SINGLE","value":0.83}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":26.391}]}},{"uid":"Y25qdn","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":500000},{"symbol":"PVMaxLim","mode":"SINGLE","value":800000}]},{"uid":"c2d6OW","props":{"calcType":"Verificateur","nubToVerify":"Y3k4bj","speciesList":["Y25qdn","SPECIES_10"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            // vérificateur
            const v = Session.getInstance().findNubByUid("c2d6OW") as Verificateur;
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            // check Verificateur global log
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_VERIF_VARYING_OK_BUT);

            // check Verificateur logs
            expect (res.resultElements.length).toBe(3);
            const re0 = res.resultElements[0];
            expect(re0.log.messages.length).toBe(2);
            expect(re0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
            expect(re0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const re1 = res.resultElements[1];
            expect(re1.log.messages.length).toBe(2);
            expect(re1.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_NUB_OK);
            expect(re1.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const re2 = res.resultElements[2];
            expect(re2.log.messages.length).toBe(2);
            expect(re2.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_NUB_OK);
            expect(re2.log.messages[1].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);

            // check Espece logs
            const sp0 = v.species[0];
            expect(sp0.result.resultElements.length).toBe(3);
            const sp0re0 = sp0.result.resultElements[0];
            expect(sp0re0.log.messages.length).toBe(2);
            expect(sp0re0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            expect(sp0re0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            const sp0re1 = sp0.result.resultElements[1];
            expect(sp0re1.log.messages.length).toBe(0);
            const sp0re2 = sp0.result.resultElements[2];
            expect(sp0re2.log.messages.length).toBe(0);

            const sp1 = v.species[1];
            expect(sp1.result.resultElements.length).toBe(3);
            const sp1re0 = sp1.result.resultElements[0];
            expect(sp1re0.log.messages.length).toBe(6);
            expect(sp1re0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            // expect(sp1re0.log.messages[1].code).toBe(MessageCode.WARNING_VERIF_PAB_PVMAX);
            expect(sp1re0.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            // expect(sp1re0.log.messages[3].code).toBe(MessageCode.WARNING_VERIF_PAB_PVMAX);
            expect(sp1re0.log.messages[2].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            // expect(sp1re0.log.messages[5].code).toBe(MessageCode.ERROR_VERIF_PAB_PVMAX);
            expect(sp1re0.log.messages[3].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            // expect(sp1re0.log.messages[7].code).toBe(MessageCode.ERROR_VERIF_PAB_PVMAX);
            expect(sp1re0.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            // expect(sp1re0.log.messages[9].code).toBe(MessageCode.ERROR_VERIF_PAB_PVMAX);
            expect(sp1re0.log.messages[5].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            const sp1re1 = sp1.result.resultElements[1];
            expect(sp1re1.log.messages.length).toBe(3);
            expect(sp1re1.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            expect(sp1re1.log.messages[1].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            expect(sp1re1.log.messages[2].code).toBe(MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
            const sp1re2 = sp1.result.resultElements[2];
            expect(sp1re2.log.messages.length).toBe(0);
        });
    });

    describe("PAR −", () => {

        it("1 espèce prédéfinie", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_2]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        // downstream fall
        it("Chute en pied de passe", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.prms.Z2.singleValue = 7;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_2]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_DH);
        });

        // species groups 3a, 3b, 7b are discouraged
        it("Espèce non recommandées", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_3a]);
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_7b]);
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_3b]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            for (const i of [ 0, 2, 3 ]) {
                const r = v.species[i].result;
                expect(r.log.messages[0].code).toBe(MessageCode.WARNING_VERIF_PAR_SPECIES_GROUP);
            }
        });

        // water level, plane & fatou
        it("Tirant d'eau insuffisant (plane)", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.parType = ParType.PLANE;
            par.calculatedParam = par.prms.Z1;
            par.prms.Q.singleValue = 0.05;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]); // 0.297 < 0.3
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_YMIN);
        });

        it("Tirant d'eau insuffisant (fatou)", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.parType = ParType.FATOU;
            par.calculatedParam = par.prms.Z1;
            par.prms.Q.singleValue = 0.05;
            par.prms.P.singleValue = 0.3;
            par.prms.Nb.singleValue = undefined;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_4b]); // 0.175 < 0.2
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_YMIN);
        });

        // water level, superactive & chevron
        it("Tirant d'eau insuffisant (superactive)", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.parType = ParType.SUPERACTIVE;
            par.calculatedParam = par.prms.Z1;
            par.prms.Q.singleValue = 0.1;
            par.prms.S.singleValue = 0.15;
            par.prms.Nb.singleValue = undefined;
            par.prms.P.singleValue = undefined;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_5]); // 0.158 < 0.2
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_YMIN);
        });

        it("Tirant d'eau insuffisant (chevron)", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.parType = ParType.CHEVRON;
            par.calculatedParam = par.prms.Z1;
            par.prms.Q.singleValue = 0.1;
            par.prms.S.singleValue = 0.15;
            par.prms.Nb.singleValue = undefined;
            par.prms.P.singleValue = undefined;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_5]); // 0.129 < 0.2
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_YMIN);
        });

        // slopes
        // @TODO si la pente dépasse de trop peu le seuil, la PAR génère un warning et non une erreur,
        // et la vérification passe… doit-on aussi contrôler la pente dans Espece ?
        /* it("Pente trop élevée (plane)", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.parType = ParType.PLANE;
            par.prms.S.singleValue = 0.21; // > 0.2
            par.prms.Nb.singleValue = 13;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_PAR_SLOPE);
            expect(res.globalLog.messages.length).toBe(0);
        }); */
    });

    describe("MacroRugo −", () => {

        it("1 espèce prédéfinie", () => {
            // contexte
            Session.getInstance().clear();
            const mr = createMR();
            mr.prms.B.singleValue = 1.789; // 2 patterns
            mr.prms.C.singleValue = 0.2; // limite la vitesse max
            mr.prms.PBH.singleValue = 0.6; // limite la vitesse max
            mr.prms.Cd0.singleValue = 2; // limite la vitesse max
            mr.prms.If.singleValue = 0.04;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mr;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_3b]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        // velocity
        it("Vitesse trop élevée", () => {
            // contexte
            Session.getInstance().clear();
            const mr = createMR();
            mr.prms.B.singleValue = 2.218; // 2 patterns
            mr.prms.Y.singleValue = 0.4; // emergent
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mr;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_8a]); // > 1.50
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MR_VMAX);
        });

        it("Cas submergé", () => {
            // contexte
            Session.getInstance().clear();
            const mr = createMR();
            mr.prms.B.singleValue = 2.218; // 2 patterns
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mr;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]); // 2.533 > 2.50
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MR_SUBMERGED);
        });

        // water level
        it("Tirant d'eau trop faible", () => {
            // contexte
            Session.getInstance().clear();
            const mr = createMR();
            mr.prms.B.singleValue = 2.218; // 2 patterns
            mr.prms.Y.singleValue = 0.3;
            /* Session.getInstance().registerNub(mr);
            console.log(Session.getInstance().serialise()); */
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mr;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]); // 0.3 < 0.4
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MR_YMIN);
        });
    });

    describe("MacroRugoCompound −", () => {

        it("1 espèce prédéfinie", () => {
            // contexte
            Session.getInstance().clear();
            const mrc = createMRC();
            mrc.prms.If.singleValue = 0.04;
            mrc.prms.Z1.singleValue = 12.9;
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mrc;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_4a]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.INFO_VERIF_OK);
        });

        it("1 radier OK, mais pas tous", () => {
            // contexte
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-04-23T09:33:41.916Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"M2prYT","props":{"calcType":"MacroRugoCompound","inclinedApron":"NOT_INCLINED"},"meta":{"title":"M-Rugo complexe"},"children":[{"uid":"dm1yNX","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.109}]},{"uid":"c3dsNG","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.11}]},{"uid":"Njl6cj","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.7},{"symbol":"B","mode":"SINGLE","value":1.109}]},{"uid":"ZzZzMW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.11}]}],"parameters":[{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"If","mode":"SINGLE","value":0.05},{"symbol":"Ks","mode":"SINGLE","value":0.01},{"symbol":"C","mode":"SINGLE","value":0.13},{"symbol":"PBD","mode":"SINGLE","value":0.4},{"symbol":"PBH","mode":"SINGLE","value":0.4},{"symbol":"Cd0","mode":"SINGLE","value":1.2},{"symbol":"Z1","mode":"SINGLE","value":13.1},{"symbol":"DH","mode":"SINGLE","value":3}]}]}`);
            const mrc = Session.getInstance().findNubByUid("M2prYT");
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mrc;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(true);
            expect(res.vCalc).toBe(1);

            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(4); // 3 errors + crossabel width info
            for (let i=0; i < 3; i++) {
                expect(r0.log.messages[i].code).toBe(MessageCode.WARNING_VERIF_MRC_SUBMERGED_APRON_N);
            }
            expect(r0.log.messages[3].code).toBe(MessageCode.INFO_VERIF_MRC_CROSSABLE_WIDTH);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.WARNING_VERIF_OK_BUT);
        });

        it("Aucun radier OK", () => {
            // contexte
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-04-23T09:35:25.565Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"M2prYT","props":{"calcType":"MacroRugoCompound","inclinedApron":"NOT_INCLINED"},"meta":{"title":"M-Rugo complexe"},"children":[{"uid":"dm1yNX","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.109}]},{"uid":"c3dsNG","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.11}]},{"uid":"Njl6cj","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.109}]},{"uid":"ZzZzMW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.5},{"symbol":"B","mode":"SINGLE","value":1.11}]}],"parameters":[{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"If","mode":"SINGLE","value":0.05},{"symbol":"Ks","mode":"SINGLE","value":0.01},{"symbol":"C","mode":"SINGLE","value":0.13},{"symbol":"PBD","mode":"SINGLE","value":0.4},{"symbol":"PBH","mode":"SINGLE","value":0.4},{"symbol":"Cd0","mode":"SINGLE","value":1.2},{"symbol":"Z1","mode":"SINGLE","value":13.1},{"symbol":"DH","mode":"SINGLE","value":3}]}]}`);
            const mrc = Session.getInstance().findNubByUid("M2prYT");
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mrc;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_4a]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(5); // warning sur les 4 radiers, plus erreur "aucun radier OK"
            for (let i=0; i < 4; i++) {
                expect(r0.log.messages[i].code).toBe(MessageCode.WARNING_VERIF_MRC_SUBMERGED_APRON_N);
            }
            expect(r0.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_MRC_AT_LEAST_ONE_APRON);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });

        it("Largeur franchissable insuffisante", () => {
            // contexte
            Session.getInstance().clear();
            Session.getInstance().unserialise(`{"header":{"source":"jalhyd","format_version":"1.3","created":"2021-02-13T10:21:53.436Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b2tnbW","props":{"calcType":"MacroRugoCompound","inclinedApron":"INCLINED"},"meta":{"title":"M-Rugo complexe"},"children":[{"uid":"MHV3dW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.052},{"symbol":"B","mode":"SINGLE","value":0.894}]},{"uid":"aDJrOG","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.156},{"symbol":"B","mode":"SINGLE","value":0.895}]},{"uid":"MXF4cW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.26},{"symbol":"B","mode":"SINGLE","value":0.894}]},{"uid":"czE5Y2","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.364},{"symbol":"B","mode":"SINGLE","value":0.895}]},{"uid":"ajZteW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":12.458},{"symbol":"B","mode":"SINGLE","value":0.722}]}],"parameters":[{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"If","mode":"SINGLE","value":0.04},{"symbol":"Ks","mode":"SINGLE","value":0.01},{"symbol":"C","mode":"SINGLE","value":0.2},{"symbol":"PBD","mode":"SINGLE","value":0.4},{"symbol":"PBH","mode":"SINGLE","value":0.6},{"symbol":"Cd0","mode":"SINGLE","value":2.6},{"symbol":"Z1","mode":"SINGLE","value":13.1},{"symbol":"ZRB","mode":"SINGLE","value":12.5},{"symbol":"ZRT","mode":"SINGLE","value":12},{"symbol":"BR","mode":"SINGLE","value":4.3},{"symbol":"DH","mode":"SINGLE","value":3}]},{"uid":"Z215Y2","props":{"calcType":"Verificateur","nubToVerify":"b2tnbW","speciesList":["dDZzOX"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]},{"uid":"dDZzOX","props":{"calcType":"Espece","divingJetSupported":"NOT_SUPPORTED","species":"SPECIES_CUSTOM"},"meta":{"title":"Espèce"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.6},{"symbol":"VeMax","mode":"SINGLE","value":2},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3},{"symbol":"PVMaxPrec","mode":"SINGLE","value":150},{"symbol":"PVMaxLim","mode":"SINGLE","value":800000}]}]}`);
            const v = Session.getInstance().findNubByUid("Z215Y2") as Verificateur;
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(5); // 4 warning, plus erreur "largeur insuffisante"
            expect(r0.log.messages[4].code).toBe(MessageCode.ERROR_VERIF_MRC_CROSSABLE_WIDTH);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_KO);
        });
    });

    describe("cas d'erreur −", () => {

        it("instanciation directe de SPECIES_CUSTOM", () => {
            // contexte
            Session.getInstance().clear();
            const pab = createPab();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_CUSTOM]);
            // résultat
            expect(() => {
                v.CalcSerie();
            }).toThrow();
        });

        // species / pass combination that has no preset
        it("PAR, espèce non gérée", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_9b]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
            const r0 = v.species[0].result;
            expect(r0.log.messages.length).toBe(1);
            expect(r0.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_NO_PRESET);
        });

        // pass with errors
        it("PAR, erreur fatale", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar();
            par.prms.Nb.singleValue = 123; // wrong number of baffles
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(FishSpecies[FishSpecies.SPECIES_1]);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(res.resultElements.length).toBe(0);
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_VERIF_ERRORS_IN_PASS);
        });

    });

    describe("espèces personnalisées", () => {

        it("PAB, critères manquants, pas de jet plongeant", () => {
            // contexte
            Session.getInstance().clear();
            const pab = createPab();
            // espèce : omit BMin, PMinP and PVMaxLim, but Pab has no diving jet so PMinP won't be marked as missing
            const e = new Espece(new EspeceParams(0.7, 0.65, undefined, 0.2, undefined, 0.8, 1, 0.65, undefined, undefined, undefined, undefined, 200));
            Session.getInstance().registerNub(e);
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = pab;
            v.speciesList.push(e.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(e.result.log.messages.length).toBe(1);
            /* expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages[0].extraVar.var_criterion).toBe("PVMaxLim"); */
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages[0].extraVar.var_criterion).toBe("BMin");
        });

        it("PAR, critères manquants", () => {
            // contexte
            Session.getInstance().clear();
            const par = createPar(); // plane baffles by default
            // espèce : omit YMinPB for Plane Baffles
            const e = new Espece(new EspeceParams(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 3, undefined));
            Session.getInstance().registerNub(e);
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = par;
            v.speciesList.push(e.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(e.result.log.messages.length).toBe(1);
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages[0].extraVar.var_criterion).toBe("YMinPB");
        });

        it("MR, critères manquants", () => {
            // contexte
            Session.getInstance().clear();
            const mr = createMR();
            // espèce : omit VeMax
            const e = new Espece(new EspeceParams(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 0.25, undefined));
            Session.getInstance().registerNub(e);
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mr;
            v.speciesList.push(e.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(e.result.log.messages.length).toBe(1);
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages[0].extraVar.var_criterion).toBe("VeMax");
        });

        it("MRC, critères manquants", () => {
            // contexte
            Session.getInstance().clear();
            const mrc = createMRC();
            // espèce : omit YMin
            const e = new Espece(new EspeceParams(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 2.5));
            Session.getInstance().registerNub(e);
            // vérificateur
            const v = new Verificateur();
            v.nubToVerify = mrc;
            v.speciesList.push(e.uid);
            // résultat
            const res = v.CalcSerie();
            expect(res.ok).toBe(false);

            expect(e.result.log.messages.length).toBe(1);
            expect(e.result.log.messages[0].code).toBe(MessageCode.ERROR_VERIF_MISSING_CRITERION);
            expect(e.result.log.messages[0].extraVar.var_criterion).toBe("YMin");
        });

    });

    describe("dépendances entre Nubs", () => {

        it("le Verificateur devrait être dépendant d'un Nub Espèce sélectionné", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-06T08:11:24.196Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Z2xxan","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Cabillaud"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"eGs4Nm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"cnBpMD","props":{"calcType":"Cloisons"},"children":[{"uid":"MjN2Yn","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"OWoxcn","props":{"calcType":"CloisonAval"},"children":[{"uid":"aDg4aH","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"YjJmYT","props":{"calcType":"ParSimulation","parType":"PLANE"},"meta":{"title":"PAR : simulation"},"children":[],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":10},{"symbol":"Z2","mode":"SINGLE","value":9},{"symbol":"ZD1","mode":"SINGLE"},{"symbol":"S","mode":"SINGLE","value":0.2},{"symbol":"P","mode":"SINGLE","value":0.4},{"symbol":"L","mode":"SINGLE","value":0.6},{"symbol":"Nb","mode":"SINGLE"},{"symbol":"ZR1","mode":"SINGLE","value":9.36},{"symbol":"ZR2","mode":"SINGLE","value":8.36},{"symbol":"ZD2","mode":"SINGLE"}]},{"uid":"OWsxdT","props":{"calcType":"Verificateur","nubToVerify":"eGs4Nm","speciesList":["Z2xxan"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const espece = Session.getInstance().findNubByUid("Z2xxan") as Espece;
            const verif = Session.getInstance().findNubByUid("OWsxdT") as Verificateur;
            // dépendences
            verif.speciesList = [ "Z2xxan" ];
            expect(Session.getInstance().getDependingNubs("Z2xxan", undefined, true, true)).toContain(verif);
        });

        it("le Verificateur ne devrait pas être dépendant d'un Nub Espèce non sélectionné", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-06T08:11:24.196Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Z2xxan","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Cabillaud"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"eGs4Nm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"cnBpMD","props":{"calcType":"Cloisons"},"children":[{"uid":"MjN2Yn","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"OWoxcn","props":{"calcType":"CloisonAval"},"children":[{"uid":"aDg4aH","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"YjJmYT","props":{"calcType":"ParSimulation","parType":"PLANE"},"meta":{"title":"PAR : simulation"},"children":[],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":10},{"symbol":"Z2","mode":"SINGLE","value":9},{"symbol":"ZD1","mode":"SINGLE"},{"symbol":"S","mode":"SINGLE","value":0.2},{"symbol":"P","mode":"SINGLE","value":0.4},{"symbol":"L","mode":"SINGLE","value":0.6},{"symbol":"Nb","mode":"SINGLE"},{"symbol":"ZR1","mode":"SINGLE","value":9.36},{"symbol":"ZR2","mode":"SINGLE","value":8.36},{"symbol":"ZD2","mode":"SINGLE"}]},{"uid":"OWsxdT","props":{"calcType":"Verificateur","nubToVerify":"eGs4Nm","speciesList":["Z2xxan"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const espece = Session.getInstance().findNubByUid("Z2xxan") as Espece;
            const verif = Session.getInstance().findNubByUid("OWsxdT") as Verificateur;
            // dépendences
            verif.speciesList = [ FishSpecies[FishSpecies.SPECIES_9b] ];
            expect(Session.getInstance().getDependingNubs("Z2xxan", undefined, true, true)).not.toContain(verif);
        });

        it("le Verificateur devrait être dépendant d'un Nub choisi comme passe à vérifier", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-06T08:11:24.196Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Z2xxan","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Cabillaud"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"eGs4Nm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"cnBpMD","props":{"calcType":"Cloisons"},"children":[{"uid":"MjN2Yn","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"OWoxcn","props":{"calcType":"CloisonAval"},"children":[{"uid":"aDg4aH","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"YjJmYT","props":{"calcType":"ParSimulation","parType":"PLANE"},"meta":{"title":"PAR : simulation"},"children":[],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":10},{"symbol":"Z2","mode":"SINGLE","value":9},{"symbol":"ZD1","mode":"SINGLE"},{"symbol":"S","mode":"SINGLE","value":0.2},{"symbol":"P","mode":"SINGLE","value":0.4},{"symbol":"L","mode":"SINGLE","value":0.6},{"symbol":"Nb","mode":"SINGLE"},{"symbol":"ZR1","mode":"SINGLE","value":9.36},{"symbol":"ZR2","mode":"SINGLE","value":8.36},{"symbol":"ZD2","mode":"SINGLE"}]},{"uid":"OWsxdT","props":{"calcType":"Verificateur","nubToVerify":"eGs4Nm","speciesList":["Z2xxan"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("eGs4Nm") as Pab;
            const par = Session.getInstance().findNubByUid("YjJmYT") as ParSimulation;
            const verif = Session.getInstance().findNubByUid("OWsxdT") as Verificateur;
            // dépendences
            verif.nubToVerify = pab;
            expect(Session.getInstance().getDependingNubs("eGs4Nm", undefined, true, true)).toContain(verif);
        });

        it("le Verificateur ne devrait pas être dépendant d'un Nub non choisi comme passe à vérifier", () => {
            // contexte
            Session.getInstance().clear();
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-05-06T08:11:24.196Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"Z2xxan","props":{"calcType":"Espece","species":"SPECIES_CUSTOM"},"meta":{"title":"Cabillaud"},"children":[],"parameters":[{"symbol":"OK","mode":"CALCUL"},{"symbol":"DHMaxS","mode":"SINGLE","value":0.35},{"symbol":"DHMaxP","mode":"SINGLE","value":0.35},{"symbol":"BMin","mode":"SINGLE","value":0.3},{"symbol":"PMinS","mode":"SINGLE","value":1},{"symbol":"PMinP","mode":"SINGLE","value":1},{"symbol":"LMinS","mode":"SINGLE","value":2.5},{"symbol":"LMinP","mode":"SINGLE","value":2.5},{"symbol":"HMin","mode":"SINGLE","value":0.3},{"symbol":"YMin","mode":"SINGLE","value":0.4},{"symbol":"VeMax","mode":"SINGLE","value":2.5},{"symbol":"YMinSB","mode":"SINGLE","value":0.2},{"symbol":"YMinPB","mode":"SINGLE","value":0.3}]},{"uid":"eGs4Nm","props":{"calcType":"Pab"},"meta":{"title":"PAB"},"children":[{"uid":"cnBpMD","props":{"calcType":"Cloisons"},"children":[{"uid":"MjN2Yn","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"LB","mode":"SINGLE","value":10},{"symbol":"BB","mode":"SINGLE","value":1},{"symbol":"ZRMB","mode":"SINGLE","value":0},{"symbol":"ZRAM","mode":"SINGLE","value":0},{"symbol":"QA","mode":"SINGLE","value":0}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":99}],"downWall":{"uid":"OWoxcn","props":{"calcType":"CloisonAval"},"children":[{"uid":"aDg4aH","props":{"calcType":"Structure","loiDebit":"OrificeSubmerged"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7}]}],"parameters":[{"symbol":"ZRAM","mode":"SINGLE","value":0}]}},{"uid":"YjJmYT","props":{"calcType":"ParSimulation","parType":"PLANE"},"meta":{"title":"PAR : simulation"},"children":[],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":10},{"symbol":"Z2","mode":"SINGLE","value":9},{"symbol":"ZD1","mode":"SINGLE"},{"symbol":"S","mode":"SINGLE","value":0.2},{"symbol":"P","mode":"SINGLE","value":0.4},{"symbol":"L","mode":"SINGLE","value":0.6},{"symbol":"Nb","mode":"SINGLE"},{"symbol":"ZR1","mode":"SINGLE","value":9.36},{"symbol":"ZR2","mode":"SINGLE","value":8.36},{"symbol":"ZD2","mode":"SINGLE"}]},{"uid":"OWsxdT","props":{"calcType":"Verificateur","nubToVerify":"eGs4Nm","speciesList":["Z2xxan"]},"meta":{"title":"Vérification"},"children":[],"parameters":[]}]}`;
            Session.getInstance().unserialise(sess);
            const pab = Session.getInstance().findNubByUid("eGs4Nm") as Pab;
            const par = Session.getInstance().findNubByUid("YjJmYT") as ParSimulation;
            const verif = Session.getInstance().findNubByUid("OWsxdT") as Verificateur;
            // dépendences
            verif.nubToVerify = par;
            expect(Session.getInstance().getDependingNubs("eGs4Nm", undefined, true, true)).not.toContain(verif);
        });
    });

});
