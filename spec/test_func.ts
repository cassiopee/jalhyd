import { isNumeric } from "../src/base";
import { CalculatorType } from "../src/compute-node";
import { Nub } from "../src/nub";
import { cLog } from "../src/util/log";
import { Message, MessageCode } from "../src/util/message";
import { Result } from "../src/util/result";
import { precDigits, precDist } from "./test_config";

export class JasmineResult {
    public static currentResult: any;
}

/**
 * Compare deux valeurs réelles à un epsilon près
 * @param val1 Première valeur à comparer
 * @param val2 Deuxième valeur à comparer
 * @param epsilon Erreur de comparaison acceptable
 * @return Vrai si les valeurs sont égales, faux sinon
 */
export function equalEpsilon(val1: number, val2: number, epsilon: number = precDist) {
    return Math.abs(val1 - val2) < epsilon;
}

export function checkPercent(valTest: number, valRef: number, rPercent: number) {
    const r: number = Math.abs(valTest - valRef) / valRef;
    const bounds = valRef * rPercent;
    expect(r < rPercent).toBeTruthy(`${valTest} should be between ${valRef - bounds} and ${valRef + bounds}`);
}

/**
 * compare 2 objets
 * @param s message
 * @param objTest objet à tester
 * @param objValid objet de référence
 * @param epsilon tolérance pour les comparaisons de nombres
 */
export function compareObject(
    s: string,
    objTest: { [key: number]: number },
    objValid: { [key: number]: number },
    epsilon: number,
) {
    const n1 = Object.keys(objTest).length;
    const n2 = Object.keys(objValid).length;
    let b: boolean = n1 === n2;
    expect(b).toBeTruthy(s + ": longueur incorrecte " + n1 + ", devrait etre " + n2);
    if (!b) { return; }

    for (let i = 0; i < n1; i++) {
        const v1: number = objTest[+Object.keys(objTest)[i]];
        const v2: number = objValid[+Object.keys(objValid)[i]];
        b = equalEpsilon(v1, v2, epsilon);
        expect(b).toBeTruthy(s + " : " + i + "ieme valeur incorrecte " + v1 + ", devrait etre " + v2);
        if (!b) { return; }
    }
}

/**
 * compare 2 tableaux
 * @param s message
 * @param arrTest tableau à tester
 * @param arrValid tableau de référence
 * @return false en cas d'échec
 */
export function compareArray(s: string, arrTest: number[], arrValid: number[]): boolean {
    const n1 = arrTest.length;
    const n2 = arrValid.length;
    let b: boolean = n1 === n2;
    expect(b).toBeTruthy(s + ": longueur incorrecte " + n1 + ", devrait etre " + n2);
    if (!b) { return false; }

    for (let i = 0; i < arrTest.length; i++) {
        const v1: number = arrTest[i];
        const v2: number = arrValid[i];
        b = equalEpsilon(v1, v2);
        expect(b).toBeTruthy(s + " : " + i + "ieme valeur incorrecte " + v1 + ", devrait etre " + v2);
        if (!b) { return false; }
    }

    return true;
}
// export function compareArray2(s: string, arrTest: string[], arrValid: string[]): boolean {
//     const n1 = arrTest.length;
//     const n2 = arrValid.length;
//     let b: boolean = n1 === n2;
//     expect(b).toBeTruthy(s + ": longueur incorrecte " + n1 + ", devrait etre " + n2);
//     if (!b) { return false; }

//     for (let i = 0; i < arrTest.length; i++) {
//         const v1: number = +arrTest[i];
//         const v2: number = +arrValid[i];
//         b = equalEpsilon(v1, v2);
//         expect(b).toBeTruthy(s + " : " + i + "ieme valeur incorrecte " + v1 + ", devrait etre " + v2);
//         if (!b) { return false; }
//     }

//     return true;
// }

/**
 * compare 2 journaux
 */
export function compareLog(logTest: cLog, logValid: cLog) {
    // console.log(JSON.stringify(logTest));
    // console.log("journal :\n" + logTest.toString());

    // taille

    const n1 = logTest.messages.length;
    const n2 = logValid.messages.length;
    let b: boolean = n1 === n2;
    expect(b).toBeTruthy("journal : nombre de messages incorrect (" + n1 + "), devrait etre " + n2);
    if (!b) { return; }

    // codes

    for (let i = 0; i < n1; i++) {
        const m1: Message = logTest.messages[i];
        const m2: Message = logValid.messages[i];
        b = m1.code === m2.code;
        expect(b).toBeTruthy(
            "journal : message n°" + i + ", code " + MessageCode[m1.code]
            + " incorrect, devrait être " + MessageCode[m2.code],
        );
        if (!b) { return; }
    }

    // données

    for (let i = 0; i < n1; i++) {
        const m1: Message = logTest.messages[i];
        const m2: Message = logValid.messages[i];
        const code1 = MessageCode[m1.code];

        // taille des données

        const nd1 = Object.keys(m1.extraVar).length;
        const nd2 = Object.keys(m2.extraVar).length;
        b = nd1 === nd2;
        expect(b).toBeTruthy(
            "journal : message n°" + i + ", code " + code1 + " : nombre de données incorrect "
            + nd1 + ", devrait etre " + nd2,
        );
        if (!b) { return; }

        // clés des données

        for (let j = 0; j < m1.extraVar.length; j++) {
            b = m2.extraVar[j] !== undefined;
            expect(b).toBeTruthy(
                "journal : message n°" + i + ", code " + code1 + " : la donnée " + j + "=" + m1.extraVar[j]
                + " ne devrait pas être présente");
            // if (!b) return;
        }

        for (let j = 0; j < m2.extraVar.length; j++) {
            b = m1.extraVar[j] !== undefined;
            expect(b).toBeTruthy(
                "journal : message n°" + i + ", code " + code1 + " : la donnée " + j + "=" + m2.extraVar[j]
                + " devrait être présente");
            // if (!b) return;
        }

        // type des données

        for (let j = 0; j < m1.extraVar.length; j++) {
            b = typeof m1.extraVar[j] === typeof m2.extraVar[j];
            expect(b).toBeTruthy(
                "journal : " + i + "ieme message, code " + code1 + " : la donnée " + j + "=" + m1.extraVar[j]
                + " a un type " + (typeof m1.extraVar[j]) + " incorrect, devrait être du type "
                + (typeof m2.extraVar[j]));
            if (!b) { return; }
        }

        // valeur des données

        for (let j = 0; j < m1.extraVar.length; j++) {
            const d: any = m1.extraVar[j];
            if (typeof d === "number") {
                b = equalEpsilon(d, m2.extraVar[j]);
            } else {
                b = d === m2.extraVar[j];
            }
            expect(b).toBeTruthy(
                "journal : " + i + "ieme message, code " + code1 + " : la donnée " + j + "=" + m1.extraVar[j]
                + " a une valeur incorrecte, devrait être " + m2.extraVar[j]);
            if (!b) { return; }
        }
    }
}

export function checkResult(result: Result, valRef: number, prec?: number) {
    expect(result.ok).toBeTruthy((!result.ok) ? "Result: ERROR code=" + MessageCode[result.log.messages[0].code] : "");
    if (result.ok) {
        /*
         * on demande une précision de vérification inférieure à la précision de calcul
         * pour ne pas échouer sur un arrondi sur le dernier chiffre
         * par ex :
         *   précision demandée 0.001
         *   valeur attendue 1.301
         *   valeur calculée 1.3016, arrondie à 1.302, faisant échouer le test
         */
        if (prec === undefined) {
            prec = Math.max(0, precDigits - 1);
        }

        expect(result.vCalc).toBeCloseTo(valRef, prec);
    }
}

/**
 * Compare the content of two results objects
 * @param rTest Result to test
 * @param rRef Result used as reference
 * @param prec numbers of digits of the needed precision
 */
export function compareTwoResults(rTest: Result, rRef: Result, prec: number = Math.max(0, precDigits - 1),
    symbolsIgnored: string[] = []) {
    expect(rTest.ok).toEqual(rRef.ok, `ok should be $rRef.ok`);
    if (!rTest.ok) { return; }
    expect(rTest.resultElements.length).toEqual(rRef.resultElements.length, `resultElements.length should be $rRef.ok`);
    if (rTest.resultElements.length !== rRef.resultElements.length) { return; }
    for (let i = 0; i < rRef.resultElements.length; i++) {
        for (const k of Object.keys(rRef.resultElements[i].values)) {
            if (symbolsIgnored.includes(k)) {
                continue;
            }
            expect(rTest.resultElements[i].values[k])
                .toBeCloseTo(
                    rRef.resultElements[i].values[k],
                    prec,
                    k
                );
        }
    }
}

export function checkResultConsistency(nub: Nub, r?: Result) {
    let bExploreChildren: boolean = false;
    if (r === undefined) {
        r = nub.result;
        bExploreChildren = true;
    }
    check1ResultConsistency(r, nub.calcType !== CalculatorType.SectionParametree && nub.calcType !== CalculatorType.Grille);
    if(bExploreChildren) {
        checkChildrenResultConsistency(nub);
    }
}

function checkChildrenResultConsistency(nub: Nub) {
    if(nub.getChildren().length > 0) {
        for(const child of nub.getChildren()) {
            check1ResultConsistency(child.result);
            checkChildrenResultConsistency(child);
        }
    }
}

function check1ResultConsistency(r: Result, bTestvCalc: boolean = false) {
    if (r.ok) {
        if (bTestvCalc) {
            // Pas de vCalc sur les sections paramétrées ni sur les grilles
            expect(isFiniteNumber(r.vCalc))
                .toBe(true, `vCalc = ${r.vCalc} isn't a finite number`);
        }
        for (const re of r.resultElements) {
            if (Object.keys(re.values).length > 0) {
                for (const key in re.values) {
                    if (re.values.hasOwnProperty(key)) {
                        const er = re.values[key];
                        expect(isFiniteNumber(er))
                            .toBe(
                                true,
                                `extraResult[${key}] = ${er} isn't a finite number`
                            );
                    }
                }
            }
        }
    } else {
        expect(r.hasErrorMessages()).toBe(true, "Result KO and no error message");
    }
}

/**
 * compare un résultat complémentaire et un tableau
 * @param s message
 * @param res Result contenant le résultat complémentaire
 * @param key clé du résultat complémentaire
 * @param objValid tableau de valeurs de référence
 * @param epsilon tolérance pour les comparaisons de nombres
 */
export function compareExtraResult(
    s: string,
    res: Result,
    key: string,
    objValid: { [key: number]: number },
    epsilon: number,
) {
    const nre = res.resultElements.length;
    let n1 = 0;
    for (let i = 0; i < nre; i++) {
        if (res.resultElements[i].getValue(key) !== undefined) {
            n1++;
        }
    }

    const n2 = Object.keys(objValid).length;
    let b: boolean = n1 === n2;
    expect(b).toBeTruthy(s + ": longueur incorrecte " + n1 + ", devrait etre " + n2);
    if (!b) { return; }

    for (let i = 0; i < n1; i++) {
        const v1: number = res.resultElements[i].getValue(key);
        const v2: number = objValid[+Object.keys(objValid)[i]];
        b = equalEpsilon(v1, v2, epsilon);
        expect(b).toBeTruthy(s + " : " + i + "ieme valeur incorrecte " + v1 + ", devrait etre " + v2);
        if (!b) { return; }
    }
}

/**
 * Add Property currentSpec to jasmine in order to inspect spec results in afterEach function
 * @note How to inspect results of a spec afterward https://stackoverflow.com/a/39882823
 */
export function SetJasmineCurrentSpec() {
    jasmine.getEnv().addReporter({
        specStarted(result: any) {
            JasmineResult.currentResult = result;
        },
        specDone() {
            JasmineResult.currentResult = null;
        }
    });
}

/**
 * Test if the variable is numeric and finite
 * @param x the variable to test
 */
export function isFiniteNumber(x: any) {
    return isNumeric(x); // Infinity does not cause trouble
}

export function extraResultLength(res: Result, name: string): number {
    let nb = 0;
    for (const re of res.resultElements) {
        if (re.getValue(name) !== undefined) {
            nb++;
        }
    }
    return nb;
}
