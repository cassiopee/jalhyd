
import { Message, MessageCode, MessageSeverity } from "../src/util/message";
import { nub } from "./nubtest";
import { precDigits } from "./test_config";

describe("Class Nub: ", () => {
    describe("Calc(): ", () => {
        it("should return a result.vCalc equal to 3", () => {
            expect(nub.Calc("C").vCalc).toBeCloseTo(3, precDigits);
        });
        it("should return a result.vCalc equal to 1", () => {
            const r = nub.Calc("A");
            const v = r.vCalc;
            expect(v).toBeCloseTo(1, precDigits);
        });
        it("should return a result.vCalc equal to 2", () => {
            expect(nub.Calc("B").vCalc).toBeCloseTo(2, precDigits);
        });
    });
});

describe("classe Message: ", () => {
    describe("getSeverity(): ", () => {
        it("criticité MessageCode.ERROR_DICHO_CONVERGE == MessageSeverity.ERROR", () => {
            const m: Message = new Message(MessageCode.ERROR_DICHO_CONVERGE);
            expect(m.getSeverity()).toEqual(MessageSeverity.ERROR);
        });
    });
});
