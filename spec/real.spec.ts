import { floatDivAndMod, isEqual } from "../src/base";

/**
 * Tests d'égalité, de divisions entières et de modulo, à la tolérance près
 */

const divAndModCases = [
    { D: 3, d: 0.2, q: 15, r: 0 },
    { D: 3, d: 0.1, q: 30, r: 0 },
    { D: 3, d: 0.3, q: 10, r: 0 },
    { D: 3, d: 3, q: 1, r: 0 },
    { D: 3, d: 3.00000000001, q: 1, r: 0 }
];

for (const c of divAndModCases) {
    describe("real numbers division and modulo - ", () => {
        it(`(${c.D} / ${c.d}) should be ${c.q}, (${c.D} % ${c.d}) should be ${c.r}`, () => {
            const divAndMod = floatDivAndMod(c.D, c.d);
            expect(divAndMod.q).toBe(c.q);
            expect(divAndMod.r).toBe(c.r);
        });
    });
}

const eqCases = [
    { a: 0, b: 0, eq: true },
    { a: 0, b: 0.0000000000001, eq: true },
    { a: 0.0000000000001, b: 0, eq: true },
    { a: 0.0000000000001, b: 0.0000000000001, eq: true },
    { a: 0, b: 0.1, eq: false }
];

for (const c of eqCases) {
    describe("real numbers equality - ", () => {
        it(c.a + " should " + (c.eq ? "" : "not ") + " be equal to " + c.b, () => {
            expect(isEqual(c.a, c.b)).toBe(c.eq);
        });
    });
}
