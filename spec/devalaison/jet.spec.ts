import { Jet } from "../../src/devalaison/jet";
import { JetParams } from "../../src/devalaison/jet_params";
import { MessageCode, ParamCalculability } from "../../src/index";

function newJet(): Jet {
    return new Jet(
        new JetParams(5, 0.03, 0, -5.04928, -6, 5.14768),
        false
    );
}

let jet: Jet = newJet();

describe("Class Jet − ", () => {
    beforeEach(() => {
        jet = newJet();
    });
    for (const p of jet.parameterIterator) {
        if ([ ParamCalculability.EQUATION, ParamCalculability.DICHO ].includes(p.calculability)) {
            it(`Calc(${p.symbol}) should return ${p.currentValue}`, () => {
                jet.calculatedParam = jet.getParameter(p.symbol);
                const ref: number = p.currentValue;
                jet.calculatedParam.singleValue = jet.calculatedParam.singleValue / 2;
                expect(jet.CalcSerie().vCalc).toBeCloseTo(ref, 3);
                expect(jet.result.values.t).toBeCloseTo(1.03, 3);
                expect(jet.result.values.Vx).toBeCloseTo(4.99775, 3);
                expect(jet.result.values.Vz).toBeCloseTo(-9.95437, 3);
                expect(jet.result.values.Vt).toBeCloseTo(11.13853, 3);
            });
        }
    }

    describe("initial slope − ", () => {
        it("jalhyd#182 : initial slope should lead to same trajectory when variating and fixed", () => {
            jet.calculatedParam = jet.prms.D;
            // fixed
            jet.prms.S.singleValue = 0.8;
            jet.CalcSerie();
            const trajFixed = jet.generateTrajectories()[0];
            // variating
            jet.prms.S.setValues(0.8, 0.9, 0.1);
            jet.CalcSerie();
            const trajVar = jet.generateTrajectories()[0];
            // comparison
            expect(trajFixed).toEqual(trajVar);
        });
    });

    describe("trajectory − ", () => {
        it("changing jet start elevation should change Y of the first point", () => {
            jet.calculatedParam = jet.prms.D;
            // ZJ = 30
            jet.prms.ZJ.singleValue = 30;
            jet.CalcSerie();
            const traj30 = jet.generateTrajectories()[0];
            // ZJ = 25
            jet.prms.ZJ.singleValue = 25;
            jet.CalcSerie();
            const traj25 = jet.generateTrajectories()[0];
            // check
            expect(traj30[0][1]).toBe(30);
            expect(traj25[0][1]).toBe(25);
        });
    });

    describe("errors and warnings − ", () => {
        it("water underground (fixed), there should be a warning", () => {
            jet.prms.ZJ.singleValue = 30;
            jet.prms.ZW.singleValue = 28.2;
            jet.prms.ZF.singleValue = 28.5;
            jet.calculatedParam = jet.prms.D;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_WATER_ELEVATION_UNDERGROUND);
        });
        it("water underground (calculated), there should be a warning", () => {
            jet.prms.ZJ.singleValue = 30;
            jet.prms.D.singleValue = 3.709;
            jet.prms.ZF.singleValue = 28.5;
            jet.calculatedParam = jet.prms.ZW;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_WATER_ELEVATION_UNDERGROUND);
        });
        it("jet underground (fixed), there should be two warnings", () => {
            jet.prms.ZJ.singleValue = 28;
            jet.prms.ZW.singleValue = 27;
            jet.prms.ZF.singleValue = 28.5;
            jet.calculatedParam = jet.prms.D;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_WATER_ELEVATION_UNDERGROUND);
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_JET_START_ELEVATION_UNDERGROUND);
        });
        it("jet underground (calculated), there should be two warnings", () => {
            jet.prms.S.singleValue = 0.3;
            jet.prms.D.singleValue = 3.003;
            jet.prms.ZW.singleValue = 27;
            jet.prms.ZF.singleValue = 28.5;
            jet.calculatedParam = jet.prms.ZJ;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_WATER_ELEVATION_UNDERGROUND);
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_JET_START_ELEVATION_UNDERGROUND);
        });
        it("jet submerged (fixed) with solution, there should be a warning", () => {
            jet.prms.ZJ.singleValue = 29.1;
            jet.prms.ZW.singleValue = 29.2;
            jet.prms.ZF.singleValue = 28.5;
            jet.prms.S.singleValue = 0.9;
            jet.calculatedParam = jet.prms.D;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_START_SUBMERGED);
        });
        it("jet submerged (calculated) with solution, there should be a warning", () => {
            jet.prms.D.singleValue = 0.950;
            jet.prms.ZW.singleValue = 29.2;
            jet.prms.ZF.singleValue = 24.5;
            jet.prms.S.singleValue = 0.9;
            jet.calculatedParam = jet.prms.ZJ;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_JET_START_SUBMERGED);
        });
        it("jet submerged without solution, there should be an error and a warning", () => {
            jet.prms.ZJ.singleValue = 29.1;
            jet.prms.ZW.singleValue = 29.2;
            jet.prms.ZF.singleValue = 28.5;
            jet.prms.S.singleValue = 0;
            jet.calculatedParam = jet.prms.D;
            const res = jet.CalcSerie();
            expect(res.log.messages.length).toBe(2);
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_JET_SUBMERGED_NO_SOLUTION);
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_JET_START_SUBMERGED);
        });
    });

    it("calculate slope with empty values − ", () => {
        // erase all values, as nghyd does when creating a new calc
        jet.prms.D.singleValue = undefined;
        jet.prms.S.singleValue = undefined;
        jet.prms.V0.singleValue = undefined;
        jet.prms.ZF.singleValue = undefined;
        jet.prms.ZW.singleValue = undefined;
        jet.prms.ZJ.singleValue = undefined;
        // calculate D (DICHO)
        jet.calculatedParam = jet.prms.D;
        // set values for all params except D
        jet.prms.S.singleValue = 0.01;
        jet.prms.V0.singleValue = 1;
        jet.prms.ZF.singleValue = 10;
        jet.prms.ZW.singleValue = 11;
        jet.prms.ZJ.singleValue = 12;
        // fixed
        const res = jet.CalcSerie();
        // comparison
        expect(res).toBeDefined();
        expect(res.vCalc).toBeCloseTo(0.452, 3);
    });

});
