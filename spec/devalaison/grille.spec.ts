import { Grille, GrilleProfile, GrilleType } from "../../src/devalaison/grille";
import { GrilleParams } from "../../src/devalaison/grille_params";
import { MessageCode } from "../../src/util/message";

function newGrilleConventionnelle(): Grille {
    const g = new Grille(
        new GrilleParams(1.45, 100, 101.5, 101.5, 2, 72, 90, 20, 20, 20, 2, 1.5, 0.5, undefined, undefined, undefined),
        false
    );
    g.type = GrilleType.Conventional;
    g.prms.Beta.singleValue = 72;
    return g;
}

function newGrilleOrientee(): Grille {
    const g = new Grille(
        new GrilleParams(2.3, 100, 101.5, 101.5, 2, 90, 40, 10, 80, 20, 2, 1.5, 0.5, undefined, undefined, undefined),
        false
    );
    g.type = GrilleType.Oriented;
    g.prms.Alpha.singleValue = 40;
    return g;
}

function newGrilleInclinee(): Grille {
    const g = new Grille(
        new GrilleParams(3.2, 100, 101.5, 101.5, 2, 23, 90, 20, 85, 20, 2, 1.5, undefined, 0.5, 0.1, 4),
        false
    );
    g.type = GrilleType.Inclined;
    g.prms.Beta.singleValue = 23;
    return g;
}

let gc: Grille;
let go: Grille;
let gi: Grille;

describe("Class Grille, ", () => {

    describe("grille conventionnelle - ", () => {

        beforeEach(() => {
            gc = newGrilleConventionnelle();
        });

        it("profil rectangulaire", () => {
            gc.profile = GrilleProfile.Rectangular;
            const r = gc.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(0.4833, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(0.4833, 4);
            expect(r.resultElement.values.RFB).toBe(1);
            expect(r.resultElement.values.REEB).toBe(1);
            expect(r.resultElement.values.OB).toBe(0.5);
            expect(r.resultElement.values.LG).toBeCloseTo(1.5772, 4);
            expect(r.resultElement.values.D).toBeCloseTo(0.4874, 4);
            expect(r.resultElement.values.DG).toBeCloseTo(0.4874, 4);
            expect(r.resultElement.values.SG).toBeCloseTo(3.1544, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4597, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(2.9791, 4);
            expect(r.resultElement.values.DH60).toBeCloseTo(27.3767, 4);
            expect(r.resultElement.values.a).toBeCloseTo(2.89, 2);
        });

        it("profil hydrodynamique", () => {
            gc.profile = GrilleProfile.Hydrodynamic;
            const r = gc.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(0.4833, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(0.4833, 4);
            expect(r.resultElement.values.RFB).toBe(1);
            expect(r.resultElement.values.REEB).toBe(1);
            expect(r.resultElement.values.OB).toBe(0.5);
            expect(r.resultElement.values.LG).toBeCloseTo(1.5772, 4);
            expect(r.resultElement.values.D).toBeCloseTo(0.4874, 4);
            expect(r.resultElement.values.DG).toBeCloseTo(0.4874, 4);
            expect(r.resultElement.values.SG).toBeCloseTo(3.1544, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4597, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(1.7524, 4);
            expect(r.resultElement.values.DH60).toBeCloseTo(16.1039, 4);
            expect(r.resultElement.values.a).toBeCloseTo(1.7, 1);
        });

        it("vitesse supérieure à la préconisation", () => {
            gc.profile = GrilleProfile.Rectangular;
            gc.prms.QMax.singleValue = 10;
            const r = gc.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(true);
            expect(r.resultElement.log.messages[0].code).toBe(MessageCode.WARNING_GRILLE_VN_GREATER_THAN_05);
        });

        it("obstruction totale inférieure à l'obstruction due aux barreaux seulement", () => {
            gc.profile = GrilleProfile.Rectangular;
            gc.prms.O.singleValue = 0.24;
            const r = gc.CalcSerie();
            expect(r.values.OB).toBe(0.5);
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(true);
            expect(r.resultElement.log.messages[0].code).toBe(MessageCode.WARNING_GRILLE_O_LOWER_THAN_OB);
        });
    });

    describe("grille orientée - ", () => {

        beforeEach(() => {
            go = newGrilleOrientee();
        });

        it("profil rectangulaire", () => {
            go.profile = GrilleProfile.Rectangular;
            const r = go.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(0.7667, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(0.7667, 4);
            expect(r.resultElement.values.RFB).toBe(0.125);
            expect(r.resultElement.values.REEB).toBe(2);
            expect(r.resultElement.values.OB).toBeCloseTo(0.3333, 4);
            expect(r.resultElement.values.LG).toBeUndefined();
            expect(r.resultElement.values.D).toBeUndefined();
            expect(r.resultElement.values.DG).toBeUndefined();
            expect(r.resultElement.values.SG).toBeCloseTo(4.6672, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4928, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(12.3342, 4);
            expect(r.resultElement.values.DH60).toBeCloseTo(80.0903, 4);
            expect(r.resultElement.values.a).toBeCloseTo(2.89, 2);
            expect(r.resultElement.values.c).toBeCloseTo(1.69, 2);
        });

        it("profil hydrodynamique", () => {
            go.profile = GrilleProfile.Hydrodynamic;
            const r = go.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(0.7667, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(0.7667, 4);
            expect(r.resultElement.values.RFB).toBe(0.125);
            expect(r.resultElement.values.REEB).toBe(2);
            expect(r.resultElement.values.OB).toBeCloseTo(0.3333, 4);
            expect(r.resultElement.values.LG).toBeUndefined();
            expect(r.resultElement.values.D).toBeUndefined();
            expect(r.resultElement.values.DG).toBeUndefined();
            expect(r.resultElement.values.SG).toBeCloseTo(4.6672, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4928, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(8.65, 2); // precision reduces because of jalhyd#142
            expect(r.resultElement.values.DH60).toBeCloseTo(47.3122, 4);
            expect(r.resultElement.values.a).toBeCloseTo(1.7, 1);
            expect(r.resultElement.values.c).toBeCloseTo(2.78, 2);
        });

        it("Alpha supérieur à la préconisation", () => {
            go.profile = GrilleProfile.Rectangular;
            go.prms.Alpha.singleValue = 46;
            const r = go.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(true);
            expect(r.resultElement.log.messages[0].code).toBe(MessageCode.WARNING_GRILLE_ALPHA_GREATER_THAN_45);
        });

        it("profil personnalisé", () => {
            go.profile = GrilleProfile.Custom;
            let r = go.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            const dh60values: number[] = [];
            // calculate with default values
            dh60values.push(r.resultElement.values.DH60);
            // modify a
            const previousA = go.prms.a.singleValue;
            go.prms.a.singleValue = 2.321;
            r = go.CalcSerie();
            expect(dh60values.includes(r.resultElement.values.DH60)).toBe(false);
            dh60values.push(r.resultElement.values.DH60);
            // restore a and modify c
            go.prms.a.singleValue = previousA;
            go.prms.c.singleValue = 1.753;
            r = go.CalcSerie();
            expect(dh60values.includes(r.resultElement.values.DH60)).toBe(false);
            dh60values.push(r.resultElement.values.DH60);
            // modify both a and c
            go.prms.a.singleValue = 2.321;
            r = go.CalcSerie();
            expect(dh60values.includes(r.resultElement.values.DH60)).toBe(false);
        });
    });

    describe("grille inclinée - ", () => {
        beforeEach(() => {
            gi = newGrilleInclinee();
        });

        it("profil rectangulaire", () => {
            gi.profile = GrilleProfile.Rectangular;
            const r = gi.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(1.0667, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(1.0667, 4);
            expect(r.resultElement.values.RFB).toBeCloseTo(0.2353, 4);
            expect(r.resultElement.values.REEB).toBe(1);
            expect(r.resultElement.values.OB).toBe(0.5);
            expect(r.resultElement.values.LG).toBeCloseTo(3.8390, 4);
            expect(r.resultElement.values.D).toBeCloseTo(3.5338, 4);
            expect(r.resultElement.values.DG).toBeCloseTo(3.5338, 4);
            expect(r.resultElement.values.SG).toBeCloseTo(7.6779, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4168, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(7.6808, 4);
            expect(r.resultElement.values.DH60).toBeCloseTo(37.844, 4);
            expect(r.resultElement.values.a).toBeCloseTo(3.85, 2);
        });

        it("profil hydrodynamique", () => {
            gi.profile = GrilleProfile.Hydrodynamic;
            const r = gi.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(false);

            expect(r.resultElement.values.H).toBe(1.5);
            expect(r.resultElement.values.HG).toBe(1.5);
            expect(r.resultElement.values.S).toBe(3);
            expect(r.resultElement.values.SPDG).toBe(3);
            expect(r.resultElement.values.VA).toBeCloseTo(1.0667, 4);
            expect(r.resultElement.values.VAPDG).toBeCloseTo(1.0667, 4);
            expect(r.resultElement.values.RFB).toBeCloseTo(0.2353, 4);
            expect(r.resultElement.values.REEB).toBe(1);
            expect(r.resultElement.values.OB).toBe(0.5);
            expect(r.resultElement.values.LG).toBeCloseTo(3.8390, 4);
            expect(r.resultElement.values.D).toBeCloseTo(3.5338, 4);
            expect(r.resultElement.values.DG).toBeCloseTo(3.5338, 4);
            expect(r.resultElement.values.SG).toBeCloseTo(7.6779, 4);
            expect(r.resultElement.values.VN).toBeCloseTo(0.4168, 4);
            expect(r.resultElement.values.DH00).toBeCloseTo(6.1314, 4);
            expect(r.resultElement.values.DH60).toBeCloseTo(22.5841, 4);
            expect(r.resultElement.values.a).toBeCloseTo(2.1, 1);
        });

        it("Beta supérieur à la préconisation", () => {
            gi.profile = GrilleProfile.Rectangular;
            gi.prms.Beta.singleValue = 27;
            const r = gi.CalcSerie();
            expect(r.resultElements.length).toBe(1);
            expect(r.hasLog()).toBe(true);
            expect(r.resultElement.log.messages[0].code).toBe(MessageCode.WARNING_GRILLE_BETA_GREATER_THAN_26);
        });
    });

});
