import { CalculatorType } from "../../src/compute-node";
import { MessageCode } from "../../src/index";
import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { Props } from "../../src/props";
import { Session } from "../../src/session";

let nub: MacroRugo;

describe("Class MacroRugo: ", () => {
    describe("jalhyd #284 warnings about block concentration - ", () => {
        it("case 1: no warnings", () => {
            nub = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
            nub.prms.B.singleValue = 1.109;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(0);
        });
        it("case 2: concentration out of bounds", () => {
            nub = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
            nub.prms.C.singleValue = 0.07;
            nub.prms.B.singleValue = 3.024;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS);
        });
    });
});
