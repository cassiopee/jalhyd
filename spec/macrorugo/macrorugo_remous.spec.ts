import { MessageCode } from "../../src/internal_modules";
import { createMacrorugoRemousTest } from "./macrorugo_util";

describe("Class MacrorugoRemous: ", () => {
    it("Parameters should be the one of the MacroRugo", () => {
        const mrr = createMacrorugoRemousTest();
        mrr.syncSectionMacroRugo();
        const nubMR = mrr.nubMacroRugo;
        expect(mrr.section.prms.If.singleValue).toBeCloseTo(nubMR.prms.If.V);
        expect(mrr.section.prms.Q.singleValue).toBeCloseTo(nubMR.prms.Q.V);
        expect(mrr.section.prms.LargeurBerge.singleValue).toBeCloseTo(nubMR.prms.B.V);
    });
    it("Uniform regime should return normal depth", () => {
        const mrr = createMacrorugoRemousTest();
        const res = mrr.CalcSerie();
        expect(res.resultElements.length).toEqual(
            Math.round(mrr.prms.Long.singleValue / mrr.prms.Dx.singleValue + 1)
        );
        for (let i = 0; i < res.resultElements.length; i++) {
            expect(res.resultElements[i].values.Y).
            toBeCloseTo(mrr.prms.Z2.singleValue - mrr.prms.ZF2.singleValue);
        }
    });
    it("Upstream section should return normal depth on slight backwater curves", () => {
        const mrr = createMacrorugoRemousTest();
        mrr.syncSectionMacroRugo();
        const y = mrr.nubMacroRugo.prms.Y.singleValue;
        mrr.prms.Z2.singleValue -= 0.05;
        const resF2 = mrr.CalcSerie();
        expect(resF2.resultElements[0].values.Y).toBeCloseTo(y);
        mrr.prms.Z2.singleValue += 0.1;
        const resF1 = mrr.CalcSerie();
        expect(resF1.resultElements[0].values.Y).toBeCloseTo(y);
    });
    it("Upstream section should return normal depth on downstream fall", () => {
        const mrr = createMacrorugoRemousTest();
        mrr.syncSectionMacroRugo();
        const y = mrr.nubMacroRugo.prms.Y.singleValue;
        mrr.prms.Z2.singleValue -= 0.2;
        const resF2 = mrr.CalcSerie();
        expect(resF2.resultElements[0].values.Y).toBeCloseTo(y);
        expect(resF2.globalLog.messages[0].code).toBe(MessageCode.WARNING_MACRORUGOREMOUS_CHUTE_AVAL);
    });

});
