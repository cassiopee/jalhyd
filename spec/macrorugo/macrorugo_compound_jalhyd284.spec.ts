import { CalculatorType } from "../../src/compute-node";
import { MacrorugoCompound, MessageCode } from "../../src/index";
import { MRCInclination } from "../../src/macrorugo/mrc-inclination";
import { Props } from "../../src/props";
import { Session } from "../../src/session";

let nub: MacrorugoCompound;

describe("MacroRugoCompound: ", () => {
    beforeEach(() => {
        nub = Session.getInstance().createNub(
            new Props({ calcType: CalculatorType.MacroRugoCompound })
        ) as MacrorugoCompound;
        nub.setPropValue("inclinedApron", MRCInclination.INCLINED);
    });

    describe("jalhyd #284 warnings about block concentration − ", () => {
        it("case 1: no warnings", () => {
            nub.prms.BR.singleValue = 3.328;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            // children should not have any width-related warning
            for (const c of nub.children) {
                for (const re of c.result.resultElements) {
                    for (const m of re.log.messages) {
                        expect(m.code).not.toBe(MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS);
                    }
                }
            }
        });
        it("case 2: block concentration is out of bounds", () => {
            nub.prms.BR.singleValue = 3.024;
            nub.prms.C.singleValue = 0.07;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS);
            // children should not have any width-related warning
            for (const c of nub.children) {
                for (const re of c.result.resultElements) {
                    for (const m of re.log.messages) {
                        expect(m.code).not.toBe(MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS);
                    }
                }
            }
        });
    });
});
