import { CalculatorType, MessageCode, ParamCalculability, ParamValueMode, ParamsSectionRectang, Props, RegimeUniforme, Session, cSnRectang } from "../../src/index";
import { MacroRugo, MacroRugoFlowType } from "../../src/macrorugo/macrorugo";
import { MacrorugoParams } from "../../src/macrorugo/macrorugo_params";
import { checkResult } from "../test_func";

/*
*** Emergent conditions Cd=1.***
*** INPUT ***
ks: 0.010000,
D: 0.500000,
k = 0.600000
Cd0: 1.000000,
S: 0.050000,
B: 1.000000,
h: 0.400000,
C: 0.130000,
RESULTS:
find_Q_nat(0.736243)=0.913414
Q=0.370878  fVal=0.000000
ZF2: 12.200000,
Vdeb: 0.927194,
Vg: 1.449999,
Fr: 0.731987,
PV: 454.788830,
flowcond: emergent,
Vmax: 1.785249,
V_technique: 1.748990,
q_technique: 0.312101,
Strickler: 11.302142,
 */

function macroRugoInstanceEmergentCd1(): MacroRugo {
    return new MacroRugo(
        new MacrorugoParams(
            12.5,   // ZF1
            6,      // L
            1,      // B
            0.05,   // If
            0.370878,    // Q
            0.4,    // h
            0.01,   // Ks
            0.13,   // C
            0.5,    // D
            0.6,    // k
            1     // Cd0
        )
    );
}

const macroRugoExtraResultEmergentCd1: { [key: string]: number | MacroRugoFlowType } = {
    ENUM_MacroRugoFlowType: MacroRugoFlowType.EMERGENT,
    ZF2: 12.200000,
    Vdeb: 0.927194,
    Vg: 1.449999,
    Fr: 0.731987,
    PV: 454.788830,
    Vmax: 1.785249,
    Strickler: 11.302142
};

/*
*** Emergent conditions Cd=2***
*** INPUT ***
ks: 0.010000,
D: 0.500000,
k = 0.600000
Cd0: 2.000000,
S: 0.050000,
B: 1.000000,
h: 0.400000,
C: 0.130000,
RESULTS:
find_Q_nat(0.512451)=0.770391
Q=0.204295  fVal=0.000000
ZF2: 12.200000,
Vdeb: 0.510737,
Vg: 0.798720,
Fr: 0.403209,
PV: 250.516618,
flowcond: emergent,
Vmax: 1.248838,
V_technique: 1.427752,
q_technique: 0.266857,
Strickler: 6.225690,
*/

function macroRugoInstanceEmergentCd2(): MacroRugo {
    const nub: MacroRugo = macroRugoInstanceEmergentCd1();
    nub.prms.Cd0.singleValue = 2;
    nub.prms.Q.singleValue = 0.204295;
    return nub;
}

const macroRugoExtraResultEmergentCd2: { [key: string]: number | MacroRugoFlowType } = {
    ENUM_MacroRugoFlowType: MacroRugoFlowType.EMERGENT,
    ZF2: 12.200000,
    Vdeb: 0.510737,
    Vg: 0.798720,
    Fr: 0.403209,
    PV: 250.516618,
    Vmax: 1.248838,
    Strickler: 6.225690
};

/*
*** Submerged conditions  Cd=1***
*** INPUT ***
ks: 0.010000,
D: 0.500000,
k = 0.600000
Cd0: 1.000000,
S: 0.050000,
B: 1.000000,
h: 0.800000,
C: 0.130000,
RESULTS:
find_Q_nat(1.472487)=0.095732
Q=1.549073  fVal=0.000000
ZF2: 12.200000,
Vdeb: 1.936341,
Vg: 3.028159,
Fr: 1.080934,
PV: 949.775168,
flowcond: immerge,
q_technique: 1.060933,
Strickler: 18.999946,
 */

function macroRugoInstanceSubmerged(): MacroRugo {
    const nub: MacroRugo = macroRugoInstanceEmergentCd1();
    nub.prms.Y.singleValue = 0.8;
    nub.prms.Q.singleValue = 1.549073;
    return nub;
}

const macroRugoExtraResultSubmerged: { [key: string]: number | MacroRugoFlowType } = {
    ENUM_MacroRugoFlowType: MacroRugoFlowType.SUBMERGED,
    ZF2: 12.200000,
    Vdeb: 1.936341,
    PV: 949.775168,
    Strickler: 18.999946,
};

function MacroRugoFactory(sInstance: string): MacroRugo {
    let nub: MacroRugo;
    switch (sInstance) {
        case "EmergentCd1": {
            nub = macroRugoInstanceEmergentCd1();
            break;
        }
        case "EmergentCd2": {
            nub = macroRugoInstanceEmergentCd2();
            break;
        }
        case "Submerged": {
            nub = macroRugoInstanceSubmerged();
            break;
        }
        default: {
            throw new Error("Instance name error");
        }
    }
    for (const p of nub.parameterIterator) {
        p.v = p.singleValue;
    }
    return nub;
}

function testMacroRugo(sInstance: string, varTest: string, valRef: number) {
    it(`Calc(${varTest}) should be ${valRef}`, () => {
        const nub = MacroRugoFactory(sInstance);
        nub.prms.Q.v = nub.Calc("Q").vCalc;
        const p = nub.getParameter(varTest);
        p.v = undefined;
        p.valueMode = ParamValueMode.CALCUL;
        const r = nub.Calc(varTest, 0.1);
        checkResult(r, valRef);
    });
}

function testMacroRugoConfig(sInstance: string, Q0: number, fVal0: number, mrExtraRes: { [key: string]: number }) {
    describe(`Condition ${sInstance}`, () => {
        it(`resolveQ(${Q0}) should be ${fVal0}`, () => {
            const nubit = MacroRugoFactory(sInstance);
            nubit.prms.Q.v = Q0;
            // tslint:disable-next-line:no-string-literal
            nubit["setFlowType"]();
            // tslint:disable-next-line:no-string-literal
            expect(Math.abs(nubit["resolveQ"]())).toBeCloseTo(fVal0, 5);
        });
        const nub = MacroRugoFactory(sInstance);
        for (const prm of nub.prms) {
            if ([ParamCalculability.DICHO, ParamCalculability.EQUATION].includes(prm.calculability)) {
                testMacroRugo(sInstance, prm.symbol, prm.v);
            }
        }
        for (const sExtraRes in mrExtraRes) {
            if (mrExtraRes.hasOwnProperty(sExtraRes)) {
                it(`${sExtraRes} should be ${mrExtraRes[sExtraRes]}`, () => {
                    expect(MacroRugoFactory(sInstance).Calc("Q").values[sExtraRes])
                        .toBeCloseTo(mrExtraRes[sExtraRes], 3);
                });
            }
        }
    });
}

function macroRugoInstanceJalHyd85(): MacroRugo {
    const nubMR = macroRugoInstanceEmergentCd1();
    nubMR.prms.C.singleValue = 0.2;
    nubMR.prms.PBH.singleValue = 0.8;
    nubMR.calculatedParam = nubMR.prms.Q;
    return nubMR;
}

describe("Class MacroRugo: ", () => {

    testMacroRugoConfig("EmergentCd1", 0.736243, 0.913414, macroRugoExtraResultEmergentCd1);
    testMacroRugoConfig("EmergentCd2", 0.512451, 0.770391, macroRugoExtraResultEmergentCd2);
    // Le test passe en debug mais pas sous Jasmine !??
    // describe(`Condition Submerged` , () => {
    //     it(`resolveAlpha_t(0.07) should be 0.074752`, () => {
    //         // tslint:disable-next-line:no-string-literal
    //         expect(macroRugoInstanceSubmerged()["resolveAlpha_t"](0.07)).toBeCloseTo(0.074752, 5);
    //     });
    // });
    testMacroRugoConfig("Submerged", 1.472487, 0.095732, macroRugoExtraResultSubmerged);

    describe("JalHyd #85", () => {
        it("CalcSerie Q should return the good result :)", () => {
            const nubMR = macroRugoInstanceJalHyd85();
            const nubMR2 = macroRugoInstanceJalHyd85();

            nubMR.prms.Y.setValues(0.7, 1.2, 0.1);
            const aQ: number[] = [0.662919, 0.793504, 1.221194, 1.542033, 1.928510, 2.378835];

            nubMR.CalcSerie();
            for (let i = 0; i < aQ.length; i++) {
                expect(nubMR.result.resultElements[i].vCalc).toBeCloseTo(aQ[i], 4);
                nubMR2.prms.Y.setValue(0.7 + i * 0.1);
                expect(nubMR2.CalcSerie().vCalc).toBeCloseTo(aQ[i], 4);
            }
        });
    });

    describe("ramp width tolerance should be 1cm", () => {
        it("", () => {
            const prms = new MacrorugoParams(
                12.5,   // ZF1
                6,      // L
                2.2,      // B
                0.05,   // If
                0.1,    // Q
                0.6,    // h
                0.01,   // Ks
                0.13,   // C
                0.4,    // D
                0.6,    // k
                1     // Cd0
            );
            const mr = new MacroRugo(prms);
            mr.calculatedParam = mr.prms.Q;

            // ramp not wide enough : warning on tolerance (min width=1.109 m)

            prms.B.singleValue = 1.099;
            let res = mr.CalcSerie();
            expect(res.log.messages.length).toEqual(1);
            expect(res.log.messages[0].code).toEqual(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);

            // ramp just wide enough for one block pattern : no warning (min width=1.109 m)

            prms.B.singleValue = 1.1;
            res = mr.CalcSerie();
            expect(res.log.messages.length).toEqual(0);

            // ramp wide enough : warning on tolerance (B=2.2, closest rounded value=2.219, B-rounded > 0.01)

            prms.B.singleValue = 2.2;
            res = mr.CalcSerie();
            expect(res.log.messages.length).toEqual(1);
            expect(res.log.messages[0].code).toEqual(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);

            // ramp wide enough : no warning (B=2.21, closest rounded value=2.219, B-rounded < 0.01)

            prms.B.singleValue = 2.21;
            res = mr.CalcSerie();
            expect(res.log.messages.length).toEqual(0);
        });
    });
    describe("jalhyd#361", () => {
        it("Ks should be equal to the one calculated by RegimeUniforme", () => {
            const nubMR = Session.getInstance().createSessionNub(
                new Props({ calcType: CalculatorType.MacroRugo })
            ) as MacroRugo;
            nubMR.CalcSerie();
            const nubRU = Session.getInstance().createSessionNub(
                new Props({ calcType: CalculatorType.RegimeUniforme})
            ) as RegimeUniforme;
            nubRU.setSection(new cSnRectang(new ParamsSectionRectang(
                nubMR.prms.Y.singleValue,
                nubMR.prms.B.singleValue,
                10,
                nubMR.prms.Q.V,
                nubMR.prms.If.singleValue,
                1
            )));
            nubRU.calculatedParam = nubRU.section.prms.Ks;
            expect(nubMR.result.values["Strickler"]).toBeCloseTo(nubRU.Calc("Ks").vCalc)
        });
    })
});
