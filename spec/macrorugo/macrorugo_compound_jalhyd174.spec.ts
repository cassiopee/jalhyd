import { CalculatorType } from "../../src/compute-node";
import { MacrorugoCompound, MessageCode } from "../../src/index";
import { MRCInclination } from "../../src/macrorugo/mrc-inclination";
import { Props } from "../../src/props";
import { Session } from "../../src/session";

let nub: MacrorugoCompound;

describe("MacroRugoCompound: ", () => {

    describe("jalhyd #174 warnings about ramp width / block width, multiple aprons − ", () => {
        it("case 1: no warnings", () => {
            nub = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            nub.addDefaultChild();
            nub.prms.C.singleValue = 0.09; // easy sqrt
            nub.prms.PBD.singleValue = 0.6; // ax = 2
            nub.children[0].prms.B.singleValue = 3;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(0);
        });
        it("case 2: ramp #1 is too narrow", () => {
            nub = Session.getInstance().createNub(
                new Props({
                    calcType: CalculatorType.MacroRugoCompound,
                    inclinedApron: MRCInclination.NOT_INCLINED
                })
            ) as MacrorugoCompound;
            nub.addDefaultChild();
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.children[0].prms.B.singleValue = 1.95;
            const res = nub.CalcSerie();
            expect(nub.children[0].result.log.messages.length).toBe(1);
            expect(nub.children[0].result.log.messages[0].code)
                .toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
        });
        it("case 3: ramp #1 width is not a multiple of half pattern width", () => {
            nub = Session.getInstance().createNub(
                new Props({
                    calcType: CalculatorType.MacroRugoCompound,
                    inclinedApron: MRCInclination.NOT_INCLINED
                })
            ) as MacrorugoCompound;
            nub.addDefaultChild();
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.children[0].prms.B.singleValue = 5.8;
            const res = nub.CalcSerie();
            expect(nub.children[0].result.log.messages.length).toBe(1);
            expect(nub.children[0].result.log.messages[0].code)
                .toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
            expect(nub.children[0].result.log.messages[0].extraVar.halfPattern).toBe(1);
            expect(nub.children[0].result.log.messages[0].extraVar.lower).toBe(5);
            expect(nub.children[0].result.log.messages[0].extraVar.higher).toBe(6);
        });
    });

    describe("jalhyd #174 warnings about ramp width / block width, inclined apron − ", () => {
        it("case 1: no warnings", () => {
            nub = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            nub.setPropValue("inclinedApron", MRCInclination.INCLINED);
            nub.prms.C.singleValue = 0.09; // easy sqrt
            nub.prms.PBD.singleValue = 0.6; // ax = 2
            nub.prms.BR.singleValue = 3;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(0);
            // children should not have any width-related warning
            for (const c of nub.children) {
                for (const re of c.result.resultElements) {
                    for (const m of re.log.messages) {
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                    }
                }
            }
        });
        it("case 2: ramp #1 is too narrow", () => {
            nub = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            nub.setPropValue("inclinedApron", MRCInclination.INCLINED);
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.prms.BR.singleValue = 1.95;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
            expect(res.log.messages[0].extraVar.pattern).toBe(2);
            // children should not have any width-related warning
            for (const c of nub.children) {
                for (const re of c.result.resultElements) {
                    for (const m of re.log.messages) {
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                    }
                }
            }
        });
        it("case 3: ramp #1 width is not a multiple of half pattern width", () => {
            nub = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            nub.setPropValue("inclinedApron", MRCInclination.INCLINED);
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.prms.BR.singleValue = 5.8;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
            expect(res.log.messages[0].extraVar.halfPattern).toBe(1);
            expect(res.log.messages[0].extraVar.lower).toBe(5);
            expect(res.log.messages[0].extraVar.higher).toBe(6);
            // children should not have any width-related warning
            for (const c of nub.children) {
                for (const re of c.result.resultElements) {
                    for (const m of re.log.messages) {
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                        expect(m.code).not.toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                    }
                }
            }
        });
    });

});
