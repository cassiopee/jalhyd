import { CalculatorType } from "../../src/compute-node";
import { MessageCode } from "../../src/index";
import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { Props } from "../../src/props";
import { Session } from "../../src/session";

let nub: MacroRugo;

describe("Class MacroRugo: ", () => {
    describe("jalhyd #174 warnings about ramp width / block width − ", () => {
        it("case 1: no warnings", () => {
            nub = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
            nub.prms.C.singleValue = 0.09; // easy sqrt
            nub.prms.PBD.singleValue = 0.6; // ax = 2
            nub.prms.B.singleValue = 3;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(0);
        });
        it("case 2: ramp is too narrow", () => {
            nub = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.prms.B.singleValue = 1.95;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
        });
        it("case 3: ramp width is not a multiple of half pattern width", () => {
            nub = Session.getInstance().createNub(new Props({ calcType: CalculatorType.MacroRugo })) as MacroRugo;
            nub.prms.C.singleValue = 0.09;
            nub.prms.PBD.singleValue = 0.6;
            nub.prms.B.singleValue = 5.8;
            const res = nub.CalcSerie();
            expect(res.log.messages.length).toBe(1);
            expect(res.log.messages[0].code).toBe(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
            expect(res.log.messages[0].extraVar.halfPattern).toBe(1);
            expect(res.log.messages[0].extraVar.lower).toBe(5);
            expect(res.log.messages[0].extraVar.higher).toBe(6);
        });
    });
});
