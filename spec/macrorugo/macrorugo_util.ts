import { CalculatorType, MacroRugo, MacrorugoRemous, Props, Session } from "../../src/internal_modules";

export function createMacrorugoRemousTest(): MacrorugoRemous {
    const nubMR = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.MacroRugo })
    ) as MacroRugo;
    const mrr = Session.getInstance().createSessionNub(
        new Props({ calcType: CalculatorType.MacrorugoRemous })
    ) as MacrorugoRemous
    mrr.setPropValue("nubMacroRugo", nubMR.uid);
    return mrr;
}
