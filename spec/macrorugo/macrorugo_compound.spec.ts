import { CalculatorType } from "../../src/compute-node";
import { Session } from "../../src/index";
import { MacrorugoCompoundParams, MessageCode } from "../../src/internal_modules";
import { MacroRugo } from "../../src/macrorugo/macrorugo";
import { MacrorugoCompound } from "../../src/macrorugo/macrorugo_compound";
import { MacrorugoParams } from "../../src/macrorugo/macrorugo_params";
import { MRCInclination } from "../../src/macrorugo/mrc-inclination";
import { Props } from "../../src/props";
import { compareTwoResults } from "../test_func";

let BR: number;
let mrc2: MacrorugoCompound;

/**
 * Return default instance of MacroRugo as reference for one apron
 * @param i Offset of ZF1 (- 0.2 * i) and B (+ i)
 */
function getMacroRugoRef(i: number = 0): MacroRugo {
    const mr = Session.getInstance().createNub(
        new Props({ calcType: CalculatorType.MacroRugo })
    ) as MacroRugo;
    mr.prms.ZF1.singleValue -= 0.21 * i;
    mr.prms.Y.singleValue += 0.21 * i;
    mr.prms.B.singleValue += i;
    mr.prms.L.singleValue = 3 / mr.prms.If.currentValue;
    mr.prms.Q.setCalculated();
    mr.CalcSerie();
    return mr;
}

describe("MacroRugoCompound: ", () => {
    describe("Default 1 apron", () => {
        it("should return same result as Macrorugo (1)", () => {
            const mrc = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            mrc.addDefaultChild();
            const mr = getMacroRugoRef();
            expect(mrc.CalcSerie().vCalc).toBeCloseTo(mr.result.vCalc, 3);
        });
        it("should return same result as Macrorugo (2)", () => {
            const mrc = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            mrc.addDefaultChild();
            mrc.children[0].prms.L.singleValue = 0.5;
            mrc.CalcSerie();
            const mr = getMacroRugoRef();
            mr.prms.L.singleValue = mrc.children[0].prms.L.v;
            mr.CalcSerie();
            compareTwoResults(mrc.children[0].result, mr.result, 3, ["ZF2"]);
        });
        it("variating Z1, children Y should not always be 0.6", () => {
            const mrc = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            mrc.addDefaultChild();
            mrc.children[0].prms.L.singleValue = 0.5;
            mrc.prms.Z1.setValues(10, 15, 5);
            mrc.CalcSerie();
            const childYs = mrc.children[0].prms.Y.getInferredValuesList();
            expect(childYs).toEqual([0, 2.5]);
        });
    });
    describe("Default inclined but flat apron", () => {
        beforeAll(() => {
            BR = 0;
        });
        beforeEach(() => {
            BR += 1;
        });
        for (let i = 1; i <= 10; i += 1) {
            it(`B = ${i} should return same result as Macrorugo`, () => {
                const mrc = Session.getInstance().createNub(
                    new Props({ calcType: CalculatorType.MacroRugoCompound })
                ) as MacrorugoCompound;
                mrc.setPropValue("inclinedApron", MRCInclination.INCLINED);
                mrc.prms.BR.singleValue = BR;
                const mr = getMacroRugoRef();
                const res = mrc.CalcSerie();
                expect(res.vCalc)
                    .toBeCloseTo(mr.result.vCalc * mrc.prms.BR.currentValue / mr.prms.B.currentValue, 2);
                expect(res.values.LIncl).toBe(0);
                const ax = mrc.prms.PBD.v / Math.sqrt(mrc.prms.C.v);
                let B: number = 0;
                for (const child of mrc.children) {
                    expect(child.result.values.xCenter)
                        .toBeCloseTo(B + child.prms.B.v / 2);
                    B += child.prms.B.v;
                    expect(child.prms.B.v).toBeGreaterThanOrEqual(ax / 4);
                }
                expect(B).toBeCloseTo(mrc.prms.BR.currentValue, 2);
            });
        }
    });
    describe("Inclined apron", () => {
        it("lateral inclination should be 0.5", () => {
            const mrc = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            mrc.setPropValue("inclinedApron", MRCInclination.INCLINED);
            // width = 4m
            mrc.prms.BR.singleValue = 5;
            // delta Z = 2m
            mrc.prms.ZRL.singleValue = 14.5;
            mrc.prms.ZRR.singleValue = 12.5;
            const res = mrc.CalcSerie();
            expect(res.values.LIncl).toEqual(0.4);
        });
    });
    describe("Multiple apron", () => {
        beforeAll(() => {
            mrc2 = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.MacroRugoCompound })
            ) as MacrorugoCompound;
            for (let i = 0; i < 3; i++) {
                mrc2.addChild(
                    new MacroRugo(
                        new MacrorugoParams(0, 100, 100, 0.5, 1, 10, 1, 1, 1, 10, 0.5)
                    )
                );
                mrc2.children[i].prms.ZF1.singleValue = 12.5 - 0.21 * i;
                mrc2.children[i].prms.B.singleValue = 1 + i;
            }
            mrc2.CalcSerie();
        });
        for (let i = 0; i < 3; i++) {
            it(`Apron #${i} should return same parameters as Macrorugo`, () => {
                const mr = getMacroRugoRef(i);
                for (const p of mrc2.children[i].parameterIterator) {
                    if (p.symbol !== "Q") {
                        expect(p.v).toBeCloseTo(mr.getParameter(p.symbol).v, 3);
                    }
                }
            });
            it(`Apron #${i} should return same result as Macrorugo`, () => {
                const mr = getMacroRugoRef(i);
                compareTwoResults(mrc2.children[i].result, mr.result, 3, ["ZF2"]);
            });
        }
    });

    describe("Ramp width tolerance", () => {
        it("tolerance should be 1cm", () => {
            const MRCprms = new MacrorugoCompoundParams(
                13.1,   // Z1
                12.5,   // ZRL
                12.5,   // ZRR
                60,      // B
                3,      // DH
                0.05,   // If
                0.01,   // RF (Ks)
                0.13,   // C (concentration de bloc)
                0.4,    // D (diamètre de bloc)
                0.4,    // H (hauteur de bloc)
                1     // Cd0
            );
            const mrc = new MacrorugoCompound(MRCprms);

            const MRprms = new MacrorugoParams( // B 1  
                12.5,   // ZF1
                6,      // L
                1,      // B
                0.05,   // If
                1.57,    // Q
                0.6,    // Y
                0.01,   // Ks
                0.13,   // C
                0.4,    // D      
                0.4,    // H
                1     // Cd0
            );
            mrc.addChild(new MacroRugo(MRprms));

            // ramp not wide enough : warning on tolerance (min width=1.109 m)

            MRprms.B.singleValue = 1.099;
            mrc.CalcSerie();
            let res = mrc.children[0].result; // warnings are in the child nub
            expect(res.log.messages.length).toEqual(1);
            expect(res.log.messages[0].code).toEqual(MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);

            // ramp just wide enough for one block pattern : no warning (min width=1.109 m)

            MRprms.B.singleValue = 1.1;
            mrc.CalcSerie();
            res = mrc.children[0].result;
            expect(res.log.messages.length).toEqual(0);

            // ramp wide enough : warning on tolerance (B=2.2, closest rounded value=2.219, B-rounded > 0.01)

            MRprms.B.singleValue = 2.2;
            mrc.CalcSerie();
            res = mrc.children[0].result;
            expect(res.log.messages.length).toEqual(1);
            expect(res.log.messages[0].code).toEqual(MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);

            // ramp wide enough : no warning (B=2.21, closest rounded value=2.219, B-rounded < 0.01)

            MRprms.B.singleValue = 2.21;
            mrc.CalcSerie();
            res = mrc.children[0].result;
            expect(res.log.messages.length).toEqual(0);
        });
    });
});
