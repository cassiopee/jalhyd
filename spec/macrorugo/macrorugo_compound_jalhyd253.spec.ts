import { Session } from "../../src/session";
import { MacrorugoCompound } from "../../src/macrorugo/macrorugo_compound";
import { ParamValueMode } from "../../src/param/param-value-mode";

describe("Class MacroRugoCompound: ", () => {

    describe("jalhyd #253 no water after variated calculation − ", () => {

        it("case 1: no warnings", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-07-22T20:24:55.753Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"b3hmeH","props":{"calcType":"MacroRugoCompound","inclinedApron":"NOT_INCLINED"},"meta":{"title":"Rhins-PontdesAllees"},"children":[{"uid":"bWxoNW","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":276.89},{"symbol":"B","mode":"SINGLE","value":3}]},{"uid":"YTlnbX","props":{"calcType":"MacroRugo"},"children":[],"parameters":[{"symbol":"ZF1","mode":"SINGLE","value":277.09},{"symbol":"B","mode":"SINGLE","value":3}]}],"parameters":[{"symbol":"L","mode":"SINGLE","value":1},{"symbol":"If","mode":"SINGLE","value":0.036},{"symbol":"Ks","mode":"SINGLE","value":0.15},{"symbol":"C","mode":"SINGLE","value":0.114},{"symbol":"PBD","mode":"SINGLE","value":0.34},{"symbol":"PBH","mode":"SINGLE","value":0.41},{"symbol":"Cd0","mode":"SINGLE","value":2},{"symbol":"Z1","mode":"MINMAX","min":276.75,"max":277.8,"step":0.05,"extensionStrategy":0},{"symbol":"DH","mode":"SINGLE","value":1}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const mrc = Session.getInstance().findNubByUid("b3hmeH") as MacrorugoCompound;

            // variated calculation : ok
            const res1 = mrc.CalcSerie();
            expect(res1.ok).toBe(true);
            expect(res1.hasMultipleValues()).toBe(true, "1st calculation should be variated");
            expect(mrc.getChildren()[0].result.values.Q).toBeGreaterThan(0);

            // subsequent fixed calculation : fails
            mrc.prms.Z1.valueMode = ParamValueMode.SINGLE;
            mrc.prms.Z1.singleValue = 277;
            const res2 = mrc.CalcSerie();
            expect(res2.hasMultipleValues()).toBe(false, "2nd calculation should not be variated");
            expect(res2.ok).toBe(true);
            expect(mrc.getChildren()[0].result.values.Q).toBeGreaterThan(0);

        });

    });

});
