import { ConcentrationBlocs } from "../../src/macrorugo/concentration_blocs";
import { ConcentrationBlocsParams } from "../../src/macrorugo/concentration_blocs_params";
import { MessageCode } from "../../src/util/message";

let nub: ConcentrationBlocs;

describe("Class ConcentrationBlocs − ", () => {

    beforeEach(() => {
        nub = new ConcentrationBlocs(
            new ConcentrationBlocsParams(
                0.49, // Concentration de blocs
                10,   // Nombre de motifs
                5,    // Largeur de la passe
                0.35  // Diamètre des plots
            )
        );
    });

    it("calc C", () => {
        nub.prms.C.singleValue = 1;
        nub.prms.C.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(0.49, 2);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
    });

    it("calc C, round N", () => {
        nub.prms.C.singleValue = 1;
        nub.prms.N.singleValue = 9.89;
        nub.prms.C.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(0.49, 2);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
        expect(res.log.messages.length).toBe(1);
        expect(res.log.messages[0].code).toBe(MessageCode.WARNING_VALUE_ROUNDED_TO_INTEGER);
        expect(res.log.messages[0].extraVar.rounded).toBe(10);
        expect(res.log.messages[0].extraVar.symbol).toBe("N");
    });

    it("calc N", () => {
        nub.prms.N.singleValue = 666;
        nub.prms.N.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBe(10);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
        expect(res.values.R).toBe(0);
    });

    it("calc N with positive rest", () => {
        nub.prms.N.singleValue = 666;
        nub.prms.L.singleValue = 5.4;
        nub.prms.N.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBe(10);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
        expect(res.values.R).toBeCloseTo(0.4, 2);
        expect(res.values.NB).toBe(10);
        expect(res.values.AXB).toBeCloseTo(0.54, 3);
        expect(res.values.CB).toBeCloseTo(0.420, 3);
        expect(res.values.NH).toBe(11);
        expect(res.values.AXH).toBeCloseTo(0.491, 3);
        expect(res.values.CH).toBeCloseTo(0.508, 3);
    });

    it("calc N with R almost equal to ax", () => {
        nub.prms.N.singleValue = 666;
        nub.prms.N.setCalculated();
        nub.prms.C.singleValue = 0.085;
        nub.prms.L.singleValue = 4.8;
        nub.prms.D.singleValue = 0.35;
        const res = nub.CalcSerie();
        expect(res.vCalc).toBe(4);
        expect(res.values.ax).toBeCloseTo(1.2, 2);
        expect(res.values.R).toBeCloseTo(-0.002, 3);
        expect(res.values.AXH).toBeUndefined();
    });

    it("calc L", () => {
        nub.prms.L.singleValue = 666;
        nub.prms.L.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(5, 2);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
    });

    it("calc D", () => {
        nub.prms.D.singleValue = 2;
        nub.prms.D.setCalculated();
        const res = nub.CalcSerie();
        expect(res.vCalc).toBeCloseTo(0.35, 2);
        expect(res.values.ax).toBeCloseTo(0.5, 2);
    });
});
