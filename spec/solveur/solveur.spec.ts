import { Bief } from "../../src/open-channel/bief";
import { PabChute } from "../../src/pab/pab_chute";
import { PabChuteParams } from "../../src/pab/pab_chute_params";
import { PabNombre } from "../../src/pab/pab_nombre";
import { PabNombreParams } from "../../src/pab/pab_nombre_params";
import { PabPuissance } from "../../src/pab/pab_puissance";
import { PabPuissanceParams } from "../../src/pab/pab_puissance_params";
import { Session } from "../../src/session";
import { Solveur } from "../../src/solveur/solveur";
import { SolveurParams } from "../../src/solveur/solveur_params";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { MessageCode } from "../../src/util/message";

describe("solveur multi-modules", () => {

    describe("", () => {

        it("test 1: PAB Chute <= PAB Nombre <= PAB Puissance", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(324.907), false);
            s.nubToCalculate = pp;
            s.searchedParameter = pc.prms.Z1;
            const res = s.CalcSerie();
            expect(res.vCalc).toBeCloseTo(2.156, 3);
        });

        it("test 2: Nubs chain with multiple values should not be solvable", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            pc.prms.Z2.setValues(1, 10, 1);
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(324.907), false);
            expect(() => {
                s.nubToCalculate = pp;
            }).toThrowError("Solveur.update(): Nub to calculate must not have multiple values");
        });

        it("test 3: Nubs outside of a chain should not be solvable", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            pc.prms.Z2.setValues(1, 10, 1);
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            // solveur
            const s = new Solveur(new SolveurParams(324.907), false);
            s.searchedParameter = pc.prms.Z1;
            expect(() => {
                s.nubToCalculate = pp;
            }).toThrowError("Solveur.update(): Nub to calculate is not linked to result of searchedParameter parent Nub");
        });

        it("test 4: PAB Chute <= PAB Nombre <= PAB Puissance, redéfinition des paramètres", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(123), false);
            s.nubToCalculate = pp;
            s.searchedParameter = pc.prms.Z2;
            // defining values a-posteriori
            s.searchedParameter = pc.prms.Z1;
            s.prms.Ytarget.singleValue = 324.907;
            s.prms.Xinit.singleValue = 1.9;
            const res = s.CalcSerie();
            expect(res.vCalc).toBeCloseTo(2.156, 3);
        });

        it("test 5: PAB Chute <= PAB Nombre <= PAB Puissance, variating Y", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(), false);
            s.nubToCalculate = pp;
            s.searchedParameter = pc.prms.Z1;
            s.prms.Ytarget.setValues(100, 500, 50);
            const res = s.CalcSerie();
            const expectedValues = [ 1.01, 1.265, 1.519, 1.774, 2.029, 2.284, 2.539, 2.794, 3.048 ];
            expect(res.resultElements.length).toBe(9);
            for (let i = 0; i < res.resultElements.length; i++) {
                expect(res.resultElements[i].vCalc).toBeCloseTo(expectedValues[i], 3);
            }
        });

        it("test 6: target nubs / searched params lists", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // check lists
            const targetNubs = Session.getInstance().getDownstreamNubs();
            expect(targetNubs.length).toBe(2); // pn, pp
            const searchedParamsPp = Solveur.getDependingNubsSearchableParams(pp);
            expect(searchedParamsPp.length).toBe(3); // pc.Z1, pc.Z2, pn.N
            expect(searchedParamsPp.map((p) => p.symbol)).toEqual([ "N", "Z1", "Z2" ]);
            const searchedParamsPn = Solveur.getDependingNubsSearchableParams(pn);
            expect(searchedParamsPn.length).toBe(2); // pc.Z1, pc.Z2
            expect(searchedParamsPn.map((p) => p.symbol)).toEqual([ "Z1", "Z2" ]);
        });

        it("test 7: suppression des extraResult", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn ]);
            pn.prms.DHT.defineReference(pc, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(12.5), false);
            s.nubToCalculate = pn;
            s.searchedParameter = pc.prms.Z1;
            const res = s.CalcSerie();
            expect(res.vCalc).toBeCloseTo(125.5, 1);
            expect(Object.keys(res.resultElement.values).length).toBe(1);
            expect(res.resultElement.vCalcSymbol).toBe("X");
        });

        it("test 8: vCalc et vCalcSymbol du Nub calculé", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn ]);
            pn.prms.DHT.defineReference(pc, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(12.5), false);
            s.nubToCalculate = pn;
            s.searchedParameter = pc.prms.Z1;
            const res = s.CalcSerie();
            // résultat du solveur
            expect(res.resultElement.vCalcSymbol).toBe("X");
            expect(res.resultElement.vCalc).toBeCloseTo(125.5, 1);
            // résultat du Nub calculé (Y)
            expect(pn.result.resultElement.vCalcSymbol).toBe("DH");
            expect(pn.result.resultElement.vCalc).toBeCloseTo(12.5, 1);
        });

        it("test 9: remonter les logs en cas d'erreur dans la chaîne de calcul", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(666, 10, 666));
            pn.calculatedParam = pn.prms.DH;
            const pp = new PabPuissance(new PabPuissanceParams(666, 0.1, 0.5, 666));
            pp.calculatedParam = pp.prms.PV;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn, pp ]);
            pn.prms.DHT.defineReference(pc, "DH");
            pp.prms.DH.defineReference(pn, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(1), false);
            s.nubToCalculate = pp;
            s.searchedParameter = pc.prms.Z1;
            const res = s.CalcSerie();
            expect(res.globalLog.messages.length).toBe(1);
            expect(res.globalLog.messages[0].code).toBe(MessageCode.ERROR_IN_CALC_CHAIN);
        });

        it("test 10: Y avec Nubs enfants, les résultats des enfants ne devraient pas être visibles", () => {
            // tslint:disable-next-line:max-line-length
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-10-23T08:12:55.293Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MHQxN3","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages"},"children":[{"uid":"cW1pZD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":393.99},{"symbol":"L","mode":"SINGLE","value":1.84},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.5},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":393.99}]},{"uid":"Mnk0Mj","props":{"regime":"Fluvial","calcType":"Bief"},"meta":{"title":"Bief"},"children":[{"uid":"ZWFod3","props":{"regime":"Fluvial","calcType":"Section","nodeType":"SectionRectangle"},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"LINK","targetNub":"MHQxN3","targetParam":"Q"},{"symbol":"YB","mode":"SINGLE","value":1},{"symbol":"Y","mode":"SINGLE","value":0.8},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.3}]}],"parameters":[{"symbol":"Long","mode":"SINGLE","value":30},{"symbol":"Dx","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"LINK","targetNub":"MHQxN3","targetParam":"Z1"},{"symbol":"ZF1","mode":"SINGLE","value":393.99},{"symbol":"ZF2","mode":"SINGLE","value":394.1}]},{"uid":"bmlreT","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages 1"},"children":[{"uid":"Zmc5dm","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"KIVI"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":393.99},{"symbol":"L","mode":"SINGLE","value":2.8},{"symbol":"alpha","mode":"SINGLE","value":0.4},{"symbol":"beta","mode":"SINGLE","value":0.001},{"symbol":"ZRAM","mode":"SINGLE","value":393.99}]}],"parameters":[{"symbol":"Q","mode":"LINK","targetNub":"MHQxN3","targetParam":"Q"},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"LINK","targetNub":"Mnk0Mj","targetParam":"Z1"}]}]}`;
            // load session with ParallelStructures
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const ps = Session.getInstance().findNubByUid("MHQxN3") as ParallelStructure;
            const ps1 = Session.getInstance().findNubByUid("bmlreT") as ParallelStructure;
            // solveur
            const s = new Solveur(new SolveurParams(100.25), false);
            s.nubToCalculate = ps1;
            s.searchedParameter = ps.prms.Q;
            s.CalcSerie();
            expect(Object.keys(s.result.resultElement.values).length).toBe(1);
            expect(s.result.sourceNub).toBe(s);
            expect(s.getChildren().length).toBe(0);
        });

        it("test 11: bug jalhyd#159", () => {
            // tslint:disable-next-line:max-line-length
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-10-25T12:31:39.494Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MHQxN3","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages"},"children":[{"uid":"cW1pZD","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":393.99},{"symbol":"L","mode":"SINGLE","value":1.84},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":1.143420537740305},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"SINGLE","value":393.99}]},{"uid":"Mnk0Mj","props":{"regime":"Fluvial","calcType":"Bief"},"meta":{"title":"Bief"},"children":[{"uid":"ZWFod3","props":{"regime":"Fluvial","calcType":"Section","nodeType":"SectionRectangle"},"children":[],"parameters":[{"symbol":"Ks","mode":"SINGLE","value":40},{"symbol":"Q","mode":"LINK","targetNub":"MHQxN3","targetParam":"Q"},{"symbol":"YB","mode":"SINGLE","value":1},{"symbol":"Y","mode":"SINGLE","value":0.8},{"symbol":"LargeurBerge","mode":"SINGLE","value":2.3}]}],"parameters":[{"symbol":"Long","mode":"SINGLE","value":30},{"symbol":"Dx","mode":"SINGLE","value":1},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"LINK","targetNub":"MHQxN3","targetParam":"Z1"},{"symbol":"ZF1","mode":"SINGLE","value":393.99},{"symbol":"ZF2","mode":"SINGLE","value":394.1}]},{"uid":"bmlreT","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages 1"},"children":[{"uid":"cnN3Y2","props":{"calcType":"Structure","structureType":"SeuilRectangulaire","loiDebit":"WeirVillemonte"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":393.99},{"symbol":"L","mode":"SINGLE","value":2.8},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"LINK","targetNub":"MHQxN3","targetParam":"Q"},{"symbol":"Z1","mode":"CALCUL"},{"symbol":"Z2","mode":"LINK","targetNub":"Mnk0Mj","targetParam":"Z1"}]},{"uid":"ZDM5OT","props":{"calcType":"Solveur","nubToCalculate":"bmlreT","searchedParameter":"MHQxN3/Q"},"meta":{"title":"Solveur"},"children":[],"parameters":[{"symbol":"Xinit","mode":"SINGLE","value":0.5},{"symbol":"Ytarget","mode":"SINGLE","value":394.66}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const s = Session.getInstance().findNubByUid("ZDM5OT") as Solveur;
            const ouvrages1 = Session.getInstance().findNubByUid("bmlreT") as ParallelStructure;
            const bief = Session.getInstance().findNubByUid("Mnk0Mj") as Bief;
            // searchable parameters
            const spBief = Solveur.getDependingNubsSearchableParams(bief);
            expect(spBief.length).toBe(5);
            const spOuvrages1 = Solveur.getDependingNubsSearchableParams(ouvrages1);
            expect(spOuvrages1.length).toBe(12);
        });

        it("test 12: extraresult on PAB Nombre", () => {
            // contexte
            const pn = new PabNombre(new PabNombreParams(6, 666, 0.62));
            pn.calculatedParam = pn.prms.N;
            Session.getInstance().clear();
            Session.getInstance().registerNub(pn);
            // solveur
            const s = new Solveur(new SolveurParams(0.321), false);
            s.nubToCalculate = pn;
            s.targettedResult = "DHR";
            s.searchedParameter = pn.prms.DHT;
            const res = s.CalcSerie();
            expect(res.vCalc).toBeCloseTo(5.901, 3);
        });

        it("test 13: extraresult on chained PAB Nombre", () => {
            // contexte
            const pc = new PabChute(new PabChuteParams(2, 0.5, 666));
            pc.calculatedParam = pc.prms.DH;
            const pn = new PabNombre(new PabNombreParams(6, 666, 0.6));
            pn.calculatedParam = pn.prms.N;
            Session.getInstance().clear();
            Session.getInstance().registerNubs([ pc, pn ]);
            pn.prms.DHT.defineReference(pc, "DH");
            // solveur
            const s = new Solveur(new SolveurParams(0.321), false);
            s.nubToCalculate = pn;
            s.targettedResult = "DHR";
            s.searchedParameter = pc.prms.Z2;
            const res = s.CalcSerie();
            expect(res.vCalc).toBeCloseTo(0.479, 3);
        });

        it("test 14: unavailable extraresult on PAB Nombre", () => {
            // contexte
            const pn = new PabNombre(new PabNombreParams(6, 666, 0.62));
            pn.calculatedParam = pn.prms.N;
            Session.getInstance().clear();
            Session.getInstance().registerNub(pn);
            // solveur
            const s = new Solveur(new SolveurParams(0.321), false);
            s.nubToCalculate = pn;
            expect(() => {
                s.targettedResult = "DHZ";
            }).toThrowError();
        });

        it("jalhyd#262 - this.prms.X is undefined in Solveur.CalcSerie()", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-01T12:31:38.756Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MHdxaz","props":{"calcType":"Jet"},"meta":{"title":"Jet"},"children":[],"parameters":[{"symbol":"V0","mode":"SINGLE","value":5},{"symbol":"S","mode":"SINGLE","value":0.03},{"symbol":"ZJ","mode":"SINGLE","value":30},{"symbol":"ZW","mode":"SINGLE","value":29.2},{"symbol":"ZF","mode":"LINK","targetNub":"MGJoMT","targetParam":"Z2"},{"symbol":"D","mode":"CALCUL"}]},{"uid":"MGJoMT","props":{"calcType":"PabChute"},"meta":{"title":"PAB : chute"},"children":[],"parameters":[{"symbol":"Z1","mode":"SINGLE","value":30},{"symbol":"Z2","mode":"CALCUL"},{"symbol":"DH","mode":"SINGLE","value":1.5}]},{"uid":"d3F5NW","props":{"calcType":"Solveur","nubToCalculate":"MHdxaz","searchedParameter":"MGJoMT/Z1","targettedResult":"YH"},"meta":{"title":"Solveur"},"children":[],"parameters":[{"symbol":"Xinit","mode":"SINGLE","value":30},{"symbol":"Ytarget","mode":"SINGLE","value":3}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const solv = Session.getInstance().findNubByUid("d3F5NW") as Solveur;
            expect(() => {
                solv.CalcSerie();
            }).not.toThrow();
        });

    });

    describe("targetting extra results −", () => {

        it("find upstream Nubs", () => {
            const pn = new PabNombre(new PabNombreParams(6, 666, 0.62));
            pn.calculatedParam = pn.prms.N;
            Session.getInstance().clear();
            Session.getInstance().registerNub(pn);
            const un = Session.getInstance().getUpstreamNubsHavingExtraResults();
            expect(un.length).toBe(1);
        });

        it("find searchable params", () => {
            const pn = new PabNombre(new PabNombreParams(6, 666, 0.62));
            pn.calculatedParam = pn.prms.N;
            Session.getInstance().clear();
            Session.getInstance().registerNub(pn);
            const dnsp = Solveur.getDependingNubsSearchableParams(pn);
            expect(dnsp.length).toBe(0);
            const dnsp2 = Solveur.getDependingNubsSearchableParams(pn, true);
            expect(dnsp2.length).toBe(2); // DHT, DH
        });

        it("load session", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-02-10T16:05:08.648Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"ZXAzeT","props":{"calcType":"PabNombre"},"meta":{"title":"PAB : nombre"},"children":[],"parameters":[{"symbol":"DHT","mode":"SINGLE","value":6},{"symbol":"N","mode":"CALCUL"},{"symbol":"DH","mode":"SINGLE","value":0.6}]},{"uid":"MjRxZD","props":{"calcType":"Solveur","nubToCalculate":"ZXAzeT","searchedParameter":"ZXAzeT/DH","targettedResult":"DHR"},"meta":{"title":"Solveur"},"children":[],"parameters":[{"symbol":"Xinit","mode":"SINGLE","value":0.6},{"symbol":"Ytarget","mode":"SINGLE","value":10}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const solv = Session.getInstance().findNubByUid("MjRxZD") as Solveur;
            const pn = Session.getInstance().findNubByUid("ZXAzeT") as PabNombre;
            expect(solv.nubToCalculate).toBe(pn);
            expect(solv.searchedParameter).toBe(pn.prms.DH);
            expect(solv.targettedResult).toBe("DHR");
        });

        it("load session with extraResult (jalhyd#263)", () => {
            const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-01T12:31:38.756Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"MHdxaz","props":{"calcType":"Jet"},"meta":{"title":"Jet"},"children":[],"parameters":[{"symbol":"V0","mode":"SINGLE","value":5},{"symbol":"S","mode":"SINGLE","value":0.03},{"symbol":"ZJ","mode":"SINGLE","value":30},{"symbol":"ZW","mode":"SINGLE","value":29.2},{"symbol":"ZF","mode":"LINK","targetNub":"MGJoMT","targetParam":"Z2"},{"symbol":"D","mode":"CALCUL"}]},{"uid":"MGJoMT","props":{"calcType":"PabChute"},"meta":{"title":"PAB : chute"},"children":[],"parameters":[{"symbol":"Z1","mode":"SINGLE","value":30},{"symbol":"Z2","mode":"CALCUL"},{"symbol":"DH","mode":"SINGLE","value":1.5}]},{"uid":"d3F5NW","props":{"calcType":"Solveur","nubToCalculate":"MHdxaz","searchedParameter":"MGJoMT/Z1","targettedResult":"YH"},"meta":{"title":"Solveur"},"children":[],"parameters":[{"symbol":"Xinit","mode":"SINGLE","value":30},{"symbol":"Ytarget","mode":"SINGLE","value":3}]}]}`;
            Session.getInstance().clear();
            Session.getInstance().unserialise(sess);
            const solv = Session.getInstance().findNubByUid("d3F5NW") as Solveur;
            const jet = Session.getInstance().findNubByUid("MHdxaz");
            const chute = Session.getInstance().findNubByUid("MGJoMT") as PabChute;
            expect(solv.nubToCalculate).toBe(jet);
            expect(solv.searchedParameter).toBe(chute.prms.Z1);
            expect(solv.targettedResult).toBe("YH");
        });

    });

    it("test 12: bug jalhyd#198", () => {
        // tslint:disable-next-line:max-line-length
        const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-02-19T15:58:34.204Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"dHczZm","props":{"calcType":"PabDimensions"},"meta":{"title":"PAB : dimensions"},"children":[],"parameters":[{"symbol":"L","mode":"MINMAX","min":1,"max":4,"step":0.15,"extensionStrategy":0},{"symbol":"W","mode":"SINGLE","value":0.6397826086956476},{"symbol":"Y","mode":"SINGLE","value":0.5},{"symbol":"V","mode":"CALCUL"}]},{"uid":"NWtydz","props":{"calcType":"PabPuissance"},"meta":{"title":"PAB : puissance"},"children":[],"parameters":[{"symbol":"DH","mode":"SINGLE","value":0.3},{"symbol":"Q","mode":"SINGLE","value":0.1},{"symbol":"V","mode":"LINK","targetNub":"dHczZm","targetParam":"V"},{"symbol":"PV","mode":"CALCUL"}]},{"uid":"Ympkdj","props":{"calcType":"Solveur","nubToCalculate":"NWtydz","searchedParameter":"dHczZm/W"},"meta":{"title":"Solveur"},"children":[],"parameters":[{"symbol":"Xinit","mode":"SINGLE","value":1},{"symbol":"Ytarget","mode":"SINGLE","value":230}]}]}`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const s = Session.getInstance().findNubByUid("Ympkdj") as Solveur;
        const res = s.CalcSerie();
        expect(res.vCalc).toBeUndefined();
        expect(res.log.messages.length).toBe(1);
        expect(res.log.messages[0].code).toBe(MessageCode.ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED);
    });

    it("test 12: bug jalhyd#202", () => {
        // tslint:disable-next-line:max-line-length
        const sess = `{ "header": { "source": "jalhyd", "format_version": "1.3", "created": "2020-03-10T10:41:12.161Z" }, "settings": { "precision": 1e-7, "maxIterations": 100, "displayPrecision": 3 }, "documentation": "\\nCette session correspond au premier cas d'étude présent dans *la méthodologie de calcul du débit du droit d'eau fondé en titre, AFB, Irstea, septembre 2017*.\\n\\nElle consiste à calculer le débit d'un moulin à partir de la cote de fonctionnement normale (fixée à 394,66m) et des différents organes hydrauliques constituant la prise d'eau du moulin (une vanne motrice, un canal d'amenée et une vanne à l'entrée du canal d'amenée).\\n\\nLe calcul s'effectue par itérations successives des opérations suivantes :\\n\\n- Choix d'un débit probable \\n- Calcul de la cote de l'eau amont de la vanne motrice\\n- Calcul de la cote de l'eau à l'amont du canal d'amenée\\n- Calcul de la cote de l'eau à l'amont de la vanne d'entrée\\n- Vérification de l'égalité de la cote obtenue avec la cote normale de fonctionnement\\n\\nLes différents paramètres utiles au calcul suivant sont liés à ceux du précédent. Il suffit de demander le calcul de la cote de l'eau à l'amont de la vanne d'entrée du canal pour que les calculs précédents s'effectuent automatiquement.\\n\\nCes itérations peuvent être faites manuellement par exemple en faisant varier le débit de la vanne motrice et d'observer pour quel débit la cote de l'eau à l'amont de la vanne d'entrée croise la cote normale de fonctionnement.\\n\\nCette session propose d'utiliser le \\"solveur multi-modules\\" qui se charge d'effectuer une dichotomie afin de trouver le débit permettant d'obtenir la cote normale de fonctionnement au niveau de la cote de l'eau de la vanne d'entrée du canal d'amenée.\\n", "session": [ { "uid": "MHQxN3", "props": { "calcType": "ParallelStructure" }, "meta": { "title": "Vanne motrice" }, "children": [ { "uid": "cW1pZD", "props": { "calcType": "Structure", "structureType": "SeuilRectangulaire", "loiDebit": "WeirFree" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 393.99 }, { "symbol": "L", "mode": "SINGLE", "value": 1.84 }, { "symbol": "CdWR", "mode": "SINGLE", "value": 0.4 } ] } ], "parameters": [ { "symbol": "Q", "mode": "SINGLE", "value": 1.143420537740305 }, { "symbol": "Z1", "mode": "CALCUL", "value": 102 }, { "symbol": "Z2", "mode": "SINGLE", "value": 393.99 } ] }, { "uid": "Mnk0Mj", "props": { "calcType": "Bief", "regime": "Fluvial" }, "meta": { "title": "Canal d'amenée" }, "children": [ { "uid": "ZWFod3", "props": { "regime": "Fluvial", "calcType": "Section", "nodeType": "SectionRectangle" }, "children": [], "parameters": [ { "symbol": "Ks", "mode": "SINGLE", "value": 40 }, { "symbol": "Q", "mode": "LINK", "targetNub": "MHQxN3", "targetParam": "Q" }, { "symbol": "YB", "mode": "SINGLE", "value": 1 }, { "symbol": "Y", "mode": "SINGLE", "value": 0.8 }, { "symbol": "LargeurBerge", "mode": "SINGLE", "value": 2.3 } ] } ], "parameters": [ { "symbol": "Long", "mode": "SINGLE", "value": 30 }, { "symbol": "Dx", "mode": "SINGLE", "value": 1 }, { "symbol": "Z1", "mode": "CALCUL" }, { "symbol": "Z2", "mode": "LINK", "targetNub": "MHQxN3", "targetParam": "Z1" }, { "symbol": "ZF1", "mode": "SINGLE", "value": 393.99 }, { "symbol": "ZF2", "mode": "SINGLE", "value": 394.1 } ] }, { "uid": "bmlreT", "props": { "calcType": "ParallelStructure" }, "meta": { "title": "Vanne à l'entrée du canal" }, "children": [ { "uid": "cnN3Y2", "props": { "calcType": "Structure", "structureType": "SeuilRectangulaire", "loiDebit": "WeirVillemonte" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 393.99 }, { "symbol": "L", "mode": "SINGLE", "value": 2.8 }, { "symbol": "CdWR", "mode": "SINGLE", "value": 0.4 } ] } ], "parameters": [ { "symbol": "Q", "mode": "LINK", "targetNub": "MHQxN3", "targetParam": "Q" }, { "symbol": "Z1", "mode": "CALCUL", "value": 102 }, { "symbol": "Z2", "mode": "LINK", "targetNub": "Mnk0Mj", "targetParam": "Z1" } ] }, { "uid": "ZDM5OT", "props": { "calcType": "Solveur", "nubToCalculate": "bmlreT", "searchedParameter": "MHQxN3/Q", "targettedResult": "" }, "meta": { "title": "Calcul du débit à partir de la cote de fonctionnement" }, "children": [], "parameters": [ { "symbol": "Xinit", "mode": "SINGLE", "value": 0.5 }, { "symbol": "Ytarget", "mode": "SINGLE", "value": 394.66 } ] } ] }`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const s = Session.getInstance().findNubByUid("ZDM5OT") as Solveur;
        const res = s.CalcSerie();
        expect(res.vCalc).toBeDefined();
        expect(res.log.messages.length).toBeGreaterThan(0);
        // Vanne à l'entrée du canal (Parallel structures)
        expect(res.log.messages[0].code).toBe(MessageCode.WARNING_NOTCH_SUBMERGENCE_GREATER_THAN_09);
        if (res.vCalc === undefined) {
            expect(res.log.messages.length).toBe(3);
            // Canal d'amenée (Cotes d'un bief)
            expect(res.log.messages[1].code).toBe(MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL);
            expect(res.log.messages[2].code).toBe(MessageCode.ERROR_BIEF_Z1_CALC_FAILED);
        }
    });
});
