import { Session } from "../../src/session";

const sessionJson = `
{
    "header": {
        "source": "jalhyd",
        "format_version": "1.3",
        "created": "2019-10-29T16:30:29.798Z"
    },
    "settings": {
        "precision": 1e-7,
        "maxIterations": 100,
        "displayPrecision": 3
    },
    "documentation": "",
    "session": [
        {
            "uid": "MHQxN3",
            "props": {
                "calcType": "ParallelStructure"
            },
            "meta": {
                "title": "Ouvrages"
            },
            "children": [
                {
                    "uid": "cW1pZD",
                    "props": {
                        "calcType": "Structure",
                        "structureType": "SeuilRectangulaire",
                        "loiDebit": "WeirFree"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "ZDV",
                            "mode": "SINGLE",
                            "value": 393.99
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 1.84
                        },
                        {
                            "symbol": "CdWR",
                            "mode": "SINGLE",
                            "value": 0.4
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "SINGLE",
                    "value": -2.1904999999999992
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z2",
                    "mode": "SINGLE",
                    "value": 393.99
                }
            ]
        },
        {
            "uid": "Mnk0Mj",
            "props": {
                "regime": "Fluvial",
                "calcType": "Bief"
            },
            "meta": {
                "title": "Bief"
            },
            "children": [
                {
                    "uid": "ZWFod3",
                    "props": {
                        "regime": "Fluvial",
                        "calcType": "Section",
                        "nodeType": "SectionRectangle"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "Ks",
                            "mode": "SINGLE",
                            "value": 40
                        },
                        {
                            "symbol": "Q",
                            "mode": "LINK",
                            "targetNub": "MHQxN3",
                            "targetParam": "Q"
                        },
                        {
                            "symbol": "YB",
                            "mode": "SINGLE",
                            "value": 1
                        },
                        {
                            "symbol": "Y",
                            "mode": "SINGLE",
                            "value": 0.8
                        },
                        {
                            "symbol": "LargeurBerge",
                            "mode": "SINGLE",
                            "value": 2.3
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Long",
                    "mode": "SINGLE",
                    "value": 30
                },
                {
                    "symbol": "Dx",
                    "mode": "SINGLE",
                    "value": 1
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z2",
                    "mode": "LINK",
                    "targetNub": "MHQxN3",
                    "targetParam": "Z1"
                },
                {
                    "symbol": "ZF1",
                    "mode": "SINGLE",
                    "value": 393.99
                },
                {
                    "symbol": "ZF2",
                    "mode": "SINGLE",
                    "value": 394.1
                }
            ]
        },
        {
            "uid": "bmlreT",
            "props": {
                "calcType": "ParallelStructure"
            },
            "meta": {
                "title": "Ouvrages 1"
            },
            "children": [
                {
                    "uid": "cnN3Y2",
                    "props": {
                        "calcType": "Structure",
                        "structureType": "SeuilRectangulaire",
                        "loiDebit": "WeirVillemonte"
                    },
                    "children": [],
                    "parameters": [
                        {
                            "symbol": "ZDV",
                            "mode": "SINGLE",
                            "value": 393.99
                        },
                        {
                            "symbol": "L",
                            "mode": "SINGLE",
                            "value": 2.8
                        },
                        {
                            "symbol": "CdWR",
                            "mode": "SINGLE",
                            "value": 0.4
                        }
                    ]
                }
            ],
            "parameters": [
                {
                    "symbol": "Q",
                    "mode": "LINK",
                    "targetNub": "MHQxN3",
                    "targetParam": "Q"
                },
                {
                    "symbol": "Z1",
                    "mode": "CALCUL"
                },
                {
                    "symbol": "Z2",
                    "mode": "LINK",
                    "targetNub": "Mnk0Mj",
                    "targetParam": "Z1"
                }
            ]
        },
        {
            "uid": "ZDM5OT",
            "props": {
                "calcType": "Solveur",
                "nubToCalculate": "bmlreT",
                "searchedParameter": "MHQxN3/Q"
            },
            "meta": {
                "title": "Solveur"
            },
            "children": [],
            "parameters": [
                {
                    "symbol": "Xinit",
                    "mode": "SINGLE",
                    "value": 6
                },
                {
                    "symbol": "Ytarget",
                    "mode": "SINGLE",
                    "value": 394.66
                }
            ]
        }
    ]
}
`;

describe("jalhyd#164", () => {
    it("CalcSerie() should return 1.143", () => {
        Session.getInstance().unserialise(sessionJson);
        // Session.getInstance().findNubByUid("ZDM5OT").DBG = true;
        const r = Session.getInstance().findNubByUid("ZDM5OT").CalcSerie();
        expect(r.vCalc).toBeCloseTo(1.143, 2)
    });
});

