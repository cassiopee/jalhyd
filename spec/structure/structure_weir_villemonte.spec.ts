import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureWeirVillemonte } from "../../src/structure/structure_weir_villemonte";
import { MessageCode } from "../../src/util/message";

function getStructTest(): StructureWeirVillemonte {
    return new StructureWeirVillemonte(new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0), false);
}

describe("Class StructureWeirVillemonte: ", () => {
    describe("Calcul avec h2/h1 > 0.7 (=0.8) : ", () => {
        it("le log devrait contenir 1 message", () => {
            const structTest = getStructTest();
            structTest.prms.Z1.singleValue = 110;
            structTest.prms.Z2.singleValue = 108;
            structTest.prms.ZDV.singleValue = 100;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_NOTCH_SUBMERGENCE_GREATER_THAN_07);
        });
    });
});
