import { Structure } from "../../src/structure/structure";
import { StructureParams } from "../../src/structure/structure_params";
import { Result } from "../../src/util/result";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";

export class StructureTest extends Structure {

    constructor(prms: StructureParams, dbg: boolean = false) {
        super(prms, dbg);
    }

    /**
     * Test of getFlowMode
     */
    public testGetFlowMode() {
        this.prms.update_h1h2();
        return this.getFlowMode();
    }

    /**
     * Test of getFlowRegime
     */
    public testGetFlowRegime() {
        this.prms.update_h1h2();
        return this.getFlowRegime();
    }

    public CalcQ(): Result {
        this.prms.update_h1h2();
        const data = this.getResultData();

        return new Result(this.prms.Z1.v - this.prms.Z2.v - this.prms.ZDV.v, this, data);
    }

}

/* Test structure with :
 *  - ZDV = 0
 *  - Z1 = 30
 *  - Z2 = 15
 *  - expected Q = 15
 */
export function CreateStructTest() {
    const structTestPrm: StructureParams = new StructureParams(1, 0, 30, 15);
    const st = new StructureTest(structTestPrm, false);
    st.prms.W.visible = true;
    return st;
}

export function createStruct2WithKids() {
    const ps = new ParallelStructure(
        new ParallelStructureParams(30, 30, 15), // Q = 30, Z1 = 30, Z2 = 15
        false // debug
    );
    /*
     * Tests avec deux structures test identiques
     */
    ps.addChild(CreateStructTest());
    ps.addChild(CreateStructTest());
    return ps;
}