import { ParallelStructure } from "../../src/structure/parallel_structure";
import { createStruct2WithKids } from "./structure_test";
import { Structure } from "../../src/structure/structure";
import { ParamValueMode } from "../../src/index";

describe("Ouvrages > paramètre calculé - ", () => {
    it("restauration du paramètre quand on supprime un ouvrage, cas 1", () => {
        // 2 ouvrages en //
        const ps: ParallelStructure = createStruct2WithKids();

        // on passe W en calcul sur le 2ème ouvrage
        const st2: Structure = ps.getChildren()[1] as Structure;
        st2.getParameter("W").valueMode = ParamValueMode.CALCUL;

        // Q (dans le parent) ne doit plus être en calcul
        expect(ps.getParameter("Q").valueMode).not.toBe(ParamValueMode.CALCUL);

        // suppression 2ème ouvrage
        ps.deleteChild(1);

        // Q (dans le parent) doit être en calcul
        expect(ps.getParameter("Q").valueMode).toBe(ParamValueMode.CALCUL);
    });

    it("restauration du paramètre quand on supprime un ouvrage, cas 2", () => {
        // 2 ouvrages en //
        const ps: ParallelStructure = createStruct2WithKids();

        // on passe W en calcul sur le 1er ouvrage
        const st1: Structure = ps.getChildren()[0] as Structure;
        st1.getParameter("W").valueMode = ParamValueMode.CALCUL;

        // Q (dans le parent) ne doit plus être en calcul
        expect(ps.getParameter("Q").valueMode).not.toBe(ParamValueMode.CALCUL);

        // suppression 1er ouvrage
        ps.deleteChild(0);

        // Q (dans le parent) doit être en calcul
        expect(ps.getParameter("Q").valueMode).toBe(ParamValueMode.CALCUL);
    });

    it("restauration du paramètre quand on supprime un ouvrage, cas 3", () => {
        // 2 ouvrages en //
        const ps: ParallelStructure = createStruct2WithKids();

        // on passe W en calcul sur le 1er ouvrage
        const st1: Structure = ps.getChildren()[0] as Structure;
        st1.getParameter("W").valueMode = ParamValueMode.CALCUL;

        // Q (dans le parent) ne doit plus être en calcul
        expect(ps.getParameter("Q").valueMode).not.toBe(ParamValueMode.CALCUL);

        // suppression 2ème ouvrage
        ps.deleteChild(1);

        // W doit toujours être en calcul sur le 1er ouvrage
        expect(st1.getParameter("W").valueMode).toBe(ParamValueMode.CALCUL);
    });

    it("restauration du paramètre quand on supprime un ouvrage, cas 4", () => {
        // 2 ouvrages en //
        const ps: ParallelStructure = createStruct2WithKids();

        // on passe W en calcul sur le 2ème ouvrage
        const st2: Structure = ps.getChildren()[1] as Structure;
        st2.getParameter("W").valueMode = ParamValueMode.CALCUL;

        // Q (dans le parent) ne doit plus être en calcul
        expect(ps.getParameter("Q").valueMode).not.toBe(ParamValueMode.CALCUL);

        // suppression 1er ouvrage
        ps.deleteChild(0);

        // W doit toujours être en calcul sur le 2ème ouvrage
        expect(st2.getParameter("W").valueMode).toBe(ParamValueMode.CALCUL);
    });
});
