import { MessageCode, StructureFlowMode, StructureFlowRegime } from "../../src/index";
import { StructureKivi } from "../../src/structure/structure_kivi";
import { StructureKiviParams } from "../../src/structure/structure_kivi_params";
import { testStructure } from "./functions";

const kiviParams: StructureKiviParams = new StructureKiviParams(
    8.516,      // Q
    101.5,      // ZDV
    103,        // Z1
    102,        // Z2
    2,          // L
    0.4,        // alpha
    0.001,      // béta
    100         // ZRAM : cote Radier Amont
);
const structTest: StructureKivi = new StructureKivi(kiviParams, false);
structTest.prms.ZDV.singleValue = 101;

describe("Class StructureKivi: ", () => {
    describe("Ecoulement noyé", () => {
        testStructure(structTest, StructureFlowMode.WEIR, StructureFlowRegime.SUBMERGED);
    });
    describe("Ecoulement dénoyé beta=0:", () => {
        beforeAll(() => {
            structTest.prms.beta.singleValue = 0;
            structTest.prms.Z2.singleValue = 101;
            structTest.prms.Q.singleValue = 10.024;
        });
        testStructure(structTest, StructureFlowMode.WEIR, StructureFlowRegime.FREE);
    });
    describe("Ecoulement dénoyé beta=0.001:", () => {
        beforeAll(() => {
            structTest.prms.beta.singleValue = 0.001;
            structTest.prms.Z2.singleValue = 101;
            structTest.prms.Q.singleValue = 10.074;
        });
        testStructure(structTest, StructureFlowMode.WEIR, StructureFlowRegime.FREE);
    });
    describe("Pelle trop faible:", () => {
        beforeAll(() => {
            structTest.prms.ZRAM.singleValue = 100.95;
            structTest.calculatedParam = structTest.prms.Q;
        });
        it("Calc(Q) should return 2 messages", () => {
            expect(structTest.CalcSerie().resultElement.log.messages.length).toBe(2);
            expect(
                structTest.CalcSerie().resultElement.log.messages[0].code
            ).toBe(MessageCode.WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE);
            expect(
                structTest.CalcSerie().resultElement.log.messages[1].code
            ).toBe(MessageCode.WARNING_STRUCTUREKIVI_HP_TROP_ELEVE);
        });
    });
    describe("Test calcul ZDV (#94):", () => {
        let structTestKivi2: StructureKivi;
        beforeAll(() => {
            const kiviParams2: StructureKiviParams = new StructureKiviParams(
                0.5,      // Q
                0,      // ZDV
                102,        // Z1
                101.5,        // Z2
                1,          // L
                0.4,        // alpha
                0.001,      // béta
                95.7         // ZRAM : cote Radier Amont
            );
            structTestKivi2 = new StructureKivi(kiviParams2, false);
        });
        it(`Calc(ZDV) should return 101.57`, () => {
            expect(structTestKivi2.Calc("ZDV").vCalc).toBeCloseTo(101.57, 2);
        });
    });
});
