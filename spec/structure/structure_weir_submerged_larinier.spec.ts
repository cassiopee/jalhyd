import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureWeirSubmergedLarinier } from "../../src/structure/structure_weir_submerged_larinier";
import { MessageCode } from "../../src/util/message";
import { itCalcQ } from "../structure/functions";
import { precDigits } from "../test_config";

function getStructTest(): StructureWeirSubmergedLarinier {
    return new StructureWeirSubmergedLarinier(new RectangularStructureParams(0, 101, 102, 101.5, 0.2, 0.65), false);
}

describe("Class StructureWeirSubmergedLarinier: ", () => {
    describe("Calc(Q): ", () => {
        const Z1: number[] = [102];
        const Q: number[] = [0.407];
        const mode: StructureFlowMode = StructureFlowMode.WEIR;
        const regime: StructureFlowRegime = StructureFlowRegime.SUBMERGED;
        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructTest(), Z1[i], Infinity, Q[i], mode, regime);
        }
    });
    describe("Calc(Z1): ", () => {
        it("Z1(Q=0.780) should be 75.301", () => {
            const testZ1 = new StructureWeirSubmergedLarinier(
                new RectangularStructureParams(0.780, 73.665, 75.301, 75.077, 0.35, 0.65),
                false);
            testZ1.prms.Z1.v = 100; // Test with initial condition far from solution
            expect(testZ1.Calc("Z1").vCalc).toBeCloseTo(75.301, precDigits);
        });
    });
    describe("Calcul avec h2/h1 > 0.9 (=0.95) : ", () => {
        it("le log devrait contenir 1 message", () => {
            const structTest = getStructTest();
            structTest.prms.Z1.singleValue = 110;
            structTest.prms.Z2.singleValue = 109.5;
            structTest.prms.ZDV.singleValue = 100;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09);
        });
    });
    describe("Calcul avec h2/h1 < 0.7 (=0.5) : ", () => {
        it("le log devrait contenir 1 message", () => {
            const structTest = getStructTest();
            structTest.prms.Z1.singleValue = 110;
            structTest.prms.Z2.singleValue = 105;
            structTest.prms.ZDV.singleValue = 100;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_SLOT_SUBMERGENCE_NOT_BETWEEN_07_AND_09);
        });
    });
    describe("Calcul avec h2/h1 < 0.5 (=0.4) : ", () => {
        it("le log devrait contenir au moins un message d'erreur", () => {
            const structTest = getStructTest();
            structTest.prms.Z1.singleValue = 110;
            structTest.prms.Z2.singleValue = 104;
            structTest.prms.ZDV.singleValue = 100;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(2);
            const ok = res.log.messages[0].code === MessageCode.ERROR_STRUCTURE_SUBMERGENCE_LOWER_THAN || res.log.messages[1].code === MessageCode.ERROR_STRUCTURE_SUBMERGENCE_LOWER_THAN;
            expect(ok).toBe(true);
        });
    });
});
