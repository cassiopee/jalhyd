import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureTriangularWeir } from "../../src/structure/structure_triangular_weir";
import { TriangularStructureParams } from "../../src/structure/structure_triangular_weir_params";
import { itCalcQ } from "./functions";

function createStruct() {
    return new StructureTriangularWeir(new TriangularStructureParams(0, 100.1, 100.1, 100.1, 45, 1.36), false);
}

describe("Class StructureTriangularWeir: ", () => {

    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const structTest = createStruct();
        const h1: number[] = [100.1, 100.2, 100.3, 100.4, 100.5, 100.6, 100.7, 100.8, 100.9, 101, 102];
        const Q: number[] = [0., 0.004, 0.024, 0.067, 0.138, 0.240, 0.379, 0.558, 0.778, 1.045, 6.767];
        const mode: StructureFlowMode = StructureFlowMode.WEIR;
        const regime: StructureFlowRegime = StructureFlowRegime.FREE;
        itCalcQ(structTest, h1[0], Infinity, Q[0], StructureFlowMode.NULL, StructureFlowRegime.NULL);
        for (let i = 1; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], Infinity, Q[i], mode, regime);
        }
    });

    it("Q nul", () => {
        const structTest = createStruct();
        // si ZDV >= Z1, débit nul
        structTest.calculatedParam = structTest.prms.Q;
        const res = structTest.CalcSerie();
        expect(res.resultElement.vCalc).toBeDefined();
        expect(res.resultElement.values.ENUM_StructureFlowMode).toBeDefined();
        expect(res.resultElement.values.ENUM_StructureFlowRegime).toBeDefined();
        expect(res.resultElement.values.ENUM_StructureJetType).toBeDefined();
    });
});
