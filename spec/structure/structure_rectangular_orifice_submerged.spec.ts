import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureRectangularOrificeSubmerged } from "../../src/structure/structure_rectangular_orifice_submerged";
import { Result } from "../../src/util/result";
import { itCalcQ } from "./functions";

const structPrm: RectangularStructureParams = new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0);
const structTest: StructureRectangularOrificeSubmerged = new StructureRectangularOrificeSubmerged(structPrm, false);

describe("Class StructureRectangularOrificeSubmerged: ", () => {
    describe("Calcul Q avec W croissant: ", () => {
        const W: number[] = [
            0.000000, 0.100000, 0.200000, 0.300000, 0.400000, 0.500000, 0.600000,
            0.700000, 0.800000, 0.900000, 1.000000, 1.100000, 1.200000, 1.300000];
        const h1: number = 1.200000;
        const Q: number[] = [0.000000, 0.237709, 0.475418, 0.713127, 0.950836, 1.188545,
            1.426254, 1.663963, 1.901673, 2.139382, 2.377091, 2.614800, 2.852509, 2.852509];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1, W[i], Q[i]);
        }
    });
    describe("Calcul Q en charge avec h1 croissant: ", () => {
        const W: number = 0.8;
        const h1: number[] = [1.050000, 1.300000, 1.500000];
        const Q: number[] = [0.950836, 2.329064, 3.006808];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.SUBMERGED, StructureFlowRegime.SUBMERGED];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const W: number = Infinity;
        const h1: number[] = [1.100000, 1.500000];
        const Q: number[] = [1.848943, 5.637766];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.SUBMERGED];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
});
