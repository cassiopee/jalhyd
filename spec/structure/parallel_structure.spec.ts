import { CalculatorType } from "../../src/compute-node";
import { Session } from "../../src/index";
import { Props } from "../../src/props";
import { SessionSettings } from "../../src/session_settings";
import { CreateStructure } from "../../src/structure/factory_structure";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { ParallelStructureParams } from "../../src/structure/parallel_structure_params";
import { RectangularStructure } from "../../src/structure/rectangular_structure";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureKivi } from "../../src/structure/structure_kivi";
import { StructureKiviParams } from "../../src/structure/structure_kivi_params";
import { LoiDebit } from "../../src/structure/structure_props";
import { StructureRectangularOrificeFree } from "../../src/structure/structure_rectangular_orifice_free";
import { MessageCode } from "../../src/util/message";
import { Result } from "../../src/util/result";
import { precDigits } from "../test_config";
import { checkResult } from "../test_func";
import { CreateParalleleStructureTest, testParallelStructures } from "./functions";
import { createStruct2WithKids } from "./structure_test";

function createEnv() {
    return createStruct2WithKids();
}

let pstruct: ParallelStructure;

function getVarCalc(pstructLoc: ParallelStructure, structIndex: number, sVarCalc: string): any {
    if (structIndex !== null) {
        return {
            uid: pstructLoc.structures[structIndex].uid,
            symbol: sVarCalc
        };
    } else {
        return sVarCalc;
    }
}

/**
 * Test sur ParallelStructure
 * @param sVarCalc Variable à calculer
 * @param rVcalc Valeur de référence à retrouver
 * @param Q Débit de chacune des structures (pour structures identiques uniquement)
 */
function itParallelStructure(structIndex: number, sVarCalc: string, rVcalc: number, Q?: number) {
    it(`${sVarCalc} should be ${rVcalc}`, () => {
        const VC1: any = getVarCalc(pstruct, structIndex, sVarCalc);
        checkResult(pstruct.Calc(VC1), rVcalc);
    });
    if (Q !== undefined) {
        const pstructLocal = createEnv();
        for (let i = 0; i < pstructLocal.structures.length; i++) {
            it(`Calc(${JSON.stringify(sVarCalc)}) ouvrage[${i}].Q should be ${Q}`, () => {
                const VC2: any = getVarCalc(pstruct, structIndex, sVarCalc);
                pstruct.Calc(VC2);
                const val = pstruct.structures[i].result.resultElement.values.Q;
                expect(val).toBeCloseTo(Q, Math.max(0, precDigits - 1));
            });
            it(`Calc(${JSON.stringify(sVarCalc)}) ExtraResult[ouvrage[${i}].Q_Mode] should be 0`, () => {
                const VC3 = getVarCalc(pstruct, structIndex, sVarCalc);
                pstruct.Calc(VC3);
                const val = pstruct.structures[i].result.resultElement.values.ENUM_StructureFlowMode;
                expect(val).toEqual(0);
            });
            it(`Calc(${JSON.stringify(sVarCalc)}) ExtraResult[ouvrage[${i}].Q_Regime] should be 0`, () => {
                const VC4 = getVarCalc(pstruct, structIndex, sVarCalc);
                pstruct.Calc(VC4);
                const val = pstruct.structures[i].result.resultElement.values.ENUM_StructureFlowRegime;
                expect(val).toEqual(0);
            });
        }
    }
}

describe("Class ParallelStructure: ", () => {

    beforeAll(() => {
        // restore default precision to avoid side-effects from other tests
        SessionSettings.precision = 1e-7;
    });

    describe("Calc()", () => {
        beforeEach(() => {
            pstruct = createEnv();
        });
        it("should return 1 result", () => {
            const p1 = createEnv();
            const res: Result = p1.Calc("Q");
            expect(p1.Calc("Q").resultElements.length).toEqual(1);
        });
        itParallelStructure(null, "Q", 30, 15);
        itParallelStructure(null, "Z1", 30, 15);
        itParallelStructure(null, "Z2", 15, 15);
        itParallelStructure(0, "ZDV", 0, 15);
        itParallelStructure(1, "ZDV", 0, 15);
        it("shoud return an error Q too high", () => {
            pstruct.prms.Q.v = 14;
            const res: Result = pstruct.Calc({
                uid: pstruct.structures[1].uid,
                symbol: "ZDV"
            });
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_STRUCTURE_Q_TROP_ELEVE);
        });
    });
    describe("testParallelStructures", () => {
        /*
        * Tests avec toutes les équations et toutes les variables (cf. jalhyd#38)
        */
        const parallelStructure: ParallelStructure = new ParallelStructure(
            new ParallelStructureParams(0, 102, 101.5), // Q = 0, Z1 = 102, Z2 = 101.5
            false // debug
        );
        testParallelStructures(CreateParalleleStructureTest(parallelStructure));
    });

    describe("#94 Lois d'ouvrage: Erreur de calcul de ZDV", () => {
        it(`Calc(ZDV) should return 101.57`, () => {
            const ps3: ParallelStructure = new ParallelStructure(
                new ParallelStructureParams(0.5, 102, 101.5), // Q = 0.5, Z1 = 102, Z2 = 101.5
                false // debug
            );
            const structTestKivi3: StructureKivi = new StructureKivi(
                new StructureKiviParams(
                    0, 0, 0, 0,     // Q, ZDV, Z1, Z2
                    1,          // L
                    0.4,        // alpha
                    0.001,      // béta
                    95.7         // ZRAM : cote Radier Amont
                ),
                false
            );
            ps3.addChild(structTestKivi3);
            ps3.calculatedParam = ps3.structures[0].prms.ZDV;
            expect(ps3.CalcSerie().vCalc).toBeCloseTo(101.57, 2);
        });
    });
    describe("#116 Pas de calcul de Z2 sur loi dénoyée", () => {
        it("Calc(Z2) should return an error", () => {
            // Create Parallel Structure with default parameters
            const ps = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.ParallelStructure })
            ) as ParallelStructure;
            // Add Free flow weir
            ps.addChild(CreateStructure(LoiDebit.WeirFree));
            // Define Z2 to calculate
            ps.calculatedParam = ps.prms.Z2;

            expect(ps.CalcSerie().ok).toBeFalsy();
        });
    });
    describe("#38 Calcul de Z2 avec Q > Qmax", () => {
        it("Calc(Z2) should return error", () => {
            const ps = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.ParallelStructure })
            ) as ParallelStructure;
            // Add Free flow weir
            ps.addChild(CreateStructure(LoiDebit.GateCem88v));
            ps.prms.Q.singleValue = 4;
            ps.calculatedParam = ps.prms.Z2;
            ps.CalcSerie();
            const m = ps.result.resultElement.log.messages[0];
            expect(m.code).toEqual(MessageCode.ERROR_DICHO_TARGET_TOO_HIGH);
            expect(m.extraVar.variableSymbol).toEqual("Z2");
            expect(m.extraVar.targetSymbol).toEqual("Q");
            expect(m.extraVar.targetValue).toEqual(ps.prms.Q.currentValue);
        });
    });
    describe("#136 StructureGateCem88v.Calc(ZDV)", () => {
        it("Calc(ZDV) should return 100", () => {
            const ps = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.ParallelStructure })
            ) as ParallelStructure;
            ps.prms.Q.singleValue = 2.309;
            ps.addChild(CreateStructure(LoiDebit.GateCem88v));
            ps.structures[0].prms.ZDV.setCalculated();
            ps.structures[0].prms.ZDV.singleValue += 0.1;
            expect(ps.CalcSerie().vCalc).toBeCloseTo(100, 3);
        });
    });
    describe("Strucure logs should be propagated to parent Result - ", () => {
        it("example with StructureRectangularOrificeFree, h2 > 0, calculating Q", () => {
            const ps = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.ParallelStructure })
            ) as ParallelStructure;
            ps.prms.Q.singleValue = 2.309;
            ps.addChild(CreateStructure(LoiDebit.RectangularOrificeFree));
            ps.prms.Z1.singleValue = 105;
            ps.prms.Z2.singleValue = 102;
            ps.structures[0].prms.ZDV.singleValue = 101.5;
            const res = ps.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION);
        });
        it("example with StructureRectangularOrificeFree, h2 > 0, calculating something other than Q", () => {
            const ps = Session.getInstance().createNub(
                new Props({ calcType: CalculatorType.ParallelStructure })
            ) as ParallelStructure;
            ps.prms.Q.singleValue = 2.309;
            const c = CreateStructure(LoiDebit.RectangularOrificeFree) as StructureRectangularOrificeFree;
            ps.addChild(c);
            ps.prms.Z1.singleValue = 105;
            ps.prms.Z2.singleValue = 102;
            ps.structures[0].prms.ZDV.singleValue = 101.5;
            ps.calculatedParam = c.prms.L;
            const res = ps.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION);
        });
    });
});

describe("switching between LoiDebit should load a new default Cd", () => {

    it("test 1", () => {
        const ps = new ParallelStructure(new ParallelStructureParams(1, 102, 100));
        const s = CreateStructure(LoiDebit.WeirSubmergedLarinier, ps);
        expect((s.prms as RectangularStructureParams).CdWSL.singleValue).toBe(0.75);
        ps.addChild(s);
        expect((ps.getChildren()[0] as RectangularStructure).prms.CdWSL.singleValue).toBe(0.75);
        const s2 = CreateStructure(LoiDebit.WeirSubmerged, ps);
        expect((s2.prms as RectangularStructureParams).CdWS.singleValue).toBe(0.9);
        ps.replaceChildInplace(s, s2);
        expect((ps.getChildren()[0] as RectangularStructure).prms.CdWS.singleValue).toBe(0.9);
    });

    it("test 2", () => {
        const ps = new ParallelStructure(new ParallelStructureParams(1, 102, 100));
        const s = CreateStructure(LoiDebit.WeirSubmerged, ps);
        expect((s.prms as RectangularStructureParams).CdWS.singleValue).toBe(0.9);
        ps.addChild(s);
        expect((ps.getChildren()[0] as RectangularStructure).prms.CdWS.singleValue).toBe(0.9);
        const s2 = CreateStructure(LoiDebit.WeirSubmergedLarinier, ps);
        expect((s2.prms as RectangularStructureParams).CdWSL.singleValue).toBe(0.75);
        ps.replaceChildInplace(s, s2);
        expect((ps.getChildren()[0] as RectangularStructure).prms.CdWSL.singleValue).toBe(0.75);
    });

});
