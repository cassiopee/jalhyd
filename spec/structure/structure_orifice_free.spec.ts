import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureOrificeFree } from "../../src/structure/structure_orifice_free";
import { StructureOrificeFreeParams } from "../../src/structure/structure_orifice_free_params";
import { itCalcQ } from "../structure/functions";

const prms: StructureOrificeFreeParams = new StructureOrificeFreeParams(0, 102, 101.5, 0.7, 0.1, 101);
const test: StructureOrificeFree = new StructureOrificeFree(prms, false);

describe("Class StructureOrificeFree: ", () => {
    describe("Calc(Q): ", () => {
        const h1: number[] = [102];
        const Q: number[] = [0.310];
        const mode: StructureFlowMode = StructureFlowMode.ORIFICE;
        const regime: StructureFlowRegime = StructureFlowRegime.FREE;
        for (let i = 0; i < Q.length; i++) {
            itCalcQ(test, h1[i], Infinity, Q[i], mode, regime);
        }
    });
});
