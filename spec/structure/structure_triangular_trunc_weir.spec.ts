import { StructureFlowMode, StructureFlowRegime } from "../../src/internal_modules";
import { StructureTriangularWeir } from "../../src/internal_modules";
import { TriangularStructureParams } from "../../src/internal_modules";
import { StructureTriangularTruncWeirFree } from "../../src/internal_modules";
import { TriangularTruncStructureParams } from "../../src/internal_modules";
import { precDigits } from "../test_config";
import { itCalcQ } from "./functions";

let structTest: StructureTriangularTruncWeirFree = new StructureTriangularTruncWeirFree(
    new TriangularTruncStructureParams(0, 100.1, 100, 100, 0.9, 101, 1.36),
    false
);

describe("Class StructureTriangularTruncWeirFree: ", () => {
    beforeEach(() => {
        structTest = new StructureTriangularTruncWeirFree(
            new TriangularTruncStructureParams(0, 100.1, 100, 100, 0.9, 101, 1.36),
            false
        );
    })
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const Z1: number[] =
            [100.1, 100.2, 100.3, 100.4, 100.5, 100.6, 100.7, 100.8, 100.9, 101, 101.1, 101.5, 101.8, 102];
        const Q: number[] =
            [0., 0.004, 0.024, 0.067, 0.138, 0.240, 0.379, 0.558, 0.778, 1.045, 1.356, 2.914, 4.346, 5.407];
        const mode: StructureFlowMode = StructureFlowMode.WEIR;
        const regime: StructureFlowRegime = StructureFlowRegime.FREE;
        itCalcQ(structTest, Z1[0], Infinity, Q[0], StructureFlowMode.NULL, StructureFlowRegime.NULL);
        for (let i = 1; i < Q.length; i++) {
            itCalcQ(structTest, Z1[i], Infinity, Q[i], mode, regime);
        }
    });
    it("Q should be equal to triangular weir on triangular part", () => {
        const strTriang = new StructureTriangularWeir(
            new TriangularStructureParams(0, 100.1, 100.5, 100, 45, 1.36),
            false
        );
        structTest.prms.Z1.v = 100.5;
        const resFree = structTest.Calc("Q");
        expect(resFree.vCalc).toBeCloseTo(strTriang.Calc("Q").vCalc, precDigits);
        expect(resFree.values.ENUM_StructureFlowRegime).toBe(StructureFlowRegime.FREE);
        structTest.prms.Z2.v = 100.4;
        strTriang.prms.Z2.v = structTest.prms.Z2.v;
        const resSubm = structTest.Calc("Q");
        expect(resSubm.vCalc).toBeCloseTo(strTriang.Calc("Q").vCalc, precDigits);
        expect(resSubm.values.ENUM_StructureFlowRegime).toBe(StructureFlowRegime.PARTIAL);
    });
});
