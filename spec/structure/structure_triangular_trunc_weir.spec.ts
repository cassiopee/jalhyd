import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureTriangularTruncWeirFree } from "../../src/structure/structure_triangular_trunc_weir";
import { TriangularTruncStructureParams } from "../../src/structure/structure_triangular_trunc_weir_params";
import { itCalcQ } from "./functions";

const structPrm: TriangularTruncStructureParams =
    new TriangularTruncStructureParams(0, 100.1, 100, 100, 0.9, 101, 1.36);
const structTest: StructureTriangularTruncWeirFree = new StructureTriangularTruncWeirFree(structPrm, false);

describe("Class StructureTriangularTruncWeirFree: ", () => {
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const Z1: number[] =
            [100.1, 100.2, 100.3, 100.4, 100.5, 100.6, 100.7, 100.8, 100.9, 101, 101.1, 101.5, 101.8, 102];
        const Q: number[] =
            [0., 0.004, 0.024, 0.067, 0.138, 0.240, 0.379, 0.558, 0.778, 1.045, 1.356, 2.914, 4.346, 5.407];
        const mode: StructureFlowMode = StructureFlowMode.WEIR;
        const regime: StructureFlowRegime = StructureFlowRegime.FREE;
        itCalcQ(structTest, Z1[0], Infinity, Q[0], StructureFlowMode.NULL, StructureFlowRegime.NULL);
        for (let i = 1; i < Q.length; i++) {
            itCalcQ(structTest, Z1[i], Infinity, Q[i], mode, regime);
        }
    });
});
