import { Session } from "../../src/index";
import { checkResultConsistency } from "../test_func";

describe("Dever", () => {
    describe("jalhyd#123", () => {
        it("1-CalcSerie() should return consistent result with no NaN or Infinity", () => {
            // tslint:disable-next-line:max-line-length
            const sessionJson = `{ "header": { "source": "jalhyd", "format_version": "1.1", "created": "2019-08-02T13:08:02.502Z" }, "session": [ { "uid": "ZTVndH", "props": { "calcType": "Dever" }, "children": [ { "uid": "YWdpdz", "props": { "calcType": "Structure", "loiDebit": "TriangularTruncWeirFree" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 435224.52874659264 }, { "symbol": "BT", "mode": "SINGLE", "value": 834000.9388248117 }, { "symbol": "ZT", "mode": "SINGLE", "value": 122035.3210189593 }, { "symbol": "CdT", "mode": "SINGLE", "value": 10 } ] } ], "parameters": [ { "symbol": "Q", "mode": "CALCUL" }, { "symbol": "Z1", "mode": "SINGLE", "value": 435224.52874657186 }, { "symbol": "BR", "mode": "SINGLE", "value": 377590.4669003834 }, { "symbol": "ZR", "mode": "SINGLE", "value": 1783112.6327096 } ] } ] }`;
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("ZTVndH");
            nub.CalcSerie();
            checkResultConsistency(nub);
        });
        it("2-CalcSerie() should return consistent result with no NaN or Infinity", () => {
            // tslint:disable-next-line:max-line-length
            const sessionJson = `{"header":{"source":"jalhyd","format_version":"1.1","created":"2019-08-02T13:50:05.186Z"},"session":[{"uid":"MnFiMX","props":{"calcType":"Dever"},"children":[{"uid":"ZHY4cn","props":{"calcType":"Structure","loiDebit":"TriangularTruncWeirFree"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":689767.6903101245},{"symbol":"BT","mode":"SINGLE","value":173652.02957055828},{"symbol":"ZT","mode":"SINGLE","value":681292.3529449722},{"symbol":"CdT","mode":"SINGLE","value":2.4436229141892696}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":694070.3903202948},{"symbol":"BR","mode":"SINGLE","value":598136.6807876453},{"symbol":"ZR","mode":"SINGLE","value":1413511.597493809}]}]}`;
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("MnFiMX");
            nub.CalcSerie();
            checkResultConsistency(nub);
        });
    });
});
