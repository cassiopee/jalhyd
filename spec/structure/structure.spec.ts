import { StructureFlowMode, StructureFlowRegime, StructureJetType } from "../../src/structure/structure";
import { checkResult } from "../test_func";
import { CreateStructTest, StructureTest } from "./structure_test";

describe("Class Structure: ", () => {
    let structTest: StructureTest;
    beforeAll(() => {
        structTest = CreateStructTest();
    });
    describe("getFlowMode()", () => {
        it("Flow Mode should be WEIR", () => {
            expect(structTest.testGetFlowMode()).toBe(StructureFlowMode.WEIR);
        });
        it("Flow Mode should be ORIFICE", () => {
            structTest.prms.W.v = 10;
            expect(structTest.testGetFlowMode()).toBe(StructureFlowMode.ORIFICE);
            structTest.prms.W.v = Infinity;
        });
    });

    describe("getFlowRegime()", () => {
        it("Flow Regime should be FREE", () => {
            expect(structTest.testGetFlowRegime()).toBe(StructureFlowRegime.FREE);
        });
        it("Flow Regime should be SUBMERGED (WEIR)", () => {
            structTest.prms.Z2.v = 21;
            expect(structTest.testGetFlowRegime()).toBe(StructureFlowRegime.SUBMERGED);
        });
        it("Flow Regime should be PARTIAL (ORIFICE)", () => {
            structTest.prms.Z2.v = 21;
            structTest.prms.W.v = 15;
            expect(structTest.testGetFlowRegime()).toBe(StructureFlowRegime.PARTIAL);
        });
        it("Flow Regime should be SUBMERGED (ORIFICE)", () => {
            structTest.prms.Z2.v = 26;
            structTest.prms.W.v = 15;
            expect(structTest.testGetFlowRegime()).toBe(StructureFlowRegime.SUBMERGED);
            structTest.prms.Z2.v = 15;
            structTest.prms.W.v = Infinity;
        });
    });

    describe("Calc()", () => {
        const flagsNull = {
            ENUM_StructureFlowMode: StructureFlowMode.NULL,
            ENUM_StructureFlowRegime: StructureFlowRegime.NULL,
            ENUM_StructureJetType: StructureJetType.SO
        };
        it("Z1=Z2 => Q=0", () => {
            structTest.prms.Z2.v = structTest.prms.Z1.v;
            checkResult(structTest.Calc("Q"), 0);
            expect(structTest.Calc("Q").extraResults).toEqual(flagsNull);
            structTest.prms.Z2.v = 15;
        });
        it("W=0 => Q=0", () => {
            structTest.prms.W.v = 0;
            const r = structTest.Calc("Q");
            checkResult(r, 0);
            expect(r.extraResults).toEqual(flagsNull);
            structTest.prms.W.v = Infinity;
        });
        it("Q=0 => Z1=Z2", () => {
            structTest.prms.Q.singleValue = 0;
            structTest.calculatedParam = structTest.prms.Q;
            checkResult(structTest.CalcSerie(), structTest.prms.h2.v);
            // expect(structTest.Calc("Z1").values).toEqual(flagsNull);
            structTest.prms.Q.singleValue = 1;
        });
        it("Q=0 => W=0", () => {
            structTest.prms.Q.v = 0;
            structTest.prms.W.v = 1; // Sinon, W = Infinity => Structure = Seuil => Throw Error
            checkResult(structTest.Calc("W"), 0);
            expect(structTest.Calc("W").extraResults).toEqual(flagsNull);
            structTest.prms.Q.v = 1;
        });
        // TODO Test inversion de débit
    });
});
