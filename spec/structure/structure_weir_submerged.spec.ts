import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureWeirSubmerged } from "../../src/structure/structure_weir_submerged";
import { MessageCode } from "../../src/util/message";
import { itCalcQ } from "../structure/functions";
import { precDigits } from "../test_config";

function getStructTest(): StructureWeirSubmerged {
    return new StructureWeirSubmerged(new RectangularStructureParams(0, 101, 102, 101.9, 0.2, 0.9), false);
}

describe("Class StructureWeirSubmerged: ", () => {
    describe("Calc(Q): ", () => {
        const Z1: number[] = [102];
        const Q: number[] = [0.227];
        const mode: StructureFlowMode = StructureFlowMode.WEIR;
        const regime: StructureFlowRegime = StructureFlowRegime.SUBMERGED;
        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructTest(), Z1[i], Infinity, Q[i], mode, regime);
        }
    });
    describe("Calc(Z1): ", () => {
        it("Z1(Q=0.780) should be 75.234", () => {
            const testZ1 = new StructureWeirSubmerged(
                new RectangularStructureParams(0.780, 73.665, 75.301, 75.077, 0.35, 0.9),
                false);
            testZ1.prms.Z1.v = 100; // Test with initial condition far from solution
            expect(testZ1.Calc("Z1").vCalc).toBeCloseTo(75.234, precDigits);
        });
    });
    describe("Calcul avec h2/h1 < 0.8 (=0.5) : ", () => {
        it("le log devrait contenir 1 message", () => {
            const structTest = getStructTest();
            structTest.prms.Z1.singleValue = 110;
            structTest.prms.Z2.singleValue = 105;
            structTest.prms.ZDV.singleValue = 100;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(2);
            const ok = res.log.messages[0].code === MessageCode.WARNING_WEIR_SUBMERGENCE_LOWER_THAN_08 || res.log.messages[1].code === MessageCode.WARNING_WEIR_SUBMERGENCE_LOWER_THAN_08;
            expect(ok).toBe(true);
        });
    });
});
