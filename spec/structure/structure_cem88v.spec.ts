import { CreateStructure, LoiDebit, MessageCode, Result } from "../../src/index";
import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureGateCem88v } from "../../src/structure/structure_gate_cem88v";
import { itCalcQ } from "./functions";

let structPrm: RectangularStructureParams = new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0);
let  structTest: StructureGateCem88v = new StructureGateCem88v(structPrm, false);

describe("Class StructureGateCem88v: ", () => {
    beforeEach( () => {
        structPrm = new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0);
        structTest = new StructureGateCem88v(structPrm, false);
    });
    describe("Calcul Q avec W croissant: ", () => {
        const W: number[] = [0.000000, 0.100000, 0.200000, 0.300000, 0.400000, 0.500000,
            0.600000, 0.700000, 0.800000, 0.900000, 1.000000, 1.100000, 1.200000, 1.300000];
        const h1: number = 1.200000;
        const Q: number[] = [.000000, 0.328260, 0.641822, 0.823867, 1.117381, 1.720738,
            2.225472, 2.575336, 2.873893, 3.113250, 3.280545, 3.349403, 3.149324, 3.149324];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1, W[i], Q[i]);
        }
    });
    describe("Calcul Q en charge avec h1 croissant: ", () => {
        const W: number = 0.8;
        const h1: number[] = [1.05, 1.3, 1.5];
        const Q: number[] = [1.365897, 3.623277, 4.214572];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.PARTIAL, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const W: number = Infinity;
        const h1: number[] = [1.100000, 1.500000];
        const Q: number[] = [2.086781, 5.207945];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul W avec débit trop élevé", () => {
        it("Calc(W) should return error", () => {
            const s = CreateStructure(LoiDebit.GateCem88v);
            s.prms.Q.singleValue = 9;
            s.calculatedParam = s.prms.W;
            const res: Result = s.CalcSerie();
            expect(res.log.messages[0].code).toBe(MessageCode.ERROR_DICHO_TARGET_TOO_HIGH);
        });
    });
});
