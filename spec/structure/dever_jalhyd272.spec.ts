import { Session, MessageCode } from "../../src/index";
import { checkResultConsistency } from "../test_func";

describe("Dever", () => {
    describe("jalhyd#272", () => {
        it("1-CalcSerie() should return an error", () => {
            // tslint:disable-next-line:max-line-length
            const sessionJson = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2020-09-14T09:49:37.398Z"},"settings":{"precision":1e-7,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"d2IwYX","props":{"calcType":"Dever"},"meta":{"title":"Déver. dénoyés"},"children":[{"uid":"eHlwY2","props":{"calcType":"Structure","loiDebit":"WeirFree","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1.95},{"symbol":"L","mode":"SINGLE","value":30},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":2},{"symbol":"BR","mode":"MINMAX","min":0.783,"max":0.784,"step":0.001,"extensionStrategy":0},{"symbol":"ZR","mode":"SINGLE","value":0}]},{"uid":"eW4yaG","props":{"calcType":"Dever"},"meta":{"title":"Déver. dénoyés 1"},"children":[{"uid":"bTYxcn","props":{"calcType":"Structure","loiDebit":"WeirFree","structureType":"SeuilRectangulaire"},"children":[],"parameters":[{"symbol":"ZDV","mode":"SINGLE","value":1.95},{"symbol":"L","mode":"SINGLE","value":30},{"symbol":"CdWR","mode":"SINGLE","value":0.4}]}],"parameters":[{"symbol":"Q","mode":"CALCUL"},{"symbol":"Z1","mode":"SINGLE","value":2},{"symbol":"BR","mode":"SINGLE","value":0.784},{"symbol":"ZR","mode":"SINGLE","value":0}]}]}`;
            Session.getInstance().unserialise(sessionJson);
            const nub = Session.getInstance().findNubByUid("d2IwYX");
            nub.CalcSerie();
            checkResultConsistency(nub);
            expect(nub.result.ok).toBeFalse();
            expect(nub.result.log.messages[0].code).toBe(MessageCode.ERROR_DICHO_CONVERGE);
        });
    });
});
