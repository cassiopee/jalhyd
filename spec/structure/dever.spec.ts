import { ParamCalculability } from "../../src/param/param-definition";
import { Dever } from "../../src/structure/dever";
import { DeverParams } from "../../src/structure/dever_params";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";
import { Session } from "../../src/session";
import { MessageCode } from "../../src/util/message";
import { checkResultConsistency } from "../test_func";

function getDever(): Dever {
    const d: Dever = new Dever(
        new DeverParams(
            3.8043,     // rQ Débit total (m3/s)
            102,   // rZ1 Cote de l'eau amont (m)
            2,     // rBR Largeur du cours d'eau amont (m)
            100    // rZR Cote du lit du cours d'eau amont (m)
        ),
        false // debug
    );

    d.addChild(CreateStructure(LoiDebit.WeirFree, d, false));
    d.structures[0].prms.ZDV.singleValue = 101;
    // ZDV=100.1; ZH=101; Alpha2=0.9; CdT=1.36
    return d;
}

let dever: Dever = getDever();

describe("Class Dever:", () => {
    beforeEach(() => {
        dever = getDever();
    });
    for (const p of dever.parameterIterator) {
        if ([ParamCalculability.EQUATION, ParamCalculability.DICHO].includes(p.calculability) && p.visible) {
            it(`Calc(${p.symbol}) should return ${p.currentValue}`, () => {
                dever.calculatedParam = dever.getParameter(p.symbol);
                const ref: number = p.currentValue;
                dever.calculatedParam.singleValue = dever.calculatedParam.singleValue / 2;
                expect(dever.CalcSerie().vCalc).toBeCloseTo(ref, 3);
                expect(dever.result.values.V).toBeCloseTo(0.95107, 3);
                expect(dever.result.values.Ec).toBeCloseTo(0.046103, 3);
                expect(dever.result.values.Cv).toBeCloseTo(1.07358, 3);
            });
        }
    }

    it("warnings should appear when ZDV < ZR", () => {
        dever.addChild(CreateStructure(LoiDebit.WeirFree, dever, false));
        dever.addChild(CreateStructure(LoiDebit.WeirFree, dever, false));
        // should have 3 children now
        dever.structures[0].prms.ZDV.singleValue = 100;
        dever.structures[1].prms.ZDV.singleValue = 101;
        dever.structures[2].prms.ZDV.singleValue = 100.4;
        // adjust ZR
        dever.prms.ZR.singleValue = 100.5;
        const res = dever.CalcSerie();
        expect(res.log.messages.length).toBe(2);
        expect(res.log.messages[0].code).toBe(MessageCode.WARNING_DEVER_ZDV_INF_ZR);
        expect(res.log.messages[0].extraVar.number).toBe("1");
        expect(res.log.messages[1].code).toBe(MessageCode.WARNING_DEVER_ZDV_INF_ZR);
        expect(res.log.messages[1].extraVar.number).toBe("3");
    });

    it("No NaN in result when Q is undefined", () => {
        dever.prms.Q.setValue(undefined);
        dever.prms.Q.setCalculated();
        dever.CalcSerie();
        checkResultConsistency(dever);
    });
});

describe("Class Dever:", () => {
    it("bug nghyd#371", () => {
        const sess = `{ "header": { "source": "jalhyd", "format_version": "1.3", "created": "2020-03-09T10:10:18.089Z" }, "settings": { "precision": 1e-7, "maxIterations": 100, "displayPrecision": 3 }, "documentation": "", "session": [ { "uid": "eXphMG", "props": { "calcType": "Jet" }, "meta": { "title": "Longueur du jet" }, "children": [], "parameters": [ { "symbol": "V0", "mode": "MINMAX", "min": 0.5, "max": 1.75, "step": 0.25, "extensionStrategy": 0 }, { "symbol": "S", "mode": "SINGLE", "value": 0 }, { "symbol": "ZJ", "mode": "SINGLE", "value": 1.5 }, { "symbol": "ZW", "mode": "SINGLE", "value": 1.4 }, { "symbol": "ZF", "mode": "SINGLE", "value": 1 }, { "symbol": "D", "mode": "CALCUL" } ] }, { "uid": "dGVrMX", "props": { "calcType": "Dever" }, "meta": { "title": "Déver. dénoyés" }, "children": [ { "uid": "NTRjcn", "props": { "calcType": "Structure", "structureType": "SeuilRectangulaire", "loiDebit": "WeirFree" }, "children": [], "parameters": [ { "symbol": "ZDV", "mode": "SINGLE", "value": 1.4 }, { "symbol": "L", "mode": "SINGLE", "value": 1.75 }, { "symbol": "CdWR", "mode": "SINGLE", "value": 0.4 } ] } ], "parameters": [ { "symbol": "Q", "mode": "MINMAX", "min": 0.07, "max": 0.1, "step": 0.01, "extensionStrategy": 0 }, { "symbol": "Z1", "mode": "CALCUL" }, { "symbol": "BR", "mode": "SINGLE", "value": 0.7 }, { "symbol": "ZR", "mode": "SINGLE", "value": 0 } ] }, { "uid": "ZDdraH", "props": { "calcType": "YAXB" }, "meta": { "title": "Charge" }, "children": [], "parameters": [ { "symbol": "Y", "mode": "LINK", "targetNub": "dGVrMX", "targetParam": "Z1" }, { "symbol": "A", "mode": "SINGLE", "value": 1 }, { "symbol": "X", "mode": "CALCUL" }, { "symbol": "B", "mode": "LINK", "targetNub": "NTRjcn", "targetParam": "ZDV" } ] }, { "uid": "NXJzdn", "props": { "calcType": "YAXB" }, "meta": { "title": "Surface hydraulique" }, "children": [], "parameters": [ { "symbol": "Y", "mode": "CALCUL" }, { "symbol": "A", "mode": "LINK", "targetNub": "NTRjcn", "targetParam": "L" }, { "symbol": "X", "mode": "LINK", "targetNub": "ZDdraH", "targetParam": "X" }, { "symbol": "B", "mode": "SINGLE", "value": 0 } ] } ] }`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        // calculate Dever.
        const dever2 = Session.getInstance().findNubByUid("dGVrMX");
        dever2.CalcSerie();
        // check that all children have 4 result elements
        expect(dever2.result.resultElements.length).toBe(4);
        for (const c of dever2.getChildren()) {
            expect(c.result.resultElements.length).toBe(4);
        }
        // calculate downstream Linear func.
        const linear = Session.getInstance().findNubByUid("ZDdraH");
        linear.CalcSerie();
        // check again that all Dever. children have 4 result elements
        expect(dever2.result.resultElements.length).toBe(4);
        for (const c of dever2.getChildren()) {
            expect(c.result.resultElements.length).toBe(4);
        }
    });
});
