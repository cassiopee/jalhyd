import { CreateStructure, EnumEx, MessageCode, Session } from "../../src/index";
import { ParamCalculability, ParamDefinition } from "../../src/param/param-definition";
import { ParallelStructure } from "../../src/structure/parallel_structure";
import { Structure, StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { LoiDebit, StructureType } from "../../src/structure/structure_props";
import { Result } from "../../src/util/result";
import { precDigits } from "../test_config";
import { checkResult } from "../test_func";

export function CreateParalleleStructureTest(parallelStructure: ParallelStructure, exceptLoiDebit?: LoiDebit[]):
    {ps: ParallelStructure, ld: number[]} {
    // Ajout d'une structure de chaque type dans ParallelStructure
    const loiDebits: number[] = [];
    const loisAdmissibles: { [key: string]: LoiDebit[] } = parallelStructure.getLoisAdmissibles();
    for (const s of EnumEx.getValues(StructureType)) {
        const loiAdmStruct = loisAdmissibles[StructureType[s]];
        if (loiAdmStruct !== undefined) {
            for (const la of loiAdmStruct) {
                if(exceptLoiDebit === undefined || !exceptLoiDebit.includes(la)) {
                    parallelStructure.addChild(CreateStructure(la, parallelStructure, false));
                    loiDebits.push(la);
                }
            }
        }
    }
    // Session.getInstance().clear();
    // Session.getInstance().registerNub(parallelStructure);
    // console.log(Session.getInstance().serialise());
    return {ps: parallelStructure, ld: loiDebits};
}

export function itCalcQ(
    struct: Structure, Z1: number, W: number, Q: number,
    mode?: StructureFlowMode, regime?: StructureFlowRegime, precDigits2?: number) {

    // struct.debug("itCalQ " + struct.constructor.name + " Z1=" + Z1 + " W=" + W + " Q=" + Q);

    let res: Result;
    // struct.debug("struct.Calc(Q)=" + res.vCalc);
    describe("itCalcQ", () => {
        beforeEach( () => {
            struct.prms.Z1.v = Z1;
            struct.prms.W.v = W;
            res = struct.Calc("Q");
        });
        it("Q(Z1=" + Z1 + ",W=" + W + ") should be " + Q, () => {
            if (precDigits2 === undefined) { precDigits2 = precDigits; }
            struct.debug("struct.Calc(Q)=" + res.vCalc);
            expect(res.vCalc).toBeCloseTo(Q, precDigits2);
        });
        if (mode !== undefined) {
            it("Q(Z1=" + Z1 + ",W=" + W + ") Mode should be " + mode, () => {
                expect(res.values.ENUM_StructureFlowMode).toBe(mode);
            });
        }
        if (regime !== undefined) {
            it("Q(Z1=" + Z1 + ",W=" + W + ") Regime should be " + regime, () => {
                expect(res.values.ENUM_StructureFlowRegime).toBe(regime);
            });
        }
    });
}

/**
 * Test de calcul de tous les paramètres d'une loi de débit
 * @param st Structure à tester
 * @param mode Mode d'écoulement
 * @param regime Régime d'écoulement
 * @param bNotZDV Loi de débit ne permettant pas de calculer ZDV
 */
export function testStructure(
    st: Structure,
    mode: StructureFlowMode,
    regime: StructureFlowRegime,
    bNotZDV: boolean = false
) {
    for (const prm of st.prms) {
        if (prm.visible) { testStructureLoopPrm(st, bNotZDV, prm); }
        // TODO Add Test for Mode and Regime
    }
}

function testStructureLoopPrm(st: Structure, bNotZDV: boolean, prm: ParamDefinition) {
    describe(`Calc(${prm.symbol})`, () => {
        beforeAll(() => {
            st.calculatedParam = prm;
        });
        it(`should return exception`, () => {
            // Le calcul de l'ouverture sur les seuils doit renvoyer une exception (cas impossible)
            if ([ParamCalculability.DICHO, ParamCalculability.EQUATION].includes(prm.calculability)) {
                if (prm.symbol === "W" && prm.currentValue === Infinity) {
                    expect(
                        () => { st.CalcSerie(); }
                    ).toThrow(new Error("Structure:Calc : Calcul de W impossible sur un seuil"));
                }
            }
        });
        it(`should return an error`, () => {
            // Les lois CEM88D et CUNGE80 ne font pas intervenir ZDV dans le calcul d'un orifice noyé
            if ([ParamCalculability.DICHO, ParamCalculability.EQUATION].includes(prm.calculability)) {
                if (!(prm.symbol === "W" && prm.currentValue === Infinity)) {
                    if (bNotZDV) {
                        expect(
                            st.CalcSerie().log.messages[0].code
                        ).toBe(MessageCode.ERROR_STRUCTURE_ZDV_PAS_CALCULABLE);
                    }
                }
            }
        });
        it(`should return prm.currentValue`, () => {
            if ([ParamCalculability.DICHO, ParamCalculability.EQUATION].includes(prm.calculability)) {
                if (!(prm.symbol === "W" && prm.currentValue === Infinity)) {
                    if (!bNotZDV) {
                        checkResult(st.CalcSerie(prm.domain.getBoundedValue(prm.singleValue + 100)), prm.currentValue);
                    }
                }
            }
        });
    });
}

/**
 * Memorization of original parameter during tests
 */
let originalCalculatedValue: number;

/**
 * Test d'ouvrages en parallèle : calcul des débits individuels et test de tous les paramètres
 * @param o.ps objet ParallelStructure à tester
 * @param iStTypes Liste ordonnée des types des ouvrages à tester
 * @param iLoiDebits Liste ordonnée des lois de débit à tester
 */
export function testParallelStructures(o: { ps: ParallelStructure, ld: number[] }) {
    // Tests sur tous les ouvrages
    for (let i = 0; i < o.ps.structures.length; i++) {
        const st: Structure = o.ps.structures[i];
        describe(`this.structures[${i}]: Structure${LoiDebit[o.ld[i]]}: `, () => {
            beforeAll(() => {
                o.ps.calculatedParam = o.ps.prms.Q;
                o.ps.prms.Q.singleValue = o.ps.CalcSerie().vCalc;
            });
            // Tests sur les résultats complémentaires
            it(`Calc(Q).values[${i}.Q] should return o.ps.structures[${i}].Calc("Q").vCalc`, () => {
                o.ps.Calc("Q");
                expect(o.ps.structures[i].result.resultElement.values.Q).toBe(
                    o.ps.structures[i].Calc("Q").vCalc
                );
            });

            // Tests de calcul des paramètres des ouvrages
            for (const prm of st.prms) {
                if (prm.visible) {
                    describe(`Calc(${prm.symbol})`, () => {
                        const ref: number = prm.currentValue;
                        beforeAll(() => {
                            o.ps.calculatedParam = prm;
                        });
                        beforeEach(() => {
                            originalCalculatedValue = o.ps.calculatedParam.currentValue;
                            if ( // #136 Multiple solutions for GateCem88v ZDV
                                !(o.ps.calculatedParam.parentNub.getPropValue("loiDebit") === LoiDebit.GateCem88v
                                    && o.ps.calculatedParam.symbol === "ZDV")
                            ) {
                                // altering value to force looking for the solution
                                o.ps.calculatedParam.singleValue = Math.min(
                                    o.ps.calculatedParam.currentValue + 1000,
                                    o.ps.calculatedParam.domain.maxValue
                                );
                            }

                        });
                        afterEach(() => {
                            o.ps.calculatedParam.singleValue = originalCalculatedValue;
                        });
                        it(`should return exception`, () => {
                            // Le calcul de l'ouverture sur les seuils doit renvoyer une exception (cas impossible)
                            if (prm.symbol === "W" && prm.currentValue === Infinity) {
                                expect(
                                    () => { o.ps.CalcSerie(); }
                                ).toThrow(new Error("Structure:Calc : Calcul de W impossible sur un seuil"));
                            }
                        });
                        it(`should return an error`, () => {
                            // Les lois GateCEM88D et CUNGE80 ne permettent pas le calcul de ZDV
                            if (!(prm.symbol === "W" && prm.currentValue === Infinity)) {
                                if (prm.symbol === "ZDV" && !o.ps.structures[i].isZDVcalculable) {
                                    expect(
                                        o.ps.CalcSerie().log.messages[0].code
                                    ).toBe(MessageCode.ERROR_STRUCTURE_ZDV_PAS_CALCULABLE);
                                }
                            }
                        });
                        it(`should return ${ref}`, () => {
                            // Le calcul de l'angle de l'équation triangulaire n'est pas assez précis
                            if (!(prm.symbol === "W" && prm.currentValue === Infinity)) {
                                if (!(prm.symbol === "ZDV" && !o.ps.structures[i].isZDVcalculable)) {
                                    if (o.ld[i] === LoiDebit.TriangularWeirFree && prm.symbol === "alpha2") {
                                        checkResult(o.ps.CalcSerie(), ref, 1);
                                    }
                                }
                            }
                        });
                        it(`should return ${ref}`, () => {
                            // Cas normal : On teste la valeur calculée
                            if (!(prm.symbol === "W" && prm.currentValue === Infinity)) {
                                if (!(prm.symbol === "ZDV" && !o.ps.structures[i].isZDVcalculable)) {
                                    if (!(o.ld[i] === LoiDebit.TriangularWeirFree && prm.symbol === "alpha2")) {
                                        if (ParamCalculability.DICHO === prm.calculability) {
                                            // console.log(o.ps.serialise());
                                            checkResult(o.ps.CalcSerie(), ref);
                                            /* checkResult(o.ps.Calc({
                                                uid: o.ps.structures[i].uid,
                                                symbol: prm.symbol
                                            }), ref); */
                                        }
                                    }
                                }
                            }
                        });
                    });
                }
            }
        });
    }
}
