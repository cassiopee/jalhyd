import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureGateCem88v } from "../../src/structure/structure_gate_cem88v";

describe("jalhyd164 solveur robust:", () => {
    it("Calc(W) should return 0.91845", () => {
        const s: StructureGateCem88v = new StructureGateCem88v(
            new RectangularStructureParams(1.498, 99, 100, 99, 1, 0.6, 0.8),
            false
        );
        s.calculatedParam = s.prms.W;
        expect(s.CalcSerie().vCalc).toBeCloseTo(0.91845, 3);
    });
});
