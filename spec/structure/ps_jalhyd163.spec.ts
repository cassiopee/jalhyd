import { ParallelStructure, ParallelStructureParams, Session } from "../../src/index";
import { StructureOrificeFree } from "../../src/structure/structure_orifice_free";
import { StructureOrificeFreeParams } from "../../src/structure/structure_orifice_free_params";

describe("ParallelStructures, jalhyd#163, calculated variables present in the results − ", () => {

    it("when Z1 is high enough and Zco calc succeeds, 10 variables should be present", () => {
        // tslint:disable-next-line:max-line-length
        const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-10-29T14:06:53.775Z"},"settings":{"precision":0.0001,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"c292bn","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages 1"},"children":[{"uid":"bGxmYT","props":{"calcType":"Structure","structureType":"Orifice","loiDebit":"OrificeFree"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7},{"symbol":"Zco","mode":"CALCUL"}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.5},{"symbol":"Z1","mode":"SINGLE","value":105},{"symbol":"Z2","mode":"SINGLE","value":100}]}]}`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const nub = Session.getInstance().findNubByUid("c292bn");
        nub.CalcSerie();
        const vN = nub.result.resultElement.values;
        const vS = nub.getChildren()[0].result.resultElement.values;
        const vSK = Object.keys(vS);
        vSK.sort();
        const vNK = Object.keys(vN);
        vNK.sort();
        expect(vNK.length).toBe(1);
        expect(vNK).toEqual([ "Zco" ]);
        expect(vSK.length).toBe(5);
        expect(vSK).toEqual(
            [ "ENUM_StructureFlowMode", "ENUM_StructureFlowRegime", "ENUM_StructureJetType", "Q", "Zco" ]
        );
    });

    it("when Z1 is too low and Zco calc fails, 10 variables should be present", () => {
        // tslint:disable-next-line:max-line-length
        const sess = `{"header":{"source":"jalhyd","format_version":"1.3","created":"2019-10-29T14:06:53.775Z"},"settings":{"precision":0.0001,"maxIterations":100,"displayPrecision":3},"documentation":"","session":[{"uid":"c292bn","props":{"calcType":"ParallelStructure"},"meta":{"title":"Ouvrages 1"},"children":[{"uid":"bGxmYT","props":{"calcType":"Structure","structureType":"Orifice","loiDebit":"OrificeFree"},"children":[],"parameters":[{"symbol":"S","mode":"SINGLE","value":0.1},{"symbol":"CdO","mode":"SINGLE","value":0.7},{"symbol":"Zco","mode":"CALCUL"}]}],"parameters":[{"symbol":"Q","mode":"SINGLE","value":0.5},{"symbol":"Z1","mode":"SINGLE","value":90},{"symbol":"Z2","mode":"SINGLE","value":100}]}]}`;
        Session.getInstance().clear();
        Session.getInstance().unserialise(sess);
        const nub = Session.getInstance().findNubByUid("c292bn");
        nub.CalcSerie();
        const vN = nub.result.resultElement.values;
        const vS = nub.getChildren()[0].result.resultElement.values;
        const vSK = Object.keys(vS);
        vSK.sort();
        const vNK = Object.keys(vN);
        vNK.sort();
        expect(vNK.length).toBe(1);
        expect(vNK).toEqual([ "Zco" ]);
        expect(vSK.length).toBe(5);
        expect(vSK).toEqual(
            [ "ENUM_StructureFlowMode", "ENUM_StructureFlowRegime", "ENUM_StructureJetType", "Q", "Zco" ]
        );
    });

});
