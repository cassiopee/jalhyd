import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureWeirFree } from "../../src/structure/structure_weir_free";
import { MessageCode } from "../../src/util/message";
import { itCalcQ } from "./functions";

function getStructTest(): StructureWeirFree {
    return new StructureWeirFree(new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0), false);
}

describe("Class StructureWeirFree: ", () => {
    describe("Calcul Q avec W croissant: ", () => {
        const W: number[] = [
            0.000000, 1.300000];
        const h1: number = 1.200000;
        const Q: number[] = [6.987191, 6.987191];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructTest(), h1, W[i], Q[i]);
        }
    });
    describe("Calcul Q en charge avec h1 croissant: ", () => {
        const W: number = 0.8;
        const h1: number[] = [1.050000, 1.300000, 1.500000];
        const Q: number[] = [5.718929, 7.878541, 9.764896];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.FREE, StructureFlowRegime.FREE, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructTest(), h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const W: number = Infinity;
        const h1: number[] = [1.100000, 1.500000];
        const Q: number[] = [6.132249, 9.764896];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.FREE, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructTest(), h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul avec Z2 > ZDV : ", () => {
        it("le log devrait contenir 1 message", () => {
            const structTest = getStructTest();
            structTest.prms.Z2.singleValue = 102;
            structTest.prms.ZDV.singleValue = 101.5;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION);
        });
    });
});
