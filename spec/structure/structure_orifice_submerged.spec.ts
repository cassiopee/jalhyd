import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
// tslint:disable-next-line:max-line-length
import { StructureOrificeSubmerged } from "../../src/structure/structure_orifice_submerged";
import { StructureOrificeSubmergedParams } from "../../src/structure/structure_orifice_submerged_params";
import { itCalcQ } from "../structure/functions";

const prms: StructureOrificeSubmergedParams = new StructureOrificeSubmergedParams(0, 102, 101.5, 0.7, 0.1);
const test: StructureOrificeSubmerged = new StructureOrificeSubmerged(prms, false);

describe("Class StructureOrificeSubmerged: ", () => {
    describe("Calc(Q): ", () => {
        const h1: number[] = [102];
        const Q: number[] = [0.219];
        const mode: StructureFlowMode = StructureFlowMode.ORIFICE;
        const regime: StructureFlowRegime = StructureFlowRegime.SUBMERGED;
        for (let i = 0; i < Q.length; i++) {
            itCalcQ(test, h1[i], Infinity, Q[i], mode, regime);
        }
    });
});
