import { StructureTriangularWeirBroad } from "../../src/structure/structure_triangular_weir_broad";
import { CreateStructure } from "../../src/structure/factory_structure";
import { LoiDebit } from "../../src/structure/structure_props";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { testStructure } from "./functions";

describe("Class StructureTriangularWeirBroad: ", () => {
    it("Q free flow should be similar to Q limit submergence", () => {
        const st = CreateStructure(LoiDebit.TriangularWeirBroad);
        st.prms.Z2.singleValue = st.prms.ZDV.singleValue - 1;
        const rQFreeFlow  = st.CalcSerie().vCalc;
        st.prms.Z2.singleValue = st.prms.ZDV.singleValue + (st.prms.Z1.singleValue - st.prms.ZDV.singleValue) * 0.8001;
        expect(st.CalcSerie().vCalc).toBeCloseTo(rQFreeFlow, 1E-3);
    })
    it("Z2 similar to Z1 should lead to null discharge", () => {
        const st = CreateStructure(LoiDebit.TriangularWeirBroad);
        st.prms.Z2.singleValue = st.prms.Z1.singleValue - 0.001;
        expect(st.CalcSerie().vCalc).toBeCloseTo(0, 1E-3);
    })
    describe("Free flow:", () => {
        const st = CreateStructure(LoiDebit.TriangularWeirBroad);
        st.prms.Q.singleValue = st.CalcSerie().vCalc; // Initialisation of Q with exact answer
        testStructure(st, StructureFlowMode.WEIR, StructureFlowRegime.FREE)
    })
});