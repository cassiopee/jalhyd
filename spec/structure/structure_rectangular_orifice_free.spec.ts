import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureRectangularOrificeFree } from "../../src/structure/structure_rectangular_orifice_free";
import { MessageCode } from "../../src/util/message";
import { itCalcQ } from "./functions";

const structPrm: RectangularStructureParams = new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0);
const structTest: StructureRectangularOrificeFree = new StructureRectangularOrificeFree(structPrm, false);

describe("Class StructureOrificeFree: ", () => {
    describe("Calcul Q avec W croissant: ", () => {
        const W: number[] = [
            0.000000, 0.100000, 0.200000, 0.300000, 0.400000, 0.500000, 0.600000,
            0.700000, 0.800000, 0.900000, 1.000000, 1.100000, 1.200000, 1.300000];
        const h1: number = 1.200000;
        const Q: number[] = [0.000000, 0.582266, 1.164532, 1.746798, 2.329064, 2.911330,
            3.493596, 4.075861, 4.658127, 5.240393, 5.822659, 6.404925, 6.987191, 6.987191];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1, W[i], Q[i]);
        }
    });
    describe("Calcul Q en charge avec h1 croissant: ", () => {
        const W: number = 0.8;
        const h1: number[] = [1.050000, 1.300000, 1.500000];
        const Q: number[] = [4.357279, 4.848333, 5.207945];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.FREE, StructureFlowRegime.FREE, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const W: number = Infinity;
        const h1: number[] = [1.100000, 1.500000];
        const Q: number[] = [6.132249, 9.764896];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.FREE, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(structTest, h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul avec Z2 > ZDV : ", () => {
        it("le log devrait contenir 1 message", () => {
            structTest.prms.Z2.singleValue = 102;
            structTest.prms.ZDV.singleValue = 101.5;
            const res = structTest.CalcSerie().resultElement;
            expect(res.log.messages.length).toBe(1);
            expect(
                res.log.messages[0].code
            ).toBe(MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION);
        });
    });
});
