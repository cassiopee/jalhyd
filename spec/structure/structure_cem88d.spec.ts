import { RectangularStructureParams } from "../../src/structure/rectangular_structure_params";
import { StructureFlowMode, StructureFlowRegime } from "../../src/structure/structure";
import { StructureGateCem88d } from "../../src/structure/structure_gate_cem88d";
import { itCalcQ } from "./functions";

const structPrm: RectangularStructureParams = new RectangularStructureParams(1, 0, 1, 1, 2, 0.6, 0);

function getStructureGateCem88d(): StructureGateCem88d {
    return new StructureGateCem88d(structPrm, false);
}

describe("Class StructureGateCem88d: ", () => {
    describe("Calcul Q avec W croissant: ", () => {
        const W: number[] = [
            0.000000, 0.100000, 0.200000, 0.300000, 0.400000, 0.500000, 0.600000,
            0.700000, 0.800000, 0.900000, 1.000000, 1.100000, 1.200000, 1.300000];
        const h1: number = 1.200000;
        const Q: number[] = [0.000000, 0.617586, 1.235173, 1.852759, 2.470345, 3.087931,
            3.705518, 4.296608, 4.831177, 5.302464, 5.700445, 6.007777, 6.175863, 6.175863];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructureGateCem88d(), h1, W[i], Q[i]);
        }
    });
    describe("Calcul Q en charge avec h1 croissant: ", () => {
        const W: number = 0.8;
        const h1: number[] = [1.050000, 1.300000, 1.500000];
        const Q: number[] = [2.470345, 5.684601, 6.651906];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE, StructureFlowMode.ORIFICE];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.PARTIAL, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructureGateCem88d(), h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
    describe("Calcul Q a surface libre avec h1 croissant: ", () => {
        const W: number = Infinity;
        const h1: number[] = [1.100000, 1.500000];
        const Q: number[] = [4.366994, 9.764896];
        const mode: StructureFlowMode[] = [
            StructureFlowMode.WEIR, StructureFlowMode.WEIR];
        const regime: StructureFlowRegime[] = [
            StructureFlowRegime.SUBMERGED, StructureFlowRegime.FREE];

        for (let i = 0; i < Q.length; i++) {
            itCalcQ(getStructureGateCem88d(), h1[i], W, Q[i], mode[i], regime[i]);
        }
    });
});
