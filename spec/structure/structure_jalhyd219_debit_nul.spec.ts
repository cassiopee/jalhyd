import { RectangularStructureParams, StructureWeirFree } from "../../src/internal_modules";

describe("jalhyd#219 cote amont d'un ouvrage dénoyé à débit nul :", () => {
    function createStruct(z1: number, zr: number): StructureWeirFree {
        const s: StructureWeirFree = new StructureWeirFree(
            new RectangularStructureParams(0, // Q
                zr, // ZDV
                z1, // Z1
                92, // Z2
                1, // L
                0.6, // Cd
                0.8 // W
            ),
            false
        );
        s.calculatedParam = s.prms.Z1;
        return s;
    }

    it("cote amont initiale < Zr", () => {
        const z1: number = 90;
        const zr: number = 100;
        const s: StructureWeirFree = createStruct(z1, zr);
        expect(s.CalcSerie().vCalc).toBeCloseTo(zr, 3);
    });

    it("cote amont initiale = Zr", () => {
        const z1: number = 100;
        const zr: number = 100;
        const s: StructureWeirFree = createStruct(z1, zr);
        expect(s.CalcSerie().vCalc).toBeCloseTo(zr, 3);
    });

    it("cote amont initiale > Zr", () => {
        const z1: number = 110;
        const zr: number = 100;
        const s: StructureWeirFree = createStruct(z1, zr);
        expect(s.CalcSerie().vCalc).toBeCloseTo(zr, 3);
    });
});
