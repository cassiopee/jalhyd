nghyd_4.17.0 - 2023-05-30
===================

new features
------------

* Structure : ajout d'une erreur sur l'ennoiement (#302)
* Courbe de remous : rendre facultatif l'une des deux conditions limites en cote (#343)
* Conduites en charge : ajout de la loi de Strickler (#215)
* Courbe de remous : calculer une courbe de remous de passe à macro-rugosités (#325)
* Courbe de remous : renommer la ligne d'eau en ZW et fournir le tirant d'eau d'après celle ci (#333)

bug fixes
---------

* Solveur multimodule : le module existe toujours après suppression (#342)
* Structure : résultat du calcul de la cote amont dépendant de la cote initiale pour un débit nul (#219)
* Un paramètre cible d'un lien ne doit pas se lier à un autre paramètre (#341)
* Passe à macro-rugosités : des champs ne sont pas liables avec le module "Concentration de blocs" (#345)
* Courbe de remous : NaN produit lors d'un fuzzy test (#339)
* les résultats ne sont pas réinitialisés quand on modifie des paramètres globaux (#331)

changes
-------

* Restructuration de Lechapt et Calmon pour de nouvelles lois de pertes de charge (#334)
* Optimisation de l'affichage des unités (#338)
* Passe à macro-rugosités : la largeur conseillée doit avoir un cm de tolérance (#344)
* Fusionner les "select" avec "source" et les "select_custom" (#328)

nghyd_4.16.2 - 2023-01-10
===================

bug fixes
---------

* Lechapt et Calmon : erreur de sélection de matériau (#337)

nghyd_4.16.1 - 2022-11-16
===================

bug fixes
---------

* Un paramètre lié ne change pas d'état après la suppression du module cible (#329)

nghyd_4.16.0 - 2022-10-12
===================

new features
------------

* Ouvrages : modification des types d'ouvrages (#326)
* PAB : ajout de la charge et l'ennoiement dans le tableau de résultat et l'export (#324)

bug fixes
---------

* Section paramétrée: crash de l'appli sur variation de paramètre (#319)
* Courbe de remous (et bief): remonter une erreur quand le pas de discrétisation est supérieur la longueur du bief (#316)
* Module avec une section : le mode champs vide ne fonctionne pas (#327)
* Déplacement du paramètre calculé lors de la duplication d'un Nub (#322)
* Lois d'ouvrages : mauvaise gestion du paramètre calculé sur suppression d'ouvrage (#321)
* Section paramétrée : crash de l'appli sur variation de paramètre (#319)
* Courbe de remous (et bief) : remonter une erreur quand le pas de discrétisation est supérieur à la longueur du bief (#316)

changes:
--------

* Mettre à jour les paquets NPM (#318)

nghyd_4.15.1 - 2022-07-04
===================

new features
------------

* Structure: Modification de l'avertissement ennoiement (#314)

bug fixes
---------

* Paramètres liés : améliorer les informations sur la cible (#311)
* PréBarrages: les champs ne sont pas vides à la création du module (#310)
* PAB nombre: mauvaise colonne de résultat (#304)
* Plantage du calcul sur modules liés (#286)
* Liens inaccessibles pour certains modules (#289)
* Log : améliorer la synthèse de journal (#308)
* PAB: lancement du calcul possible avec des champs invalides (#317)

Changes:
--------

* Log : améliorer la synthèse de journal (#308)

nghyd_4.15.0 - 2022-05-04
===================

new features
------------
* Prébarrages : mettre les enfants invalides en rouge dans le schéma (#298)

bug fixes
---------

* Backwater curve : crash on incorrect inputs (#307)
* Sections : non convergence of critical depth calculation (#301)

Changes:
--------

* Manage a `modified` flag in `ParamDefinition` (#306)
* Vérificateur : remove PV parameter (#303)
* Remove field `_valueMode` in ParamDefinition (#300)
* CI : migrate to docker and run on Gitlab server at Lyon (#305)


nghyd_4.14.2 - 2021-03-25
===================

new features
------------

* MacroRugo: go back to equation of v4.13.1 (#297)

bug fixes
---------

* MacroRugo: Calculation error on Vmax (#294)

nghyd_4.14.1 - 2021-02-17
===================

new features
------------

* MacroRugo: use Cx instead of Cd0 in Cd calculation (#291)

bug fixes
---------

* Verificateur: defective message for submerged MacroRugo (#292)


nghyd_4.14.0 - 2021-02-16
===================

new features
------------

* MacroRugo: new equations (#283)
* MacroRugo: add average velocity in results (#285)
* MacroRugo: add equivalent Strickler result in linkable family Stricklers (#287)
* MacroRugo: add warning for concentration (#284)
* Verification: Submerged MacroRugo can't be crossed (#290)

bug fixes
---------
 * Rock ramp fish pass, error in calculation of dissipated power (#284)
 * Crash at session load for session with modules named with parenthesis (#281)


nghyd_4.13.0 - 2020-09-24
===================

new features
------------
 * New module "Pre-Dam"

bug fixes
---------
 * PAB, failed wall calculation: error doesn't tell which wall is affected


nghyd_4.12.1 - 2020-09-15
===================

bug fixes
---------
 * Grille: differenciate Ob and O for inclined grids
 * Deversoirs: Infinity when variating with low width


nghyd_4.12.0 - 2020-09-09
===================

new features
------------
 * Fish pass crossability verification
 * Attraction flow in fish ladders can now be variated and linked, including to other walls of the same fish ladder
 * Cloisons : replace Cunge80 law with CEM88D

bug fixes
---------
 * Solver: undefined X parameter when unserialising fails to set property
 * Solver: when searched parameter is an extraResult, it is not initialised when loading a session
 * CreateStructure: structureType not populated in jalhyd session
 * Verificateur: error when checking head on weirs, in downwall and if pass is varying
 * Verificateur: error in mandatory parameters detection, for PAB
 * PAB: when Z1 calculation fails in a wall, error is not contextualised
 * Verificateur: make minimum slot width mandatory, for PAB


nghyd_4.11.1 - 2020-08-11
===================

new features
------------
 * Log messages: add pointer to original result for contextualisation

bug fixes
---------
 * Bad formulation of Cunge discharge law for free flow orifice
 * Expose results in Structure and Dever for linking


nghyd_4.11.0 - 2020-07-28
===================

new features
------------
 * Baffle fishway setup
 * Baffle fishway simulation
 * Allow to link parameters of sections of identical types, without using families

bug fixes
---------
 * Fix discharge coefficient of submerged rectangular orifice
 * Cloisons: a (negative) sill is calculated for orifices, which triggers unwanted warnings
 * Prevent linking to invisible parameters
 * MRC: after variated calculation, fixed calculation is broken


nghyd_4.10.6 - 2020-07-21
===================

bug fixes
---------
 * Remove Lechapt-Calmon "NONE" material

nghyd_4.10.5 - 2020-06-30
===================

new features
------------
 * Uniform Flow, circular pipe: fatal error if pipe is under load
 * Walls: add Cunge80 to admissible laws
 * Parallel structures: add broad triangular weir
 * Rename "free" triangular discharge laws to "(Villemonte)"

bug fixes
---------
 * Slope definition in Jet
 * Parallel structures, Cunge80: set default flow coefficient to 1
 * Example "Weir jet length" broken
 * Gate opening, linked: bug on variated parameter length
 * Check definition domain when setting .singleValue
 * Walls: warning when weir bottom elevation is below upstream basin's bottom elevation

nghyd_4.10.4 (hotfix) - 2020-04-17
===================

bug fixes
---------
 * Parametric Section: critical depth does not converge on a closed circular section
 * Uniform Flow: error in speed calculation
 * Jet Impact: problem with fatal errors strategy
 * Strickler coefficient's unit
 * Make solver more robust when searching for start interval

nghyd_4.10.3 (hotfix) - 2020-03-12
===================

bug fixes
---------
 * dependsOnNubResult() considered a children as required when its parent's result was required
 * Solveur : empty targettedResult when unserializing causes calculation failure

nghyd_4.10.0 - 2020-02-24
===================

new features
------------
 * Solveur: ability to target an extra result, possibly on a single Nub
 * Lechapt-Calmon: add warning when speed is outside recommanded boundaries
 * update to Typescript 3.7

bug fixes
---------
 * PAB: sometimes "Jet type" is undefined
 * simultaneous variation of parameter and linked result
 * Solveur: working on a variated Nub chain should be forbidden

nghyd_4.9.0 - 2020-01-15
===================

new features
------------
 * New module "Blocks concentration"
 * Lechapt-Calmon: added singular loss of charge

documentation
-------------
 * Developers documentation
 * Class diagram
 * Typescript and Javascript boilerplates for CLI app development

nghyd_4.8.1 - 2019-12-20
===================

new features
------------
 * PAB Puissance: calculate all variables analytically
 * New rInit variable in ParamDefinition, that is never undefined, for DICHO calc initialization
 * Dever: warning if apron elevation of a device is below river bed
 * Grille: allow partial calculation
 * MacroRugo: warning if cell size is not in adequation with ramp width
 * Dever: calculate flow using head in formulas
 * Grille: add bars shape coefficients in results
 * Jet: new input parameters based on elevations
 * Cloisons: add sill calculation
 * MacroRugoCompound: inclined apron, add calculation of lateral slope
 * MacroRugo: remove technical guide flow and speed from results
 * PAB Nombre: add harmonized numbers of falls to results
 * MacroRugo: smooth transition between emerged / submerged regimes

bug fixes
---------
 * Fix bug in partial session saving
 * Remove VanneRectangulaire laws from CloisonAval
 * Fix bug in variatingLength()
 * Jet: fix bug in fall calculation
 * Jet: initial angle was ignored when fixed
 * Open channel: warning when overflowing
 * MacroRugo: adjust definition domain of Cd0
 * Larinier equations: set default CdWSL to 0.75 (previously 0.65)
 * RegimeUniforme: expose extraResult V in SPEEDS family
 * MacroRugoCompound: inclined apron - change cells repartition

nghyd_4.8.0 - 2019-11-26
===================

new features
------------
 * New module "Y = A.X + B"
 * New module "Trigonometry"
 * New module "Sum and product of powers"
 * New discharge equation "Weir Submerged"

bug fixes
---------
 * MacroRugo: set default value for Cd0 to 1.2
 * Remous: remove LargeurBerge form log
 * Fix bug in chain calculation of linked Nubs

nghyd_4.7.0 - 2019-10-29
===================

new features
------------
 * New module "Solver"
 * New discharge equation "Orifice Free"

bug fixes
---------
 * PAB: elevations issue on Cloisons
 * Chained calculation: stop calculation if one step fails
 * Remous: sometimes an abscissa is missing

nghyd_4.6.0 - 2019-10-14
===================

new features
------------
 * New module "Jet impact"
 * New module "Up/downstream elevations of a reach"
 * New module "Slope"
 * New module "Loss of charge, water grid"
 * Set Remous input parameters to elevations
 * SectionParametree: replace Yf et Yt with Ycor
 * RegimeUniforme: add average speed
 * CalcSerie(): when a parameter varies, add to global log an abstract of errors/warnings
 * Store précision and max. number of iterations in Session, and apply the latter to Dichotomie.maxIterations
 * Add warnings for Parallel Structures usage conditions

bug fixes
---------
 * SectionParametree: input parameters were ignored
 * MacroRugo submerged: flows gap
 * Remous: when reach length is not a multiple of discretisation step, calculate last abscissa anyway
 * Remous: crash with a parameter linked to a not-yet-calculated result
 * CourbeRemous: sometimes 1st ordinate of torrential curve is missing
 * MacroRugo: calculation does not converge for low depths

nghyd_4.4.2 - 2019-08-06
===================

new features
------------
 * refactored Result, ResultElement, removed ExtraResult
 * fuzzy tests
 * Scilab scripts for testing MacroRugo
 * move Nub.V method to getter ParamDefinition.V
 * updated dependencies
 * added typedoc script
 * replace ParamCalculability.NONE with .FIXED
 * adjust parameters calculability on SectionParametree, CourbeRemous, RegimeUniforme
 * added unit in ParamDefinition constructor
 * hardcoded materials presets for Lechapt Calmon
 * hardcoded available section types for acSection
 * hardcoded available resolution methods and calculable variables for CourbeRemous
 * improved stop criteria of brent solver (related to #116 )

bug fixes
---------
 * hide session loading errors that have no consequences
 * fix bugs in CourbeRemous
 * break links when generating PAB from Cloisons
 * copy broken links values when serializing a partial session
 * converted ERROR_DEVER_ZR_SUP_Z1 to WARNING
 * parallelStructures : stop copying structures extra results into parent result
 * Dever robustness improvement
 * convergence PAB
 * PAB: calculation error without log
 * serialize extension strategy
 * stop exposing PAB Cloisons parameters
 * getInferredValuesList() in MINMAX mode : protection against 0 step
 * fixed many bugs in different Structures
 * smarter detection of depending Nubs
 * CourbeRemous input values were ignored
 * serialize enum keys rather than values
 * fix bug in PAB session loading
 * Cloisons: fix bug in ZDV calculation
 * removed references t NodeType.StructureRectangle

nghyd_4.4.0 - 2019-07-16
===================

new features
------------
 * new PAB module
 * multiple variated parameters
 * Structure: Better management of discharge inversion
 * added Fente noyée (Larinier 1992)
 * added lift gate implementation
 * replaced dichotomy with Brent method
 * new strategy for Result and ResultElement assignment
 * proper separation of singleValue, currentValue and .v
 * better strategy for .V() getter in Nub
 * debugging with Karma

bug fixes
---------
 * improved (de)serialization
 * fix bug in cascade calculation
 * fix ZDV calculation in parallel structures
 * fix ZDV calculation with KiVi discharge law
 * Cloisons: replaced ZDV with h1 (head)
 * stop exposing already linked parameters
 * dichotomy: log message when looking for a solution outside interval
 * fix structures parameters iterator

nghyd_4.3.0 - 2019-04-12
===================

new features
------------
 * improved session management
 * moved calculated parameter, value modes and value modes consistency check to model
 * chain computation and results invalidation
 * easier access to variated results
 * new linked parameters system with families
 * updated jasmine tests

bug fixes
---------
 * fixed many parameters-related bugs in Section / Remous / Structure

stable - 2019-03-05
===================

new features
------------
 * add Rock-ramp fishpasses module
 * UIDs are now strings
 * add quiet option to package script
 * implement serialisation/deserialisation at model level
 * manage parameters visibility
 * admissible laws depend on structure type
 * generic management of compute precision Pr

bug fixes
---------
 * linked parameters are properly saved
 * improve relations between parallel structures and their parent
 * prevent UID collisions
 * variated parameters: properly take upper bound in account
 * merge some classes and remove unused code
 * lint code

old_stable - 2018-08-13
=======================
